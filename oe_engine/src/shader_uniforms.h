#ifndef __OE_ENGINE_SHADER_UNIFORMS_H__
#define __OE_ENGINE_SHADER_UNIFORMS_H__

#include "oe_engine.h"

// OE_Core
#include <eigen_helpers.h>

namespace OE_Engine
{

namespace BaseShader
{

struct View
{
    alignas(16) Mat4 m_viewProjection;
    alignas(16) Mat3 m_normal;
    alignas(16) Vec3 m_lightDir;
    alignas(16) Vec4 m_ambient;
};

struct Material
{
    alignas(16) Vec4 m_specular;
    alignas(4) float m_shininess;
};

struct Instance
{
    alignas(16) Mat4 m_world;
    alignas(16) Mat4 m_worldView;
};

}

namespace Font
{
    struct UBO
    {
        Mat4 m_transform;
    };
}

namespace GouraudColor
{
    struct Transform_UBO
    {
        alignas(16) Mat4 m_mvp;
        alignas(16) Mat4 m_modelView;
        alignas(16) Mat3 m_normal;
    };

    struct Lighting_UBO
    {
        alignas(16) Vec3 m_lightDir;
        alignas(16) Vec4 m_ambient;
        alignas(16) Vec4 m_specular;
        alignas(4) float m_shininess;
    };
}

}

#endif

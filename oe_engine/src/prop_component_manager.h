#ifndef __OE_ENGINE_PROP_COMPONENT_MANAGER_H__
#define __OE_ENGINE_PROP_COMPONENT_MANAGER_H__

#include "oe_engine.h"

// OE_Core
#include <singleton.h>

// OE_Engine
#include "prop_component.h"


namespace OE_Engine
{

class OE_ENGINE_EXPORT PropComponentManager :
    public ComponentManager<PropComponent>,
    public OE_Core::Singleton<PropComponentManager>
{
public:
    NO_COPY(PropComponentManager);

    PropComponentManager();
    virtual ~PropComponentManager();
};

} // OE_Engine

#endif // __OE_ENGINE_PROP_COMPONENT_MANAGER_H__

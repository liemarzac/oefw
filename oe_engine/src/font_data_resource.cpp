#include "font_data_resource.h"

// OE_Core
#include <file.h>
#include <task_manager.h>

// OE_Engine
#include "engine_private.h"


namespace OE_Engine
{

RTTI_IMPL_PARENT(FontDataResource, Resource)

FontDataResource::FontDataResource(const String& name):
    Resource(name)
{
}

FontDataResource::~FontDataResource()
{
}

void FontDataResource::loadImpl()
{
    TaskManager::getInstance().addTask(
            SharedPtr<FontDataResourceLoadTask>(new FontDataResourceLoadTask(this)));
}

const GlyphsData* FontDataResource::getGlyphsData(
    const String& fontName,
    uint32 fontSize)
{
    FontsData::const_iterator fontsDataItr = m_data.begin();
    for(; fontsDataItr != m_data.end(); ++fontsDataItr)
    {
        if((*fontsDataItr).m_fontName == fontName)
        {
            FontSizesData::const_iterator fontSizesDataItr =  (*fontsDataItr).m_fontSizes.find(fontSize);
            if(fontSizesDataItr != (*fontsDataItr).m_fontSizes.end())
            {
                return &(*fontSizesDataItr).second;
            }
            break;
        }
    }

    return nullptr;
}

FontDataResourceLoadTask::FontDataResourceLoadTask(FontDataResource* resource)
{
    m_resource = resource;
}

void FontDataResourceLoadTask::run()
{
    // Open file.
    String fileName("fonts/");
    fileName += m_resource->getName();
    fileName += ".fnt";

    std::ifstream stream;
    appGetResourceStream(fileName, stream, std::ios::binary);

    if(!stream.is_open())
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot open resource %s.", fileName.c_str());
        return;
    }

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Copy file to buffer.
    Buffer fileBuffer(fileSize);
    stream.read((char*)fileBuffer.m_rawData, fileSize);
    stream.close();

    // Deserialize buffer to data structure.
    deserialize(fileBuffer, m_resource->m_data);
    OE_CHECK(fileBuffer.isEndOfBuffer(), OE_CHANNEL_RESOURCE);

    m_resource->setState(Resource::State::Loaded);
}

} // namespace OE_Engine

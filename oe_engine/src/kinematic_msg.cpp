#include "kinematic_msg.h"

namespace OE_Engine
{

RTTI_IMPL_PARENT(KinematicMsg, Message)

KinematicMsg::KinematicMsg(void* sender) :
    Message(sender)
{
}

} // OE_Engine


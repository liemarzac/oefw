#ifndef __OE_ENGINE_CAMERA_H__
#define __OE_ENGINE_CAMERA_H__

#include "oe_engine.h"

// OE_Core
#include <eigen_helpers.h>

namespace OE_Engine
{

class OE_ENGINE_EXPORT Camera
{
public:
    EIGEN_ALIGNED

    Camera();
    ~Camera();

    const Vec3& getPosition() const
    {
        return m_position;
    }

    const Vec3& getLookAt() const
    {
        return m_look;
    }

    const Mat4& getWorldToViewMatrix() const
    {
        return m_worldToViewMatrix;
    }

    const Mat4& getViewToWorldMatrix() const
    {
        return m_viewToWorldMatrix;
    }

    float getHFov() const
    {
        return m_hfov;
    }

    void setPosition(const Vec3& position);
    void setDirection(const Vec3& direction);
    void setLookAt(const Vec3& position);

    bool update(float deltaT);
    void yaw(float radAngle);
    void pitch(float radAngle);
    void roll(float radAngle);
private:
    Camera& operator=(const Camera& rhs);
    Camera(Camera& rhs);

    void updateMatrix();

    Vec3 m_position;
    Quat m_orientation;
    Vec3 m_look;
    Vec3 m_right;
    Vec3 m_up;
    Mat4 m_worldToViewMatrix;
    Mat4 m_viewToWorldMatrix;
    float m_hfov;
    float m_pitch;
    float m_heading;
    float m_speedModifier;
}; // namespace OE_Engine

} // 

#endif // __OE_ENGINE_CAMERA_H__

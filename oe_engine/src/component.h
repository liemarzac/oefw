#ifndef __OE_ENGINE_COMPONENT_H__
#define __OE_ENGINE_COMPONENT_H__

#include "oe_engine.h"

// OE_Core
#include <flagset.h>
#include <ring_buffer.h>
#include <rtti.h>
#include <vector.h>

// OE_Engine
#include "message_queue.h"

#define OE_CHANNEL_COMPONENT "Component"

namespace OE_Engine
{

class ComponentMessage;

struct OE_ENGINE_EXPORT ComponentUpdateParams
{
    float m_deltaTime;
};

class OE_ENGINE_EXPORT Component
{
    RTTI_DECLARATION

public:
    Component();
    virtual ~Component();

    virtual bool connectTo(Component* recipient, uint32 messageTypeHash);
    virtual bool connectFrom(Component* sender, uint32 messageTypeHash);

    bool isAttached() const;
    void attach();
    void detach();
    bool acceptMessages() const;
    void acceptMessages(bool bAccept);

#if !OE_RELEASE
    void debug(bool bDebug);
    bool debug() const;
#endif

    template<class MessageType>
    MessageType* allocMessage(void* sender);

    template<class MessageType>
    MessageType* allocMessage(void* sender, const String& senderName);

    template<class MessageType>
    MessageType* allocMessage(class Component* sender);

    void commitMessage();

    virtual const String& getName() const;
    virtual void setName(const String& name);

protected:
    MessageQueue m_messageQueue;

    struct Recipient
    {
        Component* m_component;
        uint32 m_messageHashType;
    };

    Vector<Recipient> m_recipients;

private:
    enum class ComponentFlags
    {
        Attached,
        AcceptMessages,
#if !OE_RELEASE
        Debug
#endif
    };

    OE_Core::FlagSet<ComponentFlags> m_componentFlags;

    String m_name;

};

inline bool Component::isAttached() const
{
    return m_componentFlags.isSet(ComponentFlags::Attached);
}

inline void Component::attach()
{
    OE_CHECK(!isAttached(), OE_CHANNEL_COMPONENT);
    m_componentFlags.set(ComponentFlags::Attached);
}

inline void Component::detach()
{
    OE_CHECK(isAttached(), OE_CHANNEL_COMPONENT);
    m_componentFlags.remove(ComponentFlags::Attached);
}

template<class T>
inline T* Component::allocMessage(void* sender)
{
    if(m_componentFlags.isSet(ComponentFlags::AcceptMessages))
    {
        return m_messageQueue.allocMessage<T>(sender);
    }
    else
    {
        return nullptr;
    }
}

template<class T>
inline T* Component::allocMessage(void* sender, const String& senderName)
{
    if(m_componentFlags.isSet(ComponentFlags::AcceptMessages))
    {
        T* message = m_messageQueue.allocMessage<T>(sender);

#if _DEBUG
        message->setSenderName(senderName);
#endif

        return message;
    }
    else
    {
        return nullptr;
    }
}

template<class T>
inline T* Component::allocMessage(class Component* sender)
{
    if(m_componentFlags.isSet(ComponentFlags::AcceptMessages))
    {
        T* message = m_messageQueue.allocMessage<T>(sender);

#if _DEBUG
        message->setSenderName(sender->getName());
#endif

        return message;
    }
    else
    {
        return nullptr;
    }
}

inline void Component::commitMessage()
{
    return m_messageQueue.commitMessage();
}

inline bool Component::acceptMessages() const
{
    return m_componentFlags.isSet(ComponentFlags::AcceptMessages);
}

inline void Component::acceptMessages(bool bAccept)
{
    bAccept ? m_componentFlags.set(ComponentFlags::AcceptMessages) :
            m_componentFlags.remove(ComponentFlags::AcceptMessages);
}

#if !OE_RELEASE
inline void Component::debug(bool bDebug)
{
    bDebug ? m_componentFlags.set(ComponentFlags::Debug) :
             m_componentFlags.remove(ComponentFlags::Debug);
}

inline bool Component::debug() const
{
    return m_componentFlags.isSet(ComponentFlags::Debug);
}
#endif

} // namespace OE_Engine


#define DEFINE_COMPONENT_UPDATE_PARAMS(ComponentClass)              struct ComponentClass##UpdateParams :\
                                                                        public OE_Engine::ComponentUpdateParams\
                                                                    {\
                                                                    };

#define DEFINE_COMPONENT_UPDATE_TASK_PARAMS(ComponentClass)         struct ComponentClass##UpdateTaskParams :\
                                                                        public OE_Engine::ComponentUpdateTaskParams<ComponentClass, ComponentClass##UpdateParams>\
                                                                    {\
                                                                    };


#define DEFINE_COMPONENT_UPDATE_TASK(ComponentClass)                class ComponentClass##UpdateTask :\
                                                                        public OE_Engine::ComponentUpdateTask<\
                                                                            ComponentClass,\
                                                                            ComponentClass##UpdateTaskParams>\
                                                                    {\
                                                                    };



#endif // __OE_ENGINE_COMPONENT_H__

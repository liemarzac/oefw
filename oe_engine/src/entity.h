#ifndef __OE_ENGINE_ENTITY_H__
#define __OE_ENGINE_ENTITY_H__

#include "oe_engine.h"

#include "component.h"

namespace OE_Engine
{

class OE_ENGINE_EXPORT Entity
{
friend class EntityManager;

public:
    Entity();
    Entity(const String& name);

    virtual ~Entity();

    inline Entity* getPrev() const;
    inline Entity* getNext() const;

    void attachComponent(Component* component);
    void detachComponent(Component* component);
    Vector<Component*>& getComponents();

    template<class C>
    C* getComponent() const;

    void setName(const String& name);
    const String& getName() const;

private:
    Entity(const Entity& rhs);
    Entity& operator=(const Entity& rhs);

    Vector<Component*> m_components;
    Entity* m_prev;
    Entity* m_next;
    String m_name;
};

inline Entity* Entity::getPrev() const
{
    return m_prev;
}

inline Entity* Entity::getNext() const
{
    return m_next;
}

inline Vector<Component*>& Entity::getComponents()
{
    return m_components;
}

template<class C>
C* Entity::getComponent() const
{
    for(Component* component : m_components)
    {
        if(component->isA(C::getTypeHash()))
        {
            return static_cast<C*>(component);
        }
    }

    return nullptr;
}

} // namespace OE_Engine

#endif // __OE_ENGINE_ENTITY_H__

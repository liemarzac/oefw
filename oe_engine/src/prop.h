#ifndef __OE_ENGINE_PROP_H__
#define __OE_ENGINE_PROP_H__

// OE_Core
#include <callback.h>
#include <eigen_helpers.h>

// OE_Graphic
#include <graphic_resource_helpers.h>

// OE_Engine
#include "oe_engine.h"
#include "entity.h"

namespace OE_Engine
{

class PropComponent;

class OE_ENGINE_EXPORT Prop : public Entity
{
public:
    EIGEN_ALIGNED

    Prop();
    Prop(const String& name);

    virtual ~Prop();

    PropComponent* createPropComponent(OE_Graphic::PropResPtr propResource);
    void destroyPropComponent();

    virtual void getOrientation(Mat4& orientation) const;
    virtual void setOrientation(const Mat4& orientation);
    virtual void getPosition(Vec3& position) const;
    virtual void setPosition(const Vec3& position);
    virtual void onLoadedCallbackImpl(){}

    PropComponent* getPropComponent() const;

protected:
    PropComponent* m_propComponent;
    Vec3 m_position;
    Mat4 m_orientation;

private:
    Prop(const Prop& rhs);
    Prop& operator=(const Prop& rhs);

    OE_Core::CallbackEntry m_onLoadedCallback;

    void onLoadedCallback();
};

inline void Prop::onLoadedCallback()
{
    onLoadedCallbackImpl();
}

inline PropComponent* Prop::getPropComponent() const
{
    return m_propComponent;
}

} // OE_Engine

#endif // __OE_ENGINE_PROP_H__

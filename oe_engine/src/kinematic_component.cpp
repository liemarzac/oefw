#include "kinematic_component.h"

// OE_Engine
#include "engine_private.h"

// In messages
#include "kinematic_msg.h"

// Out messages
#include "spacial_state_msg.h"

namespace OE_Engine
{

RTTI_IMPL_PARENT(KinematicComponent, Component)

KinematicComponent::KinematicComponent()
{
    m_acceleration = Vec3d(0.0, 0.0, 0.0);
    m_velocity = Vec3d(0.0, 0.0, 0.0);
    m_position = Vec3d(0.0, 0.0, 0.0);
}

KinematicComponent::~KinematicComponent()
{
}

void KinematicComponent::update(const KinematicComponentUpdateParams& params)
{
    while(Message* message = m_messageQueue.beginMessage())
    {
        uint32 messageTypeHash = message->getTypeHash();
        if(KinematicMsg* castedMsg = cast<KinematicMsg>(message))
        {
            if(castedMsg->m_set.isSet(KinematicMsg::Flags::Acceleration))
                m_acceleration = castedMsg->m_acceleration;

            if(castedMsg->m_set.isSet(KinematicMsg::Flags::Velocity))
                m_velocity = castedMsg->m_velocity;

            if(castedMsg->m_set.isSet(KinematicMsg::Flags::Position))
            {
                m_position = castedMsg->m_position;
                m_initFlags.set(InitFlags::PositionSet);
            }
        }

        m_messageQueue.endMessage();
    }

    if(isInitialized())
    {
        m_velocity += m_acceleration * params.m_deltaTime;
        m_position += m_velocity * params.m_deltaTime;

        Vec3 position((float)m_position[0], (float)m_position[1], (float)m_position[2]);

        for(Recipient& recipient : m_recipients)
        {
            if(recipient.m_messageHashType == SpacialStateMsg::getTypeHash())
            {
                SpacialStateMsg* msg = recipient.m_component->allocMessage<SpacialStateMsg>(this);
                msg->setPosition(position);
                recipient.m_component->commitMessage();
            }
            else if(recipient.m_messageHashType == KinematicMsg::getTypeHash())
            {
                KinematicMsg* msg = recipient.m_component->allocMessage<KinematicMsg>(this);
                msg->setAcceleration(m_acceleration);
                msg->setVelocity(m_velocity);
                msg->setPosition(m_position);
                recipient.m_component->commitMessage();
            }
        }
    }
}

bool KinematicComponent::isInitialized() const
{
    return m_initFlags.areAllSet(InitFlags::NumberOf);
}

} // OE_Engine


#ifndef __OE_ENGINE_MANAGER_H__
#define __OE_ENGINE_MANAGER_H__

#include "oe_engine.h"

// OE_Core
#include <flagset.h>
#include <std_atomic_helpers.h>
#include <vector.h>


namespace OE_Engine
{

class OE_ENGINE_EXPORT Manager
{
public:
    Manager();
    virtual ~Manager();

    virtual void deinit(){}

    virtual void startFrame();
    virtual bool update(float deltaTime);
    bool finished() const;
    bool wasStarted() const;
    void addPrerequisite(Manager* other);

protected:
    bool prerequesitesFinished() const;

    enum class ManagerFlags
    {
        Update,
    };

    OE_Core::FlagSet<ManagerFlags> m_managerFlags;
    Vector<Manager*> m_prerequisites;

    enum class UpdateState
    {
        NotStarted,
        Started,
        Finished
    };

    std::atomic<UpdateState> m_updateState;

private:
    Manager(const Manager& rhs);
    Manager& operator=(const Manager& rhs);
};

} // namespace OE_Engine

#endif // __OE_ENGINE_MANAGER_H__

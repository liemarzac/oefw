#include "../entity_manager.h"
#include "../entity.h"

using namespace OE_Engine;

SUITE(EntityManagerTestSuite)
{
    TEST(NotRoot)
    {
        EntityManager entityManager;

        CHECK(entityManager.getRoot() == nullptr);
    }

    TEST(OneEntity)
    {
        EntityManager entityManager;
        Entity* entity1 = new Entity();

        entityManager.addEntity(entity1);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity1, root);
        CHECK_EQUAL(entity1, root->getNext());
        CHECK_EQUAL(entity1, root->getPrev());
    }

    TEST(TwoEntities)
    {
        EntityManager entityManager;
        
        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity1, root);

        Entity* next = root->getNext();
        CHECK_EQUAL(entity2, next);

        next = next->getNext();
        CHECK_EQUAL(entity1, next);

        Entity* prev = root->getPrev();
        CHECK_EQUAL(entity2, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity1, prev);
    }

    TEST(ThreeEntities)
    {
        EntityManager entityManager;

        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* entity3 = new Entity();
        entityManager.addEntity(entity3);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity1, root);

        Entity* next = root->getNext();
        CHECK_EQUAL(entity2, next);

        next = next->getNext();
        CHECK_EQUAL(entity3, next);

        next = next->getNext();
        CHECK_EQUAL(entity1, next);

        Entity* prev = root->getPrev();
        CHECK_EQUAL(entity3, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity2, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity1, prev);
    }

    TEST(RemoveSecondOfThree)
    {
        EntityManager entityManager;

        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* entity3 = new Entity();
        entityManager.addEntity(entity3);

        entityManager.removeEntity(entity2);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity1, root);

        Entity* next = root->getNext();
        CHECK_EQUAL(entity3, next);

        next = next->getNext();
        CHECK_EQUAL(entity1, next);

        Entity* prev = root->getPrev();
        CHECK_EQUAL(entity3, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity1, prev);
    }

    TEST(RemoveRoot)
    {
        EntityManager entityManager;

        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* entity3 = new Entity();
        entityManager.addEntity(entity3);

        entityManager.removeEntity(entity1);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity2, root);

        Entity* next = root->getNext();
        CHECK_EQUAL(entity3, next);

        next = next->getNext();
        CHECK_EQUAL(entity2, next);

        Entity* prev = root->getPrev();
        CHECK_EQUAL(entity3, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity2, prev);

        prev = prev->getPrev();
        CHECK_EQUAL(entity3, prev);
    }

    TEST(RemoveAllThree)
    {
        EntityManager entityManager;

        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* entity3 = new Entity();
        entityManager.addEntity(entity3);

        entityManager.removeEntity(entity1);
        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity2, root);

        entityManager.removeEntity(entity2);
        root = entityManager.getRoot();
        CHECK_EQUAL(entity3, root);

        entityManager.removeEntity(entity3);
        root = entityManager.getRoot();
        CHECK(root == nullptr);
    }

    TEST(RemoveAllButOne)
    {
        EntityManager entityManager;

        Entity* entity1 = new Entity();
        entityManager.addEntity(entity1);

        Entity* entity2 = new Entity();
        entityManager.addEntity(entity2);

        Entity* entity3 = new Entity();
        entityManager.addEntity(entity3);

        entityManager.removeEntity(entity1);
        entityManager.removeEntity(entity2);

        Entity* root = entityManager.getRoot();
        CHECK_EQUAL(entity3, root);
        CHECK_EQUAL(entity3, root->getPrev());
        CHECK_EQUAL(entity3, root->getNext());
    }
}


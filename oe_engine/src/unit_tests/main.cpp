#include <UnitTest++.h>
#include <iostream>
#include <TestReporterStdout.h>

// Tests
#include "test_entity_manager.h"

// Undefine main defined by the SDL.
#ifdef main
#undef main
#endif

int main(int, char const *[])
{
    const int testResult = UnitTest::RunAllTests();
    std::cin.get();
    return testResult;
}

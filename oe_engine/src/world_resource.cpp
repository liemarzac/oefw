#include "world_resource.h"

#include "engine_private.h"

// External
#include <json/json.h>

// OE_Graphic
#include <render_pipeline_resource.h>

// OE_Core
#include <task_manager.h>


namespace OE_Engine
{

RTTI_IMPL_PARENT(WorldResource, Resource)

WorldResource::WorldResource(const String& path) :
    Resource(path)
{
}

WorldResource::~WorldResource()
{
}

const char* WorldResource::getType() const
{
    return "world_resource";
}

void WorldResource::loadImpl()
{
    m_loadTaskPtr = MakeShared<ResourceLoadTask<Descriptor>>(
        getName(),
        MakeShared<Descriptor>(),
        &WorldResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void WorldResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    if(m_loadTaskPtr->isFinished())
    {
        const auto descriptor = m_loadTaskPtr->getDescriptor();
        m_renderPipelineResource = addDependency<RenderPipelineResource>(
            dependencyCallback,
            descriptor->m_renderPipelinePath);

        m_loadTaskPtr.reset();
        setState(State::Loaded);
    }
}

void WorldResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)
{
    Json::Value jsonRenderPipeline = jsonContent["render_pipeline"];
    OE_CHECK(jsonRenderPipeline.isString(), OE_CHANNEL_RESOURCE);
    {
        descriptor->m_renderPipelinePath = jsonRenderPipeline.asString();
    }
}


} // namespace OE_Engine

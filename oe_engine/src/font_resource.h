#ifndef __OE_ENGINE_FONT_RESOURCE_H__
#define __OE_ENGINE_FONT_RESOURCE_H__

#include "oe_engine.h"

// OE_Core
#include <resource.h>

// OE_Graphic
#include <material_resource.h>

// OE_Graphic
#include "font_data_resource.h"


namespace OE_Engine
{

class OE_ENGINE_EXPORT FontResource : public OE_Core::Resource
{
    RTTI_DECLARATION
public:
    FontResource(const String& name);

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "font_resource";
    }

    inline const GlyphsData* getGlyphsData(const String& fontName, uint32 fontSize)
    {
        return m_dataResource->getGlyphsData(fontName, fontSize);
    }

    inline SharedPtr<OE_Graphic::MaterialResource> getMaterialResource() const
    {
        return m_materialResource;
    }

private:
    FontResource();
    FontResource& operator=(const FontResource& rhs);
    FontResource(FontResource& rhs);

    SharedPtr<FontDataResource> m_dataResource;
    SharedPtr<OE_Graphic::MaterialResource> m_materialResource;
};

} // namespace OE_Engine

#endif // __OE_ENGINE_FONT_RESOURCE_H__

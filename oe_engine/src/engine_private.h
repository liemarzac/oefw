#ifndef __OE_ENGINE_PRIVATE_H__
#define __OE_ENGINE_PRIVATE_H__

#include "oe_engine.h"
#include <SDL.h>
#include <std_smartptr_helpers.h>

using namespace OE_Core;
using namespace OE_Graphic;

#endif // __OE_ENGINE_PRIVATE_H__

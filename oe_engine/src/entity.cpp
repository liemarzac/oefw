#include "entity.h"
#include "component.h"
#include "entity_manager.h"

#define OE_CHANNEL_ENTITY "Entity"

namespace OE_Engine
{

Entity::Entity():
    m_prev(nullptr)
    , m_next(nullptr)
{
}

Entity::Entity(const String& name) :
    m_prev(nullptr)
    ,m_next(nullptr)
{
    setName(name);
}

Entity::~Entity()
{
}

void Entity::attachComponent(Component* component)
{
    component->attach();
    m_components.push_back(component);
}

void Entity::detachComponent(Component* component)
{
    auto itr = std::find(m_components.begin(), m_components.end(), component);
    OE_CHECK(itr != m_components.end(), OE_CHANNEL_ENTITY);
    m_components.erase(itr);
    component->detach();
}

void Entity::setName(const String& name)
{
    m_name = name;
}

const String& Entity::getName() const
{
    return m_name;
}

} // namespace OE_Engine

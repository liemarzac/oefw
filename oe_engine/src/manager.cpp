#include "manager.h"

// External
#include <thread>

// OE_Engine
#include "engine.h"


namespace OE_Engine
{

Manager::Manager() :
    m_updateState(UpdateState::NotStarted)
{
    Engine* engine = Engine::getInstancePointer();
    if(engine)
    {
        engine->registerManager(this);
    }
}

Manager::~Manager()
{
}

void Manager::addPrerequisite(Manager* other)
{
    // TODO: Make sure there is not circular dependency.
    // Take prerequisite tree of other and check if this manager is not inside.
    addUnique(m_prerequisites, other);
}

void Manager::startFrame()
{
    m_updateState = UpdateState::NotStarted;
}

bool Manager::update(float deltaTime)
{
    if(!prerequesitesFinished())
        return false;

    m_updateState = UpdateState::Finished;
    return true;
}

bool Manager::finished() const
{
    return m_updateState == UpdateState::Finished;
}

bool Manager::wasStarted() const
{
    return m_updateState != UpdateState::NotStarted;
}

bool Manager::prerequesitesFinished() const
{
    for(Manager* manager : m_prerequisites)
    {
        if(!manager->finished())
        {
            return false;
        }
    }

    return true;
}

}

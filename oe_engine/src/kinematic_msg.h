#ifndef __OE_ENGINE_KINEMATIC_MSG_H__
#define __OE_ENGINE_KINEMATIC_MSG_H__

// OE_Core
#include <eigen_helpers.h>
#include <flagset.h>

// OE_Engine
#include "oe_engine.h"
#include "message.h"

namespace OE_Engine
{

class OE_ENGINE_EXPORT KinematicMsg : public Message
{
    RTTI_DECLARATION
    GET_SIZE_IMPL

public:
    EIGEN_ALIGNED

    KinematicMsg(void* sender);

    void setAcceleration(const Vec3d& acceleration);
    void setVelocity(const Vec3d& velocity);
    void setPosition(const Vec3d& position);

    enum class Flags
    {
        Acceleration,
        Velocity,
        Position
    };

    Vec3d m_acceleration;
    Vec3d m_velocity;
    Vec3d m_position;
    OE_Core::FlagSet<Flags> m_set;
};

inline void KinematicMsg::setAcceleration(const Vec3d& acceleration)
{
    m_acceleration = acceleration;
    m_set.set(Flags::Acceleration);
}

inline void KinematicMsg::setVelocity(const Vec3d& velocity)
{
    m_velocity = velocity;
    m_set.set(Flags::Velocity);
}

inline void KinematicMsg::setPosition(const Vec3d& position)
{
    m_position = position;
    m_set.set(Flags::Position);
}


} // OE_Engine

#endif // __OE_ENGINE_KINEMATIC_MSG_H__

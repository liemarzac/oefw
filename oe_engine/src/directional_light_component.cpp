#include "directional_light_component.h"

#include "engine_private.h"

namespace OE_Engine
{

RTTI_IMPL_PARENT(DirectionalLightComponent, Component)

DirectionalLightComponent::DirectionalLightComponent()
{
    m_direction = Vec3(0.0f, -1.0f, 0.0f);
}

DirectionalLightComponent::~DirectionalLightComponent()
{
}

};



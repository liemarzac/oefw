#ifndef __OE_ENGINE_WORLD_RESOURCE_H__
#define __OE_ENGINE_WORLD_RESOURCE_H__

#include "oe_engine.h"

// OE_Graphic
#include <graphic_resource_helpers.h>

// OE_Core
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Engine
{

class WorldResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    struct Descriptor
    {
        String m_renderPipelinePath;
    };

    WorldResource() = delete;
    WorldResource(const String& path);
    virtual ~WorldResource();

    virtual const char* getType() const override;
    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

private:
    SharedPtr<OE_Core::ResourceLoadTask<Descriptor>> m_loadTaskPtr;
    OE_Graphic::RenderPipelineResPtr m_renderPipelineResource;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_ENGINE_WORLD_RESOURCE_H__

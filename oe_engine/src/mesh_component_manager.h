#ifndef __OE_ENGINE_MESH_COMPONENT_MANAGER_H__
#define __OE_ENGINE_MESH_COMPONENT_MANAGER_H__

#include "oe_engine.h"

// OE_Core
#include <singleton.h>

// OE_Engine
#include "component_manager.h"
#include "mesh_component.h"


namespace OE_Engine
{

DEFINE_COMPONENT_UPDATE_TASK_PARAMS(MeshComponent)

DEFINE_COMPONENT_UPDATE_TASK(MeshComponent)

class OE_ENGINE_EXPORT MeshComponentManager : public ComponentManagerMT<
    MeshComponent,
    MeshComponentUpdateTask,
    MeshComponentUpdateTaskParams>,
    public OE_Core::Singleton<MeshComponentManager>
{
public:
    MeshComponentManager();
    virtual ~MeshComponentManager();

    virtual void prepareTaskParams(
        MeshComponentUpdateTaskParams* params,
        uint iPage,
        float deltaTime) override;

private:
    MeshComponentManager(const MeshComponentManager& rhs);
    MeshComponentManager& operator=(const MeshComponentManager& rhs);
};

} // namespace OE_Engine

#endif // __OE_ENGINE_MESH_COMPONENT_MANAGER_H__

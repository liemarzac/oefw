#include "world_scene.h"

#include "engine_private.h"

// OE_Engine
#include "entity_manager.h"
#include "light.h"
#include "prop.h"
#include "prop_component.h"
#include "composer.h"
#include "scene_command.h"

// OE_Graphic
#include <renderer.h>


namespace OE_Engine
{

WorldScene::WorldScene(Renderer* renderer) :
    m_renderer(renderer)
{
    m_replayCommands = new RingBuffer();
}

WorldScene::~WorldScene()
{
    delete m_replayCommands;
}

void WorldScene::update(float dT)
{
    // Add to the render scene all mesh instances which are have finished loading.
    for(int i = static_cast<int>(m_loadingMeshInstancePairs.size()) - 1; i >= 0; --i)
    {
        if(m_loadingMeshInstancePairs[i][0]->isLoaded())
        {
            // If we have a second mesh instance, it should also be loaded.
            OE_CHECK(!m_loadingMeshInstancePairs[i][1] || m_loadingMeshInstancePairs[i][1]->isLoaded(), OE_CHANNEL_WORLD_SCENE);

            if (m_loadingMeshInstancePairs[i][0] && m_loadingMeshInstancePairs[i][1])
            {
                addMeshInstancePairToRenderScene(m_loadingMeshInstancePairs[i]);
            }
            else
            {
                addMeshInstanceToRenderScene(m_loadingMeshInstancePairs[i][0]);
            }

            removeAtUnordered(m_loadingMeshInstancePairs, static_cast<size_t>(i));
        }
    }

    for(Light* light : m_lights)
    {
        SetDominantDirectionalLightSceneCommand sc;
        sc.m_direction = light->getDirection();
        sc.m_iRenderScene = DefaultRenderSceneType::World;
        sc.execute();
    }
}

void WorldScene::replay()
{
    void* ptr = nullptr;
    size_t availableSize = 0;
    while(m_replayCommands->beginRead(ptr, availableSize))
    {
        SceneCommand* sc = reinterpret_cast<SceneCommand*>(ptr);
        const size_t size = sc->getSize();
        sc->execute();
        sc->~SceneCommand();
        m_replayCommands->endRead(size);
    }
}

void WorldScene::addProp(Prop* prop)
{
    OE_CHECK(prop && prop->getPropComponent(), OE_CHANNEL_WORLD_SCENE);

    PropComponent* propComponent = prop->getPropComponent();

    m_props.push_back(prop);

    Vector<MeshInstancePtr> meshInstancesSceneA;
    propComponent->getMeshInstances(meshInstancesSceneA, 0);

    Vector<MeshInstancePtr> meshInstancesSceneB;
    propComponent->getMeshInstances(meshInstancesSceneB, 1);

    OE_CHECK(meshInstancesSceneA.size() > 0, OE_CHANNEL_WORLD_SCENE);
    OE_CHECK(meshInstancesSceneA.size() == meshInstancesSceneB.size(), OE_CHANNEL_WORLD_SCENE);

    for(size_t i = 0; i < meshInstancesSceneA.size(); ++i)
    {
        Array<MeshInstancePtr, 2> meshInstancePair;
        meshInstancePair[0] = meshInstancesSceneA[i];
        meshInstancePair[1] = meshInstancesSceneB[i];

        if(meshInstancePair[0]->isLoaded() && meshInstancePair[1]->isLoaded())
        {
            addMeshInstancePairToRenderScene(meshInstancePair);
        }
        else
        {
            addLoadingMeshInstancePair(meshInstancePair);
        }
    }
}

void WorldScene::addMeshComponent(MeshComponent* meshComponent)
{
    OE_CHECK(meshComponent, OE_CHANNEL_WORLD_SCENE);

    Array<MeshInstancePtr, 2> meshInstancePair;
    meshInstancePair[0] = meshComponent->getMeshInstance(0);
    meshInstancePair[1] = meshComponent->getMeshInstance(1);

    if(meshInstancePair[0]->isLoaded() && meshInstancePair[1]->isLoaded())
    {
        addMeshInstancePairToRenderScene(meshInstancePair);
    }
    else
    {
        addLoadingMeshInstancePair(meshInstancePair);
    }
}

void WorldScene::addMeshInstanceToRenderScene(MeshInstancePtr meshInstance)
{
    if (meshInstance->isLoaded())
    {
        // Immediate execution.
        AddMeshInstanceSceneCommand immediateCmd;
        immediateCmd.m_meshInstance = meshInstance;
        immediateCmd.execute();
    }
    else
    {
        Array<MeshInstancePtr, 2> meshInstancePair;
        meshInstancePair[0] = meshInstance;

        addLoadingMeshInstancePair(meshInstancePair);
    }
}

void WorldScene::addLoadingMeshInstancePair(const Array<MeshInstancePtr, 2>& meshInstancePair)
{
    m_loadingMeshInstancePairs.push_back(meshInstancePair);
}

void WorldScene::removeLoadingMeshInstancePair(const Array<OE_Graphic::MeshInstancePtr, 2> & meshInstancePair)
{
    bool bFound = false;
    for (size_t i = 0; i < m_loadingMeshInstancePairs.size(); ++i)
    {
        auto meshInstance = m_loadingMeshInstancePairs[i][0];
        if (meshInstance == meshInstancePair[0])
        {
            removeAtUnordered(m_loadingMeshInstancePairs, i);
            bFound = true;
            break;
        }
    }

    OE_CHECK(bFound, OE_CHANNEL_WORLD_SCENE);
}

void WorldScene::addMeshInstancePairToRenderScene(const Array<MeshInstancePtr, 2>& meshInstancePair)
{
    OE_CHECK(meshInstancePair[0] && meshInstancePair[1], OE_CHANNEL_WORLD_SCENE);

    uint32 iMainThreadSceneIndex = m_renderer->getMainThreadRenderSceneIndex();

    MeshInstancePtr meshInstanceImmediate = meshInstancePair[iMainThreadSceneIndex];
    MeshInstancePtr meshInstanceDeferred = meshInstancePair[(iMainThreadSceneIndex + 1) % 2];

    // Immediate execution.
    AddMeshInstanceSceneCommand immediateCmd;
    immediateCmd.m_meshInstance = meshInstanceImmediate;
    immediateCmd.execute();

    // Next frame execution.
    auto* deferredCmd = allocCommand<AddMeshInstanceSceneCommand>();
    deferredCmd->m_meshInstance = meshInstanceDeferred;
    commitCommand();
}

void WorldScene::removeMeshInstancePairFromRenderScene(const Array<OE_Graphic::MeshInstancePtr, 2> & meshInstancePair)
{
    uint32 iMainThreadSceneIndex = m_renderer->getMainThreadRenderSceneIndex();

    // Immediate execution.
    RemoveMeshInstanceSceneCommand immediateCmd;
    immediateCmd.m_meshInstance = meshInstancePair[iMainThreadSceneIndex];
    immediateCmd.execute();

    // Next frame execution.
    auto* deferredCmd = allocCommand<RemoveMeshInstanceSceneCommand>();
    deferredCmd->m_meshInstance = meshInstancePair[(iMainThreadSceneIndex + 1) % 2];
    commitCommand();
}

void WorldScene::removeProp(Prop* prop)
{
    OE_CHECK(prop && prop->getPropComponent(), OE_CHANNEL_WORLD_SCENE);

    PropComponent* propComponent = prop->getPropComponent();

    Vector<MeshInstancePtr> meshInstancesSceneA;
    propComponent->getMeshInstances(meshInstancesSceneA, 0);

    Vector<MeshInstancePtr> meshInstancesSceneB;
    propComponent->getMeshInstances(meshInstancesSceneB, 1);

    // All scenes must have the same number of mesh instances.
    OE_CHECK(meshInstancesSceneA.size() == meshInstancesSceneB.size(), OE_CHANNEL_WORLD_SCENE);

    const uint nMeshInstances = uint(meshInstancesSceneA.size());

    for(uint iMeshInstance = 0; iMeshInstance < nMeshInstances; ++iMeshInstance)
    {
        Array<MeshInstancePtr, 2> meshInstancePair;
        meshInstancePair[0] = meshInstancesSceneA[iMeshInstance];
        meshInstancePair[1] = meshInstancesSceneB[iMeshInstance];

        // Try removing from the loading list.
        removeLoadingMeshInstancePair(meshInstancePair);

        // Try removing from the render scene.
        removeMeshInstancePairFromRenderScene(meshInstancePair);
    }

    removeUnordered(m_props, prop);
}

void WorldScene::addLight(Light* light)
{
    OE_CHECK(light, OE_CHANNEL_WORLD_SCENE);

    SetDominantDirectionalLightSceneCommand* sc =
        allocCommand<SetDominantDirectionalLightSceneCommand>();
    sc->m_direction = light->getDirection();
    sc->m_iRenderScene = DefaultRenderSceneType::World;
    sc->execute();
    commitCommand();
    m_lights.push_back(light);
}

void WorldScene::removeLight(Light* light)
{
    OE_CHECK(light, OE_CHANNEL_WORLD_SCENE);

    removeUnordered(m_lights, light);
    RemoveDominantDirectionalLightSceneCommand* sc =
        allocCommand<RemoveDominantDirectionalLightSceneCommand>();
    sc->m_iRenderScene = DefaultRenderSceneType::World;
    sc->execute();
    commitCommand();
}

void WorldScene::commitCommand()
{
    return m_replayCommands->commit();
}

} // namespace OE_engine

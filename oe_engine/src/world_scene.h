#ifndef __OE_ENGINE_WORLD_SCENE_H__
#define __OE_ENGINE_WORLD_SCENE_H__

#include "oe_engine.h"

// OE_Core
#include <array.h>
#include <ring_buffer.h>
#include <singleton.h>
#include <vector.h>

namespace OE_Graphic
{
    class Renderer;
}


namespace OE_Engine
{
    class Light;
    class MeshComponent;
    class Prop;
    class PropComponent;

#define OE_CHANNEL_WORLD_SCENE "WorldScene"

class OE_ENGINE_EXPORT WorldScene
{
public:
    WorldScene(OE_Graphic::Renderer* renderer);
    ~WorldScene();
    NO_COPY(WorldScene);

    void addProp(Prop* prop);
    void removeProp(Prop* prop);
    void addLight(Light* light);
    void removeLight(Light* light);
    void addMeshComponent(MeshComponent* meshComponent);
    void addMeshInstanceToRenderScene(OE_Graphic::MeshInstancePtr meshInstance);

    void replay();
    void update(float dT);

private:
    template<class T>
    inline T* allocCommand();
    void commitCommand();

    void addLoadingMeshInstancePair(const Array<OE_Graphic::MeshInstancePtr, 2>& meshInstancePair);
    void removeLoadingMeshInstancePair(const Array<OE_Graphic::MeshInstancePtr, 2> & meshInstancePair);
    void addMeshInstancePairToRenderScene(const Array<OE_Graphic::MeshInstancePtr, 2>& meshInstancePair);
    void removeMeshInstancePairFromRenderScene(const Array<OE_Graphic::MeshInstancePtr, 2> & meshInstancePair);

    Vector<Prop*> m_props;
    Vector<Light*> m_lights;
    OE_Core::RingBuffer* m_replayCommands;
    Vector<Array<OE_Graphic::MeshInstancePtr, 2>> m_loadingMeshInstancePairs;
    OE_Graphic::Renderer* m_renderer = nullptr;
};

template<class T>
T* WorldScene::allocCommand()
{
    void* writePtr;
    bool hasAllocated = m_replayCommands->alloc(writePtr, sizeof(T));
    OE_CHECK(hasAllocated, OE_CHANNEL_ENGINE);
    return new(writePtr)T();
}

}

#endif // __OE_ENGINE_WORLD_SCENE_H__

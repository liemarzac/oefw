#ifndef __OE_ENGINE_LIGHT_H__
#define __OE_ENGINE_LIGHT_H__

#include "oe_engine.h"

// OE_Core
#include <eigen_helpers.h>

// OE_Engine
#include "entity.h"


namespace OE_Engine
{

class DirectionalLightComponent;

class OE_ENGINE_EXPORT Light : public Entity
{
public:
    Light();
    virtual ~Light();

    const Vec3& getDirection() const;
    void setDirection(const Vec3&);

private:
    Light(const Light& rhs);
    Light& operator=(const Light& rhs);

    DirectionalLightComponent* m_directionalComponent;
};

} // namespace OE_Engine

#endif // __OE_ENGINE_LIGHT_H__

#include "engine.h"

#include "engine_private.h"

// OE_Core
#include <application.h>
#include <file.h>
#include <rect_2d.h>

// OE_Graphic
#include <color.h>
#include <composer.h>
#include <renderer_config.h>

// OE_Engine
#include "font_manager.h"
#include "input_manager.h"
#include "font_data_resource.h"
#include "font_resource.h"
#include "kinematic_component_manager.h"
#include "manager.h"
#include "mesh_component_manager.h"
#include "package_resource.h"
#include "prop_component_manager.h"
#include "resource_manager.h"
#include "task_manager.h"
#include "world_resource.h"


namespace OE_Engine
{

Engine::Engine()
{
    m_composer = new Composer();
    m_window = NULL;
    m_isRendererInitialized = false;
    m_isSDLInitialized = false;

    m_targetFPS = 0;
    m_bVSync = true;

    KinematicComponentManager::createInstance();
    MeshComponentManager::createInstance();
    PropComponentManager::createInstance();

    m_resourceManager = new ResourceManager();
    m_taskManager = new TaskManager();
    m_fontManager = new FontManager();
    m_inputManager = new InputManager();
}

Engine::~Engine()
{
    log(OE_CHANNEL_CORE, "Destroying engine");

    // Wait until the resource manager finished loading.
    m_resourceManager->flushLoading();

    m_composer->destroyResources();

    unloadPackage("engine");

    m_resourceManager->logResources();

    if(m_isSDLInitialized)
        SDL_Quit();

    PropComponentManager::destroyInstance();
    MeshComponentManager::destroyInstance();
    KinematicComponentManager::destroyInstance();

    delete m_inputManager;
    delete m_fontManager;
    delete m_taskManager;
    delete m_resourceManager;
    delete m_composer;
}

bool Engine::init(void* context)
{
    bool result = appLoadJsonFile("configs" PATH_SEPARATOR "engine.json", m_config);
    OE_CHECK(result, OE_CHANNEL_ENGINE);

    uint32 screenWidth = m_config["screen_width"].asUInt();
    uint32 screenHeight = m_config["screen_height"].asUInt();

#if OE_PLATFORM == OE_PLATFORM_RASPBERRY_PI
    const bool isFullScreen = true;
#else
    const bool isFullScreen = m_config["full_screen"].asBool();
#endif

    uint nCPUStatSamples = m_config["cpu_stat_samples"].asUInt();
    m_activeFrameTimeStat.init(nCPUStatSamples, true);
    m_totalFrameTimeStat.init(nCPUStatSamples, true);
    m_renderThreadFrameTimeStat.init(nCPUStatSamples, true);
    m_commandThreadFrameTimeStat.init(nCPUStatSamples, true);

    m_targetFPS = (float)m_config["target_fps"].asUInt();
    m_bVSync = m_config["v_sync"].asBool();

    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        logError(OE_CHANNEL_CORE, "SDL could not initialize. SDL Error: %s", SDL_GetError());
        return false;
    }
    m_isSDLInitialized = true;

    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);

    m_composer->SDL_Attributes();
    uint32 windowFlags = m_composer->SDL_GetWindowFlags();

    if(isFullScreen)
    {
        windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
        screenWidth = displayMode.w;
        screenHeight = displayMode.h;
    }

    windowFlags |= SDL_WINDOW_RESIZABLE;

    ::OE_Graphic::globalInit();

    // Add engine resource types to the resource factory.
    REGISTER_RESOURCE(FontDataResource);
    REGISTER_RESOURCE(FontResource);
    REGISTER_RESOURCE(PackageResource);
    REGISTER_RESOURCE(WorldResource);

    log(OE_CHANNEL_CORE, "Screen Size %dx%d", screenWidth, screenHeight);

    String appName;
    appGetName(appName);

    m_window = SDL_CreateWindow(appName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, windowFlags);
    if(!m_window)
    {
        logError(OE_CHANNEL_CORE, "SDL window could not be created. SDL Error: %s", SDL_GetError());
        return false;
    }

    RendererConfig renderConfig;
    renderConfig.m_screenSize.m_width = screenWidth;
    renderConfig.m_screenSize.m_height = screenHeight;
    renderConfig.m_bVSync = m_bVSync;
    renderConfig.m_window = m_window;

    if(!m_composer->init(renderConfig))
    {
        logError(OE_CHANNEL_CORE, "Composer could not be initialized");
        return false;
    }

    m_isRendererInitialized = true;

    loadPackage("engine");

    m_state = State::LoadingEnginePackage;

    // Give valid value to the first frame.
    m_frameTimeStopWatch.start();
    m_frameTimeStopWatch.stop();

    MeshComponentManager::getInstance().addPrerequisite(
        KinematicComponentManager::getInstancePointer()
    );

    return true;
}

bool Engine::update()
{
    switch(m_state)
    {
    case State::LoadingEnginePackage:
        if(isPackageLoaded("engine"))
        {
            m_state = State::CreateResources;
        }
        else
        {
            break;
        }

    case State::CreateResources:
        m_composer->createResources();
        m_state = State::WaitForResources;
        
    case State::WaitForResources:
        if(m_composer->areResourcesLoaded())
        {
            m_composer->ready();
            m_state = State::Loaded;
        }
        else
        {
            break;
        }
    }

    m_frameTimeStopWatch.stop();
    m_frameTime = (float)m_frameTimeStopWatch.getElapsedTime();
    m_frameTimeStopWatch.start();

    // Notify managers a new frame is starting.
    for(auto manager : m_managers)
    {
        manager->startFrame();
    }

    // Update managers before events.
    m_inputManager->update();

    const bool shouldContinue = handleEvents();

    // Update managers after events.
    m_resourceManager->update();

    // Update managers.
    bool bAllStarted = false;
    do
    {
        bAllStarted = true;
        for(size_t i = 0; i < m_managers.size(); ++i)
        {
            Manager* manager = m_managers[i];

            if(!manager->wasStarted() && !manager->update(m_frameTime))
            {
                bAllStarted = false;
            }
        }

        if(!bAllStarted)
        {
            std::this_thread::yield();
        }
    }
    while(!bAllStarted);

    m_composer->update(m_frameTime);

    if(isLoaded())
        renderDebugInfo();

    // Wait for all managers to finish.
    for(auto manager : m_managers)
    {
        while(!manager->finished())
        {
            std::this_thread::yield();
        }
    }

    // At this point, all rendering requests have been issued.
    m_composer->syncEndFrame();

    // The render scene have been flipped, we can replay the commands.
    m_composer->replay();

    // No more CPU work after this comment.
    m_activeFrameTimeStat.add(m_frameTimeStopWatch.getElapsedTime());

    m_renderThreadFrameTimeStat.add(m_composer->getRenderThreadFrameTime());
    m_commandThreadFrameTimeStat.add(m_composer->getCommandThreadFrameTime());

    double activeFrameTime = m_frameTimeStopWatch.getElapsedTime();

    if(!m_bVSync && m_targetFPS > 0.0f)
    {
        double sleepTime = ((1.0 / m_targetFPS) - activeFrameTime);
        if(sleepTime > 0)
        {
            std::chrono::milliseconds sleepDuration(int(sleepTime * 1000.0));
            std::this_thread::sleep_for(sleepDuration);
        }
    }

    m_totalFrameTimeStat.add(m_frameTimeStopWatch.getElapsedTime());

    return shouldContinue;
}

void Engine::loadPackage(const String& name)
{
    m_resourceManager->loadPackage(PackageResource::getTypeHash(), name);
}

void Engine::unloadPackage(const String& name)
{
    m_resourceManager->unloadPackage(name);
}

bool Engine::isPackageLoaded(const String& name) const
{
    return m_resourceManager->isPackageLoaded(name);
}

bool Engine::isLoaded() const
{
    return m_state == State::Loaded;
}

bool Engine::handleEvents()
{
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
        case SDL_KEYDOWN:
            if(event.key.keysym.sym == SDLK_F11)
            {
                const SDL_bool bBorderless = (SDL_bool)((SDL_GetWindowFlags(m_window) & SDL_WINDOW_BORDERLESS) == 0);
                if(bBorderless)
                {
                    SDL_SetWindowResizable(m_window, SDL_FALSE);
                    SDL_SetWindowBordered(m_window, SDL_FALSE);
                    SDL_SetWindowPosition(m_window, 0, 0);

                    SDL_DisplayMode DM;
                    SDL_GetCurrentDisplayMode(0, &DM);
                    auto Width = DM.w;
                    auto Height = DM.h;
                    SDL_SetWindowSize(m_window, Width, Height);
                    //m_renderer->setScreenSize(Width, Height);
                }
                else
                {
                    SDL_SetWindowResizable(m_window, SDL_TRUE);
                    SDL_SetWindowBordered(m_window, SDL_TRUE);
                    SDL_SetWindowPosition(m_window, 100, 100);

                    SDL_DisplayMode DM;
                    SDL_GetCurrentDisplayMode(0, &DM);
                    auto Width = DM.w;
                    auto Height = DM.h;
                    SDL_SetWindowSize(m_window, 1280, 1920);
                    //m_renderer->setScreenSize(1280, 1080);
                }
                //m_renderer->resize();
                break;
            }
        case SDL_KEYUP:
        case SDL_MOUSEMOTION:
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
        case SDL_MOUSEWHEEL:
            InputManager::getInstance().handleEvent(event);
            break;

        case SDL_WINDOWEVENT:
            if(event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
            {
                ScreenSize screenSize;
                screenSize.m_width = (uint32)event.window.data1;
                screenSize.m_height = (uint32)event.window.data2;
                m_composer->resize(screenSize);
            }
            else if(event.window.event == SDL_WINDOWEVENT_MINIMIZED)
            {
                m_composer->pause();
            }
            else if(event.window.event == SDL_WINDOWEVENT_RESTORED)
            {
                m_composer->resume();
            }
            break;

        case SDL_QUIT:
            return false;
        }
    }

    return !InputManager::getInstance().hasJustBeenPressed(SDLK_ESCAPE);
}

void Engine::renderDebugInfo()
{
    Rect2D barRect;

    // Main Thread bar value.
    Color barColor(0x00, 0xFF, 0x00, 0x44);

    ScreenSize screenSize = m_composer->getScreenSize();

    uint32 width = (uint32)(screenSize.m_width * 0.8f);
    barRect.m_x = (screenSize.m_width - width) / 2;
    barRect.m_y = screenSize.m_height - 60;
    barRect.m_width = width;
    barRect.m_height = 6;

    m_composer->renderQuadColored2d(
        barRect,
        barColor);

    double activeFrameTime = m_activeFrameTimeStat.getAvg();
    barColor = Color(0x00, 0xFF, 0x00);
    barRect.m_width = (uint32)(activeFrameTime * width / (1.0 / m_targetFPS));
    m_composer->renderQuadColored2d(
        barRect,
        barColor);

    char frameTimeAsChar[512];
    OE_SPRINTF(frameTimeAsChar, 512, "%.2f ms", activeFrameTime * 1000);

    m_composer->renderText(
        frameTimeAsChar,
        "vera",
        12,
        screenSize.m_width - 10,
        barRect.m_y,
        HorizontalAnchor::Right,
        VerticalAnchor::Top);

    // Render Thread bar value.
    barColor = Color(0x00, 0xFF, 0xFF, 0x44);

    barRect.m_x = (screenSize.m_width - width) / 2;
    barRect.m_y = screenSize.m_height - 40;
    barRect.m_width = width;
    barRect.m_height = 6;

    m_composer->renderQuadColored2d(
        barRect,
        barColor);


    activeFrameTime = m_renderThreadFrameTimeStat.getAvg();
    barColor = Color(0x00, 0xFF, 0xFF);
    barRect.m_width = (uint32)(activeFrameTime * width / (1.0 / m_targetFPS));
    m_composer->renderQuadColored2d(
        barRect,
        barColor);

    frameTimeAsChar[512];
    OE_SPRINTF(frameTimeAsChar, 512, "%.2f ms", activeFrameTime * 1000);

    m_composer->renderText(
        frameTimeAsChar,
        "vera",
        12,
        screenSize.m_width - 10,
        barRect.m_y,
        HorizontalAnchor::Right,
        VerticalAnchor::Top);

    // Command Thread bar value.
    barColor = Color(0xFF, 0xFF, 0x00, 0x44);

    barRect.m_x = (screenSize.m_width - width) / 2;
    barRect.m_y = screenSize.m_height - 20;
    barRect.m_width = width;
    barRect.m_height = 6;

    m_composer->renderQuadColored2d(
        barRect,
        barColor);

    activeFrameTime = m_commandThreadFrameTimeStat.getAvg();
    barColor = Color(0xFF, 0xFF, 0x00);
    barRect.m_width = (uint32)(activeFrameTime * width / (1.0 / m_targetFPS));
    m_composer->renderQuadColored2d(
        barRect,
        barColor);

    frameTimeAsChar[512];
    OE_SPRINTF(frameTimeAsChar, 512, "%.2f ms", activeFrameTime * 1000);

    m_composer->renderText(
        frameTimeAsChar,
        "vera",
        12,
        screenSize.m_width - 10,
        barRect.m_y,
        HorizontalAnchor::Right,
        VerticalAnchor::Top);

    // FPS
    char fpsAsChar[512];
    OE_SPRINTF(fpsAsChar, 512, "FPS: %.2f", 1.0 / m_totalFrameTimeStat.getAvg());

    m_composer->renderText(
        fpsAsChar,
        "vera",
        12,
        screenSize.m_width - 10,
        30,
        HorizontalAnchor::Right,
        VerticalAnchor::Top);

    Camera& camera = m_composer->getCamera();
    const Vec3& camPos = camera.getPosition();

    // Camera position
    {
        char camPosAsChar[512];
        OE_SPRINTF(camPosAsChar, 512, "Position: x:%.2f y:%.2f z:%.2f",
            camPos[0], camPos[1], camPos[2]);

        const Vec3& camLookAt = camera.getPosition();

        m_composer->renderText(
            camPosAsChar,
            "vera",
            12,
            screenSize.m_width - 10,
            50,
            HorizontalAnchor::Right,
            VerticalAnchor::Top);
    }

    // Camera look at
    {
        const Vec3& camLookAt = camera.getLookAt();

        char camLookAtAsChar[512];
        OE_SPRINTF(camLookAtAsChar, 512, "Look at: x:%.2f y:%.2f z:%.2f",
            camLookAt[0], camLookAt[1], camLookAt[2]);

        m_composer->renderText(
            camLookAtAsChar,
            "vera",
            12,
            screenSize.m_width - 10,
            70,
            HorizontalAnchor::Right,
            VerticalAnchor::Top);
    }
}

void Engine::registerManager(Manager* manager)
{
    OE_CHECK(std::find(
        m_managers.begin(),
        m_managers.end(),
        manager) == m_managers.end(), OE_CHANNEL_ENGINE);

    m_managers.push_back(manager);
}

void Engine::unregisterManager(Manager* manager)
{
    auto itr = std::find(
        m_managers.begin(),
        m_managers.end(),
        manager);

    OE_CHECK(itr != m_managers.end(), OE_CHANNEL_ENGINE);
    m_managers.erase(itr);
    delete (*itr);
}

} // namespace OE_Engine

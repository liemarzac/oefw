#ifndef __OE_ENGINE_SCENE_COMMAND_H__
#define __OE_ENGINE_SCENE_COMMAND_H__

#include "oe_engine.h"

// OE_Core
#include <eigen_helpers.h>

#define OE_CHANNEL_SCENE_COMMANDS "SceneCommand"

namespace OE_Engine
{

    struct OE_ENGINE_EXPORT SceneCommand
    {
        virtual ~SceneCommand(){}
        virtual void execute() = 0;
        virtual size_t getSize() const = 0;
    };

    struct OE_ENGINE_EXPORT AddMeshInstanceSceneCommand : public SceneCommand
    {
        virtual void execute() override;
        virtual size_t getSize() const override
        {
            return sizeof(*this);
        }

        OE_Graphic::MeshInstancePtr m_meshInstance;
    };

    struct OE_ENGINE_EXPORT RemoveMeshInstanceSceneCommand : public SceneCommand
    {
        virtual void execute() override;
        virtual size_t getSize() const override
        {
            return sizeof(*this);
        }

        OE_Graphic::MeshInstancePtr m_meshInstance;
    };

    struct OE_ENGINE_EXPORT SetDominantDirectionalLightSceneCommand : public SceneCommand
    {
        virtual void execute() override;
        virtual size_t getSize() const override
        {
            return sizeof(*this);
        }

        Vec3 m_direction;
        uint32 m_iRenderScene = InvalidIndex;
    };

    struct OE_ENGINE_EXPORT RemoveDominantDirectionalLightSceneCommand : public SceneCommand
    {
        virtual void execute() override;
        virtual size_t getSize() const override
        {
            return sizeof(*this);
        }

        uint32 m_iRenderScene = InvalidIndex;
    };
}

#endif // __OE_ENGINE_SCENE_COMMAND_H__

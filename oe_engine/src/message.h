#ifndef __OE_ENGINE_COMPONENT_MESSAGE_H__
#define __OE_ENGINE_COMPONENT_MESSAGE_H__

#include "oe_engine.h"
#include "rtti.h"

namespace OE_Engine
{

#define OE_CHANNEL_MESSAGE "Message"

class OE_ENGINE_EXPORT Message
{
    RTTI_DECLARATION

public:
    Message(void* sender);
    virtual ~Message(){}
    virtual size_t getSize() const = 0;

    uintptr_t m_sender;

#if _DEBUG
    void setSenderName(const String& name);
    static const uint MaxSenderNameLength = 32;
    char m_senderName[MaxSenderNameLength];
#endif

private:
    Message();
};

}

#endif // __OE_ENGINE_COMPONENT_MESSAGE_H__

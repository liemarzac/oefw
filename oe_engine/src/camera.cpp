#include "camera.h"

#include "engine_private.h"

// OE_Core
#include <eigen_helpers.h>
#include <oe_math.h>

// OE_Engine
#include "input_manager.h"
#include "engine_private.h"

#define OE_CHANNEL_CAMERA "Camera"

namespace OE_Engine
{

Camera::Camera() :
    m_speedModifier(1.0f)
{
    m_position = Vec3(0.0f, 0.0f, 0.0f);
    m_right = Vec3(1.0f, 0.0f, 0.0f);
    m_look = Vec3(0.0f, 1.0f, 0.0f);
    m_up = Vec3(0.0f, 0.0f, 1.0f);

    updateMatrix();
    m_hfov = 50;
}

Camera::~Camera()
{
}

void Camera::setPosition(const Vec3& position)
{
    m_position = position;
    updateMatrix();
}

void Camera::setDirection(const Vec3& direction)
{
    const float SmallFloat = 1.0e-5f;

    m_look = direction.normalized();

    if(abs(m_look[2] - 1.0f) > SmallFloat)
    {
        // The camera look vector and world up vector have an angle big enough to avoid being collinear.
        m_up = Vec3(0.0f, 0.0f, 1.0f);
        m_right = m_look.cross(m_up);
        m_right[2] = 0.0f;
        m_right.normalize();

        m_up = m_right.cross(m_look);
        m_up.normalize();
    }
    else
    {
        // The look vector and world up vector are nearly collinear.
        // We use the current camera right vector to calculate the camera up vector.
        m_up = m_right.cross(m_look);
        m_up.normalize();
    }

    updateMatrix();
}

void Camera::setLookAt(const Vec3& point)
{
    setDirection(point - m_position);
    updateMatrix();
}

bool Camera::update(float deltaT)
{
    bool hasPositionChanged = false;
    bool hasOrientationChanged = false;
    bool hasChanged = false;

    const InputManager& inputManager = InputManager::getConstInstance();

    bool isCamDirButtonDown = inputManager.isInputDown(InputManager::Buttons::RightMouseButton);

    bool isCtrlButtonDown = inputManager.isInputDown(SDLK_LCTRL) || inputManager.isInputDown(SDLK_RCTRL);

    int32 mouseWheelY;
    inputManager.getMouseWheel(nullptr, &mouseWheelY);
    m_speedModifier = 1.0f + ((float)mouseWheelY * 5.0f);

    if(inputManager.isInputDown(SDLK_w))
    {
        Vec3 displacement = m_look * (m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    if(inputManager.isInputDown(SDLK_s))
    {
        Vec3 displacement = m_look * (-m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    if(inputManager.isInputDown(SDLK_a))
    {
        Vec3 displacement = m_right * (-m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    if(inputManager.isInputDown(SDLK_d))
    {
        Vec3 displacement = m_right * (m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    if(inputManager.isInputDown(SDLK_PAGEUP))
    {
        Vec3 displacement = m_up * (m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    if(inputManager.isInputDown(SDLK_PAGEDOWN))
    {
        Vec3 displacement = m_up * (-m_speedModifier * deltaT);
        m_position += displacement;
        hasPositionChanged = true;
    }

    int32 mouseMoveX = 0;
    int32 mouseMoveY = 0;
    inputManager.getMouseMove(mouseMoveX, mouseMoveY);

    if(isCamDirButtonDown && (mouseMoveX != 0 || mouseMoveY != 0))
    {
        float yawValue = -mouseMoveX * 0.001f;
        yaw(yawValue);

        float pitchValue = -mouseMoveY * 0.001f;
        pitch(pitchValue);

        hasOrientationChanged = true;
    }

    hasChanged = hasPositionChanged || hasOrientationChanged;
    
    if(hasChanged)
    {
        updateMatrix();
    }

    return hasChanged;
}

void Camera::updateMatrix()
{
    const Vec3& xAxis = m_right;
    const Vec3& yAxis = m_look;
    const Vec3& zAxis = m_up;

    m_worldToViewMatrix.row(0) = Vec4(xAxis(0), xAxis(1), xAxis(2), -xAxis.dot(m_position));
    m_worldToViewMatrix.row(1) = Vec4(yAxis(0), yAxis(1), yAxis(2), -yAxis.dot(m_position));
    m_worldToViewMatrix.row(2) = Vec4(zAxis(0), zAxis(1), zAxis(2), -zAxis.dot(m_position));
    m_worldToViewMatrix.row(3) = Vec4(0.0f, 0.0f, 0.0f, 1.0f);

    m_viewToWorldMatrix.row(0) = Vec4(xAxis(0), yAxis(0), zAxis(0), m_position(0));
    m_viewToWorldMatrix.row(1) = Vec4(xAxis(1), yAxis(1), zAxis(1), m_position(1));
    m_viewToWorldMatrix.row(2) = Vec4(xAxis(2), yAxis(2), zAxis(2), m_position(2));
    m_viewToWorldMatrix.row(3) = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

void Camera::pitch(float radAngle)
{
    const float SmallFloat = 1.0e-5f;

    AngleAxis r(radAngle, m_right);

    m_up = r * m_up;
    m_up.normalize();
    m_look = m_up.cross(m_right);
    m_look.normalize();

    OE_CHECK(abs(m_right.dot(m_look)) < SmallFloat, OE_CHANNEL_CAMERA);
    OE_CHECK(abs(m_look.dot(m_up)) < SmallFloat, OE_CHANNEL_CAMERA);
}


void Camera::yaw(float radAngle)
{
    const float SmallFloat = 1.0e-7f;

    // Rotate the right and look vector.
    const Vec3 WorldUp(0.0f, 0.0f, 1.0f);
    AngleAxis r(radAngle, WorldUp);
    m_right = r * m_right;
    // Make sure right vector always remain on the world X, Y plane.
    m_right[2] = .0f;

    m_look = r * m_look;
    m_look.normalized();
    
    m_up = m_right.cross(m_look).normalized();

    // Make sure sure right vector remains orthogonal.
    m_right = m_look.cross(m_up).normalized();

    OE_CHECK(abs(m_right.dot(m_look)) < 1.0e-5f, OE_CHANNEL_CAMERA);
    OE_CHECK(abs(m_look.dot(m_up)) < 1.0e-5f, OE_CHANNEL_CAMERA);
}


void Camera::roll(float radAngle)
{
}

} // namespace OE_Engine
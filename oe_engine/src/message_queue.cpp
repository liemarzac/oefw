#include "message_queue.h"

#include "message.h"

#define OE_CHANNEL_MESSAGE_QUEUE "MessageQueue"

namespace OE_Engine
{

MessageQueue::MessageQueue()
{
    m_currentMessage = nullptr;
}

Message* MessageQueue::beginMessage()
{
    OE_CHECK(!m_currentMessage, OE_CHANNEL_MESSAGE_QUEUE);

    void* ptr;
    size_t availableSize;
    if(m_ringBuffer.beginRead(ptr, availableSize))
    {
        m_currentMessage = reinterpret_cast<Message*>(ptr);
    }
    else
    {
        m_currentMessage = nullptr;
    }

    return m_currentMessage;
}

void MessageQueue::endMessage()
{
    OE_CHECK(m_currentMessage, OE_CHANNEL_MESSAGE_QUEUE);
    const size_t messageSize = m_currentMessage->getSize();
    m_currentMessage->~Message();
    m_currentMessage = nullptr;
    m_ringBuffer.endRead(messageSize);
}

}

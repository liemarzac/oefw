#ifndef __OE_ENGINE_INPUT_MANAGER_H__
#define __OE_ENGINE_INPUT_MANAGER_H__

#include "oe_engine.h"

// External
#include <SDL_keycode.h>

// OE_Core
#include <singleton.h>
#include <vector.h>

#define OE_CHANNEL_INPUT "Input"

union SDL_Event;

namespace OE_Engine
{

class OE_ENGINE_EXPORT InputManager : public OE_Core::Singleton<InputManager>
{
public:

    struct Buttons
    {
        enum Enum
        {
            LeftMouseButton = -1,
            RightMouseButton = -2,
            MiddleMouseButton = -3,
        };
    };

    InputManager();
    ~InputManager();

    void handleEvent(SDL_Event& event);
    void update();
    void showMouse(bool show);
    void getMouseMove(int32& mouseX, int32& mouseY) const;
    void getMousePos(int32& mouseX, int32& mouseY) const;
    void getMouseWheel(int32* mouseWheelX, int32* mouseWheelY) const;

    bool isInputDown(const int32 keycode) const;
    bool hasJustBeenPressed(const int32 keycode) const;

private:
    InputManager& operator=(const InputManager& rhs);
    InputManager(InputManager& rhs);

    void addInputDown(const int32 keycode);
    void removeInputDown(const int32 keycode);

    int32 m_mouseX;
    int32 m_mouseY;
    int32 m_lastMouseX;
    int32 m_lastMouseY;
    int32 m_mouseWheelX;
    int32 m_mouseWheelY;

    Vector<int32> m_inputDownList;
    Vector<int32> m_inputPendingPressedList;
    Vector<int32> m_inputJustPressedList;
};

} // namespace OE_Engine

#endif // __OE_ENGINE_INPUT_MANAGER_H__

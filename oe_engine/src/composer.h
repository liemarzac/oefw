#ifndef __OE_ENGINE_RENDERER_H__
#define __OE_ENGINE_RENDERER_H__

#include "oe_engine.h"

// External
#include <condition_variable>
#include <thread>

// OE_Core
#include <flagset.h>
#include <singleton.h>

// OE_Graphic
#include <color.h>
#include <graphic_resource_helpers.h>
#include <instance_binding.h>
#include <render_scene.h>

// OE_Engine
#include "camera.h"
#include "mesh_instance.h"
#include "rect_2d.h"
#include "render_thread.h"

namespace OE_Core
{
    class RingBuffer;
}

namespace OE_Graphic
{
    class Renderer;
    class TextureResource;
}

namespace OE_Engine
{

class MeshComponent;
class Prop;
class WorldScene;


class OE_ENGINE_EXPORT Composer : public OE_Core::Singleton<Composer>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    enum class Flags
    {
        PendingResize,
        Pause,
        Paused,
        Resume,
    };

    static const char* RenderSceneNames[];

    Composer();
    ~Composer();

    bool init(const OE_Graphic::RendererConfig& config);

    void update(float deltaTime);

    void SDL_Attributes();
    uint32 SDL_GetWindowFlags() const;

    bool isReady() const;
    bool isOk() const;

    void resize(const OE_Graphic::ScreenSize& newSize);
    void pause();
    void resume();
    bool isPaused() const;

    inline Camera& getCamera();
    inline WorldScene& getWorldScene();

    inline const Mat4& getOrthoProjectionMatrix() const;
    const Mat4& getWorldProjectionMatrix() const;
    inline MeshComponent* getCompositionMeshComponent() const;
    const Mat4& getViewMatrix() const;
    uint32 getMainThreadSceneIndex() const;

    void addMeshInstance(OE_Graphic::MeshInstancePtr meshInstance);
    void removeMeshInstance(OE_Graphic::MeshInstancePtr meshInstance);

    void getTextDimensions(
        const String& text,
        const String& fontName,
        uint32 fontSize,
        uint32& width,
        uint32& height,
        uint32& bearingY);

    void renderText(
        const String& text,
        const String& fontName,
        uint32 fontSize,
        uint32 x,
        uint32 y,
        OE_Graphic::HorizontalAnchor hAnchor,
        OE_Graphic::VerticalAnchor vAnchor);
    void renderQuadColored2d(
        const OE_Core::Rect2D& quad,
        const OE_Graphic::Color& color);
    void renderLinedColored2d(
        const Vec2i& p1,
        const Vec2i& p2,
        const OE_Graphic::Color& color,
        uint32 thickness = 1);
    void renderQuadTextured2d(
        const OE_Core::Rect2D& quad,
        OE_Graphic::ConstTextureResPtr textureResource);

    void syncEndFrame();
    void replay();

    void addProp(Prop* prop);

    inline OE_Graphic::ScreenSize getScreenSize() const;

    inline bool shouldRender() const;
    OE_Graphic::RenderScene& getMainThreadScene();
    void clearMainThreadScene();
    inline double getRenderThreadFrameTime() const;
    inline double getCommandThreadFrameTime() const;
    void commitUICommand();

    void projectOnScreen(uint32 iRenderScene, const Vec3& worldPoint, Vec2& screenPoint);

    template<typename T>
    inline T* allocUICommand(size_t extraSize = 0);

    void createResources();
    void destroyResources();
    bool areResourcesLoaded() const;
    void ready();

private:
    void updateProjectionMatrices();
    void updateWorldMatrices();
    void updateUIMatrices();
    void flushRenderering();

    OE_Graphic::Renderer* m_renderer = nullptr;
    WorldScene* m_worldScene = nullptr;
    std::thread m_thread;
    RenderThreadParams m_renderThreadParams;
    OE_Graphic::ScreenSize m_screenSize;
    float m_vFOV;
    float m_nearPlane;
    float m_farPlane;
    Camera* m_camera;
    Mat4 m_projectionMatrix;
    Mat4 m_transform2dMatrix;

    MeshComponent* m_compositionMeshComponent = nullptr;

    OE_Core::FlagSet<Flags> m_flags;
};

OE_Graphic::ScreenSize Composer::getScreenSize() const
{
    return m_screenSize;
}

bool Composer::shouldRender() const
{
    return m_renderThreadParams.m_shouldRender;
}

Camera& Composer::getCamera()
{
    return *m_camera;
}

WorldScene& Composer::getWorldScene()
{
    return *m_worldScene;
}

inline double Composer::getRenderThreadFrameTime() const
{
    return m_renderThreadParams.m_renderThreadFrameTime;
}

inline double Composer::getCommandThreadFrameTime() const
{
    return m_renderThreadParams.m_commandThreadFrameTime;
}

inline MeshComponent* Composer::getCompositionMeshComponent() const
{
    return m_compositionMeshComponent;
}

template<typename T>
T* Composer::allocUICommand(size_t extraSize)
{
    //OE_Graphic::RenderScene& UIScene = getMainThreadScene(OE_Graphic::DefaultRenderSceneType::UI);
    //return UIScene.allocCommand<T>(extraSize);
    return nullptr;
}


} // namespace OE_Engine

#endif // __OE_ENGINE_RENDERER_H__

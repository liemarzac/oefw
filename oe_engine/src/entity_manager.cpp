#include "entity_manager.h"

#include "engine.h"
#include "entity.h"

namespace OE_Engine
{

EntityManager::EntityManager():
    m_root(nullptr)
{
}

void EntityManager::addEntity(Entity* entity)
{
    if(entity->m_prev != nullptr && entity->m_next != nullptr)
    {
        // Entity is already in the list.
        return;
    }

    if(m_root)
    {
        OE_CHECK(m_root->m_prev, OE_CHANNEL_ENGINE);
        Entity* prev = m_root->m_prev;
        entity->m_prev = prev;
        prev->m_next = entity;
        entity->m_next = m_root;
        m_root->m_prev = entity;
    }
    else
    {
        m_root = entity;
        entity->m_prev = entity;
        entity->m_next = entity;
    }
}

void EntityManager::removeEntity(Entity* entity)
{
    OE_CHECK(entity->m_prev, OE_CHANNEL_ENGINE);
    OE_CHECK(entity->m_next, OE_CHANNEL_ENGINE);

    bool fixupLinks = true;

    if(entity == m_root)
    {
        if(entity->m_next == m_root)
        {
            m_root = nullptr;
            fixupLinks = false;
        }
        else
        {
            m_root = entity->m_next;
        }
    }

    if(fixupLinks)
    {
        entity->m_prev->m_next = entity->m_next;
        entity->m_next->m_prev = entity->m_prev;
    }

    delete entity;
}

EntityManager::~EntityManager()
{
    while(m_root)
    {
        removeEntity(m_root);
    }
}

} // namespace OE_Engine

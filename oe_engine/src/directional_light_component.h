#ifndef __OE_ENGINE_DIRECTIONAL_LIGHT_COMPONENT_H__
#define __OE_ENGINE_DIRECTIONAL_LIGHT_COMPONENT_H__

#include "oe_engine.h"
#include "component.h"
#include "component_manager.h"


namespace OE_Engine
{

class OE_ENGINE_EXPORT DirectionalLightComponent : public Component
{
    RTTI_DECLARATION

public:
    DirectionalLightComponent();
    ~DirectionalLightComponent();

    inline const Vec3& getDirection() const;
    inline void setDirection(const Vec3& direction);

private:
    Vec3 m_direction;
};

const Vec3& DirectionalLightComponent::getDirection() const
{
    return m_direction;
}

void DirectionalLightComponent::setDirection(const Vec3& direction)
{
    m_direction = direction;
}


} // namespace OE_Engine

#endif // __OE_ENGINE_DIRECTIONAL_LIGHT_COMPONENT_H__

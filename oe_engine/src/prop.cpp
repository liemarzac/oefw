#include "prop.h"

// OE_Engine
#include "engine_private.h"
#include "prop_component_manager.h"

#define OE_CHANNEL_PROP "Prop"

namespace OE_Engine
{

Prop::Prop():
    m_propComponent(nullptr)
{
    m_position = Vec3(0.0f, 0.0f, 0.0f);
    m_orientation.setIdentity();
}

Prop::Prop(const String& name):
    Entity(name)
    ,m_propComponent(nullptr)
{
    m_position = Vec3(0.0f, 0.0f, 0.0f);
    m_orientation.setIdentity();
}

Prop::~Prop()
{
    destroyPropComponent();
}

PropComponent* Prop::createPropComponent(PropResPtr propResource)
{
    OE_CHECK(propResource, OE_CHANNEL_PROP);
    m_propComponent = PropComponentManager::getInstance().createComponent();
    m_propComponent->setName(getName());
    m_propComponent->setPropResource(propResource);
    m_propComponent->setPosition(m_position);
    m_propComponent->setOrientation(m_orientation);
    m_onLoadedCallback = m_propComponent->addLoadedCallback(
        MakeFunctor<Prop>(&Prop::onLoadedCallback, this, CallbackType::Permanent));
    attachComponent(m_propComponent);

    return m_propComponent;
}

void Prop::destroyPropComponent()
{
    if(!m_propComponent)
        return;

    detachComponent(m_propComponent);
    m_propComponent->removeLoadedCallback(m_onLoadedCallback);
    PropComponentManager::getInstance().destroyComponent(m_propComponent);
}

void Prop::getOrientation(Mat4& orientation) const
{
    orientation = m_orientation;
}

void Prop::setOrientation(const Mat4& orientation)
{
    m_orientation = orientation;
    if(m_propComponent)
    {
        m_propComponent->setOrientation(orientation);
    }
}

void Prop::getPosition(Vec3& position) const
{
    position = m_position;
}

void Prop::setPosition(const Vec3& position)
{
    m_position = position;
    if(m_propComponent)
    {
        m_propComponent->setPosition(position);
    }
}

} // OE_Engine


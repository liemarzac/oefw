#include "message.h"

// External
#include <cstring>


namespace OE_Engine
{

RTTI_IMPL(Message)

Message::Message(void* sender):
    m_sender((uintptr_t)sender)
{
#if _DEBUG
    m_senderName[0] = '\0';
#endif
}

#if _DEBUG
void Message::setSenderName(const String& name)
{
    if(name.size() == 0)
        return;

    size_t len = strlen(name.c_str());
    if(len + 1 < Message::MaxSenderNameLength)
    {
        memcpy(m_senderName, name.c_str(), len + 1);
    }
    else
    {
        memcpy(m_senderName, name.c_str(), MaxSenderNameLength - 1);
        m_senderName[MaxSenderNameLength - 1] = '\0';
    }

}
#endif

}

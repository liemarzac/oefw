#include "font.h"

// OE_Core
#include <buffer.h>

// OE_Engine
#include "engine_private.h"

namespace OE_Engine
{

void serialize_impl(Buffer& buffer, const GlyphData& glyphData, const GlyphData*)
{
    serialize(buffer, glyphData.m_rect);
    serialize(buffer, glyphData.m_left);
    serialize(buffer, glyphData.m_top);
    serialize(buffer, glyphData.m_advanceX);
    serialize(buffer, glyphData.m_advanceY);
    serialize(buffer, glyphData.m_kerningData);
}

void deserialize_impl(Buffer& buffer, GlyphData& glyphData, const GlyphData*)
{
    deserialize(buffer, glyphData.m_rect);
    deserialize(buffer, glyphData.m_left);
    deserialize(buffer, glyphData.m_top);
    deserialize(buffer, glyphData.m_advanceX);
    deserialize(buffer, glyphData.m_advanceY);
    deserialize(buffer, glyphData.m_kerningData);
}

size_t calculateSerializedSize_impl(const GlyphData& glyphData, const GlyphData*)
{
    return calculateSerializedSize(glyphData.m_rect) +
        calculateSerializedSize(glyphData.m_left) +
        calculateSerializedSize(glyphData.m_top) +
        calculateSerializedSize(glyphData.m_advanceX) +
        calculateSerializedSize(glyphData.m_advanceY) +
        calculateSerializedSize(glyphData.m_kerningData);
}

void serialize_impl(Buffer& buffer, const FontData& fontData, const FontData*)
{
    serialize(buffer, fontData.m_fontName);
    serialize(buffer, fontData.m_fontSizes);
}

void deserialize_impl(Buffer& buffer, FontData& fontData, const FontData*)
{
    deserialize(buffer, fontData.m_fontName);
    deserialize(buffer, fontData.m_fontSizes);
}

size_t calculateSerializedSize_impl(const FontData& fontData, const FontData*)
{
    return OE_Core::calculateSerializedSize_impl(fontData.m_fontName, static_cast<String*>(0)) +
        OE_Core::calculateSerializedSize_impl(fontData.m_fontSizes, static_cast<FontSizesData*>(0));
}

void serialize_impl(Buffer& buffer, const FontTexture& fontTexture, const FontTexture*)
{
    serialize(buffer, fontTexture.m_textureWidth);
    serialize(buffer, fontTexture.m_textureHeight);
    serialize(buffer, fontTexture.m_textureBuffer);
}

void deserialize_impl(Buffer& buffer, FontTexture& fontTexture, const FontTexture*)
{
    deserialize(buffer, fontTexture.m_textureWidth);
    deserialize(buffer, fontTexture.m_textureHeight);
    deserialize(buffer, fontTexture.m_textureBuffer);
}

size_t calculateSerializedSize_impl(const FontTexture& fontTexture, const FontTexture*)
{
    return calculateSerializedSize(fontTexture.m_textureWidth) +
        calculateSerializedSize(fontTexture.m_textureHeight) +
        calculateSerializedSize(fontTexture.m_textureBuffer);
}

} // namespace OE_Engine

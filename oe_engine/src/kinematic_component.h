#ifndef __OE_ENGINE_KINEMATIC_COMPONENT_H__
#define __OE_ENGINE_KINEMATIC_COMPONENT_H__

// OE_Core
#include <eigen_helpers.h>
#include <flagset.h>

// OE_Engine
#include "oe_engine.h"
#include "component.h"

namespace OE_Engine
{

DEFINE_COMPONENT_UPDATE_PARAMS(KinematicComponent)

class OE_ENGINE_EXPORT KinematicComponent : public Component
{
    RTTI_DECLARATION

public:
    EIGEN_ALIGNED

    KinematicComponent();
    virtual ~KinematicComponent();

    KinematicComponent(const KinematicComponent& rhs) = delete;
    KinematicComponent& operator=(const KinematicComponent& rhs) = delete;

    void update(const KinematicComponentUpdateParams& params);
    bool isInitialized() const;

private:
    Vec3d m_acceleration;
    Vec3d m_velocity;
    Vec3d m_position;

    enum class InitFlags
    {
        PositionSet,
        NumberOf,
    };

    OE_Core::FlagSet<InitFlags> m_initFlags;
};

} // OE_Engine

#endif // __OE_ENGINE_KINEMATIC_COMPONENT_H__

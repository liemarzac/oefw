#ifndef __OE_ENGINE_FONT_DATA_RESOURCE_H__
#define __OE_ENGINE_FONT_DATA_RESOURCE_H__

#include "oe_engine.h"

// OE_Core
#include <resource.h>
#include <runnable.h>

// OE_Engine
#include "font.h"

namespace OE_Engine
{

class OE_ENGINE_EXPORT FontDataResource : public OE_Core::Resource
{
friend class FontDataResourceLoadTask;
    RTTI_DECLARATION
public:
    FontDataResource(const String& name);
    virtual ~FontDataResource();

    virtual void loadImpl() override;
    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "font_data_resource";
    }

    const GlyphsData* getGlyphsData(const String& fontName, uint32 fontSize);

private:
    FontDataResource();
    FontDataResource& operator=(const FontDataResource& rhs);
    FontDataResource(FontDataResource& rhs);
    FontsData m_data;
};

class OE_ENGINE_EXPORT FontDataResourceLoadTask : public OE_Core::Runnable
{
public:
    FontDataResourceLoadTask(FontDataResource* resource);
    virtual void run() override;

private:
    FontDataResourceLoadTask();
    FontDataResourceLoadTask& operator=(const FontDataResourceLoadTask& rhs);
    FontDataResourceLoadTask(FontDataResourceLoadTask& rhs);

    FontDataResource* m_resource;
};

} // namespace OE_Engine

#endif // __OE_ENGINE_FONT_DATA_RESOURCE_H__

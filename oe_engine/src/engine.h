#ifndef __OE_ENGINE_ENGINE_H__
#define __OE_ENGINE_ENGINE_H__

#include "oe_engine.h"

// External
#include <json/json.h>

// OE_Core
#include <singleton.h>
#include <stat.h>
#include <stop_watch.h>
#include <vector.h>

struct SDL_Window;

namespace OE_Core
{
    class ResourceManager;
    class TaskManager;
}

namespace OE_Engine
{

class Manager;
class FontManager;
class InputManager;
class Composer;

class OE_ENGINE_EXPORT Engine : public OE_Core::Singleton<Engine>
{
public:

    enum class State
    {
        Unloaded,
        LoadingEnginePackage,
        CreateResources,
        WaitForResources,
        Loaded,
    };

    Engine();
    ~Engine();
    bool init(void* context = NULL);
    bool update();
    const Json::Value& getConfig() const;
    void registerManager(Manager* manager);
    void unregisterManager(Manager* manager);
    Composer& getComposer();
    bool isLoaded() const;
    inline float getFrameTime() const
    {
        return m_frameTime;
    }

    void loadPackage(const String& name);
    void unloadPackage(const String& name);
    bool isPackageLoaded(const String& name) const;

private:
    void renderDebugInfo();
    bool handleEvents();

    Composer* m_composer;
    SDL_Window* m_window;
    bool m_isRendererInitialized;
    bool m_isSDLInitialized;

    OE_Core::ResourceManager* m_resourceManager;
    OE_Core::TaskManager* m_taskManager;
    FontManager* m_fontManager;
    InputManager* m_inputManager;
    Vector<Manager*> m_managers;
    Json::Value m_config;

    OE_Core::StopWatch m_frameTimeStopWatch;
    OE_Core::Stat<double> m_activeFrameTimeStat;
    OE_Core::Stat<double> m_totalFrameTimeStat;
    OE_Core::Stat<double> m_renderThreadFrameTimeStat;
    OE_Core::Stat<double> m_commandThreadFrameTimeStat;
    float m_targetFPS;
    float m_frameTime = 0.0f;
    bool m_bVSync;
    State m_state = State::Unloaded;
};

inline const Json::Value& Engine::getConfig() const
{
    return m_config;
}

inline Composer& Engine::getComposer()
{
    return *m_composer;
}

} // namespace OE_Engine

#endif // __OE_ENGINE_ENGINE_H__


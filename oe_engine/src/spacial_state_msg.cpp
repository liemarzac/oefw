#include "spacial_state_msg.h"

namespace OE_Engine
{

RTTI_IMPL_PARENT(SpacialStateMsg, Message)

SpacialStateMsg::SpacialStateMsg(void* sender):
    Message(sender)
{
    m_position = Vec3(0.0f, 0.0f, 0.0f);
    m_orientation = Quat(0.0f, 0.0f, 0.0f, 1.0f);
}



} // OE_Engine


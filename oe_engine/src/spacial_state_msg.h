#ifndef __OE_ENGINE_SPACIAL_STATE_MSG_H__
#define __OE_ENGINE_SPACIAL_STATE_MSG_H__

#include "oe_engine.h"

// OE_Core
#include <eigen_helpers.h>
#include <flagset.h>

// OE_Engine
#include "message.h"

namespace OE_Engine
{

class OE_ENGINE_EXPORT SpacialStateMsg : public Message
{
    RTTI_DECLARATION
    GET_SIZE_IMPL

public:
    EIGEN_ALIGNED

    SpacialStateMsg(void* sender);

    void setPosition(const Vec3& position);
    void setOrientation(const Quat& position);

    enum class Flags
    {
        Position,
        Orientation
    };

    Vec3 m_position;
    Quat m_orientation;
    OE_Core::FlagSet<Flags> m_set;
};

inline void SpacialStateMsg::setPosition(const Vec3& position)
{
    m_position = position;
    m_set.set(Flags::Position);
}

inline void SpacialStateMsg::setOrientation(const Quat& orientation)
{
    m_orientation = orientation;
    m_set.set(Flags::Orientation);
}

} // OE_Engine

#endif // __OE_ENGINE_SPACIAL_STATE_MSG_H__

#include "ui_render_commands.h"

#include "engine_private.h"

// OE_Core
#include <resource_manager.h>

// OE_Graphic
#include <instance_binding.h>
#include <mesh_instance.h>
#include <render_scene.h>
#include <renderer.h>
#include <vertex_shader_resource.h>
#include <vertex_stream_resource.h>
#include <texture_resource.h>

// OE_Engine
#include "font_resource.h"
#include "composer.h"

#define OE_CHANNEL_UI_COMMANDS "UICommands"

namespace OE_Engine
{

void UIRenderCommand::addMeshResourceToScene(
    MeshResPtr meshResource
    DBG_STR_PARAM_ADD(name))
{
    Composer& composer = Composer::getInstance();
    RenderScene& scene = composer.getMainThreadScene();

    MeshInstance::CreateParams params;
    auto material = meshResource->getMaterialResource();
    params.m_pipelineHandle = material->getPipelineHandle();
    params.m_renderPassId = material->getRenderPassId();

    auto vertexStream = meshResource->getVertexStreamResource();
    params.m_vertexBufferHandle = vertexStream->getVertexBufferHandle();
    params.m_indexBufferHandle = vertexStream->getIndexBufferHandle();
    MeshInstance* meshInstance = new MeshInstance(params);
    DBG_ONLY(meshInstance->setName(name));

    // Not necessary because the modelView matrix is not used by the shader.
    meshInstance->m_instance.m_modelView = Mat4::Identity();
    // Not necessary because normals not used by the shader.
    meshInstance->m_instance.m_transInvModelView = Mat4::Identity();
    // Not necessary because tangents not used by the shader.
    meshInstance->m_instance.m_transInvModel = Mat4::Identity();
    // MVP = VP because M = Identity for 2D rendering
    meshInstance->m_instance.m_modelViewProjection = composer.getOrthoProjectionMatrix();

    scene.addMeshInstance(meshInstance);
}

void Quad2dColoredRenderCommand::execute()
{
    auto& resourceManager = ResourceManager::getInstance();
    auto material = resourceManager.getResource<MaterialResource>("materials/2d_colored");

    if(!material->isLoaded())
        return;

    UniquePtr<Buffer>vertexBuffer(new Buffer(6 * sizeof(Vertex_XYZW_Color)));

    vertexBuffer->write((float)m_quad.m_x); // x
    vertexBuffer->write((float)m_quad.m_y); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    vertexBuffer->write((float)(m_quad.m_x + m_quad.m_width)); // x
    vertexBuffer->write((float)m_quad.m_y); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    vertexBuffer->write((float)m_quad.m_x); // x
    vertexBuffer->write((float)(m_quad.m_y + m_quad.m_height)); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    vertexBuffer->write((float)m_quad.m_x); // x
    vertexBuffer->write((float)(m_quad.m_y + m_quad.m_height)); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    vertexBuffer->write((float)(m_quad.m_x + m_quad.m_width)); // x
    vertexBuffer->write((float)m_quad.m_y); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    vertexBuffer->write((float)(m_quad.m_x + m_quad.m_width)); // x
    vertexBuffer->write((float)(m_quad.m_y + m_quad.m_height)); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_ENGINE);

    // Rewind buffer to be able to read it from the start.
    vertexBuffer->rewind();

    const String name = "quad_colored";

    auto vertexStreamResource = MakeShared<VertexStreamResource>(
        std::move(vertexBuffer),
        VertexDeclaration::XYZW_Color,
        PrimitiveType::TriangleList,
        BufferUsage::Static,
        name);

    auto meshResourceDesc = MakeShared<MeshResource::Descriptor>();
    meshResourceDesc->m_vertexStreamResource = vertexStreamResource;
    meshResourceDesc->m_materialResource = material;

    auto meshResource = MakeShared<MeshResource>(
        name,
        meshResourceDesc);

    addMeshResourceToScene(
        meshResource
        DBG_PARAM_ADD(name));
}

void Line2dColoredRenderCommand::execute()
{
    auto& resourceManager = ResourceManager::getInstance();
    auto material = resourceManager.getResource<MaterialResource>("2d_colored");

    if(!material->isLoaded())
        return;

    UniquePtr<Buffer>vertexBuffer(new Buffer(4 * sizeof(Vertex_XYZW_Color)));

    Vec2 a(m_p1[0], m_p1[1]);
    Vec2 b(m_p2[0], m_p2[1]);

    Vec2 ab(b - a);
    Vec2 normal(-ab[1],ab[0]);
    normal.normalize();
    normal *= (float)m_thickness;

    Vec2 v1 = b + normal;
    Vec2 v2 = a + normal; 
    Vec2 v3 = a - normal;
    Vec2 v4 = b - normal;

    // Vertex A.
    vertexBuffer->write(v1[0]); // x
    vertexBuffer->write(v1[1]); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    // Vertex B.
    vertexBuffer->write(v2[0]); // x
    vertexBuffer->write(v2[1]); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    // Vertex C.
    vertexBuffer->write(v3[0]); // x
    vertexBuffer->write(v3[1]); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    // Vertex D.
    vertexBuffer->write(v4[0]); // x
    vertexBuffer->write(v4[1]); // x
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(m_color.color);

    OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_ENGINE);

    // Rewind buffer to be able to read it from the start.
    vertexBuffer->rewind();

    SharedPtr<const VertexStreamResource> vertexStreamResource(
        new VertexStreamResource(
        std::move(vertexBuffer),
        VertexDeclaration::XYZW_Color,
        PrimitiveType::TriangleFan,
        BufferUsage::Static,
        ""));

    //Renderer& renderer = Renderer::getInstance();
    //renderer.applyMaterial(material);
    //driver.applyMaterialConstants(material, materialConstants);
    //driver.renderVertexStream(vertexStreamResource);
}

void Text2dRenderCommand::execute()
{
    Composer& renderer = Composer::getInstance();

    uint32 width, height, bearingY;
    renderer.getTextDimensions(
        m_text,
        m_fontName,
        m_fontSize,
        width,
        height,
        bearingY);

    auto& resourceManager = ResourceManager::getInstance();
    auto fontResource = resourceManager.getResource<FontResource>(m_fontName);

    if (fontResource.get() == nullptr ||
        fontResource->getState() != Resource::State::Loaded)
    {
        return;
    }

    auto material = fontResource->getMaterialResource();

    TextureResPtr diffuse = material->getTexture(0);
    OE_CHECK(diffuse, OE_CHANNEL_UI_COMMANDS);

    const uint32 textureWidth = diffuse->getWidth();
    const uint32 textureHeight = diffuse->getHeight();

    const GlyphsData* glyphsData = fontResource->getGlyphsData(m_fontName, m_fontSize);
    if (!glyphsData)
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot find font size %d in font %s", m_fontSize, m_fontName.c_str());
        return;
    }

    int32 penX = m_x;
    int32 penY = m_y;

    switch (m_hAnchor)
    {
    case HorizontalAnchor::Left:
        break;

    case HorizontalAnchor::Center:
        penX -= width / 2;
        break;

    case HorizontalAnchor::Right:
        penX -= width;
        break;

    default:
        OE_CHECK(false, OE_CHANNEL_UI_COMMANDS);
        break;
    }

    switch (m_vAnchor)
    {
    case VerticalAnchor::Top:
        penY += bearingY;
        break;

    case VerticalAnchor::Center:
        penY += (height / 2) - (height - bearingY);
        break;

    case VerticalAnchor::Bottom:
        penY -= height - bearingY;
        break;

    default:
        OE_CHECK(false, OE_CHANNEL_UI_COMMANDS);
        break;
    }

    const size_t textLen = m_text.length();

    // Loop through all characters and count the ones which can be displayed.
    uint32 numCharacters = 0;
    for (size_t i = 0; i < textLen; ++i)
    {
        // TODO This is incorrect for non ANSI str.
        uint32 unicode = m_text.c_str()[i];

        // Space character is not displayed.
        if (unicode == ' ')
            continue;

        GlyphsData::const_iterator glyphItr = glyphsData->find(unicode);
        if (glyphItr == glyphsData->end())
        {
            // Glyph not found. Try to find the rectangle glyph.
            glyphItr = glyphsData->find(9712);
            if (glyphItr == glyphsData->end())
                continue;
        }

        ++numCharacters;
    }

    // 6 vertices with 3 floats for position, 2 for texcoord.
    UniqueBufferPtr vertexBuffer(new Buffer(numCharacters * 6 * (3 + 2) * sizeof(float)));

    for (size_t i = 0; i < textLen; ++i)
    {
        // TODO This is incorrect for non ANSI str.
        uint32 unicode = m_text.c_str()[i];

        bool isSpaceCharacter = false;

        // The metric of a space is considered to be the metric of an hyphen.
        if (unicode == ' ')
        {
            unicode = '-';
            isSpaceCharacter = true;
        }

        GlyphsData::const_iterator glyphItr = glyphsData->find(unicode);
        if (glyphItr == glyphsData->end())
        {
            // Glyph not found. Try to find the rectangle glyph.
            glyphItr = glyphsData->find(9712);
            if (glyphItr == glyphsData->end())
                continue;
        }

        const GlyphData& glyphData = (*glyphItr).second;

        if (isSpaceCharacter)
        {
            penX += glyphData.m_advanceX;
            continue;
        }

        Rect2D quadCoord;
        quadCoord.m_x = penX + glyphData.m_left;
        quadCoord.m_y = penY - glyphData.m_top;
        quadCoord.m_width = glyphData.m_rect.m_width;
        quadCoord.m_height = glyphData.m_rect.m_height;

#if OE_RENDERER == OE_RENDERER_DIRECTX_9
        const float offsetX = -0.5f;
        const float offsetY = -0.5f;
#else
        const float offsetX = 0.0f;
        const float offsetY = 0.0f;
#endif

        // Vertex 1.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 2.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 3.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        // Vertex 4.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 5.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        // Vertex 6.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        penX += glyphData.m_advanceX;
    }

    OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_UI_COMMANDS);

    // Rewind buffer to be able to read it from the start.
    vertexBuffer->rewind();

    const String& name = "text_2d";

    auto vertexStream = MakeShared<VertexStreamResource>(
        std::move(vertexBuffer),
        VertexDeclaration::XYZ_UV,
        PrimitiveType::TriangleList,
        BufferUsage::Static,
        name);

    auto meshResourceDesc = MakeShared<MeshResource::Descriptor>();
    meshResourceDesc->m_vertexStreamResource = vertexStream;
    meshResourceDesc->m_materialResource = material;

    MeshResPtr mesh = MakeShared<MeshResource>(
        name,
        meshResourceDesc);

    addMeshResourceToScene(
        mesh
        DBG_PARAM_ADD(name));
}


void Quad2dTexturedCommand::execute()
{
    auto& resourceManager = ResourceManager::getInstance();
    SharedPtr<FontResource> fontResource = resourceManager.getResource<FontResource>("vera");

    if(!fontResource->isLoaded())
        return;

    // 4 vertices with 4 floats for position, 2 floats for texCoord.
    UniquePtr<Buffer>vertexBuffer(new Buffer(4 * (4 + 2) * sizeof(float)));

    // Bottom left vertex.
    vertexBuffer->write((float)m_quad.m_x); // x
    vertexBuffer->write((float)(m_quad.m_y + m_quad.m_height)); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(0.0f); // u
    vertexBuffer->write(1.0f); // v

    // Top left vertex
    vertexBuffer->write((float)m_quad.m_x); // x
    vertexBuffer->write((float)m_quad.m_y); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(0.0f); // u
    vertexBuffer->write(0.0f); // v

    // Top right vertex
    vertexBuffer->write((float)(m_quad.m_x + m_quad.m_width)); // x
    vertexBuffer->write((float)m_quad.m_y); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(1.0f); // u
    vertexBuffer->write(0.0f); // v

    // Bottom right vertrx.
    vertexBuffer->write((float)(m_quad.m_x + m_quad.m_width)); // x
    vertexBuffer->write((float)(m_quad.m_y + m_quad.m_height)); // y
    vertexBuffer->write(0.0f); // z
    vertexBuffer->write(1.0f); // w
    vertexBuffer->write(1.0f); // u
    vertexBuffer->write(1.0f); // v

    OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_UI_COMMANDS);

    // Rewind buffer to be able to read it from the start.
    vertexBuffer->rewind();

    SharedPtr<const VertexStreamResource> vertexStreamResource(
        new VertexStreamResource(
            std::move(vertexBuffer),
            VertexDeclaration::XYZW_UV,
            PrimitiveType::TriangleFan,
            BufferUsage::Static,
            "quad_textured"));

    SharedPtr<MaterialResource> materialResource = fontResource->getMaterialResource();

    //Renderer& renderer = Renderer::getInstance();
    //renderer.applyMaterial(materialResource);
}

}

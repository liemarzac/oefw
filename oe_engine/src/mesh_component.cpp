#include "mesh_component.h"

// OE_Graphic
#include <material_resource.h>
#include <mesh_resource.h>
#include <renderer.h>
#include <vertex_stream_resource.h>

// OE_Engine
#include "engine_private.h"
#include "spacial_state_msg.h"

#define OE_CHANNEL_MESH_COMPONENT "MeshComponent"

namespace OE_Engine
{

RTTI_IMPL_PARENT(MeshComponent, Component)

MeshComponent::MeshComponent()
{
    for(auto& meshInstance : m_meshInstances)
    {
        meshInstance = new MeshInstance();
    }
}

void MeshComponent::setName(const String& name)
{
    Component::setName(name);

#ifdef _DEBUG
    for(auto& meshInstance : m_meshInstances)
    {
        meshInstance->setName(name);
    }
#endif
}

void MeshComponent::setMeshResource(MeshResPtr meshResource)
{
    m_meshResource = meshResource;

    MeshInstance::CreateParams params;
    auto material = meshResource->getMaterialResource();
    params.m_pipelineHandle = material->getPipelineHandle();
    params.m_renderPassId = material->getRenderPassId();

    auto vertexStream = meshResource->getVertexStreamResource();
    params.m_vertexBufferHandle = vertexStream->getVertexBufferHandle();
    params.m_indexBufferHandle = vertexStream->getIndexBufferHandle();

    m_meshInstances[0]->set(params);
    m_meshInstances[1]->set(params);
}

void MeshComponent::setMeshResource(MeshResPtr meshResource, uint iRenderScene)
{
    m_meshResource = meshResource;

    MeshInstance::CreateParams params;
    auto material = meshResource->getMaterialResource();
    params.m_pipelineHandle = material->getPipelineHandle();
    params.m_renderPassId = material->getRenderPassId();

    auto vertexStream = meshResource->getVertexStreamResource();
    params.m_vertexBufferHandle = vertexStream->getVertexBufferHandle();
    params.m_indexBufferHandle = vertexStream->getIndexBufferHandle();

    m_meshInstances[iRenderScene]->set(params);
}

void MeshComponent::update(const MeshComponentUpdateParams& params)
{
    if(!isLoaded())
        return;

    while(Message* message = m_messageQueue.beginMessage())
    {
        uint32 messageTypeHash = message->getTypeHash();
        if(SpacialStateMsg* castedMsg = cast<SpacialStateMsg>(message))
        {
            if(castedMsg->m_set.isSet(SpacialStateMsg::Flags::Position))
                setPosition(castedMsg->m_position, params.m_iScene);

            if(castedMsg->m_set.isSet(SpacialStateMsg::Flags::Orientation))
            {
                // TODO
            }
        }

        m_messageQueue.endMessage();
    }

    // Calculate model view projection (MVP) transform.
    const Mat4& worldMatrix = getObjectToWorldTransform(params.m_iScene);
    Mat4 modelViewProjectionMatrix = params.m_viewPerspectiveMatrix * worldMatrix;

    // Calculate matrix used to transform normals.
    Mat4 modelViewMatrix = params.m_viewMatrix * worldMatrix;
    Mat3 modelViewMatrix3x3;
    mat4ToMat3(modelViewMatrix3x3, modelViewMatrix);

#ifdef _DEBUG
    static bool bCheckInvertible = false;
    if(bCheckInvertible)
    {
        Mat3 inverse;
        float det = .0f;
        bool bInvertible = false;
        float detThreshold = .001f;
        modelViewMatrix3x3.computeInverseAndDetWithCheck(inverse, det, bInvertible, detThreshold);
        OE_CHECK(bInvertible, OE_CHANNEL_MESH_COMPONENT);
    }
#endif

    Mat3 invModelViewMatrix3x3 = modelViewMatrix3x3.inverse();
    Mat3 transInvModelViewMatrix3x3 = invModelViewMatrix3x3.transpose();
    Mat4 transInvModelViewMatrix4x4;
    mat3ToMat4(transInvModelViewMatrix4x4, transInvModelViewMatrix3x3);

    // Calculate matrix to transform tangents.
    Mat3 model3x3;
    mat4ToMat3(model3x3, worldMatrix);
    Mat3 invModelMatrix3x3 = model3x3.inverse();
    Mat3 transInvModelMatrix3x3 = invModelMatrix3x3.transpose();
    Mat4 transInvModelMatrix4x4;
    mat3ToMat4(transInvModelMatrix4x4, transInvModelMatrix3x3);

    // Set transform matrices on mesh instance and its clone in render scene.
    MeshInstance* meshInstance = *m_meshInstances[params.m_iScene];
    meshInstance->m_instance.m_modelViewProjection = modelViewProjectionMatrix;
    meshInstance->m_instance.m_modelView = modelViewMatrix;
    meshInstance->m_instance.m_transInvModelView = transInvModelViewMatrix4x4;
    meshInstance->m_instance.m_transInvModel = transInvModelMatrix4x4;
}

};



#include "component.h"

#include "engine_private.h"

namespace OE_Engine
{

RTTI_IMPL(Component)

Component::Component()
{
}

Component::~Component()
{
}

bool Component::connectTo(Component* component, uint32 messageTypeHash)
{
    if(!component->connectFrom(this, messageTypeHash))
        return false;

    OE_CHECK(component, OE_CHANNEL_ENGINE);
    if(component->acceptMessages())
    {
        Recipient recipient;
        recipient.m_component = component;
        recipient.m_messageHashType = messageTypeHash;
        m_recipients.push_back(recipient);
        return true;
    }
    else
    {
        return false;
    }
}

bool Component::connectFrom(Component* sender, uint32 messageTypeHash)
{
    return true;
}

void Component::setName(const String& name)
{
    m_name = name;
}

const String& Component::getName() const
{
    return m_name;
}

} // namespace OE_Engine

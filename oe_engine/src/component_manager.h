#ifndef __OE_ENGINE_COMPONENT_MANAGER_H__
#define __OE_ENGINE_COMPONENT_MANAGER_H__

// OE_Core
#include <callback.h>
#include <linked_array.h>
#include <singleton.h>
#include <runnable.h>
#include <task_group.h>
#include <vector_eigen.h>

// OE_Engine
#include "oe_engine.h"
#include "engine.h"
#include "manager.h"


namespace OE_Engine
{

static const uint32 DefaultNumComponentsPerPage = 100;

template<class C, uint32 N = DefaultNumComponentsPerPage>
class ComponentManager : public Manager
{
public:
    typedef OE_Core::LinkedArray<C, N> ComponentsArray;

    ComponentManager();

    C* createComponent();
    void destroyComponent(C* component);

protected:
    ComponentsArray m_components;
};

template<class C, uint32 N>
ComponentManager<C, N>::ComponentManager()
{
}

template<class C, uint32 N>
C* ComponentManager<C, N>::createComponent()
{
    C* component = ComponentManager<C, N>::m_components.acquire();
    OE_CHECK(component != nullptr, OE_CHANNEL_ENGINE);
    return component;
}

template<class C, uint32 N>
void ComponentManager<C, N>::destroyComponent(C* component)
{
    m_components.release(component);
}

template<class C, class CUT, typename CUTP, uint32 N = DefaultNumComponentsPerPage>
class ComponentManagerMT : public ComponentManager<C, N>
{
public:
    ComponentManagerMT();
    virtual ~ComponentManagerMT(){}

    virtual bool update(float deltaTime) override;
    virtual void prepareTaskParams(CUTP* params, uint iPage, float deltaTime);

private:
    ComponentManagerMT(const ComponentManagerMT& rhs);
    ComponentManagerMT& operator=(const ComponentManagerMT& rhs);

    void onTaskGroupFinished();

    VectorEigen<SharedPtr<CUT> > m_tasks;
    OE_Core::TaskGroup m_taskGroup;
    OE_Core::CallbackEntry m_onTaskGroupFinishedCallback;
};

template<class C, class CUT, typename CUTP, uint32 N>
ComponentManagerMT<C, CUT, CUTP, N>::ComponentManagerMT()
{
    m_onTaskGroupFinishedCallback = m_taskGroup.addOnTasksFinishedCallback(
        OE_Core::MakeFunctor<ComponentManagerMT<C, CUT, CUTP, N>>(
            &ComponentManagerMT<C, CUT, CUTP, N>::onTaskGroupFinished,
            this,
            OE_Core::CallbackType::Permanent));
}

template<class C, class CUT, typename CUTP, uint32 N>
bool ComponentManagerMT<C, CUT,CUTP, N>::update(float deltaTime)
{
    if(!this->prerequesitesFinished())
        return false;

    const uint nComponents = ComponentManager<C, N>::m_components.size();
    const uint nPages = ComponentManager<C, N>::m_components.getNbUsedPages();

    if(nPages > m_tasks.size())
    {
        size_t nTaskToAdd = nPages - m_tasks.size();
        m_tasks.resize(nPages);
        for(size_t i = 0; i < nTaskToAdd; ++i)
        {
            m_tasks[nPages - i - 1] = SharedPtr<CUT>(new CUT());
        }
    }

    // Make sure all tasks finished executing from the previous frame
    // before queuing more.
    m_taskGroup.waitForCompletion();

    this->m_updateState = Manager::UpdateState::Started;

    if(nPages > 0)
    {
        for(uint i = 0; i < nPages; ++i)
        {
            CUTP updateTaskParams;
            prepareTaskParams(&updateTaskParams, i, deltaTime);
            m_tasks[i]->setParams(updateTaskParams);
            m_taskGroup.addTask(m_tasks[i]);
        }

        m_taskGroup.execute();
    }
    else
    {
        // This manager has no task to execute.
        this->m_updateState = Manager::UpdateState::Finished;
    }

    return true;
}

template<class C, class CUT, typename CUTP, uint32 N>
void ComponentManagerMT<C, CUT, CUTP, N>::prepareTaskParams(CUTP* params, uint iPage, float deltaTime)
{
    OE_CHECK(params, OE_CHANNEL_ENGINE);
    params->m_beginItr = ComponentManager<C, N>::m_components.beginPage(iPage);
    params->m_endItr = ComponentManager<C, N>::m_components.beginPage(iPage + 1);
    params->m_deltaTime = deltaTime;
    params->m_componentUpdateParams.m_deltaTime = deltaTime;
}

template<class C, class CUT, typename CUTP, uint32 N>
void ComponentManagerMT<C, CUT, CUTP, N>::onTaskGroupFinished()
{
    // All the code below must be thread safe as this function is called from a worker thread.

    // UpdateState is atomic so thread-safe.
    this->m_updateState = Manager::UpdateState::Finished;
}

template<class C, typename CUP, uint32 N = DefaultNumComponentsPerPage>
struct ComponentUpdateTaskParams
{
    typedef typename OE_Core::LinkedArray<C, N>::Itr ComponentsArrayItr;

    ComponentUpdateTaskParams()
    {
        m_deltaTime = 0.0f;
    }

    ComponentsArrayItr m_beginItr;
    ComponentsArrayItr m_endItr;
    float m_deltaTime;
    CUP m_componentUpdateParams;
};


template<class C, typename CUTP, uint32 N = DefaultNumComponentsPerPage>
class ComponentUpdateTask : public OE_Core::Runnable
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ComponentUpdateTask(){}
    ComponentUpdateTask(const CUTP& params);

    virtual void run() override;
    void setParams(const CUTP& params)
    {
        m_taskParams = params;
    }

private:
    typedef typename OE_Core::LinkedArray<C, N>::Itr ComponentsArrayItr;

    CUTP m_taskParams;
};

template<class C, typename CUTP, uint32 N>
void ComponentUpdateTask<C, CUTP, N>::run()
{
    C* component = nullptr;
    ComponentsArrayItr itr = m_taskParams.m_beginItr;

    while(itr != m_taskParams.m_endItr)
    {
        component = *itr;
        component->update(m_taskParams.m_componentUpdateParams);
        ++itr;
    }
}

} // namespace OE_Engine

#define DEFINE_COMPONENT_MANAGER_MEMBERS(ComponentClass)                public ComponentManagerMT<\
                                                                            ComponentClass,\
                                                                            ComponentClass##UpdateTask,\
                                                                            ComponentClass##UpdateTaskParams>,\
                                                                        public OE_Core::Singleton<ComponentClass##Manager>\
                                                                    {\
                                                                    public:\
                                                                        ComponentClass##Manager();\
                                                                        virtual ~ComponentClass##Manager();\
                                                                    private:\
                                                                        ComponentClass##Manager(const ComponentClass##Manager& rhs);\
                                                                        ComponentClass##Manager& operator=(const ComponentClass##Manager& rhs);\
                                                                    };


#define DEFINE_ENGINE_COMPONENT_MANAGER_CLASS(ComponentClass)       class OE_ENGINE_EXPORT ComponentClass##Manager :\
                                                                        DEFINE_COMPONENT_MANAGER_MEMBERS(ComponentClass)


#define DEFINE_COMPONENT_MANAGER_CLASS(ComponentClass)              class ComponentClass##Manager :\
                                                                        DEFINE_COMPONENT_MANAGER_MEMBERS(ComponentClass)


#define DEFINE_ENGINE_COMPONENT_MANAGER(ComponentClass)             DEFINE_COMPONENT_UPDATE_TASK_PARAMS(ComponentClass)\
                                                                    DEFINE_COMPONENT_UPDATE_TASK(ComponentClass)\
                                                                    DEFINE_ENGINE_COMPONENT_MANAGER_CLASS(ComponentClass)\


#define DEFINE_COMPONENT_MANAGER(ComponentClass)                    DEFINE_COMPONENT_UPDATE_TASK_PARAMS(ComponentClass)\
                                                                    DEFINE_COMPONENT_UPDATE_TASK(ComponentClass)\
                                                                    DEFINE_COMPONENT_MANAGER_CLASS(ComponentClass)\

#endif // __OE_ENGINE_COMPONENT_MANAGER_H__

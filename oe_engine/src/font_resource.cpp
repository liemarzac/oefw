#include "font_resource.h"

// OE_Core
#include <resource_manager.h>

// OE_Graphic
#include <material_resource.h>
#include <texture_resource.h>

// OE_Engine
#include "engine_private.h"
#include "font_manager.h"


namespace OE_Engine
{

RTTI_IMPL_PARENT(FontResource, Resource)

FontResource::FontResource(const String& name):
    Resource(name)
{
}

void FontResource::loadImpl()
{
}

void FontResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    // Map font name to font data name.
    String fontDataResourceName =
        FontManager::getInstance().getConfig()[getName()].asString();

    OE_CHECK(fontDataResourceName.length() > 0, OE_CHANNEL_RESOURCE);

    // Add font description as a dependency.
    m_dataResource = addDependency<FontDataResource>(dependencyCallback, fontDataResourceName);

    // Add font material as a dependency.
    String materialName = "materials/font_" + getName();
    m_materialResource = addDependency<MaterialResource>(dependencyCallback, materialName);

    // This resource does not have to load anything,
    // because it is only a wrapper around other dependencies.
    setState(State::Loaded);
}

} // namespace OE_Engine

#include "scene_command.h"

// OE_Graphic
#include <render_scene.h>

// OE_Engine
#include "engine_private.h"
#include "composer.h"

using namespace OE_Engine;

void AddMeshInstanceSceneCommand::execute()
{
    auto& composer = Composer::getInstance();
    composer.addMeshInstance(m_meshInstance);
}

void RemoveMeshInstanceSceneCommand::execute()
{
    auto& composer = Composer::getInstance();
    composer.removeMeshInstance(m_meshInstance);
}

void SetDominantDirectionalLightSceneCommand::execute()
{
    Composer& renderer = Composer::getInstance();
    RenderScene& scene = renderer.getMainThreadScene();
    OE_CHECK(scene.m_bRendering == false, OE_CHANNEL_SCENE_COMMANDS);
    // Transform from world space to camera space.
    Vec4 dir4(m_direction[0], m_direction[1], m_direction[2], 0.0f);
    Vec4 camSpaceDir4 = renderer.getViewMatrix() * dir4;
    Vec3 cameraSpaceDirection(camSpaceDir4[0], camSpaceDir4[1], camSpaceDir4[2]);

    scene.setDominantDirectionalLight(cameraSpaceDirection);
}

void RemoveDominantDirectionalLightSceneCommand::execute()
{
    Composer& renderer = Composer::getInstance();
    RenderScene& scene = renderer.getMainThreadScene();
    OE_CHECK(scene.m_bRendering == false, OE_CHANNEL_SCENE_COMMANDS);
    scene.removeDominantDirectionalLight();
}


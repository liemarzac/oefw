#ifndef __OE_ENGINE_FONT_H__
#define __OE_ENGINE_FONT_H__

#include "oe_engine.h"

// OE_Core
#include <buffer.h>
#include <rect_2d.h>
#include <serializable.h>

namespace OE_Engine
{

struct OE_ENGINE_EXPORT GlyphData
{
    OE_Core::Rect2D m_rect;
    int32 m_left;
    int32 m_top;
    int32 m_advanceX;
    int32 m_advanceY;
    Map<int, int> m_kerningData;
};

typedef Map<int, GlyphData> GlyphsData;
typedef Map<int, GlyphsData> FontSizesData;

struct OE_ENGINE_EXPORT FontData
{
    String m_fontName;
    FontSizesData m_fontSizes;
};

typedef Vector<FontData> FontsData;


void OE_ENGINE_EXPORT serialize_impl(OE_Core::Buffer& buffer, const GlyphData& glyphData, const GlyphData*);

void OE_ENGINE_EXPORT serialize_impl(OE_Core::Buffer& buffer, GlyphData& glyphData, const GlyphData*);

size_t OE_ENGINE_EXPORT calculateSerializedSize_impl(const GlyphData& glyphData, const GlyphData*);

void OE_ENGINE_EXPORT serialize_impl(OE_Core::Buffer& buffer, const FontData& fontData, const FontData*);

void OE_ENGINE_EXPORT deserialize_impl(OE_Core::Buffer& buffer, FontData& fontData, const FontData*);

size_t OE_ENGINE_EXPORT calculateSerializedSize_impl(const FontData& fontData, const FontData*);

struct OE_ENGINE_EXPORT FontTexture
{
    uint32 m_textureWidth;
    uint32 m_textureHeight;
    OE_Core::Buffer m_textureBuffer;
};

void OE_ENGINE_EXPORT serialize_impl(OE_Core::Buffer& buffer, const FontTexture& fontData, const FontTexture*);

void OE_ENGINE_EXPORT deserialize_impl(OE_Core::Buffer& buffer, FontTexture& fontData, const FontTexture*);

size_t OE_ENGINE_EXPORT calculateSerializedSize_impl(const FontTexture& fontData, const FontTexture*);

} // namespace OE_Engine

#endif // __OE_ENGINE_FONT_H__

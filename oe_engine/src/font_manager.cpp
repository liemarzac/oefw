#include "font_manager.h"

#include "engine_private.h"

// OE_Core
#include <file.h>

// OE_Engine
#include "font_resource.h"

namespace OE_Engine
{

FontManager::FontManager()
{
    appLoadJsonFile("configs/font_manager.json", m_config);
}

FontManager::~FontManager()
{
}

} // namespace OE_Engine

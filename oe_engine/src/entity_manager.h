#ifndef __OE_ENGINE_ENTITY_MANAGER_H__
#define __OE_ENGINE_ENTITY_MANAGER_H__

#include <singleton.h>

#include "oe_engine.h"
#include "manager.h"

namespace OE_Engine
{

class Entity;

class OE_ENGINE_EXPORT EntityManager :
    public Manager,
    public OE_Core::Singleton<EntityManager>
{
public:
    EntityManager();
    ~EntityManager();

    void addEntity(Entity* entity);
    void removeEntity(Entity* entity);

    Entity* getRoot() const;

private:
    EntityManager(const EntityManager& rhs);
    EntityManager& operator=(const EntityManager& rhs);

    Entity* m_root;
};

inline Entity* EntityManager::getRoot() const
{
    return m_root;
}

} // namespace OE_Engine

#endif // __OE_ENGINE_ENTITY_MANAGER_H__

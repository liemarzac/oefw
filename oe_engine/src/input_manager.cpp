#include "input_manager.h"

// OE_Engine
#include "engine_private.h"

namespace OE_Engine
{

InputManager::InputManager()
{
    m_mouseX = 0;
    m_mouseY = 0;
    m_lastMouseX = 0;
    m_lastMouseY = 0;
    m_mouseWheelX = 0;
    m_mouseWheelY = 0;
    
    m_inputDownList.reserve(20);
    m_inputPendingPressedList.reserve(20);
    m_inputJustPressedList.reserve(20);
}

InputManager::~InputManager()
{
}


void InputManager::addInputDown(int32 keycode)
{
    if(std::find(m_inputDownList.begin(), m_inputDownList.end(), keycode) == m_inputDownList.end())
    {
        m_inputDownList.push_back(keycode);
        m_inputPendingPressedList.push_back(keycode);
    }
}


void InputManager::removeInputDown(int32 keycode)
{
    Vector<int32>::iterator inputToRemoveItr;
    inputToRemoveItr = std::find(m_inputDownList.begin(), m_inputDownList.end(), keycode);

    if(inputToRemoveItr != m_inputDownList.end())
    {
        m_inputDownList.erase(inputToRemoveItr);
    }
}


void InputManager::update()
{
    if(m_lastMouseX != m_mouseX || m_lastMouseY != m_mouseY)
    {
        m_lastMouseX = m_mouseX;
        m_lastMouseY = m_mouseY;
    }

    m_inputJustPressedList.clear();
    m_inputJustPressedList = m_inputPendingPressedList;
    m_inputPendingPressedList.clear();
}

void InputManager::handleEvent(SDL_Event& event)
{
    switch(event.type)
    {
    case SDL_KEYDOWN:
        addInputDown(event.key.keysym.sym);
        break;

    case SDL_KEYUP:
        removeInputDown(event.key.keysym.sym);
        break;

    case SDL_MOUSEMOTION:
        m_mouseX = event.motion.x;
        m_mouseY = event.motion.y;
        break;

    case SDL_MOUSEBUTTONDOWN:
        switch(event.button.button)
        {
        case SDL_BUTTON_LEFT:
            addInputDown(Buttons::LeftMouseButton);
            break;

        case SDL_BUTTON_RIGHT:
            addInputDown(Buttons::RightMouseButton);
            break;

        case SDL_BUTTON_MIDDLE:
            addInputDown(Buttons::MiddleMouseButton);
            break;
        }
        break;

    case SDL_MOUSEBUTTONUP:
        switch(event.button.button)
        {
        case SDL_BUTTON_LEFT:
            removeInputDown((int32)Buttons::LeftMouseButton);
            break;

        case SDL_BUTTON_RIGHT:
            removeInputDown((int32)Buttons::RightMouseButton);
            break;

        case SDL_BUTTON_MIDDLE:
            removeInputDown((int32)Buttons::MiddleMouseButton);
            break;
        }
        break;

    case SDL_MOUSEWHEEL:
        m_mouseWheelX += event.wheel.x;
        m_mouseWheelY += event.wheel.y;
    }
}


void InputManager::getMouseMove(int32& mouseMoveX, int32& mouseMoveY) const
{
    mouseMoveX = m_mouseX - m_lastMouseX;
    mouseMoveY = m_mouseY - m_lastMouseY;
}

void InputManager::getMousePos(int32& mouseX, int32& mouseY) const
{
    mouseX = m_mouseX;
    mouseY = m_mouseY;
}

void InputManager::getMouseWheel(int32* mouseWheelX, int32* mouseWheelY) const
{
    if(mouseWheelX)
        *mouseWheelX = m_mouseWheelX;
    
    if(mouseWheelY)
        *mouseWheelY = m_mouseWheelY;
}

bool InputManager::hasJustBeenPressed(int32 keycode) const
{
    return std::find(m_inputJustPressedList.begin(), m_inputJustPressedList.end(), keycode) != m_inputJustPressedList.end();
}


bool InputManager::isInputDown(int32 keycode) const
{
    return std::find(m_inputDownList.begin(), m_inputDownList.end(), keycode) != m_inputDownList.end();
}

} // namespace OE_Engine

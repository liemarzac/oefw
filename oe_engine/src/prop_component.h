#ifndef __OE_ENGINE_PROP_COMPONENT_H__
#define __OE_ENGINE_PROP_COMPONENT_H__

#include "oe_engine.h"

// OE_Core
#include <callback.h>

// OE_Graphic
#include <graphic_resource_helpers.h>
#include <mesh_instance.h>

// OE_Engine
#include "entity.h"
#include "mesh_component.h"


namespace OE_Engine
{

class OE_ENGINE_EXPORT PropComponent : public Component
{
    RTTI_DECLARATION

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    PropComponent();
    virtual ~PropComponent();

    virtual bool connectFrom(Component* sender, uint32 messageTypeHash) override;

    virtual void setOrientation(const Mat4& orientation);
    virtual void setPosition(const Vec3& position);
    virtual void getMeshInstances(Vector<OE_Graphic::MeshInstancePtr>& meshInstances, uint iScene);

    void setPropResource(const String& name);
    void setPropResource(OE_Graphic::PropResPtr proResource);
    OE_Graphic::PropResPtr getPropResource();
    bool isLoaded() const;
    OE_Core::CallbackEntry addLoadedCallback(SharedPtr<OE_Core::Functor> callback);
    void removeLoadedCallback(OE_Core::CallbackEntry entry);

    virtual void setName(const String& name) override;

protected:
    OE_Graphic::PropResPtr m_propResource;
    Vector<MeshComponent*> m_meshComponents;

    Vec3 m_position;
    Mat4 m_orientation;

private:
    PropComponent(const PropComponent& rhs);
    PropComponent& operator=(const PropComponent& rhs);

    MeshComponent* addMeshComponent(OE_Graphic::MeshResPtr meshResource);
    void onResourceLoaded();

    OE_Core::CallbackEntry m_onResourceLoadedCallback;
    OE_Core::FunctorGroup m_onLoadedCallbacks;
    bool m_bLoaded;
};


} // namespace OE_Engine

#endif // __OE_ENGINE_PROP_COMPONENT_H__

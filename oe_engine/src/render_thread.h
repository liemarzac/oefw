#ifndef __OE_ENGINE_RENDER_THREAD_H__
#define __OE_ENGINE_RENDER_THREAD_H__

#include <std_atomic_helpers.h>

namespace OE_Graphic
{
    struct RendererConfig;
}

namespace OE_Engine
{

class Composer;

struct RenderThreadParams
{
    RenderThreadParams()
    {
        m_shouldRender.store(true);
        m_renderThreadFrameTime.store(0.0);
        m_commandThreadFrameTime.store(0.0);
        m_bHoldRendering.store(false);
    }

    const OE_Graphic::RendererConfig* m_config = nullptr;
    AtomicBool m_shouldRender;
    AtomicDouble m_renderThreadFrameTime;
    AtomicDouble m_commandThreadFrameTime;

    // Rendering flush
    std::condition_variable m_renderResumeNotifier;
    Mutex m_holdRenderingMutex;
    AtomicBool m_bHoldRendering;
    bool m_bRenderingFlushed = false;
};

void OE_ENGINE_EXPORT renderLoop(RenderThreadParams* params);

}

#endif // __OE_ENGINE_RENDER_THREAD_H__

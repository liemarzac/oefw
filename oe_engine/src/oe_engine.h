#ifndef __OE_ENGINE_H__
#define __OE_ENGINE_H__

#include <oe_core.h>
#include <oe_graphic.h>

#include "oe_engine_export.h"

#define OE_CHANNEL_ENGINE "Engine"

namespace OE_Engine {}

#endif // __OE_ENGINE_H__

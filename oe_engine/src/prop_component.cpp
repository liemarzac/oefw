#include "prop_component.h"

// OE_Core
#include <callback.h>
#include <resource_manager.h>

// OE_Engine
#include "engine_private.h"
#include "mesh_component_manager.h"
#include "mesh_resource.h"
#include "prop_resource.h"
#include "composer.h"
#include "spacial_state_msg.h"

namespace OE_Engine
{

RTTI_IMPL_PARENT(PropComponent, Component)

PropComponent::PropComponent()
{
    m_position = Vec3(0.0f, 0.0f, 0.0f);
    m_orientation.setIdentity();
    m_bLoaded = false;
}

PropComponent::~PropComponent()
{
    if(m_propResource)
    {
        m_propResource->removeLoadedCallback(m_onResourceLoadedCallback);
    }

    for(MeshComponent* meshComponent : m_meshComponents)
    {
        MeshComponentManager::getInstance().destroyComponent(meshComponent);
    }
}

bool PropComponent::connectFrom(Component* sender, uint32 messageTypeHash)
{
    if(messageTypeHash == SpacialStateMsg::getTypeHash())
    {
        for(MeshComponent* meshComponent : m_meshComponents)
        {
            sender->connectTo(meshComponent, messageTypeHash);
        }
    }

    return true;
}

void PropComponent::setPropResource(const String& name)
{
    if(m_propResource)
    {
        m_propResource->removeLoadedCallback(m_onResourceLoadedCallback);
    }

    auto& resourceManager = ResourceManager::getInstance();
    m_propResource = resourceManager.getResource<PropResource>(name);

    m_onResourceLoadedCallback = m_propResource->addLoadedCallback(
        MakeFunctor(&PropComponent::onResourceLoaded, this, CallbackType::Permanent));
}

void PropComponent::setPropResource(PropResPtr propResource)
{
    if(m_propResource)
    {
        m_propResource->removeLoadedCallback(m_onResourceLoadedCallback);
    }

    m_propResource = propResource;

    m_onResourceLoadedCallback = m_propResource->addLoadedCallback(
        MakeFunctor(&PropComponent::onResourceLoaded, this, CallbackType::Permanent));
}

PropResPtr PropComponent::getPropResource()
{
    return m_propResource;
}

void PropComponent::onResourceLoaded()
{
    const Vector<MeshResPtr>& meshResources = m_propResource->getMeshResources();
    for(MeshResPtr meshResource : meshResources)
    {
        MeshComponent* meshComponent = addMeshComponent(meshResource);

        // Set initial spacial state for all scenes.
        // It is safe to do so because the mesh instances has not been added
        // to the render scenes yet.
        for(uint iScene = 0; iScene < 2; ++iScene)
        {
            meshComponent->setPosition(m_position, iScene);
            meshComponent->setOrientation(m_orientation, iScene);
        }
    }

    // Callbacks bubble up.
    m_onLoadedCallbacks.callback();

    m_bLoaded = true;
}

bool PropComponent::isLoaded() const
{
    return m_bLoaded;
}

CallbackEntry PropComponent::addLoadedCallback(SharedPtr<Functor> callback)
{
    CallbackEntry entry = m_onLoadedCallbacks.addCallback(callback);
    if(isLoaded())
    {
        m_onLoadedCallbacks.callback();
    }
    return entry;
}

void PropComponent::removeLoadedCallback(CallbackEntry entry)
{
    m_onLoadedCallbacks.removeCallback(entry);
}

MeshComponent* PropComponent::addMeshComponent(MeshResPtr meshResource)
{
    MeshComponent* meshComponent = MeshComponentManager::getInstance().createComponent();
    meshComponent->setMeshResource(meshResource);
    meshComponent->acceptMessages(true);
    m_meshComponents.push_back(meshComponent);
    return meshComponent;
}

void PropComponent::setOrientation(const Mat4& orientation)
{
    Composer& renderer = Composer::getInstance();
    uint iScene = renderer.getMainThreadSceneIndex();

    for(MeshComponent* meshComponent : m_meshComponents)
    {
        meshComponent->setOrientation(orientation, iScene);
    }

    m_orientation = orientation;
}

void PropComponent::setPosition(const Vec3& position)
{
    Composer& renderer = Composer::getInstance();
    uint iScene = renderer.getMainThreadSceneIndex();

    for(MeshComponent* meshComponent : m_meshComponents)
    {
        meshComponent->setPosition(position, iScene);
    }

    m_position = position;
}

void PropComponent::getMeshInstances(Vector<MeshInstancePtr>& meshInstances, uint iScene)
{
    for(MeshComponent* meshComponent : m_meshComponents)
    {
        meshInstances.push_back(meshComponent->getMeshInstance(iScene));
    }
}

void PropComponent::setName(const String& name)
{
    for(MeshComponent* meshComponent : m_meshComponents)
    {
        meshComponent->setName(name);
    }
}

}


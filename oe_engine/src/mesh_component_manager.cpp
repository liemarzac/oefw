#include "mesh_component_manager.h"

#include "engine_private.h"

#include "composer.h"
#include "spacial_state_msg.h"

namespace OE_Engine
{

MeshComponentManager::MeshComponentManager()
{
}

MeshComponentManager::~MeshComponentManager()
{
}

void MeshComponentManager::prepareTaskParams(
    MeshComponentUpdateTaskParams* params,
    uint iPage,
    float deltaTime)
{
    ComponentManagerMT::prepareTaskParams(params, iPage, deltaTime);

    const Composer& composer = Composer::getInstance();

    const Mat4& projectionMaxtrix = composer.getWorldProjectionMatrix();

    // TODO: It is incorrect to target the world scene.
    params->m_componentUpdateParams.m_viewMatrix = composer.getViewMatrix();
    params->m_componentUpdateParams.m_projectionMatrix = projectionMaxtrix;
    params->m_componentUpdateParams.m_viewPerspectiveMatrix = projectionMaxtrix * params->m_componentUpdateParams.m_viewMatrix;
    params->m_componentUpdateParams.m_iScene = composer.getMainThreadSceneIndex();
}

};



#ifndef __OE_ENGINE_MESSAGE_QUEUE_H__
#define __OE_ENGINE_MESSAGE_QUEUE_H__

#include "oe_engine.h"

// OE_Core
#include <ring_buffer.h>

namespace OE_Engine
{

class Message;

class OE_ENGINE_EXPORT MessageQueue
{
public:
    MessageQueue();

    template<typename T>
    T* allocMessage(void* sender);
    void commitMessage();

    Message* beginMessage();
    void endMessage();

private:
    OE_Core::RingBuffer m_ringBuffer;
    Message* m_currentMessage;
};

template<typename T>
inline T* MessageQueue::allocMessage(void* sender)
{
    void* ptr = nullptr;
    m_ringBuffer.alloc(ptr, sizeof(T));
    T* message = reinterpret_cast<T*>(ptr);
    return new(message) T(sender);
}

inline void MessageQueue::commitMessage()
{
    m_ringBuffer.commit();
}

}

#endif // __OE_ENGINE_MESSAGE_QUEUE_H__

#include "composer.h"

// OE_Core
#include <oe_math.h>
#include <json_helpers.h>
#include <ring_buffer.h>
#include <thread.h>

// OE_Graphic
#include <graphic_math.h>
#include <material_resource.h>
#include <mesh_resource.h>
#include <render_scene.h>
#include <renderer.h>
#include <texture_resource.h>
#include <ubo.h>
#include <vertex_stream_resource.h>
#include <view_resource.h>

// OE_Engine
#include "camera.h"
#include "ui_render_commands.h"
#include "engine_private.h"
#include "font_resource.h"
#include "mesh_component_manager.h"
#include "render_thread.h"
#include "resource_manager.h"
#include "world_scene.h"


#define OE_CHANNEL_COMPOSER "Composer"

namespace OE_Engine
{

const char* Composer::RenderSceneNames[] = {
    "World",
    "UI"
};

Composer::Composer()
{
    m_renderer = new Renderer();
    m_worldScene = new WorldScene(m_renderer);
    m_vFOV = degToRad(50.0f);
    m_camera = new Camera();
    m_nearPlane = 1.0f;
    m_farPlane = 512.0f;
}

Composer::~Composer()
{
    // Prepare destruction.
    destroyResources();

    m_renderThreadParams.m_shouldRender = false;
    flushRenderering();
    m_thread.join();

    // Mirror constructor.
    delete m_camera;
    delete m_worldScene;
    delete m_renderer;
}

bool Composer::init(const RendererConfig& config)
{
    Json::Value jsonConfig;
    if (appLoadJsonFile("configs" PATH_SEPARATOR "composer.json", jsonConfig))
    {
        Json::Value jsonCamera = jsonConfig["camera"];
        if (jsonCamera.isObject())
        {
            // Position
            {
                Json::Value jsonCameraPos = jsonCamera["position"];
                Vec3 position;
                if (jsonValueToVec3(jsonCameraPos, position))
                {
                    m_camera->setPosition(position);
                }
            }

            // Lookat
            {
                Json::Value jsonLookAt = jsonCamera["look_at"];
                Vec3 lookAt;
                if (jsonValueToVec3(jsonLookAt, lookAt))
                {
                    m_camera->setLookAt(lookAt);
                }
            }
        }
    }


    m_renderer->initFromMainThread(config);

    m_renderThreadParams.m_config = &config;

    m_thread = std::thread(renderLoop, &m_renderThreadParams);
    setThreadName(&m_thread, "render_thread");

    m_screenSize = config.m_screenSize;

    updateProjectionMatrices();

    return true;
}

void Composer::update(float deltaTime)
{
    m_worldScene->update(deltaTime);
    m_camera->update(deltaTime);
    updateWorldMatrices();
    updateUIMatrices();
}

void Composer::updateProjectionMatrices()
{
    const float aspectRatio = static_cast<float>(m_screenSize.m_width) / static_cast<float>(m_screenSize.m_height);
    const float tanHalfVFOV = tan(m_vFOV / 2.0f);
    const float xScale = 1.0f / (aspectRatio * tanHalfVFOV);

    const float yScale = -1.0f / tanHalfVFOV;

	m_projectionMatrix.col(0) = Vec4(xScale, 0.0f, 0.0f, 0.0f);
	m_projectionMatrix.col(1) = Vec4(0.0f, 0.0f, m_farPlane / (m_farPlane - m_nearPlane), 1.0f);
	m_projectionMatrix.col(2) = Vec4(0.0f, yScale, 0.0f, 0.0f);
	m_projectionMatrix.col(3) = Vec4(0.0f, 0.0f, (m_nearPlane * m_farPlane) / (m_nearPlane - m_farPlane), 0.0f);

    updateWorldMatrices();

    ortho(
        m_transform2dMatrix,
        0.0f,
        static_cast<float>(m_screenSize.m_width),
        static_cast<float>(m_screenSize.m_height),
        0.0f,
        0.0f,
        1.0f);

    updateUIMatrices();
}

void Composer::updateWorldMatrices()
{
    const Mat4& viewMatrix = m_camera->getWorldToViewMatrix();

    RenderScene* scene = m_renderer->getRenderScene();
    scene->setViewMatrix(viewMatrix);
    scene->setProjectionMatrix(m_projectionMatrix);

    auto& resourceManager = ResourceManager::getInstance();
    auto viewResource = resourceManager.getResource<ViewResource>("render/views/default");
    if(viewResource && viewResource->isLoaded())
    {
        DefaultView_UBO* ubo = viewResource->getUBO<DefaultView_UBO>();
        OE_CHECK(ubo, OE_CHANNEL_COMPOSER);
        ubo->m_data.m_projection = m_projectionMatrix;
        ubo->m_data.m_view = viewMatrix;
        ubo->m_data.m_viewProjection = m_projectionMatrix * viewMatrix;
        viewResource->stage();
    }

    auto lightMaterialResource = resourceManager.getResource<MaterialResource>("materials/composition");
    if(lightMaterialResource && lightMaterialResource->isLoaded())
    {
        Lighting_UBO* ubo = lightMaterialResource->getUBO<Lighting_UBO>();
        OE_CHECK(ubo, OE_CHANNEL_COMPOSER);
        ubo->m_data.m_view = viewMatrix;
        vec3ToVec4(ubo->m_data.m_viewPos, m_camera->getPosition() * -1.0f, .0f);

        // 5 fixed lights
        Array<Vec3, 5> lightColors;
        lightColors[0] = Vec3(1.0f, 0.0f, 0.0f);
        lightColors[1] = Vec3(1.0f, 0.7f, 0.7f);
        lightColors[2] = Vec3(1.0f, 0.0f, 0.0f);
        lightColors[3] = Vec3(0.0f, 0.0f, 1.0f);
        lightColors[4] = Vec3(1.0f, 0.0f, 0.0f);

        for(int32_t i = 0; i < lightColors.size(); i++)
        {
            ubo->m_data.m_lights[i].position = Vec4((float)(i - 2.5f) * 50.0f, -10.0f, 0.0f, 1.0f);
            vec3ToVec4(ubo->m_data.m_lights[i].color, lightColors[i], 1.0f);
            ubo->m_data.m_lights[i].radius = 120.0f;
        }

        // Lion eyes
        size_t iLightLionEye = lightColors.size();
        ubo->m_data.m_lights[iLightLionEye].position = Vec4(-122.0f, -18.0f, -3.2f, 1.0f);
        ubo->m_data.m_lights[iLightLionEye].color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
        ubo->m_data.m_lights[iLightLionEye].radius = 25.0f;

        //setupLight(&uboFragmentLights.lights[13], { -122.0f, -18.0f, -3.2f }, { 1.0f, 0.3f, 0.3f }, 25.0f);
        //setupLight(&uboFragmentLights.lights[14], { -122.0f, -18.0f,  3.2f }, { 0.3f, 1.0f, 0.3f }, 25.0f);

        //setupLight(&uboFragmentLights.lights[15], { 135.0f, -18.0f, -3.2f }, { 0.3f, 0.3f, 1.0f }, 25.0f);
        //setupLight(&uboFragmentLights.lights[16], { 135.0f, -18.0f,  3.2f }, { 1.0f, 1.0f, 0.3f }, 25.0f);

        lightMaterialResource->stage();
    }
}

void Composer::updateUIMatrices()
{
    auto& resourceManager = ResourceManager::getInstance();
    auto viewResource = resourceManager.getResource<ViewResource>("render/views/screen");
    if(viewResource && viewResource->isLoaded())
    {
        Mat4 fullscreenOrthoProjection;
        ortho(
             fullscreenOrthoProjection,
             0.0f,
             1.0f,
             1.0f,
             0.0f,
             0.0f,
             1.0f);

        ScreenView_UBO* ubo = viewResource->getUBO<ScreenView_UBO>();
        OE_CHECK(ubo, OE_CHANNEL_COMPOSER);
        ubo->m_data.m_projection = fullscreenOrthoProjection;
        viewResource->stage();
    }
}

void Composer::flushRenderering()
{
    m_renderer->flush();
}

void Composer::resize(const ScreenSize& newSize)
{
    m_screenSize = newSize;
    updateProjectionMatrices();

    m_flags.set(Flags::PendingResize);
    m_renderer->resize(newSize);
}

void Composer::pause()
{
    m_flags.set(Flags::Pause);
}

void Composer::resume()
{
    m_flags.set(Flags::Resume);
}

bool Composer::isPaused() const
{
    return m_flags.isSet(Flags::Paused);
}

void Composer::getTextDimensions(
    const String& text,
    const String& fontName,
    uint32 fontSize,
    uint32& width,
    uint32& height,
    uint32& bearingY)
{
    width = 0;
    height = 0;
    bearingY = 0;

    auto& resourceManager = ResourceManager::getInstance();
    auto fontResource = resourceManager.getResource<FontResource>(fontName);

    if(fontResource.get() == nullptr ||
        fontResource->getState() != Resource::State::Loaded)
    {
        return;
    }

    auto material = fontResource->getMaterialResource();
    const GlyphsData* glyphsData = fontResource->getGlyphsData(fontName, fontSize);
    if(!glyphsData)
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot find font size %d in font %s", fontSize, fontName.c_str());
        return;
    }

    uint32 maxUp = 0;
    uint32 maxDown = 0;

    const size_t textLen = text.length();

    for(size_t i = 0; i < textLen; ++i)
    {
        uint32 unicode = text[i];

        // The metric of a space is considered to be the metric of an hyphen.
        if(unicode == ' ')
            unicode = '-';

        GlyphsData::const_iterator glyphItr = glyphsData->find(unicode);
        if(glyphItr == glyphsData->end())
        {
            // Glyph not found. Try to find the rectangle glyph.
           glyphItr = glyphsData->find(9712);
           if(glyphItr == glyphsData->end())
               continue;
        }

        const GlyphData& glyphData = (*glyphItr).second;

        width += glyphData.m_advanceX;

        uint32 up = glyphData.m_top;
        uint32 down = glyphData.m_rect.m_height - glyphData.m_top;

        if(up > maxUp)
            maxUp = up;

        if(down > maxDown)
            maxDown = down;
    }

    height = maxUp + maxDown;
    bearingY = maxUp;
}

void Composer::renderText(
        const String& text,
        const String& fontName,
        uint32 fontSize,
        uint32 x,
        uint32 y,
        HorizontalAnchor hAnchor,
        VerticalAnchor vAnchor)
{
    if(isPaused())
        return;

    uint32 width, height, bearingY;
    getTextDimensions(
        text,
        fontName,
        fontSize,
        width,
        height,
        bearingY);

    auto& resourceManager = ResourceManager::getInstance();
    auto fontResource = resourceManager.getResource<FontResource>(fontName);

    if(fontResource->getState() != Resource::State::Loaded)
    {
        return;
    }

    auto materialResource = fontResource->getMaterialResource();

    TextureResPtr diffuse = materialResource->getTexture(0);
    OE_CHECK(diffuse, OE_CHANNEL_COMPOSER);

    const uint32 textureWidth = diffuse->getWidth();
    const uint32 textureHeight = diffuse->getHeight();

    const GlyphsData* glyphsData = fontResource->getGlyphsData(fontName, fontSize);
    if(!glyphsData)
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot find font size %d in font %s", fontSize, fontName.c_str());
        return;
    }

    int32 penX = x;
    int32 penY = y;

    switch(hAnchor)
    {
    case HorizontalAnchor::Left:
    break;

    case HorizontalAnchor::Center:
    penX -= width / 2;
    break;

    case HorizontalAnchor::Right:
    penX -= width;
    break;

    default:
    OE_CHECK(false, OE_CHANNEL_COMPOSER);
    break;
    }

    switch(vAnchor)
    {
    case VerticalAnchor::Top:
    penY += bearingY;
    break;

    case VerticalAnchor::Center:
    penY += (height / 2) - (height - bearingY);
    break;

    case VerticalAnchor::Bottom:
    penY -= height - bearingY;
    break;

    default:
    OE_CHECK(false, OE_CHANNEL_COMPOSER);
    break;
    }

    const size_t textLen = text.length();

    // Loop through all characters and count the ones which can be displayed.
    uint32 numCharacters = 0;
    for(size_t i = 0; i < textLen; ++i)
    {
        // TODO This is incorrect for non ANSI str.
        uint32 unicode = text.c_str()[i];

        // Space character is not displayed.
        if(unicode == ' ')
            continue;

        GlyphsData::const_iterator glyphItr = glyphsData->find(unicode);
        if(glyphItr == glyphsData->end())
        {
            // Glyph not found. Try to find the rectangle glyph.
            glyphItr = glyphsData->find(9712);
            if(glyphItr == glyphsData->end())
                continue;
        }

        ++numCharacters;
    }

    // 6 vertices with 3 floats for position, 2 for texcoord.
    UniqueBufferPtr vertexBuffer(new Buffer(numCharacters * 6 * (3 + 2) * sizeof(float)));

    for(size_t i = 0; i < textLen; ++i)
    {
        // TODO This is incorrect for non ANSI str.
        uint32 unicode = text.c_str()[i];

        bool isSpaceCharacter = false;

        // The metric of a space is considered to be the metric of an hyphen.
        if(unicode == ' ')
        {
            unicode = '-';
            isSpaceCharacter = true;
        }

        GlyphsData::const_iterator glyphItr = glyphsData->find(unicode);
        if(glyphItr == glyphsData->end())
        {
            // Glyph not found. Try to find the rectangle glyph.
            glyphItr = glyphsData->find(9712);
            if(glyphItr == glyphsData->end())
                continue;
        }

        const GlyphData& glyphData = (*glyphItr).second;

        if(isSpaceCharacter)
        {
            penX += glyphData.m_advanceX;
            continue;
        }

        Rect2D quadCoord;
        quadCoord.m_x = penX + glyphData.m_left;
        quadCoord.m_y = penY - glyphData.m_top;
        quadCoord.m_width = glyphData.m_rect.m_width;
        quadCoord.m_height = glyphData.m_rect.m_height;

#if OE_RENDERER == OE_RENDERER_DIRECTX_9
        const float offsetX = -0.5f;
        const float offsetY = -0.5f;
#else
        const float offsetX = 0.0f;
        const float offsetY = 0.0f;
#endif

        // Vertex 1.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 2.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 3.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        // Vertex 4.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)quadCoord.m_y + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y)) / textureHeight); // v

        // Vertex 5.
        vertexBuffer->write((float)(quadCoord.m_x + quadCoord.m_width) + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x + glyphData.m_rect.m_width)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        // Vertex 6.
        vertexBuffer->write((float)quadCoord.m_x + offsetX); // x
        vertexBuffer->write((float)(quadCoord.m_y + quadCoord.m_height) + offsetY); // y
        vertexBuffer->write(0.0f); // z
        vertexBuffer->write(((float)(glyphData.m_rect.m_x)) / textureWidth); // u
        vertexBuffer->write(((float)(glyphData.m_rect.m_y + glyphData.m_rect.m_height)) / textureHeight); // v

        penX += glyphData.m_advanceX;
    }

    OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_COMPOSER);

    // Rewind buffer to be able to read it from the start.
    vertexBuffer->rewind();

    const String& name = "text_2d";

    auto vertexStreamResource = MakeShared<VertexStreamResource>(
        std::move(vertexBuffer),
        VertexDeclaration::XYZ_UV,
        PrimitiveType::TriangleList,
        BufferUsage::Static,
        name);

    auto meshResourceDesc = MakeShared<MeshResource::Descriptor>();
    meshResourceDesc->m_vertexStreamResource = vertexStreamResource;
    meshResourceDesc->m_materialResource = materialResource;

    MeshResPtr meshResource = MakeShared<MeshResource>(
        name,
        meshResourceDesc);

    // Add to scene.
    MeshInstance::CreateParams params;
    params.m_pipelineHandle = materialResource->getPipelineHandle();
    params.m_renderPassId = materialResource->getRenderPassId();

    params.m_vertexBufferHandle = vertexStreamResource->getVertexBufferHandle();
    params.m_indexBufferHandle = vertexStreamResource->getIndexBufferHandle();
    MeshInstancePtr meshInstance(new MeshInstance(params));
    DBG_ONLY(meshInstance->setName(name));

    // Not necessary because the modelView matrix is not used by the shader.
    meshInstance->m_instance.m_modelView = Mat4::Identity();
    // Not necessary because normals not used by the shader.
    meshInstance->m_instance.m_transInvModelView = Mat4::Identity();
    // Not necessary because tangents not used by the shader.
    meshInstance->m_instance.m_transInvModel = Mat4::Identity();
    // MVP = VP because M = Identity for 2D rendering
    meshInstance->m_instance.m_modelViewProjection = getOrthoProjectionMatrix();

    m_worldScene->addMeshInstanceToRenderScene(meshInstance);
}

void Composer::renderQuadTextured2d(
    const Rect2D& quad,
    ConstTextureResPtr textureResource)
{
    if(isPaused())
        return;

    if(!textureResource->isLoaded())
        return;

    Quad2dTexturedCommand* rc =
        allocUICommand<Quad2dTexturedCommand>();
    rc->m_quad = quad;
    rc->m_textureResource = textureResource;
    commitUICommand();
}

void Composer::renderQuadColored2d(
    const Rect2D& quad,
    const Color& color)
{
    if(isPaused())
        return;

    //Quad2dColoredRenderCommand* rc =
    //    allocUICommand<Quad2dColoredRenderCommand>();
    //rc->m_quad = quad;
    //rc->m_color = color;
    //commitUICommand();
    //Quad2dColoredRenderCommand rc;
    //rc.m_quad = quad;
    //rc.m_color = color;
    //rc.execute();
}

void Composer::renderLinedColored2d(
    const Vec2i& p1,
    const Vec2i& p2,
    const Color& color,
    uint32 thickness)
{
    if(isPaused())
        return;

    Line2dColoredRenderCommand* rc =
        allocUICommand<Line2dColoredRenderCommand>();
    rc->m_p1 = p1;
    rc->m_p2 = p2;
    rc->m_color = color;
    rc->m_thickness = thickness;
    commitUICommand();
}

void Composer::syncEndFrame()
{
    m_renderer->publishScene();
}

void Composer::replay()
{
    m_worldScene->replay();
}

void Composer::projectOnScreen(uint32 iRenderScene, const Vec3& worldPoint, Vec2& screenPoint)
{
    const Mat4& viewMatrix = m_camera->getWorldToViewMatrix();

    Vec4 worldPointVec4 = Vec4(worldPoint[0], worldPoint[1], worldPoint[2], 1.0f);

    Vec4 transformed = m_projectionMatrix * viewMatrix * worldPointVec4;

    transformed[0] /= transformed[3];
    transformed[1] /= transformed[3];
    transformed[2] /= transformed[3];

    screenPoint[0] = (transformed[0] + 1.0f) / 2.0f * m_screenSize.m_width;
    screenPoint[1] = (-transformed[1] + 1.0f) / 2.0f * m_screenSize.m_height;
}

const Mat4& Composer::getOrthoProjectionMatrix() const
{
    return m_transform2dMatrix;
}

const Mat4& Composer::getWorldProjectionMatrix() const
{
    return m_projectionMatrix;
}

const Mat4& Composer::getViewMatrix() const
{
    auto* renderScene = m_renderer->getRenderScene();
    return renderScene->getViewMatrix();
}

RenderScene& Composer::getMainThreadScene()
{
    return *m_renderer->getRenderScene();
}

void Composer::clearMainThreadScene()
{
    m_renderer->clearRenderScenes();
}

uint32 Composer::getMainThreadSceneIndex() const
{
    return m_renderer->getMainThreadRenderSceneIndex();
}


void Composer::addMeshInstance(MeshInstancePtr meshInstance)
{
    auto* renderScene = m_renderer->getRenderScene();
    renderScene->addMeshInstance(meshInstance);
}

void Composer::removeMeshInstance(MeshInstancePtr meshInstance)
{
    auto* renderScene = m_renderer->getRenderScene();
    renderScene->removeMeshInstance(meshInstance);
}

void Composer::SDL_Attributes()
{
    m_renderer->SDL_Attributes();
}

uint32 Composer::SDL_GetWindowFlags() const
{
    return m_renderer->SDL_GetWindowFlags();
}

bool Composer::isReady() const
{
    return m_renderer->isReady();
}

bool Composer::isOk() const
{
    return m_renderer->isOk();
}

void Composer::commitUICommand()
{
    //RenderScene& uiScene = getMainThreadScene();
    //uiScene.commitCommand();
}

void Composer::createResources()
{
    const char* name = "fullscreen_quad";

    auto& resourceManager = ResourceManager::getInstance();
    auto material = resourceManager.getResource<MaterialResource>("materials/composition");
    OE_CHECK(material, OE_CHANNEL_COMPOSER);

    auto quadVerticesPtr = Renderer::createQuadBuffer(
        { .0f, .0f, 1.0f, 1.0f });

    // Create resources.
    auto vertexStream = MakeShared<VertexStreamResource>(
        std::move(quadVerticesPtr),
        VertexDeclaration::XYZ_UV,
        PrimitiveType::TriangleList,
        BufferUsage::Static,
        name);

    auto meshResourceDescPtr = MakeShared<MeshResource::Descriptor>();
    meshResourceDescPtr->m_materialResource = material;
    meshResourceDescPtr->m_vertexStreamResource = vertexStream;
    auto meshResource = MakeShared<MeshResource>(
        name,
        meshResourceDescPtr);
    resourceManager.addToPackage(meshResource, "engine");

    m_compositionMeshComponent = MeshComponentManager::getInstance().createComponent();
    m_compositionMeshComponent->setName("composer");
    m_compositionMeshComponent->setMeshResource(meshResource);
}

void Composer::destroyResources()
{
    if(m_compositionMeshComponent)
    {
        MeshComponentManager::getInstance().destroyComponent(m_compositionMeshComponent);
        m_compositionMeshComponent = nullptr;
    }
}

bool Composer::areResourcesLoaded() const
{
    if(m_compositionMeshComponent)
    {
        if(m_compositionMeshComponent->isLoaded())
        {
            return true;
        }
    }

    return false;
}

void Composer::ready()
{
    m_worldScene->addMeshComponent(m_compositionMeshComponent);
}

void Composer::addProp(Prop* prop)
{
    m_worldScene->addProp(prop);
}

} // namespace OE_Engine


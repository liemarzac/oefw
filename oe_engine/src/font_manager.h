#ifndef __OE_ENGINE_FONT_MANAGER_H__
#define __OE_ENGINE_FONT_MANAGER_H__

// External
#include <json/value.h>

// OE_Core
#include <map.h>
#include <singleton.h>
#include <std_smartptr_helpers.h>

// OE_Engine
#include "oe_engine.h"


namespace OE_Engine
{

class FontResource;

class OE_ENGINE_EXPORT FontManager : public OE_Core::Singleton<FontManager>
{
public:
    FontManager();
    ~FontManager();

    inline const Json::Value& getConfig() const
    {
        return m_config;
    }

private:
    typedef Map<String, SharedPtr<FontResource> > FontResourceMap;
    FontResourceMap m_resources;
    Json::Value m_config;
};

} // namespace OE_Engine

#endif // __OE_ENGINE_FONT_MANAGER_H__

#include "light.h"
#include "directional_light_component.h"


namespace OE_Engine
{

Light::Light()
{
    m_directionalComponent = new DirectionalLightComponent();
}

Light::~Light()
{
    delete m_directionalComponent;
}

const Vec3& Light::getDirection() const
{
    return m_directionalComponent->getDirection();
}

void Light::setDirection(const Vec3& direction)
{
    m_directionalComponent->setDirection(direction);
}

}
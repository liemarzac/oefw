#ifndef __OE_ENGINE_RENDER_COMMAND_H__
#define __OE_ENGINE_RENDER_COMMAND_H__

#include <oe_engine.h>

// OE_Core
#include <eigen_helpers.h>
#include <rect_2d.h>

// OE_Graphic
#include <graphic_resource_helpers.h>

// OE_Engine
#include "color.h"

namespace OE_Graphic
{
    class Driver;
}

namespace OE_Engine
{

struct OE_ENGINE_EXPORT UIRenderCommand
{
    virtual void execute() = 0;
    virtual size_t getSize() const = 0;

    static void addMeshResourceToScene(
        OE_Graphic::MeshResPtr meshResource
        DBG_STR_PARAM_ADD(name));
};

struct OE_ENGINE_EXPORT Quad2dColoredRenderCommand : public UIRenderCommand
{
    virtual void execute() override;
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    OE_Core::Rect2D m_quad;
    OE_Graphic::Color m_color;
};


struct OE_ENGINE_EXPORT Quad2dTexturedCommand : public UIRenderCommand
{
    virtual void execute() override;
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    OE_Core::Rect2D m_quad;
    OE_Graphic::ConstTextureResPtr m_textureResource;
};


struct OE_ENGINE_EXPORT Line2dColoredRenderCommand : UIRenderCommand
{
    virtual void execute() override;
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    Vec2i  m_p1;
    Vec2i  m_p2;
    OE_Graphic::Color m_color;
    uint32 m_thickness;
};


struct OE_ENGINE_EXPORT Text2dRenderCommand : public UIRenderCommand
{
    virtual void execute() override;
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    String m_text;
    String m_fontName;
    uint32 m_fontSize;
    uint32 m_x;
    uint32 m_y;
    OE_Graphic::HorizontalAnchor m_hAnchor;
    OE_Graphic::VerticalAnchor m_vAnchor;
};
}

#endif // __OE_ENGINE_RENDER_COMMAND_H__

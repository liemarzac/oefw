#ifndef __OE_ENGINE_KINEMATIC_COMPONENT_MANAGER_H__
#define __OE_ENGINE_KINEMATIC_COMPONENT_MANAGER_H__

#include "oe_engine.h"

#include "component_manager.h"
#include "kinematic_component.h"

namespace OE_Engine
{

DEFINE_ENGINE_COMPONENT_MANAGER(KinematicComponent)

} // OE_Engine

#endif // __OE_ENGINE_KINEMATIC_COMPONENT_MANAGER_H__

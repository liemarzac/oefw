#include "engine_private.h"

#include <renderer.h>

#include "driver_command.h"
#include "render_thread.h"
#include "composer.h"
#include "render_scene.h"
#include "stop_watch.h"
#include "thread.h"

namespace OE_Engine
{

void renderLoop(RenderThreadParams* params)
{
    Renderer& renderer = Renderer::getInstance();

    renderer.initFromRenderThread(*params->m_config);

    while(params->m_shouldRender)
    {
        if (renderer.preRender())
        {
            if (renderer.isReady())
            {
                renderer.render();
            }

            renderer.postRender();
        }
    }
}

}

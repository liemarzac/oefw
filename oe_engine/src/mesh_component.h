#ifndef __OE_ENGINE_MESH_COMPONENT_H__
#define __OE_ENGINE_MESH_COMPONENT_H__

#include "oe_engine.h"

// OE_Core
#include <array.h>

// OE_Graphic
#include <mesh_instance.h>
#include <graphic_resource_helpers.h>

// OE_Engine
#include "component.h"
#include "component_manager.h"
#include "task_group.h"


namespace OE_Engine
{

struct MeshComponentUpdateParams : public ComponentUpdateParams
{
    EIGEN_ALIGNED

    MeshComponentUpdateParams()
    {
        memset(this, 0, sizeof(*this));
    }

    Mat4 m_viewPerspectiveMatrix;
    Mat4 m_viewMatrix;
    Mat4 m_projectionMatrix;
    uint m_iScene;
};

class OE_ENGINE_EXPORT MeshComponent : public Component
{
    RTTI_DECLARATION

public:
    MeshComponent();

    virtual void setName(const String& name) override;

    void update(const MeshComponentUpdateParams& params);
    void setMeshResource(OE_Graphic::MeshResPtr meshResource);
    void setMeshResource(OE_Graphic::MeshResPtr meshResource, uint iRenderScene);
    void setPosition(const Vec3& position, uint iRenderScene);
    void setOrientation(const Mat4& orientation, uint iRenderScene);
    OE_Graphic::MeshInstancePtr getMeshInstance(uint iRenderScene);
    bool isLoaded() const;
    const Mat4& getObjectToWorldTransform(uint iRenderScene) const;
    void setObjectToWorldTransform(const Mat4& transform, uint iRenderScene);

private:
    Array<OE_Graphic::MeshInstancePtr, 2> m_meshInstances;
    OE_Graphic::MeshResPtr m_meshResource;
};

inline void MeshComponent::setPosition(const Vec3& position, uint iRenderScene)
{
    m_meshInstances[iRenderScene]->setPosition(position);
}

inline void MeshComponent::setOrientation(const Mat4& orientation, uint iRenderScene)
{
    m_meshInstances[iRenderScene]->setOrientation(orientation);
}

inline OE_Graphic::MeshInstancePtr MeshComponent::getMeshInstance(uint iRenderScene)
{
    return m_meshInstances[iRenderScene];
}

inline bool MeshComponent::isLoaded() const
{
    return m_meshResource && m_meshResource->isLoaded();
}

inline const Mat4& MeshComponent::getObjectToWorldTransform(uint iRenderScene) const
{
    return m_meshInstances[iRenderScene]->getObjectToWorldTransform();
}

inline void MeshComponent::setObjectToWorldTransform(const Mat4& transform, uint iRenderScene)
{
    m_meshInstances[iRenderScene]->setObjectToWorldTransform(transform);
}


} // namespace OE_Engine

#endif // __OE_ENGINE_MESH_COMPONENT_H__

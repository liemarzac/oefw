import os
import json

def insert_generated_content(file_path, tag, content):
    """
    Append generated content in a file between a tag.
    """
    content_in = []
    content_out = []
    with open(file_path, "r") as file_in:
        content_in = file_in.readlines()

    begin_tag_found = False
    end_tag_found = False
    success = False

    for line in content_in:
        if 'TAG_GENERATED_END' in line:
            if begin_tag_found and not end_tag_found:
                end_tag_found = True
                content_out.extend(content)
                success = True

        inside_tag = begin_tag_found and not end_tag_found
        if not inside_tag:
            content_out.append(line)

        if "TAG_GENERATED_BEGIN(" + tag + ")" in line:
            begin_tag_found = True

    if success:
        with(open(file_path, "w")) as file_out:
            file_out.writelines(content_out)


def append_function_params(content, params, num_spaces, append_type, append_handle):
    first_param = True
    for param in params:
        if 'is_handle' in param and param['is_handle'] and not append_handle:
            continue

        if not first_param:
            content.append(",\n")
        first_param = False
        spaces = [' '] * (num_spaces + 4)
        content.extend(spaces)
        if append_type:
            content.append(param['type'])
            if 'use_move' in param and param['use_move']:
                content.append("&&")
            content.append(" ")
        content.append(param['name'])


def generate_create_render_commands_renderer_h(command_descs):
    """
    Generate content for renderer.h.
    """
    content = []
    for desc in command_descs:
        func_return_val = "void"
        if 'resource_handle' in desc:
            func_return_val = desc['resource_handle']
        content.append("    " + func_return_val + " create" + desc["name"] + "(\n")
        if 'params' in desc:
            append_function_params(content, desc['params'], 4, append_type = True, append_handle = False)
            content.append("\n       DBG_STR_PARAM_ADD(name)")
        content.append(");\n\n")

    file_path = os.path.join("..", "oe_engine", "src", "renderer.h")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_renderer_cpp(command_descs):
    """
    Generate content for renderer.cpp.
    """
    content = []
    for desc in command_descs:
        func_return_val = "void"
        if 'resource_handle' in desc:
            func_return_val = desc['resource_handle']
        content.append(func_return_val + " Renderer::create" + desc['name'] + "(\n")
        if 'params' in desc:
            append_function_params(content, desc['params'], 0, append_type = True, append_handle = False)
            content.append("\n    DBG_STR_PARAM_ADD(name)")
        content.append(")\n")
        content.append("{\n")

        if 'resource_handle' in desc:
            content.append("    " + desc['resource_handle'] + " handle = " + "m_driver->create" + desc['name'] + "Handle(DBG_PARAM(name));\n\n")

        render_command_class_name = "Create" + desc['name'] + "RenderCommand"
        content.append("    " + render_command_class_name + "* rc =\n")
        content.append("        allocRenderCommand<" + render_command_class_name + ">(RenderGroup::World);\n")
        content.append("    rc->m_driver = m_driver;\n")
        if('params' in desc):
            for param in desc['params']:
                assign = param['name']
                if 'assign' in param:
                    assign = param['assign'] + "(" + param['name'] + ")" 
                content.append("    rc->m_" + param['name'] + " = " + assign + ";\n")
        content.append("    commitRenderCommand(RenderGroup::Screen);\n")
        content.append("\n")

        if 'resource_handle' in desc:
            content.append("    return handle;\n")

        content.append("}\n\n")

    file_path = os.path.join("..", "oe_engine", "src", "renderer.cpp")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_render_command_h(command_descs):
    """
    Generate content for render_command.h.
    """
    common = []
    common.append("    virtual void execute() override;\n")
    common.append("    virtual size_t getSize() const override\n")
    common.append("    {\n")
    common.append("        return sizeof(*this);\n")
    common.append("    }\n")

    content = []
    for desc in command_descs:
        struct_name = "Create" + desc['name'] + "RenderCommand"
        content.append("struct OE_ENGINE_EXPORT " + struct_name + " : public RenderCommand\n")
        content.append("{\n")
        content.append("    NO_COPY_DEFAULT_CONSTRUCTOR(" + struct_name + ")\n")
        content.extend(common)
        content.append("\n")
        content.append("    OE_Graphic::Driver* m_driver = nullptr;\n")
        if'params' in desc:
            for param in desc['params']:
                content.append("    " + param['type'] + " m_" + param['name'])
                if('init' in param):
                    content.append(" = " + param["init"])
                content.append(";\n")
        content.append("};\n\n")

    file_path = os.path.join("..", "oe_engine", "src", "render_command.h")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_render_command_cpp(command_descs):
    """
    Generate content for render_command.cpp.
    """
    content = []
    for desc in command_descs:
        content.append("void Create" + desc['name'] + "RenderCommand::execute()\n")
        content.append("{\n")
        content.append("    m_driver->create" + desc['name'] + "(\n")
        if'params' in desc:
            first_param = True
            for param in desc['params']:
                if not first_param:
                    content.append(",\n")
                first_param = False
                assign = "m_" + param['name']
                if 'assign' in param:
                    assign = param['assign'] + "(m_" + param['name'] + ")"
                content.append("        " + assign)
        content.append(");\n")
        content.append("}\n\n")

    file_path = os.path.join("..", "oe_engine", "src", "render_command.cpp")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_driver_h(command_descs):
    """
    Generate content for driver.h.
    """
    content = []
    for desc in command_descs:
        content.append("    void create" + desc['name'] + "(\n")
        if 'params' in desc:
            append_function_params(content, desc['params'], 4, append_type = True, append_handle = True)
        content.append(");\n\n")

    file_path = os.path.join("..", "oe_graphic", "src", "driver.h")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_driver_cpp(command_descs):
    """
    Generate content for driver.cpp.
    """
    content = []
    for desc in command_descs:
        content.append("void Driver::create" + desc['name'] + "(\n")
        if 'params' in desc:
            append_function_params(content, desc['params'], 4, append_type = True, append_handle = True)
        content.append(")\n")
        content.append("{\n")
        command_class_name = "Create" + desc['name'] + "Command"
        content.append("    " + command_class_name + "* rc = \n")
        content.append("        allocCreationCmd<" + command_class_name + ">();\n")
        content.append("\n")

        if 'params' in desc:
            for param in desc['params']:
                assign = param['name']
                if 'assign' in param:
                    assign = param['assign'] + "(" + param['name'] + ")"
                content.append("    rc->m_" + param['name'] + " = " + assign + ";\n")

        content.append("    commitCreationCmd();\n")

        content.append("}\n\n")

        file_path = os.path.join("..", "oe_graphic", "src", "driver.cpp")
        insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands_driver_command_h(command_descs):
    """
    Generate content for driver_command.h.
    """
    common = []
    common.append("    virtual void execute() override;\n")
    common.append("    virtual size_t getSize() const override\n")
    common.append("    {\n")
    common.append("        return sizeof(*this);\n")
    common.append("    }\n")

    content = []
    for desc in command_descs:
        struct_name = "Create" + desc['name'] + "Command"
        content.append("struct OE_GRAPHIC_EXPORT " + struct_name + " : public DriverCommand\n")
        content.append("{\n")
        content.append("    NO_COPY_DEFAULT_CONSTRUCTOR(" + struct_name + ")\n")
        content.extend(common)
        content.append("\n")
        if'params' in desc:
            for param in desc['params']:
                content.append("    " + param['type'] + " m_" + param['name'])
                if('init' in param):
                    content.append(" = " + param["init"])
                content.append(";\n")
        content.append("};\n\n")

    file_path = os.path.join("..", "oe_graphic", "src", "driver_command.h")
    insert_generated_content(file_path, "CreateResource", content)


def generate_create_render_commands(command_descs):
    #generate_create_render_commands_renderer_h(command_descs)
    #generate_create_render_commands_renderer_cpp(command_descs)
    generate_create_render_commands_render_command_h(command_descs)
    generate_create_render_commands_render_command_cpp(command_descs)
    generate_create_render_commands_driver_h(command_descs)
    generate_create_render_commands_driver_cpp(command_descs)
    generate_create_render_commands_driver_command_h(command_descs)


def main():
    input_json_file_path = "render_commands.json"
    with(open(input_json_file_path, "r")) as input_json_file:
        json_content = json.load(input_json_file)
        descs = json_content["resource_creation"]
        generate_create_render_commands(descs)


if __name__ == "__main__":
    main()
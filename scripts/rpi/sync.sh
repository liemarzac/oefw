#!/bin/bash

SCRIPTDIR=$(dirname $0)
ADDRESS=$1
CONFIG=$2
REMOTE=pi@$ADDRESS
BASEDIR=$SCRIPTDIR/../..

libs=("core" "graphic" "engine")

ssh $REMOTE 'mkdir -p ~/projects/oefw/lib'

for i in "${libs[@]}"
do
   rsync -av $BASEDIR/builds/rpi_$CONFIG/oe_$i/liboe_$i.so $REMOTE:~/projects/oefw/lib/liboe_$i.so
done

BIN_DIR=$BASEDIR/bin/rpi_$CONFIG
ssh $REMOTE "mkdir -p ~/projects/oefw/bin/rpi_$CONFIG"
rsync -a $BIN_DIR $REMOTE:~/projects/oefw/bin

rsync -a $BASEDIR/data $REMOTE:~/projects/oefw


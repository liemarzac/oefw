import os
import json
from optparse import OptionParser

class ClassType:
    base, component, component_manager, message = range(4)

def camel_case_to_lower_case_underscore(string):
    """
    Split string by upper case letters.
 
    F.e. useful to convert camel case strings to underscore separated ones.
 
    @return words (list)
    """
    words = []
    from_char_position = 0
    for current_char_position, char in enumerate(string):
        if char.isupper() and from_char_position < current_char_position:
            words.append(string[from_char_position:current_char_position].lower())
            from_char_position = current_char_position
    words.append(string[from_char_position:].lower())
    return '_'.join(words)


def make_header(class_name, library_folder, library_name, library_config, file_name, inherited_classname, class_type):
    with open(os.path.join(library_folder, "src", file_name + ".h"), "w") as file:
        filename_define = ''
        if 'namespace' in library_config:
            filename_define = "__" + library_config['namespace'].upper() + '_' + file_name.upper() + "_H__"
        else:
            filename_define = "__" + file_name.upper() + "_H__"
        file.write("#ifndef " + filename_define + "\n")
        file.write("#define " + filename_define + "\n")
        file.write("\n")

        if 'namespace' in library_config:
            file.write("#include \"" + library_config['namespace'].lower() + ".h\"\n")
            file.write("\n")

        if class_type is ClassType.component:
            file.write("#include \"component.h\"\n\n")
        elif class_type is ClassType.component_manager:
            file.write("#include \"component_manager.h\"\n")
            file.write("#include \"" + camel_case_to_lower_case_underscore(class_name.split('Manager')[0]) + ".h\"\n")
            file.write("\n")
        elif class_type is ClassType.message:
            file.write("#include \"message.h\"\n\n")

        if 'namespace' in library_config:
            file.write("namespace " + library_config['namespace'] + "\n")
            file.write("{\n")
            file.write("\n")

        # Write class
        if class_type is not ClassType.component_manager:
            file.write("class ")
            if 'export' in library_config:
                file.write(library_config['export'] + " ")
            file.write(class_name)
            if class_type is ClassType.component:
                file.write(" : public Component")
            elif class_type is ClassType.message:
                file.write(" : public Message")
            elif inherited_classname is not None:
                file.write(" : public " + inherited_classname)
            file.write("\n")
                
            file.write("{\n")
            
            has_macro = False
            if class_type is ClassType.component or class_type is ClassType.message:
                file.write("    RTTI_DECLARATION\n")
                has_macro = True

            if class_type is ClassType.message:
                file.write("    GET_SIZE_IMPL\n")
                has_macro = True
                
            if has_macro is True:
                file.write("\n")
            
            file.write("public:\n")

            if class_type is ClassType.component:
                file.write("EIGEN_MAKE_ALIGNED_OPERATOR_NEW\n");

            if class_type is ClassType.message:
                file.write("    " + class_name + "(void* sender);\n")
            else:
                file.write("    " + class_name + "();\n")

            if class_type is not ClassType.message:
                file.write("    virtual ~" + class_name + "();\n\n")

            if class_type is not ClassType.message:
                file.write("    " + class_name + "(const " + class_name + "& rhs) = delete;\n")
                file.write("    " + class_name + "& operator=(const " + class_name + "& rhs) = delete;\n")

            if class_type is ClassType.component:
                file.write("    void update(float deltaTime);\n\n")

            file.write("};\n")
        else:
            if library_name == "oe_engine":
                file.write("DEFINE_ENGINE_COMPONENT_MANAGER(" + class_name.split('Manager')[0] + ")\n")
            else:
                file.write("DEFINE_COMPONENT_MANAGER(" + class_name.split('Manager')[0] + ")\n")

        file.write("\n")
        if 'namespace' in library_config:
            file.write("} // " + library_config['namespace'] + "\n")
            file.write("\n")
        file.write("#endif // " + filename_define + "\n")

    add_to_cmake(library_folder, library_name, file_name + ".h")


def make_body(class_name, library_folder, library_name, library_config, file_name, class_type):
    with open(os.path.join(library_folder, "src", file_name + ".cpp"), "w") as file:
        file.write("#include \"" + file_name + ".h\"\n\n")

        if 'private_include' in library_config:
            file.write("#include \"" + library_config['private_include'] + "\"\n")
            file.write("\n")

        if class_type is ClassType.component_manager:
            file.write("#include \"engine.h\"\n")
            file.write("\n")

        if 'namespace' in library_config:
            file.write("namespace " + library_config['namespace'] + "\n")
            file.write("{\n")
            file.write("\n")
        
        has_macro = False
        if class_type is ClassType.component:
            file.write("RTTI_IMPL_PARENT(" + class_name + ", Component)\n")
            has_macro = True
        elif class_type is ClassType.message:
            file.write("RTTI_IMPL_PARENT(" + class_name + ", Message)\n")
            has_macro = True
            
        if has_macro is True:
            file.write("\n")

        if class_type is ClassType.message:
            file.write(class_name + "::" + class_name + "(void* sender) : \n\t:Message(sender)\n")
        else:
            file.write(class_name + "::" + class_name + "()\n")
            
        file.write("{\n")
        file.write("}\n\n")

        if class_type is not ClassType.message:
            file.write(class_name + "::~" + class_name + "()\n")
            file.write("{\n")
            file.write("}\n\n")

        if class_type is ClassType.component:
            file.write("void " + class_name + "::update(float deltaTime)\n")
            file.write("{\n")
            file.write("}\n\n")

        file.write("\n")
        if 'namespace' in library_config:
            file.write("} // " + library_config['namespace'] + "\n")
            file.write("\n")
    add_to_cmake(library_folder, library_name, file_name + ".cpp")


def add_to_cmake(folder, library_name, file_name):
    before_data = ""
    after_data = ""
    files = []  
    cmake_lists_file_path = os.path.join(folder, "CMakeLists.txt")
    cmake_lists_tmp_file_path = os.path.join(folder, "CMakeListsTmp.txt")
    already_exist = False
    with open(cmake_lists_file_path, "r") as cmake_file:
        file_list_marker = ""
        file_list_marker = 'set(' + library_name.upper() + '_SOURCES'
        
        # Find marker.
        line = cmake_file.readline()
        while line != '':
            before_data += line
            line = line.strip()
            if line.replace(" ", "") == file_list_marker:
                break
            line = cmake_file.readline()

        # Find existing files.
        line = cmake_file.readline()
        while line != '':
            if ")" in line:
                break
            line = line.strip()
            line = line.replace(" ", "")
            if line != "" and "#" not in line:
                files.append(line)
            line = cmake_file.readline()
        while line != '':
            after_data += line
            line = cmake_file.readline()
        
        # Add the new file in the list and sort.
        new_file_name = "src/" + file_name
        if new_file_name in files:
            already_exist = True

        if not already_exist:
            files.append("src/" + file_name)
            files.sort()

    if not already_exist:
        # Create temporary cmake.
        with open(cmake_lists_tmp_file_path, "w") as cmake_file_tmp:
            cmake_file_tmp.write(before_data)
            for file in files:
                cmake_file_tmp.write("    " + file + "\n")
            cmake_file_tmp.write(after_data)

        # Swap files.
        os.remove(cmake_lists_file_path)
        os.rename(cmake_lists_tmp_file_path, cmake_lists_file_path)
    
    
def main():
    parser = OptionParser()
    parser.add_option("-c", "--classname", dest="classname",
                        help="class name to create")
    parser.add_option("-f", "--filename", dest="filename",
                        help="h and cpp file name to create")
    parser.add_option("-l", "--library", dest="library",
                        help="library where class will be created")
    parser.add_option("-i", "--inherit", dest="inherit",
                        help="class inheriting from")
    parser.add_option("", "--component_manager",
                        action="store_true", dest="component_manager",
                        help="create component and its associated manager")
    parser.add_option("", "--message",
                        action="store_true", dest="message",
                        help="create message")

    (options, args) = parser.parse_args()
    
    if options.classname is None:
        parser.error("--classname is required")
        
    if options.library is None:
        parser.error("--library is required")

    # Find folder associated with the library.
    library_folder = os.path.join('..', options.library)
    if not os.path.exists(library_folder):
        raise ValueError('library ' + options.library + ' cannot be found')

    class_name = options.classname

    file_name = ''
    if options.filename is None:
        file_name = camel_case_to_lower_case_underscore(options.classname)
    else:
        file_name = options.filename

    # Import library config.
    libraries = []
    with open('libraries.json') as data_file:
        libraries = json.load(data_file)

    if not options.library in libraries:
        raise ValueError('library ' + options.library + ' is not part of libraries.json')

    if options.component_manager is True:
        # Make component class.
        component_class_name = class_name + 'Component'
        file_name = camel_case_to_lower_case_underscore(component_class_name)
        make_header(component_class_name, library_folder, options.library, libraries[options.library], file_name, options.inherit, ClassType.component)
        make_body(component_class_name, library_folder, options.library, libraries[options.library], file_name, ClassType.component)

        # Make component manager class.
        component_manager_class_name = class_name + 'ComponentManager'
        file_name = camel_case_to_lower_case_underscore(component_manager_class_name)
        make_header(component_manager_class_name, library_folder, options.library, libraries[options.library], file_name, options.inherit, ClassType.component_manager)
        make_body(component_manager_class_name, library_folder, options.library, libraries[options.library], file_name, ClassType.component_manager)
    elif options.message is True:
        message_class_name = class_name + 'Msg'
        file_name = camel_case_to_lower_case_underscore(message_class_name)
        make_header(message_class_name, library_folder, options.library, libraries[options.library], file_name, options.inherit, ClassType.message)
        make_body(message_class_name, library_folder, options.library, libraries[options.library], file_name, ClassType.message)    
    else:
        make_header(class_name, library_folder, options.library, libraries[options.library], file_name, options.inherit, ClassType.base)
        make_body(class_name, library_folder, options.library, libraries[options.library], file_name, ClassType.base)

if __name__ == "__main__":
    main()
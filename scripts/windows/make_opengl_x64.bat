@echo off
cd ..\..\

if not exist "builds" (
    mkdir "builds"
)

cd builds

if not exist "opengl_x64" (
    mkdir "opengl_x64"
)

cd opengl_x64

cmake ..\.. -DARCHITECTURE="x64" -DRENDERER="OpenGL 3.2" -G "Visual Studio 12 2013 Win64"

pause
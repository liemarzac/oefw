@echo off
cd ..\..\

if not exist "builds" (
    mkdir "builds"
)

cd builds

if not exist "directx_x64" (
    mkdir "directx_x64"
)

cd directx_x64

cmake ..\.. -DARCHITECTURE="x64" -DRENDERER="DirectX 9" -G "Visual Studio 12 2013 Win64"

pause
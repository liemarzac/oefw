import os
import math
import json
from progressbar import ProgressBar, Bar, Percentage
from optparse import OptionParser
#from enum import Enum
from struct import *

max_normal_angle_in_deg = 45

def rad_to_deg(angle):
    return angle * 180.0 / math.pi

    
def deg_to_rad(angle):
    return angle * math.pi / 180.0

    
def vector_len(vector):
    return math.sqrt((vector[0] * vector[0]) + (vector[1] * vector[1]) + (vector[2] * vector[2]))


def vector_dot(vector_a, vector_b):
    return (vector_a[0] * vector_b[0]) + (vector_a[1] * vector_b[1]) + (vector_a[2] * vector_b[2])


def vector_normalize(vector):
    len = vector_len(vector)
    return [vector[0] / len, vector[1] / len, vector[2] / len]


def angle_between_vectors(vector_a, vector_b):
    dot = vector_dot(vector_a, vector_b)
    len_a = vector_len(vector_a)
    len_b = vector_len(vector_b)
    cos_theta = dot / (len_a * len_b)
    cos_theta = min(1.0,max(cos_theta,-1.0))
    return math.acos(cos_theta)

    
# helper to calculate average normals
class AvgNormal:
    def __init__(self):
        self.normal = [0, 0, 0]
        self.n = 0
        
    def __init__(self, normal):
        self.normal = normal
        self.n = 1

    def __repr__(self):
        return 'n=' + str(self.n) + ' p=' + str(self.normal)

    def add_normal(self, normal):
        for i in range(3):
            self.normal[i] = ((self.normal[i] * self.n) + normal[i]) / (self.n + 1)
        self.normal = vector_normalize(self.normal)
        self.n = self.n + 1


class PosUVsAttributes:
    def __init__(self, position_id, uvs_id):
        self.position_id = position_id
        self.uvs_id = uvs_id

    def __eq__(self, other):
        return self.position_id == other.position_id \
            and self.uvs_id == other.uvs_id

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.position_id, self.uvs_id))

    def __repr__(self):
        return 'pos_id=' + str(self.position_id) + '; uvs_id=' + str(self.uvs_id)


class VertexUniqueNormal:
    def __init__(self, normal, normal_id):
        self.avg_normal = AvgNormal(normal)
        self.normal_ids = [normal_id]
        self.vertex_index = -1

    def __repr__(self):
        return 'avg_normal=' + str(self.avg_normal) + \
            '; normal_ids=' + str(self.normal_ids) + \
            '; vertex_index=' + str(self.vertex_index)

    def add_normal(self, normal, normal_id):
        self.avg_normal.add_normal(normal)
        self.normal_ids.append(normal_id)


# describes vertices with the same position and UV but they can have multiple normals
class VertexUniquePosUV:
    def __init__(self, normal, normal_id):
        self.unique_normals = [VertexUniqueNormal(normal, normal_id)]
 
    def add_normal(self, normal, normal_id):
        has_found_close_normal = False
        
        for unique_normal in self.unique_normals:
            angle = angle_between_vectors(normal, unique_normal.avg_normal.normal)
            if(angle < deg_to_rad(max_normal_angle_in_deg)):
                unique_normal.add_normal(normal, normal_id)
                has_found_close_normal = True
                break

        if not has_found_close_normal:
            # normals directions diverge too much, so create a new vertex
            self.unique_normals.append(VertexUniqueNormal(normal, normal_id))

    def find_vertex_index(self, target):
        for unique_normal in self.unique_normals:
            if target in unique_normal.normal_ids:
                return unique_normal.vertex_index
        return -1
        
    def has_normal_id(self, target):
        for unique_normal in self.unique_normals:
            if target in unique_normal.normal_ids:
                return True
        return False

    def __repr__(self):
        repr = ''
        for unique_normal in self.unique_normals:
            repr += str(unique_normal) + '\n'
        return repr


# matches enum in oe_graphic/src/graphics_private.h
class VertexFormat:
    xyzw_uv, xyzw_colo, xyz_uv_normal, xyz_normal, xyz = range(5)

class StreamExporter:
    def __init__(self, submesh_name, export_path, create_indices):
        self.positions = []
        self.uvs = []
        self.normals = []
        self.faces = []
        self.create_indices = create_indices
        self.has_uvs = False
        self.has_normals = False
        self.export_path = export_path
        self.submesh_name = submesh_name

    def export(self, is_verbose, big_endian):
        print '\nGenerating ' + self.export_path + os.sep + self.submesh_name + '.vstream ...\n'
        print 'Create indices=' + str(self.create_indices)
        print 'Has UVs=' + str(self.has_uvs)
        print 'Has normals=' + str(self.has_normals)

        # create export folder if it does not exist.
        if not os.path.isdir(self.export_path):
            print 'Create export folders'
            os.makedirs(self.export_path)

        with open(os.path.join(self.export_path, self.submesh_name + '.vstream' ), "wb") as mesh_file:
            normal_attrib_index = -1
            if self.has_normals:
                if self.has_uvs:
                    normal_attrib_index = 2
                else:
                    normal_attrib_index = 1

            # average normals
            num_vertices = 0
            vertex_map = {}
            if self.create_indices:
                for face_attributes in self.faces:
                    for vertex_attributes in face_attributes:
                        key = None
                        if self.has_uvs:
                            key = PosUVsAttributes(vertex_attributes[0], vertex_attributes[1])
                        else:
                            key = PosUVsAttributes(vertex_attributes[0], -1)

                        normal = self.normals[vertex_attributes[normal_attrib_index] - 1]
                        if key in vertex_map:
                            if not vertex_map[key].has_normal_id(vertex_attributes[normal_attrib_index]):
                                vertex_map[key].add_normal(normal, vertex_attributes[normal_attrib_index])
                        else:
                            vertex_map[key] = VertexUniquePosUV(normal, vertex_attributes[normal_attrib_index])
                
                unique_pos_uv_verts = vertex_map.values()
                for unique_pos_uv_vert in unique_pos_uv_verts:
                    num_vertices += len(unique_pos_uv_vert.unique_normals)
                
                print 'Num obj vertices=' + str(len(self.positions)) + \
                    '; Num unique vertices=' + str(num_vertices) + \
                    '; Ratio=%.2f' % (num_vertices/(float(len(self.positions))))

            else:
                num_vertices = len(self.faces) * 3

            vertex_format = 0
            if self.has_normals and self.has_uvs:
                vertex_format = VertexFormat.xyz_uv_normal
                print 'Vertex format=xyz_uv_normal'
            elif self.has_normals:
                vertex_format = VertexFormat.xyz_normal
                print 'Vertex format=xyz_normal'
            elif self.has_uvs:
                vertex_format = VertexFormat.xyz_uv
                print 'Vertex format=xyz_uv'
            else:
                vertex_format = VertexFormat.xyz
                print 'Vertex format=xyz'

            print 'Num vertices=' + str(num_vertices)

            # write vertex format
            pack_type = '>B' if big_endian else 'B'
            mesh_file.write(pack(pack_type, vertex_format))

            # write number of vertices
            pack_type = '>I' if big_endian else 'I'
            mesh_file.write(pack(pack_type, num_vertices))

            # create interleaved vertices data (position, uv, normal)
            if self.create_indices:
                vertex_index = 0
                for key in vertex_map:
                    unique_pos_uv_vert = vertex_map[key]
                    for unique_normal in unique_pos_uv_vert.unique_normals:
                        # cache vertex index
                        unique_normal.vertex_index = vertex_index

                        # write positions:
                        position = self.positions[key.position_id - 1]
                        pack_type = '>fff' if big_endian else 'fff'
                        byte_array = pack(pack_type, position[0], position[1], position[2])
                        mesh_file.write(byte_array)

                        # write uvs
                        if self.has_uvs:
                            uv = self.uvs[key.uvs_id - 1]
                            pack_type = '>ff' if big_endian else 'ff'
                            byte_array = pack(pack_type, uv[0], uv[1])
                            mesh_file.write(byte_array)

                        # write normals
                        if self.has_normals:
                            normal = unique_normal.avg_normal.normal
                            pack_type = '>fff' if big_endian else 'fff'
                            byte_array = pack(pack_type, normal[0], normal[1], normal[2])
                            mesh_file.write(byte_array)
                            
                        vertex_index = vertex_index + 1

                # now that vertex has received their index de bug them is requested
                if is_verbose:
                    for key in vertex_map.keys():
                        print "key=" + str(key)
                        print "value=" + str(vertex_map[key])
                        
                # write number of indices
                num_indices = len(self.faces) * 3
                mesh_file.write(pack('I', num_indices))

                indice_packing_type = '>H' if big_endian else 'H'
                indice_packing_type_str = '16 bit'
                if num_indices > 65536:
                    indice_packing_type = '>I' if big_endian else 'I'
                    indice_packing_type_str = '32 bit'

                print 'Num indices=' + str(num_indices)
                print 'Index format=' + indice_packing_type_str
                    
                # write indices
                num_found_indices = 0
                face_index = 0
                print 'Writing indices...'
                pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=len(self.faces)).start()
                for face_attributes in self.faces:
                    pbar.update(face_index)
                    for vertex_attributes in face_attributes:
                        vertex_index = -1
                        key = None
                        if self.has_uvs:
                            key = PosUVsAttributes(vertex_attributes[0], vertex_attributes[1])
                        else:
                            key = PosUVsAttributes(vertex_attributes[0], -1)

                        vertex_pos_uvs = vertex_map[key]
                        vertex_index = -1
                        if self.has_normals:
                            vertex_index = vertex_pos_uvs.find_vertex_index(vertex_attributes[normal_attrib_index])
                            if vertex_index == -1:
                                print 'error: normal id not found'
                                                  
                        if vertex_index != -1:
                            mesh_file.write(pack(indice_packing_type, vertex_index))
                        else:
                            print 'error: vertex not found'
                    face_index += 1
                pbar.finish()

            else:
                for face_attributes in self.faces:
                    for vertex_attributes in face_attributes:
                        # write positions
                        position = self.positions[vertex_attributes[0] - 1]
                        pack_type = '>fff' if big_endian else 'fff'
                        byte_array = pack('fff', position[0], position[1], position[2])
                        mesh_file.write(byte_array)
                                                    
                        # write uvs
                        if self.has_uvs:
                            uv = self.uvs[vertex_attributes[1] - 1]
                            pack_type = '>ff' if big_endian else 'ff'
                            byte_array = pack(pack_type, uv[0], uv[1])
                            mesh_file.write(byte_array)

                        # write normals
                        if self.has_normals:
                            normal = self.normals[vertex_attributes[normal_attrib_index] - 1]
                            pack_type = '>fff' if big_endian else 'fff'
                            byte_array = pack(pack_type, normal[0], normal[1], normal[2])
                            mesh_file.write(byte_array)

                # write number of indices
                pack_type = '>I' if big_endian else 'I'
                mesh_file.write(pack(pack_type, 0))


class ObjParser:
    def __init__(self):
        self.stream_exporter = None
        
    def export(self, object_path, export_path, create_indices, is_verbose, big_endian):
        submesh_name = ""
        positions_offset = 0
        uvs_offset = 0
        normals_offset = 0
        with open(object_path, "r") as object_file:
            for line in object_file:
                line = line.rstrip()
                tokens = line.split(' ')

                if tokens[0] == 'o':
                    # new submesh
                    if self.stream_exporter is not None:
                        self.stream_exporter.export(is_verbose, big_endian)
                        positions_offset += len(self.stream_exporter.positions)
                        uvs_offset += len(self.stream_exporter.uvs)
                        normals_offset += len(self.stream_exporter.normals)

                    submesh_name = tokens[1].lower()
                    self.stream_exporter = StreamExporter(submesh_name, export_path, create_indices)

                if tokens[0] == 'v':
                    self.stream_exporter.positions.append([float(numeric_string) for numeric_string in tokens[1:]])
                elif tokens[0] == 'vt':
                    self.stream_exporter.has_uvs = True
                    self.stream_exporter.uvs.append([1.0 - float(tokens[1]), 1.0 - float(tokens[2])])
                elif tokens[0] == 'vn':
                    self.stream_exporter.has_normals = True
                    self.stream_exporter.normals.append([float(numeric_string) for numeric_string in tokens[1:]])
                elif tokens[0] == 'f':
                    face = tokens[1:]
                    face_attributes = []
                    for face_attributes_str in face:
                        vertex_attibutes = face_attributes_str.split('/')
                        if self.stream_exporter.has_uvs and self.stream_exporter.has_normals:
                            face_attributes.append([int(vertex_attibutes[0]) - positions_offset, int(vertex_attibutes[1]) - uvs_offset, int(vertex_attibutes[2]) - normals_offset])
                        elif self.stream_exporter.has_normals:
                            face_attributes.append([int(vertex_attibutes[0]) - positions_offset, int(vertex_attibutes[2]) - normals_offset])
                        elif self.stream_exporter.has_uvs:
                            face_attributes.append([int(vertex_attibutes[0]) - positions_offset, int(vertex_attibutes[1]) - uvs_offset])
                        else:
                            face_attributes.append([int(vertex_attibutes[0])])
                                    
                    self.stream_exporter.faces.append(face_attributes)

            # process last submesh
            if self.stream_exporter is not None:
                self.stream_exporter.export(is_verbose, big_endian)


def main():
    parser = OptionParser()
    parser.add_option("-p", "--object_path", dest="object_path",
                        help="object name to export")
    parser.add_option("-o", "--export_path", dest="export_path", default="",
                        help="path to export")
    parser.add_option("-i", "--no_indices", action="store_true", dest="no_indices", default=False,
                        help="no indices")
    parser.add_option("-v", "--verbose", action="store_true", dest="is_verbose", default=False,
                        help="verbose")
    parser.add_option("-b", "--big_endian", action="store_true", dest="big_endian", default=False,
                        help="verbose")

    (options, args) = parser.parse_args()
    
    if options.object_path is None:
        parser.error("--object_path is required")
        
    if options.export_path is "":
        last_separator = options.object_path.rfind(os.sep)
        if last_separator != -1:
            options.export_path=options.object_path[:options.object_path.rfind(os.sep)]
        else:
            options.export_path=os.path.dirname(__file__)

    create_indices = not options.no_indices
    print 'object path: ' + options.object_path
    print 'export path: ' + options.export_path
    print 'create indices: ' + str(create_indices)
    print 'endianness: ' + "big endian" if options.big_endian else "little endian"
    print '\n'
    
    obj_parser = ObjParser()
    obj_parser.export(options.object_path, options.export_path, create_indices, options.is_verbose, options.big_endian)


if __name__ == "__main__":
    main()

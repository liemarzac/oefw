#include "runnable.h"

namespace OE_Core
{

Runnable::Runnable() :
    m_iTask(OE_MAX_UINT32),
    m_iWorker(OE_MAX_UINT32)
{
}

Runnable::~Runnable()
{
}

Runnable::Runnable(const Runnable& rhs)
{
    *this = rhs;
}

Runnable& Runnable::operator=(const Runnable& rhs)
{
    m_iTask = rhs.m_iTask;
    m_iWorker = rhs.m_iWorker;
    return *this;
}

void Runnable::addFinishedCallback(SharedPtr<Functor> callback)
{
    OE_CHECK(callback, OE_CHANNEL_CORE);
    OE_CHECK_MSG(callback->getType() == CallbackType::Once, OE_CHANNEL_CORE, "Only callback called once are accepted." );
    m_onFinishedCallbacks.addCallback(callback);
}

void Runnable::onFinished()
{
    // All the following code must be thread-safe
    // because this is called from a worker thread.

    // Functor groups are thread-safe.
    m_onFinishedCallbacks.callback();
}

}

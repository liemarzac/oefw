#ifndef __OE_CORE_PACKAGE_RESOURCE_H__
#define __OE_CORE_PACKAGE_RESOURCE_H__

// External
#include <json/value.h>

#include "oe_core.h"

// OE_Core
#include "resource.h"
#include "runnable.h"

namespace OE_Core
{

class OE_CORE_EXPORT PackageResource : public OE_Core::Resource
{
    friend class PackageResourceLoadTask;
    RTTI_DECLARATION
public:
    PackageResource(const String& name);
    virtual ~PackageResource();

    virtual void loadImpl() override;

    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "package_resource";
    }

    void addResource(DependencyCallback dependencyCallback, SharedPtr<Resource> resource);

private:
    PackageResource();
    PackageResource& operator=(const PackageResource& rhs);
    PackageResource(PackageResource& rhs);

    Json::Value m_packageContent;
    bool m_isPackageContentReady;
    std::mutex m_mutex;
};

class PackageResourceLoadTask : public OE_Core::Runnable
{
public:
    PackageResourceLoadTask(SharedPtr<PackageResource> resource);
    virtual void run() override;

private:
    PackageResourceLoadTask();
    PackageResourceLoadTask& operator=(const PackageResourceLoadTask& rhs);
    PackageResourceLoadTask(PackageResourceLoadTask& rhs);

    SharedPtr<PackageResource> m_resource;
};

} // namespace OE_Core

#endif // __OE_CORE_PACKAGE_RESOURCE_H__

#include "serializable.h"

namespace OE_Core
{

void serialize_impl(Buffer& buffer, const String& value, const String*)
{
    const uint32_t size = (uint32_t)value.size();
    serialize(buffer, size);

    OE_CHECK_MSG(buffer.m_offset + size <= buffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while serializing");
    memcpy(buffer.m_rawData + buffer.m_offset, value.c_str(), size);
    buffer.m_offset += size;
}

void deserialize_impl(Buffer& buffer, String& value, const String*)
{
    uint32_t size;
    deserialize(buffer, size);

    value.resize(size);

    OE_CHECK_MSG(buffer.m_offset + size <= buffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while deserializing");
    memcpy(&value[0], buffer.m_rawData + buffer.m_offset, size);
    buffer.m_offset += size;
}

size_t calculateSerializedSize_impl(const String& value, const String*)
{
    return sizeof(uint32_t) + value.size();
}

void serialize_impl(Buffer& toBuffer, const Buffer& fromBuffer, const Buffer*)
{
    const uint32_t size = (uint32_t)fromBuffer.m_size;
    serialize(toBuffer, size);

    OE_CHECK_MSG(toBuffer.m_offset + size <= toBuffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while serializing");
    memcpy(toBuffer.m_rawData + toBuffer.m_offset, fromBuffer.m_rawData, size);
    toBuffer.m_offset += size;
}

void deserialize_impl(Buffer& fromBuffer, Buffer& toBuffer, const Buffer*)
{
    uint32_t size;
    deserialize(fromBuffer, size);

    if(toBuffer.m_rawData)
        free(toBuffer.m_rawData);

    toBuffer.m_rawData = (byte*)malloc(size);
    toBuffer.m_size = size;

    OE_CHECK_MSG(fromBuffer.m_offset + size <= fromBuffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while deserializing");
    memcpy(toBuffer.m_rawData, fromBuffer.m_rawData + fromBuffer.m_offset, size);
    fromBuffer.m_offset += size;
}

size_t calculateSerializedSize_impl(const Buffer& buffer, const Buffer*)
{
    return sizeof(uint32_t) + buffer.m_size;
}

} // namespace OE_Core

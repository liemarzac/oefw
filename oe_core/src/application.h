#ifndef __OE_CORE_APPLICATION_H__
#define __OE_CORE_APPLICATION_H__

#include "oe_core.h"

#include "oe_string.h"

// External
#include <functional>

namespace OE_Core
{

extern std::function<void(String&)> g_getApplicationNameFunction;
extern String g_exeFolderPath;

void OE_CORE_EXPORT appInit();
void OE_CORE_EXPORT appSetExePath(const String& path);
void OE_CORE_EXPORT appGetName(String& appName);
void OE_CORE_EXPORT appSetGetNameFunction(std::function<void(String&)> function);

}

#endif // __OE_CORE_APPLICATION_H__

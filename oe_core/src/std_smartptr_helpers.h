#ifndef __OE_CORE_STD_SMARTPTR_HELPERS_H__
#define __OE_CORE_STD_SMARTPTR_HELPERS_H__

#include <memory>

template<typename T>
using SharedPtr = std::shared_ptr< T >;

template<typename T>
using WeakPtr = std::weak_ptr< T >;

template<typename T, typename U = std::default_delete<T> >
using UniquePtr = std::unique_ptr< T, U >;

template<typename T>
SharedPtr<const T> sharedConstCast(SharedPtr<T> sharedPtr)
{
    return SharedPtr<const T>(sharedPtr);
}

#define MakeShared std::make_shared
#define MakeUnique std::make_unique

#endif // __OE_CORE_STD_SMARTPTR_HELPERS_H__
#ifndef __OE_CORE_MATH_H__
#define __OE_CORE_MATH_H__

namespace OE_Core
{

#define PI 3.14159265358979323846
#define PI_OVER_TWO 1.57079632679489661923
#define TWO_PI (2 * PI) 
#define EPSILON std::numeric_limits<float>::epsilon()
#define EPSILON_D std::numeric_limits<double>::epsilon()
#define INFINITY_D std::numeric_limits<double>::infinity()

template<typename T>
T degToRad(T degrees)
{
    return (T)(degrees * PI / 180.0);
}

template<typename T>
T radToDeg(T rad)
{
    return (T)(180.0 * rad / PI);
}

template<typename T>
T min(T a, T b)
{
    return a < b ? a : b;
}

template<typename T>
T max(T a, T b)
{
    return a > b ? a : b;
}

}

#endif // __OE_CORE_MATH_H__

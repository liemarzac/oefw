#ifndef __OE_CORE_ARRAY_H__
#define __OE_CORE_ARRAY_H__

#include <array>

template <typename T, size_t size>
using Array = std::array<T, size>;

#endif // __OE_CORE_ARRAY_H__

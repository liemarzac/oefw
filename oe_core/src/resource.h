#ifndef __OE_CORE_RESOURCE_H__
#define __OE_CORE_RESOURCE_H__

#include "oe_core.h"

// External
#include <atomic>

// OE_Core
#include "callback.h"
#include "hash.h"
#include "rtti.h"
#include "runnable.h"

#if _DEBUG
#include "stop_watch.h"
#endif

namespace OE_Core
{

#define OE_CHANNEL_RESOURCE "Resource"

class OE_CORE_EXPORT Resource : public std::enable_shared_from_this<Resource>
{
    RTTI_DECLARATION

public:

    struct DependencyCallbackParams
    {
        Hash m_typeHash;
        String m_name;
        SharedPtr<Resource> m_explicitResource;
    };

    using DependencyCallback = std::function<SharedPtr<Resource>(const DependencyCallbackParams& params)>;

    enum class State
    {
        Unloaded,
        Error,
        Loading,
        Loaded,
    };

    Resource(const String& name);
    virtual ~Resource();

    virtual void loadImpl() = 0;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback){}
    virtual const char* getType() const = 0;

    const String& getName() const;
    uint getHash() const;

    State getStateEx() const;
    State getState() const;
    bool isLoaded() const;
    void setState(State state);
    void load();
    void updateLoading(DependencyCallback dependencyCallback);
    void onLoaded();
    CallbackEntry addLoadedCallback(SharedPtr<Functor> callback);
    void removeLoadedCallback(CallbackEntry entry);
    void getDependencies(Vector<SharedPtr<Resource>>& dependencies);

protected:
    void updateLoadingInternal(DependencyCallback dependencyCallback);
    State getStateInternal() const;
    void loadInternal();
    SharedPtr<Resource> addDependency(DependencyCallback callback, Hash typeHash, const String& name);
    SharedPtr<Resource> getDependency(Hash typeHash, const String& name) const;

    template<typename ResourceType>
    SharedPtr<ResourceType> addDependency(DependencyCallback callback, const String& name)
    {
        // Get or create the resource.
        SharedPtr<Resource> baseResource = addDependency(callback, ResourceType::getTypeHash(), name);

        // Downcast to the requested resource type.
        SharedPtr<ResourceType> typeResource = std::static_pointer_cast<ResourceType>(baseResource);

        return typeResource;
    }

    template<typename ResourceType>
    SharedPtr<ResourceType> addDependency(DependencyCallback callback, SharedPtr<ResourceType> resource)
    {
        DependencyCallbackParams params;
        params.m_explicitResource = resource;

        // Register resource using the callback.
        callback(params);

        // Add the dependency is the list.
        Dependency d;
        d.m_resource = resource;
        d.m_onLoadedCallback = resource->addLoadedCallback(MakeFunctor(
            &Resource::onDependencyLoaded, this, CallbackType::Permanent));

        OE_CHECK(!isContained(m_dependencies, d), OE_CHANNEL_RESOURCE);
        m_dependencies.push_back(d);

        return resource;
    }


    std::atomic<State> m_state;
    FunctorGroup m_onLoadedCallbacks;
    uint32 m_hash;

#if _DEBUG
    OE_Core::StopWatch m_loadTimer;
#endif

private:
    Resource();
    Resource& operator=(const Resource& rhs);
    Resource(Resource& rhs);

    void onDependencyLoaded();

    String m_name;

    struct Dependency
    {
        bool operator==(const Dependency& rhs) const
        {
            return m_resource == rhs.m_resource;
        }

        CallbackEntry m_onLoadedCallback;
        SharedPtr<Resource> m_resource;
    };

    mutable Mutex m_dependenciesSync;
    Vector<Dependency> m_dependencies;
};

inline bool Resource::isLoaded() const
{
    return getState() == State::Loaded;
}

inline uint Resource::getHash() const
{
    return m_hash;
}

inline const String& Resource::getName() const
{
    return m_name;
}

template<typename Type>
struct ResourcePath
{
    String m_path;
    Type m_resource;
};

// Used by shared pointer to delete a resource.
void OE_CORE_EXPORT resourceDeleter(Resource* resource);

} // namespace OE_Core

#endif // __OE_CORE_RESOURCE_H__

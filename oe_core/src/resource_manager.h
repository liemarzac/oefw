#ifndef __OE_CORE_RESOURCE_MANAGER_H__
#define __OE_CORE_RESOURCE_MANAGER_H__

#include "core_private.h"
#include "factory.h"
#include "singleton.h"
#include "resource.h"

#include <functional>
#include <memory>

namespace OE_Core
{

class Resource;

class OE_CORE_EXPORT ResourceManager : 
    public Factory<SharedPtr<Resource>, const String&>,
    public Singleton<ResourceManager>
{
    typedef std::weak_ptr<Resource> WeakResource;
    typedef SharedPtr<Resource> SharedResource;
    typedef Map<Hash, WeakResource> ResourceMap;
    typedef Map<Hash, ResourceMap> ResourceTypeMap;
    typedef Vector<WeakResource> WeakResourceArray;
    typedef Vector<SharedResource> SharedResourceArray;

public:
    ResourceManager();

    inline bool isLoading();

    SharedResource getResource(Hash type, const String& name);
    SharedResource loadPackage(Hash type, const String& name);
    void addToPackage(SharedResource resource, const String& packageName);
    void unloadPackage(const String& name);
    bool isPackageLoaded(const String& name) const;
    SharedResource createResource(Hash type, const String& name);
    SharedResource createDependency(const Resource::DependencyCallbackParams& params);

    void update();
    void flushLoading();
    void iterateResources(std::function<void(SharedPtr<Resource>)> callback);
    void logResources();
    void scheduleResourceLoad(SharedPtr<Resource> resourceToLoad);

    template<typename ResourceType>
    SharedPtr<ResourceType> getResource(const String& name)
    {
        LockGuard lock(m_resourceSync);

        SharedPtr<ResourceType> resource;

        // Lookup the resource type.
        const Hash typeHash = ResourceType::getTypeHash();
        auto typeItr = m_resources.find(typeHash);
        if(typeItr != m_resources.end())
        {
            // Lookup the resource name.
            const Hash nameHash = hash(name);
            auto resourceItr = (*typeItr).second.find(nameHash);
            if(resourceItr != (*typeItr).second.end())
            {
                resource = std::static_pointer_cast<ResourceType>(
                    (*resourceItr).second.lock());
            }
        }

        return resource;
    }

private:
    ResourceTypeMap m_resources;

    struct LoadStage
    {
        enum Enum
        {
            PendingLoad,
            Loading,
            WaitForDependencies,
            Count
        };
    };

    SharedResource getResourceInternal(Hash type, const String& name);
    SharedResource loadPackageInternal(Hash type, const String& name);
    SharedResource createResourceInternal(Hash type, const String& name);
    bool isInLoadStageInternal(SharedResource resource, LoadStage::Enum loadStage);

    mutable Mutex m_resourceSync;

    WeakResourceArray m_loadingResources[LoadStage::Count];
    SharedResourceArray m_packages;
    Resource::DependencyCallback m_dependencyCallback;
};

inline bool ResourceManager::isLoading()
{
    for(auto& stageArray : m_loadingResources)
    {
        if(!stageArray.empty())
        {
            return true;
        }
    }
    return false;
}

// Add a resource type to the factory.
#define REGISTER_RESOURCE(ResourceClass)        ResourceManager::getInstance().addProduction(\
                                                    ResourceClass::getTypeHash(), [](const String& name){return SharedPtr<Resource>(new ResourceClass(name), resourceDeleter);});

} // namespace OE_Core

#endif // __OE_CORE_RESOURCE_MANAGER_H__

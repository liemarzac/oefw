#ifndef __OE_CORE_SINGLETON_H__
#define __OE_CORE_SINGLETON_H__

#include "core_private.h"

namespace OE_Core
{
    template<class T>
    class Singleton
    {
    public:
        virtual ~Singleton();
        static T& createInstance();
        static void destroyInstance();
        static T& getInstance();
        static const T& getConstInstance();
        static T* getInstancePointer();
        static const T* getConstInstancePointer();

    protected:
        Singleton();

    private:
        static T* ms_instance;
    };

	template<class T>
	T& Singleton<T>::createInstance()
	{
        return *new T();
	}

	template<class T>
	void Singleton<T>::destroyInstance()
	{
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        delete ms_instance;
	}

    template<class T>
    Singleton<T>::Singleton()
    {
        OE_CHECK(!ms_instance, OE_CHANNEL_CORE);
        ms_instance = static_cast<T*>(this);
    }

    template<class T>
    Singleton<T>::~Singleton()
    {
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        ms_instance = nullptr;
    }

    template<class T>
    T& Singleton<T>::getInstance()
    {
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        return *ms_instance;
    }

    template<class T>
    const T& Singleton<T>::getConstInstance()
    {
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        return *ms_instance;
    }

    template<class T>
    T* Singleton<T>::getInstancePointer()
    {
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        return ms_instance;
    }

    template<class T>
    const T* Singleton<T>::getConstInstancePointer()
    {
        OE_CHECK(ms_instance, OE_CHANNEL_CORE);
        return ms_instance;
    }

    template<class T>
    T* Singleton<T>::ms_instance = nullptr;
}

#endif // __OE_CORE_SINGLETON_H__

#ifndef __OE_CORE_STRING_H__
#define __OE_CORE_STRING_H__

#include "oe_core.h"

// External
#include <algorithm>
#include <string>

typedef std::string String;


namespace OE_Core
{

enum class StringPos
{
    FirstOf,
    LastOf
};

inline void replace(String& s, char from, char to)
{
    std::replace(s.begin(), s.end(), from, to);
}

inline void left(String& s, char of, StringPos pos)
{
    if(pos == StringPos::FirstOf)
    {
        s = s.substr(0, s.find_first_of(of));
    }
    else
    {
        s = s.substr(0, s.find_last_of(of));
    }
}

inline void right(String& s, char of, StringPos pos)
{
    if(pos == StringPos::FirstOf)
    {
        s = s.substr(s.find_first_of(of) + 1);
    }
    else
    {
        s = s.substr(s.find_last_of(of) + 1);
    }
}

inline void removeExtension(String& s)
{
    left(s, '.', StringPos::LastOf);
}

inline void removePath(String& s)
{
    replace(s, '\\', '/');
    right(s, '/', StringPos::LastOf);
}

inline void removePathAndExtension(String& s)
{
    removePath(s);
    removeExtension(s);
}

bool OE_CORE_EXPORT startWith(const String& s, const String& start);
bool OE_CORE_EXPORT endWith(const String& s, const String& end);

} // namespace OE_Core

#endif // __OE_CORE_STRING_H__

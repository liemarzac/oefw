#ifndef __OE_CORE_STD_ATOMIC_HELPERS_H__
#define __OE_CORE_STD_ATOMIC_HELPERS_H__

#include <atomic>

using AtomicBool = std::atomic_bool;
using AtomicInt = std::atomic_int;
using AtomicUInt = std::atomic_uint;
using AtomicDouble = std::atomic<double>;

template<typename T>
using Atomic = std::atomic<T>;



#endif // __OE_CORE_STD_ATOMIC_HELPERS_H__

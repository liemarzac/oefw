#ifndef __OE_CORE_CALLBACK_H__
#define __OE_CORE_CALLBACK_H__

#include "oe_core.h"

// OE_Core
#include "std_smartptr_helpers.h"
#include "std_mutex_helpers.h"
#include "vector.h"

// External
#include <functional>

#define OE_CHANNEL_CALLBACK "Callback"

namespace OE_Core
{

enum class CallbackType
{
    Once,
    Permanent
};

class OE_CORE_EXPORT Functor
{
public:
    Functor(CallbackType type);
    virtual void callback() = 0;
    CallbackType getType() const;

private:
    Functor();
    CallbackType m_type;
};

class OE_CORE_EXPORT FunctionCallback : public Functor
{
public:
    FunctionCallback(void (*callback)(void), CallbackType type);
    virtual void callback() override;

private:
    FunctionCallback();
    void (*m_callback)(void);
};

template<class T>
class MemberCallback : public Functor
{
public:
    MemberCallback(void (T::*member)(void), T* instance, CallbackType type):
        m_member(member)
        ,m_instance(instance)
        ,Functor(type)
    {
    }

    virtual void callback() override
    {
        (*m_instance.*m_member)();
    }

private:
    void (T::*m_member)(void);
    T* m_instance;
};

OE_CORE_EXPORT SharedPtr<FunctionCallback> MakeFunctor(void (*callback)(void), CallbackType type);

template<class T>
SharedPtr<MemberCallback<T> > MakeFunctor(void (T::*member)(void), T* instance, CallbackType type)
{
    return SharedPtr<MemberCallback<T> >(new MemberCallback<T>(member, instance, type));
}

template<typename ReturnType>
class FunctorReturn
{
public:
    FunctorReturn();
    virtual ReturnType callback() = 0;
};

template<typename ReturnType>
class FunctionReturnCallback : public FunctorReturn<ReturnType>
{
public:
    FunctionReturnCallback(ReturnType(*callback)(void)):
        m_callback(callback)
    {
    }

    virtual ReturnType callback() override
    {
        return (*m_callback)();
    }

private:
    FunctionReturnCallback();
    ReturnType(*m_callback)(void);
};

template<typename ReturnType, class MemberClass>
class MemberReturnCallback : public FunctorReturn<ReturnType>
{
public:
    typedef ReturnType (MemberClass::*MemberFuncPtr)(void);
    MemberReturnCallback(MemberFuncPtr member, MemberClass* instance) :
        m_member(member)
        , m_instance(instance)
    {
    }

    virtual MemberClass callback() override
    {
        return (*m_instance.*m_member)();
    }

private:
    MemberFuncPtr m_member;
    MemberClass* m_instance;
};

template<typename ReturnType>
SharedPtr<FunctionReturnCallback<ReturnType>> MakeFunctor(ReturnType(*callback)(void))
{
    return SharedPtr<FunctionReturnCallback<ReturnType> >(new FunctionReturnCallback<ReturnType>(callback));
}

template<typename ReturnType, class MemberClass>
SharedPtr<MemberReturnCallback<ReturnType, MemberClass>> MakeFunctor(ReturnType (MemberClass::*member)(void), MemberClass* instance)
{
    return SharedPtr<MemberReturnCallback<ReturnType, MemberClass> >(new MemberReturnCallback<ReturnType, MemberClass>(member, instance));
}

template<typename ReturnType, typename P1Type>
class FunctorReturnP1
{
public:
    FunctorReturnP1()
    {
    }

    virtual ReturnType callback(P1Type p1) = 0;
};

template<typename ReturnType, typename P1Type>
class FunctionReturnP1Callback : public FunctorReturnP1<ReturnType, P1Type>
{
public:
    FunctionReturnP1Callback(std::function<ReturnType (P1Type)> func):
        m_func(func)
    {
    }

    virtual ReturnType callback(P1Type p1) override
    {
        return m_func(p1);
    }

private:
    FunctionReturnP1Callback();
    //ReturnType(*m_callback)(P1Type);
    std::function<ReturnType (P1Type)> m_func;
};

template<typename ReturnType, typename P1Type, class MemberClass>
class MemberReturnP1Callback : public FunctorReturnP1<ReturnType, P1Type>
{
public:
    MemberReturnP1Callback(ReturnType(MemberClass::*member)(P1Type), MemberClass* instance) :
        m_member(member)
        , m_instance(instance)
    {
    }

    virtual ReturnType callback(P1Type p1) override
    {
        return (*m_instance.*m_member)(p1);
    }

private:
    ReturnType(MemberClass::*m_member)(P1Type);
    MemberClass* m_instance;
};

template<typename ReturnType, typename P1Type>
SharedPtr<FunctionReturnP1Callback<ReturnType, P1Type>> MakeFunctor(std::function<ReturnType (P1Type)> func)
{
    return SharedPtr<FunctionReturnP1Callback<ReturnType, P1Type> >(new FunctionReturnP1Callback<ReturnType, P1Type>(func));
}

template<typename ReturnType, typename P1Type, class MemberClass>
SharedPtr<MemberReturnP1Callback<ReturnType, P1Type, MemberClass> > MakeFunctor(ReturnType (MemberClass::*member)(P1Type), MemberClass* instance)
{
    return SharedPtr<MemberReturnP1Callback<ReturnType, P1Type, MemberClass> >(new MemberReturnP1Callback<ReturnType, P1Type, MemberClass>(member, instance));
}

class OE_CORE_EXPORT CallbackEntry
{
friend class FunctorGroup;
public:
    CallbackEntry()
    {
        m_id = InvalidCallbackEntry;
    }

    bool operator()() const
    {
        return m_id != InvalidCallbackEntry;
    }

    CallbackEntry& operator=(const CallbackEntry& rhs)
    {
        OE_CHECK_MSG(m_id == InvalidCallbackEntry, OE_CHANNEL_CALLBACK, "Call reset function before assigning a new callback.");
        m_id = rhs.m_id;
        return *this;
    }

    void reset()
    {
        m_id = InvalidCallbackEntry;
    }

private:
    uint16 m_id;
    static const uint16 InvalidCallbackEntry = OE_MAX_UINT16;
};

class OE_CORE_EXPORT FunctorGroup
{
public:
    CallbackEntry addCallback(SharedPtr<Functor> functor);
    void removeCallback(CallbackEntry entry);
    void callback();

private:
    Vector<SharedPtr<Functor> > m_functors;
    std::mutex m_mutex;
};

}

#endif // __OE_CORE_CALLBACK_H__

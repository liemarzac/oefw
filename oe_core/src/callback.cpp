#include "callback.h"

#include <memory>

namespace OE_Core
{

Functor::Functor(CallbackType type):
    m_type(type)
{
}

CallbackType Functor::getType() const
{
    return m_type;
}



FunctionCallback::FunctionCallback(void(*callback)(void), CallbackType type) :
m_callback(callback)
    , Functor(type)
{
}

void FunctionCallback::callback()
{
    m_callback();
}

CallbackEntry FunctorGroup::addCallback(SharedPtr<Functor> functor)
{
    LockGuard lock(m_mutex);

    CallbackEntry entry;

    for(size_t iEntry = 0; iEntry < m_functors.size(); ++iEntry)
    {
        if(!m_functors[iEntry])
        {
            m_functors[iEntry] = std::move(functor);
            entry.m_id = (uint16)iEntry;
            return entry;
        }
    }

    m_functors.push_back(std::move(functor));
    entry.m_id = (uint16)m_functors.size() - 1;
    return entry;
}

void FunctorGroup::removeCallback(CallbackEntry entry)
{
    LockGuard lock(m_mutex);

    if(entry.m_id == CallbackEntry::InvalidCallbackEntry)
        return;

    OE_CHECK(entry.m_id < m_functors.size(), OE_CHANNEL_CORE);
    m_functors[entry.m_id].reset();
}

void FunctorGroup::callback()
{
    LockGuard lock(m_mutex);

    for(size_t i = 0; i < m_functors.size(); ++i)
    {
        if(m_functors[i])
        {
            m_functors[i]->callback();
            if(m_functors[i]->getType() == CallbackType::Once)
            {
                m_functors[i].reset();
            }
        }
    }
}

SharedPtr<FunctionCallback> MakeFunctor(void(*callback)(void), CallbackType type)
{
    return SharedPtr<FunctionCallback>(new FunctionCallback(callback, type));
}

}

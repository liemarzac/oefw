#ifndef __OE_CORE_RING_BUFFER_H__
#define __OE_CORE_RING_BUFFER_H__

#include "core_private.h"
#include "flagset.h"
#include "std_atomic_helpers.h"

#include <condition_variable>
#include <mutex>

namespace OE_Core
{

class OE_CORE_EXPORT RingBuffer
{
public:
    RingBuffer();
    RingBuffer(size_t size, bool allowResize, uint alignment, float reallocRatio = 1.33f);
    ~RingBuffer();

    size_t read(void* ptr, size_t maxSize);
    bool write(const void* ptr, size_t size);

    bool alloc(void*& ptr, size_t size);
    void commit();
    bool beginRead(void*& outPtr, size_t& availableSize);
    void endRead(size_t readBytes);
    void clear();
    bool isEmpty() const;
    void waitUntilEmpty(uint32 timeout = 0);
    void waitUntilNotEmpty(uint32 timeout = 0);

private:
    enum class AllocResult
    {
        Succeed,
        Fail,
        Recall,
    };

    RingBuffer(const RingBuffer& rhs);
    RingBuffer& operator=(const RingBuffer& rhs);
    bool allocInternal(void*& ptr, size_t size);
    void resize(size_t size);
    
    AllocResult allocPreRead(void*& ptr, size_t size);
    AllocResult allocPostRead(void*& ptr, size_t size);

    bool wrapWrite();

    uint8* m_rawBufferStart;
    uint8* m_userBufferStart;
    uint8* m_rawBufferEnd;
    size_t m_size;
    uint m_alignment;
    float m_reallocRatio; // ratio between re-allocated memory and required memory

    std::atomic<uint8*> m_read;
    std::atomic<uint8*> m_readEnd;
    std::atomic<uint8*> m_write;
    std::atomic<uint8*> m_dataEnd;

    AtomicBool m_isWritting;
    AtomicBool m_isReading;

    std::condition_variable m_commandNotEmptyNotifier;
    std::condition_variable m_commandEmptyNotifier;
    std::mutex m_commandSync;

    struct Flags
    {
        enum Enum
        {
            AllowResize
        };
    };

    FlagSet<Flags::Enum> m_flags;
};


} // namespace OE_Core
#endif // __OE_CORE_RING_BUFFER_H__

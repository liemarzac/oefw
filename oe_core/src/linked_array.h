#ifndef __OE_CORE_LINKED_ARRAY_H__
#define __OE_CORE_LINKED_ARRAY_H__

#include "core_private.h"

#include "memory.h"

// External
#include <cstring>


namespace OE_Core
{

typedef uint64 TocBitfield;

static const uint BitsPerBitfield = sizeof(TocBitfield) * 8;

#define NB_PAGE_OBJECTS          (MaxNbObjPerPage)
#define NB_PAGE_BITFIELDS        ((NB_PAGE_OBJECTS + BitsPerBitfield - 1) / BitsPerBitfield)
#define PAGE_TOC_SIZE            (NB_PAGE_BITFIELDS * BitsPerBitfield)
#define OBJ_SIZE                 (NB_PAGE_OBJECTS * sizeof(Object))
#define PAGE_RAW_SIZE            (OBJ_SIZE + PAGE_TOC_SIZE)
#define NB_ARRAY_BITFIELDS       ((MaxNbPages + BitsPerBitfield - 1) / BitsPerBitfield)


template<typename Object, uint MaxNbObjPerPage = 4*OneKb/sizeof(Object), uint MaxNbPages = BitsPerBitfield>
class LinkedArray
{
    friend class Itr;

public:
    class Page
    {
    friend class Itr;
    public:
        struct TocPosition
        {
            TocPosition()
            {
                memset(this, 0, sizeof(TocPosition));
            }
            uint m_iBitfield;
            uint m_iBit;
        };

        Page();
        ~Page();

        void* operator new(size_t size)
        {
            void* p = nullptr;
            ALIGNED_MALLOC(p, size, 16);
            return p;
        }

        void operator delete(void *p)
        {
            Page* page = static_cast<Page*>(p);
            ALIGNED_FREE(page);
        }

        bool isEmpty() const;
        bool isFull() const;
        Object* acquire();
        void release(Object* object);
        Object* at(uint iObject);
        void clear();

        static inline size_t getRawSize()
        {
            return PAGE_RAW_SIZE;
        }

        inline bool isUsed(uint iObject) const
        {
            uint iBitfield = iObject / BitsPerBitfield;
            uint iBit = iObject % BitsPerBitfield;

            TocBitfield mask = (TocBitfield)1 << iBit;

            const TocBitfield* toc = getToc();
            return (*(toc + iBitfield) & mask) != 0;
        }

        inline bool isFree(uint iObject) const
        {
            return !isUsed(iObject);
        }

        inline Object* getFirstObject()
        {
            return reinterpret_cast<Object*>(m_rawBuffer.m_data + PAGE_TOC_SIZE);
        }

        inline Object* getLastObject()
        {
            return reinterpret_cast<Object*>(m_rawBuffer.m_data + PAGE_RAW_SIZE - sizeof(Object));
        }

        inline bool doesContain(Object* object) const
        {
            Object* firstObject = const_cast<Page*>(this)->getFirstObject();
            Object* lastObject = const_cast<Page*>(this)->getLastObject();
            return firstObject <= object && object <= lastObject;
        }

        inline TocBitfield* getToc()
        {
            return reinterpret_cast<TocBitfield*>(m_rawBuffer.m_data);
        }

        inline const TocBitfield* getToc() const
        {
            return reinterpret_cast<const TocBitfield*>(m_rawBuffer.m_data);
        }

    private:
        uint32 m_nObjectsUsed;
        uint32 m_iTocBitfieldSeek;
        TocPosition m_unusedTocPosition;

        struct ALIGNED_(16) Buffer
        {
            uint8 m_data[PAGE_RAW_SIZE];
        }m_rawBuffer;
    };


    class Itr
    {
    public:
        struct Position
        {
            Position()
            {
                memset(this, 0, sizeof(Position));
            }

            bool operator==(const Position& rhs) const
            {
                return memcmp(this, &rhs, sizeof(Position)) == 0;
            }

            uint m_iPagesBitfield;
            uint m_iPagesBit;
            uint m_iObjectsBitfield;
            uint m_iObjectsBit;
        };

        Itr(){}
        Itr(const LinkedArray* linkedArray, const Position& position);
        bool operator!=(const Itr& other) const;
        Object* operator*() const;
        const Itr& operator++();

    private:
        const LinkedArray* m_linkedArray;
        Position m_position;
    };

    LinkedArray();
    ~LinkedArray();

    Object* acquire();
    void release(Object* object);
    uint size() const;
    Itr begin() const;
    Itr end() const;
    Itr beginPage(uint iPage) const;
    void clear();

    inline uint getNbUsedPages() const
    {
        return m_nUsedPages;
    }

private:
    struct PageRange
    {
        PageRange()
        {
            memset(this, 0, sizeof(PageRange));
        }

        Object* m_first;
        Object* m_last;
    };

    Page* m_pages[MaxNbPages];
    PageRange m_ranges[MaxNbPages];
    TocBitfield m_toc[NB_ARRAY_BITFIELDS];
    uint m_nUsedObjects;
    uint m_nUsedPages;
};

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::Page()
{
    m_nObjectsUsed = 0;
    m_iTocBitfieldSeek = 0;

    memset(m_rawBuffer.m_data, 0, PAGE_RAW_SIZE);

    // Set entries not mapped to any object as used.
    m_unusedTocPosition.m_iBitfield = NB_PAGE_OBJECTS / BitsPerBitfield;
    m_unusedTocPosition.m_iBit = NB_PAGE_OBJECTS % BitsPerBitfield;

    TocBitfield* const toc = getToc();

    for(uint iBit = m_unusedTocPosition.m_iBit; iBit < BitsPerBitfield; ++iBit)
    {
        TocBitfield mask = (TocBitfield)1 << iBit;
        *(toc + m_unusedTocPosition.m_iBitfield) |= mask;
    }
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::~Page()
{
    // It is the responsibility of the user to make sure all
    // objects have been released before the page gets deleted.
    OE_CHECK(isEmpty(), OE_CHANNEL_CORE);
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
bool LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::isFull() const
{
    return m_nObjectsUsed == MaxNbObjPerPage;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
bool LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::isEmpty() const
{
    return m_nObjectsUsed == 0;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
Object* LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::acquire()
{
    if(isFull())
        return nullptr;

    TocBitfield* const toc = getToc();
    TocBitfield* bitfield = nullptr;
    uint8* byte = nullptr;

    TocBitfield mask = 0;
    uint iBitfield = m_iTocBitfieldSeek;

    uint count = 0;
    while(count < MaxNbObjPerPage)
    {
        bitfield = toc + iBitfield;
        if((*bitfield) != (TocBitfield)-1)
        {
            byte = reinterpret_cast<uint8*>(bitfield);
            for(uint iByte = 0; iByte < sizeof(TocBitfield); ++iByte, ++byte)
            {
                if(*byte != 0xff)
                {
                    for(uint iBit = 0; iBit < 8; ++iBit)
                    {
                        uint8 mask = (uint8)1 << iBit;
                        if((*byte & mask) == 0)
                        {
                            // Record as used.
                            *byte |= mask;

                            // Set where to start scanning next time.
                            m_iTocBitfieldSeek = iBitfield + 1;
                            if(m_iTocBitfieldSeek >= NB_PAGE_BITFIELDS)
                                m_iTocBitfieldSeek = 0;

                            ++m_nObjectsUsed;

                            uint32 iObject = (iBitfield * BitsPerBitfield) + (iByte * 8) + iBit;
                            Object* object = new(at(iObject))Object();
                            return object;
                        }
                    }
                }
            }
        }

        ++iBitfield;
        if(iBitfield >= NB_PAGE_BITFIELDS)
            iBitfield = 0;

        ++count;
    }

    return nullptr;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
void LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::release(Object* object)
{
    OE_CHECK(doesContain(object), OE_CHANNEL_CORE);
    object->~Object();

    // Update TOC.
    uint iObject = (uint)(object - getFirstObject());
    uint iBitfield = iObject / BitsPerBitfield;
    TocBitfield* bitfield = getToc() + iBitfield;
    uint iBit = iObject % BitsPerBitfield;
    TocBitfield mask = ~((TocBitfield)1 << iBit);
    *bitfield &= mask;
    m_iTocBitfieldSeek = iBitfield;
    --m_nObjectsUsed;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
Object* LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::at(uint iObject)
{
    OE_CHECK(iObject < MaxNbObjPerPage, OE_CHANNEL_CORE);
    return getFirstObject() + iObject;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
void LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Page::clear()
{
    // Delete all used objects.
    for(uint iObject = 0; iObject < MaxNbObjPerPage; ++iObject)
    {
        if(isUsed(iObject))
        {
            Object* object = at(iObject);
            object->~Object();
        }
    }

    // Reset toc.
    memset(getToc(), 0, PAGE_TOC_SIZE);
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr::Itr(
    const LinkedArray* linkedArray,
    const Position& position):
        m_linkedArray(linkedArray)
        ,m_position(position)
{
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
bool LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr::operator!=(
    const Itr& other) const
{
    bool isSameLinkedArray = m_linkedArray == other.m_linkedArray;
    bool isSamePostion = m_position == other.m_position;

    return !isSameLinkedArray || !isSamePostion;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
Object* LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr::operator*() const
{
    uint iPage = (m_position.m_iPagesBitfield * BitsPerBitfield) + m_position.m_iPagesBit;
    uint iObject = (m_position.m_iObjectsBitfield * BitsPerBitfield) + m_position.m_iObjectsBit;

    return m_linkedArray->m_pages[iPage]->at(iObject);
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
const typename LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr&
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr::operator++()
{
    while(true)
    {
        // Advance object pointer.
        ++m_position.m_iObjectsBit;
        if(m_position.m_iObjectsBit >= BitsPerBitfield)
        {
            m_position.m_iObjectsBit = 0;
            ++m_position.m_iObjectsBitfield;
        }

        if(m_position.m_iObjectsBitfield * BitsPerBitfield + m_position.m_iObjectsBit >= MaxNbObjPerPage)
        {
            // Advance page pointer.
            m_position.m_iObjectsBit = 0;
            m_position.m_iObjectsBitfield = 0;

            ++m_position.m_iPagesBit;
            if(m_position.m_iPagesBit >= BitsPerBitfield)
            {
                m_position.m_iPagesBit = 0;
                ++m_position.m_iPagesBitfield;
            }

            uint iPage = m_position.m_iPagesBitfield * BitsPerBitfield + m_position.m_iPagesBit;
            if(iPage >= m_linkedArray->m_nUsedPages)
                break;
        }

        // Break if object is used.
        const uint iPage = m_position.m_iPagesBitfield * BitsPerBitfield + m_position.m_iPagesBit;
        const uint iObject = m_position.m_iObjectsBitfield + m_position.m_iObjectsBit;
        const TocBitfield mask = (TocBitfield)1 << m_position.m_iObjectsBit;
        const TocBitfield* toc = m_linkedArray->m_pages[iPage]->getToc();
        if((*(toc + m_position.m_iObjectsBitfield) & mask) != 0)
            break;
    }

    return *this;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::LinkedArray()
{
    memset(&m_pages, 0, MaxNbPages * sizeof(Page*));
    m_nUsedObjects = 0;
    m_nUsedPages = 0;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::~LinkedArray()
{
    for(uint i = 0; i < m_nUsedPages; ++i)
    {
        delete m_pages[i];
    }
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
uint LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::size() const
{
    return m_nUsedObjects;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
Object* LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::acquire()
{
    for(uint iBitfield = 0; iBitfield < NB_ARRAY_BITFIELDS; ++iBitfield)
    {
        if(m_toc[iBitfield] != (TocBitfield)-1)
        {
            uint8* byte = reinterpret_cast<uint8*>(&m_toc[iBitfield]);
            for(uint iByte = 0; iByte < sizeof(TocBitfield); ++iByte, ++byte)
            {
                if((*byte) != 0xff)
                {
                    for(uint iBit = 0; iBit < 8; ++iBit)
                    {
                        uint8 mask = (uint8)1 << iBit;
                        if(((*byte) & mask) == 0)
                        {
                            uint iPage = (iBitfield * BitsPerBitfield) + (iByte * 8) + iBit;
                            if(iPage >= m_nUsedPages)
                            {
                                goto create_page;
                            }
                            else
                            {
                                OE_CHECK(!m_pages[iPage]->isFull(), OE_CHANNEL_CORE);

                                Object* ret = m_pages[iPage]->acquire();
                                ++m_nUsedObjects;

                                OE_CHECK(ret != nullptr, OE_CHANNEL_CORE);

                                // Update TOC when page is full.
                                if(m_pages[iPage]->isFull())
                                {
                                    *byte |= mask;
                                }

                                return ret;
                            }
                        }
                    }
                }
            }
        }
    }

create_page:
    if(m_nUsedPages >= MaxNbPages)
        return nullptr;

    // Allocate new page.
    Page* newPage = new Page();
    m_pages[m_nUsedPages] = newPage;

    // Cache ranges.
    m_ranges[m_nUsedPages].m_first = newPage->getFirstObject();
    m_ranges[m_nUsedPages].m_last = newPage->getLastObject();

    ++m_nUsedPages;

    // New pages are empty so update TOC.
    TocBitfield* bitfield = &m_toc[0] + (((m_nUsedPages - 1) / BitsPerBitfield));
    uint iBit = ((m_nUsedPages - 1) % BitsPerBitfield);
    TocBitfield mask = ~((TocBitfield)1 << iBit);
    *bitfield &= mask;

    // Acquire a new object in the new page.
    Object* ret = newPage->acquire();
    ++m_nUsedObjects;
    OE_CHECK(ret != nullptr, OE_CHANNEL_CORE);

    return ret;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
void LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::release(Object* object)
{
    uint iPage = 0;
    for(;iPage < m_nUsedPages; ++iPage)
    {
        if(m_ranges[iPage].m_first <= object && object <= m_ranges[iPage].m_last)
            break;
    }

    OE_CHECK(iPage < m_nUsedPages, OE_CHANNEL_CORE);
    m_pages[iPage]->release(object);
    --m_nUsedObjects;

    // Update TOC.
    uint iBitfield = iPage / BitsPerBitfield;
    uint iBit = iPage % BitsPerBitfield;
    TocBitfield mask = ~((TocBitfield)1 << iBit);
    m_toc[iBitfield] &= mask;

    if(m_pages[iPage]->isEmpty())
    {
        --m_nUsedPages;
        delete m_pages[iPage];

        // Keep a packed array. The last page will replace the deleted one.
        if(iPage < m_nUsedPages)
        {
            m_pages[iPage] = m_pages[m_nUsedPages];
            m_ranges[iPage] = m_ranges[m_nUsedPages];
            m_pages[m_nUsedPages] = nullptr;

            // Re-update TOC
            if(m_pages[iPage]->isFull())
            {
                mask = ((TocBitfield)1 << iBit);
                m_toc[iBitfield] |= mask;
            }
        }
    }
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
typename LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::begin() const
{
    typename Itr::Position beginPosition;
    Itr beginItr(this, beginPosition);

    if(m_nUsedPages > 0)
    {
        // Find first used object.
        TocBitfield mask = 1;
        const TocBitfield* const toc = m_pages[0]->getToc();
        if((*toc & mask) == 0)
        {
            ++beginItr;
        }
    }
    else
    {
        beginItr = end();
    }

    return beginItr;
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
typename LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::end() const
{
    typename Itr::Position endPosition;
    endPosition.m_iPagesBitfield = m_nUsedPages / BitsPerBitfield;
    endPosition.m_iPagesBit = m_nUsedPages % BitsPerBitfield;

    return Itr(this, endPosition);
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
typename LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::Itr
LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::beginPage(uint iPage) const
{
    if(iPage >= m_nUsedPages)
        return end();

    typename Itr::Position pagePosition;
    pagePosition.m_iPagesBitfield = iPage / BitsPerBitfield;
    pagePosition.m_iPagesBit = iPage % BitsPerBitfield;
    Itr beginItr(this, pagePosition);

    // Find first used object in the page.
    TocBitfield mask = 1;
    const TocBitfield* const toc = m_pages[iPage]->getToc();
    if((*toc & mask) == 0)
    {
        ++beginItr;
    }

    return Itr(this, pagePosition);
}

template<typename Object, uint MaxNbObjPerPage, uint MaxNbPages>
void LinkedArray<Object, MaxNbObjPerPage, MaxNbPages>::clear()
{
    for(uint i = 0; i < m_nUsedPages; ++i)
    {
        m_pages[i]->clear();
    }
    m_nUsedObjects = 0;
}

} // namespace OE_Core

#endif // __OE_CORE_LINKED_ARRAY_H__

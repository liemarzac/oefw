#include "jpeg_utils.h"

// External
#ifdef __cplusplus
//extern "C" {
#endif 
#include <turbojpeg.h>
#ifdef __cplusplus
//}
#endif

// OE_Core
#include "buffer.h"
#include "file.h"

#define OE_CHANNEL_JPEG "JPEG"


namespace OE_Core
{

bool loadTextureJPEG(
    const String& fileName,
    Buffer& data,
    uint32& width,
    uint32& height)
{
    UniqueBufferPtr compressedBuffer = appLoadBinaryFileToBuffer(fileName);
    if (!compressedBuffer)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_JPEG, "Could not load resource file %s", fileName.c_str());
        return false;
    }

    long unsigned int jpegSize = compressedBuffer->m_size;

    int jpegSubsamp = 0;
    int jpegWidth = 0;
    int jpegHeight = 0;

    tjhandle jpegDecompressor = tjInitDecompress();

    int decompressedHeaderResult = tjDecompressHeader2(jpegDecompressor, compressedBuffer->m_rawData, jpegSize, &jpegWidth, &jpegHeight, &jpegSubsamp);
    if (decompressedHeaderResult != 0 || jpegWidth == 0 || jpegHeight == 0)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_JPEG, "Invalid jpeg file %s", fileName.c_str());
        tjDestroy(jpegDecompressor);
        return false;
    }

    const TJPF PixelFormat = TJPF_RGBA;
    const size_t PixelSize = tjPixelSize[PixelFormat];

    data.alloc(jpegWidth * jpegHeight * PixelSize);

    //UniqueBufferPtr descompressedBuffer(new Buffer(jpegWidth * jpegHeight * PixelSize) = appLoadBinaryFileToBuffer(fileName);

    int decompressedResult = tjDecompress2(jpegDecompressor, compressedBuffer->m_rawData, jpegSize, data.m_rawData, jpegWidth, 0/*pitch*/, jpegHeight, PixelFormat, TJFLAG_FASTDCT);
    if (decompressedResult != 0)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_JPEG, "Invalid jpeg file %s", fileName.c_str());
        tjDestroy(jpegDecompressor);
        return false;
    }

    tjDestroy(jpegDecompressor);

    width = static_cast<uint32>(jpegWidth);
    height = static_cast<uint32>(jpegHeight);

    return true;
}

} // namespace OE_Core

#ifndef __OE_CORE_VECTOR_H__
#define __OE_CORE_VECTOR_H__

#include <vector>

template <typename T, class U = std::allocator<T>>
using Vector = std::vector<T, U>;

#include <algorithm>

template<typename T>
void removeAtUnordered(Vector<T>& v, size_t i)
{
    if(v.size() == 0 || i >= v.size())
        return;

    if(v.size() > 1)
    {
        std::swap(v[i], v.back());
    }

    v.pop_back();
}

template<typename T>
size_t removeUnordered(Vector<T>& v, const T& e)
{
    size_t nRemoved = 0;

    if(v.size() == 1)
    {
        if(v[0] == e)
        {
            v.pop_back();
            nRemoved = 1;
        }
    }
    else if(v.size() > 1)
    {
        auto itr = std::find(v.begin(), v.end(), e);
        if(itr != v.end())
        {
            std::iter_swap(itr, v.end() - 1);
            v.pop_back();
            ++nRemoved;
        }
    }

    return nRemoved;
}

template<typename T>
size_t removeUniqueUnordered(Vector<T>& v, const T& e)
{
    OE_CHECK(v.size() > 0, OE_CHANNEL_CORE);
    for(size_t i = v.size() - 1; i >= 0; --i)
    {
        if(v[i] == e)
        {
            if(i != v.size() - 1)
            {
                v[i] = v.back();
            }

            v.pop_back();
            return 1;
        }
    }

    return 0;
}

template<typename T, typename U>
bool isContained(const Vector<T, U>& v, const T& element)
{
    return std::find(v.begin(), v.end(), element) != v.end();
}

template<typename T, typename U>
bool addUnique(Vector<T, U>& v, const T& element)
{
    if(!isContained(v, element))
    {
        v.push_back(element);
        return true;
    }

    return false;
}

template<typename T, typename U>
bool swap(Vector<T, U>& v, size_t iA, size_t iB)
{
    if(iA != iB && iA < v.size() && iB < v.size())
    {
        T tmp = v[iA];
        v[iA] = v[iB];
        v[iB] = tmp;
        return true;
    }

    return false;
}

#endif // __OE_CORE_MAP_H__

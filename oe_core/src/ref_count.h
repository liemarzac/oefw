#ifndef __OE_CORE_REF_COUNT_H__
#define __OE_CORE_REF_COUNT_H__

// External
#include <functional>

// OE_Core
#include "oe_core.h"
#include "std_atomic_helpers.h"
#include "std_mutex_helpers.h"

namespace OE_Core
{

class OE_CORE_EXPORT RefCount
{
    friend class RefCountHandle;
public:
    RefCount();
    virtual ~RefCount() = default;

private:
    virtual void deleter();

    uint32 addRef() const;
    uint32 removeRef() const;

    mutable AtomicUInt m_refCount;
};


class OE_CORE_EXPORT RefCountHandle
{
public:
    RefCountHandle();
    RefCountHandle(const RefCountHandle& rhs);
    virtual ~RefCountHandle();

    explicit RefCountHandle(RefCount* refCountObj);
    RefCountHandle& operator=(const RefCountHandle& rhs);
    bool operator==(const RefCountHandle& rhs) const;

    operator bool() const;
    void reset();

protected:
    RefCount* m_obj = nullptr;

private:
    void copy(const RefCountHandle& rhs);
};


template<typename T>
class RefCountPtr : public RefCountHandle
{
public:
    RefCountPtr()
    {
    }
    
    RefCountPtr(const RefCountHandle& rhs):
        RefCountHandle(rhs)
    {
    }

    RefCountPtr(RefCount* refCountObj):
        RefCountHandle(refCountObj)
    {
    }

    T* operator->() const
    {
        return static_cast<T*>(m_obj);
    }

    T* operator*() const
    {
        return static_cast<T*>(m_obj);
    }

    bool operator==(const RefCountPtr& rhs) const
    {
        return m_obj == rhs.m_obj;
    }
};

} // namespace OE_Core

#endif // __OE_CORE_REF_COUNT_H__

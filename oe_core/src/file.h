#ifndef __OE_CORE_FILE_H__
#define __OE_CORE_FILE_H__

#include "oe_core.h"

// External
#include <json/json.h>
#include <fstream>

// OE_Core
#include "buffer.h"
#include "oe_string.h"


namespace OE_Core
{

bool OE_CORE_EXPORT appDoesResourceExist(const String& resourceName);
bool OE_CORE_EXPORT appGetResourceStream(
    const String& resourceName,
    std::ifstream& stream,
    std::ios_base::openmode mode = std::ios_base::in);
bool OE_CORE_EXPORT appGetFullyQualifiedResourcePath(
    String& fullyQualifiedPath,
    const String& partialPath);
bool OE_CORE_EXPORT appLoadJsonFile(const char* fileName, Json::Value& outJson);
UniqueBufferPtr OE_CORE_EXPORT appLoadBinaryFileToBuffer(const String& fileName);

void OE_CORE_EXPORT getFolderPath(String& folderPath, const String& filePath);

}

#endif // __OE_CORE_FILE_H__

#include "task_group.h"

#include "core_private.h"

// OE_Core
#include "task_manager.h"
#include "runnable.h"

// External
#include <thread>


namespace OE_Core
{

TaskGroup::TaskGroup()
{
    m_nRunning = 0;
    m_callback = std::static_pointer_cast<Functor>(
        MakeFunctor<TaskGroup>(&TaskGroup::onTaskFinished, this, CallbackType::Once));
}

TaskGroup::~TaskGroup()
{
}

void TaskGroup::addTask(SharedPtr<Runnable> task)
{
    OE_CHECK(task, OE_CHANNEL_CORE);
    task->addFinishedCallback(m_callback);
    addUnique(m_pendings, task);
}

void TaskGroup::execute()
{
    m_nRunning += uint(m_pendings.size());

    for(auto task : m_pendings)
    {
        TaskManager::getInstance().addTask(task);
    }
}

bool TaskGroup::isCompleted() const
{
    return m_nRunning == 0;
}

void TaskGroup::waitForCompletion()
{
    while(m_nRunning > 0)
    {
        std::this_thread::yield();
    }
}

CallbackEntry TaskGroup::addOnTasksFinishedCallback(SharedPtr<Functor> callback)
{
    return m_onTasksFinished.addCallback(callback);
}

void TaskGroup::removeOnTasksFinishedCallback(CallbackEntry entry)
{
    m_onTasksFinished.removeCallback(entry);
}

void TaskGroup::onTaskFinished()
{
    // This function is called by a worker thread.
    // Any code added here must be thread-safe.
    m_nRunning--;

    if(m_nRunning == 0)
    {
        m_onTasksFinished.callback();
    }
}

} // namespace OE_Core

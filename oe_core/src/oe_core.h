#ifndef __OE_CORE_PUBLIC_H__
#define __OE_CORE_PUBLIC_H__

// External
#include <assert.h>
#include <cstdarg>
#include <cstdlib>
#include <stdint.h>

// Internal
#include <oefw_config.h>
#include "oe_core_export.h"

typedef int64_t int64;
typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint32_t uint;
typedef int32_t int32;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint8_t uint8;
typedef int8_t int8;

namespace OE_Core {}

#define OE_CHANNEL_CORE "Core"

static const int InvalidIndex = -1;

#define OE_MAX_UINT32 0xFFFFFFFF
#define OE_MAX_UINT16 0xFFFF

#if OE_PLATFORM != OE_PLATFORM_WINDOWS
typedef int8_t byte;
#endif

#if defined(_MSC_VER)
#pragma warning(disable: 4251)
#endif

#if defined(_MSC_VER)
#define OE_WELD_ARGS(BUFFER, BUFFER_SIZE) va_list arglist;\
                                              va_start(arglist, format);\
                                              vsnprintf_s(BUFFER,\
                                              BUFFER_SIZE,\
                                              _TRUNCATE,\
                                              format,\
                                              arglist);\
                                              va_end(arglist);
#define OE_SPRINTF(BUFFER, BUFFER_SIZE, FORMAT, ...) sprintf_s(BUFFER,\
                                                         BUFFER_SIZE,\
                                                         FORMAT,\
                                                         __VA_ARGS__);
#define OE_STRCAT(BUFFER, BUFFER_SIZE, SOURCE) strcat_s(BUFFER,\
                                                  BUFFER_SIZE,\
                                                  SOURCE);
#else
#if defined(__GNUC__)
#define OE_WELD_ARGS(BUFFER, BUFFER_SIZE) va_list arglist;\
                                              va_start(arglist, format);\
                                              vsnprintf(BUFFER,\
                                              BUFFER_SIZE,\
                                              format,\
                                              arglist);\
                                              va_end(arglist);
#define OE_SPRINTF(BUFFER, BUFFER_SIZE, FORMAT, ...) snprintf(BUFFER,\
                                                         BUFFER_SIZE,\
                                                         FORMAT,\
                                                         __VA_ARGS__);
#define OE_STRCAT(BUFFER, BUFFER_SIZE, SOURCE) strcat(BUFFER, SOURCE);
#endif
#endif

namespace OE_Core
{
    OE_CORE_EXPORT void log(const char* message);
    OE_CORE_EXPORT void log(const char* channel, const char* fmt, ...);
    OE_CORE_EXPORT void logError(const char* channel, const char* fmt, ...);
    OE_CORE_EXPORT void logWarning(const char* channel, const char* file, int line, const char* function, const char* format, ...);
    OE_CORE_EXPORT void logCheckFailed(const char* channel, const char* file, int line, const char* function, const char* format, ...);
    OE_CORE_EXPORT void getTimeAsString(char* buffer, size_t bufferSize);
}

#define ASSERT assert(false);

#if defined(_MSC_VER)
#define UNICODE
#include <windows.h>
#include <windowsx.h>
#undef min
#undef max
#elif defined(__GNUC__)
#include <signal.h>
#include <limits.h>
#include <malloc.h>
#endif

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
#define DEBUG_BREAK DebugBreak()
#define PATH_SEPARATOR "/"
#define OE_FOPEN(FILE_PTR, FILENAME, MODE) fopen_s(&FILE_PTR, FILENAME, MODE);
#elif OE_PLATFORM == OE_PLATFORM_LINUX
#define DEBUG_BREAK raise(SIGTRAP)
#define PATH_SEPARATOR "/"
#define OE_FOPEN(FILE_PTR, FILENAME, MODE) FILE_PTR = fopen(FILENAME, MODE);
#endif

#define OE_CHECK_MSG(EXPRESSION, CHANNEL, MESSAGE, ...)     if(!(EXPRESSION))\
                                                            {\
                                                                OE_Core::logCheckFailed(CHANNEL, __FILE__, __LINE__, __FUNCTION__, MESSAGE, ##__VA_ARGS__);\
                                                                DEBUG_BREAK;\
                                                                ASSERT;\
                                                            }

#define OE_CHECK(EXPRESSION, CHANNEL)                       if(!(EXPRESSION))\
                                                            {\
                                                                OE_Core::logCheckFailed(CHANNEL, __FILE__, __LINE__, __FUNCTION__, NULL);\
                                                                DEBUG_BREAK;\
                                                                ASSERT;\
                                                            }

#define OE_CHECK_RESULT(EXPRESSION, RESULT, CHANNEL)        if(!(EXPRESSION == RESULT))\
                                                            {\
                                                                OE_Core::logCheckFailed(CHANNEL, __FILE__, __LINE__, __FUNCTION__, NULL);\
                                                                DEBUG_BREAK;\
                                                                ASSERT;\
                                                            }


#define OE_WARNING(CHANNEL, MESSAGE, ...)                   OE_Core::logWarning(CHANNEL, __FILE__, __LINE__, __FUNCTION__, MESSAGE, ##__VA_ARGS__);\
                                                            DEBUG_BREAK;

#define OE_WARNING_PERF(CHANNEL, MESSAGE, ...)              OE_Core::logWarning(CHANNEL "[Perf]", __FILE__, __LINE__, __FUNCTION__, MESSAGE, ##__VA_ARGS__);\
                                                            DEBUG_BREAK;

#define OE_DATA_FOLDER ".." PATH_SEPARATOR ".." PATH_SEPARATOR "data"

#ifdef _DEBUG
#define DBG_STR_PARAM(ParamName) const String& ParamName
#define DBG_STR_PARAM_ADD(ParamName) ,const String& ParamName
#define DBG_PARAM(ParamName) ParamName
#define DBG_PARAM_ADD(ParamName) ,ParamName
#define DBG_ONLY(Statement) Statement;
#else
#define DBG_STR_PARAM(ParamName)
#define DBG_STR_PARAM_ADD(ParamName)
#define DBG_PARAM(ParamName)
#define DBG_PARAM_ADD(ParamName)
#define DBG_ONLY(Statement)
#endif

#define TAG_GENERATED_BEGIN(Tag)
#define TAG_GENERATED_END

#define NO_COPY_DEFAULT_CONSTRUCTOR(Class)  Class() = default;\
                                            Class(const Class&) = delete;\
                                            Class& operator = (const Class&) = delete;

#define NO_COPY(Class)                      Class(const Class&) = delete;\
                                            Class& operator = (const Class&) = delete;

#endif // __OE_CORE_PUBLIC_H__

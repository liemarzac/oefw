#include "linear_allocator.h"

#include "core_private.h"
#include "memory.h"
#include "oe_math.h"

#define OE_CHANNEL_LINEAR_ALLOCATOR "LinearAllocator"


namespace OE_Core
{

LinearAllocatorPage::LinearAllocatorPage(size_t size):
    m_size(size)
{
    m_raw = new byte[size];
}

LinearAllocatorPage::~LinearAllocatorPage()
{
    delete[] m_raw;
}

void* LinearAllocatorPage::alloc(size_t size, size_t targetPageSize, size_t alignment)
{
    const size_t alignedOffset = align(m_offset, alignment);
    if(alignedOffset + size <= m_size)
    {
        m_offset = alignedOffset + size;
        return m_raw + alignedOffset;
    }
    else
    {
        if(!m_next)
        {
            size_t pageSize = max(size, targetPageSize + alignment);
            insert(new LinearAllocatorPage(pageSize));
        }
        OE_CHECK(m_next, OE_CHANNEL_LINEAR_ALLOCATOR);

        return static_cast<LinearAllocatorPage*>(m_next)->alloc(size, targetPageSize, alignment);
    }

}

void LinearAllocatorPage::reset()
{
    if(m_next)
    {
        static_cast<LinearAllocatorPage*>(m_next)->reset();
    }

    // Only keep the first page.
    if(m_prev)
    {
        delete this;
    }
    else
    {
        m_offset = 0;
    }
}

} // OE_Core


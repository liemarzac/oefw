#ifndef __OE_CORE_MAP_H__
#define __OE_CORE_MAP_H__

#include <map>
#include <unordered_map>

template <typename T, typename U>
using Map = std::map<T, U>;

template <typename T, typename U>
using UnorderedMap = std::unordered_map<T, U>;

#endif // __OE_CORE_MAP_H__

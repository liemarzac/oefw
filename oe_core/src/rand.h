#ifndef __OE_CORE_RAND_H__
#define __OE_CORE_RAND_H__

#include "oe_core.h"

namespace OE_Core
{

OE_CORE_EXPORT void seedRand(uint32 seed);
OE_CORE_EXPORT int32 randi32(int32 min, int32 max);
OE_CORE_EXPORT uint32 randu32(uint32 min, uint32 max);
OE_CORE_EXPORT int64 randi64(int64 min, int64 max);
OE_CORE_EXPORT uint64 randu64(uint64 min, uint64 max);
OE_CORE_EXPORT float randr32(float min, float max);
OE_CORE_EXPORT double randr64(double min, double max);

}

#endif // __OE_CORE_RAND_H__

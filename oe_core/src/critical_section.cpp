#include "critical_section.h"
#include "std_mutex_helpers.h"

namespace OE_Core
{

CriticalSection::CriticalSection()
{
    m_counter.store(0);
    m_lockCounter.store(0);
}

CriticalSection::~CriticalSection()
{

}

void CriticalSection::lock(LockOperation operation)
{
    m_lockCounter.fetch_add(1);

    int expected;
    int desired;

    switch(operation)
    {
    case LockOperation::Read:
        expected = m_counter.load();
        if(expected >= 0)
        {
            desired = expected + 1;
        }
        else
        {
            desired = 1;
            expected = 0;
        }
 
        while(!m_counter.compare_exchange_strong(expected, desired))
        {
            if(expected < 0)
            {
                // Write operation in progress.
                UniqueLock lock(m_sync);
                m_lockCounter.fetch_sub(1);
                m_unlockNotifier.wait(lock);
                m_lockCounter.fetch_add(1);
                expected = 0;
            }

            desired = expected + 1;
        }
        break;

    case LockOperation::Write:
        expected = 0;
        desired = -1;

        while(!m_counter.compare_exchange_strong(expected, desired))
        {
            // Write operation in progress.
            UniqueLock lock(m_sync);
            m_lockCounter.fetch_sub(1);
            m_unlockNotifier.wait(lock);
            m_lockCounter.fetch_add(1);
            expected = 0;
        }
        break;
    }

    m_lockCounter.fetch_sub(1);
}

void CriticalSection::unlock()
{
    while(true)
    {
        while(m_lockCounter.load() > 0);

        if(m_sync.try_lock())
        {
            bool notify = false;
            if(m_lockCounter.load() == 0)
            {
                if(m_counter > 0)
                {
                    m_counter.fetch_sub(1);
                }
                else
                {
                    m_counter = 0;
                }
                notify = true;
            }
            m_sync.unlock();

            if(notify)
            {
                m_unlockNotifier.notify_one();
                break;
            }
        }
    }
}

ScopedCriticalSection::ScopedCriticalSection(
    CriticalSection& cs,
    CriticalSection::LockOperation operation):
    m_cs(cs)
{
    m_cs.lock(operation);
}

ScopedCriticalSection::~ScopedCriticalSection()
{
    m_cs.unlock();
}

ReadScopedCriticalSection::ReadScopedCriticalSection(CriticalSection& cs):
    m_scs(cs, CriticalSection::LockOperation::Read)
{
}

WriteScopedCriticalSection::WriteScopedCriticalSection(CriticalSection& cs) :
    m_scs(cs, CriticalSection::LockOperation::Write)
{
}


}
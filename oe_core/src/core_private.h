#ifndef __OE_CORE_PRIVATE_H__
#define __OE_CORE_PRIVATE_H__

#include "oe_core.h"

#ifndef MAX_NUM_WORKERS
#define MAX_NUM_WORKERS 8
#endif

#ifndef OE_RELEASE
#define OE_RELEASE 0
#endif

#endif // __OE_CORE_PRIVATE_H__

#include "oe_string.h"

#include "core_private.h"

namespace OE_Core
{

bool startWith(const String& s, const String& start)
{
    if (start.size() > s.size())
        return false;

    return s.substr(0, start.size()) == start;
}

bool endWith(const String& s, const String& end)
{
    if(end.size() > s.size())
        return false;

    return s.substr(s.size() - end.size() + 1, end.size()) == end;
}

}
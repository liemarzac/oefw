#ifndef __OE_CORE_JPEG_UTILS_H__
#define __OE_CORE_JPEG_UTILS_H__

#include "oe_core.h"

// External
#include <png.h>

// OE_Core
#include "oe_string.h"


namespace OE_Core
{

struct Buffer;

bool OE_CORE_EXPORT loadTextureJPEG(
    const String& fileName,
    Buffer& data,
    uint32& width,
    uint32& height);

} // namespace OE_Core

#endif // __OE_CORE_PNG_UTILS_H__

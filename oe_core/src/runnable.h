#ifndef __OE_CORE_RUNNABLE_H__
#define __OE_CORE_RUNNABLE_H__

#include "core_private.h"

#include "callback.h"

#include "std_atomic_helpers.h"
#include "std_mutex_helpers.h"

namespace OE_Core
{

class OE_CORE_EXPORT Runnable
{
public:
    enum class State
    {
        None,
        Pending,
        InProgress,
        Finished,
        Cancelled,
    };

    Runnable();
    Runnable(const Runnable& rhs);
    Runnable& operator=(const Runnable& rhs);

    virtual ~Runnable();

    virtual void run() = 0;
    inline uint getWorkerId() const;
    inline void setWorkerId(uint id);
    inline uint getTaskId() const;
    inline void setTaskId(uint id);
    inline State getState() const; 
    inline void setState(State state);
    inline bool isFinished() const;
    void addFinishedCallback(SharedPtr<Functor> callback);
    void onFinished();

private:
    mutable std::mutex m_mutex;
    uint m_iTask;
    uint m_iWorker;
    std::atomic<State> m_state;
    FunctorGroup m_onFinishedCallbacks;
};

void Runnable::setTaskId(uint id)
{
    m_iTask = id;
}

uint Runnable::getTaskId() const
{
    return m_iTask;
}

uint Runnable::getWorkerId() const
{
    return m_iWorker;
}

void Runnable::setWorkerId(uint id)
{
    m_iWorker = id;
}

Runnable::State Runnable::getState() const
{
    return m_state;
}

void Runnable::setState(State state)
{
    m_state = state;
}

bool Runnable::isFinished() const
{
    return m_state == State::Finished;
}

}

#endif // __OE_CORE_RUNNABLE_H__
#ifndef __OE_CORE_RESOURCE_LOAD_TASK_H__
#define __OE_CORE_RESOURCE_LOAD_TASK_H__

#include "oe_core.h"

// External
#include <json/json.h>


#include "file.h"
#include "oe_string.h"
#include "runnable.h"

#define OE_CHANNEL_RESOURCE_LOAD_TASK "ResourceLoadTask"


namespace Json
{
class Value;
}


namespace OE_Core
{

template<typename Descriptor>
class ResourceLoadTask : public Runnable
{
public:
    using ReadJsonCallback = std::function<void(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)>;

    ResourceLoadTask() = delete;
    ResourceLoadTask(const String& path, SharedPtr<Descriptor> descriptor, const ReadJsonCallback& callback) :
        m_path(path)
        , m_descriptor(descriptor)
        , m_callback(callback)
    {
        OE_CHECK(m_callback, OE_CHANNEL_RESOURCE_LOAD_TASK);
    }

    inline const SharedPtr<Descriptor> getDescriptor() const
    {
        return m_descriptor;
    }

private:
    virtual void run() override
    {
        String pathWithExtension = m_path;

        // Append .json to the filename.
        if(!endWith(pathWithExtension, ".json"))
        {
            pathWithExtension += ".json";
        }

        Json::Value jsonContent;

        OE_CHECK_RESULT(
            appLoadJsonFile(pathWithExtension.c_str(), jsonContent),
            true,
            OE_CHANNEL_RESOURCE_LOAD_TASK);

        m_callback(jsonContent, m_descriptor);
    }

    String m_path;
    ReadJsonCallback m_callback;
    SharedPtr<Descriptor> m_descriptor;
};


} // namespace OE_Core

#endif // __OE_CORE_RESOURCE_LOAD_TASK_H__

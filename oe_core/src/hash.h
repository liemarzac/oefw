#ifndef __OE_CORE_HASH_H__
#define __OE_CORE_HASH_H__

#include "oe_core.h"

// OE_Core
#include "oe_string.h"


namespace OE_Core
{

typedef uint32 Hash;

uint32 OE_CORE_EXPORT murmur3_32(const char* key, size_t len, uint32 seed = 0x2f68ba9c);

inline Hash hash(const String& string)
{
    return murmur3_32(string.c_str(), string.length());
}

}

#endif // __OE_CORE_HASH_H__

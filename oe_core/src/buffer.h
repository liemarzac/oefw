#ifndef __OE_CORE_BUFFER_H__
#define __OE_CORE_BUFFER_H__

#include "oe_core.h"

// External
#include <cstring>

// Internal
#include "std_smartptr_helpers.h"

#define OE_CHANNEL_BUFFER "Buffer"

namespace OE_Core
{

struct OE_CORE_EXPORT Buffer
{

    Buffer()
    {
        m_rawData = nullptr;
        m_size = 0;
        m_offset = 0;
    }

    Buffer(const Buffer& rhs)
    {
        m_rawData = nullptr;
        m_size = 0;
        m_offset = 0;
        copyFrom(rhs);
    }

    Buffer& operator=(const Buffer& rhs)
    {
        m_rawData = nullptr;
        m_size = 0;
        m_offset = 0;
        copyFrom(rhs);
        return *this;
    }

    void copyFrom(const Buffer& rhs)
    {
        if(m_rawData)
            free(m_rawData);

        if(rhs.m_size > 0)
        {
            m_rawData = (byte*)malloc(rhs.m_size);
            if(m_rawData)
            {
                memcpy(m_rawData, rhs.m_rawData, rhs.m_size);
            }
        }
        else
        {
            m_rawData = nullptr;
        }
        m_size = rhs.m_size;
        m_offset = 0;
    }

    Buffer(size_t size)
    {
        m_rawData = (byte*)malloc(size);
        OE_CHECK_MSG(m_rawData != NULL, OE_CHANNEL_BUFFER, "Out of memory");
        m_size = size;
        m_offset = 0;
    }

    void alloc(size_t size)
    {
        if(m_rawData)
        {
            m_rawData = (byte*)realloc(m_rawData, size);
        }
        else
        {
            m_rawData = (byte*)malloc(size);
        }

        OE_CHECK_MSG(m_rawData != NULL, OE_CHANNEL_BUFFER, "Out of memory");
        m_size = size;
    }

    void rewind()
    {
        m_offset = 0;
    }

    void write(const void* srcData, size_t size)
    {
        OE_CHECK_MSG(m_offset + size <= m_size, OE_CHANNEL_BUFFER, "Buffer out of bounds");
        memcpy(m_rawData + m_offset, srcData, size);
        m_offset += size;
    }

    template<typename T>
    void write(const T& srcData)
    {
        OE_CHECK_MSG(m_offset + sizeof(T) <= m_size, OE_CHANNEL_BUFFER, "Buffer out of bounds");
        memcpy(m_rawData + m_offset, &srcData, sizeof(T));
        m_offset += sizeof(T);
    }

    void read(void* dstData, size_t size)
    {
        OE_CHECK_MSG(m_offset + size <= m_size, OE_CHANNEL_BUFFER, "Buffer out of bounds");
        memcpy(dstData, m_rawData + m_offset, size);
        m_offset += size;
    }

    template<typename T>
    void read(T& dstData)
    {
        OE_CHECK_MSG(m_offset + sizeof(T) <= m_size, OE_CHANNEL_BUFFER, "Buffer out of bounds");
        memcpy(&dstData, m_rawData + m_offset, sizeof(T));
        m_offset += sizeof(T);
    }

    void skip(size_t size)
    {
        OE_CHECK_MSG(m_offset + size <= m_size, OE_CHANNEL_BUFFER, "Buffer out of bounds");
        m_offset += size;
    }

    bool isEndOfBuffer() const
    {
        return m_offset == m_size;
    }

    ~Buffer()
    {
        if(m_rawData)
            delete m_rawData;
    }

    byte* m_rawData;
    size_t  m_size;
    size_t m_offset;
};

using UniqueBufferPtr = UniquePtr<Buffer>;

} // namespace OE_Core

#endif // __OE_CORE_BUFFER_H__

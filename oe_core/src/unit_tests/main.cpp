#include <UnitTest++.h>
#include <iostream>
#include <TestReporterStdout.h>
#include "../oe_core.h"

using namespace OE_Core;

// Tests
#include "test_bin_pack.h"
#include "test_critical_section.h"
#include "test_data_per_worker.h"
#include "test_paged_allocator.h"
#include "test_ring_buffer.h"
#include "test_serialization.h"
#include "test.h"


// Undefine main defined by the SDL.
#ifdef main
#undef main
#endif

int main(int, char const *[])
{
    const int testResult = UnitTest::RunAllTests();
    std::cin.get();

    return testResult;
}

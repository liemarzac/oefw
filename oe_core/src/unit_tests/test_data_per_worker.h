#include <UnitTest++.h>
#include "../oe_core.h"

#include "../data_per_worker.h"

#include <thread>

using namespace OE_Core;

SUITE(DataPerWorkerTestSuite)
{
    // Structure given to workers.
    template<uint N>
    struct InWorker
    {
        DataPerWorker<uint, std::allocator<uint>, N>* m_dataPerWorker;
        uint m_iWorker;
        uint m_lastInteger;
    };

    template<uint N>
    void addIntegers(InWorker<N>& in)
    {
        // Add integers.
        for(uint i = 1; i <= in.m_lastInteger; ++i)
        {
            in.m_dataPerWorker->addData(i, in.m_iWorker);
        }
    }

    uint calculateSum(uint n)
    {
        return (n * (n + 1)) / 2;
    }

    TEST(Nothing)
    {
        int a = 0;
        CHECK_EQUAL(0, a);
    }

    TEST(NoElement)
    {
        DataPerWorker<uint> dataPerWorker;
        uint nElements = 0;
        for(auto data : dataPerWorker)
        {
            // Should nerver get here until we add data.
            ++nElements;
        }

        CHECK_EQUAL(0, nElements);
    }

    TEST(OneWorker)
    {
        static const uint NumWorkers = 1;

        // Create holding container for workers output.
        DataPerWorker<uint, std::allocator<uint>, NumWorkers> dataPerWorker;

        // Setup input for worker.
        InWorker<NumWorkers> in;
        in.m_dataPerWorker = &dataPerWorker;
        in.m_iWorker = 0;
        in.m_lastInteger = 10;

        // Do work.
        std::thread firstWorker(addIntegers<NumWorkers>, std::ref(in));

        // Wait for work to be finished.
        firstWorker.join();

        // Should have the sum of integers from 1 to 10.
        int expectedSum = calculateSum(10);
        int actualSum = 0;

        for(int i : dataPerWorker)
        {
            actualSum += i;
        }

        CHECK_EQUAL(expectedSum, actualSum);
    }

    TEST(EightWorkers)
    {
        static const uint NumWorkers = 8;

        // Create holding container for workers output.
        DataPerWorker<uint, std::allocator<uint>, NumWorkers> dataPerWorker;

        // Setup input for workers.

        InWorker<NumWorkers> in[NumWorkers];
        for(uint i = 0; i < NumWorkers; ++i)
        {
            in[i].m_dataPerWorker = &dataPerWorker;
            in[i].m_iWorker = 0;

            // The number of integers to add is
            // the same as the index of the worker.
            in[i].m_lastInteger = i;
        }

        // Do work.
        std::thread workerThread[NumWorkers];

        for(uint i = 0; i < NumWorkers; ++i)
        {
            workerThread[i] = std::thread(addIntegers<NumWorkers>, std::ref(in[i]));
        }

        // Wait for workers to finish.
        for(uint i = 0; i < NumWorkers; ++i)
        {
            workerThread[i].join();
        }

        // Should have the sum of the sum of integers from 1 to i.
        // with i from 0 to NumWorkers
        int expectedSum = 0;
        for(uint i = 0; i < NumWorkers; ++i)
        {
            expectedSum += calculateSum(i);
        }

        int actualSum = 0;
        for(int i : dataPerWorker)
        {
            actualSum += i;
        }

        CHECK_EQUAL(expectedSum, actualSum);

        // Clear container.
        dataPerWorker.clear();

        // No more integers should exist for any worker.
        actualSum = 0;
        for(int i : dataPerWorker)
        {
            actualSum += i;
        }

        CHECK_EQUAL(0, actualSum);
    }
}

#include "../critical_section.h"
#include "../stop_watch.h"

#include <thread>

const int WaitTimeMs = 1;


class CriticalSectionTester
{
public:    
    void sleepWrite()
    {
        WriteScopedCS wscs(m_scs);
        std::this_thread::sleep_for(std::chrono::milliseconds(WaitTimeMs));
    }

    void sleepRead()
    {
        ReadScopedCS rscs(m_scs);
        std::this_thread::sleep_for(std::chrono::milliseconds(WaitTimeMs));
    }

private:
    CriticalSection m_scs;
};

AtomicBool gWait;
CriticalSectionTester gTester;

void simulWrite()
{
    while(gWait)
    {
        std::this_thread::yield();
    }

    gTester.sleepWrite();
}

void simulRead()
{
    while(gWait)
    {
        std::this_thread::yield();
    }

    gTester.sleepRead();
}

SUITE(TestCriticalSection)
{
    TEST(ReadOnly)
    {
        gWait = true;

        Vector<std::thread> threads;
        const int NumThreads = 10;
 
        for(int i = 0; i < NumThreads; ++i)
        {
            threads.push_back(std::thread(simulRead));
        }

        StopWatch writeTime;
        writeTime.start();

        gWait = false;

        for(int i = 0; i < NumThreads; ++i)
        {
            threads[i].join();
        }

        writeTime.stop();
        const double time = writeTime.getElapsedTime();
        log(OE_CHANNEL_CORE, "Read time %.2f ms", time * 1000.0);
    }

    TEST(WriteOnly)
    {
        CriticalSectionTester tester;
        gWait = true;

        Vector<std::thread> m_threads;
        const int NumThreads = 10;

        for(int i = 0; i < NumThreads; ++i)
        {
            m_threads.push_back(std::thread(simulWrite));
        }

        StopWatch writeTime;
        writeTime.start();

        gWait = false;

        for(int i = 0; i < NumThreads; ++i)
        {
            m_threads[i].join();
        }

        writeTime.stop();
        const double time = writeTime.getElapsedTime();
        log(OE_CHANNEL_CORE, "Write time %.2f ms", time * 1000.0);
        const double minWaitTime = double(WaitTimeMs * NumThreads) / 1000.0;
        CHECK(time >= minWaitTime);
    }

    TEST(ReadWriteCombined)
    {
        gWait = true;

        Vector<std::thread> threads;
        const int NumThreads = 10;

        for(int i = 0; i < NumThreads / 2; ++i)
        {
            threads.push_back(std::thread(simulRead));
        }

        for(int i = 0; i < NumThreads / 2; ++i)
        {
            threads.push_back(std::thread(simulWrite));
        }

        StopWatch combinedTime;
        combinedTime.start();

        gWait = false;

        for(int i = 0; i < NumThreads; ++i)
        {
            threads[i].join();
        }

        combinedTime.stop();
        const double time = combinedTime.getElapsedTime();
        log(OE_CHANNEL_CORE, "Read/Write combined time %.2f ms", time * 1000.0);
    }
}

#include <UnitTest++.h>

#include "../bin_pack.h"

SUITE(BinPackTestSuite)
{
    // Function helper to test a set of data.
    void TestData(
        int binWidth,
        int binHeight,
        const Vector<BinPack<int>::Element>& elementArray);

    TEST(OneSmallRectFitsBigBin)
    {
        BinPack<int> binPack(40, 30);
        BinPack<int>::Element element;
        element.m_data = 0;
        element.m_width = 3;
        element.m_height = 7;
        binPack.addElement(element);
        CHECK(binPack.process());
    }

    TEST(OneMediumRectFitsBigBin)
    {
        BinPack<int> binPack(60, 45);
        BinPack<int>::Element element;
        element.m_data = 0;
        element.m_width = 41;
        element.m_height = 32;
        binPack.addElement(element);
        CHECK(binPack.process());
    }

    TEST(OneBigRectFitsPerfectlyBigBin)
    {
        BinPack<int> binPack(81, 72);
        BinPack<int>::Element element;
        element.m_data = 0;
        element.m_width = 81;
        element.m_height = 72;
        binPack.addElement(element);
        CHECK(binPack.process());
    }

    TEST(OneBigRectWidthDoesNotFitSmallerBin)
    {
        BinPack<int> binPack(55, 41);
        BinPack<int>::Element element;
        element.m_data = 0;
        element.m_width = 56;
        element.m_height = 40;
        binPack.addElement(element);
        CHECK(!binPack.process());
    }

    TEST(OneBigRectHeightDoesNotFitSmallerBin)
    {
        BinPack<int> binPack(12, 22);
        BinPack<int>::Element element;
        element.m_data = 0;
        element.m_width = 8;
        element.m_height = 25;
        binPack.addElement(element);
        CHECK(!binPack.process());
    }

    TEST(EmptyBinCanBeProcessed)
    {
        BinPack<int> binPack(46, 68);
        CHECK(binPack.process());
    }

    TEST(CorrectDataAfterProcessingOneElement)
    {
        BinPack<int> binPack(10, 20);
        BinPack<int>::Element element;
        element.m_data = 4;
        element.m_width = 3;
        element.m_height = 2;
        binPack.addElement(element);
        CHECK(binPack.process());

        Rect2D rect;
        int data;
        int numRects = 0;
        while(binPack.iterateResults(rect, data))
        {
            ++numRects;
            CHECK_EQUAL(4, data);
            CHECK_EQUAL(3, rect.m_width);
            CHECK_EQUAL(2, rect.m_height);
        }
        CHECK_EQUAL(1, numRects);
    }

    TEST(ThreeElementsTooBigDoNotFit)
    {
        BinPack<int> binPack(3, 3);
        BinPack<int>::Element element;
        element.m_data = 1;
        element.m_width = 2;
        element.m_height = 2;
        binPack.addElement(element);
        element.m_data = 2;
        element.m_width = 1;
        element.m_height = 1;
        binPack.addElement(element);
        element.m_data = 3;
        element.m_width = 2;
        element.m_height = 2;
        binPack.addElement(element);
        CHECK(!binPack.process());
    }

    TEST(CorrectDataAfterProcessingTwoElements)
    {
        Vector<BinPack<int>::Element> elementArray;
        BinPack<int>::Element element;
        element.m_data = 10;
        element.m_width = 3;
        element.m_height = 1;
        elementArray.push_back(element);
        element.m_data = 20;
        element.m_width = 1;
        element.m_height = 2;
        elementArray.push_back(element);

        TestData(4, 3, elementArray);
    }

    TEST(CorrectDataAfterProcessingFiveElements)
    {
        Vector<BinPack<int>::Element> elementArray;
        BinPack<int>::Element element;
        element.m_data = 1;
        element.m_width = 4;
        element.m_height = 5;
        elementArray.push_back(element);
        element.m_data = 2;
        element.m_width = 3;
        element.m_height = 2;
        elementArray.push_back(element);
        element.m_data = 3;
        element.m_width = 1;
        element.m_height = 2;
        elementArray.push_back(element);
        element.m_data = 4;
        element.m_width = 4;
        element.m_height = 4;
        elementArray.push_back(element);
        element.m_data = 5;
        element.m_width = 4;
        element.m_height = 1;
        elementArray.push_back(element);
        
        TestData(9, 7, elementArray);
    }

    TEST(CorrectDataAfterProcessingEightElements)
    {
        Vector<BinPack<int>::Element> elementArray;
        BinPack<int>::Element element;
        element.m_data = 1;
        element.m_width = 7;
        element.m_height = 2;
        elementArray.push_back(element);
        element.m_data = 2;
        element.m_width = 4;
        element.m_height = 4;
        elementArray.push_back(element);
        element.m_data = 3;
        element.m_width = 6;
        element.m_height = 1;
        elementArray.push_back(element);
        element.m_data = 4;
        element.m_width = 3;
        element.m_height = 3;
        elementArray.push_back(element);
        element.m_data = 5;
        element.m_width = 4;
        element.m_height = 1;
        elementArray.push_back(element);
        element.m_data = 6;
        element.m_width = 1;
        element.m_height = 3;
        elementArray.push_back(element);
        element.m_data = 7;
        element.m_width = 2;
        element.m_height = 1;
        elementArray.push_back(element);
        element.m_data = 8;
        element.m_width = 1;
        element.m_height = 1;
        elementArray.push_back(element);
        
        TestData(8, 8, elementArray);
    }

    void TestData(
        int binWidth,
        int binHeight,
        const Vector<BinPack<int>::Element>& elementArray)
    {
        BinPack<int> binPack(binWidth, binHeight);
        Vector<BinPack<int>::Element> originalElementArray;

        Vector<BinPack<int>::Element>::const_iterator itrElement = elementArray.begin();
        for(;itrElement != elementArray.end(); ++itrElement)
        {
            binPack.addElement(*itrElement);
            originalElementArray.push_back(*itrElement);
        }

        // Make sure all data can be packed.
        CHECK(binPack.process());

        // Structure which will hold each results.
        struct Result
        {
            Rect2D m_rect;
            int m_data;
        };

        // Gather results.
        Vector<Result> resultArray;
        Result result;
        while(binPack.iterateResults(result.m_rect, result.m_data))
        {
            resultArray.push_back(result);
        }

        // Iterate through results and make sure each of them
        // matches original data.
        Vector<Result>::iterator itrResult = resultArray.begin();
        for(;itrResult != resultArray.end(); ++itrResult)
        {
            Vector<BinPack<int>::Element>::iterator itrOriginalElement = originalElementArray.begin();
            bool hasFoundOriginalData = false;
            for(;itrOriginalElement != originalElementArray.end(); ++itrOriginalElement)
            {
                if((*itrResult).m_data == (*itrOriginalElement).m_data)
                {
                    // Found orgininal data. Remove it from the array.
                    hasFoundOriginalData = true;

                    // Check that result's rectangle offset position is not negative.
                    CHECK((*itrResult).m_rect.m_x >= 0);
                    CHECK((*itrResult).m_rect.m_y >= 0);

                    // Check that result's rectangle matches the original one.
                    CHECK_EQUAL((*itrResult).m_rect.m_width, (*itrOriginalElement).m_width);
                    CHECK_EQUAL((*itrResult).m_rect.m_height, (*itrOriginalElement).m_height);

                    // Check that result's rectangle does not exceed bin dimension.
                    CHECK((*itrResult).m_rect.m_x + (*itrResult).m_rect.m_width <= binWidth);
                    CHECK((*itrResult).m_rect.m_y + (*itrResult).m_rect.m_height <= binHeight);

                    originalElementArray.erase(itrOriginalElement);
                    break;
                }
            }
            CHECK(hasFoundOriginalData);
            
            // Now make sure the rectangle does not overlap with another one.
            Vector<Result>::iterator itrResult2 = resultArray.begin();
            for(;itrResult2 != resultArray.end(); ++itrResult2) {
                if(itrResult2 == itrResult)
                    continue;

                Rect2D rect1 = (*itrResult).m_rect;
                Rect2D rect2 = (*itrResult2).m_rect;
                bool doesNotOverlap = rect1.m_x >= (rect2.m_x + rect2.m_width) ||
                                      rect1.m_y >= (rect2.m_y + rect2.m_height) || 
                                      rect2.m_x >= (rect1.m_x + rect1.m_width) ||
                                      rect2.m_y >= (rect1.m_y + rect1.m_height);
                CHECK(doesNotOverlap);
            }
        }

        // Original element array should now be empty if all results matched.
        // If data is not empty, then at least one result is missing.
        CHECK(originalElementArray.empty());
    }
}

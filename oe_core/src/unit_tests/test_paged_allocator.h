#include <UnitTest++.h>

#include "../paged_allocator.h"

#include <algorithm>

SUITE(PagedAllocatorTestSuite)
{
    uint Indice = 0;

    struct TestStruct
    {
        TestStruct()
        {
            m_i = Indice++;
            ++m_numStructs;
        }

        ~TestStruct()
        {
            --m_numStructs;
        }

        uint32 m_i;
        char m_dummy;
        static uint32 m_numStructs;
    };

    static const size_t TinyPageSize = sizeof(TestStruct);

    typedef PagedAllocator<TestStruct, TinyPageSize> TinyPagedAllocator;
    typedef PagedAllocator<TestStruct> DefaultPagedAllocator;

    void Test_N_Elements(size_t nElements)
    {
        Indice = 0;

        DefaultPagedAllocator pagedAllocator;
        Vector<uint> indices;
        indices.reserve(nElements);
        Vector<uint> foundIndices;
        foundIndices.reserve(nElements);

        // Add objects.
        TestStruct** objects = new TestStruct*[nElements];
        memset(objects, 0, sizeof(objects));

        for(uint i = 0; i < nElements; ++i)
        {
            objects[i] = pagedAllocator.acquire();
            indices.push_back(i);

            CHECK_EQUAL(i, objects[i]->m_i);
            CHECK_EQUAL(i + 1, TestStruct::m_numStructs);
            CHECK_EQUAL(i + 1, pagedAllocator.getNumElements());
        }

        // Check that linked array contains all added objects.
        for(auto obj : pagedAllocator)
        {
            uint i = obj->m_i;

            // Make sure the index matches one which has been added.
            CHECK(std::find(indices.begin(), indices.end(), i) != indices.end());

            // Make sure an index is found only once.
            CHECK(std::find(foundIndices.begin(), foundIndices.end(), i) == foundIndices.end());

            foundIndices.push_back(i);
        }
        CHECK_EQUAL(nElements, foundIndices.size());

        // Remove objects and make sure they cannot be found after removal.
        Vector<uint> removedIndices;
        removedIndices.reserve(nElements);
        for(uint i = 0; i < nElements; ++i)
        {
            pagedAllocator.release(objects[i]);
            objects[i] = nullptr;
            removedIndices.push_back(i);

            for(auto obj : pagedAllocator)
            {
                CHECK(std::find(removedIndices.begin(), removedIndices.end(), obj->m_i) == removedIndices.end());
            }
        }
        CHECK_EQUAL(0, TestStruct::m_numStructs);

        uint32 nRemaining = 0;
        for(auto obj : pagedAllocator)
        {
            ++nRemaining;
        }
        CHECK_EQUAL(0, nRemaining);

        delete[] objects;
    }

    uint32 TestStruct::m_numStructs = 0;

    TEST(NoElement)
    {
        Indice = 0;
        DefaultPagedAllocator pagedAllocator;

        uint nObjects = 0;
        for(auto obj : pagedAllocator)
        {
            ++nObjects;
        }

        CHECK_EQUAL(0, nObjects);
    }

    TEST(OneElement)
    {
        Indice = 0;
        DefaultPagedAllocator pagedAllocator;

        TestStruct* obj = pagedAllocator.acquire();
        CHECK_EQUAL(1, TestStruct::m_numStructs);
        CHECK_EQUAL(1, pagedAllocator.getNumElements());
        CHECK_EQUAL(0, obj->m_i);

        pagedAllocator.release(obj);
        CHECK_EQUAL(0, TestStruct::m_numStructs);
        CHECK_EQUAL(0, pagedAllocator.getNumElements());
    }

    TEST(TenElements)
    {
        Test_N_Elements(10);
    }

    TEST(DeleteAndAddAgain)
    {
        Indice = 0;
        const uint NumElements = 10;

        DefaultPagedAllocator pagedAllocator;
        Vector<uint> indices;
        indices.reserve(NumElements);
        Vector<uint> foundIndices;
        foundIndices.reserve(NumElements);

        // Add objects.
        TestStruct* objects[NumElements];
        memset(objects, 0, NumElements * sizeof(TestStruct*));
        for(uint i = 0; i < NumElements; ++i)
        {
            indices.push_back(Indice);
            objects[i] = pagedAllocator.acquire();
        }

        // Remove every even indices.
        for(uint i = 0; i < NumElements; i += 2)
        {
            pagedAllocator.release(objects[i]);
            objects[i] = nullptr;
            indices.erase(std::remove(indices.begin(), indices.end(), i), indices.end());
        }

        for(auto obj : pagedAllocator)
        {
            uint i = obj->m_i;
            
            // Make sure the index matches one which still exist.
            CHECK(std::find(indices.begin(), indices.end(), i) != indices.end());

            // Make sure an index is found only once.
            CHECK(std::find(foundIndices.begin(), foundIndices.end(), i) == foundIndices.end());

            foundIndices.push_back(i);
        }
        CHECK_EQUAL(NumElements / 2, foundIndices.size());

        // Add more objects.
        for(uint i = 0; i < NumElements; i += 2)
        {
            indices.push_back(Indice);
            objects[i] = pagedAllocator.acquire();
        }

        foundIndices.clear();
        for(auto obj : pagedAllocator)
        {
            uint i = obj->m_i;

            // Make sure the index matches one which has been added.
            CHECK(std::find(indices.begin(), indices.end(), i) != indices.end());

            // Make sure an index is found only once.
            CHECK(std::find(foundIndices.begin(), foundIndices.end(), i) == foundIndices.end());

            foundIndices.push_back(i);
        }

        pagedAllocator.clear();

        CHECK_EQUAL(indices.size(), foundIndices.size());
    }

    TEST(FillCompletely)
    {
        Indice = 0;

        DefaultPagedAllocator pagedAllocator;

        const size_t NumElements = pagedAllocator.getSingleNodeCapacity();

        Vector<uint> indices;
        indices.reserve(NumElements);

        TestStruct* objects[NumElements];
        memset(objects, 0, NumElements * sizeof(TestStruct*));

        // Add lots of objects.
        for(uint i = 0; i < NumElements; ++i)
        {
            indices.push_back(Indice);
            objects[i] = pagedAllocator.acquire();
            CHECK(objects[i] != nullptr);
            if(objects[i] == nullptr)
            {
                objects[i] = nullptr;
            }
        }

        // The linked array should be full now.
        CHECK(pagedAllocator.isAtCapacity());

        // Remove one every three objects.
        uint nRemovedObjects = 0;
        for(uint i = 0; i < NumElements; i += 3)
        {
            pagedAllocator.release(objects[i]);
            CHECK(!pagedAllocator.isAtCapacity());
            objects[i] = nullptr;
            ++nRemovedObjects;
        }

        // Make sure the same number of objects removed can be re-added.
        for(uint i = 0; i < NumElements; i += 3)
        {
            objects[i] = pagedAllocator.acquire();
            CHECK(objects[i] != nullptr);
            ++nRemovedObjects;
        }
        
        // At this point the array should be full again.
        CHECK(pagedAllocator.isAtCapacity());

        pagedAllocator.clear();
    }

    TEST(OverCapacity)
    {
        DefaultPagedAllocator pagedAllocator;

        const size_t NumElements = pagedAllocator.getSingleNodeCapacity();

        // Add objects until reaching full capacity of one node.
        for(uint32 i = 0; i < NumElements; ++i)
        {
            TestStruct* object = pagedAllocator.acquire();
            CHECK(object != nullptr);
        }

        // The allocator should be full now.
        CHECK(pagedAllocator.isAtCapacity());
        CHECK_EQUAL(1, pagedAllocator.getNumNodes());

        // Add an extra elements.
        TestStruct* extraObject = pagedAllocator.acquire();
        CHECK(extraObject != nullptr);

        // The allocator should not be at capacity after the creation of a new node.
        CHECK(!pagedAllocator.isAtCapacity());
        CHECK_EQUAL(2, pagedAllocator.getNumNodes());

        // Release the extra object.
        pagedAllocator.release(extraObject);
        extraObject = nullptr;

        // The allocattor should be at capacity again after the deletion of the new node.
        CHECK(pagedAllocator.isAtCapacity());
        CHECK_EQUAL(1, pagedAllocator.getNumNodes());

        pagedAllocator.clear();
    }

    TEST(Iterators)
    {
        DefaultPagedAllocator pagedAllocator;

        auto itrBeginAllocator = pagedAllocator.begin();
        auto itrEndAllocator = pagedAllocator.end();

        // No elements yet so begin equals end.
        CHECK(itrBeginAllocator == itrEndAllocator);

        // Check iterators differ after adding an element.
        TestStruct* testObject = pagedAllocator.acquire();
        itrBeginAllocator = pagedAllocator.begin();
        itrEndAllocator = pagedAllocator.end();
        CHECK(itrBeginAllocator != itrEndAllocator);

        // After releasing the unique element, they are equal again.
        pagedAllocator.release(testObject);
        testObject = nullptr;
        itrBeginAllocator = pagedAllocator.begin();
        itrEndAllocator = pagedAllocator.end();
        CHECK(itrBeginAllocator == itrEndAllocator);

        // Fill 2 pages.
        const size_t NumElementsPerPage = pagedAllocator.getPageCapacity();
        const size_t NumElementsFor2Pages = 2 * NumElementsPerPage;
        Indice = 0;
        CHECK_EQUAL(0, pagedAllocator.getNumElements());
        for(uint32 i = 0; i < NumElementsFor2Pages; ++i)
        {
            TestStruct* object = pagedAllocator.acquire();
            CHECK(object != nullptr);
        }

        auto itrBeginPage = pagedAllocator.beginPage(0);
        itrBeginAllocator = pagedAllocator.begin();

        // Both iterators are at the beginning of the allocator.
        CHECK(itrBeginAllocator == itrBeginPage);

        // Iterator at beginning of page 1 is different from page 0.
        itrBeginPage = pagedAllocator.beginPage(1);
        CHECK(itrBeginAllocator != itrBeginPage);

        // The element pointed at beginning of page 1 is equal to NumElementsPerPage.
        CHECK_EQUAL(NumElementsPerPage, (*itrBeginPage)->m_i);

        // Invalid page iterator gives end iterator.
        itrBeginPage = pagedAllocator.beginPage(2);
        itrEndAllocator = pagedAllocator.end();
        CHECK(itrBeginPage == itrEndAllocator);

        itrBeginPage = pagedAllocator.beginPage(245);
        CHECK(itrBeginPage == itrEndAllocator);

        pagedAllocator.clear();
    }
}

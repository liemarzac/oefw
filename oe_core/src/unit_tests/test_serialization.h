
// OE_Core
#include "../oe_math.h"

struct SimpleStruct
{
    int m_int;
    float m_float;
    long m_long;

    bool operator==(const SimpleStruct& rhs) const
    {
        return m_int == rhs.m_int &&
            m_float == rhs.m_float &&
            m_long == rhs.m_long;
    }
};

void serialize_impl(Buffer& buffer, const SimpleStruct& simpleStruct, const SimpleStruct*)
{
    serializeIntrinsic(buffer, simpleStruct.m_int);
    serializeIntrinsic(buffer, simpleStruct.m_float);
    serializeIntrinsic(buffer, simpleStruct.m_long);
}

void deserialize_impl(Buffer& buffer, SimpleStruct& simpleStruct, const SimpleStruct*)
{
    deserializeIntrinsic(buffer, simpleStruct.m_int);
    deserializeIntrinsic(buffer, simpleStruct.m_float);
    deserializeIntrinsic(buffer, simpleStruct.m_long);
}

size_t calculateSerializedSize_impl(const SimpleStruct& simpleStruct, const SimpleStruct*)
{
    return sizeof(simpleStruct.m_int) +
        sizeof(simpleStruct.m_float) +
        sizeof(simpleStruct.m_long);
}

struct ComplexStruct
{
    String m_string;
    SimpleStruct m_simpleStruct;
    int m_int;

    bool operator==(const ComplexStruct& rhs) const
    {
        return m_string == rhs.m_string &&
            m_simpleStruct == rhs.m_simpleStruct &&
            m_int == rhs.m_int;
    }
};

void serialize_impl(Buffer& buffer, const ComplexStruct& complexStruct, const ComplexStruct*)
{
    serialize_impl(buffer, complexStruct.m_string, static_cast<String*>(0));
    serialize_impl(buffer, complexStruct.m_simpleStruct, static_cast<SimpleStruct*>(0));
    serializeIntrinsic(buffer, complexStruct.m_int);
}

void deserialize_impl(Buffer& buffer, ComplexStruct& complexStruct, const ComplexStruct*)
{
    deserialize_impl(buffer, complexStruct.m_string, static_cast<String*>(0));
    deserialize_impl(buffer, complexStruct.m_simpleStruct, static_cast<SimpleStruct*>(0));
    deserializeIntrinsic(buffer, complexStruct.m_int);
}

size_t calculateSerializedSize_impl(const ComplexStruct& complexStruct, const ComplexStruct*)
{
    size_t size = calculateSerializedSize_impl(complexStruct.m_string, static_cast<String*>(0));
    size += calculateSerializedSize_impl(complexStruct.m_simpleStruct, static_cast<SimpleStruct*>(0));
    size += calculateSerializedSizeIntrinsic(complexStruct.m_int);
    return size;
}

SUITE(SerializationTestSuite)
{
    TEST(SerializeSimpleStruct)
    {
        SimpleStruct from;
        from.m_int = INT_MAX;
        from.m_float = (float)PI;
        from.m_long = LONG_MAX;

        const size_t serializationSize = calculateSerializedSize(from);
        Buffer buffer(serializationSize);
        serialize(buffer, from);

        // Check the buffer is full.
        CHECK(buffer.m_offset == buffer.m_size);

        SimpleStruct to;
        buffer.m_offset = 0;
        deserialize(buffer, to);

        // Check deserialised struct is the same as the original one.
        CHECK(from == to);
    }

    TEST(SerializeString)
    {
        String fromString("hello!");

        Buffer buffer(calculateSerializedSize(fromString));
        serialize(buffer, fromString);

        CHECK(buffer.m_offset == buffer.m_size);

        String toString;
        buffer.m_offset = 0;
        deserialize(buffer, toString);

        CHECK_EQUAL("hello!", toString);
    }

    TEST(SerializeComplexStructure)
    {
        ComplexStruct from;
        from.m_int = 4687;
        from.m_simpleStruct.m_int = 314567;
        from.m_simpleStruct.m_float = -562.36f;
        from.m_simpleStruct.m_long = -46548743L;
        from.m_string = "bye bye!";

        const size_t serializationSize = calculateSerializedSize(from);
        Buffer buffer(serializationSize);
        serialize(buffer, from);

        CHECK(buffer.m_offset == buffer.m_size);

        ComplexStruct to;
        buffer.m_offset = 0;
        deserialize(buffer, to);

        CHECK(from == to);
    }

    TEST(SerializeVectorOfIntegers)
    {
        Vector<int> m_fromIntArray;
        m_fromIntArray.push_back(10);
        m_fromIntArray.push_back(20);
        m_fromIntArray.push_back(30);
        m_fromIntArray.push_back(40);

        Buffer buffer(calculateSerializedSize(m_fromIntArray));
        serialize(buffer, m_fromIntArray);
        
        CHECK(buffer.m_offset == buffer.m_size);

        Vector<int> m_toIntArray;
        buffer.m_offset = 0;
        deserialize(buffer, m_toIntArray);

        CHECK(buffer.m_offset == buffer.m_size);

        CHECK_EQUAL(10, m_toIntArray[0]);
        CHECK_EQUAL(20, m_toIntArray[1]);
        CHECK_EQUAL(30, m_toIntArray[2]);
        CHECK_EQUAL(40, m_toIntArray[3]);
    }

    TEST(SerializeVectorOfStucts)
    {
        Vector<ComplexStruct> fromStructArray;
        ComplexStruct a;
        a.m_int = 0;
        a.m_simpleStruct.m_float = 0.0f;
        a.m_simpleStruct.m_int = 0;
        a.m_simpleStruct.m_long = 0L;
        a.m_string = "zero";
        fromStructArray.push_back(a);
        
        ComplexStruct b;
        b.m_int = 1;
        b.m_simpleStruct.m_float = 1.0f;
        b.m_simpleStruct.m_int = 1;
        b.m_simpleStruct.m_long = 1L;
        b.m_string = "one";
        fromStructArray.push_back(b);
        
        ComplexStruct c;
        c.m_int = 0;
        c.m_simpleStruct.m_float = 3.0f;
        c.m_simpleStruct.m_int = 3;
        c.m_simpleStruct.m_long = 3L;
        c.m_string = "three";
        fromStructArray.push_back(c);

        Buffer buffer(calculateSerializedSize(fromStructArray));
        serialize(buffer, fromStructArray);

        CHECK(buffer.m_offset == buffer.m_size);

        Vector<ComplexStruct> toStructArray;
        buffer.m_offset = 0;
        deserialize(buffer, toStructArray);

        CHECK(buffer.m_offset == buffer.m_size);

        CHECK_EQUAL(3, toStructArray.size());
        CHECK(fromStructArray[0] == toStructArray[0]);
        CHECK(fromStructArray[1] == toStructArray[1]);
        CHECK(fromStructArray[2] == toStructArray[2]);
    }

    TEST(SerializeMapOfIntegers)
    {
        Map<String, int> fromMap;
        fromMap["zero"] = 0;
        fromMap["one"] = 1;
        fromMap["two"] = 2;        

        Buffer buffer(calculateSerializedSize(fromMap));
        serialize(buffer, fromMap);
        
        CHECK(buffer.m_offset == buffer.m_size);

        Map<String, int> toMap;
        buffer.m_offset = 0;
        deserialize(buffer, toMap);

        CHECK(buffer.m_offset == buffer.m_size);

        CHECK_EQUAL(3, toMap.size());
        CHECK_EQUAL(0, toMap["zero"]);
        CHECK_EQUAL(1, toMap["one"]);
        CHECK_EQUAL(2, toMap["two"]);
    }

    TEST(SerializeMapOfStruct)
    {
        Map<int, ComplexStruct> fromMap;
        ComplexStruct a;
        a.m_int = 0;
        a.m_simpleStruct.m_float = 0.0f;
        a.m_simpleStruct.m_int = 0;
        a.m_simpleStruct.m_long = 0L;
        a.m_string = "zero";
        fromMap[0] = a;
        
        ComplexStruct b;
        b.m_int = 1;
        b.m_simpleStruct.m_float = 1.0f;
        b.m_simpleStruct.m_int = 1;
        b.m_simpleStruct.m_long = 1L;
        b.m_string = "one";
        fromMap[1] = b;
        
        ComplexStruct c;
        c.m_int = 0;
        c.m_simpleStruct.m_float = 3.0f;
        c.m_simpleStruct.m_int = 3;
        c.m_simpleStruct.m_long = 3L;
        c.m_string = "three";
        fromMap[2] = c;

        Buffer buffer(calculateSerializedSize(fromMap));

        serialize(buffer, fromMap);
        
        CHECK(buffer.m_offset == buffer.m_size);

        Map<int, ComplexStruct> toMap;
        buffer.m_offset = 0;
        deserialize(buffer, toMap);

        CHECK(buffer.m_offset == buffer.m_size);

        CHECK_EQUAL(3, toMap.size());
        CHECK(a == toMap[0]);
        CHECK(b == toMap[1]);
        CHECK(c == toMap[2]);
    }

    TEST(SerializeMapOfPointersOfStruct)
    {
        Map<int, ComplexStruct> fromMap;
        ComplexStruct a;
        a.m_int = 0;
        a.m_simpleStruct.m_float = 0.0f;
        a.m_simpleStruct.m_int = 0;
        a.m_simpleStruct.m_long = 0L;
        a.m_string = "zero";
        fromMap[0] = a;
        
        ComplexStruct b;
        b.m_int = 1;
        b.m_simpleStruct.m_float = 1.0f;
        b.m_simpleStruct.m_int = 1;
        b.m_simpleStruct.m_long = 1L;
        b.m_string = "one";
        fromMap[1] = b;
        
        ComplexStruct c;
        c.m_int = 0;
        c.m_simpleStruct.m_float = 3.0f;
        c.m_simpleStruct.m_int = 3;
        c.m_simpleStruct.m_long = 3L;
        c.m_string = "three";
        fromMap[2] = c;

        Buffer buffer(calculateSerializedSize(fromMap));

        serialize(buffer, fromMap);
        
        CHECK(buffer.m_offset == buffer.m_size);

        Map<int, ComplexStruct> toMap;
        buffer.m_offset = 0;
        deserialize(buffer, toMap);

        CHECK(buffer.m_offset == buffer.m_size);

        CHECK_EQUAL(3, toMap.size());
        CHECK(a == toMap[0]);
        CHECK(b == toMap[1]);
        CHECK(c == toMap[2]);
    }

    TEST(SerializeBuffer)
    {
        Buffer fromBuffer(50);
        memset(fromBuffer.m_rawData, 5, fromBuffer.m_size);
        
        Buffer intermediateBuffer(calculateSerializedSize(fromBuffer));
        serialize(intermediateBuffer, fromBuffer);

        CHECK(intermediateBuffer.m_offset == intermediateBuffer.m_size);
        
        Buffer toBuffer;
        intermediateBuffer.m_offset = 0;
        deserialize(intermediateBuffer, toBuffer);

        CHECK(intermediateBuffer.m_offset == intermediateBuffer.m_size);
        CHECK(toBuffer.m_size == fromBuffer.m_size);

        CHECK_EQUAL(0, memcmp(fromBuffer.m_rawData, toBuffer.m_rawData, fromBuffer.m_size));
    }

}

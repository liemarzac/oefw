#include <UnitTest++.h>

#include "../ring_buffer.h"
#include "../std_atomic_helpers.h"

SUITE(RingBufferTestSuite)
{
    std::atomic<uint64> g_ringBufferSum;
    AtomicBool g_hasFinishedWritting;

    void writerSum(RingBuffer* ring, uint from, uint to)
    {
        for(uint i = from; i <= to; ++i)
        {
            void* ptr = nullptr;

            while(!ring->alloc(ptr, sizeof(uint)));

            *(uint*)ptr = i;
            ring->commit();
            //log("Debug", "Write %d", i);
        }
    }

    void readerSum(RingBuffer* ring)
    {
        void* ptr = nullptr;

        size_t availableSize = 0;
        while(true)
        {
            if(ring->beginRead(ptr, availableSize))
            {
                uint value = *(uint*)ptr;
                g_ringBufferSum += value;
                ring->endRead(sizeof(uint));
                //log("Debug", "Read %d, Total %d", value, g_ringBufferSum);
            }
            else
            {
                if(g_hasFinishedWritting)
                    break;
            }
        }
    }

    void readStr(RingBuffer* ring, char* result, size_t maxSize)
    {
        size_t maxRemaining = maxSize;
        char* readPtr = result;

        // Read until an end string marker is found.
        while(true)
        {
            size_t readBytes = ring->read(readPtr, maxRemaining);
            if(readBytes > 0)
            {
                readPtr += readBytes;
                if(*(readPtr - 1) == 0)
                    break;
            }
        }
    }

    void writeStr(RingBuffer* ring, const char* sample)
    {
        const char* writePtr = sample;

        // Write sample in the ring buffer one character at a time.
        while(true)
        {
            if(ring->write(writePtr, sizeof(char)))
            {
                if(*writePtr == 0)
                    break;

                ++writePtr;
            }
        }
    }

    struct RingBufferStructTest
    {
        int m_size;
        uint m_i;
    };

    void writeClass(RingBuffer* ring, uint from, uint to)
    {
        RingBufferStructTest writeStruct;

        for(uint i = from; i <= to; ++i)
        {
            // Write between 12 and 20 bytes per struct.
            // The modulo only add padding at the end of the structure,
            // but does not represent any valid data.
            writeStruct.m_size = sizeof(RingBufferStructTest) + (i % 8);
            writeStruct.m_i = i;

            void* ptr = nullptr;
            while(!ring->alloc(ptr, writeStruct.m_size));
            memcpy(ptr, &writeStruct, sizeof(RingBufferStructTest));
            ring->commit();
        }
    }

    void readClass(RingBuffer* ring)
    {
        void* ptr = nullptr;
        size_t availableSize = 0;
        while(true)
        {
            if(ring->beginRead(ptr, availableSize))
            {
                RingBufferStructTest* toRead = (RingBufferStructTest*)ptr;
                g_ringBufferSum += toRead->m_i;
                ring->endRead(toRead->m_size);
            }
            else
            {
                if(g_hasFinishedWritting)
                    break;
            }
        }
    }

    void writeRawData(RingBuffer* ring, size_t size, uint nWrites)
    {
        void* ptr = nullptr;
        size_t availableSize = 0;
        OE_CHECK(size > sizeof(uint), OE_CHANNEL_CORE);
        static uint Marker = 0xfeedcafe;
        for(uint i = 0; i < nWrites; ++i)
        {
            void* ptr = nullptr;
            while(!ring->alloc(ptr, size));
            (*(uint*)(ptr)) = Marker;
            ring->commit();
        }
    }

    void readRawData(RingBuffer* ring, size_t size, uint nWrites)
    {
        void* ptr = nullptr;
        size_t availableSize = 0;
        uint nRead = 0;
        static uint Marker = 0xfeedcafe;
        while(true)
        {
            if(ring->beginRead(ptr, availableSize))
            {
                CHECK((*(uint*)ptr) == Marker);
                ring->endRead(size);
                ++nRead;

                if(nRead == nWrites)
                    break;
            }
        }
    }

    TEST(SimpleReadWrite)
    {
        // Alloc 5 bytes with 1 byte alignment (characters do not need to be aligned).
        RingBuffer ring(5, false, 1);

        const size_t ResultMaxSize = 128;

        // The sample string will be copied to result via the ring buffer.
        const char* sample = "hello ring buffer!";
        char result[ResultMaxSize];

        std::thread reader(readStr, &ring, result, ResultMaxSize);
        std::thread writer(writeStr, &ring, sample);

        reader.join();
        writer.join();

        int diff = strcmp(sample, result);
        CHECK_EQUAL(0, diff);
    }

    TEST(NothingToRead)
    {
        RingBuffer ring(10, false, 1);

        bool hasRead = false;
        void* ptr;
        size_t sizeAvailable = 0;

        CHECK_EQUAL(true, ring.isEmpty());

        if(ring.beginRead(ptr, sizeAvailable))
        {
            hasRead = true;
        }

        CHECK_EQUAL(false, hasRead);
    }

    TEST(TooSmallToWrite)
    {
        RingBuffer ring(10, false, 1);

        void* ptr = nullptr;
        bool canWrite = ring.alloc(ptr, 11);

        CHECK_EQUAL(false, canWrite);
        CHECK_EQUAL(true, ring.isEmpty());
        CHECK(ptr == nullptr);
    }

    TEST(CanWriteOneByte)
    {
        RingBuffer ring(10, false, 1);

        void* ptr = nullptr;
        bool canWrite = ring.alloc(ptr, 1);

        CHECK_EQUAL(true, canWrite);

        //The ring buffer should still be considered empty
        // because the write has not been committed yet.
        CHECK_EQUAL(true, ring.isEmpty());
        CHECK(ptr != nullptr);
    }

    TEST(WriteAndCannotReadBeforeCommit)
    {
        RingBuffer ring(10, false, 1);

        void* writePtr = nullptr;
        bool canWrite = false;

        if(ring.alloc(writePtr, 1))
        {
            *(uint8*)writePtr = 19;
            canWrite = true;

            // Notice there is no commit.
        }

        void* readPtr = nullptr;
        bool canRead = false;
        size_t availableSize = 0;
        if(ring.beginRead(readPtr, availableSize))
        {
            canRead = true;
        }

        CHECK_EQUAL(true, canWrite);
        CHECK_EQUAL(false, canRead);
        // The ring buffer should still be considered empty
        // because the write has not been committed yet.
        CHECK_EQUAL(true, ring.isEmpty());
        CHECK_EQUAL(0, availableSize);
        CHECK(readPtr == nullptr);
    }

    TEST(WriteAndRead)
    {
        RingBuffer ring(10, false, 1);

        void* writePtr = nullptr;
        bool canWrite = false;

        if(ring.alloc(writePtr, 1))
        {
            *(uint8*)writePtr = 19;
            canWrite = true;

            // Commit to the buffer so it can be read.
            ring.commit();
        }

        CHECK_EQUAL(true, !ring.isEmpty());

        void* readPtr = nullptr;
        bool canRead = false;
        size_t availableSize = 0;
        uint8 readBackValue = 0;
        if(ring.beginRead(readPtr, availableSize))
        {
            canRead = true;
            readBackValue = *(uint8*)readPtr;
            ring.endRead(1);
        }

        CHECK_EQUAL(true, canWrite);
        CHECK_EQUAL(true, canRead);
        CHECK_EQUAL(1, availableSize);
        CHECK_EQUAL(19, readBackValue);

        // Cannot read again.
        CHECK(!ring.beginRead(readPtr, availableSize));
    }

    TEST(OneWriteThreadAndOneReadThread)
    {
        RingBuffer ring(4 * sizeof(uint), false, 4);

        g_hasFinishedWritting = false;
        g_ringBufferSum = 0;

        uint from = 1;
        uint to = 10000;

        const uint64 expectedSum = ((uint64)to * (to + 1)) / 2;

        std::thread producer(writerSum, &ring, from, to);
        std::thread consumer(readerSum, &ring);

        producer.join();
        g_hasFinishedWritting = true;

        consumer.join();

        CHECK_EQUAL(expectedSum, g_ringBufferSum);
        ring.clear();
    }

    TEST(WriteAndReadStructWithAlignment)
    {
        RingBuffer ring(10 * sizeof(RingBufferStructTest), false, 4);

        g_hasFinishedWritting = false;
        g_ringBufferSum = 0;

        uint to = 100;

        const uint64 expectedSum = ((uint64)to * (to + 1)) / 2;

        std::thread producer(writeClass, &ring, 1, 100);
        std::thread consumer(readClass, &ring);

        producer.join();
        g_hasFinishedWritting = true;

        consumer.join();

        CHECK_EQUAL(expectedSum, g_ringBufferSum);
    }

    TEST(FourWriteThreadAndFourReadThread)
    {
        RingBuffer ring(4 * sizeof(uint), false, 4);

        g_hasFinishedWritting = false;
        g_ringBufferSum = 0;

        uint to = 100;

        const uint64 expectedSum = ((uint64)to * (to + 1)) / 2;

        std::thread producer1(writerSum, &ring, 1, 24);
        std::thread producer2(writerSum, &ring, 25, 49);
        std::thread producer3(writerSum, &ring, 50, 74);
        std::thread producer4(writerSum, &ring, 75, 100);

        std::thread consumer1(readerSum, &ring);
        std::thread consumer2(readerSum, &ring);
        std::thread consumer3(readerSum, &ring);
        std::thread consumer4(readerSum, &ring);

        producer1.join();
        producer2.join();
        producer3.join();
        producer4.join();
        g_hasFinishedWritting = true;

        consumer1.join();
        consumer2.join();
        consumer3.join();
        consumer4.join();

        CHECK_EQUAL(expectedSum, g_ringBufferSum);
    }

    TEST(BigRingBufferOfRawData)
    {
        static const size_t ringBufferSize = 1 * OneMb;

        // +1 so the buffer size is not a multiple of the write size to stress test alignment.
        static const size_t writeSize = (10 * OneKb) + 1;

        static const size_t nMinFillTimes = 3;
        RingBuffer ring(ringBufferSize, false, 4);

        uint nWrites = (ringBufferSize / writeSize) * nMinFillTimes;

        std::thread consumer(readRawData, &ring, writeSize, nWrites);
        std::thread producer(writeRawData, &ring, writeSize, nWrites);

        producer.join();
        consumer.join();
    }

    TEST(ExpendingRingBuffer)
    {
        // Give no size.
        // The ring buffer should automatically reallocate memory.
        RingBuffer ring;

        g_hasFinishedWritting = false;
        g_ringBufferSum = 0;

        uint to = 1000000;
        const uint64 expectedSum = ((uint64)to * (to + 1)) / 2;

        // Create more producers than consumers to force the buffer to re-allocate
        std::thread producer1(writerSum, &ring, 1, 100000);
        std::thread producer2(writerSum, &ring, 100001, 200000);
        std::thread producer3(writerSum, &ring, 200001, 300000);
        std::thread producer4(writerSum, &ring, 300001, 400000);
        std::thread producer5(writerSum, &ring, 400001, 500000);
        std::thread producer6(writerSum, &ring, 500001, 600000);
        std::thread producer7(writerSum, &ring, 600001, 700000);
        std::thread producer8(writerSum, &ring, 700001, 800000);
        std::thread producer9(writerSum, &ring, 800001, 900000);
        std::thread producer10(writerSum, &ring, 900001, 1000000);
        std::thread consumer1(readerSum, &ring);

        producer1.join();
        producer2.join();
        producer3.join();
        producer4.join();
        producer5.join();
        producer6.join();
        producer7.join();
        producer8.join();
        producer9.join();
        producer10.join();

        g_hasFinishedWritting = true;

        consumer1.join();

        CHECK_EQUAL(expectedSum, g_ringBufferSum);
    }
}

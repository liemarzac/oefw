#ifndef __OE_CORE_MEMORY_H__
#define __OE_CORE_MEMORY_H__

namespace OE_Core
{

// Constants.
static const size_t OneKb = 1024;
static const size_t OneMb = OneKb * 1024;
static const size_t OneGb = OneMb * 1024;

inline constexpr void* align(const void* ptr, size_t alignment)
{
    return (void*)(((uintptr_t)ptr + (alignment - 1)) & ~(uintptr_t)(alignment - 1));
}

inline constexpr size_t align(size_t size, size_t alignment)
{
    return (size_t)(((uintptr_t)size + (alignment - 1)) & ~(uintptr_t)(alignment - 1));
}

#if defined(_MSC_VER)
#define ALIGNED_(x) __declspec(align(x))
#define ALIGNED_MALLOC(PTR, SIZE, ALIGNEMENT) PTR = _aligned_malloc(SIZE, ALIGNEMENT)
#define ALIGNED_FREE(PTR) _aligned_free(PTR)
#elif defined(__GNUC__)
#define ALIGNED_(x) __attribute__ ((aligned(x)))
#define ALIGNED_MALLOC(PTR, SIZE, ALIGNEMENT) PTR = memalign(ALIGNEMENT, SIZE);
#define ALIGNED_FREE(PTR) free(PTR)
#endif

}

#endif // __OE_CORE_MEMORY_H__

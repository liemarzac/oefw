#include "ring_buffer.h"

#include "core_private.h"

// External
#include <cstring>

// OE_Core
#include "oe_math.h"
#include "memory.h"
#include "std_mutex_helpers.h"

namespace OE_Core
{

RingBuffer::RingBuffer()
{
    m_rawBufferStart = nullptr;
    m_userBufferStart = nullptr;
    m_rawBufferEnd = nullptr;
    m_alignment = 16;
    m_size = 0;
    m_read = m_write = m_readEnd = m_dataEnd = m_userBufferStart;
    m_isReading = false;
    m_isWritting = false;
    m_reallocRatio = 1.33f;
    m_flags.set(Flags::AllowResize);
}

RingBuffer::RingBuffer(size_t size, bool allowResize, uint alignment, float reallocRatio)
{
    m_alignment = max<uint>(1, alignment);
    const size_t allocSize = size + m_alignment - 1;
    m_size = allocSize;
    m_rawBufferStart = (uint8*)malloc(m_size);
    m_userBufferStart = (uint8*)align(m_rawBufferStart, alignment);
    m_rawBufferEnd = m_userBufferStart + size;
    m_read = m_write = m_readEnd = m_dataEnd = m_userBufferStart;
    m_isReading = false;
    m_isWritting = false;
    m_reallocRatio = reallocRatio;

    if(allowResize)
    {
        m_flags.set(Flags::AllowResize);
    }
}

RingBuffer::~RingBuffer()
{
    if(m_rawBufferStart)
        free(m_rawBufferStart);
}

size_t RingBuffer::read(void* ptr, size_t maxSize)
{
    size_t copySize = 0;
    size_t available = 0;
    void* readPtr = nullptr;

    if(beginRead(readPtr, available))
    {
        copySize = min(available, maxSize);
        memcpy(ptr, readPtr, copySize);
        endRead(copySize);
    }

    return copySize;
}

bool RingBuffer::write(const void* ptr, size_t size)
{
    void* writePtr = nullptr;
    bool hasWritten = false;

    if(alloc(writePtr, size))
    {
        memcpy(writePtr, ptr, size);
        commit();
        hasWritten = true;
    }

    return hasWritten;
}

bool RingBuffer::alloc(void*& outPtr, size_t size)
{
    bool expected = false;
    while(!m_isWritting.compare_exchange_weak(expected, true))
        expected = false;

    bool ret = allocInternal(outPtr, size);

    if(!ret)
    {
        m_isWritting = false;
    }

    return ret;
}

bool RingBuffer::allocInternal(void*& outPtr, size_t size)
{
    if(m_write.load() == nullptr)
    {
        // The ring buffer has not been allocated yet.
        // Alloc the initial size.
        resize(size_t(size * double(m_reallocRatio)));
    }

    AllocResult res = AllocResult::Recall;
    while(res == AllocResult::Recall)
    {
        uint8* write = m_write;
        uint8* read = m_read;
        uint8* readEnd = m_readEnd;

        if(write < read)
        {
            res = allocPreRead(outPtr, size);
        }
        else
        {
            res = allocPostRead(outPtr, size);
        }
    }

    return res == AllocResult::Succeed;
}

RingBuffer::AllocResult RingBuffer::allocPreRead(void*& ptr, size_t size)
{
    // Write pointer cannot exceed read pointer.
    uint8* read = m_read;
    uint8* write = m_write;

    // This function only handle when write pointer is strictly lower than read.
    if(write >= read)
    {
        // Read pointer wrapped around after allocInternal tested
        // the relative position of read/write pointers.
        return AllocResult::Recall;
    }

    uint8* postWrite = (uint8*)align(write + size, m_alignment);

    if(postWrite >= read)
    {
        if(m_flags.isSet(Flags::AllowResize))
        {
            // There is not enough free space to fit the new allocation before the read pointer.
            // The ring buffer has to be resized.
            resize(size_t((m_size + size) * m_reallocRatio));
            return AllocResult::Recall;
        }
        else
        {
            return AllocResult::Fail;
        }
    }

    ptr = write;
    m_write = postWrite;
    
    return AllocResult::Succeed;
}

RingBuffer::AllocResult RingBuffer::allocPostRead(void*& ptr, size_t size)
{
    // Write pointer cannot exceed end of buffer.
    uint8* read = m_read;
    uint8* write = m_write;

    // This function only handle when write pointer is greater or equal to read pointer.
    OE_CHECK(write >= read, OE_CHANNEL_CORE);

    uint8* postWrite = (uint8*)align(write + size, m_alignment);

    if(postWrite >= m_rawBufferEnd)
    {
        if(wrapWrite())
        {
            return AllocResult::Recall;
        }
        else
        {
            if(m_flags.isSet(Flags::AllowResize))
            {
                // There is not enough free space to fit the new allocation before the end
                // of the ring buffer.
                // The ring buffer has to be resized.
                resize(size_t((m_size + size) * m_reallocRatio));
                return AllocResult::Recall;
            }
            else
            {
                return AllocResult::Fail;
            }
        }
    }

    ptr = write;
    m_write = postWrite;
    
    return AllocResult::Succeed;
}

void RingBuffer::resize(size_t size)
{
    // The read pointer has to be locked so not more read can progress during the resize operation.
    bool expected = false;
    while(!m_isReading.compare_exchange_weak(expected, true))
        expected = false;

    const size_t rawBufferSize = size + m_alignment - 1;
    uint8* rawBufferStart = (uint8*)malloc(rawBufferSize);
    
    uint8* userBufferStart = (uint8*)align(rawBufferStart, m_alignment);

    uint8* write = m_write;
    uint8* read = m_read;
    uint8* dataEnd = m_dataEnd;
    size_t copiedSize = 0;

    if(write)
    {
        if(write >= read)
        {
            // Only copy between read and write.
            const size_t blockSize = write - read;
            if(blockSize > 0)
            {
                OE_CHECK(userBufferStart + blockSize <= userBufferStart + size, OE_CHANNEL_CORE);
                memcpy(userBufferStart, read, blockSize);
                copiedSize += blockSize;
            }
        }
        else
        {
            // Copy between read and data end
            // And between buffer and write.
            const size_t firstBlockSize = dataEnd - read;
            if(firstBlockSize > 0)
            {
                OE_CHECK(userBufferStart + firstBlockSize <= rawBufferStart + rawBufferSize, OE_CHANNEL_CORE);
                memcpy(userBufferStart, read, firstBlockSize);
                copiedSize += firstBlockSize;
            }

            const size_t secondBlockSize = write - m_userBufferStart;
            if(secondBlockSize > 0)
            {
                uint8* secondBlockStart = userBufferStart + firstBlockSize;
                OE_CHECK(secondBlockStart + secondBlockSize <= rawBufferStart + rawBufferSize, OE_CHANNEL_CORE);
                memcpy(secondBlockStart, m_userBufferStart, secondBlockSize);
                copiedSize += secondBlockSize;
            }
        }
    }

    // Delete previous raw buffer now that its data has been copied to the new one.
    if(m_rawBufferStart)
        delete m_rawBufferStart;

    m_rawBufferStart = rawBufferStart;
    m_rawBufferEnd = m_rawBufferStart + rawBufferSize;
    m_userBufferStart = userBufferStart;
    m_size = rawBufferSize;
    m_read = m_userBufferStart;

    uint8* copiedEnd = m_userBufferStart + copiedSize;
    m_write = copiedEnd;
    m_readEnd = copiedEnd;
    m_dataEnd = copiedEnd;

    // Release reading lock.
    m_isReading = false;
}

bool RingBuffer::wrapWrite()
{
    uint8* read = m_read;
    uint8* write = m_write;

    if(read == m_userBufferStart)
        return false;

    m_dataEnd = write;
    m_write = m_userBufferStart;

    return true;
}

void RingBuffer::commit()
{
    uint8* write = m_write;
    uint8* dataEnd = m_dataEnd;
    uint8* readEnd = write == m_userBufferStart ? dataEnd : write;

    // Data end can never be lower than read end.
    if(dataEnd < readEnd)
        m_dataEnd.store(readEnd, std::memory_order_relaxed);

    m_readEnd.store(readEnd, std::memory_order_release);

    UniqueLock lock(m_commandSync);
    m_commandNotEmptyNotifier.notify_all();

    m_isWritting = false;
}

bool RingBuffer::beginRead(void*& outPtr, size_t& availableSize)
{
    bool expected = false;
    while(!m_isReading.compare_exchange_weak(expected, true))
        expected = false;


    uint8* read = m_read;
    uint8* readEnd = m_readEnd.load(std::memory_order_acquire);
    uint8* dataEnd = m_dataEnd.load(std::memory_order_relaxed);


    OE_CHECK(dataEnd >= readEnd, OE_CHANNEL_CORE);

    if(read == readEnd)
    {
        // Nothing to read.
        availableSize = 0;
        m_isReading = false;
        return false;
    }

    if(read == dataEnd)
    {
        // Wrap around.
        read = m_userBufferStart;
        m_read = read;
    }

    if(readEnd > read)
    {
        availableSize = readEnd - read;
    }
    else
    {
        availableSize = dataEnd - read;
    }

    outPtr = read;
    return true;
}

void RingBuffer::endRead(size_t readBytes)
{
    uint8* read = m_read;
    uint8* dataEnd = m_dataEnd;
    uint8* readEnd = m_readEnd;

    uint8* postReadPostAlign = (uint8*)align(read + readBytes, m_alignment);

    OE_CHECK(readEnd != read, OE_CHANNEL_CORE);

#if _DEBUG
    memset(read, 0xcd, readBytes);
#endif

    if(readEnd > read)
    {
        read = min(postReadPostAlign, readEnd);
    }
    else
    {
        read = min(postReadPostAlign, dataEnd);
    }

    m_read = read;

    if(isEmpty())
    {
        UniqueLock lock(m_commandSync);
        m_commandEmptyNotifier.notify_all();
    }

    m_isReading = false;
}

void RingBuffer::clear()
{
    bool readExpected = false;
    while(!m_isReading.compare_exchange_weak(readExpected, true));
        readExpected = false;

    bool writeExpected = false;
    while(!m_isWritting.compare_exchange_weak(writeExpected, true));
        writeExpected = false;

    m_write = m_userBufferStart;
    m_readEnd = m_userBufferStart;
    m_read = m_userBufferStart;
    m_rawBufferEnd = m_userBufferStart;

    m_isReading = false;
    m_isWritting = false;
}

bool RingBuffer::isEmpty() const
{
    uint8* read = m_read;
    uint8* readEnd = m_readEnd;

    return read == readEnd;
}

void RingBuffer::waitUntilEmpty(uint32 timeout)
{
    if(isEmpty())
        return;

    UniqueLock lock(m_commandSync);
    if(!isEmpty())
    {
        if(timeout > 0)
        {
            m_commandEmptyNotifier.wait_for(lock, std::chrono::milliseconds(timeout), [this]{return isEmpty(); });
        }
        else
        {
            m_commandEmptyNotifier.wait(lock, [this]{return isEmpty(); });
        }
    }
}

void RingBuffer::waitUntilNotEmpty(uint32 timeout)
{
    if(!isEmpty())
        return;

    UniqueLock lock(m_commandSync);
    if(isEmpty())
    {
        if(timeout > 0)
        {
            m_commandNotEmptyNotifier.wait_for(lock, std::chrono::milliseconds(timeout), [this]{return !isEmpty(); });
        }
        else
        {
            m_commandNotEmptyNotifier.wait(lock, [this]{return !isEmpty(); });
        }
    }
}


} // namespace OE_Core

#include "resource_manager.h"

#include "core_private.h"

// OE_Core
#include "package_resource.h"
#include "resource.h"


// External
#include <thread>

namespace OE_Core
{

ResourceManager::ResourceManager()
{
    m_dependencyCallback = std::bind(
        &ResourceManager::createDependency,
        this,
        std::placeholders::_1);
}

void ResourceManager::update()
{
    auto& pendingLoadResources = m_loadingResources[LoadStage::PendingLoad];
    auto& loadingResources = m_loadingResources[LoadStage::Loading];
    auto& waitingForDependenciesResources = m_loadingResources[LoadStage::WaitForDependencies];

    // Load pending resources.
    for(int iResource = int(pendingLoadResources.size()) - 1; iResource >= 0; --iResource)
    {
        SharedPtr<Resource> resource = pendingLoadResources[iResource].lock();
        if(resource)
        {
            resource->load();
            loadingResources.push_back(resource);
        }
    }
    pendingLoadResources.clear();

    // Update loading resources.
    for(int iResource = int(loadingResources.size()) - 1; iResource >= 0; --iResource)
    {
        SharedPtr<Resource> resource = loadingResources[iResource].lock();
        if(resource)
        {
            if(resource->getStateEx() == Resource::State::Loaded)
            {
                waitingForDependenciesResources.push_back(resource);
                removeAtUnordered(loadingResources, (uint)iResource);
            }
            else
            {
                resource->updateLoading(m_dependencyCallback);
            }
        }
        else
        {
            // Resource had lost its owners whilst loading.
            removeAtUnordered(loadingResources, (uint)iResource);
        }
    }

    // Wait for dependencies
    for(int iResource = int(waitingForDependenciesResources.size()) - 1; iResource >= 0; --iResource)
    {
        SharedPtr<Resource> resource = waitingForDependenciesResources[iResource].lock();
        if(resource)
        {
            if(resource->getState() == Resource::State::Loaded)
            {
                resource->onLoaded();
                removeAtUnordered(waitingForDependenciesResources, (uint)iResource);
            }
            else
            {
#ifdef _DEBUG
                Vector<SharedResource> dependencies;
                resource->getDependencies(dependencies);

                // Check that all dependencies which are not loaded yet are in
                // a loading stage array.
                for(auto dependency : dependencies)
                {
                    if(dependency->getStateEx() != Resource::State::Loaded)
                    {
                        bool bIsPendingLoad = isInLoadStageInternal(dependency, LoadStage::PendingLoad);
                        if(!bIsPendingLoad)
                        {
                            bool bIsLoading = isInLoadStageInternal(dependency, LoadStage::Loading);
                            if(!bIsLoading)
                            {
                                bool bIsWaitingForDependencies = isInLoadStageInternal(dependency, LoadStage::WaitForDependencies);
                                if(!bIsWaitingForDependencies)
                                {
                                    OE_CHECK(FALSE, OE_CHANNEL_RESOURCE);
                                }
                            }
                        }
                    }
                }
#endif
            }
        }
        else
        {
            // Resource had lost its owners whilst loading.
            removeAtUnordered(waitingForDependenciesResources, (uint)iResource);
        }
    }
}

bool ResourceManager::isInLoadStageInternal(SharedResource resource, LoadStage::Enum loadStage)
{
    auto& loadStageResources = m_loadingResources[loadStage];
    for(uint i = 0; i < loadStageResources.size(); ++i)
    {
        SharedResource rhs = loadStageResources[i].lock();
        if(resource == rhs)
        {
            return true;
        }
    }

    return false;
}

void ResourceManager::flushLoading()
{
    while(isLoading())
    {
        update();
        std::this_thread::yield();
    }
}

void ResourceManager::iterateResources(std::function<void(SharedPtr<Resource>)> callback)
{
    LockGuard lock(m_resourceSync);

    for(auto& resourceTypePair : m_resources)
    {
        for(auto& resourcePair : resourceTypePair.second)
        {
            auto resource = resourcePair.second.lock();
            if(resource)
            {
                callback(resource);
            }
        }
    }
}

void ResourceManager::logResources()
{
    iterateResources([this](SharedPtr<Resource> r) {
        log(OE_CHANNEL_RESOURCE, "Active resource: Type %s - Name %s", r->getType(), r->getName().c_str());
    });
}

void ResourceManager::scheduleResourceLoad(SharedPtr<Resource> resourceToLoad)
{
    OE_CHECK(resourceToLoad, OE_CHANNEL_RESOURCE);
    OE_CHECK(resourceToLoad->getStateEx() == Resource::State::Unloaded, OE_CHANNEL_RESOURCE);
    if(!isInLoadStageInternal(resourceToLoad, LoadStage::PendingLoad))
    {
        m_loadingResources[LoadStage::PendingLoad].push_back(resourceToLoad);
    }
}


SharedPtr<Resource> ResourceManager::getResource(Hash type, const String& name)
{
    LockGuard lock(m_resourceSync);
    return getResourceInternal(type, name);
}

SharedPtr<Resource> ResourceManager::getResourceInternal(Hash type, const String& name)
{
    ResourceTypeMap::iterator resourceTypeItr = m_resources.find(type);
    if(resourceTypeItr == m_resources.end())
    {
        return nullptr;
    }

    ResourceMap& resourceMap = (*resourceTypeItr).second;
    ResourceMap::iterator resourceItr = resourceMap.find(hash(name));

    if(resourceItr == resourceMap.end())
    {
        return nullptr;
    }

    SharedPtr<Resource> resource = (*resourceItr).second.lock();
    if(resource.get() == nullptr)
    {
        return nullptr;
    }

    return resource;
}

SharedPtr<Resource> ResourceManager::createDependency(const Resource::DependencyCallbackParams& params)
{
    OE_CHECK(params.m_name.length() > 0 || params.m_explicitResource, OE_CHANNEL_RESOURCE);

    SharedPtr<Resource> resource;

    if(params.m_explicitResource)
    {
        const Hash nameHash = params.m_explicitResource->getHash();
        m_resources[params.m_typeHash][nameHash] = params.m_explicitResource;
        resource = params.m_explicitResource;
    }
    else
    {
        resource = createResource(params.m_typeHash, params.m_name);
    }

    if(resource->getStateEx() == Resource::State::Unloaded)
    {
        scheduleResourceLoad(resource);
    }

    return resource;
}

SharedPtr<Resource> ResourceManager::createResource(Hash typeHash, const String& name)
{
    LockGuard lock(m_resourceSync);
    return createResourceInternal(typeHash, name);
}

SharedPtr<Resource> ResourceManager::createResourceInternal(Hash typeHash, const String& name)
{
    SharedPtr<Resource> resource;
    const Hash nameHash = hash(name);

    // Try to find the resource.
    bool bExist = false;
    auto itrType = m_resources.find(typeHash);
    if(itrType != m_resources.end())
    {
        auto& perTypeResourceMap = (*itrType).second;
        auto itrResource = perTypeResourceMap.find(nameHash);
        if(itrResource != perTypeResourceMap.end())
        {
            resource = (*itrResource).second.lock();
            bExist = true;
        }
    }

    if(!bExist)
    {
        // The resource does not exist, create it.
        resource = makeProduct(typeHash, name);
        m_resources[typeHash][nameHash] = resource;
    }

    return resource;
}

SharedPtr<Resource> ResourceManager::loadPackage(Hash hash, const String& name)
{
    LockGuard lock(m_resourceSync);
    return loadPackageInternal(hash, name);
}

void ResourceManager::unloadPackage(const String& name)
{
    LockGuard lock(m_resourceSync);

    for(size_t i = 0; i < m_packages.size(); ++i)
    {
        if(m_packages[i]->getName() == name)
        {
            removeAtUnordered(m_packages, i);
            break;
        }
    }
}

void ResourceManager::addToPackage(SharedResource resource, const String& packageName)
{
    auto& packageResource = getResource<PackageResource>(packageName);
    packageResource->addResource(m_dependencyCallback, resource);
}

bool ResourceManager::isPackageLoaded(const String& name) const
{
    LockGuard lock(m_resourceSync);

    for(auto package : m_packages)
    {
        if(package->getName() == name)
        {
            if(package->isLoaded())
            {
                return true;
            }
        }
    }

    return false;
}

SharedPtr<Resource> ResourceManager::loadPackageInternal(Hash hash, const String& name)
{
    SharedPtr<Resource> packageResource = createResourceInternal(hash, name);

    OE_CHECK(!isContained(m_packages, packageResource), OE_CHANNEL_RESOURCE);
    OE_CHECK(packageResource->getStateEx() == Resource::State::Unloaded, OE_CHANNEL_RESOURCE)
        ;
    m_packages.push_back(packageResource);
    scheduleResourceLoad(packageResource);

    return packageResource;
}

} // namespace OE_Core

#ifndef __OE_CORE_STD_STRING_HELPERS_H__
#define __OE_CORE_STD_STRING_HELPERS_H__

#include "core_private.h"

#include <algorithm>
#include <string>

void strToLower(String& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

#endif // __OE_CORE_STD_STRING_HELPERS_H__
#ifndef __OE_CORE_JSON_HELPERS_H__
#define __OE_CORE_JSON_HELPERS_H__

bool jsonValueToVec3(const Json::Value& jsonValue, Vec3& vec3)
{
    if (!jsonValue.isArray())
        return false;
        
    if (jsonValue.size() != 3)
        return false;

    for (uint i = 0; i < 3; ++i)
    {
        if (!jsonValue[i].isNumeric())
            return false;
    }

    vec3[0] = jsonValue[0].asFloat();
    vec3[1] = jsonValue[1].asFloat();
    vec3[2] = jsonValue[2].asFloat();

    return true;
}

#endif
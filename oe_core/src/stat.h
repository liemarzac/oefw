#ifndef __OE_CORE_STAT_H__
#define __OE_CORE_STAT_H__

#include "core_private.h"

#if OE_PLATFORM != OE_PLATFORM_WINDOWS
    #include <time.h>
#endif
namespace OE_Core
{

template<typename T>
class Stat
{
public:

    Stat():
        m_data(nullptr)
        ,m_n(0)
        ,m_lastGotDelta(0)
        ,m_lastGotVal(0)
        ,m_nMax(0)
        ,m_iNext(0)
        ,m_total(0)
        ,m_bInit(false)
        ,m_bSmooth(false)
    {
    }

    Stat(size_t n):
        m_n(0)
        ,m_nMax(0)
        ,m_iNext(0)
        ,m_total(0)
        ,m_bInit(false)
    {
        init(n);
    }

    ~Stat()
    {
        if(m_data)
            delete[] m_data;
    }

    void init(size_t n, bool bSmooth)
    {
        if(m_data)
        {
            delete[] m_data;
            m_data = nullptr;
        }

        if(n > 0)
        {
            m_data = new T[n];
        }
        
        m_nMax = n;
        m_n = 0;
        m_iNext = 0;
        m_total = 0;
        m_lastGotDelta = m_nMax;

        if(m_nMax > 0)
        {
            m_bInit = true;
            m_bSmooth = bSmooth;
        }
    }

    void add(const T& val)
    {
        if(!m_bInit)
            return;

        if(m_n == m_nMax)
        {
            m_total -= m_data[m_iNext];
            m_data[m_iNext] = val;
        }
        else
        {
            m_data[m_iNext] = val;
            ++m_n;
        }

        m_total += val;
        ++m_lastGotDelta;

        m_iNext = m_iNext + 1 == m_nMax ? 0 : m_iNext + 1;
    }

    T getAvg() const
    {
        bool bRecalculate = true;

        if(m_bSmooth)
        {
            if(m_lastGotDelta < m_nMax)
            {
                bRecalculate = false;
            }
        }

        if(bRecalculate)
        {
            m_lastGotVal = m_n > 0 ? m_total / m_n : 0;
            m_lastGotDelta = 0;
        }

        return m_lastGotVal;
    }

private:
    T* m_data;
    size_t m_n;
    mutable size_t m_lastGotDelta;
    mutable T m_lastGotVal;
    size_t m_nMax;
    size_t m_iNext;
    T m_total;
    bool m_bInit;
    bool m_bSmooth;
};



} // namespace OE_Core
#endif // __OE_CORE_STAT_H__

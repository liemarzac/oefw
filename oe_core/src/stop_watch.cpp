#include "stop_watch.h"

namespace OE_Core
{

StopWatch::StopWatch()
{
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    m_start.QuadPart = 0;
    m_stop.QuadPart = 0;
    QueryPerformanceFrequency( &m_frequency ) ;
#endif
    m_bStarted = false;
}

void StopWatch::start()
{
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    QueryPerformanceCounter(&m_start) ;
#else
    clock_gettime(CLOCK_MONOTONIC, &m_start);
#endif
    m_bStarted = true;
}

void StopWatch::stop()
{
    m_bStarted = false;
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    QueryPerformanceCounter(&m_stop);
#else
    clock_gettime(CLOCK_MONOTONIC, &m_stop);
#endif
}

double StopWatch::getElapsedTime() const
{
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    LARGE_INTEGER stop;
    if(m_bStarted)
    {
        QueryPerformanceCounter(&stop);
    }
    else
    {
        stop = m_stop;
    }

    LARGE_INTEGER deltaTime;
    deltaTime.QuadPart = stop.QuadPart - m_start.QuadPart;
    return LIToSecs(deltaTime);
#else
    timespec stop;
    if(m_bStarted)
    {
        clock_gettime(CLOCK_MONOTONIC, &stop);
    }
    else
    {
        stop = m_stop;
    }

    double elapsedTime;
    elapsedTime = (stop.tv_sec - m_start.tv_sec);
    elapsedTime += (stop.tv_nsec - m_start.tv_nsec) * 1.0e-9;
    return elapsedTime;
#endif
}

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
double StopWatch::LIToSecs( LARGE_INTEGER & L) const
{
    return ((double)L.QuadPart /(double)m_frequency.QuadPart) ;
}
#endif

} // namespace OE_Core

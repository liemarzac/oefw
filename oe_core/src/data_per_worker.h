#ifndef __OE_CORE_DATA_PER_WORKER_H__
#define __OE_CORE_DATA_PER_WORKER_H__

#include "core_private.h"

namespace OE_Core
{

template<typename T, typename A = std::allocator<T>, uint MaxWorkers = MAX_NUM_WORKERS>
class DataPerWorker
{
public:
    class Itr
    {
    friend class DataPerWorker;
    public:
        Itr()
        {
            m_iWorker = 0;
            m_iData = 0;
        }

        bool operator!=(const Itr& other) const;
        const T& operator*() const;
        Itr& operator++();

    private:
        const DataPerWorker* m_owner;
        uint m_iWorker;
        uint m_iData;
    };

    void addData(const T& Data, uint8 iWorker);
    void clear();
    Itr begin();
    Itr end();

private:
    Vector<T, A> m_workersData[MaxWorkers];
};

template<typename T, typename A, uint MaxWorkers>
void DataPerWorker<T, A, MaxWorkers>::addData(const T& Data, uint8 iWorker)
{
    OE_CHECK(iWorker < MaxWorkers, OE_CHANNEL_CORE);
    m_workersData[iWorker].push_back(Data);
}

template<typename T, typename A, uint MaxWorkers>
void DataPerWorker<T, A, MaxWorkers>::clear()
{
    for(uint iWorker = 0; iWorker < MaxWorkers; ++iWorker)
    {
        m_workersData[iWorker].clear();
    }
}

template<typename T, typename A, uint MaxWorkers>
typename DataPerWorker<T, A, MaxWorkers>::Itr DataPerWorker<T, A, MaxWorkers>::begin()
{
    Itr itr;
    itr.m_owner = this;
    itr.m_iWorker = 0;
    itr.m_iData = 0;

    if(m_workersData[0].size() == 0)
        ++itr;

    return itr;
}

template<typename T, typename A, uint MaxWorkers>
typename DataPerWorker<T, A, MaxWorkers>::Itr DataPerWorker<T, A, MaxWorkers>::end()
{
    Itr itr;
    itr.m_owner = this;
    itr.m_iWorker = MaxWorkers - 1;
    itr.m_iData = (uint)m_workersData[MaxWorkers - 1].size();

    return itr;
}

template<typename T, typename A, uint MaxWorkers>
typename DataPerWorker<T, A, MaxWorkers>::Itr& DataPerWorker<T, A, MaxWorkers>::Itr::operator++()
{
    ++m_iData;
    while(m_iData >= m_owner->m_workersData[m_iWorker].size())
    {
        if(m_iWorker + 1 >= MaxWorkers)
            break;

        m_iData = 0;
        ++m_iWorker;
    }

    return *this;
}

template<typename T, typename A, uint MaxWorkers>
const T& DataPerWorker<T, A, MaxWorkers>::Itr::operator*() const
{
    return m_owner->m_workersData[m_iWorker][m_iData];
}

template<typename T, typename A, uint MaxWorkers>
bool DataPerWorker<T, A, MaxWorkers>::Itr::operator!=(const Itr& rhs) const
{
    return m_iData != rhs.m_iData
        || m_iWorker != rhs.m_iWorker
        || m_owner != rhs.m_owner;
}

}

#endif // __OE_CORE_DATA_PER_WORKER_H__

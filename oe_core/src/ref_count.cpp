#include "ref_count.h"

#define OE_CHANNEL_REF_COUNT "RefCount"

namespace OE_Core
{

RefCount::RefCount()
{
    m_refCount.store(0);
}

void RefCount::deleter()
{
    delete this;
}

uint32 RefCount::addRef() const
{
    return m_refCount.fetch_add(1) + 1;
}

uint32 RefCount::removeRef() const
{
    return m_refCount.fetch_add(-1) - 1;
}

RefCountHandle::RefCountHandle()
{
}

RefCountHandle::RefCountHandle(RefCount* refCountObj)
{
    if(refCountObj)
    {
        OE_CHECK_RESULT(refCountObj->addRef(), 1, OE_CHANNEL_REF_COUNT);
    }
    m_obj = refCountObj;
}

RefCountHandle::RefCountHandle(const RefCountHandle& rhs)
{
    copy(rhs);
}

RefCountHandle& RefCountHandle::operator=(const RefCountHandle& rhs)
{
    copy(rhs);
    return *this;
}

bool RefCountHandle::operator==(const RefCountHandle& rhs) const
{
    return m_obj && m_obj == rhs.m_obj;
}

void RefCountHandle::copy(const RefCountHandle& rhs)
{
    if(&rhs == this)
        return;

    if(m_obj)
    {
        uint32 refCount = m_obj->removeRef();
        if(refCount == 0)
        {
            m_obj->deleter();
        }
    }

    m_obj = rhs.m_obj;
    if(m_obj)
    {
        m_obj->addRef();
    }
}

void RefCountHandle::reset()
{
    if(m_obj)
    {
        uint32 refCount = m_obj->removeRef();
        if(refCount == 0)
        {
            m_obj->deleter();
        }

        m_obj = nullptr;
    }
}

RefCountHandle::~RefCountHandle()
{
    reset();
}

RefCountHandle::operator bool() const
{
    return m_obj;
}

} // namespace OE_Core

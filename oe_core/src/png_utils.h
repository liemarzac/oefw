#ifndef __OE_CORE_PNG_UTILS_H__
#define __OE_CORE_PNG_UTILS_H__

#include "oe_core.h"

// OE_Core
#include "oe_string.h"


namespace OE_Core
{

struct Buffer;

bool OE_CORE_EXPORT loadTexturePNG(
	const String& fileName,
	Buffer& data,
	uint32& width,
	uint32& height,
    bool& bAlpha);

} // namespace OE_Core

#endif // __OE_CORE_PNG_UTILS_H__

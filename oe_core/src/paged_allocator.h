#ifndef __OE_CORE_PAGED_ALLOCATOR_H__
#define __OE_CORE_PAGED_ALLOCATOR_H__

#include "oe_core.h"

// OE_Core
#include "critical_section.h"
#include "memory.h"

namespace OE_Core
{

typedef uint64 TocBitfield;
static const uint64 BitfieldFull = uint64(-1);

template<typename Object, size_t PageSize = 4 * OneKb, size_t Alignment = 8>
class PagedAllocator
{
    static const uint32 NumBitsPerBitfield = sizeof(TocBitfield) * 8;
    static const uint32 NumObjectsPerBitfield = NumBitsPerBitfield;
    static const uint32 AlignedObjectSize = uint32(align(sizeof(Object), Alignment));
    static const uint32 NumElementsPerPage = PageSize / AlignedObjectSize;
    static const uint32 NumBitfieldsPerPage = (NumElementsPerPage + (NumObjectsPerBitfield - 1)) / NumObjectsPerBitfield;

    class Node
    {
        friend class Itr;

        class Page
        {
        public:
            struct TocPosition
            {
                uint m_iBitfield = 0;
                uint m_iBit = 0;
            };

            Page();
            ~Page();

            bool isEmpty() const;
            bool isAtCapacity() const;
            Object* acquire();
            void release(Object* object);
            Object* at(uint iObject);
            void clear();

            inline bool isUsed(uint iObject) const
            {
                uint iBitfield = iObject / NumBitsPerBitfield;
                uint iBit = iObject % NumBitsPerBitfield;

                TocBitfield mask = (TocBitfield)1 << iBit;

                const TocBitfield* toc = getToc();
                return (*(toc + iBitfield) & mask) != 0;
            }

            inline bool isFree(uint iObject) const
            {
                return !isUsed(iObject);
            }

            inline Object* getFirstObject()
            {
                return reinterpret_cast<Object*>(m_userData);
            }

            inline Object* getLastObject()
            {
                return reinterpret_cast<Object*>(m_userData + ((NumElementsPerPage - 1) * AlignedObjectSize));
            }

            inline bool doesContain(Object* object) const
            {
                Object* firstObject = const_cast<Page*>(this)->getFirstObject();
                Object* lastObject = const_cast<Page*>(this)->getLastObject();
                return firstObject <= object && object <= lastObject;
            }

            inline TocBitfield* getToc()
            {
                return m_toc;
            }

            inline const TocBitfield* getToc() const
            {
                return m_toc;
            }

        private:
            uint32 m_nUsedObjects = 0;
            TocBitfield m_toc[PagedAllocator::NumBitfieldsPerPage];
            alignas(Alignment) uint8 m_userData[PageSize];
        };

    public:
        class Itr
        {
        public:
            Itr() {}
            Itr(const Node& firstNode):
                m_node(&firstNode)
                ,m_firstNode(&firstNode)
            {
            }

            bool operator!=(const Itr& other) const
            {
                return memcmp(this, &other, sizeof(Itr)) != 0;
            }

            Object* operator*() const;
            const Itr& operator++();
            void begin();
            void beginPage(uint32 iPage);
            void end();

        private:
            const Node* m_node = nullptr;
            const Node* m_firstNode = nullptr;
            uint32 m_iNode = 0;
            uint32 m_iPage = 0;
            uint32 m_iObjectsBitfield = 0;
            uint32 m_iObjectsBit = 0;
        };

        Node();
        ~Node();

        Object* acquire();
        void release(Object* object);
        void clear();
        bool isAtCapacity() const;

        inline const Node* getNext() const
        {
            return m_next;
        }

        inline uint32 getNumElements() const
        {
            return m_nElements;
        }

        inline uint32 getNumPages() const
        {
            return m_nPages;
        }

    private:
        struct PageRange
        {
            Object* m_first = nullptr;
            Object* m_last = nullptr;
        };

        Page* m_pages[NumBitsPerBitfield];
        PageRange m_ranges[NumBitsPerBitfield];
        TocBitfield m_toc = 0;
        uint32 m_nElements = 0;
        uint32 m_nPages = 0;
        Node* m_next = nullptr;
    };

public:

    class Itr
    {
    public:
        Itr(const PagedAllocator& pagedAllocator):
            m_pagedAllocator(&pagedAllocator)
        {
            m_pimpl = typename Node::Itr(pagedAllocator.m_firstNode);
        }

        void begin()
        {
            m_pimpl.begin();
        }

        void beginPage(uint32 iPage)
        {
            m_pimpl.beginPage(iPage);
        }

        void end()
        {
            m_pimpl.end();
        }

        bool operator!=(const Itr& other) const
        {
            return m_pimpl != other.m_pimpl;
        }

        bool operator==(const Itr& other) const
        {
            return !operator!=(other);
        }

        Object* operator*() const
        {
            return *m_pimpl;
        }

        const Itr& operator++()
        {
            ++m_pimpl;
            return *this;
        }

    private:
        const PagedAllocator* m_pagedAllocator = nullptr;
        typename Node::Itr m_pimpl;
    };

    inline Object* acquire()
    {
        WriteScopedCS wscs(m_cs);
        return m_firstNode.acquire();
    }

    inline void release(Object* object)
    {
        WriteScopedCS wscs(m_cs);
        m_firstNode.release(object);
    }

    inline Itr begin() const
    {
        ReadScopedCS rscs(m_cs);
        Itr itr(*this);
        itr.begin();
        return itr;
    }

    inline Itr end() const
    {
        ReadScopedCS rscs(m_cs);
        Itr itr(*this);
        itr.end();
        return itr;
    }

    inline Itr beginPage(uint iPage) const
    {
        ReadScopedCS rscs(m_cs);
        Itr itr(*this);
        itr.beginPage(iPage);
        return itr;
    }

    inline void clear()
    {
        WriteScopedCS wscs(m_cs);
        m_firstNode.clear();
    }

    uint32 getNumElements() const;
    uint32 getNumPages() const;
    uint32 getNumNodes() const;
    bool isAtCapacity() const;
    constexpr static size_t getSingleNodeCapacity()
    {
        return NumElementsPerPage * NumBitsPerBitfield;
    }

    constexpr static size_t getPageCapacity()
    {
        return NumElementsPerPage;
    }

    size_t getCapacity() const;

private:
    Node m_firstNode;
    mutable CriticalSection m_cs;
};

template<typename Object, size_t PageSize, size_t Alignment>
PagedAllocator<Object, PageSize, Alignment>::Node::Page::Page()
{
    memset(&m_toc, 0, sizeof(m_toc));
}

template<typename Object, size_t PageSize, size_t Alignment>
PagedAllocator<Object, PageSize, Alignment>::Node::Page::~Page()
{
    // It is the responsibility of the user to make sure all
    // objects have been released before the page gets deleted.
    OE_CHECK(isEmpty(), OE_CHANNEL_CORE);
}

template<typename Object, size_t PageSize, size_t Alignment>
bool PagedAllocator<Object, PageSize, Alignment>::Node::Page::isAtCapacity() const
{
    return m_nUsedObjects == NumElementsPerPage;
}

template<typename Object, size_t PageSize, size_t Alignment>
bool PagedAllocator<Object, PageSize, Alignment>::Node::Page::isEmpty() const
{
    return m_nUsedObjects == 0;
}

template<typename Object, size_t PageSize, size_t Alignment>
Object* PagedAllocator<Object, PageSize, Alignment>::Node::Page::acquire()
{
    if(isAtCapacity())
        return nullptr;

    TocBitfield* bitfield = nullptr;
    uint8* byte = nullptr;

    TocBitfield mask = 0;
    uint iBitfield = 0;

    uint count = 0;
    while(count < NumElementsPerPage)
    {
        bitfield = m_toc + iBitfield;
        if((*bitfield) != BitfieldFull)
        {
            byte = reinterpret_cast<uint8*>(bitfield);
            for(uint iByte = 0; iByte < sizeof(TocBitfield); ++iByte, ++byte)
            {
                if(*byte != 0xff)
                {
                    for(uint iBit = 0; iBit < 8; ++iBit)
                    {
                        uint8 mask = (uint8)1 << iBit;
                        if((*byte & mask) == 0)
                        {
                            // Record as used.
                            *byte |= mask;

                            ++m_nUsedObjects;

                            uint32 iObject = (iBitfield * NumBitsPerBitfield) + (iByte * 8) + iBit;
                            Object* object = new(at(iObject))Object();
                            return object;
                        }
                    }
                }
            }
        }

        ++iBitfield;
        if(iBitfield >= NumBitfieldsPerPage)
            iBitfield = 0;

        ++count;
    }

    return nullptr;
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::Page::release(Object* object)
{
    OE_CHECK(doesContain(object), OE_CHANNEL_CORE);
    object->~Object();

    // Update TOC.
    uint iObject = (uint)(object - getFirstObject());
    uint iBitfield = iObject / NumBitsPerBitfield;
    TocBitfield* bitfield = getToc() + iBitfield;
    uint iBit = iObject % NumBitsPerBitfield;
    TocBitfield mask = ~((TocBitfield)1 << iBit);
    *bitfield &= mask;
    --m_nUsedObjects;
}

template<typename Object, size_t PageSize, size_t Alignment>
Object* PagedAllocator<Object, PageSize, Alignment>::Node::Page::at(uint iObject)
{
    OE_CHECK(iObject < NumElementsPerPage, OE_CHANNEL_CORE);
    return getFirstObject() + iObject;
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::Page::clear()
{
    // Delete all used objects.
    for(uint iObject = 0; iObject < NumElementsPerPage; ++iObject)
    {
        if(isUsed(iObject))
        {
            Object* object = at(iObject);
            object->~Object();
        }
    }

    memset(m_toc, 0, sizeof(m_toc));
    m_nUsedObjects = 0;
}

template<typename Object, size_t PageSize, size_t Alignment>
Object* PagedAllocator<Object, PageSize, Alignment>::Node::Itr::operator*() const
{
    OE_CHECK(m_node, OE_CHANNEL_CORE);

    uint32 iObject = (m_iObjectsBitfield * NumBitsPerBitfield)
        + m_iObjectsBit;

    OE_CHECK(m_iPage < m_node->getNumPages(), OE_CHANNEL_CORE);

    return m_node->m_pages[m_iPage]->at(iObject);
}

template<typename Object, size_t PageSize, size_t Alignment>
const typename PagedAllocator<Object, PageSize, Alignment>::Node::Itr&
PagedAllocator<Object, PageSize, Alignment>::Node::Itr::operator++()
{
    OE_CHECK(m_node, OE_CHANNEL_CORE);

    // Check if we have already reached end iterator.
    if(m_iPage == m_node->getNumPages())
        return *this;

    while(true)
    {
        // Advance object pointer.
        ++m_iObjectsBit;
        if(m_iObjectsBit >= NumBitsPerBitfield)
        {
            m_iObjectsBit = 0;
            ++m_iObjectsBitfield;
        }

        if((m_iObjectsBitfield * NumBitsPerBitfield) + m_iObjectsBit >= NumElementsPerPage)
        {
            // Advance page pointer.
            m_iObjectsBit = 0;
            m_iObjectsBitfield = 0;

            ++m_iPage;
            if(m_iPage >= m_node->getNumPages())
            {
                if(m_node->m_next)
                {
                    ++m_iNode;
                    m_iPage = 0;
                    m_node = m_node->m_next;
                }
            }
        }

        // Early exit when reaching end iterator.
        if(m_iPage >= m_node->getNumPages())
            break;

        // Break if object is used.
        const uint iObject = m_iObjectsBitfield + m_iObjectsBit;
        const TocBitfield mask = (TocBitfield)1 << m_iObjectsBit;
        const TocBitfield* toc = m_node->m_pages[m_iPage]->getToc();
        if((*(toc + m_iObjectsBitfield) & mask) != 0)
            break;
    }

    return *this;
}

template<typename Object, size_t PageSize, size_t Alignment>
PagedAllocator<Object, PageSize, Alignment>::Node::Node()
{
    memset(&m_pages, 0, sizeof(m_pages));
}

template<typename Object, size_t PageSize, size_t Alignment>
PagedAllocator<Object, PageSize, Alignment>::Node::~Node()
{
    if(m_next)
    {
        delete m_next;
    }

    for(uint i = 0; i < m_nPages; ++i)
    {
        delete m_pages[i];
    }
}

template<typename Object, size_t PageSize, size_t Alignment>
Object* PagedAllocator<Object, PageSize, Alignment>::Node::acquire()
{
    if(!isAtCapacity())
    {
        uint8* byte = reinterpret_cast<uint8*>(&m_toc);
        for(uint iByte = 0; iByte < sizeof(TocBitfield); ++iByte, ++byte)
        {
            if((*byte) != 0xff)
            {
                for(uint iBit = 0; iBit < 8; ++iBit)
                {
                    uint8 mask = (uint8)1 << iBit;
                    if(((*byte) & mask) == 0)
                    {
                        uint iPage = (iByte * 8) + iBit;
                        if(iPage >= m_nPages)
                        {
                            OE_CHECK(iPage < NumBitsPerBitfield, OE_CHANNEL_CORE);

                            // Allocate new page.
                            Page* newPage = new Page();
                            m_pages[m_nPages] = newPage;

                            // Cache ranges.
                            m_ranges[m_nPages].m_first = newPage->getFirstObject();
                            m_ranges[m_nPages].m_last = newPage->getLastObject();

                            ++m_nPages;
                        }
                        
                        OE_CHECK(!m_pages[iPage]->isAtCapacity(), OE_CHANNEL_CORE);

                        Object* object = m_pages[iPage]->acquire();
                        ++m_nElements;

                        OE_CHECK(object != nullptr, OE_CHANNEL_CORE);

                        // Update TOC when page is full.
                        if(m_pages[iPage]->isAtCapacity())
                        {
                            *byte |= mask;
                        }

                        return object;
                    }
                }
            }
        }
    }
    else
    {
        OE_WARNING_PERF(OE_CHANNEL_CORE, "Paged allocator node is full. Increase the page size");
        // We need a new allocator
        if(!m_next)
        {
            m_next = new Node();
        }

        return m_next->acquire();
    }

    return nullptr;
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::release(Object* object)
{
    uint iPage = 0;
    for(;iPage < m_nPages; ++iPage)
    {
        if(m_ranges[iPage].m_first <= object && object <= m_ranges[iPage].m_last)
            break;
    }

    if(iPage < m_nPages)
    {
        m_pages[iPage]->release(object);
        --m_nElements;

        // Update TOC.
        uint iBit = iPage;
        TocBitfield mask = ~((TocBitfield)1 << iBit);
        m_toc &= mask;

        if(m_pages[iPage]->isEmpty())
        {
            --m_nPages;
            delete m_pages[iPage];

            // Keep a packed array. The last page will replace the deleted one.
            if(iPage < m_nPages)
            {
                m_pages[iPage] = m_pages[m_nPages];
                m_ranges[iPage] = m_ranges[m_nPages];

                // Re-update TOC
                mask = ((TocBitfield)1 << iBit);
                if(m_pages[iPage]->isAtCapacity())
                {
                    m_toc |= mask;
                }
                else
                {
                    m_toc &= ~mask;
                }
            }
        }
    }
    else
    {
        m_next->release(object);

        if(m_next->getNumElements() == 0)
        {
            // Delete the empty node.
            Node* emptyNode = m_next;
            m_next = emptyNode->m_next;
            delete emptyNode;
        }
    }
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::Itr::begin()
{
    OE_CHECK(m_node, OE_CHANNEL_CORE);

    if(m_node->getNumPages() > 0)
    {
        // Find first used object.
        TocBitfield mask = 1;
        const TocBitfield* const toc = m_node->m_pages[0]->getToc();
        if((*toc & mask) == 0)
        {
            ++(*this);
        }
    }
    else
    {
        end();
    }
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::Itr::end()
{
    OE_CHECK(m_node, OE_CHANNEL_CORE);

    while(const Node* nextNode = m_node->getNext())
    {
        m_node = nextNode;
    }

    m_iPage = m_node->getNumPages();
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::Itr::beginPage(uint iPage)
{
    OE_CHECK(m_node, OE_CHANNEL_CORE);

    bool isValid = true;

    m_iPage = iPage;

    while(m_iPage >= NumBitsPerBitfield)
    {
        if(const Node* nextNode = m_node->getNext())
        {
            m_node = nextNode;
            m_iPage -= NumBitsPerBitfield;
        }
        else
        {
            isValid = false;
            break;
        }
    }

    isValid &= m_iPage < m_node->getNumPages();

    if(isValid)
    {
        // Find first used object in the page.
        TocBitfield mask = 1;
        const TocBitfield* toc = m_node->m_pages[m_iPage]->getToc();
        if((*toc & mask) == 0)
        {
            ++(*this);
        }
    }
    else
    {
        // Invalid page, set to end iterator.
        end();
    }
}

template<typename Object, size_t PageSize, size_t Alignment>
void PagedAllocator<Object, PageSize, Alignment>::Node::clear()
{
    if(m_next)
    {
        m_next->clear();
    }

    for(int32 i = m_nPages - 1; i >= 0; --i)
    {
        m_pages[i]->clear();
        delete m_pages[i];
    }

    m_nElements = 0;
    m_nPages = 0;
}

template<typename Object, size_t PageSize, size_t Alignment>
bool PagedAllocator<Object, PageSize, Alignment>::Node::isAtCapacity() const
{
    return m_toc == BitfieldFull;
}

template<typename Object, size_t PageSize, size_t Alignment>
uint32 PagedAllocator<Object, PageSize, Alignment>::getNumPages() const
{
    uint32 nPages = 0;
    const Node* node = &m_firstNode;
    while(true)
    {
        nPages += node->getNumPages();

        if(node->m_next)
        {
            node = node->m_next;
        }
        else
        {
            break;
        }
    }

    return nPages;
}

template<typename Object, size_t PageSize, size_t Alignment>
bool PagedAllocator<Object, PageSize, Alignment>::isAtCapacity() const
{
    const Node* node = &m_firstNode;

    bool ret = true;
    while(true)
    {
        if(!node->isAtCapacity())
        {
            ret = false;
            break;
        }

        if(const Node* nextNode = node->getNext())
        {
            node = nextNode;
        }
        else
        {
            break;
        }
    }

    return ret;
}

template<typename Object, size_t PageSize, size_t Alignment>
uint32 PagedAllocator<Object, PageSize, Alignment>::getNumNodes() const
{
    ReadScopedCS rscs(m_cs);

    uint32 nNodes = 1;
    const Node* node = &m_firstNode;

    while(const Node* nextNode = node->getNext())
    {
        ++nNodes;
        node = nextNode;
    }

    return nNodes;
}

template<typename Object, size_t PageSize, size_t Alignment>
uint32 PagedAllocator<Object, PageSize, Alignment>::getNumElements() const
{
    ReadScopedCS rscs(m_cs);

    uint32 nElements = 0;
    const Node* node = &m_firstNode;

    do
    {
        nElements += node->getNumElements();
        node = node->getNext();
    } while(node);

    return nElements;
}

template<typename Object, size_t PageSize, size_t Alignment>
size_t PagedAllocator<Object, PageSize, Alignment>::getCapacity() const
{
    return getNumNodes() * getSingleNodeCapacity();
}


} // namespace OE_Core

#endif // __OE_CORE_LINKED_ARRAY_H__

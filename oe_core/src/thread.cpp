#include "thread.h"

namespace OE_Core
{

#if OE_PLATFORM == OE_PLATFORM_WINDOWS

void setThreadName(uint32_t dwThreadID, const char* threadName)
{
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = threadName;
    info.dwThreadID = dwThreadID;
    info.dwFlags = 0;

    __try
    {
        RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
    }
}

void setThreadName(const char* threadName)
{
    setThreadName(GetCurrentThreadId(), threadName);
}

void setThreadName(std::thread* thread, const char* threadName)
{
    DWORD threadId = ::GetThreadId(static_cast<HANDLE>(thread->native_handle()));
    setThreadName(threadId, threadName);
}

#else

void OE_CORE_EXPORT setThreadName(std::thread* thread, const char* threadName)
{
    auto handle = thread->native_handle();
    pthread_setname_np(handle, threadName);
}


#include <sys/prctl.h>
void OE_CORE_EXPORT setThreadName(const char* threadName)
{
    prctl(PR_SET_NAME, threadName, 0, 0, 0);
}

#endif

}

#ifndef __OE_CORE_FLAGSET_H__
#define __OE_CORE_FLAGSET_H__

#include "core_private.h"

namespace OE_Core
{

template<typename T, typename ContainerType = uint8>
class FlagSet
{
public:
    FlagSet() :
        m_flags(0)
    {
    }

    void set(T flag)
    {
        m_flags |= (1 << (uint)flag);
    }

    void remove(T flag)
    {
        m_flags &= ~(1 << (uint)flag);
    }

    bool isSet(T flag) const
    {
        return (m_flags & (1 << (uint)flag)) != 0;
    }

    void clear()
    {
        m_flags = 0;
    }

    bool areAllSet(ContainerType nFlags) const
    {
        return  (ContainerType)m_flags == (1 << nFlags) - 1;
    }

    bool areAllSet(T nFlags) const
    {
        return  (ContainerType)m_flags == (1 << (ContainerType)nFlags) - 1;
    }

private:
    ContainerType m_flags;
};

}

#endif // __OE_CORE_FLAGSET_H__

#ifndef __OE_CORE_STD_MUTEX_HELPERS_H__
#define __OE_CORE_STD_MUTEX_HELPERS_H__

#include <mutex>

using LockGuard = std::lock_guard<std::mutex>;
using UniqueLock = std::unique_lock<std::mutex>;

typedef std::mutex Mutex;

#endif // __OE_CORE_STD_MUTEX_HELPERS_H__

#ifndef __OE_CORE_RTTI_H__
#define __OE_CORE_RTTI_H__

#include "oe_core.h"

// OE_Core
#include "hash.h"

#define RTTI_DECLARATION                                        public:\
                                                                    virtual bool isA(uint32 typeHash, bool bstrict = false);\
                                                                    static uint32 getTypeHash();\
                                                                private:


#define RTTI_GET_TYPE_HASH(ClassName)                           uint32 ClassName::getTypeHash()\
                                                                {\
                                                                    static uint32 typeHash = OE_Core::hash(#ClassName);\
                                                                    return typeHash;\
                                                                }


#define RTTI_IMPL(ClassName)                                    bool ClassName::isA(uint32 typeHash, bool bStrict)\
                                                                {\
                                                                    if(typeHash == getTypeHash())\
                                                                    {\
                                                                        return true;\
                                                                    }\
                                                                    return false;\
                                                                }\
                                                                RTTI_GET_TYPE_HASH(ClassName)


#define RTTI_IMPL_PARENT(ClassName, ParentClassName)            bool ClassName::isA(uint32 typeHash, bool bStrict)\
                                                                {\
                                                                    if(typeHash == getTypeHash())\
                                                                    {\
                                                                        return true; \
                                                                    }\
                                                                    if(!bStrict)\
                                                                    {\
                                                                        return ParentClassName::isA(typeHash, bStrict);\
                                                                    }\
                                                                    return false;\
                                                                }\
                                                                RTTI_GET_TYPE_HASH(ClassName)


#define GET_SIZE_IMPL                                           virtual size_t getSize() const\
                                                                {\
                                                                    return sizeof(*this);\
                                                                }\

template<class T, class U>
T* cast(U* rhs)
{
    if(rhs->isA(T::getTypeHash(), false))
    {
        return static_cast<T*>(rhs);
    }
    return nullptr;
}


#endif // __OE_CORE_RTTI_H__

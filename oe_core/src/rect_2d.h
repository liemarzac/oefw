#ifndef __OE_CORE_RECT_2D_H__
#define __OE_CORE_RECT_2D_H__

#include "oe_core.h"
#include "buffer.h"
#include "serializable.h"

namespace OE_Core
{

struct OE_CORE_EXPORT Rect2D
{
    Rect2D();

    bool doesOverlap(const Rect2D& other) const;
    bool doesContain(const Rect2D& other) const;

    int m_x, m_y, m_width, m_height;
};

struct OE_CORE_EXPORT Rect2Df
{
    float m_x = .0f;
    float m_y = .0f;
    float m_width = .0f;
    float m_height = .0f;
};

template<>
void OE_CORE_EXPORT serialize(Buffer& buffer, const Rect2D& rect);

template<>
void OE_CORE_EXPORT deserialize(Buffer& buffer, Rect2D& rect);

template<>
size_t OE_CORE_EXPORT calculateSerializedSize(const Rect2D& rect);

} // namespace OE_Core

#endif // __OE_CORE_RECT_2D_H__

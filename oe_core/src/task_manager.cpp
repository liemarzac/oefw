#include "runnable.h"

#include "core_private.h"

// OE_Core
#include "task_manager.h"
#include "thread.h"

//External
#include <thread>


namespace OE_Core
{

void work(const TaskManager::WorkerParams& params);

TaskManager::TaskManager()
{
    m_nextTaskId = 0;
    m_bTerminates = false;

    WorkerParams params;
    params.m_taskManager = this;

    // Change by using a number dependent of the number of cores.
    m_workers.resize(2);

    for(uint i = 0; i < uint(m_workers.size()); ++i)
    {
        params.m_id = i;
        m_workers[i] = std::thread(work, params);
    }
}

TaskManager::~TaskManager()
{
    std::unique_lock<std::mutex> lock(m_tasksMutex);
    m_bTerminates = true;
    lock.unlock();
    m_taskNotifier.notify_all();

    // Wait for workers to finish their task.
    for(size_t i = 0; i < m_workers.size(); ++i)
    {
        m_workers[i].join();
    }
}

uint32 TaskManager::addTask(std::shared_ptr<Runnable> task)
{
    std::unique_lock<std::mutex> lock(m_tasksMutex);
    uint32 taskId = m_nextTaskId++;
    task->setTaskId(taskId);
    task->setState(Runnable::State::Pending);
    m_pendingTasks.push_back(task);
    m_remainingTaskIds.push_back(taskId);
    lock.unlock();
    m_taskNotifier.notify_one();

    return taskId;
}

SharedPtr<Runnable> TaskManager::popNextTask()
{
    SharedPtr<Runnable> nextRunnable;
    std::unique_lock<std::mutex> lock(m_tasksMutex);

    while(m_pendingTasks.empty() && !m_bTerminates)
    {
        m_taskNotifier.wait(lock);
    }

    if(!m_pendingTasks.empty())
    {
        nextRunnable = m_pendingTasks.front();
        m_pendingTasks.pop_front();
    }

    return nextRunnable;
}

void TaskManager::onTaskFinished(SharedPtr<Runnable> task)
{
    std::unique_lock<std::mutex> lock(m_tasksMutex);
    task->setState(Runnable::State::Finished);
    OE_CHECK_RESULT(removeUniqueUnordered(m_remainingTaskIds, task->getTaskId()), 1, OE_CHANNEL_CORE);
}

bool TaskManager::exist(uint32 taskId) const
{
    std::unique_lock<std::mutex> lock(m_tasksMutex);
    return isContained(m_remainingTaskIds, taskId);
}

bool TaskManager::cancel(uint taskId)
{
    std::unique_lock<std::mutex> lock(m_tasksMutex);

    for(size_t i = 0; i < m_pendingTasks.size(); ++i)
    {
        if(m_pendingTasks[i]->getTaskId() == taskId)
        {
            m_pendingTasks[i]->setState(Runnable::State::Cancelled);
            m_pendingTasks.erase(m_pendingTasks.begin() + i);
            removeUniqueUnordered(m_remainingTaskIds, taskId);
            return true;
        }
    }

    return false;
}

void work(const TaskManager::WorkerParams& params)
{
    std::shared_ptr<Runnable> task;

    while(task = params.m_taskManager->popNextTask())
    {
        if(!task)
            break;

        task->setWorkerId(params.m_id);
        task->setState(Runnable::State::InProgress);
        task->run();
        task->onFinished();
        params.m_taskManager->onTaskFinished(task);

        // Do not keep unnecessary reference to the task
        // because we don't know how long we'll be blocked by popNextTask.
        task.reset();
    }
}

} // namespace OE_Core

#include "png_utils.h"

// External
#include <png.h>

// OE_Core
#include "buffer.h"
#include "file.h"

#define OE_CHANNEL_PNG "PNG"

namespace OE_Core
{

bool loadTexturePNG(
    const String& fileName,
    Buffer& data,
    uint32& width,
    uint32& height,
    bool& bAlpha)
{
    png_byte header[8];

    FILE *fp = nullptr;
    String resourcePath;
    appGetFullyQualifiedResourcePath(resourcePath, fileName);

    OE_FOPEN(fp, resourcePath.c_str(), "rb");
    if(fp == 0)
    {
        OE_WARNING(OE_CHANNEL_PNG, "Cannot open file %s", fileName.c_str());
        return false;
    }

    // read the header
    fread(header, 1, 8, fp);

    if(png_sig_cmp(header, 0, 8))
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "%s is not a PNG", fileName.c_str());
        fclose(fp);
        return false;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "png_create_read_struct returned 0");
        fclose(fp);
        return false;
    }

    // create png info struct
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "png_create_info_struct returned 0");
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(fp);
        return false;
    }

    // create png info struct
    png_infop end_info = png_create_info_struct(png_ptr);
    if(!end_info)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "png_create_info_struct returned 0");
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
        fclose(fp);
        return false;
    }

    // the code in this if statement gets called if libpng encounters an error
    if(setjmp(png_jmpbuf(png_ptr)))
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "Unknown libpng error");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return 0;
    }

    // init png reading
    png_init_io(png_ptr, fp);

    // let libpng know you already read the first 8 bytes
    png_set_sig_bytes(png_ptr, 8);

    // read all the info up to the image data
    png_read_info(png_ptr, info_ptr);

    // variables to pass to get info
    int bit_depth, color_type;
    png_uint_32 temp_width, temp_height;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &temp_width, &temp_height, &bit_depth, &color_type,
        NULL, NULL, NULL);

#if OE_RENDERER_FAMILY == OE_RENDERER_FAMILY_DIRECTX
    if(color_type == PNG_COLOR_TYPE_RGB ||
        color_type == PNG_COLOR_TYPE_RGB_ALPHA)
        png_set_bgr(png_ptr);
#endif

    if((color_type & PNG_COLOR_MASK_PALETTE) != 0)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "Palette mode are unsupported");
        return 0;
    }

    bAlpha = (color_type & PNG_COLOR_MASK_ALPHA) != 0;

    width = temp_width;
    height = temp_height;

    // Update the png info struct.
    png_read_update_info(png_ptr, info_ptr);

    // Row size in bytes.
    size_t rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    //NSLog(@"Row bytes: %d, Heights: %d, PNG Byte:%lu", rowbytes, temp_height, sizeof(png_byte));

    // Allocate the image_data as a big block.
    data.alloc(rowbytes * temp_height * sizeof(png_byte));

    if(data.m_rawData == NULL)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "Could not allocate memory for PNG image data");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return 0;
    }

    // row_pointers is for pointing to image_data for reading the png with libpng
    png_bytep* row_pointers = (png_bytep*)malloc(temp_height * sizeof(png_bytep));
    if(row_pointers == NULL)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_PNG, "Could not allocate memory for PNG row pointers");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return 0;
    }

    // set the individual row_pointers to point at the correct offsets of image_data
    for(png_uint_32 i = 0; i < temp_height; i++)
    {
        row_pointers[i] = (png_bytep)(data.m_rawData + i * rowbytes);
    }

    // read the png into image_data through row_pointers
    png_read_image(png_ptr, row_pointers);

    // clean up
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    free(row_pointers);
    fclose(fp);
    return true;
}

} // namespace OE_Core

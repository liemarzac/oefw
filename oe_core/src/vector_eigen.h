#ifndef __OE_CORE_VECTOR_EIGEN_H__
#define __OE_CORE_VECTOR_EIGEN_H__

#include "vector.h"

//External
#include <Eigen/Core>

// OE_Core
#include "eigen_helpers.h"


template<typename T>
using VectorEigen = Vector<T, EigenAlloc<T> >;

#endif // __OE_CORE_VECTOR_EIGEN_H__

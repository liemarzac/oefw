#include "file.h"

#include "application.h"
#include <fstream>
#include <functional>

namespace OE_Core
{

bool appDoesResourceExist(const String& resourceName)
{
    String appName;
    appGetName(appName);

    String resourcePath = g_exeFolderPath + "/" OE_DATA_FOLDER "/" + appName + "/" + resourceName;
    std::ifstream stream(resourcePath);

    if(!stream.is_open())
    {
        resourcePath = g_exeFolderPath + "/" OE_DATA_FOLDER "/engine/" + resourceName;
        stream.open(resourcePath);
    }

    const bool isStreamOpen = stream.is_open();
    if(isStreamOpen)
        stream.close();

    return isStreamOpen;
}

bool appGetResourceStream(
    const String& resourceName,
    std::ifstream& stream,
    std::ios_base::openmode mode)
{
    String appName;
    appGetName(appName);

    String resourcePath = g_exeFolderPath + PATH_SEPARATOR OE_DATA_FOLDER PATH_SEPARATOR + appName + PATH_SEPARATOR + resourceName;
    stream.open(resourcePath, mode);

    if(!stream.is_open())
    {
        resourcePath = g_exeFolderPath + PATH_SEPARATOR OE_DATA_FOLDER PATH_SEPARATOR "engine" PATH_SEPARATOR + resourceName;
        stream.open(resourcePath, mode);
    }

    return stream.is_open();
}

bool appGetFullyQualifiedResourcePath(String& fullyQualifiedPath, const String& partialPath)
{
    String appName;
    appGetName(appName);

    fullyQualifiedPath = g_exeFolderPath + "/" OE_DATA_FOLDER "/" + appName + "/" + partialPath;
    std::ifstream stream(fullyQualifiedPath);

    if(stream.good())
    {
        return true;
    }
    else
    {
        fullyQualifiedPath = g_exeFolderPath + "/" OE_DATA_FOLDER "/engine/" + partialPath;
        stream.open(fullyQualifiedPath);
        if(stream.good())
        {
            return true;
        }
    }

    return false;
}

void getFolderPath(String& folderPath, const String& filePath)
{
    folderPath = filePath;
    replace(folderPath, '\\', '/');
    left(folderPath, '/', StringPos::LastOf);
}

bool appLoadJsonFile(const char* fileName, Json::Value& outJson)
{
    std::ifstream stream;

    if (!appGetResourceStream(fileName, stream, std::ios::in))
        return false;

    Json::CharReaderBuilder builder;
    Json::String errors;
    
    const bool isValidJson = Json::parseFromStream(builder, stream, &outJson, &errors);
    if(!isValidJson)
    {
        logError(OE_CHANNEL_CORE, "Cannot read json file %s. Error:%s", fileName, errors.c_str());
    }

    if(stream.is_open())
    {
        stream.close();
    }

    return isValidJson;
}

UniqueBufferPtr appLoadBinaryFileToBuffer(const String& fileName)
{
    // Open file.
    std::ifstream stream;
    appGetResourceStream(fileName, stream, std::ios::binary);

    if (!stream.is_open())
    {
        OE_CHECK_MSG(false, OE_CHANNEL_CORE, "Cannot open file %s.", fileName.c_str());
        return UniqueBufferPtr();
    }

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Copy file to buffer.
    UniqueBufferPtr fileBuffer(new Buffer(fileSize));
    stream.read((char*)fileBuffer->m_rawData, fileSize);
    stream.close();

    return fileBuffer;
}

}

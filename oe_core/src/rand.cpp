#include "rand.h"

#include "core_private.h"

// External
#include <random>

std::random_device g_randomDevice;
std::mt19937 g_mersenneTwisterEngine;

namespace OE_Core
{

void seedRand(uint32 seed)
{
    g_mersenneTwisterEngine.seed(seed);
}

int32 randi32(int32 min, int32 max)
{
    std::uniform_int_distribution<int32> distr(min, max - 1);
    return distr(g_mersenneTwisterEngine);
}

uint32 randu32(uint32 min, uint32 max)
{
    std::uniform_int_distribution<uint32> distr(min, max == 0 ? 0 : max - 1);
    return distr(g_mersenneTwisterEngine);
}

int64 randi64(int64 min, int64 max)
{
    std::uniform_int_distribution<int64> distr(min, max - 1);
    return distr(g_mersenneTwisterEngine);
}

uint64 randu64(uint64 min, uint64 max)
{
    std::uniform_int_distribution<uint64> distr(min, max == 0 ? max : max - 1);
    return distr(g_mersenneTwisterEngine);
}

float randr32(float min, float max)
{
    std::uniform_real_distribution<float> distr(min, max);
    return distr(g_mersenneTwisterEngine);
}

double randr64(double min, double max)
{
    std::uniform_real_distribution<double> distr(min, max);
    return distr(g_mersenneTwisterEngine);
}

} // namespace OE_Core

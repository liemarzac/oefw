#ifndef __OE_CORE_EIGEN_HELPERS_H__
#define __OE_CORE_EIGEN_HELPERS_H__

#include "oe_core.h"

// External
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#define EIGEN_ALIGNED EIGEN_MAKE_ALIGNED_OPERATOR_NEW

typedef Eigen::Matrix2f Mat2;
typedef Eigen::Matrix3f Mat3;
typedef Eigen::Matrix4f Mat4;
typedef Eigen::Vector2f Vec2;
typedef Eigen::Vector3f Vec3;
typedef Eigen::Vector4f Vec4;
typedef Eigen::Quaternionf Quat;
typedef Eigen::AngleAxisf AngleAxis;

typedef Eigen::Matrix2d Mat2d;
typedef Eigen::Matrix3d Mat3d;
typedef Eigen::Matrix4d Mat4d;
typedef Eigen::Vector2d Vec2d;
typedef Eigen::Vector3d Vec3d;
typedef Eigen::Vector4d Vec4d;
typedef Eigen::Quaterniond Quatd;
typedef Eigen::AngleAxisd AngleAxisd;

typedef Eigen::Matrix2i Mat2i;
typedef Eigen::Matrix3i Mat3i;
typedef Eigen::Matrix4i Mat4i;
typedef Eigen::Vector2i Vec2i;
typedef Eigen::Vector3i Vec3i;
typedef Eigen::Vector4i Vec4i;

template<typename T>
using EigenAlloc = Eigen::aligned_allocator<T>;

inline void vec3ToFloat3(const Vec3& vector, float* floats)
{
    floats[0] = vector[0];
    floats[1] = vector[1];
    floats[2] = vector[2];
}

inline void float3ToVec3(const float* floats, Vec3& vector)
{
    vector[0] = floats[0];
    vector[1] = floats[1];
    vector[2] = floats[2];
}

inline void rotationXMatrix(Mat4& rotationMatrix, float radAngle)
{
    rotationMatrix.row(0) = Vec4(1.0f, 0.0f, 0.0f, 0.0f);
    rotationMatrix.row(1) = Vec4(0.0f, cos(radAngle), -sin(radAngle), 0.0f);
    rotationMatrix.row(2) = Vec4(0.0f, sin(radAngle), cos(radAngle), 0.0f);
    rotationMatrix.row(3) = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

inline void rotationYMatrix(Mat4& rotationMatrix, float radAngle)
{
    rotationMatrix.row(0) = Vec4(cos(radAngle), 0.0f, sin(radAngle), 0.0f);
    rotationMatrix.row(1) = Vec4(0.0f, 1.0f, 0.0f, 0.0f);
    rotationMatrix.row(2) = Vec4(-sin(radAngle), 0.0f, cos(radAngle), 0.0f);
    rotationMatrix.row(3) = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

inline void rotationZMatrix(Mat4& rotationMatrix, float radAngle)
{
    rotationMatrix.row(0) = Vec4(cos(radAngle), -sin(radAngle), 0.0f, 0.0f);
    rotationMatrix.row(1) = Vec4(sin(radAngle), cos(radAngle), 0.0f, 0.0f);
    rotationMatrix.row(2) = Vec4(0.0f, 0.0f, 1.0f, 0.0f);
    rotationMatrix.row(3) = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

inline void translationMatrix(Mat4& translationMatrix, float x, float y, float z)
{
    translationMatrix.setIdentity();
    translationMatrix(0, 3) = x;
    translationMatrix(1, 3) = y;
    translationMatrix(2, 3) = z;
}

inline void mat4ToMat3(Mat3& mat3, const Mat4& mat4)
{
    mat3(0, 0) = mat4(0, 0);
    mat3(0, 1) = mat4(0, 1);
    mat3(0, 2) = mat4(0, 2);
    mat3(1, 0) = mat4(1, 0);
    mat3(1, 1) = mat4(1, 1);
    mat3(1, 2) = mat4(1, 2);
    mat3(2, 0) = mat4(2, 0);
    mat3(2, 1) = mat4(2, 1);
    mat3(2, 2) = mat4(2, 2);
}

inline void mat3ToMat4(Mat4& mat4, const Mat3& mat3)
{
    mat4(0, 0) = mat3(0, 0);
    mat4(0, 1) = mat3(0, 1);
    mat4(0, 2) = mat3(0, 2);
    mat4(0, 3) = .0f;
    mat4(1, 0) = mat3(1, 0);
    mat4(1, 1) = mat3(1, 1);
    mat4(1, 2) = mat3(1, 2);
    mat4(1, 3) = .0f;
    mat4(2, 0) = mat3(2, 0);
    mat4(2, 1) = mat3(2, 1);
    mat4(2, 2) = mat3(2, 2);
    mat4(2, 3) = .0f;
    mat4(3, 0) = .0f;
    mat4(3, 1) = .0f;
    mat4(3, 2) = .0f;
    mat4(3, 3) = 1.0f;
}

inline void vec3ToVec4(Vec4& vec4, const Vec3& vec3, float forthValue)
{
    vec4 = Vec4(vec3(0), vec3(1), vec3(2), forthValue);
}

inline void vec3ToVec4(Vec4& vec4, const Vec3& vec3)
{
    vec3ToVec4(vec4, vec3, 1.0f);
}

inline Vec3d vec3ToVect3d(const Vec3& vec)
{
    return Vec3d(vec[0], vec[1], vec[2]);
}

inline void vec3ToVect3d(const Vec3& vec, Vec3d vec3d)
{
    vec3d = Vec3d(vec[0], vec[1], vec[2]);
}

inline Vec3 vec3dToVect3(const Vec3d& vec3d)
{
    return Vec3((float)vec3d[0], (float)vec3d[1], (float)vec3d[2]);
}

inline void vec3dToVect3(const Vec3d& vec3d, Vec3 vec)
{
    vec = Vec3((float)vec3d[0], (float)vec3d[1], (float)vec3d[2]);
}

/**
* Project vector a onto b
*
* @param a: Vector to project.
* @param b: Vector to project onto.
*
* @return The projected vector of a onto b.
*/
inline Vec3 projectVec(const Vec3& a, const Vec3& b)
{
    return (a.dot(b) / b.norm()) * b;
}

#endif // __OE_CORE_EIGEN_HELPERS_H__

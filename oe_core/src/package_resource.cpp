#include "package_resource.h"

#include "core_private.h"

#include "file.h"
#include "task_manager.h"

namespace OE_Core
{

RTTI_IMPL_PARENT(PackageResource, Resource)

PackageResource::PackageResource(const String& name):
    Resource(name)
{
    m_isPackageContentReady = false;
}

PackageResource::~PackageResource()
{
}

void PackageResource::loadImpl()
{
    TaskManager::getInstance().addTask(
            SharedPtr<PackageResourceLoadTask>(new PackageResourceLoadTask(
                std::static_pointer_cast<PackageResource>(shared_from_this()))));
}

void PackageResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    LockGuard lock(m_mutex);
    if(m_isPackageContentReady)
    {
        Json::ValueIterator itr = m_packageContent.begin();
        for( ;itr != m_packageContent.end(); ++itr)
        {
            for(uint32 i = 0; i < (*itr).size(); ++i)
            {
                const String& typeName = itr.key().asString();
                Hash typeHash = hash(typeName);
                const String& name = (*itr)[i].asString();

                addDependency(dependencyCallback, typeHash, name);
            }
        }

        setState(State::Loaded);
    }
}

void PackageResource::addResource(DependencyCallback dependencyCallback, SharedPtr<Resource> resource)
{
    addDependency(dependencyCallback, resource);
}

PackageResourceLoadTask::PackageResourceLoadTask(SharedPtr<PackageResource> resource)
{
    m_resource = resource;
}

void PackageResourceLoadTask::run()
{
    // Open file.
    String fileName("packages/");
    fileName += m_resource->getName();
    fileName += ".json";

    Json::Value packageContent;
    bool result = appLoadJsonFile(fileName.c_str(), packageContent);
    OE_CHECK_MSG(result, OE_CHANNEL_RESOURCE,
        "Cannot open resource %s.", fileName.c_str());

    LockGuard lock(m_resource->m_mutex);
    m_resource->m_packageContent = packageContent;
    m_resource->m_isPackageContentReady = true;
}

} // namespace OE_Engine

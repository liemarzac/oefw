#ifndef __OE_CORE_ALLOCATOR_PAGE_H__
#define __OE_CORE_ALLOCATOR_PAGE_H__

#include "oe_core.h"

#include "std_mutex_helpers.h"

namespace OE_Core
{

class OE_CORE_EXPORT AllocatorPage
{
public:
    virtual ~AllocatorPage()
    {
        if(m_prev)
        {
            m_prev->m_next = m_next;
        }

        if(m_next)
        {
            m_next->m_prev = m_prev;
        }
    }

protected:
    void insert(AllocatorPage* newAllocator)
    {
        if(m_next)
        {
            m_next->m_prev = newAllocator;
        }

        newAllocator->m_prev = this;
        newAllocator->m_next = m_next;

        m_next = newAllocator;
    }

    AllocatorPage* m_next = nullptr;
    AllocatorPage* m_prev = nullptr;
};

} // OE_Core

#endif // __OE_CORE_LINEAR_ALLOCATOR_H__

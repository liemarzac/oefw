#include "resource.h"

#include <memory>

namespace OE_Core
{

RTTI_IMPL(Resource)


Resource::Resource(const String& name):
    m_name(name)
{
    OE_CHECK(name.length() > 0, OE_CHANNEL_RESOURCE);
    m_hash = hash(name);

    m_state = State::Unloaded;
}

Resource::~Resource()
{
    LockGuard lock(m_dependenciesSync);
    for(auto& dependency : m_dependencies)
    {
        dependency.m_resource->removeLoadedCallback(dependency.m_onLoadedCallback);
    }
}

Resource::State Resource::getState() const
{
    if (m_state == State::Unloaded)
        return m_state;

    LockGuard lock(m_dependenciesSync);
    return getStateInternal();
}

Resource::State Resource::getStateInternal() const
{
    // Returns the lowest state value found.
    State state = m_state;

    for(auto& dependency : m_dependencies)
    {
        State dependencyState = dependency.m_resource->getState();
        if(dependencyState < state)
        {
            state = dependencyState;
            if(state == State::Unloaded)
                break;
        }
    }

    return state;
}

Resource::State Resource::getStateEx() const
{
    return m_state;
}

void Resource::setState(State state)
{
    if(m_state == state)
        return;

    m_state = state;
}

void Resource::getDependencies(Vector<SharedPtr<Resource>>& dependencies)
{
    LockGuard lock(m_dependenciesSync);
    for(auto& dependency : m_dependencies)
    {
        dependencies.push_back(dependency.m_resource);
        dependency.m_resource->getDependencies(dependencies);
    }
}

CallbackEntry Resource::addLoadedCallback(SharedPtr<Functor> callback)
{
    if(isLoaded())
    {
        callback->callback();
    }

    return m_onLoadedCallbacks.addCallback(callback);
}

void Resource::removeLoadedCallback(CallbackEntry entry)
{
    m_onLoadedCallbacks.removeCallback(entry);
}

SharedPtr<Resource> Resource::addDependency(DependencyCallback callback, Hash typeHash, const String& name)
{
    // Create resource using the callback.
    DependencyCallbackParams dependencyParams;
    dependencyParams.m_typeHash = typeHash;
    dependencyParams.m_name = name;
    SharedPtr<Resource> baseResource = callback(dependencyParams);

    // Make sure the resource was either existing or created.
    OE_CHECK(baseResource, OE_CHANNEL_RESOURCE);

    // Add the dependency in the list.
    Dependency d;
    d.m_resource = baseResource;
    d.m_onLoadedCallback = baseResource->addLoadedCallback(MakeFunctor(
        &Resource::onDependencyLoaded, this, CallbackType::Permanent));

    if(!isContained(m_dependencies, d))
    {
        m_dependencies.push_back(d);
    }

    return baseResource;
}

SharedPtr<Resource> Resource::getDependency(Hash typeHash, const String& name) const
{
    SharedPtr<Resource> dependency;

    Hash nameHash = hash(name);

    for(auto& d : m_dependencies)
    {
        if(d.m_resource->getTypeHash() == typeHash)
        {
            if(d.m_resource->getHash() == nameHash)
            {
                dependency = d.m_resource;
                break;
            }
        }
    }

    return dependency;
}


void Resource::load()
{
    LockGuard lock(m_dependenciesSync);
    loadInternal();
}

void Resource::loadInternal()
{
    log(OE_CHANNEL_RESOURCE, "Resource loading... [%s] %s", getType(), m_name.c_str());

#if _DEBUG
    m_loadTimer.start();
#endif

    OE_CHECK(m_state == State::Unloaded, OE_CHANNEL_RESOURCE);
    setState(State::Loading);
    loadImpl();
}

void Resource::updateLoading(DependencyCallback dependencyCallback)
{
    LockGuard lock(m_dependenciesSync);
    updateLoadingInternal(dependencyCallback);
}

void Resource::updateLoadingInternal(DependencyCallback dependencyCallback)
{
    OE_CHECK(getStateEx() == State::Loading, OE_CHANNEL_RESOURCE);

    updateLoadingImpl(dependencyCallback);

#if _DEBUG
    if(getStateEx() == State::Loaded)
    {
        m_loadTimer.stop();
    }
    else
    {
        const double TimeOut = 10.0;
        if(m_loadTimer.getElapsedTime() > TimeOut)
        {
            OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE,
                "Resource %s is spending too much time in loading state", getName().c_str());
        }
    }
#endif
}

void Resource::onDependencyLoaded()
{
}

void Resource::onLoaded()
{
    log(OE_CHANNEL_RESOURCE, "Resource loaded - [%s] %s", getType(), m_name.c_str());

    m_onLoadedCallbacks.callback();
}

void resourceDeleter(Resource* resource)
{
    log(OE_CHANNEL_RESOURCE,
        "Resource deleted [%s] %s", resource->getType(), resource->getName().c_str());

    delete resource;
}

} // namespace OE_Core

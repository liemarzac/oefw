#ifndef __OE_CORE_BIN_PACK_H__
#define __OE_CORE_BIN_PACK_H__

#include "oe_core.h"

#include "rect_2d.h"

#include <algorithm>

namespace OE_Core
{

template<typename T>
class BinPack
{
public:
    struct Element
    {
        Element() {
            m_width = 0;
            m_height = 0;
        }

        int m_width;
        int m_height;
        T m_data;
    };

    struct Result
    {
        Rect2D m_rect;
        T m_data;
    };

    BinPack(int width, int height);
    ~BinPack();

    void addElement(const Element& element);
    size_t getNumElements() const;
    bool process();
    bool iterateResults(Rect2D& rect, T& data);
    void resetResultIteration();
    void resetBin();
    void resetBin(int width, int height);
    static bool isElementABiggerThanElementB(const Element& a, const Element& b);

private:
    struct Node;
    typedef Vector<Node*> ChildrenArray;
    typedef typename Vector<Node*>::iterator ChildrenItr;
    typedef typename Vector<Node*>::const_iterator ChildrenConstItr;
    struct Node
    {
        Node() {
            m_parent = NULL;
            m_isLeaf = true;
        }

        ~Node() {
            // Recursively delete children.
            for(ChildrenItr itr = m_children.begin(); itr != m_children.end(); ++itr) {
                delete *itr;
            }
        }

        bool add(const Element& element, Rect2D& rect);
        void getBestFit(
            const Element& element,
            int& currentMinSurfaceDelta,
            Node** currentBestFit);
        void split(const Rect2D& intersectRect, Vector<Node*>* newNodes);
        bool isLeaf() const;
        bool doesContain(const Node* other) const;
        void remove(Node* child);
        void removeDegeneratedNodes(Vector<Node*> nodeArray);

        Rect2D m_rect;
        ChildrenArray m_children;
        Node* m_parent;
        bool m_isLeaf;
    };

    BinPack();

    Vector<Element> m_elementArray;
    int m_width;
    int m_height;
    Node* m_rootNode;
    Vector<Result> m_resultArray;
    int m_currentResultIndex;
};

template<typename T>
BinPack<T>::BinPack(int width, int height) {
    m_width = width;
    m_height = height;
    m_rootNode = new Node();
    m_rootNode->m_rect.m_width = m_width;
    m_rootNode->m_rect.m_height = m_height;
    m_currentResultIndex = 0;
}

template<typename T>
BinPack<T>::~BinPack() {
    delete m_rootNode;
}

template<typename T>
void BinPack<T>::addElement(const Element& element) {
    OE_CHECK_MSG(element.m_width > 0, OE_CHANNEL_CORE, "Cannot pack a rectangle of 0 width");
    OE_CHECK_MSG(element.m_height > 0, OE_CHANNEL_CORE, "Cannot pack a rectangle of 0 height");
    m_elementArray.push_back(element);
}

template<typename T>
void BinPack<T>::resetBin() {
    resetBin(m_width, m_height);
}

template<typename T>
void BinPack<T>::resetBin(int width, int height) {
    for(ChildrenItr itr = m_rootNode->m_children.begin(); itr != m_rootNode->m_children.end(); ++itr) {
        delete *itr;
    }
    m_rootNode->m_children.clear();
    m_rootNode->m_rect.m_width = width;
    m_rootNode->m_rect.m_height = height;
    m_rootNode->m_isLeaf = true;
    m_resultArray.clear();
    m_currentResultIndex = 0;
    m_elementArray.clear();
}

template<typename T>
bool BinPack<T>::process() {
    // Reset tree and result in order to call this function several time safely.
    for(ChildrenItr itr = m_rootNode->m_children.begin(); itr != m_rootNode->m_children.end(); ++itr) {
        delete *itr;
    }
    m_rootNode->m_children.clear();
    m_resultArray.clear();
    m_currentResultIndex = 0;

    // sort bigger to smaller.
    std::sort(m_elementArray.begin(), m_elementArray.end(), isElementABiggerThanElementB);

    typename Vector<Element>::const_iterator itr = m_elementArray.begin();
    const typename Vector<Element>::const_iterator itrEnd = m_elementArray.end();
    Rect2D rect;
    for(;itr != itrEnd; ++itr) {
        if(m_rootNode->add(*itr, rect)) {
            Result result;
            result.m_rect = rect;
            result.m_data = (*itr).m_data;
            m_resultArray.push_back(result);
        }
        else {
            // Cannot add node, the bin is full.
            return false;
        }
    }

    return true;
}

template<typename T>
size_t BinPack<T>::getNumElements() const {
    return m_resultArray.size();
}

template<typename T>
bool BinPack<T>::iterateResults(Rect2D& rect, T& data) {
    if(m_currentResultIndex >= (int)m_resultArray.size())
        return false;

    rect = m_resultArray[m_currentResultIndex].m_rect;
    data = m_resultArray[m_currentResultIndex].m_data;
    ++m_currentResultIndex;

    return true;
}

template<typename T>
void BinPack<T>::resetResultIteration() {
    m_currentResultIndex = 0;
}

template<typename T>
bool BinPack<T>::isElementABiggerThanElementB(const Element& a, const Element& b) {
    return a.m_width + a.m_height > b.m_width + b.m_height;
}

template<typename T>
bool BinPack<T>::Node::add(const Element& element, Rect2D& rect) {
    // Get the best fit.
    Node* bestFitNode = NULL;
    int bestSurface = INT_MAX;
    getBestFit(element, bestSurface, &bestFitNode);

    if(!bestFitNode)
        return false;

    // Create a used rectangle at the top-left corner.
    rect.m_x = bestFitNode->m_rect.m_x;
    rect.m_y = bestFitNode->m_rect.m_y;
    rect.m_width = element.m_width;
    rect.m_height = element.m_height;

    // Split the best fit node.
    Vector<Node*> newNodes;
    bestFitNode->split(rect, &newNodes);
    removeDegeneratedNodes(newNodes);
    newNodes.clear();

    // Split other rectangles intersecting with the element.
    split(rect, &newNodes);
    removeDegeneratedNodes(newNodes);

    return bestFitNode != NULL;
}

template<typename T>
void BinPack<T>::Node::getBestFit(
    const Element& element,
    int& currentMinDelta,
    Node** currentBestFit) {
    // First check if the element fits in the rectangle.
    // If it does not, then it is not worth checking this node or its children.
    if(m_rect.m_width < element.m_width || m_rect.m_height < element.m_height)
        return;

    if(isLeaf()) {
        // Check if the delta width and height is smaller than other free rectangles.
        const int delta = (m_rect.m_width - element.m_width) + (m_rect.m_height - element.m_height);
        if(delta < currentMinDelta) {
            // This node is a better fit.
            currentMinDelta = delta;
            *currentBestFit = this;
        }
    }
    else {
        for(ChildrenItr itr = m_children.begin(); itr < m_children.end(); ++itr) {
            (*itr)->getBestFit(element, currentMinDelta, currentBestFit);
        }
    }
}

template<typename T>
void BinPack<T>::Node::split(const Rect2D& intersectRect, Vector<Node*>* newNodes) {
    // Do not go further if there is no overlap between both rectangles.
    if(!intersectRect.doesOverlap(m_rect))
        return;

    if(isLeaf()) {
        // The node is no longer a leaf wheter we can create subnodes of not.
        m_isLeaf = false;

        // Create new node at the top of the rectangle intersecting.
        if(intersectRect.m_y > m_rect.m_y &&
            intersectRect.m_y < m_rect.m_y + m_rect.m_height) {
            Node* topNode = new Node();
            topNode->m_rect.m_x = m_rect.m_x;
            topNode->m_rect.m_y = m_rect.m_y;
            topNode->m_rect.m_width = m_rect.m_width;
            topNode->m_rect.m_height = intersectRect.m_y - m_rect.m_y;
            topNode->m_parent = this;
            m_children.push_back(topNode);
            if(newNodes)
                newNodes->push_back(topNode);
        }

        // Create new node at the bottom of the rectangle intersecting.
        if(intersectRect.m_y + intersectRect.m_height < m_rect.m_y + m_rect.m_height &&
            intersectRect.m_y +  intersectRect.m_height > m_rect.m_y) {
            Node* bottomNode = new Node();
            bottomNode->m_rect.m_x = m_rect.m_x;
            bottomNode->m_rect.m_y = intersectRect.m_y +  intersectRect.m_height;
            bottomNode->m_rect.m_width = m_rect.m_width;
            bottomNode->m_rect.m_height = (m_rect.m_y + m_rect.m_height) - (intersectRect.m_y +  intersectRect.m_height);
            bottomNode->m_parent = this;
            m_children.push_back(bottomNode);
            if(newNodes)
                newNodes->push_back(bottomNode);
        }

        // Create new node at the left of the rectangle intersecting.
        if(intersectRect.m_x > m_rect.m_x &&
            intersectRect.m_x < m_rect.m_x + m_rect.m_width) {
            Node* leftNode = new Node();
            leftNode->m_rect.m_x = m_rect.m_x;
            leftNode->m_rect.m_y = m_rect.m_y;
            leftNode->m_rect.m_width = intersectRect.m_x - m_rect.m_x;
            leftNode->m_rect.m_height = m_rect.m_height;
            leftNode->m_parent = this;
            m_children.push_back(leftNode);
            if(newNodes)
                newNodes->push_back(leftNode);
        }

        // Create new node at the right of the rectangle intersecting.
        if(intersectRect.m_x + intersectRect.m_width > m_rect.m_x &&
            intersectRect.m_x + intersectRect.m_width < m_rect.m_x + m_rect.m_width) {
            Node* rightNode = new Node();
            rightNode->m_rect.m_x = intersectRect.m_x + intersectRect.m_width;
            rightNode->m_rect.m_y = m_rect.m_y;
            rightNode->m_rect.m_width = (m_rect.m_x + m_rect.m_width) - (intersectRect.m_x + intersectRect.m_width);
            rightNode->m_rect.m_height = m_rect.m_height;
            rightNode->m_parent = this;
            m_children.push_back(rightNode);
            if(newNodes)
                newNodes->push_back(rightNode);
        }
    }
    else {
        for(ChildrenItr itr = m_children.begin(); itr != m_children.end(); ++itr) {
            (*itr)->split(intersectRect, newNodes);
        }
    }
}

template<typename T>
bool BinPack<T>::Node::doesContain(const Node* other) const {
    if(other != this) {
        if(isLeaf()) {
            return  m_rect.doesContain(other->m_rect);
        }
        else {
            if(m_rect.doesContain(other->m_rect)) {
                for(ChildrenConstItr itr = m_children.begin(); itr != m_children.end(); ++itr) {
                    if((*itr)->doesContain(other))
                        return true;
                }
            }
        }
    }
    return false;
}

template<typename T>
void BinPack<T>::Node::remove(Node* childToDelete) {
    for(ChildrenItr itr = m_children.begin(); itr != m_children.end(); ++itr) {
        if((*itr) == childToDelete) {
            delete childToDelete;
            m_children.erase(itr);
            if(m_children.size() == 0 && m_parent) {
                // There are no more children so this node becomes useless.
                // Tell the parent to delete this node.
                m_parent->remove(this);
            }
            break;
        }
    }
}

template<typename T>
void BinPack<T>::Node::removeDegeneratedNodes(Vector<Node*> nodeArray) {
    // Degenerated rectangles can be fully contained in bigger ones.
    for(typename Vector<Node*>::iterator itr = nodeArray.begin(); itr != nodeArray.end(); ++itr) {
        if(doesContain(*itr)) {
            // Delete degenerated node.
            (*itr)->m_parent->remove(*itr);
        }
    }
}

template<typename T>
bool BinPack<T>::Node::isLeaf() const {
    return m_isLeaf;
}

}  // namespace OE_Core

#endif // __OE_CORE_BIN_PACK_H__

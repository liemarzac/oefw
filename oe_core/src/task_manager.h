#ifndef __OE_CORE_TASK_MANAGER_H__
#define __OE_CORE_TASK_MANAGER_H__

#include "core_private.h"
#include "singleton.h"
#include "std_smartptr_helpers.h"

#include <condition_variable>
#include <deque>
#include <thread>


namespace OE_Core
{

class Runnable;

class OE_CORE_EXPORT TaskManager : public Singleton<TaskManager>
{
public:
    struct WorkerParams
    {
        TaskManager* m_taskManager;
        uint m_id;
    };

    TaskManager();
    ~TaskManager();

    uint addTask(SharedPtr<Runnable> task);
    void update(float deltaT);
    inline bool isQuitting() const;
    SharedPtr<Runnable> popNextTask();
    void onTaskFinished(SharedPtr<Runnable> task);
    bool exist(uint taskId) const;
    bool cancel(uint taskId);

private:
    std::deque<SharedPtr<Runnable>> m_pendingTasks;
    Vector<uint> m_remainingTaskIds;
    Vector<std::thread> m_workers;
    mutable std::mutex m_tasksMutex;
    std::condition_variable m_taskNotifier;
    bool m_bTerminates;
    uint m_nextTaskId;
};

} // namespace OE_Core

#endif // __OE_CORE_TASK_MANAGER_H__

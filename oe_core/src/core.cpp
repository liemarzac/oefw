#include "core_private.h"

#include "memory.h"

#include <ctime>
#include <random>
#include <Eigen/Geometry>

namespace OE_Core
{

static const size_t MaxMessageSize = 10 * OneKb;

void log(const char* message)
{
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    OutputDebugStringA(message);
#else
    printf("%s", message);
#endif
}

void log(const char* channel, const char* format, ...)
{
    char message[MaxMessageSize];
    getTimeAsString(message, MaxMessageSize);
    size_t messageSize = strlen(message);
    OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, " [%s] ", channel);
    messageSize = strlen(message);
    OE_WELD_ARGS(message + messageSize, MaxMessageSize - messageSize);
    messageSize = strlen(message);
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\n");
    log(message);
}

void logWarning(const char* channel, const char* file, int line, const char* function, const char* format, ...)
{
    char message[MaxMessageSize];
    getTimeAsString(message, MaxMessageSize);
    size_t messageSize = strlen(message);
    char* failMessage = NULL;
    char info[MaxMessageSize];
    OE_SPRINTF(info, MaxMessageSize, " [%s][Warning]\n    File: %s\n    Line: %d\n    Function: %s", channel, file, line, function);
    if(format)
    {
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "%s\n    Msg: ", info);
#else
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "\033[1;33m%s\n    Msg: ", info);
#endif
        messageSize = strlen(message);
        OE_WELD_ARGS(message + messageSize, MaxMessageSize - messageSize);
        messageSize = strlen(message);
    }
    else
    {
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "%s", info);
#else
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "\033[1;31m%s", info);
#endif
    }

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\n");
#else
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\033[0m\n");
#endif

    log(message);
}

void logError(const char* channel, const char* format, ...)
{
    char message[MaxMessageSize];
    getTimeAsString(message, MaxMessageSize);
    size_t messageSize = strlen(message);
    OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "\033[1;31m [%s][Error] ", channel);
    messageSize = strlen(message);
    OE_WELD_ARGS(message + messageSize, MaxMessageSize - messageSize);
    messageSize = strlen(message);
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\033[0m\n");
    log(message);
}

void logCheckFailed(const char* channel, const char* file, int line, const char* function, const char* format, ...)
{
    char message[MaxMessageSize];
    getTimeAsString(message, MaxMessageSize);
    size_t messageSize = strlen(message);
    char* failMessage = NULL;
    char info[MaxMessageSize];
    OE_SPRINTF(info, MaxMessageSize, " [%s][Error] Check failed.\n    File: %s\n    Line: %d\n    Function: %s", channel, file, line, function);
    if(format)
    {
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "%s\n    Msg: ", info);
#else
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "\033[1;31m%s\n    Msg: ", info);
#endif
        messageSize = strlen(message);
        OE_WELD_ARGS(message + messageSize, MaxMessageSize - messageSize);
        messageSize = strlen(message);
    }
    else
    {
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "%s", info);
#else
        OE_SPRINTF(message + messageSize, MaxMessageSize - messageSize, "\033[1;31m%s", info);
#endif
    }

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\n");
#else
    OE_STRCAT(message + messageSize, MaxMessageSize - messageSize, "\033[0m\n");
#endif

    log(message);
}

void getTimeAsString(char* buffer, size_t bufferSize)
{
    const int MinBufferSize = 9;
    OE_CHECK_MSG(buffer != NULL, OE_CHANNEL_CORE, "Buffer is null.");
    OE_CHECK_MSG(bufferSize >= MinBufferSize, OE_CHANNEL_CORE, "Buffer size must be equals or greater than %d.", MinBufferSize);

    time_t timeValue;
    time(&timeValue);
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    tm timeStruct;
    tm* pTime = &timeStruct;
    gmtime_s(pTime, &timeValue);
#else
    tm* pTime = gmtime(&timeValue);
#endif
    OE_SPRINTF(buffer, bufferSize, "%02d:%02d:%02d", pTime->tm_hour, pTime->tm_min, pTime->tm_sec);
}

}

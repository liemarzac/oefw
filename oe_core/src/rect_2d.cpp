#include "rect_2d.h"

namespace OE_Core
{

Rect2D::Rect2D()
{
    m_x = 0;
    m_y = 0;
    m_width = 0;
    m_height = 0;
}

bool Rect2D::doesOverlap(const Rect2D& other) const
{
    return !(m_x >= other.m_x + other.m_width ||
        m_y >= other.m_y + other.m_height ||
        other.m_x >= m_x + m_width ||
        other.m_y >= m_y + m_height);
}

bool Rect2D::doesContain(const Rect2D& other) const
{
    return other.m_x >= m_x && 
        other.m_x + other.m_width <= m_x + m_width &&
        other.m_y >= m_y &&
        other.m_y + other.m_height <= m_y + m_height;
}

template<>
void serialize(Buffer& buffer, const Rect2D& rect)
{
    serialize(buffer, rect.m_x);
    serialize(buffer, rect.m_y);
    serialize(buffer, rect.m_width);
    serialize(buffer, rect.m_height);
}

template<>
void deserialize(Buffer& buffer, Rect2D& rect)
{
    deserialize(buffer, rect.m_x);
    deserialize(buffer, rect.m_y);
    deserialize(buffer, rect.m_width);
    deserialize(buffer, rect.m_height);
}

template<>
size_t calculateSerializedSize(const Rect2D& rect)
{
    return sizeof(rect.m_x) +
        sizeof(rect.m_x) +
        sizeof(rect.m_width) +
        sizeof(rect.m_height);
}

} //namespace OE_Core

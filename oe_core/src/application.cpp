#include "application.h"
#include "core_private.h"

#include <algorithm>

namespace OE_Core
{

std::function<void(String&)> g_getApplicationNameFunction;
String g_exeFolderPath;

void appSetExePath(const String& exePath)
{
    g_exeFolderPath = exePath;
    std::replace(g_exeFolderPath.begin(), g_exeFolderPath.end(), '\\', '/');
    const std::size_t posOfLastSlash = g_exeFolderPath.find_last_of('/');
    g_exeFolderPath = g_exeFolderPath.substr(0, posOfLastSlash);
}

void appInit()
{
}

void appGetName(String& appName)
{
    if(g_getApplicationNameFunction)
    {
        g_getApplicationNameFunction(appName);
    }
}

void appSetGetNameFunction(std::function<void(String&)> func)
{
    g_getApplicationNameFunction = func;
}

}
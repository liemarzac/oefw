#ifndef __OE_CORE_SERIALIZABLE_H__
#define __OE_CORE_SERIALIZABLE_H__

#include "oe_core.h"

// OE_Core
#include "buffer.h"
#include "map.h"
#include "oe_string.h"
#include "vector.h"

namespace OE_Core
{

void OE_CORE_EXPORT serialize_impl(Buffer& buffer, const String& value, const String*);
void OE_CORE_EXPORT deserialize_impl(Buffer& buffer, String& value, const String*);
size_t OE_CORE_EXPORT calculateSerializedSize_impl(const String& value, const String*);

void OE_CORE_EXPORT serialize_impl(Buffer& toBuffer, const Buffer& fromBuffer, const Buffer*);
void OE_CORE_EXPORT deserialize_impl(Buffer& fromBuffer, Buffer& toBuffer, const Buffer*);
size_t OE_CORE_EXPORT calculateSerializedSize_impl(const Buffer& buffer, const Buffer*);

template<typename T>
inline void serializeIntrinsic(Buffer& buffer, const T& value)
{
    OE_CHECK_MSG(buffer.m_offset + sizeof(value) <= buffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while serializing");
    memcpy(buffer.m_rawData + buffer.m_offset, &value, sizeof(value));
    buffer.m_offset += sizeof(value);
}

inline void serialize_impl(Buffer& buffer, const uint32& value, const uint32*)
{
    serializeIntrinsic(buffer, value);
}

inline void serialize_impl(Buffer& buffer, const int32& value, const int32*)
{
    serializeIntrinsic(buffer, value);
}

template<typename T>
void serialize_impl(Buffer& buffer, const Vector<T>& array, const Vector<T>*)
{
    serialize_impl(buffer, (uint32)array.size(), static_cast<uint32*>(0));
    typename Vector<T>::const_iterator itr = array.begin();
    for(; itr != array.end(); ++itr)
    {
        serialize_impl(buffer, *itr, static_cast<T*>(0));
    }
}

template<typename K, typename V>
void serialize_impl(Buffer& buffer, const Map<K, V>& map, const Map<K, V>*)
{
    serialize_impl(buffer, (uint32)map.size(), static_cast<uint32*>(0));
    typename Map<K, V>::const_iterator itr = map.begin();
    for(; itr != map.end(); ++itr)
    {
        serialize_impl(buffer, (*itr).first, static_cast<K*>(0));
        serialize_impl(buffer, (*itr).second, static_cast<V*>(0));
    }
}

template<typename T>
void serialize(Buffer& buffer, const T& value)
{
    serialize_impl(buffer, value, static_cast<const T*>(0));
}

template<typename T>
void deserializeIntrinsic(Buffer& buffer, T& value)
{
    OE_CHECK_MSG(buffer.m_offset + sizeof(value) <= buffer.m_size, OE_CHANNEL_CORE, "Buffer overflow while deserializing");
    memcpy(&value, buffer.m_rawData + buffer.m_offset, sizeof(value));
    buffer.m_offset += sizeof(value);
}

inline void deserialize_impl(Buffer& buffer, uint32& value, uint32*)
{
    deserializeIntrinsic(buffer, value);
}

inline void deserialize_impl(Buffer& buffer, int32& value, int32*)
{
    deserializeIntrinsic(buffer, value);
}

template<typename T>
void deserialize_impl(Buffer& buffer, Vector<T>& array, Vector<T>*)
{
    uint32 arraySize;
    deserialize_impl(buffer, arraySize, static_cast<uint32*>(0));

    array.resize(arraySize);
    for(uint32_t i = 0; i < arraySize; ++i)
    {
        deserialize_impl(buffer, array[i], static_cast<T*>(0));
    }
}

template<typename K, typename V>
void deserialize_impl(Buffer& buffer, Map<K, V>& map, Map<K, V>*)
{
    uint32 mapSize;
    deserialize_impl(buffer, mapSize, static_cast<uint32*>(0));

    map.clear();
    for(uint32 i = 0; i < mapSize; ++i)
    {
        std::pair<K, V> pair;
        deserialize_impl(buffer, pair.first, static_cast<K*>(0));
        deserialize_impl(buffer, pair.second, static_cast<V*>(0));
        map.insert(pair);
    }
}

template<typename T>
void deserialize(Buffer& buffer, T& value)
{
    deserialize_impl(buffer, value, static_cast<T*>(0));
}

template<typename T>
size_t calculateSerializedSizeIntrinsic(const T& value)
{
    return sizeof(value);
}

inline size_t calculateSerializedSize_impl(const uint32& value, const uint32*)
{
    return calculateSerializedSizeIntrinsic(value);
}

inline size_t calculateSerializedSize_impl(const int32& value, const int32*)
{
    return calculateSerializedSizeIntrinsic(value);
}

template<typename T>
size_t calculateSerializedSize_impl(const Vector<T>& array, const Vector<T>*)
{
    size_t size = sizeof(uint32_t);
    typename Vector<T>::const_iterator itr = array.begin();
    for(; itr != array.end(); ++itr)
    {
        size += calculateSerializedSize_impl(*itr, static_cast<T*>(0));
    }
    return size;
}

template<typename K, typename V>
size_t calculateSerializedSize_impl(const Map<K, V>& map, const Map<K, V>* disambiguation)
{
    size_t size = sizeof(uint32_t);
    typename Map<K, V>::const_iterator itr = map.begin();
    for(; itr != map.end(); ++itr)
    {
        size += calculateSerializedSize_impl((*itr).first, static_cast<K*>(0));
        size += calculateSerializedSize_impl((*itr).second, static_cast<V*>(0));
    }
    return size;
}

template<typename T>
size_t calculateSerializedSize(const T& value)
{
    return calculateSerializedSize_impl(value, static_cast<const T*>(0));
}

} // namespace OE_Core

#endif // __OE_CORE_SERIALIZABLE_H__

#ifndef __OE_CORE_TASK_GROUP_H__
#define __OE_CORE_TASK_GROUP_H__

#include "core_private.h"

#include "callback.h"
#include "std_smartptr_helpers.h"
#include <atomic>

namespace OE_Core
{

class Runnable;

class OE_CORE_EXPORT TaskGroup
{
public:
    TaskGroup();
    ~TaskGroup();

    void addTask(SharedPtr<Runnable> task);
    void execute();
    void waitForCompletion();
    bool isCompleted() const;

    CallbackEntry addOnTasksFinishedCallback(SharedPtr<Functor> callback);
    void removeOnTasksFinishedCallback(CallbackEntry entry);

private:
    TaskGroup& operator=(const TaskGroup& rhs);
    TaskGroup(TaskGroup& rhs);

    void onTaskFinished();

    Vector<SharedPtr<Runnable>> m_pendings;
    std::atomic<uint> m_nRunning;
    SharedPtr<Functor> m_callback;
    FunctorGroup m_onTasksFinished;
};

} // namespace OE_Core

#endif // __OE_CORE_TASK_GROUP_H__

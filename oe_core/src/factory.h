#ifndef __OE_CORE_FACTORY_H__
#define __OE_CORE_FACTORY_H__

#include "oe_core.h"

// OE_Core
#include "callback.h"
#include "map.h"
#include "hash.h"
#include "std_smartptr_helpers.h"

namespace OE_Core
{

#define OE_CHANNEL_FACTORY "Factory"

template<typename ProductType, typename ConstructParamType>
class Factory
{
public:
    typedef std::function<ProductType(ConstructParamType)> ProductMaker;

    Factory();
    virtual ~Factory();

    void addProduction(Hash productHash, ProductMaker maker);
    ProductType makeProduct(Hash productHash, ConstructParamType params);

private:
    typedef Map<Hash, ProductMaker> ProductionLines;
    ProductionLines m_productionLines;
};

template<typename ProductType, typename ConstructParamType>
Factory<ProductType, ConstructParamType>::Factory()
{
}

template<typename ProductType, typename ConstructParamType>
Factory<ProductType, ConstructParamType>::~Factory()
{
}

template<typename ProductType, typename ConstructParamType>
void Factory<ProductType, ConstructParamType>::addProduction(Hash productHash, ProductMaker maker)
{
    m_productionLines[productHash] = maker;
}

template<typename ProductType, typename ConstructParamType>
ProductType Factory<ProductType, ConstructParamType>::makeProduct(Hash productHash, ConstructParamType params)
{
    typename ProductionLines::iterator itr = m_productionLines.find(productHash);
    ProductType product = 0;

    OE_CHECK(itr != m_productionLines.end(), OE_CHANNEL_FACTORY);

    if(itr != m_productionLines.end())
    {
        product = (*itr).second(params);
    }

    return product;
}

} // namespace OE_Core

#endif // __OE_CORE_FACTORY_H__

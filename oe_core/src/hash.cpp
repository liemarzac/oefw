#include "hash.h"

#include "core_private.h"

namespace OE_Core
{

uint32 murmur3_32(const char* key, size_t len, uint32 seed)
{
    static const uint32 c1 = 0xcc9e2d51;
    static const uint32 c2 = 0x1b873593;
    static const uint32 r1 = 15;
    static const uint32 r2 = 13;
    static const uint32 m = 5;
    static const uint32 n = 0xe6546b64;

    uint32 hash = seed;

    const size_t nblocks = len / 4;
    const uint32* blocks = (const uint32*)key;
    size_t i;
    for(i = 0; i < nblocks; i++)
    {
        uint32 k = blocks[i];
        k *= c1;
        k = (k << r1) | (k >> (32 - r1));
        k *= c2;

        hash ^= k;
        hash = ((hash << r2) | (hash >> (32 - r2))) * m + n;
    }

    const uint8* tail = (const uint8*)(key + nblocks * 4);
    uint32 k1 = 0;

    switch(len & 3)
    {
        case 3:
        k1 ^= tail[2] << 16;
        case 2:
        k1 ^= tail[1] << 8;
        case 1:
        k1 ^= tail[0];

        k1 *= c1;
        k1 = (k1 << r1) | (k1 >> (32 - r1));
        k1 *= c2;
        hash ^= k1;
    }

    hash ^= len;
    hash ^= (hash >> 16);
    hash *= 0x85ebca6b;
    hash ^= (hash >> 13);
    hash *= 0xc2b2ae35;
    hash ^= (hash >> 16);

    return hash;
}

}

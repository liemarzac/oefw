#ifndef __OE_CORE_THREAD_H__
#define __OE_CORE_THREAD_H__

#include "core_private.h"

#include <thread>

namespace OE_Core
{

#if OE_PLATFORM == OE_PLATFORM_WINDOWS

#include <windows.h>

const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push,8)
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.
        LPCSTR szName; // Pointer to name (in user addr space).
        DWORD dwThreadID; // Thread ID (-1=caller thread).
        DWORD dwFlags; // Reserved for future use, must be zero.
    } THREADNAME_INFO;
#pragma pack(pop)

#endif // OE_PLATFORM == OE_PLATFORM_WINDOWS

void OE_CORE_EXPORT setThreadName(uint32_t dwThreadID, const char* threadName);
void OE_CORE_EXPORT setThreadName(const char* threadName);
void OE_CORE_EXPORT setThreadName(std::thread* thread, const char* threadName);

}

#endif // __OE_CORE_THREAD_H__

#ifndef __OE_CORE_STOP_WATCH_H__
#define __OE_CORE_STOP_WATCH_H__

#include "core_private.h"

#if OE_PLATFORM != OE_PLATFORM_WINDOWS
    #include <time.h>
#endif
namespace OE_Core
{

class OE_CORE_EXPORT StopWatch
{
public:
    StopWatch();
    void start();
    void stop();
    double getElapsedTime() const;

private:
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    LARGE_INTEGER m_start;
    LARGE_INTEGER m_stop;
    LARGE_INTEGER m_frequency;
    double LIToSecs(LARGE_INTEGER & L) const;
#else
    timespec m_start;
    timespec m_stop;
#endif

    bool m_bStarted;
};

} // namespace OE_Core
#endif // __OE_CORE_STOP_WATCH_H__

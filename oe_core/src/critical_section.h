#ifndef __OE_CORE_CRITICAL_SECTION_H__
#define __OE_CORE_CRITICAL_SECTION_H__

#include "core_private.h"

#include "std_atomic_helpers.h"
#include <condition_variable>
#include <mutex>

namespace OE_Core
{

class OE_CORE_EXPORT CriticalSection
{
public:

    enum class LockOperation
    {
        Read,
        Write
    };

    CriticalSection();
    ~CriticalSection();

    void lock(LockOperation operation);
    void unlock();

private:
    std::condition_variable m_unlockNotifier;
    AtomicInt m_counter;
    AtomicInt m_lockCounter;
    std::mutex m_sync;
};

class OE_CORE_EXPORT ScopedCriticalSection
{
public:
    ScopedCriticalSection(CriticalSection& cs,
        CriticalSection::LockOperation operation =
        CriticalSection::LockOperation::Write);

    ~ScopedCriticalSection();

private:
    CriticalSection& m_cs;
};

class OE_CORE_EXPORT ReadScopedCriticalSection
{
public:
    ReadScopedCriticalSection(CriticalSection& cs);

private:
    ScopedCriticalSection m_scs;
};

class OE_CORE_EXPORT WriteScopedCriticalSection
{
public:
    WriteScopedCriticalSection(CriticalSection& cs);

private:
    ScopedCriticalSection m_scs;
};

using ScopedCS = ScopedCriticalSection;
using WriteScopedCS = WriteScopedCriticalSection;
using ReadScopedCS = ReadScopedCriticalSection;

}
#endif

#ifndef __OE_CORE_LINEAR_ALLOCATOR_H__
#define __OE_CORE_LINEAR_ALLOCATOR_H__

#include "oe_core.h"

#include "allocator_page.h"
#include "std_mutex_helpers.h"

namespace OE_Core
{

class OE_CORE_EXPORT LinearAllocatorPage : public AllocatorPage
{
public:
    LinearAllocatorPage() = delete;
    LinearAllocatorPage(size_t size);
    virtual ~LinearAllocatorPage();
    NO_COPY(LinearAllocatorPage);

    void* alloc(size_t size, size_t targetPageSize, size_t alignment);
    void reset();

private:
    size_t m_offset = 0;
    size_t m_size = 0;
    byte* m_raw = nullptr;
};

class OE_CORE_EXPORT LinearAllocator
{
public:
    LinearAllocator() = delete;
    LinearAllocator(size_t size)
    {
        m_targetPageSize = size;
        m_firstPage = new LinearAllocatorPage(size);
    }
    ~LinearAllocator()
    {
        delete m_firstPage;
    }
    NO_COPY(LinearAllocator);

    void* alloc(size_t size, size_t alignement)
    {
        LockGuard lock(m_mutex);
        return m_firstPage->alloc(size, m_targetPageSize, alignement);
    }

    void reset()
    {
        LockGuard lock(m_mutex);
        m_firstPage->reset();
    }

private:
    LinearAllocatorPage* m_firstPage = nullptr;
    size_t m_targetPageSize = 0;
    Mutex m_mutex;
};


} // OE_Core

#endif // __OE_CORE_LINEAR_ALLOCATOR_H__

#include "AssertException.h"
#include <cstring>

namespace UnitTest {

AssertException::AssertException(char const* description, char const* filename, int lineNumber)
    : m_lineNumber(lineNumber)
{
    using namespace std;

#ifdef _WIN32
    strcpy_s(m_description, strlen(description), description);
    strcpy_s(m_filename, strlen(filename), filename);
#else
    strcpy(m_description, description);
    strcpy(m_filename, filename);
#endif
}

AssertException::~AssertException() throw()
{
}

char const* AssertException::what() const throw()
{
    return m_description;
}

char const* AssertException::Filename() const
{
    return m_filename;
}

int AssertException::LineNumber() const
{
    return m_lineNumber;
}

}

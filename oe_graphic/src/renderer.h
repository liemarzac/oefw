﻿#ifndef __GRAPHIC_RENDERER_H__
#define __GRAPHIC_RENDERER_H__

#include "oe_graphic.h"

// External
#include <condition_variable>
#include <thread>

// OE_Core
#include <buffer.h>
#include <oe_string.h>
#include <rect_2d.h>
#include <singleton.h>
#include <std_smartptr_helpers.h>
#include <vector.h>

// OE_Graphic
#include "graphic_resource_helpers.h"
#include "instance_binding.h"
#include "renderer_config.h"
#include "resource_creation_manager.h"

namespace OE_Core
{
    class RingBuffer;
    class LinearAllocatorPage;
}

namespace OE_Graphic
{

struct DriverCommand;
class DriverInterface;
struct MaterialConstants;
class MaterialResource;
struct RendererConfig;
class RenderScene;
class VertexStreamResource;
class UBOFactory;


class OE_GRAPHIC_EXPORT Renderer : public OE_Core::Singleton<Renderer>
{
public:
    Renderer();
    virtual ~Renderer();

    bool initFromMainThread(const RendererConfig& config);
    bool initFromRenderThread(const RendererConfig& config);

    void SDL_Attributes();
    uint32 SDL_GetWindowFlags();

    bool isReady();
    bool isOk() const;
    bool isRenderThread() const;

    /*
    * @brief Hold the rendering thread until the main thread provides work.
    * @return True if the main thread provided rendering work or False otherwise.
    */
    bool preRender();
    void render();
    void postRender();
    
    void resize(const ScreenSize& newSize);
    void getShaderResourcePath(
        ShaderType shaderType,
        const String& resourceName,
        String& resourcePath);
    void getShaderResourcePath(String& resourcePath);
    double getFrameTime() const;

    template<class T>
    inline T* allocCreationCmd();
    void commitCreationCmd();

    void waitUntilReady();
    void executeResourceCreationCmds();

    void waitUntilGPUIdle();
    void flush();

    RenderScene* getRenderScene() const;
    UBOFactory* getUBOFactory() const;
    
    VertexBufferHandle createVertexBuffer(CreateVertexBufferParams& params);

    IndexBufferHandle createIndexBuffer(CreateIndexBufferParams& params);

    VertexShaderHandle createVertexShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name));

    PixelShaderHandle createPixelShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name));

    ShaderBindingHandle createShaderBinding(
        const ShaderBindingLayout& layout
        DBG_STR_PARAM_ADD(name));

    PipelineHandle createPipeline(
        const CreatePipelineParams& params
        DBG_STR_PARAM_ADD(name));

    DrawInstanceHandle createDrawInstance(
        const CreateDrawInstanceParams& params
        DBG_STR_PARAM_ADD(name));

    TextureHandle createTexture(CreateTextureParams& params);

    RenderPipelineHandle createRenderPipeline(
        const CreateRenderPipelineParams& params
        DBG_STR_PARAM_ADD(name));

    RenderPassHandle createRenderPass(CreateRenderPassParams& params);

    RenderTargetHandle createRenderTarget(
        const CreateRenderTargetParams& params
        DBG_STR_PARAM_ADD(name));

    VertexBufferHandle createQuad(
        const OE_Core::Rect2Df& extent
        DBG_STR_PARAM_ADD(name));

    ViewHandle createView(
        const CreateViewParams& params
        DBG_STR_PARAM_ADD(name));

    void updateVertexBuffer(
        const VertexBufferHandle& handle,
        OE_Core::UniqueBufferPtr vertices);

    void updateIndexBuffer(
        const IndexBufferHandle& handle,
        OE_Core::UniqueBufferPtr vertices);

    void applyMaterial(const PipelineHandle& handle);

    void updateInstance(
        const DrawInstanceHandle& handle,
        OE_Core::UniqueBufferPtr buffer);

    void drawInstance(
        const DrawInstanceHandle& handle);

    void updateView(
        ViewHandle& handle,
        OE_Core::UniqueBufferPtr viewUBO);

    void updateMaterial(
        PipelineHandle& handle,
        OE_Core::UniqueBufferPtr viewUBO);

    void createRenderScene();

    void destroyRenderScene();
    void clearRenderScenes();

    uint32 getMainThreadRenderSceneIndex() const;

    void publishScene();
    void waitForRenderThread();

    static OE_Core::UniqueBufferPtr createQuadBuffer(
        const OE_Core::Rect2Df& extent);

private:
    void beginFrame();
    void endFrame();
    void beginRenderPass(RenderPassHandle handle, uint32 iSubpass);
    void endRenderPass();
    void iterateRenderPipeline(std::function<void(const RenderPassId&)> callback);

    void flipScenes();

    /*
    * @brief Block the rendering thread until the main thread provides work.
    * @return True if the main thread provided rendering work or False otherwise
    */
    bool waitForMainThread();


    ResourceCreationManager* m_resourceCreationManager = nullptr;
    DriverInterface* m_driver = nullptr;
    ScreenSize m_screenSize;
    std::thread::id m_renderThreadId;

    struct RenderScenePair
    {
        RenderScene* m_pair[2] = { nullptr, nullptr };
    };
    
    RenderScenePair m_scenes;
    uint32 m_iMainThreadScene = 0;
    uint32 m_iRenderThreadScene = 1;
    AtomicBool m_bSceneToRenderReady;
    AtomicBool m_bFlush;
    Mutex m_scenesMutex;
    std::condition_variable m_renderThreadNotifier;
    std::condition_variable m_mainThreadNotifier;
    UBOFactory* m_uboFactory = nullptr;
};

template<class T>
T* Renderer::allocCreationCmd()
{
    void* writePtr = m_resourceCreationManager->addCommand(sizeof(T));
    T* creationCmd = new(writePtr)T();
    return creationCmd;
}

} // namespace OE_Graphic

#endif // __GRAPHIC_RENDERER_H__

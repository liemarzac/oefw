#ifndef __OE_GRAPHIC_RENDER_PIPELINE_RESOURCE_H__
#define __OE_GRAPHIC_RENDER_PIPELINE_RESOURCE_H__

#include "oe_graphic.h"

// OE_Graphic
#include "graphic_resource_helpers.h"

// OE_Core
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT RenderPipelineResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    RenderPipelineResource() = delete;
    RenderPipelineResource(const String& path);
    virtual ~RenderPipelineResource();

    virtual const char* getType() const override;
    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

private:
    enum class LoadingState
    {
        WaitingForDescriptor,
        DescriptorReady,
        WaitingForRenderPasses,
        CreateRenderPipeline,
        WaitingForRenderPipeline,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;

    struct Descriptor
    {
        using RenderPassPath = OE_Core::ResourcePath<RenderPassResPtr>;
        struct RenderPassNode
        {
            RenderPassPath m_renderPass;
            Vector<RenderPassPath> m_waitForDependencies;
        };

        Vector<RenderPassNode> m_renderPassSequence;
    };

    SharedPtr<OE_Core::ResourceLoadTask<Descriptor>> m_loadTaskPtr;
    SharedPtr<Descriptor> m_descriptorPtr;

    RenderPipelineHandle m_handle;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_GRAPHIC_RENDER_PIPELINE_RESOURCE_H__

#ifndef __OE_GRAPHIC_INSTANCE_BINDING_H__
#define __OE_GRAPHIC_INSTANCE_BINDING_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>



namespace OE_Graphic
{

struct InstanceBinding
{
    InstanceBinding() = default;
    InstanceBinding(const InstanceBinding&) = delete;
    InstanceBinding& operator = (const InstanceBinding&) = delete;

    InstanceBinding(InstanceBinding&& rhs)
    {
        *this = std::move(rhs);
    }

    InstanceBinding& operator=(InstanceBinding&& rhs)
    {
        m_textureHandle = rhs.m_textureHandle;
        m_constantBuffer = std::move(rhs.m_constantBuffer);
        m_type = rhs.m_type;
        return *this;
    };

    TextureHandle m_textureHandle;
    UniquePtr<OE_Core::Buffer> m_constantBuffer;
    enum class Type : uint8
    {
        ConstantBuffer,
        ImageSampler
    }m_type;

    uint32 m_iBind = InvalidIndex;
};

}


#endif // __OE_GRAPHIC_INSTANCE_BINDING_H__

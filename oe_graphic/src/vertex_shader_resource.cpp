#include "vertex_shader_resource.h"

#include "graphic_private.h"

// External
#include <memory>

// OE_Core
#include <file.h>
#include <task_manager.h>

// OE_Graphic
#include "renderer.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(VertexShaderResource, Resource)

VertexShaderResource::VertexShaderResource(const String& name):
    Resource(name)
{
}

VertexShaderResource::~VertexShaderResource()
{
}

void VertexShaderResource::loadImpl()
{
    m_loadTask = MakeShared<VertexShaderResourceLoadTask>(getName());
    TaskManager::getInstance().addTask(m_loadTask);
}

void VertexShaderResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    if(m_loadTask->isFinished())
    {
        if(m_loadTask->getHandle().isReady())
        {
            m_handle = m_loadTask->getHandle();
            m_loadTask.reset();
            setState(State::Loaded);
        }
    }
}

VertexShaderResourceLoadTask::VertexShaderResourceLoadTask(const String& resourceName):
    m_resourceName(resourceName)
{
}

void VertexShaderResourceLoadTask::run()
{
    String shaderResourcePath;

    auto& renderer = Renderer::getInstance();

    renderer.getShaderResourcePath(
        ShaderType::VertexShader,
        m_resourceName,
        shaderResourcePath);

    std::ifstream stream;
    appGetResourceStream (shaderResourcePath, stream, std::ios_base::binary);

    OE_CHECK_MSG(
        stream.is_open(),
        OE_CHANNEL_RESOURCE,
        "Cannot open resource %s",
        shaderResourcePath.c_str());

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Create buffer.
    UniqueBufferPtr shaderByteCode(new Buffer(fileSize));
    stream.read((char*)shaderByteCode->m_rawData, fileSize);
    stream.close();

    m_handle = renderer.createVertexShader(
        std::move(shaderByteCode)
        DBG_PARAM_ADD(m_resourceName));
}

} // namespace OE_Graphic

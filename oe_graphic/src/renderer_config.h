#ifndef _OE_GRAPHIC_RENDERER_CONFIG_H__
#define _OE_GRAPHIC_RENDERER_CONFIG_H__

struct SDL_Window;

namespace OE_Graphic
{

struct OE_GRAPHIC_EXPORT RendererConfig
{
    RendererConfig()
    {
        m_window = nullptr;
        m_bVSync = true;
    }

    SDL_Window* m_window;
    ScreenSize m_screenSize;
    bool m_bVSync;
};

}

#endif // _OE_GRAPHIC_RENDERER_CONFIG_H__

#include "graphic_private.h"
#include "material_resource.h"
#include "mesh_resource.h"
#include "pixel_shader_resource.h"
#include "prop_resource.h"
#include "texture_resource.h"
#include "render_pass_resource.h"
#include "render_pipeline_resource.h"
#include "render_target_resource.h"
#include "shader_binding_resource.h"
#include "vertex_shader_resource.h"
#include "vertex_stream_resource.h"
#include "view_resource.h"
#include "resource_manager.h"
#include "std_string_helpers.h"

#include <assimp/cimport.h>

namespace OE_Graphic
{

aiLogStream g_assimpStream;

std::function<void(GraphicResource*)> GraphicResource::m_deleter;

void globalInit()
{
    g_assimpStream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, nullptr);
    aiAttachLogStream(&g_assimpStream);

    g_assimpStream = aiGetPredefinedLogStream(aiDefaultLogStream_FILE, "assimp_log.txt");
    aiAttachLogStream(&g_assimpStream);

    // Register graphic resources to the factory.
    REGISTER_RESOURCE(MaterialResource);
    REGISTER_RESOURCE(MeshResource);
    REGISTER_RESOURCE(PixelShaderResource);
    REGISTER_RESOURCE(PropResource);
    REGISTER_RESOURCE(RenderPassResource);
    REGISTER_RESOURCE(RenderPipelineResource);
    REGISTER_RESOURCE(RenderTargetResource);
    REGISTER_RESOURCE(ShaderBindingResource);
    REGISTER_RESOURCE(TextureResource);
    REGISTER_RESOURCE(VertexShaderResource);
    REGISTER_RESOURCE(VertexStreamResource);
    REGISTER_RESOURCE(ViewResource);
}

AttributeType stringToAttributeType(const String& str)
{
    String localString = str;
    std::transform(localString.begin(), localString.end(), localString.begin(), ::tolower);

    if(localString == "float")
    {
        return AttributeType::Float;
    }
    else if(localString == "float2")
    {
        return AttributeType::Float2;
    }
    else if(localString == "float3")
    {
        return AttributeType::Float3;
    }
    else if(localString == "float4")
    {
        return AttributeType::Float4;
    }
    else if(localString == "uint_norm")
    {
        return AttributeType::UInt_Norm;
    }
    else if(localString == "mat2x2")
    {
        return AttributeType::Mat2x2;
    }
    else if(localString == "mat3x3")
    {
        return AttributeType::Mat3x3;
    }
    else if(localString == "mat4x4")
    {
        return AttributeType::Mat4x4;
    }
    else if(localString == "tex2d")
    {
        return AttributeType::Tex2d;
    }

    OE_CHECK_MSG(
        false,
        OE_CHANNEL_GRAPHIC,
        "Unknown attribute type %s", str.c_str());
    return AttributeType::Unknown;
}

BindingType stringToBindingType(const String& str)
{
    String localString = str;
    std::transform(localString.begin(), localString.end(), localString.begin(), ::tolower);

    if(localString == "uniform")
    {
        return BindingType::Uniform;
    }
    else if(localString == "sampler")
    {
        return BindingType::Sampler;
    }
    else if (localString == "input")
    {
        return BindingType::InputAttachment;
    }

    OE_CHECK_MSG(
        false,
        OE_CHANNEL_GRAPHIC,
        "Unknown binding type %s", str.c_str());
    return BindingType::Unknown;
}

bool strVertexDeclarationToInputs(
    const String& vertexDeclaration,
    Vector<AttributeType>& inputs)
{
    bool bRet = false;

    if(vertexDeclaration == "Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent")
    {
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::Float2);
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::Float3);
        bRet = true;
    }
    else if(vertexDeclaration == "Vertex_XYZ_Color_Normal")
    {
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::UInt_Norm);
        inputs.push_back(AttributeType::Float3);
    }
    else if(vertexDeclaration == "Vertex_XYZ_Color")
    {
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::UInt_Norm);
    }
    else if(vertexDeclaration == "Vertex_XYZW_UV")
    {
        inputs.push_back(AttributeType::Float4);
        inputs.push_back(AttributeType::Float2);
    }
    else if(vertexDeclaration == "Vertex_XYZ_UV")
    {
        inputs.push_back(AttributeType::Float3);
        inputs.push_back(AttributeType::Float2);
    }
    else
    {
        OE_CHECK_MSG(false, OE_CHANNEL_GRAPHIC, "Unknown vertex declaration %s", vertexDeclaration.c_str());
        bRet = false;
    }

    return bRet;

}


}
#ifndef __OE_GRAPHIC_PIXEL_SHADER_RESOURCE_H__
#define __OE_GRAPHIC_PIXEL_SHADER_RESOURCE_H__

#include "oe_graphic.h"

// External
#include <mutex>

// OE_Core
#include <buffer.h>
#include <resource.h>
#include <runnable.h>

// OE_Graphic
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT PixelShaderResource : public OE_Core::Resource
{
friend class PixelShaderResourceLoadTask;
    RTTI_DECLARATION
public:
    PixelShaderResource(const String& name);
    virtual ~PixelShaderResource();

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline PixelShaderHandle getHandle() const
    {
        return m_handle;
    }

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "pixel_shader_resource";
    }

private:
    PixelShaderResource();
    PixelShaderResource& operator=(const PixelShaderResource& rhs);
    PixelShaderResource(PixelShaderResource& rhs);

    PixelShaderHandle m_handle;
    SharedPtr<class PixelShaderResourceLoadTask> m_loadTask;
};

class PixelShaderResourceLoadTask : public OE_Core::Runnable
{
public:
    PixelShaderResourceLoadTask(const String& resourceName);
    virtual void run() override;

    PixelShaderHandle getHandle() const
    {
        return m_handle;
    }

private:
    PixelShaderResourceLoadTask();
    PixelShaderResourceLoadTask& operator=(const PixelShaderResourceLoadTask& rhs);
    PixelShaderResourceLoadTask(PixelShaderResourceLoadTask& rhs);

    String m_resourceName;
    PixelShaderHandle m_handle;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_PIXEL_SHADER_RESOURCE_H__

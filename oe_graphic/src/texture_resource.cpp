#include "texture_resource.h"

#include "graphic_private.h"

// External
#include <gli/gli.hpp>

// OE_Core
#include <file.h>
#include <jpeg_utils.h>
#include <png_utils.h>
#include <serializable.h>
#include <std_mutex_helpers.h>
#include <task_manager.h>

// OE_Graphic
#include "raw_texture.h"
#include "renderer.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(TextureResource, Resource)

TextureResource::TextureResource(const String& name):
    Resource(name)
{
    m_type = Type::PNG;
    m_format = Format::A8R8G8B8;

    if(name.find(".dds") != String::npos)
    {
        m_type = Type::DDS;
    }
    else if(name.find(".png") != String::npos)
    {
        m_type = Type::PNG;
    }
    else if (name.find(".jpg") != String::npos)
    {
        m_type = Type::JPEG;
    }
    else if (name.find(".jpeg") != String::npos)
    {
        m_type = Type::JPEG;
    }
    else if(name.find(".raw") != String::npos)
    {
        m_type = Type::Raw;
    }
    else
    {
        OE_CHECK(false, OE_CHANNEL_RESOURCE);
    }
}

TextureResource::~TextureResource()
{
}

void TextureResource::loadImpl()
{
    m_descriptor = MakeShared<Descriptor>();

    switch(m_type)
    {
    case Type::DDS:
        m_loadTask = MakeShared<TextureDDSResourceLoadTask>(getName(), m_descriptor);
        break;

    case Type::JPEG:
        m_loadTask = MakeShared<TextureJPEGResourceLoadTask>(getName(), m_descriptor);
        break;

    case Type::PNG:
        m_loadTask = MakeShared<TexturePNGResourceLoadTask>(getName(), m_descriptor);
        break;

    case Type::Raw:
        m_loadTask = MakeShared<TextureRawResourceLoadTask>(getName(), m_descriptor);
        break;

    default:
        OE_CHECK(false, OE_CHANNEL_RESOURCE);
    }
    
    TaskManager::getInstance().addTask(m_loadTask);
}

void TextureResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
    {
        if(m_loadTask->isFinished())
        {
            m_width = m_descriptor->m_params.m_width;
            m_height = m_descriptor->m_params.m_height;

            OE_CHECK(m_descriptor->m_params.m_data, OE_CHANNEL_RESOURCE);
            DBG_ONLY(m_descriptor->m_params.m_name = getName());
            auto& renderer = Renderer::getInstance();
            m_handle = renderer.createTexture(m_descriptor->m_params);

            m_loadingState = LoadingState::WaitingForHandle;
        }
        else
        {
            break;
        }
    }

    case LoadingState::WaitingForHandle:
    {
        if(m_handle.isReady())
        {
            m_loadingState = LoadingState::Loaded;
        }
        else
        {
            break;
        }
    }

    case LoadingState::Loaded:
    {
        m_loadTask.reset();
        setState(State::Loaded);
    }
    }
}

TextureRawResourceLoadTask::TextureRawResourceLoadTask(
    const String& resourcePath,
    SharedPtr<TextureResource::Descriptor> descriptor):
        TextureResourceLoadTask(resourcePath, descriptor)
{
}

void TextureRawResourceLoadTask::run()
{
    // Open file.
    std::ifstream stream;
    appGetResourceStream(m_resourcePath, stream, std::ios::binary);

    if(!stream.is_open())
    {
        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Cannot open resource %s.", m_resourcePath.c_str());
        return;
    }

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Copy file to buffer.
    Buffer fileBuffer(fileSize);
    stream.read((char*)fileBuffer.m_rawData, fileSize);
    stream.close();

    RawTexture rawTexture;
    deserialize(fileBuffer, rawTexture);

    // Convert from alpha only to ARGB.
    // 4 for ARGB
    UniquePtr<Buffer> textureBuffer(new Buffer(rawTexture.m_textureWidth * rawTexture.m_textureHeight * 4));
    const uint32  numPixels = (uint32)rawTexture.m_textureBuffer.m_size;
    for(uint32 i = 0; i < numPixels; ++i)
    {
#if OE_RENDERER == OE_RENDERER_DIRECTX_9
        uint32 r = 0xFF;
        uint32 g = 0xFF;
        uint32 b = 0xFF;
        uint32 argb = (r << 16) | (g << 8) | b;
        uint32 alpha = 0;
        rawTexture.m_textureBuffer.read(&alpha, 1);
        argb |= alpha << 24;
#else
        uint32 r = 0xFF;
        uint32 g = 0xFF;
        uint32 b = 0xFF;
        uint32 argb = (b << 16) | (g << 8) | r;
        uint32 alpha = 0;
        rawTexture.m_textureBuffer.read(&alpha, 1);
        argb |= (alpha << 24);
#endif

        textureBuffer->write(argb);
    }
	OE_CHECK(rawTexture.m_textureBuffer.isEndOfBuffer(), OE_CHANNEL_RESOURCE);
    OE_CHECK(textureBuffer->isEndOfBuffer(), OE_CHANNEL_RESOURCE);

    // Create texture.
    textureBuffer->rewind();

    CreateTextureParams& params = m_descriptor->m_params;
    params.m_width = rawTexture.m_textureWidth;
    params.m_height = rawTexture.m_textureHeight;
    params.m_format = TextureFormat::R8G8B8A8_UNORM;
    params.m_nMips = 1;
    params.m_mipSizes.push_back(textureBuffer->m_size);

    m_descriptor->m_params.m_data = std::move(textureBuffer);
}

TexturePNGResourceLoadTask::TexturePNGResourceLoadTask(
    const String& resourcePath,
    SharedPtr<TextureResource::Descriptor> descriptor) :
        TextureResourceLoadTask(resourcePath, descriptor)
{
}

void TexturePNGResourceLoadTask::run()
{
    UniqueBufferPtr data = MakeUnique<Buffer>();
  
    uint32 width = 0;
    uint32 height = 0;
    bool bAlpha = false;
    if(!loadTexturePNG(m_resourcePath, *data, width, height, bAlpha))
    {
        OE_CHECK(false, OE_CHANNEL_RESOURCE);
    }

    m_descriptor->m_params.m_width = width;
    m_descriptor->m_params.m_height = height;
    m_descriptor->m_params.m_format = TextureFormat::R8G8B8A8_UNORM;
    m_descriptor->m_params.m_nMips = 1;
    m_descriptor->m_params.m_mipSizes.push_back(data->m_size);
    m_descriptor->m_params.m_data = std::move(data);
}

TextureDDSResourceLoadTask::TextureDDSResourceLoadTask(
    const String& resourcePath,
    SharedPtr<TextureResource::Descriptor> descriptor) :
        TextureResourceLoadTask(resourcePath, descriptor)
{
}

void TextureDDSResourceLoadTask::run()
{
    String fullyQualifiedPath;

    OE_CHECK_RESULT(appGetFullyQualifiedResourcePath(
        fullyQualifiedPath,
        m_resourcePath), true, OE_CHANNEL_RESOURCE);

    gli::texture2d texture(gli::load(fullyQualifiedPath));

    CreateTextureParams& params = m_descriptor->m_params;
    params.m_width = texture.extent().x;
    params.m_height = texture.extent().y;
    params.m_nMips = (uint32)texture.levels();
    gli::format gliFormat = texture.format();

    switch(gliFormat)
    {
    case gli::FORMAT_RGBA_DXT3_UNORM_BLOCK16:
        m_descriptor->m_params.m_format = TextureFormat::RGBA_DXT3_UNORM_BLOCK16;
        break;

    default:
        OE_CHECK(false, OE_CHANNEL_RESOURCE);
    }

    params.m_mipSizes.reserve(params.m_nMips);
    for(uint32 i = 0; i < params.m_nMips; ++i)
    {
        params.m_mipSizes.push_back(texture[i].size());
    }

    UniqueBufferPtr data = MakeUnique<Buffer>(texture.size());
    data->write(texture.data(), texture.size());
    data->rewind();

    m_descriptor->m_params.m_data = std::move(data);
}

TextureJPEGResourceLoadTask::TextureJPEGResourceLoadTask(
    const String& resourcePath,
    SharedPtr<TextureResource::Descriptor> descriptor) :
    TextureResourceLoadTask(resourcePath, descriptor)
{
}

void TextureJPEGResourceLoadTask::run()
{
    UniqueBufferPtr data = MakeUnique<Buffer>();

    uint32 width = 0;
    uint32 height = 0;
    bool bAlpha = false;
    if (!loadTextureJPEG(m_resourcePath, *data, width, height))
    {
        OE_CHECK(false, OE_CHANNEL_RESOURCE);
    }

    m_descriptor->m_params.m_width = width;
    m_descriptor->m_params.m_height = height;
    m_descriptor->m_params.m_format = TextureFormat::R8G8B8A8_UNORM;
    m_descriptor->m_params.m_nMips = 1;
    m_descriptor->m_params.m_mipSizes.push_back(data->m_size);
    m_descriptor->m_params.m_data = std::move(data);
}

} // namespace OE_Graphic

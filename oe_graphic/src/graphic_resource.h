#ifndef __OE_GRAPHIC_RESOURCE_H__
#define __OE_GRAPHIC_RESOURCE_H__

// OE_Core
#include <oe_core.h>
#include <oe_string.h>
#include <ref_count.h>


namespace OE_Graphic
{

struct GraphicResource : OE_Core::RefCount
{
    GraphicResource()
    {
        m_bReady.store(false);
        m_bError.store(false);
    }

    virtual void deleter() override
    {
        //DBG_ONLY(OE_Core::log(OE_CHANNEL_GRAPHIC, "Unreferenced graphics resource %s", m_name.c_str()));
        m_deleter(this);
    }

    virtual bool isReady() const
    {
        return m_bReady.load();
    }

    AtomicBool m_bReady;
    AtomicBool m_bError;

#ifdef _DEBUG
    String m_name;
#endif

    static std::function<void(GraphicResource*)> m_deleter;
};

struct GraphicHandle
{
    GraphicHandle() = default;

    GraphicHandle(GraphicResource* graphicResource)
    {
        m_resource = graphicResource;
    }

    operator bool() const
    {
        return isValid();
    }

    bool operator==(const GraphicHandle& rhs) const
    {
        return m_resource == rhs.m_resource;
    }

    bool operator!=(const GraphicHandle& rhs) const
    {
        return !m_resource.operator==(rhs.m_resource);
    }

    bool isValid() const
    {
        return m_resource;
    }

    bool isReady() const
    {
        return m_resource && m_resource->isReady();
    }

    void reset()
    {
        m_resource.reset();
    }

    OE_Core::RefCountPtr<GraphicResource> m_resource;
};

struct TextureHandle : GraphicHandle
{
    TextureHandle() = default;
    TextureHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct VertexShaderHandle : GraphicHandle
{
    VertexShaderHandle() = default;
    VertexShaderHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct PixelShaderHandle : GraphicHandle
{
    PixelShaderHandle() = default;
    PixelShaderHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct ShaderBindingHandle : GraphicHandle
{
    ShaderBindingHandle() = default;
    ShaderBindingHandle(GraphicResource* shaderBinding) :
        GraphicHandle(shaderBinding)
    {
    }
};

struct VertexBufferHandle : GraphicHandle
{
    VertexBufferHandle() = default;
    VertexBufferHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct IndexBufferHandle : GraphicHandle
{
    IndexBufferHandle() = default;
    IndexBufferHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct PipelineHandle : GraphicHandle
{
    PipelineHandle() = default;
    PipelineHandle(GraphicResource* pipeline) :
        GraphicHandle(pipeline)
    {
    }
};

struct DrawInstanceHandle : GraphicHandle
{
    DrawInstanceHandle() = default;
    DrawInstanceHandle(GraphicResource* resource) :
        GraphicHandle(resource)
    {
    }
};

struct RenderPipelineHandle : GraphicHandle
{
    RenderPipelineHandle() = default;
    RenderPipelineHandle(GraphicResource* renderPipeline) :
        GraphicHandle(renderPipeline)
    {
    }
};

struct RenderPassHandle : GraphicHandle
{
    RenderPassHandle() = default;
    RenderPassHandle(GraphicResource* renderPass) :
        GraphicHandle(renderPass)
    {
    }
};

struct RenderTargetHandle : GraphicHandle
{
    RenderTargetHandle() = default;
    RenderTargetHandle(GraphicResource* renderTarget) :
        GraphicHandle(renderTarget)
    {
    }
};

struct ViewHandle : GraphicHandle
{
    ViewHandle() = default;
    ViewHandle(GraphicResource* view) :
        GraphicHandle(view)
    {
    }
};

struct BaseRenderPass : public GraphicResource
{
    bool m_bPersistent = false;
};

BaseRenderPass* getBaseResource(const RenderPassHandle& handle);

}

#endif // __OE_GRAPHIC_RESOURCE_H__

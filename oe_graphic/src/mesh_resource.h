#ifndef __OE_GRAPHIC_MESH_RESOURCE_H__
#define __OE_GRAPHIC_MESH_RESOURCE_H__

#include "oe_graphic.h"

// External
#include <atomic>
#include <json/json.h>

// OE_Core
#include <buffer.h>
#include <resource.h>
#include <runnable.h>

// OE_Graphic
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT MeshResource : public OE_Core::Resource
{
friend class MeshResourceLoadTask;
friend class MeshResourceGenerateNormalsTask;
    RTTI_DECLARATION
public:

    struct Descriptor
    {
        VertexStreamResPtr m_vertexStreamResource;
        MaterialResPtr m_materialResource;
    };

    MeshResource(const String& name);
    MeshResource(
        const String& name,
        SharedPtr<Descriptor> descriptor);
    virtual ~MeshResource();

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    void updateVertexStreamResource(
        OE_Core::UniqueBufferPtr vertices,
        OE_Core::UniqueBufferPtr indicies);

    inline ConstVertexStreamResPtr getVertexStreamResource() const
    {
        return m_descriptor->m_vertexStreamResource;
    }

#if !_RELEASE
    inline ConstVertexStreamResPtr getNormalsResource() const
    {
        return m_normalResource;
    }
#endif

    inline MaterialResPtr getMaterialResource() const
    {
        return m_descriptor->m_materialResource;
    }

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "mesh_resource";
    }

private:
    MeshResource();
    MeshResource& operator=(const MeshResource& rhs);
    MeshResource(MeshResource& rhs);

#if !_RELEASE
    VertexStreamResPtr m_normalResource;
#endif

    SharedPtr<Descriptor> m_descriptor;

    //Json::Value m_meshContent;

    enum class InternalLoadingState
    {
        Unloaded,
#if !_RELEASE
        WaitingForNormals,
        NormalsReady,
#endif
        Loaded,
    }m_internalLoadingState = InternalLoadingState::Unloaded;
};

#if !_RELEASE
class MeshResourceGenerateNormalsTask : public OE_Core::Runnable
{
public:
    MeshResourceGenerateNormalsTask(MeshResPtr resource);
    virtual void run() override;

private:
    MeshResourceGenerateNormalsTask();
    MeshResourceGenerateNormalsTask& operator=(const MeshResourceGenerateNormalsTask& rhs);
    MeshResourceGenerateNormalsTask(MeshResourceGenerateNormalsTask& rhs);

    MeshResPtr m_resource;
};
#endif

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_MESH_RESOURCE_H__

#include "render_scene.h"

#include "graphic_private.h"

// OE_Graphic
#include "renderer.h"

// OE_Core
#include <memory.h>
#include <ring_buffer.h>


namespace OE_Graphic
{

RenderScene::RenderScene()
{
    m_viewMatrix = Mat4::Identity();
    m_projectionMatrix = Mat4::Identity();
    m_hasDirectionalDominantLight = false;
    m_bRendering = false;
    m_renderCommands = new RingBuffer(100 * OneKb, false, 16);
}

RenderScene::~RenderScene()
{
    delete m_renderCommands;
}

void RenderScene::copyFrom(const RenderScene& rhs)
{
    m_viewMatrix = rhs.m_viewMatrix;
}

void RenderScene::addMeshInstance(MeshInstancePtr meshInstance)
{
    LockGuard lock(m_meshInstanceCollectionMutex);

    OE_CHECK(m_bRendering == false, OE_CHANNEL_RENDER_SCENE);

    OE_CHECK(meshInstance->isLoaded(), OE_CHANNEL_RENDER_SCENE);
    OE_CHECK(meshInstance->getDrawInstanceHandle(), OE_CHANNEL_RENDER_SCENE);

    auto renderPassId = meshInstance->getRenderPassId();
    OE_CHECK(renderPassId.m_handle, OE_CHANNEL_RENDER_SCENE);
    PerRenderPass& renderPass = m_renderPassMap[renderPassId];
    renderPass.m_id = renderPassId;

    auto pipelineHandle = meshInstance->getPipelineHandle();
    OE_CHECK(pipelineHandle, OE_CHANNEL_RENDER_SCENE);
    uintptr_t pipelineUIntPtr = reinterpret_cast<uintptr_t>(*pipelineHandle.m_resource);

    PerMaterial& material = renderPass.m_materials[pipelineUIntPtr];   
    material.m_pipelineHandle = pipelineHandle;
    material.m_meshInstances.push_back(meshInstance);
}

void RenderScene::removeMeshInstance(MeshInstancePtr meshInstance)
{
    LockGuard lock(m_meshInstanceCollectionMutex);

    OE_CHECK(m_bRendering == false, OE_CHANNEL_RENDER_SCENE);

    auto renderPassId = meshInstance->getRenderPassId();
    auto renderPassItr = m_renderPassMap.find(renderPassId);
    OE_CHECK(renderPassItr != m_renderPassMap.end(), OE_CHANNEL_RENDER_SCENE);
    PerRenderPass& renderPass = (*renderPassItr).second;

    auto pipelineHandle = meshInstance->getPipelineHandle();
    uintptr_t pipelineUIntPtr = reinterpret_cast<uintptr_t>(*pipelineHandle.m_resource);
    auto materialItr = renderPass.m_materials.find(pipelineUIntPtr);
    OE_CHECK(materialItr != renderPass.m_materials.end(), OE_CHANNEL_RENDER_SCENE);
    PerMaterial& material = (*materialItr).second;

    auto meshInstanceItr = std::find(material.m_meshInstances.begin(), material.m_meshInstances.end(), meshInstance);
    OE_CHECK(meshInstanceItr != material.m_meshInstances.end(), OE_CHANNEL_RENDER_SCENE);
    material.m_meshInstances.erase(meshInstanceItr);

    if(material.m_meshInstances.size() == 0)
    {
        // Remove the material from the map.
        renderPass.m_materials.erase(pipelineUIntPtr);
    }

    if(renderPass.m_materials.size() == 0)
    {
        // Remove the render pass from the map.
        m_renderPassMap.erase(renderPassId);
    }
}

void RenderScene::clear()
{
    OE_CHECK(m_bRendering == false, OE_CHANNEL_RENDER_SCENE);

    m_renderPassMap.clear();
}

void RenderScene::startRendering()
{
    OE_CHECK(m_bRendering == false, OE_CHANNEL_RENDER_SCENE);
    m_bRendering = true;

    for(auto& renderPassPair : m_renderPassMap)
    {
        auto& renderPass = renderPassPair.second;
        for(auto& materialPair : renderPass.m_materials)
        {
            for(auto& meshInstance : materialPair.second.m_meshInstances)
            {
                OE_CHECK(!meshInstance->m_bRendering, OE_CHANNEL_RENDER_SCENE)
                meshInstance->m_bRendering = true;
            }
        }
    }
}

void RenderScene::stopRendering()
{
    OE_CHECK(m_bRendering == true, OE_CHANNEL_RENDER_SCENE);

    for(auto& renderPassPair : m_renderPassMap)
    {
        auto& perRenderPass = renderPassPair.second;

        BaseRenderPass* renderPass = getBaseResource(perRenderPass.m_id.m_handle);

        if(renderPass->m_bPersistent)
        {
            for(auto& materialPair : perRenderPass.m_materials)
            {
                for(auto& meshInstance : materialPair.second.m_meshInstances)
                {
                    OE_CHECK(meshInstance->m_bRendering, OE_CHANNEL_RENDER_SCENE)
                        meshInstance->m_bRendering = false;
                }
            }
        }
        else
        {
            perRenderPass.m_materials.clear();
        }
    }

    m_bRendering = false;
}

void RenderScene::commitCommand()
{
    m_renderCommands->commit();
}

void RenderScene::generateDriverCommands(const RenderPassId& renderPassId)
{
     Renderer& renderer = Renderer::getInstance();

     auto& itr = m_renderPassMap.find(renderPassId);
     if(itr == m_renderPassMap.end())
     {
         return;
     }

     for(auto& materialPair : (*itr).second.m_materials)
     {
         auto& material = materialPair.second;
         renderer.applyMaterial(material.m_pipelineHandle);
         for(auto& meshInstance : material.m_meshInstances)
         {
             DrawInstanceHandle handle = meshInstance->getDrawInstanceHandle();
             UniquePtr<Buffer> buffer(new Buffer(sizeof(meshInstance->m_instance)));
             buffer->write(meshInstance->m_instance);
             renderer.updateInstance(handle, std::move(buffer));
             renderer.drawInstance(handle);
         }
     }
}

#ifdef _DEBUG
void RenderScene::setName(const String& name)
{
    m_name = name;
}
#endif


} // namespace OE_Graphic

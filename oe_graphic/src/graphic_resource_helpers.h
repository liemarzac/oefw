#ifndef __OE_GRAPHIC_RESOURCE_HELPERS_H__
#define __OE_GRAPHIC_RESOURCE_HELPERS_H__

#include "std_smartptr_helpers.h"

namespace OE_Graphic
{

class MeshResource;
typedef SharedPtr<MeshResource> MeshResPtr;
typedef SharedPtr<const MeshResource> ConstMeshPtr;

class MaterialResource;
typedef SharedPtr<MaterialResource> MaterialResPtr;
typedef SharedPtr<const MaterialResource> ConstMaterialResPtr;

class PixelShaderResource;
typedef SharedPtr<PixelShaderResource> PixelShaderResPtr;
typedef SharedPtr<const PixelShaderResource> ConstPixelShaderResPtr;

class VertexShaderResource;
typedef SharedPtr<VertexShaderResource> VertexShaderResPtr;
typedef SharedPtr<const VertexShaderResource> ConstVertexShaderResPtr;

class ShaderBindingResource;
typedef SharedPtr<ShaderBindingResource> ShaderBindingResPtr;
typedef SharedPtr<const ShaderBindingResource> ConstVShaderBindingResPtr;

class VertexStreamResource;
typedef SharedPtr<VertexStreamResource> VertexStreamResPtr;
typedef SharedPtr<const VertexStreamResource> ConstVertexStreamResPtr;

class PropResource;
typedef SharedPtr<PropResource> PropResPtr;
typedef SharedPtr<const PropResource> ConstPropResPtr;

class TextureResource;
typedef SharedPtr<TextureResource> TextureResPtr;
typedef SharedPtr<const TextureResource> ConstTextureResPtr;

class RenderPipelineResource;
using RenderPipelineResPtr = SharedPtr<RenderPipelineResource>;
using ConstRenderPipelineResPtr = SharedPtr<const RenderPipelineResource>;

class RenderPassResource;
using RenderPassResPtr = SharedPtr<RenderPassResource>;
using ConstRenderPassResPtr = SharedPtr<const RenderPassResource>;

class RenderTargetResource;
using RenderTargetResPtr = SharedPtr<RenderTargetResource>;
using ConstRenderTargetResPtr = SharedPtr<const RenderTargetResource>;

class ViewResource;
using ViewResPtr = SharedPtr<ViewResource>;
using ConstViewResPtr = SharedPtr<const ViewResPtr>;

}

#endif // __OE_GRAPHIC_RESOURCE_HELPERS_H__

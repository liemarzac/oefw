#include "scene_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>

// OE_Core
#include <task_manager.h>

namespace OE_Graphic
{

SceneResource::SceneResource(const String& path):
    Resource(path)
{
}

void SceneResource::loadImpl()
{
    m_loadTaskPtr = MakeShared<ResourceLoadTask<Descriptor>>(
        getName(),
        MakeShared<Descriptor>(),
        &SceneResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void SceneResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    
}

void SceneResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)
{
    
}


} // namespace OE_Graphic

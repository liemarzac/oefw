#include "resource_creation_manager.h"

#include "driver_command.h"
#include "graphic_private.h"

// OE_Core
#include <memory.h>

namespace OE_Graphic
{

ResourceCreationManager::ResourceCreationManager()
{
    m_creations[0].m_creationCmdAllocator = new LinearAllocator(512 * OneKb);
    m_creations[1].m_creationCmdAllocator = new LinearAllocator(512 * OneKb);
    m_creationProducer = &m_creations[0];
    m_creationConsumer = &m_creations[1];
}

ResourceCreationManager::~ResourceCreationManager()
{
    delete m_creations[1].m_creationCmdAllocator;
    delete m_creations[0].m_creationCmdAllocator;
}

void* ResourceCreationManager::addCommand(size_t size)
{
    m_creationMutex.lock();
    void* cmdPtr = m_creationProducer->m_creationCmdAllocator->alloc(size, 8);
    m_creationProducer->m_creationCmds.push_back((DriverCommand*)cmdPtr);
    return cmdPtr;
}

void ResourceCreationManager::commitCommand()
{
    m_creationMutex.unlock();
}

void ResourceCreationManager::execute()
{
    LockGuard lock(m_executionMutex);

    swap();

    for(auto cmd : m_creationConsumer->m_creationCmds)
    {
        cmd->execute();
        cmd->~DriverCommand();
    }

    m_creationConsumer->m_creationCmds.clear();
    m_creationConsumer->m_creationCmdAllocator->reset();
}

void ResourceCreationManager::swap()
{
    LockGuard lock(m_creationMutex);

    std::swap(m_creationProducer, m_creationConsumer);
}

} // namespace OE_Graphic

#ifndef __OE_GRAPHIC_DRIVER_COMMAND_H__
#define __OE_GRAPHIC_DRIVER_COMMAND_H__

#include "oe_graphic.h"

// External
#include <functional>

// Core
#include <buffer.h>
#include <std_smartptr_helpers.h>
#include <vector.h>

// OE_Graphic
#include "instance_binding.h"


namespace OE_Graphic
{

class ResourceCreationManager;

struct OE_GRAPHIC_EXPORT DriverCommand
{
    virtual ~DriverCommand(){}
    virtual void execute() = 0;
    virtual size_t getSize() const = 0;
};

struct OE_GRAPHIC_EXPORT NullCommand : public DriverCommand
{
    virtual ~NullCommand(){}
    virtual void execute();
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }
};

struct OE_GRAPHIC_EXPORT CreateVertexBufferCommand : public DriverCommand
{
    NO_COPY_DEFAULT_CONSTRUCTOR(CreateVertexBufferCommand)
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    CreateVertexBufferParams m_params;
};

struct OE_GRAPHIC_EXPORT CreateIndexBufferCommand : public DriverCommand
{
    NO_COPY_DEFAULT_CONSTRUCTOR(CreateIndexBufferCommand)
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    CreateIndexBufferParams m_params;
};

struct OE_GRAPHIC_EXPORT CreateTextureCommand : public DriverCommand
{
    NO_COPY_DEFAULT_CONSTRUCTOR(CreateTextureCommand);
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    CreateTextureParams m_params;
};

struct OE_GRAPHIC_EXPORT CreateRenderPassCommand : public DriverCommand
{
    NO_COPY_DEFAULT_CONSTRUCTOR(CreateRenderPassCommand);
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    CreateRenderPassParams m_params;
};

struct OE_GRAPHIC_EXPORT UpdateVertexBufferCommand : public DriverCommand
{
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    VertexBufferHandle m_handle;
    OE_Core::UniqueBufferPtr m_vertices;
};

struct OE_GRAPHIC_EXPORT UpdateIndexBufferCommand : public DriverCommand
{
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    IndexBufferHandle m_handle;
    OE_Core::UniqueBufferPtr m_indices;
};

struct OE_GRAPHIC_EXPORT ExecuteCommand : public DriverCommand
{
    virtual void execute() override;
    virtual size_t getSize() const override
    {
        return sizeof(*this);
    }

    std::function<void()> m_func;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_DRIVER_COMMAND_H__


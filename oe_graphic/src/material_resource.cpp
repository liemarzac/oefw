#include "material_resource.h"

#include "graphic_private.h"

// OE_Core
#include <file.h>
#include <resource_manager.h>
#include <task_manager.h>

// OE_Graphic
#include "pixel_shader_resource.h"
#include "render_pass_resource.h"
#include "render_target_resource.h"
#include "renderer.h"
#include "shader_binding_resource.h"
#include "texture_resource.h"
#include "ubo_factory.h"
#include "vertex_shader_resource.h"
#include "view_resource.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(MaterialResource, Resource)

MaterialResource::MaterialResource(const String& name) :
    Resource(name)
{
    m_descriptor = MakeShared<Descriptor>();
}

MaterialResource::MaterialResource(
    const String& name,
    SharedPtr<Descriptor> descriptor):
        Resource(name)
        ,m_descriptor(descriptor)
{
    OE_CHECK(descriptor->m_vsPath.size() > 0, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_psPath.size() > 0, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_renderPassPath.size() > 0, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_shaderBindingLayoutPath.size() > 0, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_viewResourcePath.size() > 0, OE_CHANNEL_RESOURCE);

    m_loadingState = LoadingState::DescriptorReady;
}

MaterialResource::~MaterialResource()
{
    if(m_ubo)
    {
        delete m_ubo;
    }
}


TextureResPtr MaterialResource::getTexture(uint32 id) const
{
    uint32 iTextureBind = 0;
    for(auto& sampler : m_samplers)
    {
        if(sampler.m_texture)
        {
            if(iTextureBind == id)
            {
                return sampler.m_texture;
            }
            else
            {
                ++iTextureBind;
            }
        }
    }

    return TextureResPtr();
}


void MaterialResource::loadImpl()
{
    if(m_loadingState == LoadingState::WaitingForDescriptor)
    {
        String resourcePath = getName() + ".json";
        m_loadTaskPtr = MakeShared<MaterialResourceLoadTask>(resourcePath, m_descriptor);
        TaskManager::getInstance().addTask(m_loadTaskPtr);
    }
}

void MaterialResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    auto& renderer = Renderer::getInstance();

    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
        if(m_loadTaskPtr->isFinished())
        {
            m_loadingState = LoadingState::DescriptorReady;
        }
        else
        {
            break;
        }

    case LoadingState::DescriptorReady:
        loadDependencies(dependencyCallback);
        m_loadingState = LoadingState::WaitingForTextures;

    case LoadingState::WaitingForTextures:
        if(!updatePendingSamplers())
        {
            m_loadingState = LoadingState::WaitingForShaders;
        }
        else
        {
            break;
        }

    case LoadingState::WaitingForShaders:
        if(m_vertexShaderResource->isLoaded() && m_pixelShaderResource->isLoaded())
        {
            m_loadingState = LoadingState::WaitingForShaderBinding;
        }
        else
        {
            break;
        }

    case LoadingState::WaitingForShaderBinding:
        if(m_shaderBindingResource->isLoaded())
        {
            m_loadingState = LoadingState::WaitingForRenderPass;
        }
        else
        {
            break;
        }

    case LoadingState::WaitingForRenderPass:
        if(m_renderPassResource->isLoaded())
        {
            m_renderPassId.m_handle = m_renderPassResource->getHandle();
            m_renderPassId.m_iSubpass = m_descriptor->m_iSubpass;
            m_loadingState = LoadingState::WaitingForView;
        }
        else
        {
            break;
        }

    case LoadingState::WaitingForView:
    {
        if(!m_viewResource || m_viewResource->isLoaded())
        {
            m_loadingState = LoadingState::CreatePipeline;
        }
        else
        {
            break;
        }
    }
 
    case LoadingState::CreatePipeline:
        CreatePipelineParams params;
        params.m_vsHandle = m_vertexShaderResource->getHandle();
        params.m_psHandle = m_pixelShaderResource->getHandle(),
        params.m_shaderBindingHandle = m_shaderBindingResource->getHandle();
        params.m_renderPassHandle = m_renderPassResource->getHandle();
        if(m_viewResource)
        {
            params.m_viewHandle = m_viewResource->getHandle();
        }
        params.m_iSubpass = m_descriptor->m_iSubpass;
        params.m_vertexShaderInputAttribs = m_descriptor->m_inputs;
        params.m_shaderBindings = m_shaderBindings;
        params.m_topology = m_descriptor->m_topology;
        if(m_ubo)
        {
            params.m_uboSize = m_ubo->getSize();
        }
        params.m_bAlphaBlendEnabled = m_descriptor->m_renderStates.m_alphaBlendEnable;
        m_pipelineHandle = renderer.createPipeline(
            params
            DBG_PARAM_ADD(getName()));

        m_loadingState = LoadingState::Loaded;
        setState(State::Loaded);
    }
}

void MaterialResource::loadDependencies(DependencyCallback dependencyCallback)
{
    m_vertexShaderResource = addDependency<VertexShaderResource>(dependencyCallback, m_descriptor->m_vsPath);
    m_pixelShaderResource = addDependency<PixelShaderResource>(dependencyCallback, m_descriptor->m_psPath);
    m_shaderBindingResource = addDependency<ShaderBindingResource>(dependencyCallback, m_descriptor->m_shaderBindingLayoutPath);
    m_renderPassResource = addDependency<RenderPassResource>(dependencyCallback, m_descriptor->m_renderPassPath);

    if(!m_descriptor->m_viewResourcePath.empty())
    {
        m_viewResource = addDependency<ViewResource>(dependencyCallback, m_descriptor->m_viewResourcePath);
    }

    for(auto& descShaderBinding : m_descriptor->m_shaderBindings)
    {
        ShaderBind shaderBinding;
        shaderBinding.m_bindId = descShaderBinding.m_bindId;
        shaderBinding.m_bindType = descShaderBinding.m_bindingType;

        switch(descShaderBinding.m_bindingType)
        {
        case BindingType::Uniform:
        {
            m_ubo = Renderer::getInstance().getUBOFactory()->createUBO(descShaderBinding.m_uboClass);
        }
        break;

        case BindingType::Sampler:
        case BindingType::InputAttachment:
        {
            shaderBinding.m_samplerType = descShaderBinding.m_samplerType;
            shaderBinding.m_bindId = descShaderBinding.m_bindId;

            Sampler sampler;
            sampler.m_type = descShaderBinding.m_samplerType;
            sampler.m_bindId = descShaderBinding.m_bindId;

            OE_CHECK(descShaderBinding.m_samplerPath.length() > 0, OE_CHANNEL_RESOURCE);
            switch(descShaderBinding.m_samplerType)
            {
            case SamplerType::Texture2D:
            sampler.m_texture = addDependency<TextureResource>(dependencyCallback, descShaderBinding.m_samplerPath);
            break;

            case SamplerType::RenderTarget:
            sampler.m_renderTarget = addDependency<RenderTargetResource>(dependencyCallback, descShaderBinding.m_samplerPath);
            break;

            default:
            OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown sampler type");
            }

            // The handle will be set when the sampler resource is loaded in updatePendingSamplers.

            m_samplers.push_back(sampler);
        }
        break;

        default:
        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown binding type");
        }

        m_shaderBindings.push_back(shaderBinding);
    }
}

bool MaterialResource::updatePendingSamplers()
{
    // Assume they are all loaded.
    bool bAllLoaded = true;

    for(size_t iSampler = 0; iSampler < m_samplers.size(); ++iSampler)
    {
        auto& sampler = m_samplers[iSampler];

        // Find shader binding with the same bind id as the sampler.
        ShaderBind* matchingShaderBind = nullptr;
        for(auto& shaderBind : m_shaderBindings)
        {
            if(shaderBind.m_bindId == sampler.m_bindId)
            {
                matchingShaderBind = &shaderBind;
                break;
            }
        }

        OE_CHECK(matchingShaderBind, OE_CHANNEL_RESOURCE);

        SharedPtr<Resource> resource;

        switch(sampler.m_type)
        {
        case SamplerType::Texture2D:
        resource = sampler.m_texture;
        break;

        case SamplerType::RenderTarget:
        resource = sampler.m_renderTarget;
        break;
        }

        OE_CHECK(resource, OE_CHANNEL_RESOURCE);

        if(resource->getState() == Resource::State::Loaded)
        {
            switch(sampler.m_type)
            {
            case SamplerType::Texture2D:
            if(!matchingShaderBind->m_textureHandle)
            {
                matchingShaderBind->m_textureHandle = sampler.m_texture->getHandle();
            }
            break;

            case SamplerType::RenderTarget:
            if(!matchingShaderBind->m_renderTargetHandle)
            {
                matchingShaderBind->m_renderTargetHandle = sampler.m_renderTarget->getHandle();
            }
            break;
            }
        }
        else
        {
            bAllLoaded = false;
        }
    }

#if _DEBUG
    if(bAllLoaded)
    {
        // Check that all shader bindings of type sampler have a texture or render target handle.
        for(auto& shaderBinding : m_shaderBindings)
        {
            if(shaderBinding.m_bindType == BindingType::Sampler)
            {
                OE_CHECK(shaderBinding.m_textureHandle || shaderBinding.m_renderTargetHandle, OE_CHANNEL_RESOURCE);
            }
        }
    }
#endif

    return !bAllLoaded;
}

void MaterialResource::stage()
{
    if(m_pipelineHandle)
    {
        OE_CHECK(m_ubo, OE_CHANNEL_RESOURCE);
        const size_t uboSize = m_ubo->getSize();
        const void* uboData = m_ubo->getData();
        UniqueBufferPtr buffer = MakeUnique<Buffer>(uboSize);
        buffer->write(uboData, uboSize);
        Renderer::getInstance().updateMaterial(m_pipelineHandle, std::move(buffer));
    }
}


MaterialResourceLoadTask::MaterialResourceLoadTask(
    const String& resourcePath,
    SharedPtr<MaterialResource::Descriptor> descriptor):
        m_resourcePath(resourcePath)
        ,m_descriptor(descriptor)
{
}

void MaterialResourceLoadTask::run()
{
    Json::Value jsonContent;

    OE_CHECK_RESULT(
        appLoadJsonFile(m_resourcePath.c_str(), jsonContent),
        true,
        OE_CHANNEL_RESOURCE);

    // Shaders.
    {
        Json::Value jsonShader = jsonContent["vertex_shader"];
        OE_CHECK(jsonShader.isString(), OE_CHANNEL_RESOURCE);
        {
            m_descriptor->m_vsPath = jsonShader.asString();
        }
    }
    {
        Json::Value jsonShader = jsonContent["pixel_shader"];
        OE_CHECK(jsonShader.isString(), OE_CHANNEL_RESOURCE);
        {
            m_descriptor->m_psPath = jsonShader.asString();
        }
    }
    {
        Json::Value jsonShader = jsonContent["shader_binding"];
        OE_CHECK(jsonShader.isString(), OE_CHANNEL_RESOURCE);
        {
            m_descriptor->m_shaderBindingLayoutPath = jsonShader.asString();
        }
    }

    // Topology.
    {
        Json::Value jsonTopology = jsonContent["topology"];
        if(jsonTopology.isString())
        {
            const String strTopology = jsonTopology.asString();
            if(strTopology == "triangle_list")
            {
                m_descriptor->m_topology = PrimitiveType::TriangleList;
            }
            else if(strTopology == "line_list")
            {
                m_descriptor->m_topology = PrimitiveType::LineList;
            }
            else
            {
                OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown topology: %s", strTopology.c_str());
            }
        }
    }

    // Render States.
    {
        Json::Value& renderStates = jsonContent["render_states"];
        if(renderStates.isObject())
        {
            {
                Json::Value jsonValue = renderStates["z_enable"];
                if(jsonValue.isBool())
                {
                    m_descriptor->m_renderStates.m_zEnable = jsonValue.asBool();
                }
            }
            {
                Json::Value jsonValue = renderStates["z_write_enable"];
                if(jsonValue.isBool())
                {
                    m_descriptor->m_renderStates.m_zWriteEnable = jsonValue.asBool();
                }
            }
            {
                Json::Value jsonValue = renderStates["alpha_blend_enable"];
                if(jsonValue.isBool())
                {
                    m_descriptor->m_renderStates.m_alphaBlendEnable = jsonValue.asBool();
                }
            }
        }
    }

    // Inputs.
    {
        Json::Value& jsonInputs = jsonContent["inputs"];
        OE_CHECK(jsonInputs.isString(), OE_CHANNEL_RESOURCE);
        String strInputs = jsonInputs.asString();
        strVertexDeclarationToInputs(strInputs, m_descriptor->m_inputs);
    }

    // Bindings.
    {
        Json::Value& jsonBindings = jsonContent["bindings"];
        if(jsonBindings.isArray())
        {
            Json::ValueIterator jsonItr = jsonBindings.begin();
            for(; jsonItr != jsonBindings.end(); ++jsonItr)
            {
                MaterialResource::Descriptor::ShaderBinding shaderBinding;

                OE_CHECK((*jsonItr).isObject(), OE_CHANNEL_RESOURCE);

                Json::Value jsonId = (*jsonItr)["id"];
                if(jsonId.isUInt())
                {
                    shaderBinding.m_bindId = jsonId.asUInt();
                }
                else
                {
                    Json::Value jsonType = (*jsonItr)["type"];
                    OE_CHECK(jsonType.isString(), OE_CHANNEL_RESOURCE);

                    String strType = jsonType.asString();
                    if(strType == "diffuse")
                    {
                        shaderBinding.m_bindId = 0;
                    }
                    else if(strType == "emissive")
                    {
                        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Emissive currently unsupported");
                    }
                    else if(strType == "specular")
                    {
                        shaderBinding.m_bindId = 2;
                    }
                    else if(strType == "normals")
                    {
                        shaderBinding.m_bindId = 1;
                    }
                    else if(strType == "opacity")
                    {
                        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Opacity currently unsupported");
                    }
                    else
                    {
                        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown texture type: %s", strType.c_str());
                    }
                }

                Json::Value jsonTexture = (*jsonItr)["texture"];
                if(jsonTexture.isString())
                {
                    shaderBinding.m_bindingType = BindingType::Sampler;
                    shaderBinding.m_samplerPath = jsonTexture.asString();
                    shaderBinding.m_samplerType = SamplerType::Texture2D;
                }

                Json::Value jsonRenderTarget = (*jsonItr)["render_target"];
                if(jsonRenderTarget.isString())
                {
                    shaderBinding.m_bindingType = BindingType::Sampler;
                    shaderBinding.m_samplerPath = jsonRenderTarget.asString();
                    shaderBinding.m_samplerType = SamplerType::RenderTarget;
                }

                Json::Value jsonInputAttachment = (*jsonItr)["input_attachment"];
                if (jsonInputAttachment.isString())
                {
                    shaderBinding.m_bindingType = BindingType::InputAttachment;
                    shaderBinding.m_samplerPath = jsonInputAttachment.asString();
                    shaderBinding.m_samplerType = SamplerType::RenderTarget;
                }

                Json::Value jsonUBOClass = (*jsonItr)["ubo_class"];
                if(jsonUBOClass.isString())
                {
                    shaderBinding.m_bindingType = BindingType::Uniform;
                    shaderBinding.m_uboClass = jsonUBOClass.asString();
                }

                OE_CHECK(shaderBinding.m_bindingType != BindingType::Unknown, OE_CHANNEL_RESOURCE);

                m_descriptor->m_shaderBindings.push_back(shaderBinding);
            }
        }
    }

    // Render pass.
    {
        Json::Value& jsonRenderPass = jsonContent["render_pass"];
        OE_CHECK(jsonRenderPass.isObject(), OE_CHANNEL_RESOURCE);
        
        Json::Value& jsonPath = jsonRenderPass["path"];
        OE_CHECK(jsonPath.isString(), OE_CHANNEL_RESOURCE);
        m_descriptor->m_renderPassPath = jsonPath.asString();

        Json::Value& jsonSubpass = jsonRenderPass["subpass"];
        OE_CHECK(jsonSubpass.isInt(), OE_CHANNEL_RESOURCE);
        m_descriptor->m_iSubpass = jsonSubpass.asInt();
    }

    // View resource.
    {
        const Json::Value& jsonView = jsonContent["view"];
        //OE_CHECK(jsonView.isString(), OE_CHANNEL_RESOURCE);
        if(jsonView.isString())
        {
            m_descriptor->m_viewResourcePath = jsonView.asString();
        }
    }
}

} // namespace OE_Graphic

#ifndef __OE_GRAPHIC_TEXTURE_RESOURCE_H__
#define __OE_GRAPHIC_TEXTURE_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>
#include <std_mutex_helpers.h>
#include <resource.h>
#include <runnable.h>

// OE_Grpahic
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT TextureResource : public OE_Core::Resource
{
    friend class TextureRawResourceLoadTask;
    friend class TexturePNGResourceLoadTask;
    RTTI_DECLARATION
public:
    enum class Type
    {
        Raw,
        PNG,
        JPEG,
        DDS
    };

    enum class Format
    {
        A8R8G8B8,
        A8
    };

    struct Descriptor
    {
        CreateTextureParams m_params;
    };

    TextureResource(const String& name);
    virtual ~TextureResource();

    virtual void loadImpl() override;

    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline TextureHandle getHandle() const
    {
        return m_handle;
    }

    inline uint32 getWidth() const
    {
        return m_width;
    }

    inline uint32 getHeight() const
    {
        return m_height;
    }

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "texture_resource";
    }

private:
    TextureResource();
    TextureResource& operator=(const TextureResource& rhs);
    TextureResource(TextureResource& rhs);

    TextureHandle m_handle;
    Type m_type;
    Format m_format;
    Mutex m_handleMutex;
    uint32 m_width;
    uint32 m_height;

    SharedPtr<OE_Core::Runnable> m_loadTask;
    SharedPtr<Descriptor> m_descriptor;

    enum class LoadingState
    {
        WaitingForDescriptor,
        WaitingForHandle,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;

};


class TextureResourceLoadTask : public OE_Core::Runnable
{
public:
    TextureResourceLoadTask(
        const String& resourcePath,
        SharedPtr<TextureResource::Descriptor> descriptor):
            m_resourcePath(resourcePath)
            ,m_descriptor(descriptor)
    {
    }

protected:
    SharedPtr<TextureResource::Descriptor> m_descriptor;
    String m_resourcePath;
};


class TextureRawResourceLoadTask : public TextureResourceLoadTask
{
public:
    TextureRawResourceLoadTask(
        const String& resourcePath,
        SharedPtr<TextureResource::Descriptor> descriptor);

    virtual void run() override;

private:
    TextureRawResourceLoadTask();
    TextureRawResourceLoadTask& operator=(const TextureRawResourceLoadTask& rhs);
    TextureRawResourceLoadTask(TextureRawResourceLoadTask& rhs);
};


class TexturePNGResourceLoadTask : public TextureResourceLoadTask
{
public:
    TexturePNGResourceLoadTask(
        const String& resourcePath,
        SharedPtr<TextureResource::Descriptor> descriptor);

    virtual void run() override;

private:
    TexturePNGResourceLoadTask();
    TexturePNGResourceLoadTask& operator=(const TexturePNGResourceLoadTask& rhs);
    TexturePNGResourceLoadTask(TexturePNGResourceLoadTask& rhs);
};


class TextureDDSResourceLoadTask : public TextureResourceLoadTask
{
public:
    TextureDDSResourceLoadTask() = delete;
    TextureDDSResourceLoadTask(
        const String& resourcePath,
        SharedPtr<TextureResource::Descriptor> descriptor);
    NO_COPY(TextureDDSResourceLoadTask);

    virtual void run() override;
};

class TextureJPEGResourceLoadTask : public TextureResourceLoadTask
{
public:
    TextureJPEGResourceLoadTask(
        const String& resourcePath,
        SharedPtr<TextureResource::Descriptor> descriptor);

    virtual void run() override;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_TEXTURE_RESOURCE_H__

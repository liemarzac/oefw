#ifndef __OE_GRAPHIC_UBO_FACTORY_H__
#define __OE_GRAPHIC_UBO_FACTORY_H__

#include "oe_graphic.h"

// OE_Graphic
#include "ubo.h"

// OE_Core
#include <factory.h>


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT UBOFactory
{
public:
    UBOFactory();
    virtual ~UBOFactory();

    UBOFactory(const UBOFactory& rhs) = delete;
    UBOFactory& operator=(const UBOFactory& rhs) = delete;

    OE_Core::Factory<UBO*, void*> m_factory;

    template<typename UBOType>
    void registerUBOClass()
    {
        m_factory.addProduction(UBOType::getTypeHash(), [](void*) {return new UBOType(); });
    }

    template<typename UBOType>
    UBO* createUBO()
    {
        return m_factory.makeProduct(UBOType::getTypeHash(), nullptr);
    }

    UBO* createUBO(const String& className);
};

} // OE_Graphic

#endif // __OE_GRAPHIC_UBO_FACTORY_H__

#ifndef __OE_GRAPHIC_PROP_RESOURCE_H__
#define __OE_GRAPHIC_PROP_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>
#include <resource.h>
#include <runnable.h>

// OE_Graphic
#include "graphic_resource_helpers.h"


struct aiScene;

namespace OE_Graphic
{

class MeshResource;

class OE_GRAPHIC_EXPORT PropResource : public OE_Core::Resource
{
    RTTI_DECLARATION
public:
    NO_COPY(PropResource);

    struct Descriptor
    {
        Vector<MeshResPtr> m_meshResources;
    };

    PropResource(const String& path);
    PropResource(const String& name, SharedPtr<Descriptor> descriptor);
    virtual ~PropResource();

    virtual const char* getType() const override;
    static const char* getTypeStatic();

    const Vector<MeshResPtr>& getMeshResources() const;

private:
    enum class InternalLoadState
    {
        Unloaded,
        WaitForLoadTask,
        Loaded
    }m_internalLoadState = InternalLoadState::Unloaded;

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    SharedPtr<Descriptor> m_descriptor;
    SharedPtr<class PropResourceLoadTask> m_loadTask;
};

class PropResourceLoadTask : public OE_Core::Runnable
{
public:
    struct TextureContent
    {
        String m_path;
        TextureBindingType::Enum m_type;
    };

    struct MaterialContent
    {
        Vector<TextureContent> m_textures;
    };

    struct PerMaterial
    {
        MaterialContent m_material;
        Vector<VertexStreamResPtr> m_vertexStreams;
    };

    NO_COPY(PropResourceLoadTask);

    PropResourceLoadTask() = delete;
    PropResourceLoadTask(const String& resourcePath, SharedPtr<PropResource::Descriptor> descriptor);
    virtual ~PropResourceLoadTask();

    virtual void run() override;

private:
    void loadAssimpScene();
    void createResources();

    String m_resourcePath;
    Vector<PerMaterial> m_perMaterials;
    SharedPtr<PropResource::Descriptor> m_descriptor;
    const aiScene* m_scene = nullptr;
};

} // OE_Graphic

#endif // __OE_GRAPHIC_PROP_RESOURCE_H__

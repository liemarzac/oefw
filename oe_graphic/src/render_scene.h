#ifndef __OE_GRAPHIC_RENDER_SCENE_H__
#define __OE_GRAPHIC_RENDER_SCENE_H__

#include "oe_graphic.h"

// OE_Core
#include <array.h>
#include <map.h>
#include <ring_buffer.h>

// OE_Graphic
#include "mesh_instance.h"
#include "view_resource.h"

#define OE_CHANNEL_RENDER_SCENE "RenderScene"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT RenderScene
{
public:
    EIGEN_ALIGNED

    struct PerMaterial
    {
        PipelineHandle m_pipelineHandle;
        Vector<MeshInstancePtr> m_meshInstances;
    };

    struct PerRenderPass
    {
        RenderPassId m_id;
        Map<uintptr_t, PerMaterial> m_materials;
    };

    using RenderPassMap = Map<RenderPassId, PerRenderPass>;

    RenderScene();
    ~RenderScene();

    // Copy is used when flipping the main and render thread RenderScene.
    // Data set by the main thread needs to be know to the RenderScene rendered.
    void copyFrom(const RenderScene& rhs);

    inline void setDominantDirectionalLight(const Vec3& direction);
    inline bool getDominantDirectionalLight(Vec3& lightDir) const;
    inline void removeDominantDirectionalLight();
    inline void setViewMatrix(const Mat4& viewMatrix);
    inline const Mat4& getViewMatrix() const;
    inline void setProjectionMatrix(const Mat4& projection);
    void addMeshInstance(MeshInstancePtr meshInstance);
    void removeMeshInstance(MeshInstancePtr meshInstance);
    void generateDriverCommands(const RenderPassId& renderPassId);
    void clear();
    DBG_ONLY(void setName(const String& name));

    void startRendering();
    void stopRendering();

    template<typename T>
    inline T* allocCommand(size_t extraSize = 0);
    void commitCommand();

    bool m_bRendering;
private:
    RenderScene(const RenderScene& rhs);
    RenderScene& operator=(const RenderScene& rhs);

    OE_Core::RingBuffer* m_renderCommands = nullptr;

    RenderPassMap m_renderPassMap;
    Mutex m_meshInstanceCollectionMutex;
    Mat4 m_viewMatrix;
    Mat4 m_projectionMatrix;
    uint32 m_id = InvalidIndex;
    bool m_hasDirectionalDominantLight;
    DBG_ONLY(String m_name);
};

void RenderScene::setDominantDirectionalLight(const Vec3& direction)
{
    //auto defaultViewResource = getDefaultViewResource();
    //if(defaultViewResource)
    //{
    //    ViewResource::UBO& ubo = defaultViewResource->getUBO();
    //    ubo.m_lightDir = direction;
    //    m_hasDirectionalDominantLight = true;
    //}
}

void RenderScene::removeDominantDirectionalLight()
{
    m_hasDirectionalDominantLight = false;
}

bool RenderScene::getDominantDirectionalLight(Vec3& lightDir) const
{
    //auto defaultViewResource = getDefaultViewResource();
    //if(defaultViewResource)
    //{
    //    ViewResource::UBO& ubo = defaultViewResource->getUBO();
    //    lightDir = ubo.m_lightDir;
    //    return true;
    //}

    return false;
}

void RenderScene::setViewMatrix(const Mat4& view)
{
    m_viewMatrix = view;
}

const Mat4& RenderScene::getViewMatrix() const
{
    return m_viewMatrix;
}

void RenderScene::setProjectionMatrix(const Mat4& projection)
{
    m_projectionMatrix = projection;
}

template<typename T>
T* RenderScene::allocCommand(size_t extraSize)
{
    void* ptr = nullptr;
    m_renderCommands->alloc(ptr, sizeof(T) + extraSize);
    OE_CHECK(ptr != nullptr, OE_CHANNEL_RENDER_SCENE);
    return new(ptr)T();
}

}

#endif // __OE_GRAPHIC_RENDER_SCENE_H__

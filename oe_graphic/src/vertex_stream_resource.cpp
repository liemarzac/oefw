#include "vertex_stream_resource.h"

#include "graphic_private.h"

// OE_Core
#include <file.h>
#include <task_manager.h>

// OE_Graphic
#include "renderer.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(VertexStreamResource, Resource)

VertexStreamResource::VertexStreamResource(const String& name):
    Resource(name)
{
    m_internalLoadState = InternalLoadState::LoadFromFile;
}

VertexStreamResource::VertexStreamResource(
    UniquePtr<Buffer> vertexBuffer,
    VertexDeclaration vertexDeclaration,
    PrimitiveType primitiveType,
    BufferUsage bufferUsage,
    const String& name) :
    Resource(name)
{
    auto& renderer = Renderer::getInstance();

    CreateVertexBufferParams params;
    params.m_buffer = std::move(vertexBuffer);
    params.m_format = vertexDeclaration;
    params.m_usage = bufferUsage;
    DBG_ONLY(params.m_name = getName());

    m_vertexBufferHandle = renderer.createVertexBuffer(params);

    m_vertexDeclaration = vertexDeclaration;
    m_topology = primitiveType;
    m_usage = bufferUsage;

    m_internalLoadState = InternalLoadState::WaitForHandlers;
}

VertexStreamResource::VertexStreamResource(
    UniqueBufferPtr vertexBuffer,
    UniqueBufferPtr indexBuffer,
    IndexType indexType,
    VertexDeclaration vertexDeclaration,
    PrimitiveType primitiveType,
    BufferUsage bufferUsage,
    const String& name):
        Resource(name)
{
    auto& renderer = Renderer::getInstance();

    OE_CHECK(vertexBuffer->m_size > 0, OE_CHANNEL_RESOURCE);
    CreateVertexBufferParams vbCreateParams;
    vbCreateParams.m_buffer = std::move(vertexBuffer);
    vbCreateParams.m_format = vertexDeclaration;
    vbCreateParams.m_usage = bufferUsage;
    DBG_ONLY(vbCreateParams.m_name = getName());
    m_vertexBufferHandle = renderer.createVertexBuffer(vbCreateParams);

    OE_CHECK(indexBuffer->m_size > 0, OE_CHANNEL_RESOURCE);
    CreateIndexBufferParams ibCreateParams;
    ibCreateParams.m_buffer = std::move(indexBuffer);
    ibCreateParams.m_indexType = indexType;
    ibCreateParams.m_usage = bufferUsage;
    DBG_ONLY(ibCreateParams.m_name = getName());
    m_indexBufferHandle = renderer.createIndexBuffer(ibCreateParams);

    m_vertexDeclaration = vertexDeclaration;
    m_topology = primitiveType;
    m_usage = bufferUsage;

    m_internalLoadState = InternalLoadState::WaitForHandlers;
}

VertexStreamResource::~VertexStreamResource()
{
}

void VertexStreamResource::loadImpl()
{
    if(m_internalLoadState == InternalLoadState::LoadFromFile)
    {
        m_loadTask = MakeShared<VertexStreamResourceLoadTask>(std::static_pointer_cast<VertexStreamResource>(shared_from_this()));
        TaskManager::getInstance().addTask(m_loadTask);
        m_internalLoadState = InternalLoadState::WaitForLoadTask;
    }
}

void VertexStreamResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_internalLoadState)
    {
    case InternalLoadState::WaitForLoadTask:
    {
        if(m_loadTask->isFinished())
        {
            m_loadTask.reset();
            m_internalLoadState = InternalLoadState::WaitForHandlers;
        }
        else
        {
            break;
        }
    }

    case InternalLoadState::WaitForHandlers:
    {
        if(!m_vertexBufferHandle.isReady())
            break;

        if(m_indexBufferHandle && !m_indexBufferHandle.isReady())
            break;

        m_internalLoadState = InternalLoadState::Loaded;
    }

    case InternalLoadState::Loaded:
    {
        setState(State::Loaded);
    }
    }
}

void VertexStreamResource::update(
    UniqueBufferPtr vertices,
    UniqueBufferPtr indicies)
{
    auto& renderer = Renderer::getInstance();
    renderer.updateVertexBuffer(m_vertexBufferHandle, std::move(vertices));

    if(m_indexBufferHandle)
    {
        renderer.updateIndexBuffer(m_indexBufferHandle, std::move(indicies));
    }
}

VertexStreamResourceLoadTask::VertexStreamResourceLoadTask(SharedPtr<VertexStreamResource> resource)
{
    m_resource = resource;
}

void VertexStreamResourceLoadTask::run()
{
    // Open file.
    String fileName("meshes/" OE_PLATFORM_STR "/");
    fileName += m_resource->getName();
    fileName += ".vstream";

    std::ifstream stream;
    appGetResourceStream(fileName, stream, std::ios::binary);

    if(!stream.is_open())
    {
        OE_CHECK_MSG(
            false,
            OE_CHANNEL_RESOURCE,
            "Cannot open resource %s", fileName.c_str());
        return;
    }

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Copy file to buffer.
    Buffer fileBuffer(fileSize);
    stream.read((char*)fileBuffer.m_rawData, fileSize);
    stream.close();

    // Read vertex format.
    uint8 vertexFormat = 0;
    fileBuffer.read(vertexFormat);

    // Read number of vertices.
    uint32 numVertices;
    fileBuffer.read(numVertices);

    UniquePtr<Buffer> vertices;
    switch((VertexDeclaration)vertexFormat)
    {
        case VertexDeclaration::XYZ_UV_Normal:
            vertices = UniquePtr<Buffer>(
                new Buffer(numVertices * sizeof(Vertex_XYZ_UV_Normal)));
            break;

        default:
            OE_CHECK_MSG(false, 
                OE_CHANNEL_RESOURCE,
                "Unknown vertex format %u", (uint)vertexFormat);
    }

    fileBuffer.read(vertices->m_rawData, vertices->m_size);

#if !_RELEASE
    // Cache normals.
    if((VertexDeclaration)vertexFormat == VertexDeclaration::XYZ_UV_Normal)
    {
        size_t readSize = 0;
        const size_t normalsBufferSize = 
            (vertices->m_size / sizeof(Vertex_XYZ_UV_Normal)) * sizeof(float) * 6;
        m_resource->m_normals.alloc(normalsBufferSize);
        while(!vertices->isEndOfBuffer())
        {
            float x1, y1, z1;
            vertices->read(x1);
            vertices->read(y1);
            vertices->read(z1);

            // discard uvs.
            vertices->skip(2 * sizeof(float));

            float nx, ny, nz;
            vertices->read(nx);
            vertices->read(ny);
            vertices->read(nz);
            OE_CHECK_MSG(
                fabs(1.0 - (nx * nx + ny * ny + nz * nz)) < sqrtf(0.001f),
                OE_CHANNEL_RESOURCE,
                "Normal length should be 1.0");

            float x2 = x1 + nx;
            float y2 = y1 + ny;
            float z2 = z1 + nz;

            m_resource->m_normals.write(x1);
            m_resource->m_normals.write(y1);
            m_resource->m_normals.write(z1);
            m_resource->m_normals.write(x2);
            m_resource->m_normals.write(y2);
            m_resource->m_normals.write(z2);
        }
        OE_CHECK(m_resource->m_normals.isEndOfBuffer(), OE_CHANNEL_RESOURCE);
        m_resource->m_normals.rewind();
        vertices->rewind();
    }
#endif // !_RELEASE

    // Read indices
    uint32 nIndices = 0;
    fileBuffer.read(nIndices);

    UniquePtr<Buffer> indices;
    IndexType indexType = IndexType::U16;
    if(nIndices > 0)
    {
        if(nIndices > 65536)
        {
            indices = UniquePtr<Buffer>(new Buffer(sizeof(uint32) * nIndices));
            indexType = IndexType::U32;
        }
        else
        {
            indices = UniquePtr<Buffer>(new Buffer(sizeof(uint16) * nIndices));
        }

        fileBuffer.read(indices->m_rawData, indices->m_size);
    }

    OE_CHECK(fileBuffer.isEndOfBuffer(), OE_CHANNEL_RESOURCE);

    m_resource->m_vertexDeclaration = VertexDeclaration::XYZ_UV_Normal;
    m_resource->m_topology = PrimitiveType::TriangleList;

    auto& renderer = Renderer::getInstance();
    CreateVertexBufferParams vbCreateParams;
    vbCreateParams.m_buffer = std::move(vertices);
    vbCreateParams.m_format = m_resource->m_vertexDeclaration;
    vbCreateParams.m_usage = BufferUsage::Static;
    DBG_ONLY(vbCreateParams.m_name = m_resource->getName());
    m_resource->m_vertexBufferHandle = renderer.createVertexBuffer(vbCreateParams);

    if(nIndices > 0)
    {
        CreateIndexBufferParams ibCreateParams;
        ibCreateParams.m_buffer = std::move(indices);
        ibCreateParams.m_indexType = indexType;
        ibCreateParams.m_usage = BufferUsage::Static;
        DBG_ONLY(ibCreateParams.m_name = m_resource->getName());
        m_resource->m_indexBufferHandle = renderer.createIndexBuffer(ibCreateParams);
    }

    m_resource->setState(Resource::State::Loaded);
}


} // namespace OE_Graphic

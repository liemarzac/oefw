#include "mesh_resource.h"

#include "graphic_private.h"

// External
#include <assimp/scene.h>
#include <assimp/cimport.h>
#include <assimp/postprocess.h>

// OE_Core
#include <task_manager.h>

// OE_Graphic
#include "material_resource.h"
#include "vertex_stream_resource.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(MeshResource, Resource)

MeshResource::MeshResource(const String& name) :
    Resource(name)
{
    OE_CHECK(false, OE_CHANNEL_RESOURCE);
}

MeshResource::MeshResource(
    const String& name,
    SharedPtr<Descriptor> descriptor) :
    Resource(name)
    ,m_descriptor(descriptor)
{
    OE_CHECK(descriptor, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_vertexStreamResource, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_materialResource, OE_CHANNEL_RESOURCE);
    OE_CHECK(descriptor->m_vertexStreamResource->getTopology() ==
        descriptor->m_materialResource->getTopology(), OE_CHANNEL_RESOURCE);
}

MeshResource::~MeshResource()
{
}

void MeshResource::loadImpl()
{
    m_internalLoadingState = InternalLoadingState::Loaded;
}

void MeshResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_internalLoadingState)
    {
    case InternalLoadingState::Loaded:
    {
        addDependency(dependencyCallback, m_descriptor->m_vertexStreamResource);
        addDependency(dependencyCallback, m_descriptor->m_materialResource);
        setState(State::Loaded);
    }
    }
}

void MeshResource::updateVertexStreamResource(
    UniqueBufferPtr vertices,
    UniqueBufferPtr indicies)
{
    OE_CHECK(m_descriptor, OE_CHANNEL_RESOURCE);
    OE_CHECK(m_descriptor->m_vertexStreamResource, OE_CHANNEL_RESOURCE);
    m_descriptor->m_vertexStreamResource->update(std::move(vertices), std::move(indicies));
}

#if !_RELEASE
MeshResourceGenerateNormalsTask::MeshResourceGenerateNormalsTask(MeshResPtr resource)
{
    m_resource = resource;
}

void MeshResourceGenerateNormalsTask::run()
{
    //SharedPtr<VertexStreamResource> vsResource =
    //    m_resource->m_vertexStreamResource;

    //while(!vsResource->isLoaded())
    //{
    //    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    //}

    //UniquePtr<Buffer> normalsBuffer(new Buffer(
    //        m_resource->m_vertexStreamResource->getNormals()));

    //char nameBuffer[512];
    //OE_SPRINTF(nameBuffer, 512, "%s_normals", vsResource->getName().c_str());

    //m_resource->m_normalResource = SharedPtr<VertexStreamResource>(
    //    new VertexStreamResource(
    //        std::move(normalsBuffer),
    //        VertexDeclaration::XYZ,
    //        PrimitiveType::LineList,
    //        BufferUsage::Static,
    //        nameBuffer));

    //m_resource->m_internalLoadingState = MeshResource::LoadingState::NormalsReady;
}
#endif // !_RELEASE

} // namespace OE_Graphic

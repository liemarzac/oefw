#ifndef __OE_GRAPHIC_RAW_TEXTURE_H__
#define __OE_GRAPHIC_RAW_TEXTURE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>
#include <serializable.h>


namespace OE_Graphic
{

struct OE_GRAPHIC_EXPORT RawTexture
{
	uint32 m_textureWidth;
	uint32 m_textureHeight;
    bool m_bAlpha;
	OE_Core::Buffer m_textureBuffer;
};

void OE_GRAPHIC_EXPORT serialize_impl(OE_Core::Buffer& buffer, const RawTexture& fontData, const RawTexture*);

void OE_GRAPHIC_EXPORT deserialize_impl(OE_Core::Buffer& buffer, RawTexture& fontData, const RawTexture*);

size_t OE_GRAPHIC_EXPORT calculateSerializedSize_impl(const RawTexture& fontData, const RawTexture*);

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_RAW_TEXTURE_H__

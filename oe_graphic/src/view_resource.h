#ifndef __OE_GRAPHIC_VIEW_RESOURCE_H__
#define __OE_GRAPHIC_VIEW_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <eigen_helpers.h>
#include <factory.h>
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Graphic
{

struct UBO;

class OE_GRAPHIC_EXPORT ViewResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    struct Descriptor
    {
        String m_uboClassName;
        OE_Core::FlagSet<ShaderType> m_stages;
    };

    ViewResource() = delete;
    ViewResource(const String& path);
    virtual ~ViewResource();

    virtual const char* getType() const override;
    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline ViewHandle getHandle() const
    {
        return m_handle;
    }

    void stage();

    template<typename ViewUBOType>
    ViewUBOType* getUBO() const
    {
        ViewUBOType* uboType = cast<ViewUBOType>(m_ubo);
        return uboType;
    }

private:
    SharedPtr<OE_Core::ResourceLoadTask<Descriptor>> m_loadTaskPtr;

    ViewHandle m_handle;
    UBO* m_ubo = nullptr;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VIEW_RESOURCE_H__

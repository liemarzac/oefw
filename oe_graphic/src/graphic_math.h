#ifndef __OE_GRAPHIC_MATH_H__
#define __OE_GRAPHIC_MATH_H__

// OE_Core
#include <eigen_helpers.h>

// OE_Graphic
#include "oe_graphic.h"

namespace OE_Graphic
{

void OE_GRAPHIC_EXPORT ortho(
    Mat4& ortho,
    float left,
    float right,
    float bottom,
    float top,
    float nearZ,
    float farZ);

}

#endif // __OE_GRAPHIC_MATH_H__

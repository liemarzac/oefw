#include "prop_resource.h"

#include "graphic_private.h"

// External
#include <assimp/scene.h>
#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <float.h>

// OE_Core
#include <file.h>
#include <oe_string.h>
#include <resource_manager.h>
#include <task_manager.h>

// OE_Graphic
#include "material_resource.h"
#include "mesh_resource.h"
#include "texture_resource.h"
#include "vertex_stream_resource.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(PropResource, Resource)

PropResource::PropResource(const String& path) :
    Resource(path)
{
    m_internalLoadState = InternalLoadState::WaitForLoadTask;
}

PropResource::PropResource(const String& name, SharedPtr<Descriptor> descriptor) :
    Resource(name)
{
    OE_CHECK(descriptor, OE_CHANNEL_RESOURCE);
    m_descriptor = descriptor;
    m_internalLoadState = InternalLoadState::Loaded;
}

PropResource::~PropResource()
{
}

const char* PropResource::getType() const
{
    return getTypeStatic();
}

const char* PropResource::getTypeStatic()
{
    return "prop_resource";
}

void PropResource::loadImpl()
{
    if(m_internalLoadState == InternalLoadState::WaitForLoadTask)
    {
        m_descriptor = MakeShared<Descriptor>();
        m_loadTask = MakeShared<PropResourceLoadTask>(
            getName(),
            m_descriptor);
        TaskManager::getInstance().addTask(m_loadTask);
    }
}

void PropResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_internalLoadState)
    {
    case InternalLoadState::WaitForLoadTask:
    {
        if(m_loadTask->isFinished())
        {
            m_loadTask.reset();
            m_internalLoadState = InternalLoadState::Loaded;
        }
        else
        {
            break;
        }
    }

    case InternalLoadState::Loaded:
    {
        for(auto mesh : m_descriptor->m_meshResources)
        {
            addDependency(dependencyCallback, mesh);
        }

        setState(State::Loaded);
    }
    }
}

const Vector<MeshResPtr>& PropResource::getMeshResources() const
{
    return m_descriptor->m_meshResources;
}

PropResourceLoadTask::PropResourceLoadTask(
    const String& resourcePath
    ,SharedPtr<PropResource::Descriptor> descriptor):
    m_resourcePath(resourcePath)
    ,m_descriptor(descriptor)
{
}

PropResourceLoadTask::~PropResourceLoadTask()
{
    if(m_scene)
    {
        aiReleaseImport(m_scene);
    }
}

void PropResourceLoadTask::run()
{
    loadAssimpScene();
    createResources();
}

void PropResourceLoadTask::loadAssimpScene()
{
    String fullyQualifiedPath;
    appGetFullyQualifiedResourcePath(fullyQualifiedPath, m_resourcePath);

    const int aiImportflags = aiProcess_FlipWindingOrder | aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals;

    m_scene = aiImportFile(
        fullyQualifiedPath.c_str(),
        aiImportflags);

    String resourceFolderPath;
    getFolderPath(resourceFolderPath, m_resourcePath);

    OE_CHECK(m_scene, OE_CHANNEL_RESOURCE);

    Vector<TextureResPtr> textures;

    // Populate material content..
    for(size_t iMaterial = 0; iMaterial < m_scene->mNumMaterials; iMaterial++)
    {
        m_perMaterials.push_back(PerMaterial());
        auto& perMaterial = m_perMaterials.back();

        const aiMaterial* material = m_scene->mMaterials[iMaterial];
        aiString aipath;
        TextureContent textureContent;

        if(material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            if(material->GetTexture(aiTextureType_DIFFUSE, 0, &aipath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                String texturePath(aipath.C_Str());

                // Remove leading slash if there is one.
                if (startWith(texturePath, "/"));
                {
                    right(texturePath, '/', StringPos::FirstOf);
                }

                textureContent.m_path = resourceFolderPath + PATH_SEPARATOR + texturePath;
                textureContent.m_type = TextureBindingType::Diffuse;
                perMaterial.m_material.m_textures.push_back(textureContent);
            }
        }

        if(material->GetTextureCount(aiTextureType_EMISSIVE) > 0)
        {
            if(material->GetTexture(aiTextureType_EMISSIVE, 0, &aipath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                String texturePath(aipath.C_Str());

                // Remove leading slash if there is one.
                if (startWith(texturePath, "/"));
                {
                    right(texturePath, '/', StringPos::FirstOf);
                }

                textureContent.m_path = resourceFolderPath + PATH_SEPARATOR + texturePath;
                textureContent.m_type = TextureBindingType::Emissive;
                perMaterial.m_material.m_textures.push_back(textureContent);
            }
        }

        if(material->GetTextureCount(aiTextureType_SPECULAR) > 0)
        {
            if(material->GetTexture(aiTextureType_SPECULAR, 0, &aipath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                String texturePath(aipath.C_Str());

                // Remove leading slash if there is one.
                if (startWith(texturePath, "/"));
                {
                    right(texturePath, '/', StringPos::FirstOf);
                }

                textureContent.m_path = resourceFolderPath + PATH_SEPARATOR + texturePath;
                textureContent.m_type = TextureBindingType::Specular;
                perMaterial.m_material.m_textures.push_back(textureContent);
            }
        }

        if(material->GetTextureCount(aiTextureType_NORMALS) > 0)
        {
            if(material->GetTexture(aiTextureType_NORMALS, 0, &aipath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                String texturePath(aipath.C_Str());

                // Remove leading slash if there is one.
                if (startWith(texturePath, "/"));
                {
                    right(texturePath, '/', StringPos::FirstOf);
                }

                textureContent.m_path = resourceFolderPath + PATH_SEPARATOR + texturePath;
                textureContent.m_type = TextureBindingType::Normals;
                perMaterial.m_material.m_textures.push_back(textureContent);
            }
        }

        if(material->GetTextureCount(aiTextureType_OPACITY) > 0)
        {
            if(material->GetTexture(aiTextureType_OPACITY, 0, &aipath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                String texturePath(aipath.C_Str());

                // Remove leading slash if there is one.
                if (startWith(texturePath, "/"));
                {
                    right(texturePath, '/', StringPos::FirstOf);
                }

                textureContent.m_path = resourceFolderPath + PATH_SEPARATOR + texturePath;
                textureContent.m_type = TextureBindingType::Opacity;
                perMaterial.m_material.m_textures.push_back(textureContent);
            }
        }
    }

    // Load meshes.
    Vector<VertexStreamResPtr> vertexStreamResources;

    for(size_t iMesh = 0; iMesh < m_scene->mNumMeshes; ++iMesh)
    {
        const aiMesh* mesh = m_scene->mMeshes[iMesh];
        uint32 iMaterial = mesh->mMaterialIndex;

        const size_t stride = sizeof(Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent);
        UniqueBufferPtr vertexBuffer(
            new Buffer(stride * mesh->mNumVertices));

        const bool bHasUVs = mesh->HasTextureCoords(0);
        const bool bHasColor = mesh->HasVertexColors(0);
        const bool bHasTangentBitangent = mesh->HasTangentsAndBitangents();

        float AABB[2][3];
        AABB[0][0] = FLT_MAX;
        AABB[0][1] = FLT_MAX;
        AABB[0][2] = FLT_MAX;
        AABB[1][0] = -FLT_MAX;
        AABB[1][1] = -FLT_MAX;
        AABB[1][2] = -FLT_MAX;

        for(size_t iVertex = 0; iVertex < mesh->mNumVertices; iVertex++)
        {
            const aiVector3D* pos = &(mesh->mVertices[iVertex]);
            const aiVector3D* normal = &(mesh->mNormals[iVertex]);

            if (pos->x < AABB[0][0])
                AABB[0][0] = pos->x;
            if (pos->y < AABB[0][1])
                AABB[0][1] = pos->y;
            if (pos->z < AABB[0][2])
                AABB[0][2] = pos->z;
            if (pos->x > AABB[1][0])
                AABB[1][0] = pos->x;
            if (pos->y > AABB[1][1])
                AABB[1][1] = pos->y;
            if (pos->z > AABB[1][2])
                AABB[1][2] = pos->z;

            const aiVector3D* uv_or_color = nullptr;
            if(bHasUVs || bHasColor)
            {
                uv_or_color = &(mesh->mTextureCoords[0][iVertex]);
            }

            Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent vertex = {};
            memcpy(vertex.m_position, pos, 3 * sizeof(float));

            if(bHasUVs)
            {
                const aiVector3D* uv = &mesh->mTextureCoords[0][iVertex];
                vertex.m_uv[0] = uv->x;
                vertex.m_uv[1] = uv->y;
            }
            else
            {
                vertex.m_uv[0] = 0.0f;
                vertex.m_uv[1] = 0.0f;
            }

            if(bHasColor)
            {
                const aiColor4D* color = mesh->mColors[0];
                vertex.m_color[0] = color->r;
                vertex.m_color[1] = color->g;
                vertex.m_color[2] = color->b;
            }
            else
            {
                vertex.m_color[0] = 1.0f;
                vertex.m_color[1] = 1.0f;
                vertex.m_color[2] = 1.0f;
            }

            vertex.m_normal[0] = normal->x;
            vertex.m_normal[1] = -normal->y;
            vertex.m_normal[2] = normal->z;

            if(bHasTangentBitangent)
            {
                const aiVector3D* tangent = &mesh->mTangents[iVertex];
                const aiVector3D* bitangent = &mesh->mBitangents[iVertex];
                vertex.m_tangent[0] = tangent->x;
                vertex.m_tangent[1] = tangent->y;
                vertex.m_tangent[2] = tangent->z;
                vertex.m_bitangent[0] = bitangent->x;
                vertex.m_bitangent[1] = bitangent->y;
                vertex.m_bitangent[2] = bitangent->z;
            }
            else
            {
                vertex.m_tangent[0] = 0.0f;
                vertex.m_tangent[1] = 1.0f;
                vertex.m_tangent[2] = 0.0f;
                vertex.m_bitangent[0] = 0.0f;
                vertex.m_bitangent[1] = 1.0f;
                vertex.m_bitangent[2] = 0.0f;
            }

            vertexBuffer->write(vertex);
        }

        OE_CHECK(vertexBuffer->isEndOfBuffer(), OE_CHANNEL_RESOURCE);

        // Loop through all indices and check the biggest one to see if they have to be 16 or 32 bit.
        uint32 maxIndex = 0;
        for(size_t iFace = 0; iFace < mesh->mNumFaces; ++iFace)
        {
            const aiFace& face = mesh->mFaces[iFace];
            for(size_t iIndex = 0; iIndex < 3; ++iIndex)
            {
                if(face.mIndices[iIndex] > maxIndex)
                    maxIndex = face.mIndices[iIndex];
            }
        }

        size_t sizeofIndex = maxIndex >= OE_MAX_UINT16 ? sizeof(uint32) : sizeof(uint16);

        UniquePtr<Buffer> indexBuffer(new Buffer(mesh->mNumFaces * 3 * sizeofIndex));

        if(sizeofIndex == sizeof(uint16))
        {
            for(size_t iFace = 0; iFace < mesh->mNumFaces; ++iFace)
            {
                const aiFace& face = mesh->mFaces[iFace];
                for(size_t iIndex = 0; iIndex < 3; ++iIndex)
                {
                    indexBuffer->write((uint16)(face.mIndices[iIndex]));
                }
            }
        }
        else
        {
            for(size_t iFace = 0; iFace < mesh->mNumFaces; ++iFace)
            {
                const aiFace& face = mesh->mFaces[iFace];
                for(size_t iIndex = 0; iIndex < 3; ++iIndex)
                {
                    indexBuffer->write((uint32)(face.mIndices[iIndex]));
                }
            }
        }

        OE_CHECK(indexBuffer->isEndOfBuffer(), OE_CHANNEL_RESOURCE);

        char nameBuffer[512];
        OE_SPRINTF(nameBuffer, 512, "%s.vertex_buffer_%d", m_resourcePath.c_str(), (uint32)iMesh);

        auto vertexStreamResource = MakeShared<VertexStreamResource>(
            std::move(vertexBuffer),
            std::move(indexBuffer),
            sizeofIndex == sizeof(uint16) ? IndexType::U16 : IndexType::U32,
            VertexDeclaration::XYZ_UV_Color_Normal_Tangent_Bitangent,
            PrimitiveType::TriangleList,
            BufferUsage::Static,
            nameBuffer);

        OE_CHECK(mesh->mMaterialIndex < (uint32)m_perMaterials.size(), OE_CHANNEL_RESOURCE);
        m_perMaterials[mesh->mMaterialIndex].m_vertexStreams.push_back(vertexStreamResource);
    }

}

void PropResourceLoadTask::createResources()
{
    uint32 iMaterial = 0;
    uint32 iMesh = 0;

    for(auto& perMaterial : m_perMaterials)
    {
        auto materialDescPtr = MakeShared<MaterialResource::Descriptor>();
        materialDescPtr->m_vsPath = "mrt";
        materialDescPtr->m_psPath = "mrt";
        materialDescPtr->m_renderPassPath = "render/deferred_render_pass";
        materialDescPtr->m_viewResourcePath = "render/views/deferred";
        materialDescPtr->m_shaderBindingLayoutPath = "deferred_binding";
        strVertexDeclarationToInputs(
            "Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent",
            materialDescPtr->m_inputs);
        materialDescPtr->m_renderStates.m_alphaBlendEnable = false;
        materialDescPtr->m_renderStates.m_cullMode = CullMode::CounterClockWise;
        materialDescPtr->m_renderStates.m_zEnable = true;
        materialDescPtr->m_renderStates.m_zWriteEnable = true;
        materialDescPtr->m_topology = PrimitiveType::TriangleList;

        bool bHasDiffuse = false;
        bool bHasNormal = false;
        bool bHasSpecular = false;
        for(auto& texture : perMaterial.m_material.m_textures)
        {
            bool bSupportedTexture = true;

            switch(texture.m_type)
            {
            case TextureBindingType::Diffuse:
            bHasDiffuse = true;
            break;

            case TextureBindingType::Normals:
            bHasNormal = true;
            break;

            case TextureBindingType::Specular:
            bHasSpecular = true;
            break;

            default:
            bSupportedTexture = false;
            }

            if(bSupportedTexture)
            {
                MaterialResource::Descriptor::ShaderBinding shaderBinding;
                shaderBinding.m_bindId = texture.m_type;
                shaderBinding.m_bindingType = BindingType::Sampler;
                shaderBinding.m_samplerPath = texture.m_path;
                shaderBinding.m_samplerType = SamplerType::Texture2D;
                materialDescPtr->m_shaderBindings.push_back(shaderBinding);
            }
        }

        if(!bHasDiffuse)
        {
            materialDescPtr->addDefaultDiffuse();
        }

        if(!bHasNormal)
        {
            materialDescPtr->addDefaultNormal();
        }

        if(!bHasSpecular)
        {
            materialDescPtr->addDefaultSpecular();
        }
        OE_CHECK(materialDescPtr->m_shaderBindings.size() > 0, OE_CHANNEL_RESOURCE);

        char materialName[512];
        OE_SPRINTF(materialName, 512, "%s.material_%d", m_resourcePath.c_str(), iMaterial);
        ++iMaterial;

        auto materialResource = MakeShared<MaterialResource>(
            materialName,
            materialDescPtr);

        for(auto& vertexStreamResource : perMaterial.m_vertexStreams)
        {
            char meshName[512];
            OE_SPRINTF(meshName, 512, "%s.mesh_%d", m_resourcePath.c_str(), iMesh);
            ++iMesh;

            SharedPtr<MeshResource::Descriptor> meshResourceDescriptor = 
                MakeShared<MeshResource::Descriptor>();
            meshResourceDescriptor->m_materialResource = materialResource;
            meshResourceDescriptor->m_vertexStreamResource = vertexStreamResource;

            auto meshResource = MakeShared<MeshResource>(
                meshName,
                meshResourceDescriptor);

            m_descriptor->m_meshResources.push_back(meshResource);
        }
    }
}


} // OE_Graphic


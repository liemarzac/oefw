#ifndef __OE_GRAPHIC_H__
#define __OE_GRAPHIC_H__

#include <oe_core.h>

// OE_Core
#include <buffer.h>
#include <flagset.h>
#include <oe_string.h>
#include <ref_count.h>
#include <std_atomic_helpers.h>
#include <vector.h>

// OE_Graphic
#include "oe_graphic_export.h"
#include "graphic_resource.h"

#define OE_CHANNEL_GRAPHIC "Graphic"

namespace OE_Graphic
{

enum class HorizontalAnchor
{
    Left,
    Center,
    Right
};

enum class VerticalAnchor
{
    Top,
    Center,
    Bottom
};

enum class ShaderType
{
    VertexShader,
    PixelShader,
};

enum class VertexDeclaration
{
    XYZ_UV,
    XYZW_UV,
    XYZW_Color,
    XYZ_UV_Normal,
    XYZ_UV_Color_Normal_Tangent_Bitangent,
    XYZ_Normal,
    XYZ,
    XYZ_Color_Normal,
    XYZ_Color,
    Count
};

namespace TextureBindingType
{
enum Enum
{
    Diffuse = 0,
    Normals,
    Specular,
    Emissive,
    Opacity,
    Count
};
}

enum class TextureFormat
{
    R8G8B8A8_UNORM,
    R8G8B8_UNORM,
    B8G8R8A8_UNORM,
    R32G32B32A32_UINT,
    R32G32B32A32_SFLOAT,
    RGBA_DXT3_UNORM_BLOCK16,
    Count
};

enum class IndexType
{
    U16,
    U32
};

enum class CullMode
{
    None,
    ClockWise,
    CounterClockWise
};

enum class PrimitiveType
{
    TriangleList,
    TriangleFan,
    LineList,
};

enum class AttributeType
{
    Float,
    Float2,
    Float3,
    Float4,
    UInt_Norm,
    Mat2x2,
    Mat3x3,
    Mat4x4,
    Tex2d,
    Unknown
};

enum class BindingType
{
    Uniform,
    Sampler,
    InputAttachment,
    Unknown
};

enum class SamplerType
{
    Texture2D,
    RenderTarget,
    Unknown
};

enum class BufferUsage
{
    Static,
    Dynamic
};

enum class RenderTargetType
{
    Color,
    Depth,
    Backbuffer,
    Count
};

enum class RenderTargetLoadOperation
{
    NoCare,
    Preserve,
    Clear
};

enum class RenderTargetFlags
{
    SubpassInput // Vulkan usage
};

namespace DefaultRenderSceneType
{
    enum Enum
    {
        World,
        UI,
        Count
    };
}

struct Vertex_XYZ_Color_Normal
{
    float m_position[3];
    uint32 m_color;
    float m_normal[3];
};

struct Vertex_XYZ_Color
{
    float m_position[3];
    uint32 m_color;
};

struct Vertex_XYZW_Color
{
    float m_position[4];
    uint32 m_color;
};

struct Vertex_XYZW_UV
{
    float m_position[4];
    float m_uv[2];
};

struct Vertex_XYZ_UV_Normal
{
    float m_position[3];
    float m_uv[2];
    float m_normal[3];
};

struct Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent
{
    float m_position[3];
    float m_uv[2];
    float m_color[3];
    float m_normal[3];
    float m_tangent[3];
    float m_bitangent[3];
};

struct Vertex_XYZ_Normal
{
    float m_position[3];
    float m_normal[3];
};

struct Vertex_XYZ
{
    float m_position[3];
};

struct Vertex_XYZ_UV
{
    float m_position[3];
    float m_uv[2];
};

static const size_t VertexStrideTable[(uint32)VertexDeclaration::Count] =
{
    sizeof(Vertex_XYZ_UV),
    sizeof(Vertex_XYZW_UV),
    sizeof(Vertex_XYZW_Color),
    sizeof(Vertex_XYZ_UV_Normal),
    sizeof(Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent),
    sizeof(Vertex_XYZ_Normal),
    sizeof(Vertex_XYZ),
    sizeof(Vertex_XYZ_Color_Normal),
    sizeof(Vertex_XYZ_Color)
};

struct RenderPassId
{
    RenderPassHandle m_handle;
    uint32 m_iSubpass = 0;

    bool operator==(const RenderPassId& rhs) const
    {
        return m_iSubpass == rhs.m_iSubpass && m_handle == rhs.m_handle;
    }

    bool operator<(const RenderPassId& rhs) const
    {
        const intptr_t thisIntPtr = reinterpret_cast<intptr_t>(*m_handle.m_resource);
        const intptr_t rhsIntPtr = reinterpret_cast<intptr_t>(*rhs.m_handle.m_resource);
        if(thisIntPtr < rhsIntPtr)
        {
            return true;
        }
        else if(thisIntPtr == rhsIntPtr)
        {
            if(m_iSubpass < rhs.m_iSubpass)
            {
                return true;
            }
        }

        return false;
    }
};

struct ShaderBind
{
    uint32 m_bindId = InvalidIndex;
    BindingType m_bindType = BindingType::Unknown;
    SamplerType m_samplerType = SamplerType::Unknown;
    TextureHandle m_textureHandle;
    RenderTargetHandle m_renderTargetHandle;
};

struct ShaderBindingLayout
{
    struct Binding
    {
        enum class Type
        {
            Uniform,
            UniformDynamic,
            Sampler,
            InputAttachment,
            Unknown
        };

        OE_Core::FlagSet<ShaderType> m_stages;
        Type m_type = Type::Unknown;
    };

    struct Layout
    {
        Vector<Binding> m_bindings;
    };

    Vector<Layout> m_layouts;
};

struct RenderStates
{
    RenderStates()
    {
        m_zEnable = false;
        m_zWriteEnable = false;
        m_alphaBlendEnable = false;
        m_cullMode = CullMode::CounterClockWise;
    }

    bool m_zEnable;
    bool m_zWriteEnable;
    bool m_alphaBlendEnable;
    CullMode m_cullMode;
};

struct RenderPassDescriptor
{
    struct Attachment
    {
        String m_path;
        RenderTargetHandle m_handle;
        RenderTargetLoadOperation m_loadOp = RenderTargetLoadOperation::NoCare;
    };

    struct Subpass
    {
        struct InputRenderTarget
        {
            String m_path;
            RenderTargetHandle m_handle;
        };

        struct OutputRenderTarget
        {
            String m_path;
            RenderTargetHandle m_handle;
        };

        Vector<InputRenderTarget> m_inputRenderTargets;
        Vector<OutputRenderTarget> m_outputRenderTargets;
    };

    Vector<Attachment> m_attachments;
	Vector<Subpass> m_subpasses;
	bool m_persistent = false;
};

struct CreateRenderPassParams
{
    CreateRenderPassParams& operator&&(CreateRenderPassParams& rhs)
    {
        m_renderPassHandle = rhs.m_renderPassHandle;
        DBG_ONLY(m_name = std::move(rhs.m_name));
    }

    RenderPassHandle m_renderPassHandle;
    SharedPtr<RenderPassDescriptor> m_descriptor;
    DBG_ONLY(String m_name);
};

struct CreateRenderPipelineParams
{
    struct RenderPassDescriptor
    {
        RenderPassHandle m_renderPass;
        Vector<RenderPassHandle> m_waitFor;
    };

    Vector<RenderPassDescriptor> m_renderPassDescriptors;
};

struct CreateRenderTargetParams
{
    RenderTargetType m_type = RenderTargetType::Count;
    OE_Core::FlagSet<RenderTargetFlags> m_flags;
    uint32 m_width = 0;
    uint32 m_height = 0;
    TextureFormat m_textureFormat = TextureFormat::Count;
};

struct CreatePipelineParams
{
    VertexShaderHandle m_vsHandle;
    PixelShaderHandle m_psHandle;
    ShaderBindingHandle m_shaderBindingHandle;
    RenderPassHandle m_renderPassHandle;
    ViewHandle m_viewHandle;
    uint32 m_iSubpass = 0;
    Vector<AttributeType> m_vertexShaderInputAttribs;
    Vector<ShaderBind> m_shaderBindings;
    PrimitiveType m_topology = PrimitiveType::TriangleList;
    size_t m_uboSize = 0;
    bool m_bAlphaBlendEnabled = false;
};

struct CreateDrawInstanceParams
{
    PipelineHandle m_pipelineHandle;
    VertexBufferHandle m_vertexBufferHandle;
    IndexBufferHandle m_indexBufferHandle;
};

struct CreateTextureParams
{
    CreateTextureParams& operator=(CreateTextureParams&& rhs)
    {
        m_handle = rhs.m_handle;
        m_width = rhs.m_width;
        m_height = rhs.m_height;
        m_nMips = rhs.m_nMips;
        m_pitch = rhs.m_pitch;
        m_format = rhs.m_format;
        m_mipSizes = std::move(rhs.m_mipSizes);
        m_data = std::move(rhs.m_data);
        DBG_ONLY(m_name = std::move(rhs.m_name));
        return *this;
    }

    TextureHandle m_handle; // optional, will be created if not given.
    uint32 m_width = 0;
    uint32 m_height = 0;
    uint32 m_nMips = 0;
    uint32 m_pitch = 0;
    TextureFormat m_format = TextureFormat::R8G8B8A8_UNORM;
    Vector<size_t> m_mipSizes;
    OE_Core::UniqueBufferPtr m_data;
    DBG_ONLY(String m_name);
};

struct CreateVertexBufferParams
{
    CreateVertexBufferParams& operator&&(CreateVertexBufferParams& rhs)
    {
        m_vextexBufferHandle = rhs.m_vextexBufferHandle;
        m_buffer = std::move(rhs.m_buffer);
        m_format = rhs.m_format;
        m_usage = rhs.m_usage;
        DBG_ONLY(m_name = std::move(m_name));
    }

    VertexBufferHandle m_vextexBufferHandle;
    OE_Core::UniqueBufferPtr m_buffer;
    VertexDeclaration m_format;
    BufferUsage m_usage;
    DBG_ONLY(String m_name);
};

struct CreateIndexBufferParams
{
    CreateIndexBufferParams& operator&&(CreateIndexBufferParams& rhs)
    {
        m_indexBufferHandle = rhs.m_indexBufferHandle;
        m_buffer = std::move(rhs.m_buffer);
        m_indexType = rhs.m_indexType;
        m_usage = rhs.m_usage;
        DBG_ONLY(m_name = std::move(m_name));
    }

    IndexBufferHandle m_indexBufferHandle;
    OE_Core::UniqueBufferPtr m_buffer;
    IndexType m_indexType;
    BufferUsage m_usage;
    DBG_ONLY(String m_name);
};

struct CreateViewParams
{
    size_t m_uboSize = 0;
    OE_Core::FlagSet<ShaderType> m_stages;
};

struct ScreenSize
{
    uint32 m_width = 0;
    uint32 m_height = 0;

    bool operator==(const ScreenSize& rhs)
    {
        return memcmp(this, &rhs, sizeof(ScreenSize)) == 0;
    }
};

using MeshInstancePtr = OE_Core::RefCountPtr<class MeshInstance>;

void OE_GRAPHIC_EXPORT globalInit();

bool OE_GRAPHIC_EXPORT strVertexDeclarationToInputs(
    const String& vertexDeclaration,
    Vector<AttributeType>& inputs);

}

#endif // __OE_GRAPHIC_H__

#ifndef __OE_GRAPHIC_MESH_INSTANCE_H__
#define __OE_GRAPHIC_MESH_INSTANCE_H__

#include "oe_graphic.h"

// OE_Core
#include <eigen_helpers.h>
#include <ref_count.h>

// OE_Graphic
#include "mesh_resource.h"

#define OE_CHANNEL_MESH_INSTANCE "MeshInstance"

namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT MeshInstance : public OE_Core::RefCount
{

public:
    EIGEN_ALIGNED

    struct CreateParams
    {
        PipelineHandle m_pipelineHandle;
        VertexBufferHandle m_vertexBufferHandle;
        IndexBufferHandle m_indexBufferHandle;
        RenderPassId m_renderPassId;
    };

    MeshInstance();
    MeshInstance(const CreateParams& params);

    ~MeshInstance();

    DBG_ONLY(void setName(const String& name));
    void set(const CreateParams& params);

    PipelineHandle getPipelineHandle() const;
    DrawInstanceHandle getDrawInstanceHandle() const;
    const RenderPassId& getRenderPassId() const;

    const Mat4& getObjectToWorldTransform() const;
    void setObjectToWorldTransform(const Mat4& transform);
    void setPosition(const Vec3& position);
    void setOrientation(const Mat4& orientation);

    void updateBuffer();

    bool isLoaded() const;
    //bool operator<(const MeshInstance& rhs);

    struct Instance
    {
        alignas(16) Mat4 m_modelViewProjection;
        alignas(16) Mat4 m_modelView;
        alignas(16) Mat4 m_transInvModelView;
        alignas(16) Mat4 m_transInvModel;
    }m_instance;

    AtomicBool m_bRendering;

private:
    PipelineHandle m_pipelineHandle;
    DrawInstanceHandle m_drawInstanceHandle;
    RenderPassId m_renderPassId;
    Mat4 m_objectToWorldTransform;
    DBG_ONLY(String m_name)
};

inline bool MeshInstance::isLoaded() const
{
    return m_drawInstanceHandle.isReady();
}

inline const Mat4& MeshInstance::getObjectToWorldTransform() const
{
    return m_objectToWorldTransform;
}

inline void MeshInstance::setObjectToWorldTransform(const Mat4& transform)
{
    m_objectToWorldTransform = transform;
}

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_MESH_INSTANCE_H__

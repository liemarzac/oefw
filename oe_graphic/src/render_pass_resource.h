#ifndef __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__
#define __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__

#include "oe_graphic.h"

// OE_Graphic
#include "graphic_resource_helpers.h"

// OE_Core
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Graphic
{

class RenderPassResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    RenderPassResource() = delete;
    RenderPassResource(const String& path);
    virtual ~RenderPassResource();

    virtual const char* getType() const override;
    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    const RenderPassHandle& getHandle() const;

private:
    enum class LoadingState
    {
        WaitingForDescriptor,
        DescriptorReady,
        WaitingForRenderTargets,
        CreateRenderPass,
        WaitingForRenderPass,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;


    SharedPtr<OE_Core::ResourceLoadTask<RenderPassDescriptor>> m_loadTaskPtr;
    SharedPtr<RenderPassDescriptor> m_descriptorPtr;
    Vector<RenderTargetResPtr> m_renderTargets;

    RenderPassHandle m_handle;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<RenderPassDescriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__

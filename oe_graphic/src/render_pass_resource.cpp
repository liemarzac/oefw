#include "render_pass_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>

// OE_Graphic
#include "render_target_resource.h"
#include "renderer.h"
#include "view_resource.h"

// OE_Core
#include <task_manager.h>

namespace OE_Graphic
{

RTTI_IMPL_PARENT(RenderPassResource, Resource)

RenderPassResource::RenderPassResource(const String& path):
    Resource(path)
{
}

RenderPassResource::~RenderPassResource()
{
}

const char* RenderPassResource::getType() const
{
    return "render_pass_resource";
}

void RenderPassResource::loadImpl()
{
    m_descriptorPtr = MakeShared<RenderPassDescriptor>();
    m_loadTaskPtr = MakeShared<ResourceLoadTask<RenderPassDescriptor>>(
        getName(),
        m_descriptorPtr,
        &RenderPassResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void RenderPassResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
    {
        if(m_loadTaskPtr->isFinished())
        {
            for (auto& renderTarget : m_descriptorPtr->m_attachments)
            {
                addUnique(m_renderTargets,
                    addDependency<RenderTargetResource>(
                        dependencyCallback,
                        renderTarget.m_path));
            }

            m_loadingState = LoadingState::WaitingForRenderTargets;
        }
        else
        {
            break;
        }
    }

    case LoadingState::WaitingForRenderTargets:
    {
        bool bAllLoaded = true;

        for (auto& rt : m_renderTargets)
        {
            if (!rt->isLoaded())
            {
                bAllLoaded = false;
                break;
            }
        }

        if(bAllLoaded)
        {
            for (auto& renderTarget : m_descriptorPtr->m_attachments)
            {
                bool bFound = false;
                for (auto& rt : m_renderTargets)
                {
                    if (rt->getName() == renderTarget.m_path)
                    {
                        renderTarget.m_handle = rt->getHandle();
                        bFound = true;
                        break;
                    }
                }
                OE_CHECK(bFound, OE_CHANNEL_RESOURCE);
            }

            for (auto& descSubpass : m_descriptorPtr->m_subpasses)
            {
                // Assign render target handles to corresponding resource
                for (auto& inputRT : descSubpass.m_inputRenderTargets)
                {
                    bool bFound = false;
                    for (auto& rt : m_renderTargets)
                    {
                        if (rt->getName() == inputRT.m_path)
                        {
                            inputRT.m_handle = rt->getHandle();
                            bFound = true;
                            break;
                        }
                    }
                    OE_CHECK(bFound, OE_CHANNEL_RESOURCE);
                }

                for (auto& outputRT : descSubpass.m_outputRenderTargets)
                {
                    bool bFound = false;
                    for (auto& rt : m_renderTargets)
                    {
                        if (rt->getName() == outputRT.m_path)
                        {
                            outputRT.m_handle = rt->getHandle();
                            bFound = true;
                            break;
                        }
                    }
                    OE_CHECK(bFound, OE_CHANNEL_RESOURCE);
                }
            }

            m_loadingState = LoadingState::CreateRenderPass;
        }
        else
        {
            break;
        }
    }

    case LoadingState::CreateRenderPass:
    {
        CreateRenderPassParams params;
        DBG_ONLY(params.m_name = getName());
        params.m_descriptor = m_descriptorPtr;

        auto& renderer = Renderer::getInstance();
        m_handle = renderer.createRenderPass(params);

        m_loadingState = LoadingState::WaitingForRenderPass;
    }

    case LoadingState::WaitingForRenderPass:
    {
        if(m_handle.isReady())
        {
            m_loadingState = LoadingState::Loaded;
        }
        else
        {
            break;
        }
    }

    case LoadingState::Loaded:
    {
        m_loadTaskPtr.reset();
        setState(State::Loaded);
    }

    }
}

const RenderPassHandle& RenderPassResource::getHandle() const
{
    return m_handle;
}

void RenderPassResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<RenderPassDescriptor> descriptor)
{
    Vector<String> m_attachments;

    // Attachment array
    {
        const auto& jsonAttachmentArray = jsonContent["attachments"];
        if (jsonAttachmentArray.isArray())
        {
            for (auto& jsonAttachment : jsonAttachmentArray)
            {
                descriptor->m_attachments.push_back({});
                auto& attachment = descriptor->m_attachments.back();

                // Name
                {
                    const auto& jsonName = jsonAttachment["name"];
                    OE_CHECK(jsonName.isString(), OE_CHANNEL_RESOURCE);
                    attachment.m_path = jsonName.asString();
                    m_attachments.push_back(attachment.m_path);
                }

                // Load operation
                {
                    const auto& jsonLoadOp = jsonAttachment["load_operation"];
                    OE_CHECK(jsonLoadOp.isString(), OE_CHANNEL_RESOURCE);
                    String loadOpAsString = jsonLoadOp.asString();
                    if (loadOpAsString == "no_care")
                    {
                        attachment.m_loadOp = RenderTargetLoadOperation::NoCare;
                    }
                    else if (loadOpAsString == "preserve")
                    {
                        attachment.m_loadOp = RenderTargetLoadOperation::Preserve;
                    }
                    else if (loadOpAsString == "clear")
                    {
                        attachment.m_loadOp = RenderTargetLoadOperation::Clear;
                    }
                    else
                    {
                        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown load operation %s", loadOpAsString.c_str());
                    }
                }
            }
        }
    }

    // Subpass array
    {
        const auto& jsonSubpassArray = jsonContent["subpasses"];
        OE_CHECK(jsonSubpassArray.isArray(), OE_CHANNEL_RESOURCE);
        OE_CHECK(jsonSubpassArray.size() > 0, OE_CHANNEL_RESOURCE);
        for (auto& jsonSubpass : jsonSubpassArray)
        {
            descriptor->m_subpasses.push_back({});
            auto& subpass = descriptor->m_subpasses.back();

            // Subpass
            {
                OE_CHECK(jsonSubpass.isObject(), OE_CHANNEL_RESOURCE);

                // Input render Target array (optional)
                {
                    const auto& jsonRenderTargetArray = jsonSubpass["input_render_targets"];
                    if (jsonRenderTargetArray.isArray())
                    {
                        OE_CHECK(jsonRenderTargetArray.size() > 0, OE_CHANNEL_RESOURCE);
                        for (auto& jsonRT : jsonRenderTargetArray)
                        {
                            subpass.m_inputRenderTargets.push_back({});
                            auto& inputRT = subpass.m_inputRenderTargets.back();

                            // Render Target
                            OE_CHECK(jsonRT.isString(), OE_CHANNEL_RESOURCE);
                            inputRT.m_path = jsonRT.asString();
                            OE_CHECK_MSG(isContained(m_attachments, inputRT.m_path), OE_CHANNEL_RESOURCE, "Input render target %s is not in the list of attachments", inputRT.m_path.c_str());
                        }
                    }
                }

                // Output render Target array
                {
                    const auto& jsonRenderTargetArray = jsonSubpass["output_render_targets"];
                    OE_CHECK(jsonRenderTargetArray.isArray(), OE_CHANNEL_RESOURCE);
                    OE_CHECK(jsonRenderTargetArray.size() > 0, OE_CHANNEL_RESOURCE);
                    for (auto& jsonRT : jsonRenderTargetArray)
                    {
                        subpass.m_outputRenderTargets.push_back({});
                        auto& outputRT = subpass.m_outputRenderTargets.back();

                        // Render Target
                        OE_CHECK(jsonRT.isString(), OE_CHANNEL_RESOURCE);
                        outputRT.m_path = jsonRT.asString();
                        OE_CHECK_MSG(isContained(m_attachments, outputRT.m_path), OE_CHANNEL_RESOURCE, "Output render target %s is not in the list of attachments", outputRT.m_path.c_str());
                    }
                }
            }
        }
    }

    const auto& jsonPersistent = jsonContent["persistent"];
    OE_CHECK(jsonPersistent.isBool(), OE_CHANNEL_RESOURCE);
    descriptor->m_persistent = jsonPersistent.asBool();
}


} // namespace OE_Graphic

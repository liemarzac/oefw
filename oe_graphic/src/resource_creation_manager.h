#ifndef __OE_GRAPHIC_RESOURCE_CREATION_MANAGER_H__
#define __OE_GRAPHIC_RESOURCE_CREATION_MANAGER_H__

#include "oe_graphic.h"

// OE_Core
#include <array.h>
#include <linear_allocator.h>
#include <vector.h>


namespace OE_Graphic
{

struct DriverCommand;

class ResourceCreationManager
{
public:
    ResourceCreationManager();
    ~ResourceCreationManager();

    void* addCommand(size_t size);
    void commitCommand();
    void execute();

private:

    void swap();

    struct CreationCommands
    {
        OE_Core::LinearAllocator* m_creationCmdAllocator = nullptr;
        Vector<DriverCommand*> m_creationCmds;
    };

    Array<CreationCommands, 2> m_creations;
    CreationCommands* m_creationProducer = nullptr;
    CreationCommands* m_creationConsumer = nullptr;
    Mutex m_creationMutex;
    Mutex m_executionMutex;
};

}

#endif

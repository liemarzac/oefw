#ifndef __OE_GRAPHIC_RENDER_TARGET_RESOURCE_H__
#define __OE_GRAPHIC_RENDER_TARGET_RESOURCE_H__

#include "oe_graphic.h"

// OE_Graphic
#include "graphic_resource_helpers.h"

// OE_Core
#include <flagset.h>
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Graphic
{

class RenderTargetResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    struct Descriptor
    {
        RenderTargetType m_type;
        TextureFormat m_format = TextureFormat::Count;
        OE_Core::FlagSet<RenderTargetFlags> m_flags;
    };

    RenderTargetResource() = delete;
    RenderTargetResource(const String& path);

    virtual const char* getType() const override;
    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    const RenderTargetHandle& getHandle() const;

private:
    enum class LoadingState
    {
        WaitingForDescriptor,
        CreateRenderTarget,
        WaitingForRenderTarget,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;

    SharedPtr<OE_Core::ResourceLoadTask<Descriptor>> m_loadTaskPtr;
    RenderTargetHandle m_handle;
    RenderTargetType m_type;
    TextureFormat m_format = TextureFormat::Count;
    OE_Core::FlagSet<RenderTargetFlags> m_flags;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_GRAPHIC_RENDER_TARGET_RESOURCE_H__

#include "raw_texture.h"

#include "graphic_private.h"


namespace OE_Graphic
{

	void serialize_impl(Buffer& buffer, const RawTexture& fontTexture, const RawTexture*)
{
	serialize(buffer, fontTexture.m_textureWidth);
	serialize(buffer, fontTexture.m_textureHeight);
	serialize(buffer, fontTexture.m_textureBuffer);
}

	void deserialize_impl(Buffer& buffer, RawTexture& fontTexture, const RawTexture*)
{
	deserialize(buffer, fontTexture.m_textureWidth);
	deserialize(buffer, fontTexture.m_textureHeight);
	deserialize(buffer, fontTexture.m_textureBuffer);
}

	size_t calculateSerializedSize_impl(const RawTexture& fontTexture, const RawTexture*)
{
	return calculateSerializedSize(fontTexture.m_textureWidth) +
		calculateSerializedSize(fontTexture.m_textureHeight) +
		calculateSerializedSize(fontTexture.m_textureBuffer);
}

} // namespace OE_Graphic

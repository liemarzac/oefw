﻿#include "renderer.h"

#include "graphic_private.h"

// OE_Core
#include <ring_buffer.h>

// OE_Graphic
#include "driver_command.h"
#include "material_resource.h"
#include "resource_creation_manager.h"
#include "render_scene.h"
#include "ubo_factory.h"

#if OE_RENDERER == OE_RENDERER_VULKAN
#include "drivers/vulkan/vulkan_driver.h"
#elif OE_RENDERER == OE_RENDERER_OPENGL_ES2
#include "drivers/opengl_es2/opengl_es2_renderer.h"
#elif OE_RENDERER == OE_RENDERER_OPENGL_3_2
#include "drivers/opengl_3_2/opengl_3_2_renderer.h"
#elif OE_RENDERER == OE_RENDERER_DIRECTX_9
#include "drivers/directx_9/directx_9_renderer.h"
#endif

#define OE_CHANNEL_RENDERER "Renderer"

namespace OE_Graphic
{

Renderer::Renderer()
{
#if OE_RENDERER == OE_RENDERER_VULKAN
    m_driver = &Vulkan::Driver::createInstance();
#elif OE_RENDERER == OE_RENDERER_OPENGL_ES2
    m_driver = new OpenGl_ES2_Renderer();
#elif OE_RENDERER == OE_RENDERER_OPENGL_3_2
    m_driver = new OpenGl_3_2_Renderer();
#elif OE_RENDERER == OE_RENDERER_DIRECTX_9
    m_driver = new DirectX_9_Renderer();
#endif

    m_bSceneToRenderReady.store(false);
    m_bFlush.store(false);
    
    m_resourceCreationManager = new ResourceCreationManager();
    m_uboFactory = new UBOFactory();
    m_uboFactory->registerUBOClass<DefaultView_UBO>();
    m_uboFactory->registerUBOClass<ScreenView_UBO>();
    m_uboFactory->registerUBOClass<Lighting_UBO>();

    // Create the default render scenes.
    createRenderScene();
}

Renderer::~Renderer()
{
    // Prepare destruction.
    flush();

    // Mirror constructor.
    destroyRenderScene();
    delete m_uboFactory;
    delete m_resourceCreationManager;
    delete m_driver;
}

bool Renderer::initFromMainThread(const RendererConfig& config)
{
    m_screenSize = config.m_screenSize;
    return m_driver->initFromMainThread(config);
}

bool Renderer::initFromRenderThread(const RendererConfig& config)
{
    m_renderThreadId = std::this_thread::get_id();

    if(!m_driver->initFromDriverThread(config))
        return false;

    return true;
}

void Renderer::waitUntilReady()
{
    m_driver->waitUntilReady();
}

void Renderer::beginFrame()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->beginFrame();
}

void Renderer::endFrame()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->endFrame();
}

void Renderer::beginRenderPass(RenderPassHandle handle, uint32 iSubpass)
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->beginRenderPass(handle, iSubpass);
}

void Renderer::endRenderPass()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->endRenderPass();
}

void Renderer::iterateRenderPipeline(std::function<void(const RenderPassId&)> callback)
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->iterateRenderPipeline(callback);
}

void Renderer::commitCreationCmd()
{
    m_resourceCreationManager->commitCommand();
}

VertexBufferHandle Renderer::createVertexBuffer(CreateVertexBufferParams& params)
{
    // Create handle.
    VertexBufferHandle handle = m_driver->createVertexBufferHandle();
    handle.m_resource = new VertexStream();

    // Submit creation command.
    auto* rc = allocCreationCmd<CreateVertexBufferCommand>();
    rc->m_params = std::move(params);
    rc->m_params.m_vextexBufferHandle = handle;
    commitCreationCmd();

    return handle;
}

IndexBufferHandle Renderer::createIndexBuffer(CreateIndexBufferParams& params)
{
    // Create handle.
    IndexBufferHandle handle;
    handle.m_resource = new IndexBuffer();

    // Submit creation command.
    auto* rc = allocCreationCmd<CreateIndexBufferCommand>();
    rc->m_params = std::move(params);
    rc->m_params.m_indexBufferHandle = handle;
    commitCreationCmd();

    return handle;
}

VertexShaderHandle Renderer::createVertexShader(
        UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name))
{
    return m_driver->createVertexShader(
        std::move(buffer)
        DBG_PARAM_ADD(name));
}

PixelShaderHandle Renderer::createPixelShader(
        UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name))
{
    return m_driver->createPixelShader(
        std::move(buffer)
        DBG_PARAM_ADD(name));
}


ShaderBindingHandle Renderer::createShaderBinding(
    const ShaderBindingLayout& layout
    DBG_STR_PARAM_ADD(name))
{
    return m_driver->createShaderBinding(
        layout
        DBG_PARAM_ADD(name));
}

PipelineHandle Renderer::createPipeline(
        const CreatePipelineParams& params
        DBG_STR_PARAM_ADD(name))
{
    return m_driver->createPipeline(
        params
        DBG_PARAM_ADD(name));
}

DrawInstanceHandle Renderer::createDrawInstance(
    const CreateDrawInstanceParams& params
    DBG_STR_PARAM_ADD(name))
{
    return m_driver->createDrawInstance(
        params
        DBG_PARAM_ADD(name));
}

TextureHandle Renderer::createTexture(CreateTextureParams& params)
{
    OE_CHECK(params.m_data, OE_CHANNEL_RENDERER);

    // Create handle.
    TextureHandle handle = m_driver->createTextureHandle();

    // Submit creation command.
    auto* rc = allocCreationCmd<CreateTextureCommand>();
    rc->m_params = std::move(params);
    rc->m_params.m_handle = handle;
    commitCreationCmd();

    return handle;
}

RenderPipelineHandle Renderer::createRenderPipeline(
    const CreateRenderPipelineParams& params
    DBG_STR_PARAM_ADD(name))
{
    return m_driver->createRenderPipeline(
        params
        DBG_PARAM_ADD(name));
}

RenderPassHandle Renderer::createRenderPass(CreateRenderPassParams& params)
{
    // Create handle.
    auto handle = m_driver->createRenderPassHandle();

    // Submit creation command.
    auto* rc = allocCreationCmd<CreateRenderPassCommand>();
    rc->m_params = std::move(params);
    rc->m_params.m_renderPassHandle = handle;
    commitCreationCmd();

    return handle;
}

RenderTargetHandle Renderer::createRenderTarget(
    const CreateRenderTargetParams& params
    DBG_STR_PARAM_ADD(name))
{
    return m_driver->createRenderTarget(
        params
        DBG_PARAM_ADD(name));
}

ViewHandle Renderer::createView(
    const CreateViewParams& params
    DBG_STR_PARAM_ADD(name))
{
    return m_driver->createView(
        params
        DBG_PARAM_ADD(name));
}

UniqueBufferPtr Renderer::createQuadBuffer(
    const OE_Core::Rect2Df& extent)
{
    using Vertex = Vertex_XYZ_UV;

    const Vertex topLeft = {
        {extent.m_x, extent.m_y, .0f}, // XYZ
        {.0f, .0f}, // UV
    };

    const Vertex topRight = {
        {extent.m_x + extent.m_width, extent.m_y, .0f}, // XYZ
        {1.0f, .0f}, // UV
    };

    const Vertex bottomLeft = {
        {extent.m_x, extent.m_y + extent.m_height, .0f}, // XYZ
        {.0f, 1.0f}, // UV
    };

    const Vertex bottomRight = {
        {extent.m_x + extent.m_width, extent.m_y + extent.m_height, .0f}, // XYZ
        {1.0f, 1.0f}, // UV

    };

    const size_t nVertices = 6;
    const size_t vertexSize = sizeof(Vertex);
    const size_t bufferSize = nVertices * vertexSize;
    auto verticesPtr = MakeUnique<Buffer>(bufferSize);

    // Top Left triangle
    verticesPtr->write(topLeft);
    verticesPtr->write(topRight);
    verticesPtr->write(bottomLeft);

    // Bottom Right triangle
    verticesPtr->write(bottomLeft);
    verticesPtr->write(topRight);
    verticesPtr->write(bottomRight);

    return verticesPtr;
}

VertexBufferHandle Renderer::createQuad(
    const OE_Core::Rect2Df& extent
     DBG_STR_PARAM_ADD(name))
{
    auto verticesPtr = createQuadBuffer(extent);

    CreateVertexBufferParams params;
    params.m_buffer = std::move(verticesPtr);
    params.m_format = VertexDeclaration::XYZ_UV;
    params.m_usage = BufferUsage::Static;
    DBG_ONLY(params.m_name = name);

    return createVertexBuffer(params);
}


void Renderer::updateVertexBuffer(
    const VertexBufferHandle& handle,
    UniqueBufferPtr vertices)
{
    auto* rc = allocCreationCmd<UpdateVertexBufferCommand>();
    rc->m_handle = handle;
    rc->m_vertices = std::move(vertices);
    commitCreationCmd();
}

void Renderer::updateIndexBuffer(
    const IndexBufferHandle& handle,
    UniqueBufferPtr indicies)
{
    auto* rc = allocCreationCmd<UpdateIndexBufferCommand>();
    rc->m_handle = handle;
    rc->m_indices = std::move(indicies);
    commitCreationCmd();
}

void Renderer::applyMaterial(const PipelineHandle& handle)
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->setPipeline(handle);
}

void Renderer::updateInstance(
    const DrawInstanceHandle& handle,
    UniqueBufferPtr buffer)
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->updateInstance(handle, std::move(buffer));
}

void Renderer::drawInstance(const DrawInstanceHandle& handle)
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->drawInstance(handle);
}

void Renderer::updateView(ViewHandle& handle, UniqueBufferPtr viewUBO)
{
    OE_CHECK(!isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->updateView(handle, std::move(viewUBO));
}

void Renderer::updateMaterial(PipelineHandle& handle, UniqueBufferPtr viewUBO)
{
    OE_CHECK(!isRenderThread(), OE_CHANNEL_RENDERER);
    m_driver->updateMaterial(handle, std::move(viewUBO));
}

void Renderer::executeResourceCreationCmds()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);
    m_resourceCreationManager->execute();
}

void Renderer::resize(const ScreenSize& newSize)
{
    m_driver->resize(newSize);
    m_screenSize = newSize;
}

void Renderer::SDL_Attributes()
{
    m_driver->SDL_Attributes();
}

uint32 Renderer::SDL_GetWindowFlags()
{
    return m_driver->SDL_GetWindowFlags();
}

void Renderer::getShaderResourcePath(
    ShaderType shaderType,
    const String& resourceName,
    String& resourcePath)
{
    return m_driver->getShaderResourcePath(
        shaderType,
        resourceName,
        resourcePath);
}

void Renderer::getShaderResourcePath(
    String& resourcePath)
{
    return m_driver->getShaderResourcePath(resourcePath);
}

bool Renderer::isReady()
{
    return m_driver->isReady();
}

bool Renderer::isOk() const
{
    return m_driver->isOk();
}

double Renderer::getFrameTime() const
{
    return m_driver->getFrameTime();
}

void Renderer::waitUntilGPUIdle()
{
    return m_driver->waitUntilGPUIdle();
}

bool Renderer::isRenderThread() const
{
    return std::this_thread::get_id() == m_renderThreadId;
}

RenderScene* Renderer::getRenderScene() const
{
    uint32 iThreadScene = isRenderThread() ? m_iRenderThreadScene : m_iMainThreadScene;
    return m_scenes.m_pair[iThreadScene];
}

UBOFactory* Renderer::getUBOFactory() const
{
    return m_uboFactory;
}

void Renderer::createRenderScene()
{
    m_scenes.m_pair[0] = new RenderScene();
    m_scenes.m_pair[1] = new RenderScene();
    m_scenes.m_pair[m_iRenderThreadScene]->startRendering();
}

void Renderer::destroyRenderScene()
{
    delete m_scenes.m_pair[0];
    m_scenes.m_pair[0] = nullptr;
    delete m_scenes.m_pair[1];
    m_scenes.m_pair[1] = nullptr;
}

void Renderer::clearRenderScenes()
{
    m_scenes.m_pair[m_iMainThreadScene]->clear();
}

uint32 Renderer::getMainThreadRenderSceneIndex() const
{
    return m_iMainThreadScene;
}

void Renderer::publishScene()
{
    UniqueLock lock(m_scenesMutex);
    m_bSceneToRenderReady = true;
    lock.unlock();

    m_renderThreadNotifier.notify_one();
    waitForRenderThread();
}

void Renderer::flush()
{
    UniqueLock lock(m_scenesMutex);
    m_bFlush = true;
    lock.unlock();

    m_renderThreadNotifier.notify_one();
    waitForRenderThread();
}

void Renderer::waitForRenderThread()
{
    UniqueLock lock(m_scenesMutex);
    m_mainThreadNotifier.wait(
        lock,
        [this] {return !m_bSceneToRenderReady.load();});
}


bool Renderer::waitForMainThread()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);

    UniqueLock lock(m_scenesMutex);
    m_renderThreadNotifier.wait(lock, [this] {
        bool bWakeup = m_bSceneToRenderReady.load();
        bWakeup |= m_bFlush.load();

        return bWakeup;
    });

    bool bHasRenderingWork = false;

    if(m_bSceneToRenderReady)
    {
        flipScenes();
        bHasRenderingWork = true;
    }

    if(m_bFlush)
    {
        m_driver->flush();
    }

    m_bSceneToRenderReady = false;
    m_bFlush = false;

    lock.unlock();

    m_mainThreadNotifier.notify_one();

    return bHasRenderingWork;
}

void Renderer::flipScenes()
{
    OE_CHECK(isRenderThread(), OE_CHANNEL_RENDERER);

    uint32 tmp = m_iMainThreadScene;
    m_iMainThreadScene = m_iRenderThreadScene;
    m_iRenderThreadScene = tmp;

    RenderScene* renderedScene = m_scenes.m_pair[m_iMainThreadScene];
    RenderScene* sceneToBeRendered = m_scenes.m_pair[m_iRenderThreadScene];

    renderedScene->stopRendering();
    sceneToBeRendered->startRendering();

    // Copy changes from the scene ready to render
    // to the scene we are giving back to the main thread.
    renderedScene->copyFrom(*sceneToBeRendered);
}

bool Renderer::preRender()
{
    executeResourceCreationCmds();
    m_driver->preRender();
    return waitForMainThread();
}

void Renderer::render()
{
    beginFrame();

    iterateRenderPipeline([this](const RenderPassId& renderPassId){
        beginRenderPass(renderPassId.m_handle, renderPassId.m_iSubpass);

        RenderScene* renderScene = getRenderScene();
        renderScene->generateDriverCommands(renderPassId);

        endRenderPass();
    });

    endFrame();
}

void Renderer::postRender()
{
    m_driver->postRender();
}

}

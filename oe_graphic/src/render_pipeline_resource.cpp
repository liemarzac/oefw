#include "render_pipeline_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>

// OE_Graphic
#include "render_pass_resource.h"
#include "renderer.h"

// OE_Core
#include <task_manager.h>


namespace OE_Graphic
{

RTTI_IMPL_PARENT(RenderPipelineResource, Resource)

RenderPipelineResource::RenderPipelineResource(const String& path) :
    Resource(path)
{
}

RenderPipelineResource::~RenderPipelineResource()
{
}

const char* RenderPipelineResource::getType() const
{
    return "render_pipeline_resource";
}

void RenderPipelineResource::loadImpl()
{
    m_descriptorPtr = MakeShared<Descriptor>();
    m_loadTaskPtr = MakeShared<ResourceLoadTask<Descriptor>>(
        getName(),
        m_descriptorPtr,
        &RenderPipelineResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void RenderPipelineResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
    {
        if(m_loadTaskPtr->isFinished())
        {
            m_loadTaskPtr.reset();

            for(auto& renderPassNode : m_descriptorPtr->m_renderPassSequence)
            {
                renderPassNode.m_renderPass.m_resource = addDependency<RenderPassResource>(
                    dependencyCallback,
                    renderPassNode.m_renderPass.m_path);

                for(auto& waitForDependency : renderPassNode.m_waitForDependencies)
                {
                    waitForDependency.m_resource = addDependency<RenderPassResource>(
                        dependencyCallback,
                        waitForDependency.m_path);
                }
            }

            m_loadingState = LoadingState::WaitingForRenderPasses;
        }
        else
        {
            break;
        }
    }

    case LoadingState::WaitingForRenderPasses:
    {
        bool bAllRenderPassLoaded = true;
        for(auto& renderPassNode : m_descriptorPtr->m_renderPassSequence)
        {
            if(!renderPassNode.m_renderPass.m_resource->isLoaded())
            {
                bAllRenderPassLoaded = false;
                break;
            }

            for(auto& waitForDependency : renderPassNode.m_waitForDependencies)
            {
                if(!waitForDependency.m_resource->isLoaded())
                {
                    bAllRenderPassLoaded = false;
                    break;
                }
            }

            if(!bAllRenderPassLoaded)
            {
                break;
            }
        }

        if(bAllRenderPassLoaded)
        {
            m_loadingState = LoadingState::CreateRenderPipeline;
        }
        else
        {
            break;
        }
    }

    case LoadingState::CreateRenderPipeline:
    {
        CreateRenderPipelineParams params;
        for(auto& renderPassNode : m_descriptorPtr->m_renderPassSequence)
        {
            CreateRenderPipelineParams::RenderPassDescriptor desc;
            desc.m_renderPass = renderPassNode.m_renderPass.m_resource->getHandle();

            for(auto& waitForDependency : renderPassNode.m_waitForDependencies)
            {
                desc.m_waitFor.push_back(waitForDependency.m_resource->getHandle());
            }

            params.m_renderPassDescriptors.push_back(desc);
        }

        m_handle = Renderer::getInstance().createRenderPipeline(
            params
            DBG_PARAM_ADD(getName()));
    }

    case LoadingState::WaitingForRenderPipeline:
    {
        if(m_handle.isReady())
        {
            m_loadingState = LoadingState::Loaded;
        }
        else
        {
            break;
        }
    }

    case LoadingState::Loaded:
    {
        setState(State::Loaded);
    }
    }
}

void RenderPipelineResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)
{
    const auto& jsonRenderPassesArray = jsonContent["render_passes"];
    OE_CHECK(jsonRenderPassesArray.isArray(), OE_CHANNEL_RESOURCE);
    OE_CHECK(jsonRenderPassesArray.size() > 0, OE_CHANNEL_RESOURCE);
    for(auto& jsonRenderPass : jsonRenderPassesArray)
    {
        OE_CHECK(jsonRenderPass.isObject(), OE_CHANNEL_RESOURCE);
        Descriptor::RenderPassNode node;

        const Json::Value& renderPassPath = jsonRenderPass["path"];
        OE_CHECK(renderPassPath.isString(), OE_CHANNEL_RESOURCE);
        node.m_renderPass.m_path = renderPassPath.asString();

        const Json::Value& waitForArray = jsonRenderPass["wait_for"];
        if(waitForArray.isArray())
        {
            for(auto& jsonWaitForRenderPass : waitForArray)
            {
                OE_CHECK(jsonWaitForRenderPass.isString(), OE_CHANNEL_RESOURCE);

                Descriptor::RenderPassPath dependency;
                dependency.m_path = jsonWaitForRenderPass.asString();
                node.m_waitForDependencies.push_back(dependency);
            }
        }

        descriptor->m_renderPassSequence.push_back(node);
    }
}

} // namespace OE_Graphic

#include "ubo_factory.h"

#include "graphic_private.h"

// OE_Core
#include "hash.h"


namespace OE_Graphic
{

UBOFactory::UBOFactory()
{
}

UBOFactory::~UBOFactory()
{
}

UBO* UBOFactory::createUBO(const String& className)
{
    const uint32 classNameHash = OE_Core::hash(className);
    return m_factory.makeProduct(classNameHash, nullptr);
}


} // OE_Graphic


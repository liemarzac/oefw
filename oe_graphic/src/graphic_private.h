﻿#ifndef __OE_GRAPHIC_PRIVATE_H__
#define __OE_GRAPHIC_PRIVATE_H__

// External
#include <functional>
#include <SDL.h>
#include <oefw_config.h>

// OE_Core
#include <oe_string.h>
#include <ref_count.h>
#include <std_atomic_helpers.h>
#include <std_smartptr_helpers.h>

// OE_Graphic
#include "oe_graphic.h"

using namespace OE_Core;


namespace OE_Graphic
{

static size_t BindingTypeSizeTable[] =
{
    4,
    8,
    12,
    16,
    4,
    16,
    36,
    64
};

BindingType stringToBindingType(const String& str);
AttributeType stringToAttributeType(const String& str);

template<typename T>
struct BindingInfo
{
    T m_location;
    String m_name;
    ShaderType m_shaderType;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_PRIVATE_H__


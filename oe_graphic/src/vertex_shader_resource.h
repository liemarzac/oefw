#ifndef __OE_GRAPHIC_VERTEX_SHADER_RESOURCE_H__
#define __OE_GRAPHIC_VERTEX_SHADER_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>
#include <resource.h>
#include <runnable.h>
#include <std_mutex_helpers.h>

// OE_Graphics
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT VertexShaderResource : public OE_Core::Resource
{
friend class VertexShaderResourceLoadTask;
    RTTI_DECLARATION

public:
    VertexShaderResource(const String& name);
    virtual ~VertexShaderResource();

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline VertexShaderHandle getHandle() const
    {
        return m_handle;
    }

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "vertex_shader_resource";
    }

private:
    VertexShaderResource();
    VertexShaderResource& operator=(const VertexShaderResource& rhs);
    VertexShaderResource(VertexShaderResource& rhs);

    VertexShaderHandle m_handle;
    SharedPtr<class VertexShaderResourceLoadTask> m_loadTask;
};

class VertexShaderResourceLoadTask : public OE_Core::Runnable
{
public:
    VertexShaderResourceLoadTask(const String& resourceName);
    virtual void run() override;

    inline VertexShaderHandle getHandle() const
    {
        return m_handle;
    }

private:
    VertexShaderResourceLoadTask();
    VertexShaderResourceLoadTask& operator=(const VertexShaderResourceLoadTask& rhs);
    VertexShaderResourceLoadTask(VertexShaderResourceLoadTask& rhs);

    VertexShaderHandle m_handle;
    String m_resourceName;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VERTEX_SHADER_RESOURCE_H__

#ifndef __OE_GRAPHIC_UBO_H__
#define __OE_GRAPHIC_UBO_H__

#include "oe_graphic.h"

// OE_Core
#include <eigen_helpers.h>
#include <rtti.h>


namespace OE_Graphic
{

struct OE_GRAPHIC_EXPORT UBO
{
    RTTI_DECLARATION;

public:
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    virtual void* getData() = 0;
};


struct OE_GRAPHIC_EXPORT DefaultView_UBO : public UBO
{
    RTTI_DECLARATION;

public:
    virtual size_t getSize() const override
    {
        return sizeof(m_data);
    }

    virtual void* getData() override
    {
        return &m_data;
    }

    struct Data
    {
        Mat4 m_view;
        Mat4 m_projection;
        Mat4 m_viewProjection;
    }m_data;
};

struct OE_GRAPHIC_EXPORT ScreenView_UBO : public UBO
{
    RTTI_DECLARATION;

public:
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    virtual void* getData() override
    {
        return &m_data;
    }

    struct Data
    {
        Mat4 m_projection;
    }m_data;
};

struct OE_GRAPHIC_EXPORT Lighting_UBO : public UBO
{
    RTTI_DECLARATION;

public:
    virtual size_t getSize() const
    {
        return sizeof(*this);
    }

    virtual void* getData() 
    {
        return &m_data;
    }

    struct Data
    {
        struct Light {
            Vec4 position;
            Vec4 color;
            float radius;
            float quadraticFalloff;
            float linearFalloff;
            float _pad;
        };

        Light m_lights[17];
        Mat4 m_view;
        Vec4 m_viewPos;
    }m_data;
};

} // OE_Graphic

#endif // __OE_GRAPHIC_UBO_H__

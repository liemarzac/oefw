#ifndef __OE_GRAPHIC_MATERIAL_RESOURCE_H__
#define __OE_GRAPHIC_MATERIAL_RESOURCE_H__

#include "oe_graphic.h"

// External
#include <json/json.h>
#include <mutex>

// OE_Core
#include <array.h>
#include <resource.h>
#include <runnable.h>

// OE_Graphic
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

struct UBO;

class OE_GRAPHIC_EXPORT MaterialResource : public OE_Core::Resource
{
    friend class MaterialResourceLoadTask;
    RTTI_DECLARATION

public:
    struct Descriptor
    {
        struct ShaderBinding
        {
            String m_samplerPath;
            String m_uboClass;
            SamplerType m_samplerType = SamplerType::Unknown;
            BindingType m_bindingType = BindingType::Unknown;
            uint32 m_bindId = 0;
        };

        RenderStates m_renderStates;
        PrimitiveType m_topology = PrimitiveType::TriangleList;
        String m_vsPath;
        String m_psPath;
        String m_shaderBindingLayoutPath;
        String m_renderPassPath;
        String m_viewResourcePath;
        uint32 m_iSubpass = 0;
        Vector<AttributeType> m_inputs;
        Vector<ShaderBinding> m_shaderBindings;

        void addDefaultDiffuse()
        {
            ShaderBinding defaultDiffuse;
            defaultDiffuse.m_bindId = TextureBindingType::Diffuse;
            defaultDiffuse.m_bindingType = BindingType::Sampler;
            defaultDiffuse.m_samplerPath = "textures/dummy_diffuse.dds";
            defaultDiffuse.m_samplerType = SamplerType::Texture2D;
            m_shaderBindings.push_back(defaultDiffuse);
        }

        void addDefaultNormal()
        {
            ShaderBinding defaultNormal;
            defaultNormal.m_bindId = TextureBindingType::Normals;
            defaultNormal.m_bindingType = BindingType::Sampler;
            defaultNormal.m_samplerPath = "textures/dummy_normal.dds";
            defaultNormal.m_samplerType = SamplerType::Texture2D;
            m_shaderBindings.push_back(defaultNormal);
        }

        void addDefaultSpecular()
        {
            ShaderBinding defaultSpecular;
            defaultSpecular.m_bindId = TextureBindingType::Specular;
            defaultSpecular.m_bindingType = BindingType::Sampler;
            defaultSpecular.m_samplerPath = "textures/dummy_specular.dds";
            defaultSpecular.m_samplerType = SamplerType::Texture2D;
            m_shaderBindings.push_back(defaultSpecular);
        }
    };

    MaterialResource(const String& name);
    MaterialResource(
        const String& name,
        SharedPtr<Descriptor> descriptor);
    virtual ~MaterialResource();
    NO_COPY(MaterialResource);

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline ConstVertexShaderResPtr getVertexShaderResource() const
    {
        return sharedConstCast(m_vertexShaderResource);
    }

    inline ConstPixelShaderResPtr getPixelResource() const
    {
        return sharedConstCast(m_pixelShaderResource);
    }

    inline RenderPassResPtr getRenderPassResource() const
    {
        return m_renderPassResource;
    }

    inline const RenderPassId& getRenderPassId() const
    {
        return m_renderPassId;
    }

    inline const RenderStates& getRenderStates() const
    {
        return m_descriptor->m_renderStates;
    }

    inline PipelineHandle getPipelineHandle() const
    {
        return m_pipelineHandle;
    }

    inline PrimitiveType getTopology() const
    {
        return m_descriptor->m_topology;
    }

    TextureResPtr getTexture(uint32 id) const;

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "material_resource";
    }

    void stage();

    template<typename UBOType>
    UBOType* getUBO() const
    {
        UBOType* uboType = cast<UBOType>(m_ubo);
        return uboType;
    }

private:
    void loadDependencies(DependencyCallback dependencyCallback);
    bool updatePendingSamplers();

    struct Sampler
    {
        TextureResPtr m_texture;
        RenderTargetResPtr m_renderTarget;
        SamplerType m_type = SamplerType::Unknown;
        uint32 m_bindId = 0;
    };

    VertexShaderResPtr m_vertexShaderResource;
    PixelShaderResPtr m_pixelShaderResource;
    ShaderBindingResPtr m_shaderBindingResource;
    RenderPassResPtr m_renderPassResource;
    ViewResPtr m_viewResource;
    RenderPassId m_renderPassId;
    Vector<Sampler> m_samplers;
    PipelineHandle m_pipelineHandle;
    Vector<ShaderBind> m_shaderBindings;
    UBO* m_ubo = nullptr;

    enum class LoadingState
    {
        WaitingForDescriptor,
        DescriptorReady,
        WaitingForTextures,
        WaitingForShaders,
        WaitingForShaderBinding,
        WaitingForRenderPass,
        WaitingForView,
        CreatePipeline,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;

    SharedPtr<Descriptor> m_descriptor;
    SharedPtr<class MaterialResourceLoadTask> m_loadTaskPtr;
};

class MaterialResourceLoadTask : public OE_Core::Runnable
{
public:
    MaterialResourceLoadTask() = delete;
    MaterialResourceLoadTask(
        const String& resourcePath,
        SharedPtr<MaterialResource::Descriptor> descriptor);
    NO_COPY(MaterialResourceLoadTask);

    virtual void run() override;

private:
    String m_resourcePath;
    SharedPtr<MaterialResource::Descriptor> m_descriptor;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_MATERIAL_RESOURCE_H__

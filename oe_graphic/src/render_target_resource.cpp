#include "render_target_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>

// OE_Graphic
#include "renderer.h"

// OE_Core
#include <task_manager.h>

namespace OE_Graphic
{

RTTI_IMPL_PARENT(RenderTargetResource, Resource)

RenderTargetResource::RenderTargetResource(const String& path):
    Resource(path)
{
}

const char* RenderTargetResource::getType() const
{
    return "render_target_resource";
}

void RenderTargetResource::loadImpl()
{
    m_loadTaskPtr = MakeShared<ResourceLoadTask<Descriptor>>(
        getName(),
        MakeShared<Descriptor>(),
        &RenderTargetResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void RenderTargetResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
    {
        if(m_loadTaskPtr->isFinished())
        {
            const auto& descriptor = m_loadTaskPtr->getDescriptor();
            m_type = descriptor->m_type;
            m_format = descriptor->m_format;
            m_flags = descriptor->m_flags;

            m_loadTaskPtr.reset();
            m_loadingState = LoadingState::CreateRenderTarget;
        }
        else
        {
            break;
        }
    }

    case LoadingState::CreateRenderTarget:
    {
        CreateRenderTargetParams params;
        params.m_type = m_type;
        params.m_textureFormat = m_format;
        params.m_flags = m_flags;

        m_handle = Renderer::getInstance().createRenderTarget(
            params
            DBG_PARAM_ADD(getName()));

        m_loadingState = LoadingState::WaitingForRenderTarget;
    }

    case LoadingState::WaitingForRenderTarget:
    {
        if(m_handle.isReady())
        {
            m_loadingState = LoadingState::Loaded;
        }
        else
        {
            break;
        }
    }

    case LoadingState::Loaded:
    {
        setState(State::Loaded);
    }
    }

}

const RenderTargetHandle& RenderTargetResource::getHandle() const
{
    return m_handle;
}

void RenderTargetResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)
{
    // Type.
    {
        const Json::Value& jsonType = jsonContent["type"];
        OE_CHECK(jsonType.isString(), OE_CHANNEL_RESOURCE);
        String strType = jsonType.asString();
        if(strType == "color")
        {
            descriptor->m_type = RenderTargetType::Color;
        }
        else if(strType == "depth")
        {
            descriptor->m_type = RenderTargetType::Depth;
        }
        else if(strType == "backbuffer")
        {
            descriptor->m_type = RenderTargetType::Backbuffer;
        }
        else
        {
            OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown render target type %s", strType.c_str());
        }
    }
    
    // Format
    {
        const Json::Value& jsonFormat = jsonContent["format"];
        if(jsonFormat.isString())
        {
            String strFormat = jsonFormat.asString();
            if(strFormat == "R8G8B8A8_UNORM")
            {
                descriptor->m_format = TextureFormat::R8G8B8A8_UNORM;
            }
            else if(strFormat == "B8G8R8A8_UNORM")
            {
                descriptor->m_format = TextureFormat::B8G8R8A8_UNORM;
            }
            else if(strFormat == "R32G32B32A32_UINT")
            {
                descriptor->m_format = TextureFormat::R32G32B32A32_UINT;
            }
            else if(strFormat == "R32G32B32A32_SFLOAT")
            {
                descriptor->m_format = TextureFormat::R32G32B32A32_SFLOAT;
            }
            else
            {
                OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown render target format %s", strFormat.c_str());
            }
        }
    }

    // Flags.
    {
        const Json::Value& jsonFlags = jsonContent["subpass_input"];
        if(jsonFlags.isBool() && jsonFlags.asBool() == true)
        {
            descriptor->m_flags.set(RenderTargetFlags::SubpassInput);
        }
    }
}


} // namespace OE_Graphic

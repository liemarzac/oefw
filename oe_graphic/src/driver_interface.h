#ifndef __DRIVER_INTERFACE_H__
#define __DRIVER_INTERFACE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>

// OE_Graphic
#include "renderer_config.h"


namespace OE_Graphic
{

class DriverInterface
{
public:
    DriverInterface() = default;
    virtual ~DriverInterface() = default;

    virtual bool initFromMainThread(const RendererConfig& config) = 0;
    virtual bool initFromDriverThread(const RendererConfig& config) = 0;

    virtual void SDL_Attributes() = 0;
    virtual uint32 SDL_GetWindowFlags() const = 0;

    virtual bool isReady() const = 0;
    virtual bool isOk() const = 0;

    virtual void resize(const ScreenSize& newSize) = 0;

    virtual void getShaderResourcePath(
        ShaderType shaderType,
        const String& resourceName,
        String& resourcePath) const = 0;

    virtual void getShaderResourcePath(
        String& resourcePath) const = 0;

    virtual double getFrameTime() const = 0;

    virtual void waitUntilGPUIdle() = 0;
    virtual void flush() = 0;

    virtual void waitUntilReady() = 0;

    virtual void preRender() = 0;
    virtual void postRender() = 0;
    virtual void beginFrame() = 0;
    virtual void endFrame() = 0;
    virtual void beginRenderPass(RenderPassHandle handle, uint32 iSubpass) = 0;
    virtual void endRenderPass() = 0;
    virtual void iterateRenderPipeline(std::function<void(const RenderPassId&)> callback) = 0;

    virtual void setPipeline(const PipelineHandle& handle) = 0;
    virtual void updateInstance(const DrawInstanceHandle& handle, OE_Core::UniqueBufferPtr buffer) = 0;
    virtual void drawInstance(const DrawInstanceHandle& handle) = 0;
    virtual void updateView(ViewHandle& handle, OE_Core::UniqueBufferPtr viewUBO) = 0;
    virtual void updateMaterial(PipelineHandle& handle, OE_Core::UniqueBufferPtr materialUBO) = 0;
    virtual void flushInstance() = 0;
    virtual VertexBufferHandle createVertexBufferHandle() = 0;
    virtual VertexBufferHandle createVertexBuffer(const CreateVertexBufferParams& params) = 0;
    virtual IndexBufferHandle createIndexBufferHandle() = 0;
    virtual IndexBufferHandle createIndexBuffer(const CreateIndexBufferParams& params) = 0;
    virtual VertexShaderHandle createVertexShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual PixelShaderHandle createPixelShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual ShaderBindingHandle createShaderBinding(
        const ShaderBindingLayout& layout
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual RenderPipelineHandle createRenderPipeline(
        const CreateRenderPipelineParams& params
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual RenderPassHandle createRenderPassHandle() = 0;
    virtual RenderPassHandle createRenderPass(const CreateRenderPassParams& params) = 0;
    virtual RenderTargetHandle createRenderTarget(
        const CreateRenderTargetParams& params
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual PipelineHandle createPipeline(
        const CreatePipelineParams& params
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual DrawInstanceHandle createDrawInstance(
        const CreateDrawInstanceParams& params
        DBG_STR_PARAM_ADD(name)) = 0;
    virtual TextureHandle createTextureHandle() = 0;
    virtual TextureHandle createTexture(CreateTextureParams& params) = 0;
    virtual ViewHandle createView(
        const CreateViewParams& params
        DBG_STR_PARAM_ADD(name)) = 0;
};

} // namespace OE_Graphic

#endif // __DRIVER_INTERFACE_H__

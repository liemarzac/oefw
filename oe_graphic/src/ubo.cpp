#include "ubo.h"

#include "graphic_private.h"

namespace OE_Graphic
{

RTTI_IMPL(UBO)

// Views
RTTI_IMPL_PARENT(DefaultView_UBO, UBO)
RTTI_IMPL_PARENT(ScreenView_UBO, UBO)

// Materials
RTTI_IMPL_PARENT(Lighting_UBO, UBO)



} // OE_Graphic


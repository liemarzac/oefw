#ifndef __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__
#define __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <resource.h>
#include <resource_load_task.h>
#include <runnable.h>


namespace OE_Graphic
{

class SceneResource : public OE_Core::Resource
{
public:
    struct Descriptor
    {
    };

    SceneResource() = delete;
    SceneResource(const String& path);

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

private:
    SharedPtr<OE_Core::ResourceLoadTask<Descriptor>> m_loadTaskPtr;

    static void readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor);
};


} // namespace OE_Graphic

#endif // __OE_GRAPHIC_RENDER_PASS_RESOURCE_H__

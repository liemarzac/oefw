#include "vulkan_linear_allocator.h"

// OE_Core
#include <oe_math.h>
#include <memory.h>

// OE_Graphic
#include "../../graphic_private.h"

namespace OE_Graphic
{
namespace Vulkan
{

LinearAllocator::LinearAllocator(
    VkDevice device,
    VkDeviceSize targetSize,
    const VkMemoryAllocateInfo& memoryAllocateInfo,
    bool bMap) :
        Allocator(device, targetSize, memoryAllocateInfo)
{
    if(vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &m_deviceMemory) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Failed to allocate memory");
    }

    if(bMap)
    {
        vkMapMemory(device, m_deviceMemory, 0, VK_WHOLE_SIZE, 0, &m_mapped);
    }
}

LinearAllocator::~LinearAllocator()
{
    if(m_mapped)
    {
        vkUnmapMemory(m_device, m_deviceMemory);
    }

    vkFreeMemory(m_device, m_deviceMemory, nullptr);
}

bool LinearAllocator::alloc(VkDeviceSize size, Allocation& allocation)
{
    const VkDeviceSize alignedOffset = (VkDeviceSize)align(m_offset, 256);
    if(alignedOffset + size <= m_memAllocInfo.allocationSize)
    {
        allocation.m_offset = alignedOffset;
        allocation.m_allocatedSize = size;
        allocation.m_allocator = this;
        allocation.m_deviceMemory = m_deviceMemory;
        allocation.m_mapped = m_mapped;
        m_offset = alignedOffset + size;
    }
    else
    {
        if(!m_next)
        {
            VkMemoryAllocateInfo memAllocInfo = m_memAllocInfo;
            memAllocInfo.allocationSize = max(size, m_targetSize);

            LinearAllocator* newAllocator = new LinearAllocator(
                m_device,
                m_targetSize,
                memAllocInfo,
                m_mapped ? true : false);
            insert(newAllocator);

        }

        OE_CHECK(m_next, OE_CHANNEL_VULKAN);
        m_next->alloc(size, allocation);
    }

    return true;
}

void LinearAllocator::free(VkDeviceSize offset, VkDeviceSize size)
{
    // Intentionally do nothing
}

void LinearAllocator::reset()
{
    if(m_next)
    {
        m_next->reset();
    }

    m_offset = 0;

    if(m_prev)
    {
        delete this;
    }
}


} // namespace Vulkan
} // namespace OE_Graphic

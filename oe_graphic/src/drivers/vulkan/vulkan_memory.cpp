#include "vulkan_memory.h"

// OE_Core
#include "memory.h"

// Vulkan
#include "vulkan_heap_allocator.h"
#include "vulkan_linear_allocator.h"

namespace OE_Graphic
{
namespace Vulkan
{

AllocatorProxy::AllocatorProxy(Allocator* allocator, VkMemoryPropertyFlags propertyFlags) :
    m_allocator(allocator)
    ,m_memoryPropertyFlags(propertyFlags)
{
}

MemoryManager::~MemoryManager()
{
    shutdown();
}

void MemoryManager::init(VkDevice device, AllocatorType type)
{
    m_device = device;
    m_type = type;
}

void MemoryManager::shutdown()
{
    for(auto& proxy : m_proxies)
    {
        delete proxy.m_allocator;
    }

    m_proxies.clear();
}

bool MemoryManager::alloc(const AllocParams& params, Allocation& allocation)
{
    Allocator* foundAllocator = nullptr;
    for(auto& proxy : m_proxies)
    {
        if(proxy.m_memoryPropertyFlags == params.m_propertyFlags)
        {
            foundAllocator = proxy.m_allocator;
            break;
        }
    }

    if(!foundAllocator)
    {
        foundAllocator = createAllocator(params.m_allocInfo->memoryTypeIndex, params.m_bMap);
        m_proxies.emplace_back(foundAllocator, params.m_propertyFlags);
    }

    return foundAllocator->alloc(params.m_size, allocation);
}

void MemoryManager::reset()
{
    for(auto& proxy : m_proxies)
    {
        proxy.m_allocator->reset();
    }
}

Allocator* MemoryManager::createAllocator(uint32 memoryType, bool bMap)
{
    Allocator* allocator = nullptr;

    VkMemoryAllocateInfo allocateInfo = {};
    allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocateInfo.allocationSize = 10 * OE_Core::OneMb;
    allocateInfo.memoryTypeIndex = memoryType;

    switch(m_type)
    {
    case AllocatorType::Heap:
        allocator = new HeapAllocator(m_device, allocateInfo.allocationSize, allocateInfo, bMap);
        break;

    case AllocatorType::Linear:
        allocator = new LinearAllocator(m_device, allocateInfo.allocationSize, allocateInfo, bMap);
        break;

    default:
        OE_CHECK(false, OE_CHANNEL_VULKAN);
    }

    return allocator;
}

} // namespace Vulkan
} // namespace OE_Graphic
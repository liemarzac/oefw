#include "vulkan_heap_allocator.h"

// OE_Core
#include <oe_math.h>
#include <memory.h>

// OE_Graphic
#include "../../graphic_private.h"

namespace OE_Graphic
{
namespace Vulkan
{

#ifdef _DEBUG
    bool HeapAllocator::bCheckSanity = true;
    bool HeapAllocator::bCheckAllocations = true;
    #define CHECK_SANITY { if(bCheckSanity) { checkSanity(); } }
#else
    #define CHECK_SANITY {}
#endif

HeapAllocator::HeapAllocator(
    VkDevice device,
    VkDeviceSize targetSize,
    const VkMemoryAllocateInfo& memoryAllocateInfo,
    bool bMap) :
        Allocator(device, targetSize, memoryAllocateInfo)
{
    if(vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &m_deviceMemory) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Failed to allocate memory");
    }

    if(bMap)
    {
        vkMapMemory(device, m_deviceMemory, 0, VK_WHOLE_SIZE, 0, &m_mapped);
    }

    m_freeBlocks.emplace_back(0, memoryAllocateInfo.allocationSize, InvalidIndex, InvalidIndex);
    m_freeSize = memoryAllocateInfo.allocationSize;
    m_iFirstFreeBlock = 0;
}

HeapAllocator::~HeapAllocator()
{
    OE_CHECK_MSG(m_freeSize == m_memAllocInfo.allocationSize, OE_CHANNEL_VULKAN, "Allocator deleted with unfreed allocations");
    OE_CHECK_MSG(m_freeBlocks.size() == 1, OE_CHANNEL_VULKAN, "Only one free block should remain at deletion");

    if(m_mapped)
    {
        vkUnmapMemory(m_device, m_deviceMemory);
    }

    vkFreeMemory(m_device, m_deviceMemory, nullptr);
}

bool HeapAllocator::alloc(VkDeviceSize size, Allocation& allocation)
{
    DBG_ONLY(m_iAlloc++);

    bool bRet = false;

    for(uint32 iFree = 0; iFree < (uint32)m_freeBlocks.size(); ++iFree)
    {
        FreeBlock& b = m_freeBlocks[iFree];
        if(b.m_size >= size)
        {
            VkDeviceSize alignedSize = align((size_t)size, ms_alignment);
            if(alignedSize <= b.m_size)
            {
                allocation.m_allocator = this;
                allocation.m_offset = b.m_offset;
                allocation.m_allocatedSize = alignedSize;
                allocation.m_deviceMemory = m_deviceMemory;
                allocation.m_mapped = m_mapped;

#ifdef _DEBUG
                if(bCheckAllocations)
                {
                    static uint64 ms_allocId = 0;
                    AllocTag tag;
                    tag.m_allocId = ms_allocId;
                    tag.m_offset = b.m_offset;
                    m_tags.push_back(tag);
                    ++ms_allocId;
                }
#endif

                VkDeviceSize newOffset = b.m_offset + alignedSize;
                if(newOffset == b.m_offset + b.m_size)
                {
                    // Degenerated free block.
                    if(b.m_iPrev != InvalidIndex)
                    {
                        m_freeBlocks[b.m_iPrev].m_iNext = b.m_iNext;
                    }

                    if(b.m_iNext != InvalidIndex)
                    {
                        m_freeBlocks[b.m_iNext].m_iPrev = b.m_iPrev;
                    }

                    if(m_iFirstFreeBlock == iFree)
                    {
                        m_iFirstFreeBlock = b.m_iNext;
                    }

                    removeFreeBlock(iFree);
                }
                else
                {
                    b.m_offset = newOffset;
                    b.m_size -= alignedSize;
                }
            }

            m_freeSize -= alignedSize;
            bRet = true;
            break;
        }
    }

    if(!bRet)
    {
        if(!m_next)
        {
            VkMemoryAllocateInfo memAllocInfo = m_memAllocInfo;
            memAllocInfo.allocationSize = max(size, m_targetSize);
            HeapAllocator* newAllocator = new HeapAllocator(
                m_device,
                m_targetSize,
                memAllocInfo,
                m_mapped ? true : false);
            insert(newAllocator);
        }

        OE_CHECK(m_next, OE_CHANNEL_VULKAN);
        bRet = m_next->alloc(size, allocation);
    }

    CHECK_SANITY;
    OE_CHECK(bRet, OE_CHANNEL_VULKAN);
    return bRet;
}

void HeapAllocator::free(VkDeviceSize offset, VkDeviceSize size)
{
    DBG_ONLY(++m_iFree);

    // Find neighbors.
    uint32 iPrev = InvalidIndex;
    uint32 iNext = InvalidIndex;

    for(uint32 iFree = 0; iFree < (uint32)m_freeBlocks.size(); ++iFree)
    {
        FreeBlock& b = m_freeBlocks[iFree];
        if(b.m_offset > offset)
        {
            if(iNext != InvalidIndex)
            {
                if(b.m_offset < m_freeBlocks[iNext].m_offset)
                {
                    iNext = iFree;
                }
            }
            else
            {
                iNext = iFree;
            }
        }
        else
        {
            if(iPrev != InvalidIndex)
            {
                if(b.m_offset > m_freeBlocks[iPrev].m_offset)
                {
                    iPrev = iFree;
                }
            }
            else
            {
                iPrev = iFree;
            }
        }
    }

    bool bMerged = false;
    if(iPrev != InvalidIndex)
    {
        if(m_freeBlocks[iPrev].m_offset + m_freeBlocks[iPrev].m_size == offset)
        {
            // Merge by extending prev block size by the size of the block to be freed.
            m_freeBlocks[iPrev].m_size += size;
            bMerged = true;
        }
    }

    if(iNext != InvalidIndex)
    {
        if(offset + size == m_freeBlocks[iNext].m_offset)
        {
            if(bMerged)
            {
                // Already merged with the prev block so extend prev block
                // also to the next block.
                FreeBlock& next = m_freeBlocks[iNext];
                m_freeBlocks[iPrev].m_size += next.m_size;
                m_freeBlocks[iPrev].m_iNext = next.m_iNext;
                if(next.m_iNext != InvalidIndex)
                {
                    m_freeBlocks[next.m_iNext].m_iPrev = iPrev;
                }

                OE_CHECK(m_iFirstFreeBlock != iNext, OE_CHANNEL_VULKAN);
                removeFreeBlock(iNext);
            }
            else
            {
                m_freeBlocks[iNext].m_offset = offset;
                m_freeBlocks[iNext].m_size += size;
            }
            bMerged = true;
        }
    }

    if(!bMerged)
    {
        m_freeBlocks.emplace_back(offset, size, iPrev, iNext);
        uint32 iLast = (uint32)m_freeBlocks.size() - 1;
        if(iPrev != InvalidIndex)
            m_freeBlocks[iPrev].m_iNext = iLast;

        if(iNext != InvalidIndex)
            m_freeBlocks[iNext].m_iPrev = iLast;

        // Update the first free block index
        // when the new free block become the single free block
        // or the offset of the new free block is less than the currently
        // free block offset.
        if(m_iFirstFreeBlock == InvalidIndex ||
            offset < m_freeBlocks[m_iFirstFreeBlock].m_offset)
        {
            m_iFirstFreeBlock = iLast;
        }
    }

    m_freeSize += size;
    CHECK_SANITY;

#ifdef _DEBUG
    if(bCheckAllocations)
    {
        bool bFound = false;
        for(size_t iTag = 0; iTag < m_tags.size(); ++iTag)
        {
            if(m_tags[iTag].m_offset == offset)
            {
                bFound = true;
                removeAtUnordered(m_tags, iTag);
                break;
            }
        }
        OE_CHECK(bFound, OE_CHANNEL_VULKAN);
    }
#endif

    if(m_freeSize == m_memAllocInfo.allocationSize)
    {
        // We always want to keep the first allocator of the linked list.
        if(m_prev != nullptr)
        {
            // Free this allocation. The destructor will take care of the linked list.
            delete this;
        }
    }
}

void HeapAllocator::reset()
{
    // Does intentionally nothing.
}

void HeapAllocator::removeFreeBlock(uint32 i)
{
    OE_CHECK(i < (uint32)m_freeBlocks.size(), OE_CHANNEL_VULKAN);

    // removeAtUnordered will copy the last block at the removed block index.
    // So the prev and next block of the last block in the array
    // need to have their prev or next index corrected.
    uint32 iLast = (uint32)m_freeBlocks.size() - 1;
    if(i != iLast)
    {
        uint32 iLastPrev = m_freeBlocks[iLast].m_iPrev;
        if(iLastPrev != InvalidIndex)
        {
            m_freeBlocks[iLastPrev].m_iNext = i;
        }

        uint32 iLastNext = m_freeBlocks[iLast].m_iNext;
        if(iLastNext != InvalidIndex)
        {
            m_freeBlocks[iLastNext].m_iPrev = i;
        }

        // If the first block index is about to be moved to the removed block index
        // we need to update the first block index.
        if(iLast == m_iFirstFreeBlock)
        {
            m_iFirstFreeBlock = i;
        }
    }

    removeAtUnordered(m_freeBlocks, i);
}

void HeapAllocator::checkSanity() const
{
    Vector<uint32> tags(m_freeBlocks.size(), 0);
    VkDeviceSize calculatedFreeSize = 0;
    if(m_iFirstFreeBlock != InvalidIndex)
    {
        uint32 i = m_iFirstFreeBlock;
        while(i != InvalidIndex)
        {
            calculatedFreeSize += m_freeBlocks[i].m_size;

            // Check that blocks are only once in the linked list.
            tags[i]++;
            OE_CHECK(tags[i] == 1, OE_CHANNEL_VULKAN);

            // Check linked list sanity.
            if(m_freeBlocks[i].m_iPrev != InvalidIndex)
            {
                OE_CHECK(m_freeBlocks[i].m_iPrev < m_freeBlocks.size(), OE_CHANNEL_VULKAN);
                const FreeBlock& prev = m_freeBlocks[m_freeBlocks[i].m_iPrev];
                OE_CHECK(prev.m_iNext == i, OE_CHANNEL_VULKAN);
                OE_CHECK(prev.m_offset < m_freeBlocks[i].m_offset, OE_CHANNEL_VULKAN);
                OE_CHECK(prev.m_offset + prev.m_size < m_freeBlocks[i].m_offset, OE_CHANNEL_VULKAN);
            }

            if(m_freeBlocks[i].m_iNext != InvalidIndex)
            {
                OE_CHECK(m_freeBlocks[i].m_iNext < m_freeBlocks.size(), OE_CHANNEL_VULKAN);
                const FreeBlock& next = m_freeBlocks[m_freeBlocks[i].m_iNext];
                OE_CHECK(next.m_iPrev == i, OE_CHANNEL_VULKAN);
                OE_CHECK(m_freeBlocks[i].m_offset < next.m_offset, OE_CHANNEL_VULKAN);
                OE_CHECK(m_freeBlocks[i].m_offset + m_freeBlocks[i].m_size < next.m_offset, OE_CHANNEL_VULKAN);
            }

            i = m_freeBlocks[i].m_iNext;
        }
    }

    // Check that all blocks have been processed.
    for(uint32 tag : tags)
    {
        OE_CHECK(tag == 1, OE_CHANNEL_VULKAN);
    }

    OE_CHECK(calculatedFreeSize == m_freeSize, OE_CHANNEL_VULKAN);
}

} // namespace Vulkan
} // namespace OE_Graphic

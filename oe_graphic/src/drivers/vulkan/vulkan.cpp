#include "vulkan.h"

#include "../../graphic_private.h"

namespace OE_Graphic
{
namespace Vulkan
{

int findMemoryProperties(
    const VkPhysicalDeviceMemoryProperties& memoryProperties,
    uint32 memoryTypeBits,
    VkMemoryPropertyFlags requiredProperties)
{
    // Search memory types to find first index with those properties
    for(uint i = 0; i < memoryProperties.memoryTypeCount; i++)
    {
        if((memoryTypeBits & 1) == 1)
        {
            // Type is available, does it match user properties?
            if((memoryProperties.memoryTypes[i].propertyFlags & requiredProperties) == requiredProperties)
            {
                return i;
            }
        }
        memoryTypeBits >>= 1;
    }

    // Failed to find memory type
    return InvalidIndex;
}

VkFormat toVulkanFormat(TextureFormat format)
{
    switch(format)
    {
    case TextureFormat::R8G8B8A8_UNORM:
        return VK_FORMAT_R8G8B8A8_UNORM;

    case TextureFormat::R8G8B8_UNORM:
        return VK_FORMAT_R8G8B8_UNORM;

    case TextureFormat::B8G8R8A8_UNORM:
        return VK_FORMAT_B8G8R8A8_UNORM;

    case TextureFormat::R32G32B32A32_UINT:
        return VK_FORMAT_R32G32B32A32_UINT;

    case TextureFormat::R32G32B32A32_SFLOAT:
        return VK_FORMAT_R32G32B32A32_SFLOAT;

    case TextureFormat::RGBA_DXT3_UNORM_BLOCK16:
        return VK_FORMAT_BC3_UNORM_BLOCK;

    default:
        OE_CHECK(false, OE_CHANNEL_VULKAN);
    }

    return VK_FORMAT_R8G8B8A8_UNORM;
}

VkShaderStageFlags toVulkanStageFlags(FlagSet<ShaderType> flagset)
{
    VkShaderStageFlags stageFlags = 0;

    // Binding Stage
    if(flagset.isSet(ShaderType::VertexShader))
    {
        stageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
    }

    if(flagset.isSet(ShaderType::PixelShader))
    {
        stageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
    }

    return stageFlags;
}

} // Vulkan
} // OE_Graphic

#ifndef __OE_GRAPHIC_VULKAN_MEMORY_H__
#define __OE_GRAPHIC_VULKAN_MEMORY_H__

#include "../../oe_graphic.h"

// OE_Core
#include <vector.h>

// Vulkan
#include "vulkan_allocator.h"
#include "vulkan.h"

namespace OE_Graphic
{
namespace Vulkan
{

struct AllocatorProxy
{
    AllocatorProxy(Allocator* allocator, VkMemoryPropertyFlags propertyFlags);

    void alloc(
        VkDeviceSize size,
        VkDeviceSize alignment,
        Allocator** allocator,
        VkDeviceMemory* memory,
        VkDeviceSize* offset
    );

    Allocator* m_allocator = nullptr;
    VkMemoryPropertyFlags m_memoryPropertyFlags = 0;
};


class OE_GRAPHIC_EXPORT MemoryManager
{
public:
    enum AllocatorType
    {
        Heap,
        Linear,
    };

    MemoryManager() = default;
    ~MemoryManager();

    void init(VkDevice device, AllocatorType type);
    void shutdown();

    struct AllocParams
    {
        VkDeviceSize m_size = 0;
        VkDeviceSize m_alignment = 0;
        VkMemoryPropertyFlags m_propertyFlags = 0;
        VkMemoryAllocateInfo* m_allocInfo = nullptr;
        bool m_bMap = false;
    };

    bool alloc(const AllocParams& params, Allocation& allocation);

    void reset();

private:
    VkDevice m_device = VK_NULL_HANDLE;
    AllocatorType m_type;
    Vector<AllocatorProxy> m_proxies;

    Allocator* createAllocator(uint32 memoryType, bool bMap);
};

} // Vulkan
} // OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_HEAP_ALLOCATOR_H__
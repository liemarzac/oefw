#ifndef __OE_GRAPHIC_VULKAN_DRIVER_H__
#define __OE_GRAPHIC_VULKAN_DRIVER_H__

#include "../../oe_graphic.h"

// External
#include <condition_variable>
#include <mutex>
#include <vulkan/vulkan.h>

// OE_Core
#include <array.h>
#include <std_atomic_helpers.h>
#include <singleton.h>
#include <stop_watch.h>

// OE_Graphic
#include "../../driver_interface.h"

// Vulkan renderer
#include "vulkan.h"
#include "vulkan_buffer.h"
#include "vulkan_dynamic_buffer.h"
#include "vulkan_frame.h"
#include "vulkan_frame_buffer.h"
#include "vulkan_memory.h"
#include "vulkan_resources.h"

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
#define VK_USE_PLATFORM_WIN32_KHR
#endif

struct SDL_Window;

namespace OE_Graphic
{

namespace Vulkan
{
class HeapAllocator;

class Driver :
    public DriverInterface,
    public OE_Core::Singleton<Driver>
{
public:
    enum class AllocLifetime
    {
        Frame,
        Persistent
    };

    struct RenderingState
    {
        RenderPipelineHandle m_renderPipelineHandle;
        uint32 m_iImage = InvalidIndex;
        RenderPassHandle m_renderPassHandle;
        uint32 m_iRenderPass = 0;
        uint32 m_iSubpass = 0;
        VkDescriptorSet m_viewDescSet = VK_NULL_HANDLE;
        VkDescriptorSet m_materialDescSet = VK_NULL_HANDLE;
        ViewHandle m_viewHandle;
    };

    Driver();
    virtual ~Driver();

    virtual void SDL_Attributes() override
    {
    }

    virtual uint32 SDL_GetWindowFlags() const override;

    virtual bool initFromMainThread(const RendererConfig& config) override;
    virtual bool initFromDriverThread(const RendererConfig& config) override;

    virtual bool isReady() const override;
    virtual bool isOk() const override;

    virtual void resize(const ScreenSize& newSize) override;
    virtual void waitUntilGPUIdle() override;
    virtual void flush() override;

    virtual double getFrameTime() const override;

    // Render Thread Only functions
    virtual void waitUntilReady() override final;
    virtual void preRender() override final;
    virtual void postRender() override final;
    virtual void beginFrame() override;
    virtual void endFrame() override;
    virtual void beginRenderPass(RenderPassHandle handle, uint32 iSubpass) override;
    virtual void endRenderPass() override;
    virtual void iterateRenderPipeline(std::function<void(const RenderPassId&)> callback) override;
    virtual void setPipeline(const PipelineHandle& handle) override;
    virtual void updateInstance(const DrawInstanceHandle& handle, OE_Core::UniqueBufferPtr buffer) override;
    virtual void drawInstance(const DrawInstanceHandle& handle) override;
    virtual void updateView(ViewHandle& handle, OE_Core::UniqueBufferPtr viewUBO) override;
    virtual void updateMaterial(PipelineHandle& handle, OE_Core::UniqueBufferPtr materialUBO) override;
    virtual void flushInstance() override;
    virtual VertexBufferHandle createVertexBufferHandle() override final;
    virtual VertexBufferHandle createVertexBuffer(const CreateVertexBufferParams& params) override final;
    virtual IndexBufferHandle createIndexBufferHandle() override final;
    virtual IndexBufferHandle createIndexBuffer(const CreateIndexBufferParams& params) override final;
    virtual VertexShaderHandle createVertexShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name)) override final;
    virtual PixelShaderHandle createPixelShader(
        OE_Core::UniqueBufferPtr buffer
        DBG_STR_PARAM_ADD(name)) override final;
    virtual RenderPassHandle createRenderPassHandle() override final;
    virtual RenderPassHandle createRenderPass(const CreateRenderPassParams& params) override final;
    virtual RenderTargetHandle createRenderTarget(
        const CreateRenderTargetParams& params
        DBG_STR_PARAM_ADD(name)) override final;
    virtual ShaderBindingHandle createShaderBinding(
        const ShaderBindingLayout& layout
        DBG_STR_PARAM_ADD(name)) override final;
    virtual RenderPipelineHandle createRenderPipeline(
        const CreateRenderPipelineParams& params
        DBG_STR_PARAM_ADD(name)) override final;
    virtual PipelineHandle createPipeline(
        const CreatePipelineParams& params
        DBG_STR_PARAM_ADD(name)) override final;
    virtual DrawInstanceHandle createDrawInstance(
        const CreateDrawInstanceParams& params
        DBG_STR_PARAM_ADD(name)) override final;
    virtual TextureHandle createTextureHandle() override final;
    virtual TextureHandle createTexture(CreateTextureParams& params) override final;
    virtual ViewHandle createView(
        const CreateViewParams& params
        DBG_STR_PARAM_ADD(name)) override final;

    virtual void getShaderResourcePath(
        ShaderType shaderType,
        const String& resourceName,
        String& resourcePath) const final override;
    
    virtual void getShaderResourcePath(String& resourcePath) const final override;

    void addPendingDeleteResource(GraphicResource* resource);


    inline VkDevice getDevice() const
    {
        return m_device;
    }

    inline VkCommandBuffer getCurrentCommandBuffer()
    {
        return getCurrentFrame()->m_commandBuffer;
    }

    inline VkDescriptorPool getDescriptorPool() const
    {
        return m_descriptorPool;
    }

    inline uint32 getNumImages() const
    {
        return (uint32)m_frames.size();
    }

    inline uint32 getImageIndex() const
    {
        return m_state.m_iImage;
    }

    inline Vulkan::Frame& getFrame(uint32 iImage)
    {
        return m_frames[iImage];
    }

    inline RenderingState& getRenderingState()
    {
        return m_state;
    }

    Frame* getCurrentFrame();

    Vulkan::BufferHandle createBuffer(
        VkDeviceSize size,
        VkBufferUsageFlags usage,
        VkMemoryPropertyFlags properties,
        AllocLifetime timeline);

    Vulkan::DynamicBufferHandle createDynamicBuffer(VkDeviceSize size);

    void copyBuffer(
        Vulkan::BufferHandle srcBufferHandle,
        Vulkan::BufferHandle dstBufferHandle,
        VkDeviceSize size);

    void createTexture(
        uint32 width,
        uint32 height,
        VkFormat format,
        uint32 nMips,
        const Vector<size_t>& mipSizes,
        OE_Core::UniqueBufferPtr m_buffer,
        Texture& texture);

    VkSemaphore acquireSemaphore();
    void releaseSemaphore(VkSemaphore& semaphore);

private:
    enum class ReadyFlags
    {
        Swapchain,
        RenderPipeline,
        Count
    };

    struct SwapChainSupport
    {
        VkSurfaceCapabilitiesKHR m_capabilities;
        Vector<VkSurfaceFormatKHR> m_formats;
        Vector<VkPresentModeKHR> m_presentModes;
    }m_swapChainSupport;

    static bool checkValidationLayerSupport();
    static bool checkInstanceExtensionsSupport();
    static Vulkan::DeviceExtFlags checkPhysicalDeviceExtensionsSupport(VkPhysicalDevice device);
    static int findQueue(VkPhysicalDevice device, VkQueueFlagBits queue);

    bool createVulkanInstance();
    VkPhysicalDevice pickPhysicalDevice() const;
    VkPresentModeKHR pickPresentMode(const Vector<VkPresentModeKHR>& presentModes) const;
    VkFormat findSupportedFormat(
        const Vector<VkFormat>& candidates,
        VkImageTiling tiling,
        VkFormatFeatureFlags features) const;
    VkFormat findDepthFormat() const;
    static bool hasStencilComponent(VkFormat format);

    bool createLogicalDevice();
    bool createSurface();
    void createFrames();
    void destroyFrames();
    bool createSwapChain(ScreenSize size);
    void destroySwapChain();
    bool createCommandPool();
    void destroySemaphores();
    bool createGlobalResources();
    void destroyGlobalResources();
    bool createDescriptorPool();
    void createAttachment(
        VkFormat format,
        VkImageUsageFlagBits usage,
        Vulkan::FrameBufferAttachment& attachment,
        uint32 width,
        uint32 height);
    int findBestDevice(const Vector<VkPhysicalDevice>& devices) const;
    bool isPhysicalDeviceSuitable(VkPhysicalDevice device) const;
    void querySwapChainSupport(SwapChainSupport& support, VkPhysicalDevice device) const;
    uint32 queryImageCount() const;
    void handleReady(ReadyFlags flag);
    void cleanPendingDeleteResources();


    struct AllocParams
    {
        VkDeviceSize m_size = 0;
        VkDeviceSize m_alignment = 0;
        VkMemoryPropertyFlags m_propertyFlags = 0;
        VkMemoryAllocateInfo* m_allocInfo = nullptr;
        AllocLifetime m_timeline = AllocLifetime::Persistent;
        bool m_bMap = false;
    };

    bool alloc(const AllocParams& params, Vulkan::Allocation& allocation);

#ifdef _DEBUG
    void setupDebugMessenger();
    VkResult createDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo);
    void destroyDebugUtilsMessengerEXT();
#endif

    static const Vector<const char*> ms_validationLayers;
    static const Vector<const char*> ms_instanceExtensions;
    static const Vector<const char*> ms_deviceExtensions;
    static const VkFormat ms_surfaceFormat;
    static const VkColorSpaceKHR ms_surfaceColorSpace;

    VkInstance m_instance = VK_NULL_HANDLE;
    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceMemoryProperties m_memoryProperties;
    VkDevice m_device = VK_NULL_HANDLE;
    VkPhysicalDeviceFeatures m_deviceFeatures = {};
    VkPhysicalDeviceProperties m_deviceProperties = {};
    VkQueue m_graphicsQueue = VK_NULL_HANDLE;;
    VkSurfaceKHR m_surface = VK_NULL_HANDLE;
    VkRenderPass m_compositionRenderPass = VK_NULL_HANDLE;
    VkSwapchainKHR m_swapChain = VK_NULL_HANDLE;
    VkCommandPool m_commandPool = VK_NULL_HANDLE;
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;
    VkCommandBuffer m_copyCommandBuffer = VK_NULL_HANDLE;
    Vector<Vulkan::Frame> m_frames;
    Vector<VkSemaphore> m_semaphorePool;
    Vulkan::MemoryManager m_heapAllocators;
    Vulkan::DepthBuffer m_depthBuffer;
    VkCommandBuffer m_deferredCommandBuffer = VK_NULL_HANDLE;
    VkSampler m_colorAttachmentSampler = VK_NULL_HANDLE;
    Vector<VkImageView> m_swapchainImageViews;

    // Only accessed by the command thread.
    RenderingState m_state;

    AtomicBool m_isReady;
    AtomicBool m_isOk;
    ScreenSize m_screenSize_MainThread;
    ScreenSize m_screenSize_RenderThread;

    OE_Core::StopWatch m_frameStopWatch;
    AtomicDouble m_frameTime;
    SDL_Window* m_window = nullptr;

    Vector<GraphicResource*> m_pendingDeleteResources;
    Mutex m_pendingDeleteResourcesMutex;

    enum class VulkanFlags
    {
        EnableValidationLayers,
        VSync
    };
    OE_Core::FlagSet<VulkanFlags> m_flags;

    Vulkan::DeviceExtFlags m_deviceExtensionFlags;
    std::condition_variable m_readyCondition;
    std::mutex m_readyMutex;
    OE_Core::FlagSet<ReadyFlags> m_readyFlags;
    bool m_bReady = false;
    
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    HWND m_activeWindowhandle;
#endif

    bool m_bHasWaitedForBackbuffer = false;

#ifdef _DEBUG
    VkDebugUtilsMessengerEXT m_debugMessenger;
#endif
};


} // namespace Vulkan

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_DRIVER_H__

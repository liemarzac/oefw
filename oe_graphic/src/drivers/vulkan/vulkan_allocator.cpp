#include "vulkan_heap_allocator.h"

// OE_Core
#include <memory.h>

// OE_Graphic
#include "../../graphic_private.h"

namespace OE_Graphic
{
namespace Vulkan
{

Allocator::Allocator(
    VkDevice device,
    VkDeviceSize targetSize,
    const VkMemoryAllocateInfo& memoryAllocateInfo) :
        m_device(device)
        ,m_targetSize(targetSize)
        ,m_memAllocInfo(memoryAllocateInfo)
{
}

Allocator::~Allocator()
{
    if(m_prev)
    {
        m_prev->m_next = m_next;
    }

    if(m_next)
    {
        m_next->m_prev = m_prev;
    }
}

void Allocator::insert(Allocator* newAllocator)
{
    if(m_next)
    {
        m_next->m_prev = newAllocator;
    }

    newAllocator->m_prev = this;
    newAllocator->m_next = m_next;

    m_next = newAllocator;
}

} // namespace Vulkan
} // namespace OE_Graphic

#include "vulkan_buffer.h"

// External
#include <cstring>

// Vulkan
#include "vulkan_allocator.h"

#define OE_CHANNEL_VULKAN_BUFFER "VulkanBuffer"

namespace OE_Graphic
{
namespace Vulkan
{

Buffer::~Buffer()
{
    destroy();
}

VkResult Buffer::bind(VkDeviceSize offset)
{
    m_memoryOffset = offset;
    return vkBindBufferMemory(m_device, m_buffer, m_memory, offset);
}

void Buffer::setupDescriptor(VkDeviceSize offset, VkDeviceSize size)
{
    m_desc.offset = offset;
    m_desc.buffer = m_buffer;
    m_desc.range = size;
}

void Buffer::copyTo(const void* data, VkDeviceSize size)
{
    OE_CHECK(m_mapped, OE_CHANNEL_VULKAN_BUFFER);
    OE_CHECK(size <= m_size, OE_CHANNEL_VULKAN_BUFFER);
    byte* dst = ((byte*)m_mapped) + m_memoryOffset;
    memcpy(dst, data, size);
}

VkResult Buffer::flush(VkDeviceSize size, VkDeviceSize offset)
{
    VkMappedMemoryRange mappedRange = {};
    mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mappedRange.memory = m_memory;
    mappedRange.offset = offset;
    mappedRange.size = size;
    return vkFlushMappedMemoryRanges(m_device, 1, &mappedRange);
}

VkResult Buffer::invalidate(VkDeviceSize size, VkDeviceSize offset)
{
    VkMappedMemoryRange mappedRange = {};
    mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mappedRange.memory = m_memory;
    mappedRange.offset = offset;
    mappedRange.size = size;
    return vkInvalidateMappedMemoryRanges(m_device, 1, &mappedRange);
}

void Buffer::destroy()
{
    if(m_allocator)
    {
        m_allocator->free(m_memoryOffset, m_size);
    }

    if(m_buffer)
    {
        vkDestroyBuffer(m_device, m_buffer, nullptr);
    }
}

} // namespace Vulkan
} // namespace OE_Graphic

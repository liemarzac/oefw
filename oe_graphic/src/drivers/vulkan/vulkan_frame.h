#ifndef __OE_GRAPHIC_VULKAN_FRAME_H__
#define __OE_GRAPHIC_VULKAN_FRAME_H__

#include "../../oe_graphic.h"

// External
#include <vulkan/vulkan.h>

// OE_Core
#include <array.h>
#include <buffer.h>
#include <std_smartptr_helpers.h>
#include <vector.h>

// Vulkan
#include "vulkan_buffer.h"
#include "vulkan_dynamic_buffer.h"
#include "vulkan_frame_buffer.h"
#include "vulkan_memory.h"


namespace OE_Graphic
{
namespace Vulkan
{

class LinearAllocator;

class Frame
{
public:
    struct AllocatorProxy
    {
        void alloc(
            VkDeviceSize size,
            VkDeviceSize alignment,
            Allocator** allocator,
            VkDeviceMemory* memory,
            VkDeviceSize* offset
        );

        Allocator* m_allocator = nullptr;
        uint32 m_memoryType = 0;
    };

    Vector<GraphicResource*> m_pendingDeleteResources;
    VkDevice m_device = VK_NULL_HANDLE;
    VkQueue m_graphicsQueue = VK_NULL_HANDLE;
    VkCommandBuffer m_commandBuffer = VK_NULL_HANDLE;
    VkSemaphore m_imageAvailSemaphore = VK_NULL_HANDLE;
    VkSemaphore m_commandQueueFishinshed = VK_NULL_HANDLE;
    VkFence m_renderQueueFinished = VK_NULL_HANDLE;
    VkPhysicalDeviceProperties* m_deviceProperties = nullptr;
    VkFramebuffer m_compositionFrameBuffer = VK_NULL_HANDLE;
    VkRenderPass m_compostionRenderPass = VK_NULL_HANDLE;
    VkExtent2D m_compositionExtent = {};

    MemoryManager m_memoryManager;
    uint32 m_iImage = InvalidIndex;
    uint32 m_iView = InvalidIndex;

    DynamicBufferHandle m_instanceBufferHandle;

    Frame(VkDevice device);
    ~Frame();

    void beginFrame();
    void endFrame();

    void beginRenderPass(RenderPassHandle handle, uint32 iSubpass);
    void endRenderPass();

    void waitUntilCompleted();
    void cleanPendingDeleteResources();

    struct AllocParams
    {
        VkDeviceSize m_size = 0;
        VkDeviceSize m_alignment = 0;
        VkMemoryPropertyFlags m_propertyFlags = 0;
        VkMemoryAllocateInfo* m_allocInfo = nullptr;
        bool m_bMap = false;
    };

    bool alloc(const AllocParams& params, Allocation& allocation);

    // PROTOTYPE
    VkDescriptorSetLayout getInstanceDescriptorSetLayout() const
    {
        return m_instanceBufferHandle.getDynamicBuffer()->getDescriptorLayout();
    }

    void updateDynamic(UniquePtr<OE_Core::Buffer> buffer, VkDescriptorSet& descSet, VkDeviceSize& offset);
    void flushInstances();

};

} // namespace Vulkan
} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_FRAME_H__

#ifndef __OE_GRAPHIC_VULKAN_DYNAMIC_BUFFER_H__
#define __OE_GRAPHIC_VULKAN_DYNAMIC_BUFFER_H__

#include "../../oe_graphic.h"

// OE_Core
#include <vector.h>

// OE_Graphic
#include "vulkan.h"


namespace OE_Graphic
{
namespace Vulkan
{

class DynamicBuffer : public GraphicResource
{
public:
    struct CreateInfo
    {
        VkDevice m_device = VK_NULL_HANDLE;
        VkDeviceSize m_size = 0;
        VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;
        uint32 m_iBinding = InvalidIndex;
        VkDeviceSize m_alignment = InvalidIndex;
        VkPhysicalDeviceProperties* m_deviceProperties = nullptr;
        VkPhysicalDeviceMemoryProperties* m_memoryProperties = nullptr;
    };

    DynamicBuffer(const CreateInfo& createInfo);
    ~DynamicBuffer();

    void write(const byte* buffer, VkDeviceSize size, VkDescriptorSet& descSet, VkDeviceSize& offset);
    void flush();

    // PROTOTYPE
    VkDescriptorSetLayout getDescriptorLayout() const
    {
        return m_descSetLayout;
    }

private:
    VkDevice m_device = VK_NULL_HANDLE;
    VkDeviceMemory m_deviceMemory = VK_NULL_HANDLE;
    VkDeviceSize m_size = 0;
    Vector<VkBuffer> m_buffers;
    Vector<VkDescriptorSet> m_descSets;
    VkDescriptorPool m_descPool = VK_NULL_HANDLE;
    byte* m_mapped = nullptr;
    VkDeviceSize m_writeOffset = 0;
    VkDescriptorSetLayout m_descSetLayout = VK_NULL_HANDLE;
    VkDeviceSize m_alignment = 0;
    VkDeviceSize m_bufferSize = 0;
};


struct DynamicBufferHandle : GraphicHandle
{
    static DynamicBufferHandle create(const DynamicBuffer::CreateInfo& createInfo)
    {
        DynamicBufferHandle handle;
        handle.m_resource = new DynamicBuffer(createInfo);
        return handle;
    }

    DynamicBuffer* getDynamicBuffer() const
    {
        return static_cast<DynamicBuffer*>(*m_resource);
    }
};

} // namespace Vulkan
} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_DYNAMIC_BUFFER_H__

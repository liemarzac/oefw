#include "vulkan.h"
#include "vulkan_resources.h"


namespace OE_Graphic
{

void RenderPass::setupFramebuffers(const SetupFrameBuffersParams& params)
{
    OE_CHECK(params.m_screenSize.m_width > 0, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_screenSize.m_height > 0, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_iFrame < m_framebuffers.size(), OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_swapChain != VK_NULL_HANDLE, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_swapchainImageViews.size() > 0, OE_CHANNEL_VULKAN);

    if (m_screenSize == params.m_screenSize)
    {
        // Frame buffers are already setup as required.
        //return;
    }

    // Set parameters for the images used by the render targets used by the framebuffers.
    RenderTarget::SetupImagesParams setupImagesParams;
    setupImagesParams.m_screenSize = params.m_screenSize;
    setupImagesParams.m_memoryProperties = params.m_memoryProperties;
    setupImagesParams.m_swapChain = params.m_swapChain;
    setupImagesParams.m_swapChainImageViews = params.m_swapchainImageViews;
    setupImagesParams.m_bNVidiaDedicatedAlloc = params.m_bNVidiaDedicatedAlloc;

    // Setup images associated with the render targets used by the framebuffer.
    for (auto& rtHandle : m_renderTargets)
    {
        RenderTarget* rt = getResource(rtHandle);
        rt->setupImages(setupImagesParams);
    }

    if (m_framebuffers[params.m_iFrame] != VK_NULL_HANDLE)
    {
        vkDestroyFramebuffer(m_device, m_framebuffers[params.m_iFrame], nullptr);
    }

    // Create frame buffers.
    const uint32 nAttachments = static_cast<uint32>(m_renderTargets.size());

    Vector<VkImageView> fbAttachements(nAttachments);

    for (uint32 i = 0; i < nAttachments; ++i)
    {
        if (i == m_iBackbufferAttachment)
        {
            SwapchainRenderTarget* swapchainRT = static_cast<SwapchainRenderTarget*>(
                getResource(m_renderTargets[m_iBackbufferAttachment]));

            fbAttachements[i] = swapchainRT->m_imageViews[params.m_iFrame];
        }
        else
        {
            DefaultRenderTarget* defaultRT = static_cast<DefaultRenderTarget*>(
                getResource(m_renderTargets[i]));

            fbAttachements[i] = defaultRT->m_imageView;
        }
    }

    VkFramebufferCreateInfo fbCreateInfo = {};
    fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fbCreateInfo.renderPass = m_renderPass;
    fbCreateInfo.attachmentCount = nAttachments;
    fbCreateInfo.pAttachments = fbAttachements.data();
    fbCreateInfo.width = params.m_screenSize.m_width;
    fbCreateInfo.height = params.m_screenSize.m_height;
    fbCreateInfo.layers = 1;

    OE_VULKAN_CHECK(vkCreateFramebuffer(
        m_device,
        &fbCreateInfo,
        nullptr,
        &m_framebuffers[params.m_iFrame]));

    m_screenSize = params.m_screenSize;
}

void DefaultRenderTarget::setupImages(const SetupImagesParams& params)
{
    OE_CHECK(params.m_screenSize.m_width > 0, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_screenSize.m_height > 0, OE_CHANNEL_VULKAN);

    if (m_screenSize == params.m_screenSize)
    {
        // The image is already setup as required.
        return;
    }

    if(m_imageView != VK_NULL_HANDLE )
        vkDestroyImageView(m_device, m_imageView, nullptr);

    if(m_memory != VK_NULL_HANDLE)
        vkFreeMemory(m_device, m_memory, nullptr);

    if(m_image != VK_NULL_HANDLE)
        vkDestroyImage(m_device, m_image, nullptr);

    // A default render target needs to create
    // an image object, an image view object and memory allocation to fit
    // the image data.

    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.format = m_format;
    imageCreateInfo.extent = { params.m_screenSize.m_width, params.m_screenSize.m_height, 1 };
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCreateInfo.usage = m_usage | VK_IMAGE_USAGE_SAMPLED_BIT;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VkDedicatedAllocationImageCreateInfoNV dedicatedImageInfo{ VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV };
    if (params.m_bNVidiaDedicatedAlloc)
    {
        dedicatedImageInfo.dedicatedAllocation = VK_TRUE;
        imageCreateInfo.pNext = &dedicatedImageInfo;
    }

    OE_VULKAN_CHECK(vkCreateImage(
        m_device,
        &imageCreateInfo,
        nullptr,
        &m_image));

    VkMemoryRequirements memReqs;
    vkGetImageMemoryRequirements(
        m_device,
        m_image,
        &memReqs);

    VkMemoryAllocateInfo memAllocInfo = {};
    memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memAllocInfo.allocationSize = memReqs.size;
    memAllocInfo.memoryTypeIndex = Vulkan::findMemoryProperties(
        params.m_memoryProperties,
        memReqs.memoryTypeBits,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkDedicatedAllocationMemoryAllocateInfoNV dedicatedAllocationInfo{ VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV };
    if (params.m_bNVidiaDedicatedAlloc)
    {
        dedicatedAllocationInfo.image = m_image;
        memAllocInfo.pNext = &dedicatedAllocationInfo;
    }

    OE_VULKAN_CHECK(vkAllocateMemory(
        m_device,
        &memAllocInfo,
        nullptr,
        &m_memory));

    OE_VULKAN_CHECK(vkBindImageMemory(
        m_device,
        m_image,
        m_memory,
        0));

    VkImageViewCreateInfo imageViewCreateInfo = {};
    imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewCreateInfo.format = m_format;
    imageViewCreateInfo.subresourceRange = {};
    imageViewCreateInfo.subresourceRange.aspectMask = m_aspectMask;
    imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    imageViewCreateInfo.subresourceRange.levelCount = 1;
    imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    imageViewCreateInfo.subresourceRange.layerCount = 1;
    imageViewCreateInfo.image = m_image;

    OE_VULKAN_CHECK(vkCreateImageView(
        m_device,
        &imageViewCreateInfo,
        nullptr,
        &m_imageView));

    m_screenSize = params.m_screenSize;
}

void SwapchainRenderTarget::setupImages(const SetupImagesParams& params)
{
    OE_CHECK(params.m_screenSize.m_width > 0, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_screenSize.m_height > 0, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_swapChain != VK_NULL_HANDLE, OE_CHANNEL_VULKAN);
    OE_CHECK(params.m_swapChainImageViews.size() > 0, OE_CHANNEL_VULKAN);

    if (m_screenSize == params.m_screenSize)
    {
        // The swapchain images are already setup as required.
        return;
    }

    m_imageViews.clear();

    // The swapchain has already created the images.
    // so the render target only need to create the image views.
    uint32 nImages = static_cast<uint32>(params.m_swapChainImageViews.size());

    Vector<VkImage> swapchainImages(nImages);

    OE_VULKAN_CHECK(vkGetSwapchainImagesKHR(
        m_device,
        params.m_swapChain,
        &nImages,
        swapchainImages.data()));

    // Copy image views.
    m_imageViews = params.m_swapChainImageViews;

    m_screenSize = params.m_screenSize;
}

void Pipeline::setupWriteDescriptorSet(const SetupWriteDescriptorSet& params)
{
    OE_CHECK(params.m_device != VK_NULL_HANDLE, OE_CHANNEL_VULKAN);

    // Create write desc sets.
    const size_t nWriteDescSet = m_shaderBindings.size();

    Vector<VkWriteDescriptorSet> writeDescSets;
    writeDescSets.resize(nWriteDescSet);

    // For allocation purpose, assume there will be an image info per write desc set.
    Vector<VkDescriptorImageInfo> imageInfos;
    imageInfos.resize(nWriteDescSet);

    size_t iWriteDescSet = 0;

    // Bind UBO and samplers to descriptor set.
    for(auto& shaderBinding : m_shaderBindings)
    {
        VkWriteDescriptorSet& writeDescSet = writeDescSets[iWriteDescSet];
        VkDescriptorImageInfo& imageInfo = imageInfos[iWriteDescSet];
        writeDescSet = {};
        imageInfo = {};

        switch(shaderBinding.m_bindType)
        {
        case BindingType::Uniform:
        {
            writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            writeDescSet.descriptorCount = 1;
            writeDescSet.dstArrayElement = 0;
            writeDescSet.dstBinding = shaderBinding.m_bindId;
            writeDescSet.dstSet = m_descSets[params.m_iFrame];

            Vulkan::BufferHandle materialUBOHandle = m_materialUBOs[params.m_iFrame];
            writeDescSet.pBufferInfo = &materialUBOHandle.getBuffer()->m_desc;
        }
        break;

        case BindingType::Sampler:
        case BindingType::InputAttachment:
        {
            writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;

            if (shaderBinding.m_bindType == BindingType::Sampler)
            {
                writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            }
            else if(shaderBinding.m_bindType == BindingType::InputAttachment)
            {
                writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
            }

            writeDescSet.descriptorCount = 1;
            writeDescSet.dstArrayElement = 0;
            writeDescSet.dstBinding = shaderBinding.m_bindId;
            writeDescSet.dstSet = m_descSets[params.m_iFrame];

            switch(shaderBinding.m_samplerType)
            {
            case SamplerType::Texture2D:
            {
                imageInfo = getResource(shaderBinding.m_textureHandle)->m_descImageInfo;
                imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            }
            break;

            case SamplerType::RenderTarget:
            {
                RenderTarget* renderTarget = getResource(shaderBinding.m_renderTargetHandle);

                switch(renderTarget->m_type)
                {
                case RenderTargetType::Color:
                {
                    DefaultRenderTarget* defaultRenderTarget = static_cast<DefaultRenderTarget*>(renderTarget);
                    imageInfo.imageView = defaultRenderTarget->m_imageView;
                    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                }
                break;

                case RenderTargetType::Depth:
                {
                    DefaultRenderTarget* defaultRenderTarget = static_cast<DefaultRenderTarget*>(renderTarget);
                    imageInfo.imageView = defaultRenderTarget->m_imageView;
                    imageInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL;
                }
                break;

                default:
                OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Unsupported render target type");
                }

                // TODO: The sampler should be a parameter of the pipeline creation.
                OE_CHECK(params.m_colorAttachmentSampler != VK_NULL_HANDLE, OE_CHANNEL_VULKAN);
                imageInfo.sampler = params.m_colorAttachmentSampler;
            }
            break;

            default:
            OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Unsupported sampler type");
            }

            writeDescSet.pImageInfo = &imageInfo;
        }
        break;

        default:
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Unknown binding type");
        }

        ++iWriteDescSet;
    }

    vkUpdateDescriptorSets(
        params.m_device,
        (uint32)writeDescSets.size(),
        writeDescSets.data(),
        0,
        nullptr);
}

}

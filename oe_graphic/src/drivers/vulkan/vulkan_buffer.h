#ifndef __OE_GRAPHIC_VULKAN_BUFFER_H__
#define __OE_GRAPHIC_VULKAN_BUFFER_H__

#include "../../oe_graphic.h"

// External
#include <vulkan/vulkan.h>

namespace OE_Graphic
{
namespace Vulkan
{

class Allocator;

struct Buffer : public GraphicResource
{
    VkDevice m_device;
    VkBuffer m_buffer = VK_NULL_HANDLE;
    VkDeviceMemory m_memory = VK_NULL_HANDLE;
    VkDescriptorBufferInfo m_desc;
    VkDeviceSize m_size = 0;
    VkDeviceSize m_memoryOffset = 0;
    VkDeviceSize m_alignment = 0;
    void* m_mapped = nullptr;
    Allocator* m_allocator = nullptr;

    /** @brief Usage flags to be filled by external source at buffer creation (to query at some later point) */
    VkBufferUsageFlags usageFlags;
    /** @brief Memory propertys flags to be filled by external source at buffer creation (to query at some later point) */
    VkMemoryPropertyFlags memoryPropertyFlags;

    ~Buffer();

    /**
    * Attach the allocated memory block to the buffer
    *
    * @param offset (Optional) Byte offset (from the beginning) for the memory region to bind
    *
    * @return VkResult of the bindBufferMemory call
    */
    VkResult bind(VkDeviceSize offset = 0);

    /**
    * Setup the default descriptor for this buffer
    *
    * @param size (Optional) Size of the memory range of the descriptor
    * @param offset (Optional) Byte offset from beginning
    *
    */
    void setupDescriptor(VkDeviceSize offset, VkDeviceSize size);

    /**
    * Copies the specified data to the mapped buffer
    *
    * @param data Pointer to the data to copy
    * @param size Size of the data to copy in machine units
    *
    */
    void copyTo(const void* data, VkDeviceSize size);

    /**
    * Flush a memory range of the buffer to make it visible to the device
    *
    * @note Only required for non-coherent memory
    *
    * @param size (Optional) Size of the memory range to flush. Pass VK_WHOLE_SIZE to flush the complete buffer range.
    * @param offset (Optional) Byte offset from beginning
    *
    * @return VkResult of the flush call
    */
    VkResult flush(VkDeviceSize size, VkDeviceSize offset);

    /**
    * Invalidate a memory range of the buffer to make it visible to the host
    *
    * @note Only required for non-coherent memory
    *
    * @param size (Optional) Size of the memory range to invalidate. Pass VK_WHOLE_SIZE to invalidate the complete buffer range.
    * @param offset (Optional) Byte offset from beginning
    *
    * @return VkResult of the invalidate call
    */
    VkResult invalidate(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);

    /**
    * Release all Vulkan resources held by this buffer
    */
    void destroy();
};


struct BufferHandle : GraphicHandle
{
    static BufferHandle create()
    {
        BufferHandle handle;
        handle.m_resource = new Buffer();
        return handle;
    }

    Buffer* getBuffer() const
    {
        return static_cast<Buffer*>(*m_resource);
    }
};

} // namespace Vulkan
} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_RENDERER_H__

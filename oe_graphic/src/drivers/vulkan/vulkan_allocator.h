#ifndef __OE_GRAPHIC_VULKAN_ALLOCATOR_H__
#define __OE_GRAPHIC_VULKAN_ALLOCATOR_H__

#include "../../oe_graphic.h"

// OE_Core
#include <vector.h>

// Vulkan
#include "vulkan.h"

namespace OE_Graphic
{
namespace Vulkan
{

class OE_GRAPHIC_EXPORT Allocator
{
public:
    Allocator() = delete;
    Allocator(
        VkDevice device,
        VkDeviceSize normtargetSize,
        const VkMemoryAllocateInfo& memoryAllocateInfo);

    virtual ~Allocator();

    Allocator(const Allocator& rhs) = delete;
    Allocator& operator=(const Allocator& rhs) = delete;

    virtual bool alloc(VkDeviceSize size, Allocation& allocation) = 0;
    virtual void free(VkDeviceSize offset,  VkDeviceSize size) = 0;
    virtual void reset() = 0;

    inline VkDeviceMemory getDeviceMemory() const
    {
        return m_deviceMemory;
    }

    inline uint32 getMemoryType() const
    {
        return m_memAllocInfo.memoryTypeIndex;
    }

    inline void* getMapped() const
    {
        return m_mapped;
    }

protected:
    void insert(Allocator* newAllocator);

    VkDevice m_device = VK_NULL_HANDLE;
    VkDeviceMemory m_deviceMemory = VK_NULL_HANDLE;
    VkMemoryAllocateInfo m_memAllocInfo = {};
    // Size that the page should have if a single allocation size does not exceed it.
    VkDeviceSize m_targetSize = 0;
    void* m_mapped = nullptr;
    Allocator* m_next = nullptr;
    Allocator* m_prev = nullptr;
}
;
} // Vulkan
} // OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_LINEAR_ALLOCATOR_H__

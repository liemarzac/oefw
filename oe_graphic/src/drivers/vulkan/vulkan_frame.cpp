#include "vulkan_frame.h"

// OE_Core
#include <memory.h>

// OE_Graphic
#include "../../graphic_private.h"

// Vulkan
#include "vulkan_buffer.h"
#include "vulkan_linear_allocator.h"

namespace OE_Graphic
{
namespace Vulkan
{

Frame::Frame(VkDevice device) :
    m_device(device)
{
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    vkCreateFence(m_device, &fenceCreateInfo, nullptr, &m_renderQueueFinished);

    m_memoryManager.init(device, Vulkan::MemoryManager::Linear);
}

Frame::~Frame()
{
    vkDeviceWaitIdle(m_device);

    // Release reference to resources held by this frame.
    cleanPendingDeleteResources();

    m_memoryManager.shutdown();

    vkDestroyFence(m_device, m_renderQueueFinished, nullptr);
}

void Frame::beginFrame()
{
    // Clean state from previous use.
    cleanPendingDeleteResources();
    m_memoryManager.reset();

    VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
    cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(m_commandBuffer, &cmdBufferBeginInfo);
}

void Frame::endFrame()
{
    flushInstances();

    vkEndCommandBuffer(m_commandBuffer);

    VkSubmitInfo renderSubmitInfo = {};
    renderSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    renderSubmitInfo.commandBufferCount = 1;
    renderSubmitInfo.pCommandBuffers = &m_commandBuffer;

    // Wait for semaphores
    Vector<VkSemaphore> waitForSemaphores;
    waitForSemaphores.push_back(m_imageAvailSemaphore);

    renderSubmitInfo.waitSemaphoreCount = static_cast<uint32>(waitForSemaphores.size());
    renderSubmitInfo.pWaitSemaphores = waitForSemaphores.data();

    Vector<VkPipelineStageFlags> waitForStages(waitForSemaphores.size(), VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    renderSubmitInfo.pWaitDstStageMask = waitForStages.data();

    // Signal semaphores
    Vector<VkSemaphore> signalSemaphores;
    signalSemaphores.push_back(m_commandQueueFishinshed);

    renderSubmitInfo.signalSemaphoreCount = static_cast<uint32>(signalSemaphores.size());
    renderSubmitInfo.pSignalSemaphores = signalSemaphores.data();

    OE_VULKAN_CHECK(vkQueueSubmit(
        m_graphicsQueue,
        1,
        &renderSubmitInfo,
        m_renderQueueFinished));
}

void Frame::beginRenderPass(RenderPassHandle handle, uint32 iSubpass)
{
}

void Frame::endRenderPass()
{
}

void Frame::waitUntilCompleted()
{
    // Will block until this frame has finished rendering.
    OE_VULKAN_CHECK(vkWaitForFences(
        m_device,
        1,
        &m_renderQueueFinished,
        VK_TRUE,
        UINT64_MAX));

    vkResetFences(m_device, 1, &m_renderQueueFinished);
}

void Frame::cleanPendingDeleteResources()
{
    for(auto graphicResource : m_pendingDeleteResources)
    {
        delete graphicResource;
    }
    m_pendingDeleteResources.clear();
}

bool Frame::alloc(const AllocParams& params, Allocation& allocation)
{
    MemoryManager::AllocParams memManagerAllocParams;
    memManagerAllocParams.m_alignment = params.m_alignment;
    memManagerAllocParams.m_allocInfo = params.m_allocInfo;
    memManagerAllocParams.m_bMap = params.m_bMap;
    memManagerAllocParams.m_propertyFlags = params.m_propertyFlags;
    memManagerAllocParams.m_size = params.m_size;

    return m_memoryManager.alloc(memManagerAllocParams, allocation);
}

void Frame::updateDynamic(UniquePtr<OE_Core::Buffer> buffer, VkDescriptorSet& descSet, VkDeviceSize& offset)
{
    m_instanceBufferHandle.getDynamicBuffer()->write(buffer->m_rawData, buffer->m_size, descSet, offset);
}

void Frame::flushInstances()
{
    // Flush dynamic buffer
    m_instanceBufferHandle.getDynamicBuffer()->flush();
}

} // namespace Vulkan
} // namespace OE_Graphic

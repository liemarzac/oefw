#ifndef __OE_GRAPHIC_VULKAN_HEAP_ALLOCATOR_H__
#define __OE_GRAPHIC_VULKAN_HEAP_ALLOCATOR_H__

#include "../../oe_graphic.h"

// OE_Core
#include <vector.h>

// Vulkan
#include "vulkan_allocator.h"
#include "vulkan.h"

namespace OE_Graphic
{
namespace Vulkan
{

class OE_GRAPHIC_EXPORT HeapAllocator : public Allocator
{
public:
    HeapAllocator() = delete;
    HeapAllocator(
        VkDevice device,
        VkDeviceSize targetSize,
        const VkMemoryAllocateInfo& memoryAllocateInfo,
        bool bMap);
    virtual ~HeapAllocator();

    HeapAllocator(const HeapAllocator& rhs) = delete;
    HeapAllocator& operator=(const HeapAllocator& rhs) = delete;

    virtual bool alloc(VkDeviceSize size, Allocation& allocation) override;
    virtual void free(VkDeviceSize offset, VkDeviceSize size) override;
    virtual void reset() override;

private:
    void removeFreeBlock(uint32 i);
    void checkSanity() const;
    static const size_t ms_alignment = 256;
    struct FreeBlock
    {
        FreeBlock(
            VkDeviceSize offset,
            VkDeviceSize size,
            uint32 iPrev,
            uint32 iNext) :
            m_offset(offset)
            , m_size(size)
            , m_iPrev(iPrev)
            , m_iNext(iNext)
        {}
        VkDeviceSize m_offset = 0;
        VkDeviceSize m_size = 0;
        uint32 m_iPrev = InvalidIndex;
        uint32 m_iNext = InvalidIndex;
    };

    Vector<FreeBlock> m_freeBlocks;
    uint32 m_iFirstFreeBlock = 0;
    VkDeviceSize m_freeSize = 0;

#ifdef _DEBUG
    static bool bCheckSanity;
    static bool bCheckAllocations;
    uint64 m_iAlloc = 0;
    uint64 m_iFree = 0;

    struct AllocTag
    {
        VkDeviceSize m_offset = InvalidIndex;
        uint64 m_allocId = 0;
    };

    Vector<AllocTag> m_tags;
#endif
};

} // Vulkan
} // OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_HEAP_ALLOCATOR_H__

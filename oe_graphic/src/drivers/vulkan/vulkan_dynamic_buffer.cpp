#include "vulkan_dynamic_buffer.h"

#include "../../graphic_private.h"

// OE_Core
#include <oe_math.h>
#include <memory.h>

namespace OE_Graphic
{
namespace Vulkan
{

DynamicBuffer::DynamicBuffer(const CreateInfo& createInfo) :
        m_device(createInfo.m_device)
        ,m_size(createInfo.m_size)
        ,m_descPool(createInfo.m_descriptorPool)
        ,m_alignment(createInfo.m_alignment)
{
    const size_t maxBufferSize = createInfo.m_deviceProperties->limits.maxUniformBufferRange;
    m_bufferSize = min((size_t)m_size, maxBufferSize);
    VkBufferCreateInfo bufferCreateInfo = {};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = m_bufferSize;
    bufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    const uint32 nBuffers = (uint32)(m_size / m_bufferSize);
    m_buffers.resize(nBuffers);

    for(uint32 iBuffer = 0; iBuffer < nBuffers; ++iBuffer)
    {
        OE_VULKAN_CHECK(vkCreateBuffer(m_device, &bufferCreateInfo, nullptr, &m_buffers[iBuffer]));
    }

    // Allocate memory in one block.
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(m_device, m_buffers[0], &memRequirements);

    VkMemoryAllocateInfo memAllocInfo = {};
    memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memAllocInfo.allocationSize = m_size;
    memAllocInfo.memoryTypeIndex = Vulkan::findMemoryProperties(
            *createInfo.m_memoryProperties,
            memRequirements.memoryTypeBits,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    OE_VULKAN_CHECK(vkAllocateMemory(m_device, &memAllocInfo, nullptr, &m_deviceMemory));

    vkMapMemory(m_device, m_deviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)(&m_mapped));

    for(size_t i = 0; i < m_buffers.size(); ++i)
    {
        const VkDeviceSize memOffset = m_bufferSize * i;
        vkBindBufferMemory(m_device, m_buffers[i], m_deviceMemory, memOffset);
    }

    VkDescriptorSetLayoutBinding descSetLayoutBind = {};
    descSetLayoutBind.binding = createInfo.m_iBinding;
    descSetLayoutBind.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    descSetLayoutBind.descriptorCount = 1;
    descSetLayoutBind.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    descSetLayoutBind.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &descSetLayoutBind;

    if(vkCreateDescriptorSetLayout(m_device, &layoutInfo, nullptr, &m_descSetLayout) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN, "Failed to create descriptor set layout");
    }

    VkDescriptorSetAllocateInfo descSetAllocInfo = {};
    descSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAllocInfo.descriptorPool = createInfo.m_descriptorPool;
    descSetAllocInfo.descriptorSetCount = 1;
    descSetAllocInfo.pSetLayouts = &m_descSetLayout;

    m_descSets.resize(m_buffers.size());
    for(size_t i = 0; i < m_descSets.size(); ++i)
    {
        OE_VULKAN_CHECK(vkAllocateDescriptorSets(m_device, &descSetAllocInfo, &m_descSets[i]));
    }

	Vector<VkDescriptorBufferInfo> descBufferInfos(m_buffers.size());
	Vector<VkWriteDescriptorSet> writeDescSets(m_buffers.size());
	for (size_t i = 0; i < descBufferInfos.size(); ++i)
	{
		descBufferInfos[i].buffer = m_buffers[i];
		descBufferInfos[i].offset = 0;
		descBufferInfos[i].range = 256;
		writeDescSets[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescSets[i].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        writeDescSets[i].pBufferInfo = &descBufferInfos[i];
        writeDescSets[i].descriptorCount = 1;
        writeDescSets[i].dstArrayElement = 0;
        writeDescSets[i].dstBinding = 0;
        writeDescSets[i].dstSet = m_descSets[i];
    }

    vkUpdateDescriptorSets(m_device, (uint32)writeDescSets.size(), writeDescSets.data(), 0, nullptr);
}

DynamicBuffer::~DynamicBuffer()
{
    vkFreeDescriptorSets(m_device, m_descPool, (uint32)m_descSets.size(), m_descSets.data());
    vkDestroyDescriptorSetLayout(m_device, m_descSetLayout, nullptr);

    for(auto buffer : m_buffers)
    {
        vkDestroyBuffer(m_device, buffer, nullptr);
    }

    vkFreeMemory(m_device, m_deviceMemory, nullptr);
}

void DynamicBuffer::write(const byte* buffer, VkDeviceSize size, VkDescriptorSet& descSet, VkDeviceSize& offset)
{
    OE_CHECK_MSG(
        m_writeOffset + size <= m_size,
        OE_CHANNEL_VULKAN,
        "Dynamic buffer is too small or was not flushed correctly");

    offset = m_writeOffset % m_bufferSize;
    const uint32 iDesc = (uint32)(m_writeOffset / m_bufferSize);
    descSet = m_descSets[iDesc];
    byte* writePtr = (byte*)m_mapped + m_writeOffset;
    memcpy((void*)writePtr, (void*)buffer, (size_t)size);
    m_writeOffset += size;
    m_writeOffset = align((size_t)m_writeOffset, m_alignment);
}

void DynamicBuffer::flush()
{
    VkMappedMemoryRange memoryRange = {};
    memoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    memoryRange.memory = m_deviceMemory;
    memoryRange.offset = 0;
    memoryRange.size = m_writeOffset;
    vkFlushMappedMemoryRanges(m_device, 1, &memoryRange);
    m_writeOffset = 0;
}

} // namespace Vulkan
} // namespace OE_Graphic

#include "../../driver_command.h"

// OE_Core
#include <array.h>
#include <buffer.h>
#include <memory.h>
#include <std_mutex_helpers.h>

// OE_Graphic
#include "../../graphic_private.h"
#include "vulkan_driver.h"

#define OE_CHANNEL_VULKAN_COMMANDS "VulkanCommands"

namespace OE_Graphic
{

Vulkan::Driver& getVulkanDriver()
{
    return Vulkan::Driver::getInstance();
}

VkDevice getDevice()
{
    return getVulkanDriver().getDevice();
}

VkCommandBuffer getCurrentPassCommandBuffer()
{
    return getVulkanDriver().getCurrentCommandBuffer();
}

VkDescriptorType getDescriptorType(BindingType bindingType)
{
    switch(bindingType)
    {
    case BindingType::Sampler:
        return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

    case BindingType::Uniform:
        return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

	case BindingType::InputAttachment:
        return VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;

    default:
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_COMMANDS, "Unknown binding type");
    }

    return VK_DESCRIPTOR_TYPE_MAX_ENUM;
}

void NullCommand::execute()
{
    // Intentionally do nothing.
}

void CreateVertexBufferCommand::execute()
{
    Vulkan::Driver& renderer = getVulkanDriver();
    renderer.createVertexBuffer(m_params);
}

void CreateIndexBufferCommand::execute()
{
    Vulkan::Driver& renderer = getVulkanDriver();
    renderer.createIndexBuffer(m_params);
}

void CreateTextureCommand::execute()
{
    Vulkan::Driver& driver = getVulkanDriver();
    driver.createTexture(m_params);
}

void CreateRenderPassCommand::execute()
{
    Vulkan::Driver& driver = getVulkanDriver();
    driver.createRenderPass(m_params);
}

void UpdateVertexBufferCommand::execute()
{
    VertexStream* vertexBuffer = getResource(m_handle);
    Vulkan::Buffer* buffer = vertexBuffer->m_bufferHandle.getBuffer();

    if(m_vertices->m_size > buffer->m_size)
    {
        auto& renderer = getVulkanDriver();

        // The new vertices don't fit in the buffer so we need to create a new one.
        vertexBuffer->m_bufferHandle = renderer.createBuffer(
            m_vertices->m_size,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);

        buffer = vertexBuffer->m_bufferHandle.getBuffer();
    }

    // Update the number of vertices.
    const uint32 stride = (uint32)VertexStrideTable[(uint32)vertexBuffer->m_format];
    OE_CHECK(m_vertices->m_size % stride == 0, OE_CHANNEL_VULKAN_COMMANDS);
    vertexBuffer->m_nVertices = (uint32)m_vertices->m_size / stride;

    buffer->copyTo(m_vertices->m_rawData, m_vertices->m_size);
}

void UpdateIndexBufferCommand::execute()
{
    IndexBuffer* indexBuffer = getResource(m_handle);
    Vulkan::Buffer* buffer = indexBuffer->m_bufferHandle.getBuffer();

    if(m_indices->m_size > buffer->m_size)
    {
        auto& renderer = getVulkanDriver();

        // The new vertices don't fit in the buffer so we need to create a new one.
        indexBuffer->m_bufferHandle = getVulkanDriver().createBuffer(
            m_indices->m_size,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);

        buffer = indexBuffer->m_bufferHandle.getBuffer();
    }

    // Update the number of indicies.
    if(indexBuffer->m_indexType == VK_INDEX_TYPE_UINT16)
    {
        indexBuffer->m_nIndices = (uint32)(m_indices->m_size / sizeof(uint16));
    }
    else if(indexBuffer->m_indexType == VK_INDEX_TYPE_UINT32)
    {
        indexBuffer->m_nIndices = (uint32)(m_indices->m_size / sizeof(uint32));
    }

    buffer->copyTo(m_indices->m_rawData, m_indices->m_size);
}

} // namespace OE_Graphics


#include "vulkan_driver.h"

#include "../../graphic_private.h"

// External
#include <SDL_vulkan.h>
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
#include <vulkan/vulkan_win32.h>
#endif

// OE_Core
#include <application.h>
#include <array.h>
#include <map.h>
#include <memory.h>
#include <oe_math.h>
#include <std_mutex_helpers.h>

// Vulkan
#include <gli/sampler.hpp>

#include "vulkan_heap_allocator.h"

#define OE_CHANNEL_VULKAN_RENDERER "VulkanRenderer"

namespace OE_Graphic
{

namespace Vulkan
{

const Vector<const char*> Driver::ms_validationLayers =
{
    "VK_LAYER_KHRONOS_validation"
};

const Vector<const char*> Driver::ms_instanceExtensions =
{
    VK_KHR_SURFACE_EXTENSION_NAME,

#if _DEBUG
    VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#endif

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
};


// Follow DeviceExtensionFlags.
const Vector<const char*> Driver::ms_deviceExtensions =
{
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_NV_DEDICATED_ALLOCATION_EXTENSION_NAME
};

const VkFormat Driver::ms_surfaceFormat = VK_FORMAT_B8G8R8A8_UNORM;
const VkColorSpaceKHR Driver::ms_surfaceColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;

#if _DEBUG
static VKAPI_ATTR VkBool32 VKAPI_CALL g_debugCallback(

    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    if(messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        OE_CHECK_MSG(
            false,
            OE_CHANNEL_VULKAN_RENDERER,
            "validation layer: %s", pCallbackData->pMessage);
    }

    return VK_FALSE;
}
#endif

Driver::Driver()
{
    m_isReady = false;
    m_isOk = true;

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    m_activeWindowhandle = nullptr;
#endif

    m_memoryProperties = {};
    DBG_ONLY(m_flags.set(VulkanFlags::EnableValidationLayers));
}

Driver::~Driver()
{
    m_state.m_renderPipelineHandle.reset();
    m_state.m_renderPassHandle.reset();
    m_state.m_viewHandle.reset();

    destroyFrames();

    cleanPendingDeleteResources();

    destroySemaphores();

    destroyGlobalResources();

    vkDestroyCommandPool(m_device, m_commandPool, nullptr);

    destroySwapChain();

    vkDestroyDescriptorPool(m_device, m_descriptorPool, nullptr);
    m_heapAllocators.shutdown();
    vkDestroyDevice(m_device, nullptr);
    vkDestroySurfaceKHR(m_instance, m_surface, nullptr);

#ifdef _DEBUG
    if(m_flags.isSet(VulkanFlags::EnableValidationLayers))
    {
        destroyDebugUtilsMessengerEXT();
    }
#endif

    vkDestroyInstance(m_instance, nullptr);
}

uint32 Driver::SDL_GetWindowFlags() const
{
    return SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN;
}

bool Driver::initFromMainThread(const RendererConfig& config)
{
    if(m_flags.isSet(VulkanFlags::EnableValidationLayers)
        && !checkValidationLayerSupport())
    {
        return false;
    }

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    m_activeWindowhandle = GetActiveWindow();
#endif

    m_screenSize_MainThread = config.m_screenSize;
    m_window = config.m_window;

    GraphicResource::m_deleter = std::bind(
        &Driver::addPendingDeleteResource, this, std::placeholders::_1);

    if(config.m_bVSync)
    {
        m_flags.set(VulkanFlags::VSync);
    }

    if(!createVulkanInstance())
    {
        throw std::runtime_error("Vulkan instance cannot be created");
    }

#if _DEBUG
    setupDebugMessenger();
#endif

    if(!createSurface())
    {
        throw std::runtime_error("Vulkan surface cannot be created");
    }

    m_physicalDevice = pickPhysicalDevice();
    if(!m_physicalDevice)
    {
        throw std::runtime_error("No physical device can be found");
    }

    vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_memoryProperties);
    vkGetPhysicalDeviceProperties(m_physicalDevice, &m_deviceProperties);
    m_deviceExtensionFlags = checkPhysicalDeviceExtensionsSupport(m_physicalDevice);

    querySwapChainSupport(m_swapChainSupport, m_physicalDevice);

    if(!createLogicalDevice())
    {
        throw std::runtime_error("Vulkan logical device can be created");
    }

    m_heapAllocators.init(m_device, Vulkan::MemoryManager::Heap);

    vkGetDeviceQueue(
        m_device,
        findQueue(m_physicalDevice, VK_QUEUE_GRAPHICS_BIT),
        0,
        &m_graphicsQueue);

    if(!createDescriptorPool())
    {
        throw std::runtime_error("Vulkan descriptor pool cannot be created");
    }

    // Cap the screen size to the maximum capabilities of the physical device.
    m_screenSize_MainThread.m_width = min(m_screenSize_MainThread.m_width, m_swapChainSupport.m_capabilities.minImageExtent.width);
    m_screenSize_MainThread.m_height = min(m_screenSize_MainThread.m_height, m_swapChainSupport.m_capabilities.minImageExtent.height);

    m_screenSize_RenderThread = m_screenSize_MainThread;

    if(!createSwapChain(m_screenSize_MainThread))
    {
        throw std::runtime_error("Vulkan swap chain cannot be created");
    }

    if(!createCommandPool())
    {
        throw std::runtime_error("Vulkan command pool cannot be created");
    }

    if(!createGlobalResources())
    {
        throw std::runtime_error("Vulkan global resources cannot be created");
    }

    createFrames();

    return true;
}

bool Driver::initFromDriverThread(const RendererConfig& config)
{
    m_isReady = true;
    m_isOk = true;

    return true;
}

double Driver::getFrameTime() const
{
    return m_frameTime;
}

void Driver::addPendingDeleteResource(GraphicResource* resource)
{
    LockGuard lock(m_pendingDeleteResourcesMutex);
    m_pendingDeleteResources.push_back(resource);
}

bool Driver::createVulkanInstance()
{
    if(!checkInstanceExtensionsSupport())
    {
        return false;
    }

    String appName;
    appGetName(appName);

    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = appName.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "oefw";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_3;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    if(m_flags.isSet(VulkanFlags::EnableValidationLayers))
    {
        createInfo.enabledLayerCount = uint32(ms_validationLayers.size());
        createInfo.ppEnabledLayerNames = ms_validationLayers.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
    }

    uint nSDLVulkanExtensions = 0;
    SDL_Vulkan_GetInstanceExtensions(m_window, &nSDLVulkanExtensions, nullptr);
    Vector<const char*> SDLVulkanExtensionsNames;

    if(nSDLVulkanExtensions > 0)
    {
        SDLVulkanExtensionsNames.resize(nSDLVulkanExtensions);
        SDL_Vulkan_GetInstanceExtensions(m_window, &nSDLVulkanExtensions, SDLVulkanExtensionsNames.data());
    }

#if _DEBUG
    SDLVulkanExtensionsNames.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

    createInfo.enabledExtensionCount = uint32(SDLVulkanExtensionsNames.size());
    createInfo.ppEnabledExtensionNames = SDLVulkanExtensionsNames.data();

    VkResult result = vkCreateInstance(&createInfo, nullptr, &m_instance);
    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Instance creation failed");
    }

    return result == VK_SUCCESS;
}

VkPhysicalDevice Driver::pickPhysicalDevice() const
{
    // Query how many devices are present in the system
    uint32 nDevices = 0;
    VkResult result = vkEnumeratePhysicalDevices(m_instance, &nDevices, NULL);
    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to query the number of physical devices present");
        return nullptr;
    }

    // There has to be at least one device present
    if(nDevices == 0)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "No device found supporting Vulkan");
        return nullptr;
    }

    // Get the physical devices
    Vector<VkPhysicalDevice> physicalDevices(nDevices, VkPhysicalDevice());
    result = vkEnumeratePhysicalDevices(m_instance, &nDevices, &physicalDevices[0]);
    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to enumerate physical devices present");
        return nullptr;
    }

    // Enumerate all physical devices
    VkPhysicalDeviceProperties deviceProperties;
    for(uint32 i = 0; i < nDevices; i++)
    {
        memset(&deviceProperties, 0, sizeof deviceProperties);
        vkGetPhysicalDeviceProperties(physicalDevices[i], &deviceProperties);
        log(OE_CHANNEL_VULKAN_RENDERER, "Driver Version: %u", deviceProperties.driverVersion);
        log(OE_CHANNEL_VULKAN_RENDERER, "Device Name:    %s", deviceProperties.deviceName);
        log(OE_CHANNEL_VULKAN_RENDERER, "Device Type:    %u", deviceProperties.deviceType);
        log(OE_CHANNEL_VULKAN_RENDERER, "API Version:    %u.%u.%u",
            // See note below regarding this:
            (deviceProperties.apiVersion >> 22) & 0x3FF,
            (deviceProperties.apiVersion >> 12) & 0x3FF,
            (deviceProperties.apiVersion & 0xFFF));
    }

    int iBestDevice = findBestDevice(physicalDevices);
    if(iBestDevice == InvalidIndex)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to find suitable physical device");
        return nullptr;
    }

    return physicalDevices[iBestDevice];
}

VkPresentModeKHR Driver::pickPresentMode(const Vector<VkPresentModeKHR>& presentModes) const
{
    // Default to FIFO which is guaranteed to be supported.
    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;

    bool supportMailBox = false;
    bool supportImmediate = false;

    for(auto presentModeSupported : presentModes)
    {
        if(presentModeSupported == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            supportMailBox = true;
        }
        else if(presentModeSupported == VK_PRESENT_MODE_IMMEDIATE_KHR)
        {
            supportImmediate = true;
        }
    }

    if(m_flags.isSet(VulkanFlags::VSync))
    {
        if(supportMailBox)
        {
            presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
        }
    }
    else
    {
        if(supportImmediate)
        {
            presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        }
        else if(supportMailBox)
        {
            presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
        }
    }

    return presentMode;
}

VkFormat Driver::findSupportedFormat(
    const Vector<VkFormat>& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features) const
{
    for(VkFormat format : candidates)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(m_physicalDevice, format, &props);

        if(tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
        {
            return format;
        }
        else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
        {
            return format;
        }
    }

    OE_CHECK(false, OE_CHANNEL_VULKAN_RENDERER);
    return VK_FORMAT_UNDEFINED;
}

VkFormat Driver::findDepthFormat() const
{
    return findSupportedFormat(
        { VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

bool Driver::hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

bool Driver::createLogicalDevice()
{
    VkDeviceQueueCreateInfo queueCreateInfo = {};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = findQueue(m_physicalDevice, VK_QUEUE_GRAPHICS_BIT);
    queueCreateInfo.queueCount = 1;
    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = &m_deviceFeatures;

    // Enable extension found for the current device.
    Vector<const char*> extensionNames;
    for(size_t i = 0; i < ms_deviceExtensions.size(); ++i)
    {
        if(m_deviceExtensionFlags.isSet(static_cast<DeviceExtensionFlags>(i)))
        {
            extensionNames.push_back(ms_deviceExtensions[i]);
        }
    }

    createInfo.enabledExtensionCount = uint32(extensionNames.size());
    createInfo.ppEnabledExtensionNames = extensionNames.data();


    if(m_flags.isSet(VulkanFlags::EnableValidationLayers))
    {
        createInfo.enabledLayerCount = uint32(ms_validationLayers.size());
        createInfo.ppEnabledLayerNames = ms_validationLayers.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
    }

    VkResult result = vkCreateDevice(m_physicalDevice, &createInfo, nullptr, &m_device);
    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to create logical device");
    }

    return result == VK_SUCCESS;
}

bool Driver::createSurface()
{
#if OE_PLATFORM == OE_PLATFORM_WINDOWS
    VkWin32SurfaceCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    createInfo.hwnd = m_activeWindowhandle;
    createInfo.hinstance = GetModuleHandle(nullptr);

    VkResult result = vkCreateWin32SurfaceKHR(m_instance, &createInfo, nullptr, &m_surface);
    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to create surface");
    }

    return result == VK_SUCCESS;
#else
    return SDL_Vulkan_CreateSurface(
        m_window,
        (SDL_vulkanInstance)m_instance,
        (SDL_vulkanSurface*)&m_surface);
#endif
}

void Driver::createFrames()
{
    const uint32 nFrames = queryImageCount();
    m_frames.reserve(nFrames);

    for(uint32 i = 0; i < nFrames; ++i)
    {
        m_frames.emplace_back(m_device);
        m_frames[i].m_iImage = i;
        m_frames[i].m_instanceBufferHandle = createDynamicBuffer(1 * OneMb);
        m_frames[i].m_device = m_device;
        m_frames[i].m_graphicsQueue = m_graphicsQueue;
        m_frames[i].m_deviceProperties = &m_deviceProperties;
        
        VkCommandBufferAllocateInfo cmdBufferAllocInfo = {};
        cmdBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdBufferAllocInfo.commandBufferCount = 1;
        cmdBufferAllocInfo.commandPool = m_commandPool;
        cmdBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

        vkAllocateCommandBuffers(m_device, &cmdBufferAllocInfo, &m_frames[i].m_commandBuffer);

        VkSemaphoreCreateInfo semaphoreCreateInfo = {};
        semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        m_frames[i].m_commandQueueFishinshed = acquireSemaphore();
    }
}

void Driver::destroyFrames()
{
    for(auto& frame : m_frames)
    {
        // Return semaphore to the pool before deleting the frame.
        if(frame.m_imageAvailSemaphore)
        {
            releaseSemaphore(frame.m_imageAvailSemaphore);
        }

        releaseSemaphore(frame.m_commandQueueFishinshed);

        vkFreeCommandBuffers(m_device, m_commandPool, 1, &frame.m_commandBuffer);
    }

    m_frames.clear();
}

bool Driver::createSwapChain(ScreenSize size)
{
    uint32 nImages = queryImageCount();

    //
    // Image usage
	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physicalDevice, m_surface, &surfaceCapabilities);

	// Enable transfer source on swap chain images if supported
    VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT)
    {
        imageUsage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	}

	// Enable transfer destination on swap chain images if supported
	if (surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT)
    {
        imageUsage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	}

    VkImageFormatProperties imageProperties;
    VkResult r = vkGetPhysicalDeviceImageFormatProperties(m_physicalDevice, ms_surfaceFormat, VK_IMAGE_TYPE_2D, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, 0, &imageProperties);

    VkSwapchainKHR oldSwapChain = m_swapChain;

    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = m_surface;
    createInfo.minImageCount = nImages;
    createInfo.imageFormat = ms_surfaceFormat;
    createInfo.imageColorSpace = ms_surfaceColorSpace;
    createInfo.imageExtent = { size.m_width, size.m_height };
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = imageUsage;
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = 0;
    createInfo.pQueueFamilyIndices = nullptr;
    createInfo.preTransform = m_swapChainSupport.m_capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = pickPresentMode(m_swapChainSupport.m_presentModes);
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = oldSwapChain;

    VkSwapchainKHR newSwapChain = VK_NULL_HANDLE;
    VkResult result = vkCreateSwapchainKHR(
        m_device,
        &createInfo,
        nullptr,
        &newSwapChain);

    if(result != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to create swap chain");
        return false;
    }

    Vector<VkImage> swapchainImages(nImages);
    OE_VULKAN_CHECK(vkGetSwapchainImagesKHR(
        m_device,
        newSwapChain,
        &nImages,
        swapchainImages.data()));

    if (oldSwapChain == VK_NULL_HANDLE)
    {
        OE_CHECK(m_swapchainImageViews.size() == 0, OE_CHANNEL_VULKAN);
        m_swapchainImageViews.resize(nImages);
    }
    else
    {
        // Destroy previous image views.
        OE_CHECK(m_swapchainImageViews.size() == nImages, OE_CHANNEL_VULKAN);
        for (auto imageView : m_swapchainImageViews)
        {
            vkDestroyImageView(m_device, imageView, nullptr);
        }

        // Destroy previous swapchain.
        vkDestroySwapchainKHR(m_device, oldSwapChain, nullptr);
        oldSwapChain = nullptr;
    }

    // Create image views.
    for (uint32 iImage = 0; iImage < nImages; iImage++)
    {
        VkImageViewCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = swapchainImages[iImage];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = VK_FORMAT_B8G8R8A8_UNORM;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        OE_VULKAN_CHECK(vkCreateImageView(
            m_device,
            &createInfo,
            nullptr,
            &m_swapchainImageViews[iImage]));
    }

    m_swapChain = newSwapChain;

    return result == VK_SUCCESS;
}

void Driver::destroySwapChain()
{
    // Destroy image views.
    for (auto imageView : m_swapchainImageViews)
    {
        vkDestroyImageView(m_device, imageView, nullptr);
    }
    m_swapchainImageViews.clear();

    vkDestroySwapchainKHR(m_device, m_swapChain, nullptr);
    m_swapChain = VK_NULL_HANDLE;
}

bool Driver::createCommandPool()
{
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = findQueue(m_physicalDevice, VK_QUEUE_GRAPHICS_BIT);
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if(vkCreateCommandPool(m_device, &poolInfo, nullptr, &m_commandPool) != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to create command pool");
        return false;
    }

    return true;
}

void Driver::destroySemaphores()
{
    // Free the semaphores in the pool.
    for(auto semaphore : m_semaphorePool)
    {
        vkDestroySemaphore(m_device, semaphore, nullptr);
    }

    m_semaphorePool.clear();
}

bool Driver::createDescriptorPool()
{
    const uint32 NumDescriptors = 1000;
    Array<VkDescriptorPoolSize, 4> poolSizes;
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = NumDescriptors;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    poolSizes[1].descriptorCount = NumDescriptors;
    poolSizes[2].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[2].descriptorCount = NumDescriptors;
    poolSizes[3].type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    poolSizes[3].descriptorCount = 10;


    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = (uint32)poolSizes.size();
    poolInfo.pPoolSizes = poolSizes.data();
    poolInfo.maxSets = NumDescriptors * (uint32)poolSizes.size();
    poolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

    if(vkCreateDescriptorPool(m_device, &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
    {
        logError(OE_CHANNEL_VULKAN_RENDERER, "Failed to create descriptor pool");
        return false;
    }

    return true;
}

bool Driver::createGlobalResources()
{
    // Shared sampler for color attachments
    VkSamplerCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    createInfo.pNext = nullptr;
    createInfo.magFilter = VK_FILTER_LINEAR;
    createInfo.minFilter = VK_FILTER_LINEAR;
    createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    createInfo.mipLodBias = 0.0f;
    createInfo.maxAnisotropy = 0;
    createInfo.minLod = 0.0f;
    createInfo.maxLod = 1.0f;
    createInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    OE_VULKAN_CHECK(
        vkCreateSampler(
            m_device,
            &createInfo,
            nullptr,
            &m_colorAttachmentSampler));

    return true;
}

void Driver::destroyGlobalResources()
{
    vkDestroySampler(m_device, m_colorAttachmentSampler, nullptr);
}

bool Driver::isReady() const
{
    return m_bReady;
}

bool Driver::isOk() const
{
    return m_isOk;
}

void Driver::resize(const ScreenSize& newSize)
{
    m_screenSize_MainThread = newSize;
    waitUntilGPUIdle();
    m_screenSize_RenderThread = m_screenSize_MainThread;
    createSwapChain(m_screenSize_RenderThread);
}

void Driver::waitUntilGPUIdle()
{
    vkDeviceWaitIdle(m_device);
}

void Driver::flush()
{
    waitUntilGPUIdle();
}

void Driver::getShaderResourcePath(
    ShaderType shaderType,
    const String& resourceName,
    String& resourcePath) const
{
    String shaderExtension;
    if(shaderType == ShaderType::VertexShader)
    {
        shaderExtension = "_vert";
    }
    else
    {
        shaderExtension = "_frag";
    }

    resourcePath = "shaders/vulkan/" + resourceName + shaderExtension + ".spv";
}

void Driver::getShaderResourcePath(String& resourcePath) const
{
    resourcePath = "shaders/vulkan";
}

bool Driver::checkValidationLayerSupport()
{
    uint32 nLayers;
    vkEnumerateInstanceLayerProperties(&nLayers, nullptr);

    Vector<VkLayerProperties> availableLayers(nLayers);
    vkEnumerateInstanceLayerProperties(&nLayers, availableLayers.data());

    bool areAllLayersFound = true;

    for(const char* layerName : ms_validationLayers)
    {
        bool isLayerFound = false;

        for(const auto& layerProperties : availableLayers)
        {
            if(strcmp(layerName, layerProperties.layerName) == 0)
            {
                isLayerFound = true;
                break;
            }
        }

        if(!isLayerFound)
        {
            areAllLayersFound = false;
            logError(OE_CHANNEL_VULKAN_RENDERER, "Validation layer %s was not found", layerName);
        }
    }

    return areAllLayersFound;
}

bool Driver::checkInstanceExtensionsSupport()
{
    uint32 nExtensionProperties = 0;
    vkEnumerateInstanceExtensionProperties(
        nullptr,
        &nExtensionProperties,
        nullptr);

    Vector<VkExtensionProperties> extensionProperties(nExtensionProperties);
    vkEnumerateInstanceExtensionProperties(
        nullptr,
        &nExtensionProperties,
        extensionProperties.data());
 
    bool areAllExtensionsFound = true;

    for(const char* extensionName : ms_instanceExtensions)
    {
        bool isExtensionFound = false;

        for(const auto& extensionProperties : extensionProperties)
        {
            if(strcmp(extensionName, extensionProperties.extensionName) == 0)
            {
                isExtensionFound = true;
                break;
            }
        }

        if(!isExtensionFound)
        {
            areAllExtensionsFound = false;
            logError(OE_CHANNEL_VULKAN_RENDERER, "Instance extension %s was not found", extensionName);
        }
    }

    return areAllExtensionsFound;
}

Vulkan::DeviceExtFlags Driver::checkPhysicalDeviceExtensionsSupport(VkPhysicalDevice device)
{
    Vulkan::DeviceExtFlags flags;

    uint32 nExtensionProperties = 0;
    vkEnumerateDeviceExtensionProperties(
        device,
        nullptr,
        &nExtensionProperties,
        nullptr);

    Vector<VkExtensionProperties> extensionProperties(nExtensionProperties);
    vkEnumerateDeviceExtensionProperties(
        device,
        nullptr,
        &nExtensionProperties,
        extensionProperties.data());

    for(uint32 iDeviceExtension = 0; iDeviceExtension < ms_deviceExtensions.size(); ++iDeviceExtension)
    {
        const char* extensionName = ms_deviceExtensions[iDeviceExtension];

        for(const auto& extensionProperties : extensionProperties)
        {
            if(strcmp(extensionName, extensionProperties.extensionName) == 0)
            {
                flags.set((Vulkan::DeviceExtensionFlags)iDeviceExtension);
                break;
            }
        }
    }

    return flags;
}

void Driver::querySwapChainSupport(SwapChainSupport& support, VkPhysicalDevice device) const
{
    OE_CHECK(m_surface, OE_CHANNEL_VULKAN_RENDERER);

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, m_surface, &support.m_capabilities);

    uint32 nFormat;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_surface, &nFormat, nullptr);

    if(nFormat != 0)
    {
        support.m_formats.resize(nFormat);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_surface, &nFormat, support.m_formats.data());
    }

    uint32 nPresentMode;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_surface, &nPresentMode, nullptr);

    if(nPresentMode != 0)
    {
        support.m_presentModes.resize(nPresentMode);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_surface, &nPresentMode, support.m_presentModes.data());
    }
}

uint32 Driver::queryImageCount() const
{
    uint32 nImages = m_swapChainSupport.m_capabilities.minImageCount + 1;
    const uint32 nMaxImages = m_swapChainSupport.m_capabilities.maxImageCount;
    if(nMaxImages > 0 && nImages > nMaxImages)
    {
        nImages = nMaxImages;
    }

    return nImages;
}

bool Driver::alloc(const AllocParams& params, Vulkan::Allocation& allocation)
{
    Vulkan::MemoryManager* memoryManager = &m_heapAllocators;

    Vulkan::MemoryManager::AllocParams memManagerAllocParam;
    memManagerAllocParam.m_alignment = params.m_alignment;
    memManagerAllocParam.m_allocInfo = params.m_allocInfo;
    memManagerAllocParam.m_bMap = params.m_bMap;
    memManagerAllocParam.m_propertyFlags = params.m_propertyFlags;
    memManagerAllocParam.m_size = params.m_size;
    return memoryManager->alloc(memManagerAllocParam, allocation);
}

int Driver::findBestDevice(const Vector<VkPhysicalDevice>& devices) const
{
    int iBestDevice = InvalidIndex;

    Vector<int> deviceScores(devices.size());

    for(int iDevice = 0; iDevice < devices.size(); ++iDevice)
    {
        deviceScores[iDevice] = -1;

        VkPhysicalDevice physicalDevice = devices[iDevice];

        VkPhysicalDeviceProperties deviceProperties;
        VkPhysicalDeviceFeatures deviceFeatures;

        if(!isPhysicalDeviceSuitable(physicalDevice))
        {
            continue;
        }

        vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);
        vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);

        int score = 0;
        if(deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
        {
            score += 1000;
        }

        deviceScores[iDevice] = score;
    }

    int maxScore = -1;
    for(int iDevice = 0; iDevice < devices.size(); ++iDevice)
    {
        if(deviceScores[iDevice] > maxScore)
        {
            maxScore = deviceScores[iDevice];
            iBestDevice = iDevice;
        }
    }

    return iBestDevice;
}

int Driver::findQueue(VkPhysicalDevice device, VkQueueFlagBits queue)
{
    uint32 nQueueFamily = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &nQueueFamily, nullptr);

    Vector<VkQueueFamilyProperties> queueFamilies(nQueueFamily);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &nQueueFamily, queueFamilies.data());

    int iFoundQueue = InvalidIndex;
    for(int iQueueFamily = 0; iQueueFamily < int(queueFamilies.size()); ++iQueueFamily)
    {
        if(queueFamilies[iQueueFamily].queueFlags & queue)
        {
            iFoundQueue = iQueueFamily;
            break;
        }
    }

    return iFoundQueue;
}

bool Driver::isPhysicalDeviceSuitable(VkPhysicalDevice device) const
{
    OE_CHECK(m_surface, OE_CHANNEL_VULKAN_RENDERER);

    int iQueue = findQueue(device, VK_QUEUE_GRAPHICS_BIT);
    if(iQueue == InvalidIndex)
    {
        return false;
    }

    VkBool32 presentSupport = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(device, iQueue, m_surface, &presentSupport);
    if(!presentSupport)
    {
        return false;
    }

    // Swapchain is a required extension.
    auto deviceExtFlags = checkPhysicalDeviceExtensionsSupport(device);
    if(!deviceExtFlags.isSet(Vulkan::DeviceExtensionFlags::Swapchain))
    {
        return false;
    }

    SwapChainSupport swapChainSupport;
    querySwapChainSupport(swapChainSupport, device);

    bool hasFoundSurfaceFormat = false;
    for(auto format : swapChainSupport.m_formats)
    {
        if(format.format == ms_surfaceFormat && format.colorSpace == ms_surfaceColorSpace)
        {
            hasFoundSurfaceFormat = true;
            break;
        }
    }

    if(!hasFoundSurfaceFormat)
    {
        return false;
    }

    if(swapChainSupport.m_formats.empty() || swapChainSupport.m_presentModes.empty())
    {
        return false;
    }

    return true;
}

void Driver::waitUntilReady()
{
    UniqueLock lock(m_readyMutex);
    m_readyCondition.wait(lock, [&]{return this->m_bReady;});
}

void Driver::preRender()
{
}

void Driver::postRender()
{
}

void Driver::beginFrame()
{
    m_frameStopWatch.start();
    m_state.m_iRenderPass = 0;

    VkSemaphore imageAvailableSemaphore = acquireSemaphore();

    vkAcquireNextImageKHR(
        m_device,
        m_swapChain,
        UINT64_MAX,
        imageAvailableSemaphore,
        VK_NULL_HANDLE,
        &m_state.m_iImage);

    // No render wait synchronized with the backbuffer yet.
    m_bHasWaitedForBackbuffer = false;

    auto& frame = m_frames[m_state.m_iImage];

    // This frame must have finished rendering before re-using it.
    frame.waitUntilCompleted();

    // Recycle the image available semaphore previously used.
    if (frame.m_imageAvailSemaphore)
    {
        releaseSemaphore(frame.m_imageAvailSemaphore);
    }

    // Assign the newly acquired semaphore.
    frame.m_imageAvailSemaphore = imageAvailableSemaphore;

    frame.beginFrame();
}

void Driver::endFrame()
{
    auto* frame = getCurrentFrame();
    frame->endFrame();

    cleanPendingDeleteResources();

    // Wait for the primary queue to be finished.
    VkSemaphore waitSemaphore = frame->m_commandQueueFishinshed;

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    VkSemaphore waitSemaphores[] = { waitSemaphore };
    presentInfo.pWaitSemaphores = waitSemaphores;
    presentInfo.waitSemaphoreCount = 1;

    VkSwapchainKHR swapChains[] = { m_swapChain };
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &m_state.m_iImage;
    presentInfo.pResults = nullptr;
    vkQueuePresentKHR(m_graphicsQueue, &presentInfo);

    m_frameTime = m_frameStopWatch.getElapsedTime();
}

void Driver::beginRenderPass(RenderPassHandle handle, uint32 iSubpass)
{
    m_state.m_renderPassHandle = handle;
    m_state.m_iSubpass = iSubpass;

    if(iSubpass > 0)
    {
        // When executing multiple subpasses, there is no need to begin a new render pass.
        return;
    }

    RenderPass* renderPass = getResource(handle);

    RenderPass::SetupFrameBuffersParams setupFrameBuffersParams;
    setupFrameBuffersParams.m_iFrame = getImageIndex();
    setupFrameBuffersParams.m_screenSize = m_screenSize_RenderThread;
    setupFrameBuffersParams.m_swapChain = m_swapChain;
    setupFrameBuffersParams.m_swapchainImageViews = m_swapchainImageViews;
    setupFrameBuffersParams.m_memoryProperties = m_memoryProperties;
    setupFrameBuffersParams.m_bNVidiaDedicatedAlloc = 
        m_deviceExtensionFlags.isSet(Vulkan::DeviceExtensionFlags::NVDedicatedAllocation);

    renderPass->setupFramebuffers(setupFrameBuffersParams);

    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = renderPass->m_renderPass;
    renderPassInfo.framebuffer = renderPass->m_framebuffers[m_state.m_iImage];
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent.width = m_screenSize_RenderThread.m_width;
    renderPassInfo.renderArea.extent.height = m_screenSize_RenderThread.m_height;

    renderPassInfo.clearValueCount = static_cast<uint32>(renderPass->m_clearValues.size());
    renderPassInfo.pClearValues = renderPass->m_clearValues.data();

    auto cmdBuffer = getCurrentCommandBuffer();

    vkCmdBeginRenderPass(
        cmdBuffer,
        &renderPassInfo,
        VK_SUBPASS_CONTENTS_INLINE);

    // TODO: This should be a command per view.
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float)m_screenSize_RenderThread.m_width;
    viewport.height = (float)m_screenSize_RenderThread.m_height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

    // TODO: This should be a command per view.
    VkRect2D scissor = {};
    scissor.offset = { 0, 0 };
    scissor.extent.width = m_screenSize_RenderThread.m_width;
    scissor.extent.height = m_screenSize_RenderThread.m_height;
    vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);
}

void Driver::endRenderPass()
{
    RenderPass* renderPass = getResource(m_state.m_renderPassHandle);

    auto cmdBuffer = getCurrentCommandBuffer();

    const uint32 nSubpasses = static_cast<uint32>(renderPass->m_subpasses.size());
    if(m_state.m_iSubpass + 1 < nSubpasses)
    {
        vkCmdNextSubpass(cmdBuffer, VK_SUBPASS_CONTENTS_INLINE);
    }
    else
    {

        vkCmdEndRenderPass(cmdBuffer);

        //vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE);
    }
}

void Driver::iterateRenderPipeline(std::function<void(const RenderPassId& renderPassId)> callback)
{
    OE_CHECK(callback, OE_CHANNEL_VULKAN_RENDERER);

    RenderPipeline* renderPipeline = getResource(m_state.m_renderPipelineHandle);

    for(auto& renderPassNode : renderPipeline->m_renderSequence)
    {
        RenderPassHandle& renderPassHandle = renderPassNode.m_renderPass;
        RenderPass* renderPass = getResource(renderPassHandle);

        const uint32 nSubpasses = static_cast<uint32>(renderPass->m_subpasses.size());
        for(uint32 iSubpass = 0; iSubpass < nSubpasses; ++iSubpass)
        {
            RenderPassId renderPassId{ renderPassHandle , iSubpass };
            callback(renderPassId);
        }
    }
}

void Driver::setPipeline(const PipelineHandle& handle)
{
    Pipeline* pipeline = getResource(handle);

    const uint32 iImage = getImageIndex();

    Pipeline::SetupWriteDescriptorSet setupDescriptorsParams;
    setupDescriptorsParams.m_device = m_device;
    setupDescriptorsParams.m_iFrame = iImage;
    setupDescriptorsParams.m_colorAttachmentSampler = m_colorAttachmentSampler;

    pipeline->setupWriteDescriptorSet(setupDescriptorsParams);

    vkCmdBindPipeline(
        getCurrentCommandBuffer(),
        VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->m_graphicsPipeline);

    pipeline->applyStagedUBO(iImage);

    // Material descriptor set.
    m_state.m_materialDescSet = pipeline->m_descSets[iImage];

    if(m_state.m_viewHandle != pipeline->m_viewHandle)
    {
        if(pipeline->m_viewHandle)
        {
            // Set the view UBO.
            View* view = getResource(pipeline->m_viewHandle);
            const uint32 iImage = getImageIndex();
            view->applyStagedUBO(iImage);
            m_state.m_viewDescSet = view->m_perFrame[iImage].m_descSet;
            m_state.m_viewHandle = pipeline->m_viewHandle;
        }
        else
        {
            m_state.m_viewDescSet = VK_NULL_HANDLE;
            m_state.m_viewHandle = VK_NULL_HANDLE;
        }
    }
}

void Driver::updateInstance(const DrawInstanceHandle& handle, OE_Core::UniqueBufferPtr buffer)
{
    DrawInstance* drawInstance = getResource(handle);

    auto* frame = getCurrentFrame();
    VkDeviceSize offset = 0;
    VkDescriptorSet descSet = VK_NULL_HANDLE;
    frame->updateDynamic(std::move(buffer), descSet, offset);
    drawInstance->m_offset = (uint32)offset;
    drawInstance->m_descSet = descSet;
}

void Driver::drawInstance(const DrawInstanceHandle& handle)
{
    DrawInstance* drawInstance = getResource(handle);
    VkCommandBuffer cmdBuffer = getCurrentCommandBuffer();
    VertexStream* vertexBuffer = getResource(drawInstance->m_vertexBufferHandle);
    IndexBuffer* indexBuffer = getResource(drawInstance->m_indexBufferHandle);
    Pipeline* pipeline = getResource(drawInstance->m_pipelineHandle);

    VkDeviceSize offsets[1] = { 0 };
    VkBuffer vertexBuffers[1] = { vertexBuffer->m_bufferHandle.getBuffer()->m_buffer };

    vkCmdBindVertexBuffers(
        cmdBuffer,
        0,
        1,
        vertexBuffers, offsets);

    Array<VkDescriptorSet, 3> descSets = {
        m_state.m_viewDescSet,
        m_state.m_materialDescSet,
        drawInstance->m_descSet };

    vkCmdBindDescriptorSets(
        cmdBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipeline->m_pipelineLayout,
        0,
        (uint32)descSets.size(),
        descSets.data(),
        1,
        &drawInstance->m_offset);

    if(indexBuffer)
    {
        vkCmdBindIndexBuffer(
            cmdBuffer,
            indexBuffer->m_bufferHandle.getBuffer()->m_buffer,
            0,
            indexBuffer->m_indexType);

        vkCmdDrawIndexed(
            cmdBuffer,
            indexBuffer->m_nIndices,
            1,
            0,
            0,
            0);
    }
    else
    {
        vkCmdDraw(
            cmdBuffer,
            vertexBuffer->m_nVertices,
            1,
            0,
            0);
    }
}

void Driver::flushInstance()
{
    Frame* frame = getCurrentFrame();
    frame->flushInstances();
}

Vulkan::BufferHandle Driver::createBuffer(
    VkDeviceSize size,
    VkBufferUsageFlags usage,
    VkMemoryPropertyFlags properties,
    AllocLifetime allocLifetime)
{
    Vulkan::BufferHandle handle = Vulkan::BufferHandle::create();
    Vulkan::Buffer& buffer = *handle.getBuffer();

    buffer.m_device = m_device;
    buffer.memoryPropertyFlags = properties;

    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    // Create a temporary buffer just to get the memory requirements.
    VkBuffer tempBuffer = VK_NULL_HANDLE;
    OE_VULKAN_CHECK(vkCreateBuffer(m_device, &bufferInfo, nullptr, &tempBuffer));

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(m_device, tempBuffer, &memRequirements);

    // The temporary buffer can now be discarded.
    vkDestroyBuffer(m_device, tempBuffer, nullptr);
    tempBuffer = VK_NULL_HANDLE;

    // Create a final buffer with the memory requirements.
    bufferInfo.size = memRequirements.size;
    OE_VULKAN_CHECK(vkCreateBuffer(m_device, &bufferInfo, nullptr, &buffer.m_buffer));

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = Vulkan::findMemoryProperties(
        m_memoryProperties,
        memRequirements.memoryTypeBits,
        properties);

    AllocParams allocParams;
    allocParams.m_alignment = memRequirements.alignment;
    allocParams.m_allocInfo = &allocInfo;
    allocParams.m_bMap = properties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    allocParams.m_propertyFlags = properties;
    allocParams.m_size = memRequirements.size;
    allocParams.m_timeline = allocLifetime;

    Vulkan::Allocation allocation;

    alloc(allocParams, allocation);

    buffer.m_memory = allocation.m_deviceMemory;
    buffer.m_allocator = allocation.m_allocator;
    buffer.m_size = allocation.m_allocatedSize;
    buffer.m_mapped = allocation.m_mapped;

    buffer.setupDescriptor(0, size);
    buffer.bind(allocation.m_offset);

    return handle;
}

Vulkan::DynamicBufferHandle Driver::createDynamicBuffer(VkDeviceSize size)
{
    Vulkan::DynamicBuffer::CreateInfo createInfo;
    createInfo.m_alignment = m_deviceProperties.limits.minUniformBufferOffsetAlignment;
    createInfo.m_descriptorPool = m_descriptorPool;
    createInfo.m_device = m_device;
    createInfo.m_deviceProperties = &m_deviceProperties;
    createInfo.m_iBinding = 0;
    createInfo.m_memoryProperties = &m_memoryProperties;
    createInfo.m_size = size;

    return Vulkan::DynamicBufferHandle::create(createInfo);
}

void Driver::copyBuffer(
    Vulkan::BufferHandle srcBufferHandle,
    Vulkan::BufferHandle dstBufferHandle,
    size_t size)
{
    RefCountPtr<CommandBuffer> cmdBuffer(
        new CommandBuffer(m_device, m_commandPool));

    cmdBuffer->begin();

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = 0;
    copyRegion.size = size;
    vkCmdCopyBuffer(
        cmdBuffer->m_buffer,
        srcBufferHandle.getBuffer()->m_buffer,
        dstBufferHandle.getBuffer()->m_buffer,
        1,
        &copyRegion);

    cmdBuffer->end();

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuffer->m_buffer;

    vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
}

void Driver::createTexture(
    uint32 width,
    uint32 height,
    VkFormat format,
    uint32 nMips,
    const Vector<size_t>& mipSizes,
    UniqueBufferPtr buffer,
    Texture& texture)
{
    texture.m_device = m_device;
    texture.m_width = width;
    texture.m_height = height;
    texture.m_mipLevels = nMips;

    Vulkan::BufferHandle stagingBufferHandle = createBuffer(
        (VkDeviceSize)buffer->m_size,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        AllocLifetime::Frame);

    stagingBufferHandle.getBuffer()->copyTo(buffer->m_rawData, (VkDeviceSize)buffer->m_size);

    // Setup buffer copy regions for each mip level
    Vector<VkBufferImageCopy> bufferCopyRegions;
    VkDeviceSize offset = 0;

    for(uint32 i = 0; i < texture.m_mipLevels; i++)
    {
        // Setup a buffer image copy structure for the current mip level
        VkBufferImageCopy bufferCopyRegion = {};
        bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferCopyRegion.imageSubresource.mipLevel = i;
        bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
        bufferCopyRegion.imageSubresource.layerCount = 1;
        bufferCopyRegion.imageExtent.width = max<uint32>(width >> i, 1);
        bufferCopyRegion.imageExtent.height = max<uint32>(height >> i, 1);
        bufferCopyRegion.imageExtent.depth = 1;
        bufferCopyRegion.bufferOffset = offset;
        bufferCopyRegions.push_back(bufferCopyRegion);
        offset += (VkDeviceSize)mipSizes[i];
    }

    // Create optimal tiled target image on the device
    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.format = format;
    imageCreateInfo.mipLevels = nMips;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    // Set initial layout of the image to undefined
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.extent = { width, height, 1 };
    imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

    if(vkCreateImage(getDevice(), &imageCreateInfo, nullptr, &texture.m_image) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to create image");
    }

    VkMemoryAllocateInfo memAllocInfo = {};
    memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    VkMemoryRequirements memReqs = {};
    vkGetImageMemoryRequirements(getDevice(), texture.m_image, &memReqs);
    memAllocInfo.allocationSize = memReqs.size;
    memAllocInfo.memoryTypeIndex = Vulkan::findMemoryProperties(
        m_memoryProperties,
        memReqs.memoryTypeBits,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    if(vkAllocateMemory(getDevice(), &memAllocInfo, nullptr, &texture.m_deviceMemory) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to allocate memory");
    }

    if(vkBindImageMemory(getDevice(), texture.m_image, texture.m_deviceMemory, 0) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to bind image memory");
    }

    RefCountPtr<CommandBuffer> copyCmdBuffer(
        new CommandBuffer(m_device, m_commandPool));

    copyCmdBuffer->begin();

    //
    // Image memory barriers for the texture image
    //

    // The sub resource range describes the regions of the image that will be transitioned using the memory barriers below
    VkImageSubresourceRange subresourceRange = {};
    // Image only contains color data
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    // Start at first mip level
    subresourceRange.baseMipLevel = 0;
    // We will transition on all mip levels
    subresourceRange.levelCount = texture.m_mipLevels;
    // The 2D texture only has one layer
    subresourceRange.layerCount = 1;

    // Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
    VkImageMemoryBarrier imageMemoryBarrier = {};
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.image = texture.m_image;
    imageMemoryBarrier.subresourceRange = subresourceRange;
    imageMemoryBarrier.srcAccessMask = 0;
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

    // Insert a memory dependency at the proper pipeline stages that will execute the image layout transition
    // Source pipeline stage is host write/read execution (VK_PIPELINE_STAGE_HOST_BIT)
    // Destination pipeline stage is copy command execution (VK_PIPELINE_STAGE_TRANSFER_BIT)
    vkCmdPipelineBarrier(
        copyCmdBuffer->m_buffer,
        VK_PIPELINE_STAGE_HOST_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, &imageMemoryBarrier);

    // Copy mip levels from staging buffer
    vkCmdCopyBufferToImage(
        copyCmdBuffer->m_buffer,
        stagingBufferHandle.getBuffer()->m_buffer,
        texture.m_image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        static_cast<uint32_t>(bufferCopyRegions.size()),
        bufferCopyRegions.data());

    // Once the data has been uploaded we transfer to the texture image to the shader read layout, so it can be sampled from
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    // Insert a memory dependency at the proper pipeline stages that will execute the image layout transition
    // Source pipeline stage stage is copy command execution (VK_PIPELINE_STAGE_TRANSFER_BIT)
    // Destination pipeline stage fragment shader access (VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
    vkCmdPipelineBarrier(
        copyCmdBuffer->m_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, &imageMemoryBarrier);

    // Store current layout for later reuse
    texture.m_descImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    copyCmdBuffer->end();

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &copyCmdBuffer->m_buffer;

    vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);

    //
    // Create image sampler
    //

    VkSamplerCreateInfo sampler = {};
    sampler.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler.magFilter = VK_FILTER_LINEAR;
    sampler.minFilter = VK_FILTER_LINEAR;
    sampler.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler.mipLodBias = 0.0f;
    sampler.compareOp = VK_COMPARE_OP_NEVER;
    sampler.minLod = 0.0f;
    // Set max level-of-detail to mip level count of the texture
    sampler.maxLod = (float)texture.m_mipLevels;
    // Enable anisotropic filtering
    // This feature is optional, so we must check if it's supported on the device
    if(m_deviceFeatures.samplerAnisotropy)
    {
        // Use max. level of anisotropy for this example
        sampler.maxAnisotropy = m_deviceProperties.limits.maxSamplerAnisotropy;
        sampler.anisotropyEnable = VK_TRUE;
    }
    else
    {
        // The device does not support anisotropic filtering
        sampler.maxAnisotropy = 1.0;
        sampler.anisotropyEnable = VK_FALSE;
    }
    sampler.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    if(vkCreateSampler(m_device, &sampler, nullptr, &texture.m_descImageInfo.sampler) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to create sampler");
    }

    //
    // Create image view
    //

    // Textures are not directly accessed by the shaders and
    // are abstracted by image views containing additional
    // information and sub resource ranges
    VkImageViewCreateInfo view = {};
    view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view.viewType = VK_IMAGE_VIEW_TYPE_2D;
    view.format = format;
    view.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
    // The subresource range describes the set of mip levels (and array layers) that can be accessed through this image view
    // It's possible to create multiple image views for a single image referring to different (and/or overlapping) ranges of the image
    view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    view.subresourceRange.baseMipLevel = 0;
    view.subresourceRange.baseArrayLayer = 0;
    view.subresourceRange.layerCount = 1;
    // Linear tiling usually won't support mip maps
    // Only set mip map count if optimal tiling is used
    view.subresourceRange.levelCount = texture.m_mipLevels;
    // The view will be based on the texture's image
    view.image = texture.m_image;
    if(vkCreateImageView(m_device, &view, nullptr, &texture.m_descImageInfo.imageView) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to create image view");
    }
}

void Driver::updateView(ViewHandle& handle, UniqueBufferPtr viewUBO)
{
    View* view = getResource(handle);
    view->stageUBO(std::move(viewUBO));
}

void Driver::updateMaterial(PipelineHandle& handle, UniqueBufferPtr materialUBO)
{
    Pipeline* pipeline = getResource(handle);
    pipeline->stageUBO(std::move(materialUBO));
}

VkSemaphore Driver::acquireSemaphore()
{
    // Guarantee a semaphore will be available if pool is exhausted.
    if(m_semaphorePool.size() == 0)
    {
        VkSemaphoreCreateInfo semaphoreCreateInfo = {};
        semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        VkSemaphore semaphore = VK_NULL_HANDLE;
        vkCreateSemaphore(m_device, &semaphoreCreateInfo, nullptr, &semaphore);
        m_semaphorePool.push_back(semaphore);
    }

    VkSemaphore semaphore = m_semaphorePool.back();
    m_semaphorePool.pop_back();

    return semaphore;
}

void Driver::releaseSemaphore(VkSemaphore& semaphore)
{
    OE_CHECK(semaphore != VK_NULL_HANDLE, OE_CHANNEL_VULKAN_RENDERER);
    m_semaphorePool.push_back(semaphore);
    semaphore = VK_NULL_HANDLE;
}

Frame* Driver::getCurrentFrame()
{
    if(m_state.m_iImage < m_frames.size())
    {
        return &m_frames[m_state.m_iImage];
    }

    return nullptr;
}

VertexBufferHandle Driver::createVertexBufferHandle()
{
    return VertexBufferHandle(new VertexStream());
}

VertexBufferHandle Driver::createVertexBuffer(const CreateVertexBufferParams& params)
{
    VertexBufferHandle handle;
    if(params.m_vextexBufferHandle)
    {
        handle = params.m_vextexBufferHandle;
    }
    else
    {
        handle = createVertexBufferHandle();
    }

    VertexStream* vertexBuffer = getResource(handle);
    VkDevice device = getDevice();
    vertexBuffer->m_format = params.m_format;
    const size_t stride = VertexStrideTable[static_cast<uint32>(params.m_format)];
    const size_t size = params.m_buffer->m_size;
    OE_CHECK(size % stride == 0, OE_CHANNEL_VULKAN_RENDERER);
    vertexBuffer->m_nVertices = static_cast<uint32>(size / stride);

    if(params.m_usage == BufferUsage::Static)
    {
        Vulkan::BufferHandle stagingBufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Frame);

        stagingBufferHandle.getBuffer()->copyTo(params.m_buffer->m_rawData, size);

        vertexBuffer->m_bufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);

        copyBuffer(
            stagingBufferHandle,
            vertexBuffer->m_bufferHandle,
            size);
    }
    else if(params.m_usage == BufferUsage::Dynamic)
    {
        vertexBuffer->m_bufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);
        vertexBuffer->m_bufferHandle.getBuffer()->copyTo(params.m_buffer->m_rawData, size);
    }
    else
    {
        OE_CHECK(false, OE_CHANNEL_VULKAN_RENDERER);
    }

    vertexBuffer->m_bReady = true;

    return handle;
}

IndexBufferHandle Driver::createIndexBufferHandle()
{
    return IndexBufferHandle(new IndexBuffer());
}

IndexBufferHandle Driver::createIndexBuffer(const CreateIndexBufferParams& params)
{
    IndexBufferHandle handle;
    if(params.m_indexBufferHandle)
    {
        handle = params.m_indexBufferHandle;
    }
    else
    {
        handle = createIndexBufferHandle();
    }

    IndexBuffer* indexBuffer = getResource(handle);
    VkDevice device = getDevice();
    
    const size_t size = params.m_buffer->m_size;

    if(params.m_usage == BufferUsage::Static)
    {
        Vulkan::BufferHandle stagingBufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Frame);

        stagingBufferHandle.getBuffer()->copyTo(
            params.m_buffer->m_rawData,
            size);

        indexBuffer->m_bufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);

        copyBuffer(
            stagingBufferHandle,
            indexBuffer->m_bufferHandle,
            size);
    }
    else if(params.m_usage == BufferUsage::Dynamic)
    {
        indexBuffer->m_bufferHandle = createBuffer(
            static_cast<VkDeviceSize>(size),
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            Vulkan::Driver::AllocLifetime::Persistent);
        indexBuffer->m_bufferHandle.getBuffer()->copyTo(params.m_buffer->m_rawData, size);
    }
    else
    {
        OE_CHECK(false, OE_CHANNEL_VULKAN_RENDERER);
    }

    if(params.m_indexType == IndexType::U16)
    {
        indexBuffer->m_indexType = VK_INDEX_TYPE_UINT16;
        indexBuffer->m_nIndices = static_cast<uint32>(size / sizeof(uint16));
    }
    else
    {
        indexBuffer->m_indexType = VK_INDEX_TYPE_UINT32;
        indexBuffer->m_nIndices = static_cast<uint32>(size / sizeof(uint32));
    }

    indexBuffer->m_bReady = true;

    return handle;
}

VertexShaderHandle Driver::createVertexShader(
    UniqueBufferPtr buffer
    DBG_STR_PARAM_ADD(name))
{
    VertexShaderHandle handle(new VertexShader());
    VertexShader* vertexShader = getResource(handle);
    DBG_ONLY(vertexShader->m_name = name);
    vertexShader->m_device = getDevice();
    vertexShader->createShaderModule(std::move(buffer));
    vertexShader->m_bReady = true;

    return handle;
}

PixelShaderHandle Driver::createPixelShader(
    UniqueBufferPtr buffer
    DBG_STR_PARAM_ADD(name))
{
    PixelShaderHandle handle(new PixelShader());
    PixelShader* pixelShader = getResource(handle);
    DBG_ONLY(pixelShader->m_name = name);
    pixelShader->m_device = getDevice();
    pixelShader->createShaderModule(std::move(buffer));
    pixelShader->m_bReady = true;

    return handle;
}

RenderPassHandle Driver::createRenderPassHandle()
{
    return RenderPassHandle(new RenderPass());
}

RenderPassHandle Driver::createRenderPass(const CreateRenderPassParams& params)
{
    OE_CHECK(params.m_descriptor, OE_CHANNEL_VULKAN_RENDERER);
    const RenderPassDescriptor& descriptor = (*params.m_descriptor);

    RenderPassHandle handle;
    if(params.m_renderPassHandle)
    {
        handle = params.m_renderPassHandle;
    }
    else
    {
        handle = createRenderPassHandle();
    }

    // Create render pass.
    RenderPass* renderPass = getResource(handle);
    DBG_ONLY(renderPass->m_name = params.m_name);
    renderPass->m_device = m_device;
    renderPass->m_bPersistent = params.m_descriptor->m_persistent;

    // There will be a maximum of n frames frame buffer per render pass.
    // If the backbuffer is not used by the frame buffer,
    // only the first one will be used for all frames.
    renderPass->m_framebuffers.resize(getNumImages());
    for (size_t i = 0; i < renderPass->m_framebuffers.size(); ++i)
    {
        renderPass->m_framebuffers[i] = VK_NULL_HANDLE;
    }

    // Create semaphores.
    VkSemaphoreCreateInfo semaphoreCreateInfo = {};
    semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    const uint32 nFrames = getNumImages();
    renderPass->m_perFrameCompleteSemaphore.resize(nFrames);
    for(uint32 iFrame = 0; iFrame < nFrames; ++iFrame)
    {
        vkCreateSemaphore(
            m_device,
            &semaphoreCreateInfo,
            nullptr,
            &renderPass->m_perFrameCompleteSemaphore[iFrame]);
    }

    // Create command buffers.
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = m_commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = nFrames;

    const uint32 nSubpasses = static_cast<uint32>(descriptor.m_subpasses.size());
    OE_CHECK(nSubpasses >= 1, OE_CHANNEL_VULKAN);


    UnorderedMap<RenderTarget*, uint32> rtToAttachmentId;
    renderPass->m_renderTargets.reserve(descriptor.m_attachments.size());
    for (size_t i = 0; i < descriptor.m_attachments.size(); ++i)
    {
        renderPass->m_renderTargets.push_back(descriptor.m_attachments[i].m_handle);
        RenderTarget* rt = getResource(descriptor.m_attachments[i].m_handle);
        rtToAttachmentId[rt] = i;
    }

    renderPass->m_subpasses.resize(nSubpasses);
    for(uint iSubpass = 0; iSubpass < nSubpasses; ++iSubpass)
    {
        auto& subpass = renderPass->m_subpasses[iSubpass];
        const auto& paramSubpass = descriptor.m_subpasses[iSubpass];

        subpass.m_inputRenderTargets.reserve(paramSubpass.m_inputRenderTargets.size());
        for(auto& inRT : paramSubpass.m_inputRenderTargets)
        {
            RenderTarget* attachment = getResource(inRT.m_handle);
            subpass.m_inputRenderTargets.push_back(inRT.m_handle);
        }

        subpass.m_outputRenderTargets.reserve(paramSubpass.m_outputRenderTargets.size());
        for(auto& outRT : paramSubpass.m_outputRenderTargets)
        {
            RenderTarget* attachment = getResource(outRT.m_handle);
            subpass.m_outputRenderTargets.push_back(outRT.m_handle);
        }
    }

    const uint32 nAttachments = static_cast<uint32>(renderPass->m_renderTargets.size());

    // Map render target to its index in rtAttachments.
    for(uint32 i = 0; i < nAttachments; ++i)
    {
        RenderTarget* rt = getResource(renderPass->m_renderTargets[i]);
        rtToAttachmentId[rt] = i;
    }

    // Setup attachments.
    Vector<VkAttachmentDescription> attachmentDescs;
    attachmentDescs.resize(nAttachments, {});
    renderPass->m_clearValues.resize(nAttachments);
    
    for(uint32 i = 0; i < nAttachments; i++)
    {
        RenderTarget* rt = getResource(renderPass->m_renderTargets[i]);

        attachmentDescs[i].samples = VK_SAMPLE_COUNT_1_BIT;
        switch (descriptor.m_attachments[i].m_loadOp)
        {
        case RenderTargetLoadOperation::NoCare:
            attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            break;

        case RenderTargetLoadOperation::Preserve:
            attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            if (rt->m_type == RenderTargetType::Backbuffer)
            {
                attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            }
            else
            {
                attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }
            break;

        case RenderTargetLoadOperation::Clear:
            attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            break;

        default:
            OE_CHECK(false, OE_CHANNEL_VULKAN_RENDERER);
        }

        attachmentDescs[i].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachmentDescs[i].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachmentDescs[i].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

        switch(rt->m_type)
        {
        case RenderTargetType::Color:
        attachmentDescs[i].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        renderPass->m_clearValues[i].color = { 0.0f, 0.0f, 0.0f, 0.0f };
        break;

        case RenderTargetType::Depth:
        attachmentDescs[i].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        renderPass->m_clearValues[i].depthStencil = { 1.0f, 0 };
        break;

        case RenderTargetType::Backbuffer:
        attachmentDescs[i].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        renderPass->m_clearValues[i].color = { 0.0f, 0.0f, 0.0f, 0.0f };
        renderPass->m_iBackbufferAttachment = static_cast<uint32>(i);
        break;

        default:
        OE_CHECK_MSG(
            false,
            OE_CHANNEL_VULKAN,
            "Unknown render target type %d", static_cast<uint32>(rt->m_type));
        }

        attachmentDescs[i].format = rt->m_format;
    }

    // Setup subpasses.
    Vector<VkSubpassDescription> subpassDescriptions(nSubpasses);

    struct SubpassReferences
    {
        Vector<VkAttachmentReference> m_inputRefs;
        Vector<VkAttachmentReference> m_colorRefs;
        VkAttachmentReference m_depthRef = {};
        bool m_bUseDepth = false;
    };

    Vector<SubpassReferences> perSubpassRefs(nSubpasses);

    for(uint32 iSubpass = 0; iSubpass < nSubpasses; ++iSubpass)
    {
        auto& subpassDesc = subpassDescriptions[iSubpass];
        auto& subpassRefs = perSubpassRefs[iSubpass];
        auto& subpass = renderPass->m_subpasses[iSubpass];

        // Find input references.
        if(iSubpass > 0)
        {
            for(auto inRTHandle : subpass.m_inputRenderTargets)
            {
                RenderTarget* inRT = getResource(inRTHandle);
                uint32 iRef = rtToAttachmentId[inRT];
                VkAttachmentReference inputRef = { iRef, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
                subpassRefs.m_inputRefs.push_back(inputRef);
            }
        }

        // Find output references.
        for(auto outRTHandle : subpass.m_outputRenderTargets)
        {
            RenderTarget* outRT = getResource(outRTHandle);
            uint32 iRef = rtToAttachmentId[outRT];
            if(outRT->m_type == RenderTargetType::Color || outRT->m_type == RenderTargetType::Backbuffer)
            {
                VkAttachmentReference colorRef = { iRef, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
                subpassRefs.m_colorRefs.push_back(colorRef);
                ++subpass.m_nColorAttachments;
            }
            else if(outRT->m_type == RenderTargetType::Depth)
            {
                subpassRefs.m_depthRef = { iRef, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
                subpassRefs.m_bUseDepth = true;
            }
        }

        subpassDesc = {};
        subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpassDesc.inputAttachmentCount = static_cast<uint32>(subpassRefs.m_inputRefs.size());
        subpassDesc.pInputAttachments = subpassRefs.m_inputRefs.data();
        subpassDesc.colorAttachmentCount = static_cast<uint32>(subpassRefs.m_colorRefs.size());
        subpassDesc.pColorAttachments = subpassRefs.m_colorRefs.data();
        if(subpassRefs.m_bUseDepth)
        {
            subpassDesc.pDepthStencilAttachment = &subpassRefs.m_depthRef;
        }
    }

    Vector<VkSubpassDependency> dependencies;

    {
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dependency.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
        dependency.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
        dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        dependencies.push_back(dependency);
    }

    for(uint iSubpass = 0; iSubpass < nSubpasses - 1; ++iSubpass)
    {
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = iSubpass;
        dependency.dstSubpass = iSubpass + 1;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependency.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT;
        dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        dependencies.push_back(dependency);
    }

    {
        // Last pass.
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = nSubpasses - 1;
        dependency.dstSubpass = VK_SUBPASS_EXTERNAL;
        dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dependency.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
        dependency.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
        dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        dependencies.push_back(dependency);
    }


    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.pAttachments = attachmentDescs.data();
    renderPassInfo.attachmentCount = nAttachments;
    renderPassInfo.subpassCount = static_cast<uint32>(renderPass->m_subpasses.size());
    renderPassInfo.pSubpasses = subpassDescriptions.data();
    renderPassInfo.dependencyCount = static_cast<uint32>(dependencies.size());
    renderPassInfo.pDependencies = dependencies.data();

    OE_VULKAN_CHECK(vkCreateRenderPass(
        m_device,
        &renderPassInfo,
        nullptr,
        &renderPass->m_renderPass));

    renderPass->m_bReady = true;
    return handle;
}

RenderTargetHandle Driver::createRenderTarget(
    const CreateRenderTargetParams& params
    DBG_STR_PARAM_ADD(name))
{
    RenderTarget* renderTarget = nullptr;
    VkImageLayout imageLayout= VK_IMAGE_LAYOUT_UNDEFINED;

    bool bSwapchainRenderTarget = false;

    switch(params.m_type)
    {
    case RenderTargetType::Color:
    {
        auto* defaultRenderTarget = new DefaultRenderTarget();
        defaultRenderTarget->m_format = toVulkanFormat(params.m_textureFormat);
        renderTarget = defaultRenderTarget;
        renderTarget->m_usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        renderTarget->m_aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        break;
    }

    case RenderTargetType::Depth:
    {
        auto* defaultRenderTarget = new DefaultRenderTarget();
        defaultRenderTarget->m_format = findDepthFormat();
        renderTarget = defaultRenderTarget;
        renderTarget->m_usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        renderTarget->m_aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
        imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        break;
    }

    case RenderTargetType::Backbuffer:
    {
        auto* swapchainRenderTarget = new SwapchainRenderTarget();
        swapchainRenderTarget->m_format = toVulkanFormat(params.m_textureFormat);
        renderTarget = swapchainRenderTarget;
        bSwapchainRenderTarget = true;
        break;
    }

    default:
    {
        OE_CHECK_MSG(
            false,
            OE_CHANNEL_VULKAN,
            "Unknown render target type %d", static_cast<uint32>(params.m_type));
    }
    }

    if(params.m_flags.isSet(RenderTargetFlags::SubpassInput))
    {
        renderTarget->m_usage |= VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
    }

    renderTarget->m_type = params.m_type;
    renderTarget->m_device = m_device;

    RenderTarget::SetupImagesParams setupImagesParams;
    setupImagesParams.m_screenSize = m_screenSize_RenderThread;
    setupImagesParams.m_memoryProperties = m_memoryProperties;
    setupImagesParams.m_swapChain = m_swapChain;
    setupImagesParams.m_swapChainImageViews = m_swapchainImageViews;
    setupImagesParams.m_bNVidiaDedicatedAlloc = 
        m_deviceExtensionFlags.isSet(Vulkan::DeviceExtensionFlags::NVDedicatedAllocation);
    renderTarget->setupImages(setupImagesParams);

    if (bSwapchainRenderTarget)
    {
        // The render thread is dependent of the swapchain being created.
        handleReady(ReadyFlags::Swapchain);
    }

    DBG_ONLY(renderTarget->m_name = name);
    renderTarget->m_bReady = true;

    return RenderTargetHandle(renderTarget);
}

PipelineHandle Driver::createPipeline(
    const CreatePipelineParams& params
    DBG_STR_PARAM_ADD(name))
{
    PipelineHandle handle(new Pipeline());
    Pipeline* pipeline = getResource(handle);
    pipeline->m_device = getDevice();
    pipeline->m_descPool = getDescriptorPool();
    pipeline->m_viewHandle = params.m_viewHandle;
    DBG_ONLY(pipeline->m_name = name);

    Array<VkPipelineShaderStageCreateInfo, 2> shaderStagesInfo;
    shaderStagesInfo[0] = {};
    shaderStagesInfo[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStagesInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    VertexShader* vertexShader = getResource(params.m_vsHandle);
    OE_CHECK(vertexShader->m_shaderModule != VK_NULL_HANDLE, OE_CHANNEL_VULKAN_RENDERER);
    shaderStagesInfo[0].module = vertexShader->m_shaderModule;
    shaderStagesInfo[0].pName = "main";

    shaderStagesInfo[1] = {};
    shaderStagesInfo[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStagesInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    PixelShader* pixelShader = getResource(params.m_psHandle);
    OE_CHECK(pixelShader->m_shaderModule != VK_NULL_HANDLE, OE_CHANNEL_VULKAN_RENDERER);
    shaderStagesInfo[1].module = pixelShader->m_shaderModule;
    shaderStagesInfo[1].pName = "main";

    // Follow AttributeType enum.
    const uint NumFormats = 5;
    static const VkFormat attribFormat[NumFormats] =
    {
        VK_FORMAT_R32_SFLOAT,
        VK_FORMAT_R32G32_SFLOAT,
        VK_FORMAT_R32G32B32_SFLOAT,
        VK_FORMAT_R32G32B32A32_SFLOAT,
        VK_FORMAT_B8G8R8A8_UNORM
    };

    Vector<VkVertexInputAttributeDescription> attribDescs;
    uint32 stride = 0;
    for (uint32 iAttrib = 0; iAttrib < params.m_vertexShaderInputAttribs.size(); ++iAttrib)
    {
        const uint32 attribAsUInt = (uint32)params.m_vertexShaderInputAttribs[iAttrib];
        OE_CHECK(attribAsUInt < NumFormats, OE_CHANNEL_VULKAN_RENDERER);

        VkVertexInputAttributeDescription attribDesc = {};
        attribDesc.binding = 0;
        attribDesc.location = iAttrib;
        attribDesc.format = attribFormat[attribAsUInt];
        attribDesc.offset = stride;

        attribDescs.push_back(attribDesc);

        stride += (uint32)BindingTypeSizeTable[attribAsUInt];
    }

    VkVertexInputBindingDescription inputBindingDesc = {};
    inputBindingDesc.binding = 0;
    inputBindingDesc.stride = stride;
    inputBindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    const uint32 nFrames = getNumImages();

    // Use the same layout for all frames.
    ShaderBinding* shaderBinding = getResource(params.m_shaderBindingHandle);
    const uint32 iMateriaDescSetlLayout = 1;
    Vector<VkDescriptorSetLayout> materialDescSetLayouts(nFrames, shaderBinding->m_descriptorSetLayouts[iMateriaDescSetlLayout]);

    pipeline->m_descSets.resize((size_t)nFrames);
    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = getDescriptorPool();
    descriptorSetAllocateInfo.descriptorSetCount = nFrames;
    descriptorSetAllocateInfo.pSetLayouts = materialDescSetLayouts.data();
    if (vkAllocateDescriptorSets(
        getDevice(),
        &descriptorSetAllocateInfo, pipeline->m_descSets.data()) != VK_SUCCESS)
    {
        OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to create descriptor sets");
    }

    pipeline->m_shaderBindings = params.m_shaderBindings;

    for (uint32 iFrame = 0; iFrame < nFrames; ++iFrame)
    {
        BufferHandle materialUBOHandle;
        if (params.m_uboSize > 0)
        {
            materialUBOHandle = createBuffer(params.m_uboSize,
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                Vulkan::Driver::AllocLifetime::Persistent);
            pipeline->m_materialUBOs.push_back(materialUBOHandle);
        }
    }

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.pVertexBindingDescriptions = &inputBindingDesc;
    vertexInputInfo.vertexAttributeDescriptionCount = (uint32)attribDescs.size();
    vertexInputInfo.pVertexAttributeDescriptions = attribDescs.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;

    if (params.m_topology == PrimitiveType::TriangleList)
    {
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }
    else if (params.m_topology == PrimitiveType::LineList)
    {
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    }
    else
    {
        OE_CHECK(false, OE_CHANNEL_VULKAN_RENDERER);
    }

    inputAssembly.primitiveRestartEnable = VK_FALSE;

    // Values in the Viewport will be ignored because we use VK_DYNAMIC_STATE_VIEWPORT.
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = 0.0f;
    viewport.height = 0.0f;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    // Values of the scissor will be ignored because we use VK_DYNAMIC_STATE_SCISSOR.
    VkRect2D scissor = {};
    scissor.offset = { 0, 0 };
    scissor.extent = {0, 0};

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    RenderPass* renderPass = getResource(params.m_renderPassHandle);
    OE_CHECK(params.m_iSubpass < renderPass->m_subpasses.size(), OE_CHANNEL_VULKAN_RENDERER);

    Vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments;
    const uint32 nColorAttachements = renderPass->m_subpasses[params.m_iSubpass].m_nColorAttachments;
    const bool bBlendEnable = params.m_bAlphaBlendEnabled;
    for(uint i = 0; i < nColorAttachements; ++i)
    {
        colorBlendAttachments.push_back({});
        auto& attachment = colorBlendAttachments.back();
        attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        attachment.blendEnable = bBlendEnable;
        attachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        attachment.colorBlendOp = VK_BLEND_OP_ADD;
        attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        attachment.alphaBlendOp = VK_BLEND_OP_ADD;
    }

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = nColorAttachements;
    colorBlending.pAttachments = colorBlendAttachments.data();
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    VkPipelineDepthStencilStateCreateInfo depthStencil = {};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f;
    depthStencil.maxDepthBounds = 1.0f;
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front = {};
    depthStencil.back = {};

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = static_cast<uint32>(shaderBinding->m_descriptorSetLayouts.size());
    pipelineLayoutInfo.pSetLayouts = shaderBinding->m_descriptorSetLayouts.data();
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = nullptr;

    if(vkCreatePipelineLayout(
        getDevice(),
        &pipelineLayoutInfo,
        nullptr,
        &pipeline->m_pipelineLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Vulkan PipelineLayout cannot be created");
    }

    VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
    dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    Vector<VkDynamicState> dynamicStateFlags = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    dynamicStateCreateInfo.dynamicStateCount = (uint32)dynamicStateFlags.size();
    dynamicStateCreateInfo.pDynamicStates = dynamicStateFlags.data();

    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStagesInfo.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = &depthStencil;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = &dynamicStateCreateInfo;
    pipelineInfo.layout = pipeline->m_pipelineLayout;
    pipelineInfo.renderPass = renderPass->m_renderPass;
    pipelineInfo.subpass = params.m_iSubpass;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    OE_VULKAN_CHECK(vkCreateGraphicsPipelines(
        getDevice(),
        VK_NULL_HANDLE,
        1,
        &pipelineInfo,
        nullptr,
        &pipeline->m_graphicsPipeline));

    pipeline->m_bReady = true;

    return handle;
}

DrawInstanceHandle Driver::createDrawInstance(
    const CreateDrawInstanceParams& params
    DBG_STR_PARAM_ADD(name))
{
    DrawInstanceHandle handle(new DrawInstance());
    
    DrawInstance* drawInstance = getResource(handle);
    DBG_ONLY(drawInstance->m_name = name);

    drawInstance->m_device = getDevice();
    drawInstance->m_descriptorPool = getDescriptorPool();
    drawInstance->m_pipelineHandle = params.m_pipelineHandle;
    drawInstance->m_vertexBufferHandle = params.m_vertexBufferHandle;
    drawInstance->m_indexBufferHandle = params.m_indexBufferHandle;
    drawInstance->m_bReady = true;

    return handle;
}

TextureHandle Driver::createTextureHandle()
{
    TextureHandle handle(new Texture());
    return handle;
}

TextureHandle Driver::createTexture(CreateTextureParams& params)
{
    TextureHandle handle;
    if(params.m_handle)
    {
        // Reuse the handle passed in.
        handle = params.m_handle;
    }
    else
    {
        handle = createTextureHandle();
    }

    Texture* texture = getResource(handle);
    DBG_ONLY(texture->m_name = params.m_name);

    VkFormat vkFormat = Vulkan::toVulkanFormat(params.m_format);

    createTexture(
        params.m_width,
        params.m_height,
        vkFormat,
        params.m_nMips,
        params.m_mipSizes,
        std::move(params.m_data),
        *texture);

    texture->m_bReady = true;

    return handle;
}

ViewHandle Driver::createView(
    const CreateViewParams& params
    DBG_STR_PARAM_ADD(name))
{
    OE_CHECK(params.m_uboSize > 0, OE_CHANNEL_VULKAN);

    ViewHandle handle(new View());
    View* view = getResource(handle);
    DBG_ONLY(view->m_name = name);

    view->m_device = m_device;
    view->m_pool = m_descriptorPool;

    // Create layout.
    VkDescriptorSetLayoutBinding viewDescSetLayoutBind = {};
    viewDescSetLayoutBind.binding = 0;
    viewDescSetLayoutBind.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    viewDescSetLayoutBind.descriptorCount = 1;
    viewDescSetLayoutBind.stageFlags = toVulkanStageFlags(params.m_stages);
    viewDescSetLayoutBind.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &viewDescSetLayoutBind;

    OE_VULKAN_CHECK(vkCreateDescriptorSetLayout(m_device, &layoutInfo, nullptr, &view->m_layout));

    const uint32 nImages = getNumImages();
    view->m_perFrame.resize(nImages);

    VkDescriptorSetAllocateInfo descSetAllocInfo = {};
    descSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAllocInfo.descriptorPool = m_descriptorPool;
    descSetAllocInfo.descriptorSetCount = 1;
    descSetAllocInfo.pSetLayouts = &view->m_layout;

    for(uint32 iImage = 0; iImage < nImages; ++iImage)
    {
        VkDescriptorSet descSet = VK_NULL_HANDLE;
        OE_VULKAN_CHECK(vkAllocateDescriptorSets(m_device, &descSetAllocInfo, &descSet));

        auto viewUBOHandle = createBuffer(params.m_uboSize,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            AllocLifetime::Persistent);

        VkWriteDescriptorSet writeDescSet = {};
        writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writeDescSet.pBufferInfo = &viewUBOHandle.getBuffer()->m_desc;
        writeDescSet.descriptorCount = 1;
        writeDescSet.dstArrayElement = 0;
        writeDescSet.dstBinding = 0;
        writeDescSet.dstSet = descSet;

        vkUpdateDescriptorSets(
                m_device,
                1,
                &writeDescSet,
                0,
                nullptr);

        view->m_perFrame[iImage].m_descSet = descSet;
        view->m_perFrame[iImage].m_UBO = viewUBOHandle;
    }

    return handle;
}

ShaderBindingHandle Driver::createShaderBinding(
    const ShaderBindingLayout& layout
    DBG_STR_PARAM_ADD(name))
{
    ShaderBindingHandle handle(new ShaderBinding());
    ShaderBinding* shaderBinding = getResource(handle);
    shaderBinding->m_device = m_device;

    for(const auto& slot : layout.m_layouts)
    {
        Vector<VkDescriptorSetLayoutBinding> vkLayoutBindings;
        for(size_t iLayoutBinding = 0; iLayoutBinding < slot.m_bindings.size(); ++iLayoutBinding)
        {
            const ShaderBindingLayout::Binding& binding = slot.m_bindings[iLayoutBinding];
            VkDescriptorSetLayoutBinding vkLayoutBinding;
            vkLayoutBinding.binding = static_cast<uint32>(iLayoutBinding);

            // Binding Type
            switch(binding.m_type)
            {
            case ShaderBindingLayout::Binding::Type::Uniform:
            vkLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            break;

            case ShaderBindingLayout::Binding::Type::UniformDynamic:
            vkLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
            break;

            case ShaderBindingLayout::Binding::Type::Sampler:
            vkLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            break;

            case ShaderBindingLayout::Binding::Type::InputAttachment:
            vkLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
            break;

            default:
            OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Unknown binding type");
            }

            vkLayoutBinding.descriptorCount = 1;
            vkLayoutBinding.stageFlags = 0;

            // Binding Stage
            if(binding.m_stages.isSet(ShaderType::VertexShader))
            {
                vkLayoutBinding.stageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
            }

            if(binding.m_stages.isSet(ShaderType::PixelShader))
            {
                vkLayoutBinding.stageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
            }

            vkLayoutBinding.pImmutableSamplers = nullptr;
            vkLayoutBindings.push_back(vkLayoutBinding);
        }

        VkDescriptorSetLayoutCreateInfo layoutInfo = {};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = static_cast<uint32>(vkLayoutBindings.size());
        layoutInfo.pBindings = vkLayoutBindings.data();

        VkDescriptorSetLayout vkDescLayout = VK_NULL_HANDLE;
        if(vkCreateDescriptorSetLayout(getDevice(), &layoutInfo, nullptr, &vkDescLayout) != VK_SUCCESS)
        {
            throw std::runtime_error("Vulkan DescriptorSetLayout cannot be created");
        }

        shaderBinding->m_descriptorSetLayouts.push_back(vkDescLayout);
    }

    return handle;
}

RenderPipelineHandle Driver::createRenderPipeline(
    const CreateRenderPipelineParams& params
    DBG_STR_PARAM_ADD(name))
{
    RenderPipelineHandle handle(new RenderPipeline());
    RenderPipeline* renderPipeline = getResource(handle);

    for(auto& desc : params.m_renderPassDescriptors)
    {
        RenderPipeline::RenderPassNode node;
        node.m_renderPass = desc.m_renderPass;

        for(auto& dependency : desc.m_waitFor)
        {
            node.m_dependencies.push_back(dependency);
        }

        renderPipeline->m_renderSequence.push_back(node);
    }

    // Tag render passes that others depend on so they can signal a semaphore when their command queue is executed.
    for (auto& node : renderPipeline->m_renderSequence)
    {
        for (auto& dependency : node.m_dependencies)
        {
            RenderPass* renderPass = getResource(dependency);
            renderPass->m_bDependentOn = true;
        }
    }

    // Tag the last render pass.
    OE_CHECK(renderPipeline->m_renderSequence.size() > 0, OE_CHANNEL_VULKAN_RENDERER);
    RenderPassHandle lastRenderPassHandle = renderPipeline->m_renderSequence.back().m_renderPass;
    RenderPass* lastRenderPass = getResource(lastRenderPassHandle);
    lastRenderPass->m_bLastRenderPass = true;

    handle.m_resource->m_bReady = true;

    m_state.m_renderPipelineHandle = handle;

    handleReady(ReadyFlags::RenderPipeline);

    return handle;
}

void Driver::handleReady(ReadyFlags flag)
{
    UniqueLock lock(m_readyMutex);

    m_readyFlags.set(flag);

    if(m_readyFlags.areAllSet(static_cast<uint32>(ReadyFlags::Count)))
    {
        m_bReady = true;
    }

    m_readyCondition.notify_all();
}

void Driver::cleanPendingDeleteResources()
{
    Frame* currentFrame = getCurrentFrame();

    if(currentFrame)
    {
        // Move the delete responsibility to the current frame.
        OE_CHECK(currentFrame->m_pendingDeleteResources.empty(), OE_CHANNEL_VULKAN_RENDERER);

        LockGuard lock(m_pendingDeleteResourcesMutex);
        currentFrame->m_pendingDeleteResources = std::move(m_pendingDeleteResources);
    }
    else
    {
        // Without current frame, the driver is responsible for deletion.
        waitUntilGPUIdle();

        while(true)
        {
            Vector<GraphicResource*> resourcesToDelete;
            {
                LockGuard lock(m_pendingDeleteResourcesMutex);
                resourcesToDelete = std::move(m_pendingDeleteResources);
            }

            for(auto* graphicResource : resourcesToDelete)
            {
                delete graphicResource;
            }

            // Deleting a graphic resource can cause other resources to become pending delete.
            {
                LockGuard lock(m_pendingDeleteResourcesMutex);
                if(m_pendingDeleteResources.empty())
                {
                    break;
                }
            }
        }
    }
}

#ifdef _DEBUG
void Driver::setupDebugMessenger()
{
    if(m_flags.isSet(VulkanFlags::EnableValidationLayers))
    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfo.messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = g_debugCallback;
        createInfo.pUserData = nullptr; // Optional

        if(createDebugUtilsMessengerEXT(&createInfo) != VK_SUCCESS)
        {
            OE_CHECK_MSG(false, OE_CHANNEL_VULKAN_RENDERER, "Failed to setup debug messenger");
        }
    }
}

VkResult Driver::createDebugUtilsMessengerEXT(
    const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo)
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkCreateDebugUtilsMessengerEXT");
    if(func != nullptr)
    {
        return func(m_instance, pCreateInfo, nullptr, &m_debugMessenger);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void Driver::destroyDebugUtilsMessengerEXT()
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkDestroyDebugUtilsMessengerEXT");
    if(func != nullptr)
    {
        func(m_instance, m_debugMessenger, nullptr);
    }
}
#endif

} // namespace Vulkan

} // namespace OE_Graphic


#ifndef __OE_GRAPHIC_VULKAN_RESOURCES_H__
#define __OE_GRAPHIC_VULKAN_RESOURCES_H__

// OE_Core
#include <buffer.h>

#include "vulkan.h"
#include "vulkan_buffer.h"

namespace OE_Graphic
{

struct VertexStream : GraphicResource
{
    Vulkan::BufferHandle m_bufferHandle;
    uint32 m_nVertices = 0;
    VertexDeclaration m_format = VertexDeclaration::Count;
};

struct IndexBuffer : GraphicResource
{
    Vulkan::BufferHandle m_bufferHandle;
    uint32 m_nIndices = 0;
    VkIndexType m_indexType = VK_INDEX_TYPE_UINT16;
};

struct Shader : GraphicResource
{
    virtual ~Shader()
    {
        destroyShaderModule();
    }

    VkShaderModule m_shaderModule = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;

    void createShaderModule(OE_Core::UniqueBufferPtr buffer)
    {
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = buffer->m_size;
        createInfo.pCode = reinterpret_cast<const uint32*>(buffer->m_rawData);

        VkResult result = vkCreateShaderModule(
            m_device,
            &createInfo,
            nullptr,
            &m_shaderModule);

        OE_CHECK(result == VK_SUCCESS, OE_CHANNEL_VULKAN);
    }

    void destroyShaderModule()
    {
        vkDestroyShaderModule(
            m_device,
            m_shaderModule,
            nullptr);
    }
};

struct VertexShader : Shader
{
};

struct PixelShader : Shader
{
};

struct ShaderBinding : GraphicResource
{
    virtual ~ShaderBinding()
    {
        for(auto& descSetLayout : m_descriptorSetLayouts)
        {
            vkDestroyDescriptorSetLayout(m_device, descSetLayout, nullptr);
        }
    }

    Vector<VkDescriptorSetLayout> m_descriptorSetLayouts;
    VkDevice m_device = VK_NULL_HANDLE;
};

struct Pipeline : public GraphicResource
{
    virtual ~Pipeline()
    {
        vkFreeDescriptorSets(m_device, m_descPool, (uint32)m_descSets.size(), m_descSets.data());
        vkDestroyPipeline(m_device, m_graphicsPipeline, nullptr);
        vkDestroyPipelineLayout(m_device, m_pipelineLayout, nullptr);
    }

    struct SetupWriteDescriptorSet
    {
        VkDevice m_device = VK_NULL_HANDLE;
        uint32 m_iFrame = 0;
        VkSampler m_colorAttachmentSampler = VK_NULL_HANDLE;
    };

    void setupWriteDescriptorSet(const SetupWriteDescriptorSet& params);

    void stageUBO(OE_Core::UniqueBufferPtr ubo)
    {
        LockGuard lock(m_mutex);
        m_stagedUBO = std::move(ubo);
    }

    void applyStagedUBO(uint32 iFrame)
    {
        LockGuard lock(m_mutex);
        if(m_stagedUBO)
        {
            m_materialUBOs[iFrame].getBuffer()->copyTo(
                m_stagedUBO->m_rawData,
                m_stagedUBO->m_size);
            m_stagedUBO.reset();
        }
    }

    VkDevice m_device = VK_NULL_HANDLE;
    VkDescriptorPool m_descPool = VK_NULL_HANDLE;
    Vector<VkDescriptorSet> m_descSets;
    Vector<Vulkan::BufferHandle> m_materialUBOs;
    Vector<ShaderBind> m_shaderBindings;
    ViewHandle m_viewHandle;
    VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;
    VkPipeline m_graphicsPipeline = VK_NULL_HANDLE;
    Mutex m_mutex;
    OE_Core::UniqueBufferPtr m_stagedUBO;
};

struct Texture : GraphicResource
{
    virtual ~Texture()
    {
        vkDestroyImageView(m_device, m_descImageInfo.imageView, nullptr);
        vkDestroySampler(m_device, m_descImageInfo.sampler, nullptr);
        vkFreeMemory(m_device, m_deviceMemory, nullptr);
        vkDestroyImage(m_device, m_image, nullptr);
    }

    VkDevice m_device = VK_NULL_HANDLE;
    VkImage m_image = VK_NULL_HANDLE;
    VkImageView m_view = VK_NULL_HANDLE;
    VkSampler m_sampler = VK_NULL_HANDLE;
    VkDeviceMemory m_deviceMemory = VK_NULL_HANDLE;
    VkDescriptorImageInfo m_descImageInfo = {};

    uint32 m_width = 0;
    uint32 m_height = 0;
    uint32 m_mipLevels = 0;
};

struct DrawInstance : public GraphicResource
{
    VkDevice m_device = VK_NULL_HANDLE;
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;
    PipelineHandle m_pipelineHandle;
    VertexBufferHandle m_vertexBufferHandle;
    IndexBufferHandle m_indexBufferHandle;
    Vector<Vulkan::BufferHandle> m_uniformBuffers;
    VkDescriptorSet m_descSet = VK_NULL_HANDLE;
    uint32 m_offset = 0;

    virtual bool isReady() const
    {
        bool bReady = GraphicResource::isReady();
        bReady &= m_pipelineHandle.isReady();
        bReady &= m_vertexBufferHandle.isReady();

        if (m_indexBufferHandle)
        {
            bReady &= m_indexBufferHandle.isReady();
        }
        
        return bReady;
    }
};

struct RenderTarget : public GraphicResource
{
    virtual ~RenderTarget()
    {
    }

    struct SetupImagesParams
    {
        ScreenSize m_screenSize;
        VkPhysicalDeviceMemoryProperties m_memoryProperties = {};
        VkSwapchainKHR m_swapChain = VK_NULL_HANDLE;
        Vector<VkImageView> m_swapChainImageViews;
        bool m_bNVidiaDedicatedAlloc = false;
    };

    virtual void setupImages(const SetupImagesParams& params) = 0;

    RenderTargetType m_type = RenderTargetType::Count;
    VkDevice m_device = VK_NULL_HANDLE;
    VkFormat m_format = VK_FORMAT_UNDEFINED;
    ScreenSize m_screenSize;
    VkImageUsageFlags m_usage = 0;
    VkImageAspectFlags m_aspectMask = 0;
    bool m_bScreenSizeDependent = true;
};

struct DefaultRenderTarget : public RenderTarget
{
    virtual ~DefaultRenderTarget()
    {
        vkDestroyImageView(m_device, m_imageView, nullptr);
        vkFreeMemory(m_device, m_memory, nullptr);
        vkDestroyImage(m_device, m_image, nullptr);
    }

    virtual void setupImages(const SetupImagesParams& params) override;

    VkImage m_image = VK_NULL_HANDLE;
    VkDeviceMemory m_memory = VK_NULL_HANDLE;
    VkImageView m_imageView = VK_NULL_HANDLE;
    VkFramebuffer m_framebuffer = VK_NULL_HANDLE;
};

struct SwapchainRenderTarget : public RenderTarget
{
    virtual void setupImages(const SetupImagesParams& params);

    Vector<VkImageView> m_imageViews;
};

struct RenderPass : public BaseRenderPass
{
    struct Subpass
    {
        Vector<RenderTargetHandle> m_inputRenderTargets;
        Vector<RenderTargetHandle> m_outputRenderTargets;
        uint32 m_nColorAttachments = 0;
    };

    struct SetupFrameBuffersParams
    {
        ScreenSize m_screenSize;
        uint32 m_iFrame = 0;
        VkPhysicalDeviceMemoryProperties m_memoryProperties = {};
        VkSwapchainKHR m_swapChain = VK_NULL_HANDLE;
        Vector<VkImageView> m_swapchainImageViews;
        bool m_bNVidiaDedicatedAlloc = false;
    };

    ~RenderPass()
    {
        vkDestroyRenderPass(m_device, m_renderPass, nullptr);

        for(auto fb : m_framebuffers)
        {
            vkDestroyFramebuffer(m_device, fb, nullptr);
        }

        for(auto semaphore : m_perFrameCompleteSemaphore)
        {
            vkDestroySemaphore(m_device, semaphore, nullptr);
        }
    }

    bool useBackbuffer() const
    {
        return m_iBackbufferAttachment != InvalidIndex;
    }

    void setupFramebuffers(const SetupFrameBuffersParams& params);

    Vector<Subpass> m_subpasses;
    VkDevice m_device = VK_NULL_HANDLE;
    VkRenderPass m_renderPass = VK_NULL_HANDLE;
    Vector<VkFramebuffer> m_framebuffers;
    Vector<VkSemaphore> m_perFrameCompleteSemaphore;
    Vector<VkClearValue> m_clearValues;
    Vector<RenderTargetHandle> m_renderTargets;
    ScreenSize m_screenSize;
    int32 m_iBackbufferAttachment = InvalidIndex;
    bool m_bLastRenderPass = false;
    bool m_bDependentOn = false;
};

struct RenderPipeline : public GraphicResource
{
    struct RenderPassNode
    {
        RenderPassHandle m_renderPass;
        Vector<RenderPassHandle> m_dependencies;
        bool m_bIsDependentOn = false;
    };

    Vector<RenderPassNode> m_renderSequence;
};

struct View : public GraphicResource
{
    struct PerFrame
    {
        VkDescriptorSet m_descSet = VK_NULL_HANDLE;
        Vulkan::BufferHandle m_UBO;
    };

    ~View()
    {
        for(auto& frame : m_perFrame)
        {
            vkFreeDescriptorSets(m_device, m_pool, 1, &frame.m_descSet);
        }

        vkDestroyDescriptorSetLayout(m_device, m_layout, nullptr);
    }

    void stageUBO(OE_Core::UniqueBufferPtr ubo)
    {
        LockGuard lock(m_mutex);
        m_stagedUBO = std::move(ubo);
    }

    void applyStagedUBO(uint32 iFrame)
    {
        LockGuard lock(m_mutex);
        if(m_stagedUBO)
        {
            m_perFrame[iFrame].m_UBO.getBuffer()->copyTo(
                m_stagedUBO->m_rawData,
                m_stagedUBO->m_size);
            m_stagedUBO.reset();
        }
    }

    Vector<PerFrame> m_perFrame;
    VkDescriptorPool m_pool = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_layout = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;
    OE_Core::UniqueBufferPtr m_stagedUBO;
    Mutex m_mutex;
};

struct CommandBuffer : public GraphicResource
{
    VkCommandBuffer m_buffer = VK_NULL_HANDLE;
    VkCommandPool m_pool = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;

    CommandBuffer(VkDevice device, VkCommandPool pool) :
        m_device(device)
        ,m_pool(pool)
    {
        VkCommandBufferAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandPool = pool;
        allocInfo.commandBufferCount = 1;

        vkAllocateCommandBuffers(m_device, &allocInfo, &m_buffer);

        m_bReady.store(true);
    }

    void begin()
    {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(m_buffer, &beginInfo);
    }

    void end()
    {
        vkEndCommandBuffer(m_buffer);
    }

    virtual ~CommandBuffer()
    {
        vkFreeCommandBuffers(m_device, m_pool, 1, &m_buffer);
    }
};

inline VertexShader* getResource(VertexShaderHandle handle)
{
    return static_cast<VertexShader*>(*handle.m_resource);
}

inline PixelShader* getResource(PixelShaderHandle handle)
{
    return static_cast<PixelShader*>(*handle.m_resource);
}

inline VertexStream* getResource(VertexBufferHandle handle)
{
    return static_cast<VertexStream*>(*handle.m_resource);
}

inline ShaderBinding* getResource(ShaderBindingHandle handle)
{
    return static_cast<ShaderBinding*>(*handle.m_resource);
}

inline RenderPipeline* getResource(RenderPipelineHandle handle)
{
    return static_cast<RenderPipeline*>(*handle.m_resource);
}

inline IndexBuffer* getResource(IndexBufferHandle handle)
{
    return static_cast<IndexBuffer*>(*handle.m_resource);
}

inline Texture* getResource(TextureHandle handle)
{
    return static_cast<Texture*>(*handle.m_resource);
}

inline Pipeline* getResource(PipelineHandle handle)
{
    return static_cast<Pipeline*>(*handle.m_resource);
}

inline DrawInstance* getResource(DrawInstanceHandle handle)
{
    return static_cast<DrawInstance*>(*handle.m_resource);
}

inline RenderPass* getResource(RenderPassHandle handle)
{
    return static_cast<RenderPass*>(*handle.m_resource);
}

inline RenderTarget* getResource(RenderTargetHandle handle)
{
    return static_cast<RenderTarget*>(*handle.m_resource);
}

inline View* getResource(ViewHandle handle)
{
    return static_cast<View*>(*handle.m_resource);
}

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_RESOURCES_H__
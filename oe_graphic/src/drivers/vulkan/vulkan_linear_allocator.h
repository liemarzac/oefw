#ifndef __OE_GRAPHIC_VULKAN_LINEAR_ALLOCATOR_H__
#define __OE_GRAPHIC_VULKAN_LINEAR_ALLOCATOR_H__

#include "../../oe_graphic.h"

// External
#include "vulkan.h"

// Vulkan
#include "vulkan_allocator.h"

namespace OE_Graphic
{
namespace Vulkan
{

class OE_GRAPHIC_EXPORT LinearAllocator : public Allocator
{
public:
    LinearAllocator() = delete;
    LinearAllocator(
        VkDevice device,
        VkDeviceSize targetSize,
        const VkMemoryAllocateInfo& memoryAllocateInfo,
        bool bMap);
    virtual ~LinearAllocator();

    LinearAllocator(const LinearAllocator& rhs) = delete;
    LinearAllocator& operator=(const LinearAllocator& rhs) = delete;

    virtual bool alloc(VkDeviceSize size, Allocation& allocation) override;
    virtual void free(VkDeviceSize offset, VkDeviceSize size) override;
    virtual void reset() override;

private:
    VkDeviceSize m_offset = 0;
}
;
} // Vulkan
} // OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_LINEAR_ALLOCATOR_H__

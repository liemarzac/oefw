#ifndef __OE_GRAPHIC_VULKAN_FRAME_BUFFER_H__
#define __OE_GRAPHIC_VULKAN_FRAME_BUFFER_H__

namespace OE_Graphic
{
namespace Vulkan
{

struct FrameBufferAttachment
{
    VkImage m_image = VK_NULL_HANDLE;
    VkDeviceMemory m_memory = VK_NULL_HANDLE;
    VkImageView m_view = VK_NULL_HANDLE;
    VkFormat m_format = VK_FORMAT_UNDEFINED;

    void destroy(VkDevice device)
    {
        vkDestroyImage(device, m_image, nullptr);
        vkDestroyImageView(device, m_view, nullptr);
        vkFreeMemory(device, m_memory, nullptr);
    }
};

struct FrameBuffer
{
    uint32 m_width = 0;
    uint32 m_height = 0;
    VkFramebuffer m_frameBuffer = VK_NULL_HANDLE;
    FrameBufferAttachment m_depth;
    VkRenderPass m_renderPass = VK_NULL_HANDLE;

    void destroy(VkDevice device)
    {
        vkDestroyFramebuffer(device, m_frameBuffer, nullptr);
        vkDestroyRenderPass(device, m_renderPass, nullptr);
    }
};

} // namespace Vulkan
} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VULKAN_FRAME_BUFFER_H__


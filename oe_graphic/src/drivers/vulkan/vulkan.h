#ifndef __OE_GRAPHIC_VULKAN__H__
#define __OE_GRAPHIC_VULKAN__H__

#include "../../oe_graphic.h"

// External
#include <vulkan/vulkan.h>

// OE_Core
#include <flagset.h>
#include <ref_count.h>

#include "vulkan_buffer.h"

#define OE_CHANNEL_VULKAN "Vulkan"

#define OE_VULKAN_CHECK(X) OE_CHECK_RESULT(X, VK_SUCCESS, "Vulkan");

namespace OE_Graphic
{

namespace Vulkan
{

struct CommandBufferType
{
    enum Enum
    {
        Deferred = 0,
        Copy,
        Composite,
        Count
    };
};

enum class DeviceExtensionFlags
{
    Swapchain,
    NVDedicatedAllocation
};
using DeviceExtFlags = OE_Core::FlagSet<DeviceExtensionFlags>;


struct Allocation
{
    VkDeviceMemory m_deviceMemory = VK_NULL_HANDLE;
    Vulkan::Allocator* m_allocator = nullptr;
    VkDeviceSize m_offset = 0;
    VkDeviceSize m_allocatedSize = 0;
    void* m_mapped = nullptr;
};

struct DepthBuffer
{
    VkImage m_image = VK_NULL_HANDLE;
    VkImageView m_imageView = VK_NULL_HANDLE;
    VkDeviceMemory m_deviceMemory = VK_NULL_HANDLE;
};

int findMemoryProperties(
    const VkPhysicalDeviceMemoryProperties& memoryProperties,
    uint32 memoryTypeBits,
    VkMemoryPropertyFlags requiredProperties);

VkFormat toVulkanFormat(TextureFormat format);

VkShaderStageFlags toVulkanStageFlags(OE_Core::FlagSet<ShaderType> flagset);

} // Vulkan
} // OE_Graphic

#endif

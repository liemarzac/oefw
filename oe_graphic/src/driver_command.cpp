#include "driver_command.h"

#include "graphic_private.h"

// OE_Graphic
#include "resource_creation_manager.h"

#define OE_CHANNEL_DRIVER_COMMAND "DriverCommand"

namespace OE_Graphic
{

void ExecuteCommand::execute()
{
    OE_CHECK(m_func, OE_CHANNEL_DRIVER_COMMAND);
    m_func();
}

} // namesapce OE_Graphic
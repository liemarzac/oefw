#include "material_resource.h"

#include "graphic_private.h"

// OE_Core
#include <file.h>
#include <resource_manager.h>
#include <task_manager.h>

// OE_Graphic
#include "renderer.h"
#include "shader_binding_resource.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(ShaderBindingResource, Resource)

ShaderBindingResource::ShaderBindingResource(const String& name):
    Resource(name)
{
    m_layout = MakeShared<ShaderBindingLayout>();
}

ShaderBindingResource::ShaderBindingResource(
    const String& name,
    SharedPtr<ShaderBindingLayout> descriptor
):
    Resource(name)
{
}

ShaderBindingResource::~ShaderBindingResource()
{

}

void ShaderBindingResource::loadImpl()
{
    if(m_loadingState == LoadingState::WaitingForDescriptor)
    {
        String resourcePath = getName() + ".json";
        m_loadTaskPtr = MakeShared<ShaderBindingResourceLoadTask>(resourcePath, m_layout);
        TaskManager::getInstance().addTask(m_loadTaskPtr);
    }
}

void ShaderBindingResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    switch(m_loadingState)
    {
    case LoadingState::WaitingForDescriptor:
        if(m_loadTaskPtr->isFinished())
        {
            m_loadTaskPtr.reset();
            m_loadingState = LoadingState::CreateShaderBinding;
        }
        else
        {
            break;
        }

    case LoadingState::CreateShaderBinding:
        m_handle = Renderer::getInstance().createShaderBinding(
            *m_layout
            DBG_PARAM_ADD(getName()));

        m_loadingState = LoadingState::Loaded;
        setState(State::Loaded);
        break;
    }
}

ShaderBindingResourceLoadTask::ShaderBindingResourceLoadTask(
    const String& resourceName,
    SharedPtr<ShaderBindingLayout> layout
):
    m_resourceName(resourceName)
    ,m_layout(layout)
{
    OE_CHECK(m_layout, OE_CHANNEL_RESOURCE);
}

void ShaderBindingResourceLoadTask::run()
{
    Json::Value jsonContent;

    auto& renderer = Renderer::getInstance();

    String shaderResourceFolder;
    renderer.getShaderResourcePath(shaderResourceFolder);
    String resourcePath = shaderResourceFolder + "/" + m_resourceName;

    OE_CHECK_RESULT(
        appLoadJsonFile(resourcePath.c_str(), jsonContent),
        true,
        OE_CHANNEL_RESOURCE);


    // Layouts.
    {
        Json::Value& jsonLayouts = jsonContent["layouts"];
        OE_CHECK(jsonLayouts.isArray(), OE_CHANNEL_RESOURCE);

        Json::ValueIterator jsonLayoutItr = jsonLayouts.begin();
        for(; jsonLayoutItr != jsonLayouts.end(); ++jsonLayoutItr)
        {
            ShaderBindingLayout::Layout layout;

            Json::Value jsonBindings = (*jsonLayoutItr);
            OE_CHECK(jsonBindings.isArray(), OE_CHANNEL_RESOURCE);

            Json::ValueIterator jsonBindingItr = jsonBindings.begin();
            for(; jsonBindingItr != jsonBindings.end(); ++jsonBindingItr)
            {
                ShaderBindingLayout::Binding binding;

                Json::Value& jsonBinding = (*jsonBindingItr);
                OE_CHECK(jsonBinding.isObject(), OE_CHANNEL_RESOURCE);

                // Type
                Json::Value jsonType = jsonBinding["type"];
                OE_CHECK(jsonType.isString(), OE_CHANNEL_RESOURCE);

                String strType = jsonType.asString();
                if(strType == "uniform")
                {
                    binding.m_type = ShaderBindingLayout::Binding::Type::Uniform;
                }
                else if(strType == "uniform_dynamic")
                {
                    binding.m_type = ShaderBindingLayout::Binding::Type::UniformDynamic;
                }
                else if(strType == "sampler")
                {
                    binding.m_type = ShaderBindingLayout::Binding::Type::Sampler;
                }
                else if (strType == "input_attachment")
                {
                    binding.m_type = ShaderBindingLayout::Binding::Type::InputAttachment;
                }
                else
                {
                    OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown binding type: %s", strType.c_str());
                }

                // Stages
                Json::Value jsonStages = jsonBinding["stages"];
                OE_CHECK(jsonStages.isArray(), OE_CHANNEL_RESOURCE);

                Json::ValueIterator jsonStageItr = jsonStages.begin();
                for(; jsonStageItr != jsonStages.end(); ++jsonStageItr)
                {
                    Json::Value& jsonStage = *jsonStageItr;
                    OE_CHECK(jsonStage.isString(), OE_CHANNEL_RESOURCE);

                    String strStage = jsonStage.asString();
                    if(strStage == "vertex_shader")
                    {
                        binding.m_stages.set(ShaderType::VertexShader);
                    }
                    else if(strStage == "pixel_shader")
                    {
                        binding.m_stages.set(ShaderType::PixelShader);
                    }
                    else
                    {
                        OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown stage: %s", strStage.c_str());
                    }
                }

                layout.m_bindings.push_back(binding);
            }

            m_layout ->m_layouts.push_back(layout);
        }
    }
}


}

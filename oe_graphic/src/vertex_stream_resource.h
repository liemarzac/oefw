#ifndef __OE_GRAPHIC_VERTEX_STREAM_RESOURCE_H__
#define __OE_GRAPHIC_VERTEX_STREAM_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <buffer.h>
#include <resource.h>
#include <runnable.h>

// OE_Graphic
#include "graphic_resource_helpers.h"


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT VertexStreamResource : public OE_Core::Resource
{
friend class VertexStreamResourceLoadTask;
    RTTI_DECLARATION
public:
    VertexStreamResource(const String& name);
    VertexStreamResource(
        UniquePtr<OE_Core::Buffer> vertexBuffer,
        VertexDeclaration vertexDeclaration,
        PrimitiveType primitiveType,
        BufferUsage bufferUsage,
        const String& name);
    VertexStreamResource(
        UniquePtr<OE_Core::Buffer> vertexBuffer,
        UniquePtr<OE_Core::Buffer> indexBuffer,
        IndexType indexType,
        VertexDeclaration vertexDeclaration,
        PrimitiveType primitiveType,
        BufferUsage bufferUsage,
        const String& name);

    virtual ~VertexStreamResource();

    virtual void loadImpl() override;

    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline VertexBufferHandle getVertexBufferHandle() const
    {
        return m_vertexBufferHandle;
    }

    inline IndexBufferHandle getIndexBufferHandle() const
    {
        return m_indexBufferHandle;
    }

    inline VertexDeclaration getVertexDeclaration() const
    {
        return m_vertexDeclaration;
    }

    inline PrimitiveType getTopology() const
    {
        return m_topology;
    }

    inline const OE_Core::Buffer& getNormals() const
    {
        return m_normals;
    }

    void update(
        OE_Core::UniqueBufferPtr vertices,
        OE_Core::UniqueBufferPtr indicies);

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "vertex_stream_resource";
    }

private:
    VertexStreamResource();
    VertexStreamResource& operator=(const VertexStreamResource& rhs);
    VertexStreamResource(VertexStreamResource& rhs);

    VertexBufferHandle m_vertexBufferHandle;
    IndexBufferHandle m_indexBufferHandle;
    VertexDeclaration m_vertexDeclaration = VertexDeclaration::Count;
    PrimitiveType m_topology = PrimitiveType::TriangleList;
    BufferUsage m_usage = BufferUsage::Static;
    SharedPtr<VertexStreamResourceLoadTask> m_loadTask;

    enum class InternalLoadState
    {
        Unloaded,
        LoadFromFile,
        WaitForLoadTask,
        WaitForHandlers,
        Loaded
    }m_internalLoadState = InternalLoadState::Unloaded;

#if !_RELEASE
    OE_Core::Buffer m_normals;
#endif
};


class VertexStreamResourceLoadTask : public OE_Core::Runnable
{
public:
    VertexStreamResourceLoadTask(SharedPtr<VertexStreamResource> resource);
    virtual void run() override;

private:
    VertexStreamResourceLoadTask();
    VertexStreamResourceLoadTask& operator=(const VertexStreamResourceLoadTask& rhs);
    VertexStreamResourceLoadTask(VertexStreamResourceLoadTask& rhs);

    SharedPtr<VertexStreamResource> m_resource;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_VERTEX_STREAM_RESOURCE_H__

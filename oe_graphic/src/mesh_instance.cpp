#include "mesh_instance.h"

#include "graphic_private.h"

#include "material_resource.h"
#include "mesh_resource.h"
#include "renderer.h"
#include "vertex_stream_resource.h"

namespace OE_Graphic
{

MeshInstance::MeshInstance()
{
    m_objectToWorldTransform.setIdentity();
    m_bRendering.store(0);
}

MeshInstance::MeshInstance(const CreateParams& params)
{
    m_objectToWorldTransform.setIdentity();
    m_bRendering.store(0);
    set(params);
}

MeshInstance::~MeshInstance()
{
}

void MeshInstance::set(const CreateParams& params)
{
    auto& renderer = Renderer::getInstance();

    OE_CHECK(params.m_renderPassId.m_handle, OE_CHANNEL_MESH_INSTANCE);
    OE_CHECK(params.m_vertexBufferHandle, OE_CHANNEL_MESH_INSTANCE);
    OE_CHECK(params.m_pipelineHandle, OE_CHANNEL_MESH_INSTANCE);

    m_pipelineHandle = params.m_pipelineHandle;
    m_renderPassId = params.m_renderPassId;

    CreateDrawInstanceParams createDrawInstanceParams;
    createDrawInstanceParams.m_pipelineHandle = params.m_pipelineHandle;
    createDrawInstanceParams.m_vertexBufferHandle = params.m_vertexBufferHandle;
    createDrawInstanceParams.m_indexBufferHandle = params.m_indexBufferHandle;

    m_drawInstanceHandle = renderer.createDrawInstance(
        createDrawInstanceParams
        DBG_PARAM_ADD(m_name));
}

DrawInstanceHandle MeshInstance::getDrawInstanceHandle() const
{
    return m_drawInstanceHandle;
}

PipelineHandle MeshInstance::getPipelineHandle() const
{
    return m_pipelineHandle;
}

const RenderPassId& MeshInstance::getRenderPassId() const
{
    return m_renderPassId;
}

void MeshInstance::setPosition(const Vec3& position)
{
    OE_CHECK(m_bRendering == false, OE_CHANNEL_MESH_INSTANCE);
    m_objectToWorldTransform(0, 3) = position(0);
    m_objectToWorldTransform(1, 3) = position(1);
    m_objectToWorldTransform(2, 3) = position(2);
    m_objectToWorldTransform(3, 3) = 1.0f;
}

void MeshInstance::setOrientation(const Mat4& orientation)
{
    OE_CHECK(m_bRendering == false, OE_CHANNEL_MESH_INSTANCE);
    Vec3 savedPosition;
    savedPosition(0) = m_objectToWorldTransform(0, 3);
    savedPosition(1) = m_objectToWorldTransform(1, 3);
    savedPosition(2) = m_objectToWorldTransform(2, 3);
    m_objectToWorldTransform = orientation;
    setPosition(savedPosition);
}

void MeshInstance::updateBuffer()
{
}

#ifdef _DEBUG
void MeshInstance::setName(const String& name)
{
    m_name = name;
}
#endif // _DEBUG


} // namespace OE_Graphic

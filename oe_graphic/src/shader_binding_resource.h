#ifndef __OE_GRAPHIC_SHADER_BINNDING_RESOURCE_H__
#define __OE_GRAPHIC_SHADER_BINNDING_RESOURCE_H__

#include "oe_graphic.h"

// OE_Core
#include <resource.h>
#include <runnable.h>


namespace OE_Graphic
{

class OE_GRAPHIC_EXPORT ShaderBindingResource : public OE_Core::Resource
{
    RTTI_DECLARATION

public:
    ShaderBindingResource(const String& name);
    ShaderBindingResource(
        const String& name,
        SharedPtr<ShaderBindingLayout> layout);
    virtual ~ShaderBindingResource();
    NO_COPY(ShaderBindingResource);

    virtual void loadImpl() override;
    virtual void updateLoadingImpl(DependencyCallback dependencyCallback) override;

    inline ShaderBindingHandle getHandle() const
    {
        return m_handle;
    }

    virtual const char* getType() const override
    {
        return getTypeStatic();
    }

    static const char* getTypeStatic()
    {
        return "shader_binding_resource";
    }

private:
    enum class LoadingState
    {
        WaitingForDescriptor,
        CreateShaderBinding,
        WaitingForShaderBinding,
        Loaded
    }m_loadingState = LoadingState::WaitingForDescriptor;

    SharedPtr<ShaderBindingLayout> m_layout;
    ShaderBindingHandle m_handle;

    SharedPtr<class ShaderBindingResourceLoadTask> m_loadTaskPtr;
};

class ShaderBindingResourceLoadTask : public OE_Core::Runnable
{
public:
    ShaderBindingResourceLoadTask() = delete;
    ShaderBindingResourceLoadTask(
        const String& resourceName,
        SharedPtr<ShaderBindingLayout> layout);
    NO_COPY(ShaderBindingResourceLoadTask);

    virtual void run() override;

private:
    String m_resourceName;
    SharedPtr<ShaderBindingLayout> m_layout;
};

} // namespace OE_Graphic

#endif // __OE_GRAPHIC_SHADER_BINDING_RESOURCE_H__

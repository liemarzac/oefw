#include "view_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>

// OE_Graphics
#include "renderer.h"
#include "ubo_factory.h"

// OE_Core
#include <task_manager.h>


namespace OE_Graphic
{

RTTI_IMPL_PARENT(ViewResource, Resource)

ViewResource::ViewResource(const String& path) :
    Resource(path)
{
}

ViewResource::~ViewResource()
{
    if(m_ubo)
    {
        delete m_ubo;
    }
}

const char* ViewResource::getType() const
{
    return "view_resource";
}

void ViewResource::loadImpl()
{
    m_loadTaskPtr = MakeShared<ResourceLoadTask<Descriptor>>(
        getName(),
        MakeShared<Descriptor>(),
        &ViewResource::readJsonContent);
    TaskManager::getInstance().addTask(m_loadTaskPtr);
}

void ViewResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    if(m_loadTaskPtr && m_loadTaskPtr->isFinished())
    {
        auto& descriptor = m_loadTaskPtr->getDescriptor();
        auto uboFactory = Renderer::getInstance().getUBOFactory();
        m_ubo = uboFactory->createUBO(descriptor->m_uboClassName);

        CreateViewParams params;
        params.m_uboSize = m_ubo->getSize();
        params.m_stages = descriptor->m_stages;

        m_handle = Renderer::getInstance().createView(
            params
            DBG_PARAM_ADD(getName()));

        m_loadTaskPtr.reset();
        setState(Resource::State::Loaded);
    }
}

void ViewResource::stage()
{
    if(m_handle)
    {
        OE_CHECK(m_ubo, OE_CHANNEL_RESOURCE);
        const size_t uboSize = m_ubo->getSize();
        const void* uboData = m_ubo->getData();
        UniqueBufferPtr buffer = MakeUnique<Buffer>(uboSize);
        buffer->write(uboData, uboSize);
        Renderer::getInstance().updateView(m_handle, std::move(buffer));
    }
}

void ViewResource::readJsonContent(const Json::Value& jsonContent, SharedPtr<Descriptor> descriptor)
{
    // UBO class.
    const Json::Value& jsonUBOClass = jsonContent["ubo_class"];
    OE_CHECK(jsonUBOClass.isString(), OE_CHANNEL_RESOURCE);
    descriptor->m_uboClassName = jsonUBOClass.asString();

    // Stages.
    const Json::Value& jsonStages = jsonContent["stages"];
    OE_CHECK(jsonStages.isArray(), OE_CHANNEL_RESOURCE);
    for(auto jsonStage : jsonStages)
    {
        OE_CHECK(jsonStage.isString(), OE_CHANNEL_RESOURCE);

        String strStage = jsonStage.asString();
        if(strStage == "vertex_shader")
        {
            descriptor->m_stages.set(ShaderType::VertexShader);
        }
        else if(strStage == "pixel_shader")
        {
            descriptor->m_stages.set(ShaderType::PixelShader);
        }
        else
        {
            OE_CHECK_MSG(false, OE_CHANNEL_RESOURCE, "Unknown stage: %s", strStage.c_str());
        }
    }
}

} // namespace OE_Graphic

#include "pixel_shader_resource.h"

#include "graphic_private.h"

// External
#include <json/json.h>
#include <utility>

// OE_Core
#include <file.h>
#include <task_manager.h>

// OE_Graphic
#include "renderer.h"


namespace OE_Graphic
{

RTTI_IMPL_PARENT(PixelShaderResource, Resource)

PixelShaderResource::PixelShaderResource(const String& name):
    Resource(name)
{
}

PixelShaderResource::~PixelShaderResource()
{
}

void PixelShaderResource::loadImpl()
{
    m_loadTask = MakeShared<PixelShaderResourceLoadTask>(getName());
    TaskManager::getInstance().addTask(m_loadTask);
}

void PixelShaderResource::updateLoadingImpl(DependencyCallback dependencyCallback)
{
    if(m_loadTask->isFinished())
    {
        if(m_loadTask->getHandle().isReady())
        {
            m_handle = m_loadTask->getHandle();
            m_loadTask.reset();
            setState(State::Loaded);
        }
    }
}

PixelShaderResourceLoadTask::PixelShaderResourceLoadTask(const String& resourceName)
{
    m_resourceName = resourceName;
}

void PixelShaderResourceLoadTask::run()
{
    String shaderResourcePath;

    auto& renderer = Renderer::getInstance();
    renderer.getShaderResourcePath(
        ShaderType::PixelShader,
        m_resourceName,
        shaderResourcePath);

    std::ifstream stream;
    appGetResourceStream(shaderResourcePath, stream, std::ios::binary);

    OE_CHECK_MSG(
        stream.is_open(),
        OE_CHANNEL_RESOURCE,
        "Cannot open resource %s", shaderResourcePath.c_str());

    // Get file size.
    const std::streampos begin = stream.tellg();
    stream.seekg(0, std::ios::end);
    const std::streampos end = stream.tellg();
    stream.seekg(0);
    const size_t fileSize = (size_t)(end - begin);

    // Create buffer.
    OE_Core::UniqueBufferPtr shaderByteCode(new Buffer(fileSize));
    stream.read((char*)shaderByteCode->m_rawData, fileSize);
    stream.close();

    m_handle = renderer.createPixelShader(
        std::move(shaderByteCode)
        DBG_PARAM_ADD(m_resourceName));
}

} // namespace OE_Graphic

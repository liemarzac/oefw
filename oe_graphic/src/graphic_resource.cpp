#include "graphic_resource.h"


namespace OE_Graphic
{

BaseRenderPass* getBaseResource(const RenderPassHandle& handle)
{
    return static_cast<BaseRenderPass*>(*handle.m_resource);
}


}

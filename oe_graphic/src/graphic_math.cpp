#include "graphic_math.h"
#include "graphic_private.h"

namespace OE_Graphic
{

void ortho(
    Mat4& ortho,
    float left,
    float right,
    float bottom,
    float top,
    float nearZ,
    float farZ)
{
    float deltaX = right - left;
#if OE_RENDERER_FAMILY == OE_RENDERER_FAMILY_VULKAN
    float deltaY = bottom - top;
    float deltaZ = nearZ - farZ;
#else
    float deltaY = top - bottom;
    float deltaZ = farZ - nearZ;
#endif

    if((deltaX == 0.0f) || (deltaY == 0.0f) || (deltaZ == 0.0f))
    {
        return;
    }

    ortho = ortho.Identity();
    ortho(0, 0) = 2.0f / deltaX;
    ortho(0, 3) = -(right + left) / deltaX;
    ortho(1, 1) = 2.0f / deltaY;
    ortho(1, 3) = -(top + bottom) / deltaY;
#if OE_RENDERER_FAMILY == OE_RENDERER_FAMILY_OPENGL
    // right handed with near plane at 1.0 and far plane at 1.0
    ortho(2, 2) = -2.0f / deltaZ;
    ortho(2, 3) = (nearZ + farZ) / deltaZ;
#elif OE_RENDERER_FAMILY == OE_RENDERER_FAMILY_VULKAN
    // right handed with near plane at 0.0 and far plane at 1.0
    ortho(2, 2) = 1.0f / deltaZ;
    ortho(2, 3) = nearZ / deltaZ;
#else
    // left handed with near plane at 0.0 and far plane at 1.0
    ortho(2, 2) = 1.0f / deltaZ;
    ortho(2, 3) = -nearZ / deltaZ;
#endif
}

}

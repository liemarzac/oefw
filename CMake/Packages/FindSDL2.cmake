if(WIN32)
    set(SDL2_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/SDL2
    )
    
    set(INCLUDE_PATH_SUFFIX
        include
    )    

 message(STATUS "Architecture: ${ARCHITECTURE}")
    
    if(${ARCHITECTURE} MATCHES "x32")
        set(LIB_PATH_SUFFIX
            lib/x32
        )        
    else()
        set(LIB_PATH_SUFFIX
            lib/x64
        )    
    endif() 
 
    message(STATUS "SDL Path suffix: ${LIB_PATH_SUFFIX}") 
else()
    set(INCLUDE_PATH_SUFFIX
        SDL2
    )
    set(SDL2_SEARCH_PATHS
        /usr/local
    )
endif()

find_path(SDL2_INCLUDE_DIR SDL.h
  PATH_SUFFIXES ${INCLUDE_PATH_SUFFIX}
  PATHS ${SDL2_SEARCH_PATHS}
)

find_library(SDL2_LIBRARY_1
  NAMES SDL2
  PATH_SUFFIXES ${LIB_PATH_SUFFIX}
  PATHS ${SDL2_SEARCH_PATHS}
)

if(WIN32)
    find_library(SDL2_LIBRARY_2
      NAMES SDL2main
      PATH_SUFFIXES ${LIB_PATH_SUFFIX}
      PATHS ${SDL2_SEARCH_PATHS}
    )
endif()

if(WIN32)
    set(SDL2_LIBRARY
        ${SDL2_LIBRARY_1}
        ${SDL2_LIBRARY_2}
    )
else()
    set(SDL2_LIBRARY
        ${SDL2_LIBRARY_1}
    )
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDL2 REQUIRED_VARS SDL2_LIBRARY SDL2_INCLUDE_DIR)

if(WIN32)
    set(SEARCH_PATHS ${DEPENDENCIES_DIR}/libjpeg-turbo)
    
    set(LIB_PATH_SUFFIX_RELEASE
        build/Win64/Release
    )

    set(GENERATED_INCLUDE_PATH_SUFFIX
        build/Win64
    )
else()
    set(SEARCH_PATHS
        /usr
        /usr/local
        /opt
        /opt/local
    )
endif()

# Find include dir which is the root of libjpegfolder.
find_path(LIBJPEGTURBO_BASE_INCLUDE_DIR
    NAMES jpeglib.h
    PATHS ${SEARCH_PATHS}
)

# Find generated include dir (containing generated files such as jconfig.h)
find_path(LIBJPEGTURBO_GENERATED_INCLUDE_DIR
    NAMES jconfig.h
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES ${GENERATED_INCLUDE_PATH_SUFFIX}
)

# Find the library and its path
set(LIB_NAME turbojpeg)
find_library(LIBJPEGTURBO_LIBRARY
    NAMES ${LIB_NAME}
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES ${LIB_PATH_SUFFIX_RELEASE}
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(libjpegturbo REQUIRED_VARS LIBJPEGTURBO_BASE_INCLUDE_DIR LIBJPEGTURBO_GENERATED_INCLUDE_DIR LIBJPEGTURBO_LIBRARY)

if(WIN32)
    set(GLEW_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/glew
    )
    
    set(INCLUDE_PATH_SUFFIX
        include
    )
    
    if(${ARCHITECTURE} MATCHES "x32")
        set(LIB_PATH_SUFFIX
            lib/Release/Win32
        )        
    else()
        set(LIB_PATH_SUFFIX
            lib/Release/x64
        )    
    endif()   

endif()

find_path(GLEW_INCLUDE_DIR GL
  HINTS
  PATH_SUFFIXES ${INCLUDE_PATH_SUFFIX}
  PATHS ${GLEW_SEARCH_PATHS}
)

if(WIN32)
    find_library(GLEW_LIBRARY
      NAMES glew32s
      HINTS
      PATH_SUFFIXES ${LIB_PATH_SUFFIX}
      PATHS ${GLEW_SEARCH_PATHS}
    )
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLEW REQUIRED_VARS GLEW_LIBRARY GLEW_INCLUDE_DIR)

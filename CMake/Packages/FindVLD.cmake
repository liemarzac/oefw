if(WIN32)
    set(VLD_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/vld
    )
    
    if(${ARCHITECTURE} MATCHES "x86")
        set(LIB_PATH_SUFFIX
            lib/Win32
        )
    else()
        set(LIB_PATH_SUFFIX
            lib/Win64
        )        
    endif()   

    set(LIB_NAME
        vld
    )
    
    find_path(VLD_INCLUDE_DIR
        NAMES vld.h
        PATH_SUFFIXES include
        PATHS ${VLD_SEARCH_PATHS}
    )

    find_library(VLD_LIBRARY
        NAMES ${LIB_NAME}
        PATH_SUFFIXES ${LIB_PATH_SUFFIX}
        PATHS ${VLD_SEARCH_PATHS}
    )

    INCLUDE(FindPackageHandleStandardArgs)
    FIND_PACKAGE_HANDLE_STANDARD_ARGS(VLD REQUIRED_VARS VLD_LIBRARY VLD_INCLUDE_DIR)

endif()
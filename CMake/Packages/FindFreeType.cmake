if(WIN32)
    set(FREETYPE_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/FreeType
    )

    if(${ARCHITECTURE} MATCHES "x86")
        set(FREETYPE_LIB_PATH_SUFFIX_DEBUG
            objs/x32/Debug
        )
        
        set(FREETYPE_LIB_PATH_SUFFIX_RELEASE
            objs/x32/Release
        )
    else()
        set(FREETYPE_LIB_PATH_SUFFIX_DEBUG
            objs/x64/Debug
        )
        
        set(FREETYPE_LIB_PATH_SUFFIX_RELEASE
            objs/x64/Release
        )    
    endif()
else()
    set(FREETYPE_SEARCH_PATHS
        /usr
        /opt/local
        /opt
    )
endif()

find_path(FREETYPE_INCLUDE_DIR freetype.h
  HINTS
  PATH_SUFFIXES include
  PATHS ${FREETYPE_SEARCH_PATHS}
)

find_library(FREETYPE_LIBRARY_DEBUG
  NAMES freetype_d
  HINTS
  PATH_SUFFIXES ${FREETYPE_LIB_PATH_SUFFIX_DEBUG}
  PATHS ${FREETYPE_SEARCH_PATHS}
)

find_library(FREETYPE_LIBRARY_RELEASE
  NAMES freetype
  HINTS
  PATH_SUFFIXES ${FREETYPE_LIB_PATH_SUFFIX_RELEASE}
  PATHS ${FREETYPE_SEARCH_PATHS}
)

set(FREETYPE_LIBRARY
    debug ${FREETYPE_LIBRARY_DEBUG}
    optimized ${FREETYPE_LIBRARY_RELEASE}
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FreeType REQUIRED_VARS FREETYPE_LIBRARY FREETYPE_INCLUDE_DIR)

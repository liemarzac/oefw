if(!WIN32)
    set(LIBPNG_SEARCH_PATHS
        /usr
        /opt/local
        /opt
    )
endif()

find_path(LIBPNG_INCLUDE_DIR Eigen
    HINTS
    PATH_SUFFIXES eigen3
    PATHS ${LIBPNG_SEARCH_PATHS}
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(EIGEN REQUIRED_VARS EIGEN_INCLUDE_DIR)

if(WIN32)
    set(GLM_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/GLM
    )    
else()
    set(GLM_SEARCH_PATHS
        /usr
        /opt/local
        /opt
    )
endif()

find_path(GLM_INCLUDE_DIR glm
    PATHS ${GLM_SEARCH_PATHS}
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLM REQUIRED_VARS GLM_INCLUDE_DIR)
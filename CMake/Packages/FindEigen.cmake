if(WIN32)
    set(EIGEN_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/Eigen
    )    
else()
    set(EIGEN_SEARCH_PATHS
        /usr
        /opt/local
        /opt
    )
endif()

find_path(EIGEN_INCLUDE_DIR Eigen
    HINTS
    PATH_SUFFIXES eigen3
    PATHS ${EIGEN_SEARCH_PATHS}
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Eigen REQUIRED_VARS EIGEN_INCLUDE_DIR)
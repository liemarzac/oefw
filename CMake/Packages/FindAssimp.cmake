if(WIN32)
    set(ASSIMP_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/Assimp
    )

    set(LIB_PATH_SUFFIX_DEBUG
        builds/${ARCHITECTURE}/code/Debug
    )
    
    set(LIB_PATH_SUFFIX_RELEASE
        builds/${ARCHITECTURE}/code/Release
    )

    set(LIB_NAME
        assimp
    )

else()
    set(ASSIMP_SEARCH_PATHS
        /usr
        /usr/local
        /opt
        /opt/local
    )

    set(LIB_NAME
        assimp
    )
endif()

find_path(ASSIMP_INCLUDE_DIR assimp
    HINTS
    $ENV{ASSIMPDIR}
    PATH_SUFFIXES include
    PATHS ${ASSIMP_SEARCH_PATHS}
)

find_library(ASSIMP_LIBRARY_RELEASE
    NAMES ${LIB_NAME}
    HINTS
    $ENV{JSONDIR}
    PATH_SUFFIXES ${LIB_PATH_SUFFIX_RELEASE}
    PATHS ${ASSIMP_SEARCH_PATHS}
)

set(ASSIMP_LIBRARY
    ${ASSIMP_LIBRARY_RELEASE}
)


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Assimp REQUIRED_VARS ASSIMP_LIBRARY ASSIMP_INCLUDE_DIR)

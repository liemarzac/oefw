if(WIN32)
    set(JSONCPP_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/JsonCpp
    )

    set(LIB_PATH_SUFFIX_DEBUG
        build/${ARCHITECTURE}/lib/Debug
    )
    
    set(LIB_PATH_SUFFIX_RELEASE
        build/${ARCHITECTURE}/lib/Release
    )

    set(LIB_NAME
        jsoncpp
    )
else()
    set(JSONCPP_SEARCH_PATHS
        /usr
        /usr/local
        /opt
        /opt/local
    )

    set(LIB_NAME
        jsoncpp
    )
endif()

find_path(JSONCPP_INCLUDE_DIR json
    HINTS
    $ENV{JSONDIR}
    PATH_SUFFIXES include
    PATHS ${JSONCPP_SEARCH_PATHS}
)

if(WIN32)
    find_library(JSONCPP_LIBRARY_DEBUG
        NAMES ${LIB_NAME}
        HINTS
        $ENV{JSONDIR}
        PATH_SUFFIXES ${LIB_PATH_SUFFIX_DEBUG}
        PATHS ${JSONCPP_SEARCH_PATHS}
    )
endif()

find_library(JSONCPP_LIBRARY_RELEASE
    NAMES ${LIB_NAME}
    HINTS
    $ENV{JSONDIR}
    PATH_SUFFIXES ${LIB_PATH_SUFFIX_RELEASE}
    PATHS ${JSONCPP_SEARCH_PATHS}
)

if(WIN32)
    set(JSONCPP_LIBRARY
        debug ${JSONCPP_LIBRARY_DEBUG}
        optimized ${JSONCPP_LIBRARY_RELEASE}
    )
else()
    set(JSONCPP_LIBRARY
        ${JSONCPP_LIBRARY_RELEASE}
    )
endif()

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(JsonCpp REQUIRED_VARS JSONCPP_LIBRARY JSONCPP_INCLUDE_DIR)

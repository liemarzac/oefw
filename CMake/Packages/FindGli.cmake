if(WIN32)
    set(GLI_SEARCH_PATHS
        ${DEPENDENCIES_DIR}/GLI
    )    
else()
    set(GLI_SEARCH_PATHS
        /usr
        /opt/local
        /opt
    )
endif()

find_path(GLI_INCLUDE_DIR gli
    PATHS ${GLI_SEARCH_PATHS}
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLI REQUIRED_VARS GLI_INCLUDE_DIR)
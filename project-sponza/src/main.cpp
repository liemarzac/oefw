#include "sponza_private.h"

// External
#include <SDL.h>

// OE_Core
#include "application.h"

// OE_Engine
#include <engine.h>

// App
#include "game.h"

#if OE_PLATFORM == OE_PLATFORM_WINDOWS
// this is to avoid the command window to appear when launching the application.
#pragma comment(linker, "/SUBSYSTEM:WINDOWS")
#endif

#define OE_CHANNEL_MAIN "Main"

void getApplicationName(String& appName)
{
    appName = OE_APPLICATION_NAME;
}

void run()
{
    Engine engine;

    if(engine.init(NULL))
    {
        Game game;

        bool bRun = true;
        while(bRun)
        {
            bRun = engine.update();
            if(bRun)
            {
                game.update();
            }
        }
    }
}

int main(int argc, char* argv[])
{
    // Setup engine globals.
    appSetGetNameFunction(std::function<void(String&)>(&getApplicationName));
    appSetExePath(argv[0]);

    run();

    return 0;
}

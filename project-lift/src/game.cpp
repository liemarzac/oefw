#include "game.h"

#include "lift_private.h"

// OE_Core
#include <file.h>
#include <oe_math.h>
#include <rand.h>
#include <resource_manager.h>

// OE_Engine
#include <camera.h>
#include <composer.h>
#include <engine.h>
#include <entity_manager.h>
#include <light.h>
#include <prop.h>

// OE_Graphic
#include <prop_resource.h>

#define OE_CHANNEL_GAME "Game"

Game::Game()
{
}

Game::~Game()
{
    if (m_plane)
    {
        delete m_plane;
        m_plane = nullptr;
    }

    Engine::getInstance().unloadPackage(OE_APPLICATION_NAME);
}

void Game::update()
{
    auto& engine = Engine::getInstance();

    switch(m_state)
    {
        case State::LoadingEngine:
        {
            if(engine.isLoaded())
            {
                m_state = State::LoadingGamePackage;
                engine.loadPackage(OE_APPLICATION_NAME);
            }
            else
            {
                break;
            }
        }

        case State::LoadingGamePackage:
        {
            if(engine.isPackageLoaded(OE_APPLICATION_NAME))
            {
                m_state = State::CreateGameResources;
            }
            else
            {
                break;
            }
        }

        case State::CreateGameResources:
        {
            Composer& composer = Composer::getInstance();

            m_plane = new Prop("plane");
            auto& resourceManager = ResourceManager::getInstance();
            auto propResource = resourceManager.getResource<PropResource>("props/airliner/Airliner.dae");
            m_plane->createPropComponent(propResource);

            composer.addProp(m_plane);

            m_state = State::LoadingGameResources;
            break;
        }

        case State::LoadingGameResources:
        {
            m_state = State::Running;
            break;
        }
    }
}

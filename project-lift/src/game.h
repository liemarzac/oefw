#ifndef __GAME_H__
#define __GAME_H__

#include "lift.h"

namespace OE_Engine
{
    class Light;
    class Prop;
}

class Game
{
public:
    Game();
    ~Game();

    void update();

private:

    enum class State
    {
        LoadingEngine,
        LoadingGamePackage,
        CreateGameResources,
        LoadingGameResources,
        Running
    };

    OE_Engine::Prop* m_plane = nullptr;
    State m_state = State::LoadingEngine;
};

#endif // __GAME_H__

#include <core.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <windows.h>
#include <windowsx.h>
#include <ft2build.h>
#include FT_FREETYPE_H


using namespace OE_Core;
#define OE_CHANNEL_FONT_GEN_ATLAS "FontGenAtlas"

#pragma comment(linker, "/SUBSYSTEM:WINDOWS")

HWND g_hWnd;
FT_Library  g_ftLibrary;
LPDIRECT3D9 g_d3d = NULL;
LPDIRECT3DDEVICE9 g_d3ddev = NULL;
IDirect3DVertexBuffer9* g_vertexBuffer = NULL;
LPDIRECT3DTEXTURE9 g_atlasTexture = NULL;
LPDIRECT3DSURFACE9 g_backBuffer = NULL;
int g_atlasTextureWidth = 16;
int g_atlasTextureHeight = 16;
const char* g_outputDataFileName = NULL;
const char* g_outputTextureFileName = NULL;
static const int DPI = 72;
static const int HRES = 64;
static const float HRESF = 64.0f;

struct ElementData
{
    int m_fontIndex;
    int m_fontSize;
    int m_glyphUnicode;
};
BinPack<ElementData> g_binPack(g_atlasTextureWidth, g_atlasTextureHeight);

struct Font
{
    FT_Face m_face;
    std::string m_fileName;
    std::vector<int> m_sizes;
    std::vector<int> m_unicodes;
    int m_index;
    FontData m_data;
};
std::vector<Font> g_fonts;

const DWORD D3DFVF_VERTEX = D3DFVF_XYZRHW | D3DFVF_TEX1;
struct Vertex
{
    float x;
    float y;
    float z;
    float rhw;
    float u;
    float v;
};

FT_Matrix g_glyphTransform = 
{
    (int)((1.0 / HRES) * 0x10000L),
    (int)((0.0) * 0x10000L),
    (int)((0.0) * 0x10000L),
    (int)((1.0) * 0x10000L)
};

bool initD3D()
{
    g_d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface

    D3DPRESENT_PARAMETERS d3dpp;    // create a struct to hold various device information

    ZeroMemory(&d3dpp, sizeof(d3dpp));    // clear out the struct for use
    d3dpp.Windowed = TRUE;    // program windowed, not fullscreen
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
    d3dpp.hDeviceWindow = GetActiveWindow();    // set the window to be used by Direct3D

    // create a device class using this information and information from the d3dpp stuct
    g_d3d->CreateDevice(D3DADAPTER_DEFAULT,
                        D3DDEVTYPE_HAL,
                        GetActiveWindow(),
                        D3DCREATE_HARDWARE_VERTEXPROCESSING,
                        &d3dpp,
                        &g_d3ddev);

    if(!g_d3d)
        return false;

    g_d3ddev->SetRenderState(D3DRS_LIGHTING, FALSE);    // turn off the 3D lighting
    g_d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer

    g_d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);    // turn on the color blending
    g_d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);    // set source factor
    g_d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);    // set dest factor
    g_d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);    // set the operation

    return true;
}

void deinitD3D() 
{
    g_d3ddev->Release();    // close and release the 3D device
    g_d3d->Release();    // close and release Direct3D
}

bool calculateAtlasSize(const Json::Value& config)
{
    for(int fontIndex = 0; fontIndex < (int)config["in"].size(); ++fontIndex)
    {
        // Set font info.
        Font font;
        font.m_fileName = config["in"][fontIndex]["font_file_name"].asString();        
        font.m_index = fontIndex;
        size_t posOfLastSlash = font.m_fileName.find_last_of("/"); 
        font.m_data.m_fontName = font.m_fileName.substr(posOfLastSlash == std::string::npos ? 0 : posOfLastSlash + 1);
        size_t posOfExtension = font.m_data.m_fontName.find_last_of("."); 
        font.m_data.m_fontName = font.m_data.m_fontName.substr(0, posOfExtension);

        FT_Error ftError = FT_New_Face(g_ftLibrary,
                               font.m_fileName.c_str(),
                               0,
                               &font.m_face);

        if( ftError == FT_Err_Unknown_File_Format ) 
        {
            logError( OE_CHANNEL_FONT_GEN_ATLAS, 
                      "The font file could be opened and read, but it appears that its font format is unsupported" );
            return false;
        }
        else if( ftError )
        {
            logError( OE_CHANNEL_FONT_GEN_ATLAS, 
                      "The font file could not be opened or read, or simply that it is broken" );
            return false;
        }

        // Get sizes.
        std::vector<int> fontSizeArray;
        const int numSizeRanges = config["in"][fontIndex]["font_size_ranges"].size();
        for(int rangeIndex = 0; rangeIndex < numSizeRanges; ++rangeIndex)
        {
            Json::Value range = config["in"][fontIndex]["font_size_ranges"][rangeIndex];

            const int lowerRange = range[(Json::Value::UInt)0].asInt();
            const int higherRange = range[(Json::Value::UInt)1].asInt();
            
            for(int i = lowerRange; i < higherRange; ++i)
            {
                font.m_sizes.push_back(i);
            }
        }

        // Get unicodes.
        const int numUnicodeRanges = config["in"][fontIndex]["unicode_ranges"].size();
        for(int rangeIndex = 0; rangeIndex < numUnicodeRanges; ++rangeIndex)
        {
            Json::Value range = config["in"][fontIndex]["unicode_ranges"][rangeIndex];

            const int lowerRange  = range[(Json::Value::UInt)0].asInt();
            const int higherRange  = range[(Json::Value::UInt)1].asInt();
            
            for(int i = lowerRange; i < higherRange; ++i)
            {
                font.m_unicodes.push_back(i);
            }
        }
        
        g_fonts.push_back(font);
    }

    bool doesFit = false;
    while( !doesFit )
    {
        doesFit = true;

        for(int fontIndex = 0; fontIndex != g_fonts.size(); ++fontIndex)
        {
            const Font& font = g_fonts[fontIndex];
            std::vector<int>::const_iterator sizeItr = font.m_sizes.begin();
            for(;sizeItr != font.m_sizes.end(); ++sizeItr)
            {
                const int fontSize = (*sizeItr) * HRES;

                FT_Set_Char_Size(font.m_face, fontSize, 0, (FT_UInt)(DPI * HRES), DPI);
                FT_Set_Transform(font.m_face, &g_glyphTransform, NULL);

                std::vector<int>::const_iterator unicodeItr = font.m_unicodes.begin();
                for(;unicodeItr != font.m_unicodes.end(); ++unicodeItr) 
                {
                    const int unicode = *unicodeItr;
                    int glyph_index = FT_Get_Char_Index(font.m_face, unicode);

                    FT_Error ftError = FT_Load_Glyph(
                        font.m_face,
                        glyph_index,
                        FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT);

                    if( ftError )
                        continue;

                    const int bitmapWidth = font.m_face->glyph->bitmap.pitch;
                    const int bitmapHeight = font.m_face->glyph->bitmap.rows;

                    if(bitmapWidth == 0 || bitmapHeight == 0)
                        continue;

                    BinPack<ElementData>::Element element;
                    ElementData inData;
                    inData.m_fontIndex = fontIndex;
                    inData.m_fontSize = fontSize;
                    inData.m_glyphUnicode = unicode;
                    element.m_data = inData;
                    element.m_width = bitmapWidth;
                    element.m_height = bitmapHeight;
                    g_binPack.addElement(element);
                }
            }
        }

        CStopWatch stopWatch;
        stopWatch.startTimer();
        doesFit = g_binPack.process();
        stopWatch.stopTimer();
        log(OE_CHANNEL_FONT_GEN_ATLAS, "Bin pack time for size %dx%d: %f", g_atlasTextureWidth, g_atlasTextureHeight, stopWatch.getElapsedTime());
        if(!doesFit)
        {
            if(g_atlasTextureWidth == g_atlasTextureHeight)
            {
                g_atlasTextureWidth <<= 1;
            }
            else
            {
                g_atlasTextureHeight = g_atlasTextureWidth;
            }

            g_binPack.resetBin(g_atlasTextureWidth, g_atlasTextureHeight);

            if(g_atlasTextureWidth > 4096)
            {
                OE_CHECK_MSG(false, "BinPack", "Cannot fit all glyphs on a 4096 pixels wide texture");
                return false;
            }
        }
    }

    log(OE_CHANNEL_FONT_GEN_ATLAS, "Atlas size: %dx%d", g_atlasTextureWidth, g_atlasTextureHeight);
    return true;
}

void generateAtlas()
{
    g_d3ddev->CreateTexture(
        g_atlasTextureWidth,
        g_atlasTextureHeight,
        1,
        D3DUSAGE_DYNAMIC,
        D3DFMT_A8R8G8B8,
        D3DPOOL_DEFAULT,
        &g_atlasTexture,
        NULL);

    // Render glyph texture.
    D3DLOCKED_RECT atlasTextureRect;
    g_atlasTexture->LockRect(0, &atlasTextureRect, 0, D3DLOCK_DISCARD);    
    uint32* atlasTextureBuffer = reinterpret_cast<uint32*>(atlasTextureRect.pBits);
    memset(atlasTextureBuffer, 0, g_atlasTextureWidth * g_atlasTextureHeight * 4);

    // Create output files.
    std::ofstream outputDataFile;
    outputDataFile.open(g_outputDataFileName, std::ios::out | std::ios::binary);
    if(!outputDataFile.is_open())
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot create output file %s", g_outputDataFileName);
        return;
    }

    std::ofstream outputTextureFile;
    outputTextureFile.open(g_outputTextureFileName, std::ios::out | std::ios::binary);
    if(!outputTextureFile.is_open())
    {
        logError(OE_CHANNEL_RESOURCE, "Cannot create output file %s", g_outputTextureFileName);
        return;
    }

    // Create output resource.
    FontTexture fontTexture;
    fontTexture.m_textureBuffer.alloc(g_atlasTextureWidth * g_atlasTextureHeight);
    memset(fontTexture.m_textureBuffer.m_rawData, 0, g_atlasTextureWidth * g_atlasTextureHeight);
    fontTexture.m_textureWidth = g_atlasTextureWidth;
    fontTexture.m_textureHeight = g_atlasTextureHeight;

    // Get the pos of all glyphs packed.    
    Rect2D rect;
    ElementData packedData;

    log(OE_CHANNEL_FONT_GEN_ATLAS, "Num glyphs packed: %d", g_binPack.getNumElements());

    FontsData fontsData;

    while(g_binPack.iterateResults(rect, packedData))
    {
        // Get the font.
        Font& font = g_fonts[packedData.m_fontIndex];

        FT_Set_Char_Size(font.m_face, packedData.m_fontSize, 0, (FT_UInt)(DPI * HRES), DPI);
        FT_Set_Transform(font.m_face, &g_glyphTransform, NULL);

        int glyph_index = FT_Get_Char_Index(font.m_face, packedData.m_glyphUnicode);

        FT_Error ftError = FT_Load_Glyph(
            font.m_face,
            glyph_index,
            FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT);

        OE_CHECK(ftError == 0, OE_CHANNEL_FONT_GEN_ATLAS);

        const bool hasKerning = FT_HAS_KERNING(font.m_face) > 0L ? true : false;

        GlyphData glyphData;
        glyphData.m_rect = rect;
        glyphData.m_left = font.m_face->glyph->bitmap_left;
        glyphData.m_top = font.m_face->glyph->bitmap_top;
        glyphData.m_advanceX = font.m_face->glyph->advance.x >> 6;
        glyphData.m_advanceY = font.m_face->glyph->advance.y >> 6;

        if(hasKerning)
        {
            // Get kerning data.
            for(uint32 i = 0; i < (uint32)font.m_unicodes.size(); ++i)
            {
                if(i == packedData.m_glyphUnicode)
                    continue;

                FT_Vector  delta;
                int prev_glyph_index = FT_Get_Char_Index(font.m_face, i);
                FT_Get_Kerning(font.m_face, prev_glyph_index, glyph_index,
                               FT_KERNING_DEFAULT, &delta );

                if(delta.x > 0)
                {
                    glyphData.m_kerningData[packedData.m_glyphUnicode] = delta.x >> 6;
                }
            }
        }

        font.m_data.m_fontSizes[packedData.m_fontSize >> 6][packedData.m_glyphUnicode] = glyphData;

        const uint32 bitmapWidth = font.m_face->glyph->bitmap.pitch;
        const uint32 bitmapHeight = font.m_face->glyph->bitmap.rows;

        for(uint32 y = 0; y < bitmapHeight; ++y)
        {            
            for(uint32 x = 0; x < bitmapWidth; ++x)
            {
                const uint32 srcPixelIndex = (y * bitmapWidth) + x;
                const uint32 destPixelIndex = ((rect.m_y + y) * g_atlasTextureWidth) + rect.m_x + x;

                const uint8 alpha = font.m_face->glyph->bitmap.buffer[srcPixelIndex];
                
                // Fill directx texture to render.
                atlasTextureBuffer[destPixelIndex] = 0xFFFFFF; // RGB is always white
                atlasTextureBuffer[destPixelIndex] |= (uint32)(alpha)<< 24; // A;

                // Fill texture buffer to output in file.
                fontTexture.m_textureBuffer.m_rawData[destPixelIndex] = alpha;
            }
        }
    }

    g_atlasTexture->UnlockRect(0);

    for(uint32 i = 0; i < (uint32)g_fonts.size(); ++i)
    {
        fontsData.push_back(g_fonts[i].m_data);
    }

    Buffer serializedFontsData(calculateSerializedSize(fontsData));
    serialize(serializedFontsData, fontsData);
    outputDataFile.write((const char*)serializedFontsData.m_rawData, serializedFontsData.m_size);
    outputDataFile.close();

    Buffer seralizedFontTexture(calculateSerializedSize(fontTexture));
    serialize(seralizedFontTexture, fontTexture);
    outputTextureFile.write((const char*)seralizedFontTexture.m_rawData, seralizedFontTexture.m_size);
    outputTextureFile.close();
}

bool createAtlas(const char* configJsonFileName)
{
    Json::Value config;
    if(!loadJsonFile(configJsonFileName, config))
        return false;

    FT_Error ftError = FT_Init_FreeType(&g_ftLibrary);

    g_outputDataFileName = config["out_data"].asCString();
    g_outputTextureFileName = config["out_texture"].asCString();
    OE_CHECK(g_outputDataFileName != nullptr, OE_CHANNEL_FONT_GEN_ATLAS);
    OE_CHECK(g_outputTextureFileName != nullptr, OE_CHANNEL_FONT_GEN_ATLAS);

    if(!calculateAtlasSize(config))
        return false;

    const int windowWidth = g_atlasTextureWidth < 128 ? 128 : g_atlasTextureWidth;
    const int windowHeight = g_atlasTextureHeight < 128 ? 128 : g_atlasTextureHeight;

    RECT rect;
    rect.top = 0;
    rect.left = 0;
    rect.right = rect.left + windowWidth;
    rect.bottom = rect.top + windowHeight;
    AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);

    SetWindowPos(
      g_hWnd,
      NULL,
      100,
      100,
      rect.right - rect.left,
      rect.bottom - rect.top,
      0
    );

    if(!initD3D())
        return false;

    generateAtlas();

    return true;
}

// the WindowProc function prototype
LRESULT CALLBACK WindowProc(HWND hWnd,
                            UINT message,
                            WPARAM wParam,
                            LPARAM lParam);

// the entry point for any Windows program
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    // this struct holds information for the window class
    WNDCLASSEX wc;

    // clear out the window class for use
    ZeroMemory(&wc, sizeof(WNDCLASSEX));

    // fill in the struct with the needed information
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = L"WindowClass1";

    // register the window class
    RegisterClassEx(&wc);

    // create the window and use the result as the handle
    g_hWnd = CreateWindowEx(NULL,
        L"WindowClass1",    // name of the window class
        L"Font Atlas Gen",   // title of the window
        WS_OVERLAPPEDWINDOW,    // window style
        CW_USEDEFAULT,    // x-position of the window
        CW_USEDEFAULT,    // y-position of the window
        128,    // width of the window
        128,    // height of the window
        NULL,    // we have no parent window, NULL
        NULL,    // we aren't using menus, NULL
        hInstance,    // application handle
        NULL);    // used with multiple windows, NULL

    // display the window on the screen
    ShowWindow(g_hWnd, nCmdShow);

    if(!createAtlas("data/json/font_gen/vera.json"))
        return 1;

    g_d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    g_d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
    g_d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    g_d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

    g_d3ddev->CreateVertexBuffer( sizeof(Vertex) * 4,
        NULL,
        D3DFVF_VERTEX,
        D3DPOOL_MANAGED,
        &g_vertexBuffer,
        NULL );

    float quadScale = 1.0f;
    float offsetAtlasX = -0.5f;
    float offsetAtlasY = -0.5f;
    float quadXMin = offsetAtlasX;
    float quadXMax = quadXMin + (g_atlasTextureWidth * quadScale);
    float quadYMin = offsetAtlasY;
    float quadYMax = quadYMin + (g_atlasTextureHeight * quadScale);

    // Create vertices for the atlas texture.
    Vertex vertices[] = {
        { quadXMin, quadYMin, 0.0f, 1.0f, 0.0f, 0.0f }, 
        { quadXMax, quadYMin, 0.0f, 1.0f, 1.0f, 0.0f },
        { quadXMin, quadYMax, 0.0f, 1.0f, 0.0f, 1.0f }, 
        { quadXMax, quadYMax, 0.0f, 1.0f, 1.0f, 1.0f }
    };

    void* bufferData; 
    g_vertexBuffer->Lock(0, 0, (void**)&bufferData, 0);
    memcpy(bufferData, vertices, sizeof(vertices));
    g_vertexBuffer->Unlock();   

    // wait for the next message in the queue, store the result in 'msg'
    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0))
    {
        // translate keystroke messages into the right format
        TranslateMessage(&msg);

        // send the message to the WindowProc function
        DispatchMessage(&msg);

        g_d3ddev->BeginScene();

        g_d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

        D3DXMATRIX matview;
        D3DXMATRIX matproj;
        D3DXMatrixIdentity(&matview);
        g_d3ddev->SetTransform(D3DTS_VIEW, &matview);
        D3DXMatrixOrthoLH(&matproj, 1000, 200, 0.5f, 1.0f);
        g_d3ddev->SetTransform(D3DTS_PROJECTION, &matproj);
        g_d3ddev->SetFVF(D3DFVF_VERTEX);
        g_d3ddev->SetStreamSource(
            0,
            g_vertexBuffer,
            0,
            sizeof(Vertex));

        g_d3ddev->SetTexture(0, g_atlasTexture);
        g_d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

        g_d3ddev->EndScene();
        g_d3ddev->Present(NULL, NULL, NULL, NULL);
    }

    g_vertexBuffer->Release();
    g_atlasTexture->Release();

    deinitD3D();

    // return this part of the WM_QUIT message to Windows
    return 0;
}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    // sort through and find what code to run for the message given
    switch(message)
    {
        // this message is read when the window is closed
    case WM_DESTROY:
        {
            // close the application entirely
            PostQuitMessage(0);
            return 0;
        } break;
    }

    // Handle any messages the switch statement didn't
    return DefWindowProc (hWnd, message, wParam, lParam);
}

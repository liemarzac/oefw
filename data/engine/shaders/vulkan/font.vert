#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "font.h"

layout(location = 0) in vec4 inPos;
layout(location = 1) in vec2 inTexCoord;

layout(location = 0) out vec2 outTexCoord;

void main()
{
    gl_Position = instance.modelViewProjection * inPos;
    outTexCoord = inTexCoord;
}
// Set 0 = View
layout(set = 0, binding = 0) uniform View
{
    mat4 projection;
    mat4 view;
    mat4 viewProjection;
} view;

// Set 1 = Material
layout (set = 1, binding = 0) uniform sampler2D samplerColor;
layout (set = 1, binding = 1) uniform sampler2D samplerNormal;
layout (set = 1, binding = 2) uniform sampler2D samplerSpecular;

// Set 2 = Instance
layout(set = 2, binding = 0) uniform Instance
{
    mat4 modelViewProjection;
    mat4 modelView;
    mat3 invTransModelView;
    vec4 _pad;
    mat3 invTransModel;
} instance;
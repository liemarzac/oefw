// Set 0 = View
layout(set = 0, binding = 0) uniform View
{
    mat4 projection;
    mat4 model;
    mat4 view;
    mat4 viewProjection;
    vec3 lightDir;
    vec4 ambient;
} view;

// Set 1 = Material
layout(set = 1, binding = 0) uniform Material
{
    vec4 specular;
    float shininess;
} material;
layout(set = 1, binding = 1) uniform sampler2D Diffuse;
layout(set = 1, binding = 2) uniform usampler2D samplerAlbedo;
layout(set = 1, binding = 3) uniform sampler2D samplerposition;
layout(set = 1, binding = 4) uniform sampler2D samplerNormal;
layout(set = 1, binding = 5) uniform sampler2D samplerSSAO;

// Set 2 = Instance
layout(set = 2, binding = 0) uniform Instance
{
    mat4 modelViewProjection;
    mat4 modelView;
    mat3 invTransModelView;
    vec4 _pad;
    mat3 invTransModel;
} instance;
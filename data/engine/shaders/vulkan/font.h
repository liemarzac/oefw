// Set 1 = Material
layout(set = 1, binding = 0) uniform sampler2D samplerAlbedo;

// Set 2 = Instance
layout(set = 2, binding = 0) uniform Instance
{
    mat4 modelViewProjection;
} instance;

#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Set 0 = View
layout(set = 0, binding = 0) uniform View
{
    mat4 projection;
} view;

// Set 1 = Material
struct Light {
    vec4 position;
    vec4 color;
    float radius;
    float quadraticFalloff;
    float linearFalloff;
    float _pad;
};
#define NUM_LIGHTS 17


layout(set = 1, binding = 0) uniform Material
{
    Light lights[NUM_LIGHTS];
    mat4 view;
    vec4 viewPos;
} material;
layout(input_attachment_index = 0, set = 1, binding = 1) uniform subpassInput inputPosition;
layout(input_attachment_index = 1, set = 1, binding = 2) uniform subpassInput inputNormal;
layout(input_attachment_index = 2, set = 1, binding = 3) uniform usubpassInput inputAlbedo;

// Set 2 = Instance
layout(set = 2, binding = 0) uniform Instance
{
    mat4 modelViewProjection;
    mat4 modelView;
} instance;


layout (constant_id = 0) const int SSAO_ENABLED = 0;
layout (constant_id = 1) const float AMBIENT_FACTOR = 0.3;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragcolor;


void main() 
{
	// Get G-Buffer values
	vec3 fragPos = subpassLoad(inputPosition).rgb;
	vec3 normal = subpassLoad(inputNormal).rgb * 2.0 - 1.0;

	// unpack
	//uvec4 albedo = uvec4(subpassLoad(inputAlbedo));
	uvec4 albedo = subpassLoad(inputAlbedo);

	//vec4 color = albedo;
	//vec4 spec = vec4(.0f, .0f, .0f, .0f);
	vec4 color;
	color.rg = unpackHalf2x16(albedo.r);
	color.ba = unpackHalf2x16(albedo.g);
	vec4 spec;
	spec.rg = unpackHalf2x16(albedo.b);

	vec3 ambient = color.rgb * AMBIENT_FACTOR;
	vec3 fragcolor  = ambient;
	
	if (length(fragPos) == 0.0)
	{
		fragcolor = color.rgb;
	}
	else
	{	
		for(int i = 0; i < NUM_LIGHTS; ++i)
		{
			// Light to fragment
			vec3 lightPos = vec3(material.view * vec4(material.lights[i].position.xyz, 1.0));
			vec3 L = lightPos - fragPos;
			float dist = length(L);
			L = normalize(L);

			// Viewer to fragment
			vec3 viewPos = vec3(material.view * vec4(material.viewPos.xyz, 1.0));
			vec3 V = viewPos - fragPos;
			V = normalize(V);

			// Attenuation
			float atten = material.lights[i].radius / (pow(dist, 2.0) + 1.0);

			// Diffuse part
			vec3 N = normalize(normal);
			float NdotL = max(0.0, dot(N, L));
			vec3 diff = material.lights[i].color.rgb * color.rgb * NdotL * atten;

			// Specular part
			vec3 R = reflect(-L, N);
			float NdotR = max(0.0, dot(R, V));
			vec3 spec = material.lights[i].color.rgb * spec.r * pow(NdotR, 16.0) * (atten * 1.5);

			fragcolor += diff + spec;
		}

		// if (SSAO_ENABLED == 1)
		// {
		// 	float ao = texture(samplerSSAO, inUV).r;
		// 	fragcolor *= ao.rrr;
		// }
	}
   
	outFragcolor = vec4(fragcolor, 1.0);	
}
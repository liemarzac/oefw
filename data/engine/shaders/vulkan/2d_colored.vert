#version 450

#include "base_uniforms.h"

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_color;

layout(location = 0) out vec4 out_color;

void main(void)
{
    gl_Position = instance.modelViewProjection * in_position;
    out_color = in_color;
}

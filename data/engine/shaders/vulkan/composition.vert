#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Set 0 = View
layout(set = 0, binding = 0) uniform View
{
    mat4 projection;
} view;

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec2 inUV;

layout (location = 0) out vec2 outUV;


void main() 
{
	outUV = inUV;
	gl_Position = view.projection * vec4(inPos.xyz, 1.0);
}

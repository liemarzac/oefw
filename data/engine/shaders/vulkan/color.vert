#version 450

// Set 2 = Instance
layout(set = 2, binding = 0) uniform Instance
{
    mat4 modelViewProjection;
} instance;

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec4 in_color;

layout(location = 0) out vec4 out_color;

void main()
{
    vec4 position4 = vec4(in_position.xyz, 1.0);
    gl_Position = instance.modelViewProjection * position4;
    out_color = in_color;
}


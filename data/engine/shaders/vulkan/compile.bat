@echo off

set compiler=%VULKAN_SDK%\Bin\glslc.exe

rem glgc does not return error code when compilation failed therefore the following code will always trigger success.

rem Compile vertex shaders.
for %%f in (*.vert) do (
    echo %%f
    %compiler% %%f -o %%~nf_vert.spv
    if %errorlevel% gtr 0 goto error
)

rem Compile pixel shaders.
for %%f in (*.frag) do (
    echo %%f
    %compiler% %%f -o %%~nf_frag.spv
    if %errorlevel% gtr 0 goto error
)

:success
rem echo Shaders successfully compiled
goto end

:error

:end

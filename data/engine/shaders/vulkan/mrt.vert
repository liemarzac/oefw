#version 450

#include "mrt.h"

layout (location = 0) in vec4 inPos;
layout (location = 1) in vec2 inUV;
layout (location = 2) in vec3 inColor;
layout (location = 3) in vec3 inNormal;
layout (location = 4) in vec3 inTangent;
layout (location = 5) in vec3 inBitangent;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec2 outUV;
layout (location = 2) out vec3 outColor;
layout (location = 3) out vec3 outWorldPos;
layout (location = 4) out vec3 outTangent;
layout (location = 5) out vec3 outBitangent;

void main() 
{
	gl_Position = instance.modelViewProjection * inPos;
	
	outUV = inUV;
	outUV.t = 1.0 - outUV.t;

	// Vertex position in world space
	outWorldPos = inPos.xyz;

	outWorldPos = vec3(instance.modelView * inPos);

	// Normal in world space
	mat3 mNormal = instance.invTransModel;
	outNormal = mNormal * normalize(inNormal);	

	// Normal in view space
	mat3 normalMatrix = instance.invTransModelView;
	outNormal = normalMatrix * inNormal;

	outTangent = mNormal * normalize(inTangent);

	// Currently just vertex color
	outColor = inColor;

	outBitangent = inBitangent;
}

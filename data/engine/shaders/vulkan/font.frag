#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "font.h"

layout(location = 0) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = texture(samplerAlbedo, inTexCoord);
}
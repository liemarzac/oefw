import os
from optparse import OptionParser
from struct import *


def export_object(object_path, object_name):
    vertices = []
    uvs = []
    normals = []
    faces = []
    has_indices = False
    with open(os.path.join(object_path, object_name + '.obj'), "r") as object_file:
        for line in object_file:
            line = line.rstrip()
            tokens = line.split(' ')
            if tokens[0] == 'v':
                vertices.append([float(numeric_string) for numeric_string in tokens[1:]])
            elif tokens[0] == 'vt':
                uvs.append([1.0 - float(tokens[1]), 1.0 - float(tokens[2])])
            elif tokens[0] == 'vn':
                normals.append([float(numeric_string) for numeric_string in tokens[1:]])
            elif tokens[0] == 'f':
                face = tokens[1:]
                face_attributes = []
                for face_attributes_str in face:
                    if '/' in face_attributes_str:
                        has_indices = False
                        face_attributes.append([int(numeric_string) for numeric_string in face_attributes_str.split('/')])
                faces.append(face_attributes)

    print 'vertices:'
    for vertex in vertices:
        print vertex

    print '\nuvs:'
    for uv in uvs:
        print uv

    print '\nnormals:'
    for normal in normals:
        print normal

    print '\nfaces:'
    for face in faces:
        print face

    with open(os.path.join(object_path, object_name + '.vstream' ), "wb") as mesh_file:
        print 'Generating ' + object_name + '.vstream ...\n'
        if not has_indices:
            print 'Not using indices'
            # write number of vertices
            mesh_file.write(pack('H', len(faces)*3))

            # create interleaved vertices data (position, uv, normal)
            for face_attributes in faces:
                for vertex_attributes in face_attributes:
                    # write vertices
                    vertex = vertices[vertex_attributes[0] - 1]
                    byte_array = pack('fff', vertex[0], vertex[1], vertex[2])
                    mesh_file.write(byte_array)

                    # write uvs
                    uv = uvs[vertex_attributes[1] - 1]
                    byte_array = pack('ff', uv[0], uv[1])
                    mesh_file.write(byte_array)

                    # write normals
                    normal = normals[vertex_attributes[2] - 1]
                    byte_array = pack('fff', normal[0], normal[1], normal[2])
                    mesh_file.write(byte_array)


def main():
    parser = OptionParser()
    parser.add_option("-p", "--object_path", dest="object_path",
                        help="path to object to export")
    parser.add_option("-n", "--object_name", dest="object_name",
                        help="object name to export")

    (options, args) = parser.parse_args()
    
    if options.object_path is None:
        options.object_path=os.path.dirname(__file__)
        print options.object_path

    if options.object_name is None:
        parser.error("--object_name is required")
    
    export_object(options.object_path, options.object_name)

    
if __name__ == "__main__":
    main()
#ifndef __CELESTIAL_BODY_H__
#define __CELESTIAL_BODY_H__

#include "proxima_private.h"
#include "prop.h"

namespace OE_Engine
{
    class PropComponent;
    class KinematicComponent;
}

class OrbitalComponent;

class CelestialBody : public Prop
{
public:
    CelestialBody(const String& name);
    virtual ~CelestialBody();

    void setMass(Real mass);
    Real getMass() const;

    void setVelocity(const Vec3d& velocity);

protected:
    Real m_mass;
    KinematicComponent* m_kinematicComponent;
    OrbitalComponent* m_orbitalComponent;

private:
    CelestialBody();
    CelestialBody(const CelestialBody& rhs);
    CelestialBody& operator=(const CelestialBody& rhs);
};

inline void CelestialBody::setMass(Real mass)
{
    m_mass = mass;
}

inline Real CelestialBody::getMass() const
{
    return m_mass;
}

#endif // __CELESTIAL_BODY_H__

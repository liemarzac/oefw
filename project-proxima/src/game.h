#ifndef __TA_GAME_H__
#define __TA_GAME_H__

#include "proxima.h"

// External 
#include <json/json.h>

// OE_Core
#include <eigen_helpers.h>
#include <vector.h>

class Planet;
class SolarSystem;

namespace OE_Engine
{
    class Light;
    class Prop;
}

class Game
{
public:
    EIGEN_ALIGNED

    Game();
    ~Game();

    void update();

private:

    enum class State
    {
        LoadingEngine,
        LoadingGamePackage,
        CreateGameResources,
        LoadingGameResources,
        Running
    };

    void addSatellite(Planet* planet, Real circularVelocityRatio);

    SolarSystem* createSolarSystem();

    Mat4 m_zRotation;
    Json::Value m_config;

    Planet* m_earth;
    OE_Engine::Prop* m_sponza = nullptr;
    Vector<Planet*> m_moons;
    OE_Engine::Light* m_light;
    SolarSystem* m_solarSystem;
    bool m_bLoaded = false;
    State m_state = State::LoadingEngine;
};

#endif // __TA_GAME_H__

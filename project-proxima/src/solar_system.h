#ifndef __SOLAR_SYSTEM_H__
#define __SOLAR_SYSTEM_H__

#include "proxima_private.h"
#include "entity.h"

class CelestialBody;
class Planet;

class SolarSystem : public Entity
{
public:
    SolarSystem();
    virtual ~SolarSystem();

    void addPlanet(Planet* planet);
    void connect(Planet* planetA, Planet* planetB);

    bool isLoaded() const;
    void addToScene();

private:
    SolarSystem(const SolarSystem& rhs);
    SolarSystem& operator=(const SolarSystem& rhs);

    Vector<Planet*> m_planets;
};

#endif // __SOLAR_SYSTEM_H__

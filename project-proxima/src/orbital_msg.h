#ifndef __ORBITAL_MSG_H__
#define __ORBITAL_MSG_H__

#include "proxima_private.h"

#include <flagset.h>

#include "message.h"

class OrbitalMsg : public Message
{
    RTTI_DECLARATION
    GET_SIZE_IMPL

public:
    OrbitalMsg(void* sender);

    void setOwnerKinematicComp(void* component);
    void setCentralBodyKinematicComp(void* component);

    uintptr_t m_ownerKinematicCompId;
    uintptr_t m_centralKinematicCompId;

    enum Flags
    {
        OwnerKinematicCompId,
        CentralBodyKinematicCompId,
    };

    FlagSet<Flags> m_set;
};

inline void OrbitalMsg::setOwnerKinematicComp(void* component)
{
    m_ownerKinematicCompId = (uintptr_t)component;
    m_set.set(OwnerKinematicCompId);
}

inline void OrbitalMsg::setCentralBodyKinematicComp(void* component)
{
    m_centralKinematicCompId = (uintptr_t)component;
    m_set.set(CentralBodyKinematicCompId);
}

#endif // __ORBITAL_MSG_H__

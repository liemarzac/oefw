#include "icosahedron_component_manager.h"

#include "proxima_private.h"

// OE_Engine
#include <camera.h>
#include <composer.h>

IcosahedronComponentManager::IcosahedronComponentManager()
{
    IcosahedronComponentConfig::createInstance();
}

IcosahedronComponentManager::~IcosahedronComponentManager()
{
    IcosahedronComponentConfig::destroyInstance();
}

void IcosahedronComponentManager::prepareTaskParams(
    IcosahedronComponentUpdateTaskParams* params,
    uint32 iPage,
    float deltaTime)
{
    ComponentManagerMT::prepareTaskParams(params, iPage, deltaTime);

    const Camera& camera = Composer::getInstance().getCamera();
    params->m_componentUpdateParams.m_camPosition = camera.getPosition();
    params->m_componentUpdateParams.m_camLookAt = camera.getLookAt();
}

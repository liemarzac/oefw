#ifndef __ICOSAHEDRON_COMPONENT_MANAGER_H__
#define __ICOSAHEDRON_COMPONENT_MANAGER_H__

#include "proxima.h"

 // OE_Engine
#include <component_manager.h>

// Ta
#include "icosahedron_component.h"

DEFINE_COMPONENT_UPDATE_TASK_PARAMS(IcosahedronComponent);
DEFINE_COMPONENT_UPDATE_TASK(IcosahedronComponent);


class IcosahedronComponentManager : public OE_Engine::ComponentManagerMT<
    IcosahedronComponent,
    IcosahedronComponentUpdateTask,
    IcosahedronComponentUpdateTaskParams>,
    public OE_Core::Singleton<IcosahedronComponentManager>
{
public:
    IcosahedronComponentManager();
    virtual ~IcosahedronComponentManager();

    virtual void prepareTaskParams(
        IcosahedronComponentUpdateTaskParams* params,
        uint32 iPage,
        float deltaTime) override;
};

#endif // __ICOSAHEDRON_COMPONENT_MANAGER_H__

#ifndef __ASTRO_PHYSICS_H__
#define __ASTRO_PHYSICS_H__

#include "proxima_private.h"
#include "eigen_helpers.h"

namespace AstroPhysics
{

// Gravitational constant
extern Real G;

struct OrbitalElements
{
    OrbitalElements()
    {
        memset(this, 0, sizeof(OrbitalElements));
    }

    Real m_semiMajorAxis;
    Real m_eccentricity;
    Real m_argumentOfPeriapsis;
    Real m_lan; // Longitude of ascending node
    Real m_inclination;
    Real m_trueAnomaly;
    Real m_eccentricAnomaly;
    Real m_meanAnomaly;
    Real m_trueLongitude;
    Real m_longitudeOfPeriapsis;
    Real m_argumentOfLatitude;
};

inline Real circularOrbitVelocity(Real mass, Real r)
{
    return sqrt((G * mass) / r);
}

void stateVectorsToOrbitalElements(
    const Vec3d& position,
    const Vec3d& velocity,
    Real mass,
    OrbitalElements& orbitalElements);

void orbitalElementsToStateVectors(
    const OrbitalElements& orbitalElements,
    Real mass,
    Vec3d& positionVector,
    Vec3d& velocityVector);

Real eccentricAnomaly(Real M, Real e, Real delta);

Real trueAnomaly(Real E, Real e);

Real orbitalPeriod(Real a, Real M);

Real radiusFromTrueAnomaly(Real T, Real a, Real e);

Real radiusFromEccentricAnomaly(Real T, Real a, Real e);


}; // namespace AstroPhysics

#endif // __ASTRO_PHYSICS_H__

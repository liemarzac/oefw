#ifndef __ICOSAHEDRON_COMPONENT_H__
#define __ICOSAHEDRON_COMPONENT_H__

#include "proxima.h"

// External
#include <thread>
#include <json/json.h>

// OE_Core
#include <eigen_helpers.h>
#include <flagset.h>
#include <singleton.h>
#include <stop_watch.h>

// OE_Engine
#include <component.h>

// OE_Graphic
#include <color.h>
#include <graphic_resource_helpers.h>

// Ta
#include "component.h"
#include "color.h"


struct IcosahedronComponentUpdateParams : public OE_Engine::ComponentUpdateParams
{
    EIGEN_ALIGNED
    Vec3 m_camPosition;
    Vec3 m_camLookAt;
};

class IcosahedronComponentConfig : public OE_Core::Singleton<IcosahedronComponentConfig>
{
public:
    IcosahedronComponentConfig();

    struct TesselationEntry
    {
        uint32 m_surface = 0;
        uint32 m_lod = 0;
    };

    Vector<TesselationEntry> m_tesselation;
};

class IcosahedronComponent : public OE_Engine::Component
{
    RTTI_DECLARATION

public:
    typedef uint16 TIndex;
    typedef uint8 TLevel;
    typedef uint8 TTag;

    struct GenerationParams
    {
        float m_radius = 1.0f;;
        OE_Graphic::Color m_color = OE_Graphic::ColorList::Yellow;
    };

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    IcosahedronComponent();
    virtual ~IcosahedronComponent();

    OE_Graphic::PropResPtr generate(const GenerationParams& params);
    void setColor(OE_Graphic::Color color);
    void update(const IcosahedronComponentUpdateParams& params);

private:
    NO_COPY(IcosahedronComponent)

    uint16 addUniqueVertex(const Vec3& position);
    OE_Graphic::PropResPtr generateMesh(const Vec3& camLookAt);
    void getPosition(uint16 index, Vec3& position) const;
    void subdivideNode(TIndex iNode);
    void tesselate();
    bool isInNode(const Vec3& pos, TIndex iNode) const;
    TIndex getNodeFromPosition(const Vec3& pos, int32 iMaxLOD = -1) const;
    TIndex getNodeFromPosition(const Vec3& pos, TIndex iNode, int32 iMaxLOD) const;

    struct SubdivisionParams
    {
        SubdivisionParams()
        {
            memset(this, 0, sizeof(*this));
        }

        TIndex iSrcNode;
        TIndex iEdgePoints[3];
        TIndex iSrcTriangleNeighbors[2];
        TIndex iDstNeighborNodes[2];
        bool bInitial;
    };

    class TesselationSync
    {
    public:

        enum Writer
        {
            Owner,
            Worker
        }m_writer;

        enum class Stage
        {
            Starting,
            Tesselating,
            WorkFinished,
            Aborting
        };
        Atomic<Stage> m_stage;

        TesselationSync()
        {
            m_stage = Stage::WorkFinished;
            m_writer = Writer::Owner;
        }

        void start()
        {
            OE_CHECK(m_writer == Writer::Owner, OE_CHANNEL_CORE);

            UniqueLock lock(m_mutex);
            m_stage = Stage::Starting;
            m_writer = Writer::Worker;

            // Assume the tessellation will not change.
            m_bChanged = false;

#if !OE_RELEASE
            m_workStopWatch.start();
#endif
            m_notifier.notify_all();
        }

        bool waitForOwner()
        {
            UniqueLock lock(m_mutex);

            if(m_stage != Stage::Starting && m_stage != Stage::Aborting)
            {
                m_notifier.wait(lock, [this]{return m_stage == Stage::Starting || m_stage == Stage::Aborting;});
            }

            return m_stage == Stage::Starting;
        }

        bool isWorkerFinished()
        {
            return m_stage == Stage::WorkFinished;
        }

        void workFinished()
        {
            OE_CHECK(m_writer == Writer::Worker, OE_CHANNEL_CORE);

            UniqueLock lock(m_mutex);
#if !OE_RELEASE
            m_workStopWatch.stop();
#endif
            if(m_stage != Stage::Aborting)
            {
                m_stage = Stage::WorkFinished;
            }
            
            m_writer = Writer::Owner;
        }

        void abort()
        {
            UniqueLock lock(m_mutex);
            m_stage = Stage::Aborting;
            m_notifier.notify_all();
        }

        bool shouldContinue() const
        {
            return m_stage != Stage::Aborting;
        }

        bool hasChanged() const
        {
            return m_bChanged;
        }

        void setChanged()
        {
            m_bChanged = true;
        }

#if !OE_RELEASE
        double getWorkTime() const
        {
            OE_CHECK(m_writer == Writer::Owner, OE_CHANNEL_CORE);
            return m_workStopWatch.getElapsedTime();
        }
#endif

    private:
        std::condition_variable m_notifier;
        std::mutex m_mutex;
        OE_Core::StopWatch m_workStopWatch;
        bool m_bChanged = false;
    };

    enum class FanDirection
    {
        Clockwise,
        AntiClockwise
    };

    bool subdivideNode(TIndex iNode, SubdivisionParams& params);
    void subdivideNodeInTransition(TIndex iNode, SubdivisionParams& params);
    void subdivideNodeToTransition(TIndex iNode, SubdivisionParams& params);
    bool popNode(TIndex iNode);
    bool popNode(TIndex iNode, Vector<TIndex>& iPointsToTelease);
    void popNodeToTransition(TIndex iNode, TIndex iIncomingNode, Vector<TIndex>& iPointsToTelease);
    void findNodesAroundFanPoint(TIndex iInitialNode, TIndex iCurrentNode, TIndex iFanPoint, Vector<TIndex>& iNodes, FanDirection direction) const;
    void findNodeAroundFanPoint(TIndex iInitialNode, TIndex iFanPoint, Vector<TIndex>& iNodes) const;
    bool findEdgeForNeighbor(TIndex iNode, TIndex iNeighborNode, uint& iEdge) const;
    uint32 findTransitionEdge(TIndex iNode) const;
    TIndex findCounterClockWiseNeighborNode(TIndex iNode, TIndex iPoint) const;
    TIndex findOppositveNodeOfTransitioningNode(TIndex iTransitioningNode) const;
    TIndex getSubpointForEdge(TIndex iNode, uint32 iEdge) const;

    void connectChildrenNeighbors(TIndex iNode, SubdivisionParams& params);
    void acquirePoints(uint32 n, TIndex indices[]);
    void acquireNodes(uint32 n, TIndex indices[]);
    void releasePoints(const TIndex indices[], uint32 n);
    void releaseNodes(const TIndex indices[], uint32 n);
    void makeSubNodes(TIndex iParentNode, const TIndex iOriginIndices[3], const TIndex iNewIndices[3], TIndex iNewNodes[4]);
    void setMidPointPosition(TIndex iP1, TIndex iP2, TIndex iPMid);
    void propagateAndConnect(
        TIndex iNode,
        const TIndex iOriginIndices[3],
        const TIndex iSubIndices[3],
        const TIndex iSubNodes[4]);
    void setChildren(TIndex iNode, const TIndex iSubNodes[4]);
    bool hasChildren(TIndex iNode) const;
    bool isFullySubdivided(TIndex iNode) const;
    uint32 getNumChildren(TIndex iNode) const;
    bool isTransitioning(TIndex iNode) const;
    bool isNodeUsed(TIndex iNode) const;
    bool isTagCurrent(TIndex iNode) const;
    bool hasGrandChildren(TIndex iNode) const;
    float calcNormalDotLookAt(TIndex iNode, const Vec3& lookAt, bool bNormalize) const;
    float calcVisibleSurface(TIndex iNode) const;
    bool processPendingNodes(const Vec3& lookAt);
    void tag(TIndex iNode);
    static uint32 getLODFromVisibleSurface(uint32 visibleSurface);

    struct FoundNeighborChildSharedResult 
    {
        TIndex m_iNode;
        uint m_iEdge;
    };

    bool findNeighborsChildSharingEdge(
        TIndex iNeightborNode,
        const TIndex points[2],
        FoundNeighborChildSharedResult& result) const;

    void findParentNeighborsSharingChildEdge(TIndex iChildNode, TIndex iChildEdgeParentNeightbor[3]) const;

#if _DEBUG
    void sanityCheck();
#endif

    static bool isValidIndex(TIndex iIndex);

    static uint32 modIndex(TIndex i);

    float m_radius;
    OE_Graphic::Color m_color;

    struct Triangle
    {
        TIndex m_indices[3];
    };

    struct Node
    {
        enum class Flags
        {
            HasChildren,
            Transitioning,
            MarkedToPop,
            Used
        };

        Node()
        {
            memset(m_iNeighbors, InvalidIndex, sizeof(m_iNeighbors));
            memset(m_iChildren, InvalidIndex, sizeof(m_iChildren));
        }

        TIndex m_iParent = InvalidIndex;
        TIndex m_iNeighbors[3];
        TIndex m_iChildren[4];
        OE_Core::FlagSet<Flags> m_flags;
        TLevel m_level = InvalidIndex;
    };

    struct Point
    {
        Point()
        {
#ifdef _DEBUG
            for(uint i = 0; i < 3; ++i)
            {
                m_pos[i] = std::numeric_limits<float>::quiet_NaN();
            }
            m_bUsed = false;
#endif
        }

        float m_pos[3];

#if !OE_RELEASE
        bool m_bUsed;
#endif
    };

    struct Edge
    {
        Edge()
        {
#ifdef _DEBUG
            memset(m_indices, -1, sizeof(m_indices));
#endif
        }

        TIndex m_indices[2];
    };

    OE_Graphic::PropResPtr m_propResource;
    Vector<Triangle> m_triangles;
    Vector<Point> m_points;
    Vector<Node> m_nodes;
    Vector<TTag> m_nodeTags;
    Vector<TIndex> m_freePoints;
    Vector<TIndex> m_freeTriangles;
    Vector<TIndex> m_freeNodes;
    Vector<TIndex> m_pendingProcessNodes;
    uint32 m_nPoints;
    uint32 m_nTriangles;
    uint32 m_iCurrentTesselationLOD;
    Vec3 m_currentPos;
    Vec3 m_currentLookAt;
    TTag m_currentTag;

    std::thread m_tesselationThread;
    TesselationSync m_tesselationSync;

    bool m_bPop;

    static TIndex m_splitThreshold;

#if !OE_RELEASE
    enum class DebugRenderMode
    {
        None,
        General,
        Triangles,
        Points,
        Surface
    }m_debugRenderMode = DebugRenderMode::None;

    bool m_bFreeze = false;
    double m_workTime = 0.0;
    bool m_debug = true;
#endif
};

inline bool IcosahedronComponent::isNodeUsed(TIndex iNode) const
{
    return m_nodes[iNode].m_flags.isSet(Node::Flags::Used);
}

inline bool IcosahedronComponent::isValidIndex(TIndex iIndex)
{
    return iIndex != (TIndex)-1;
}

inline bool IcosahedronComponent::isFullySubdivided(TIndex iNode) const
{
    return hasChildren(iNode) && !isTransitioning(iNode);
}

inline bool IcosahedronComponent::isTagCurrent(TIndex iNode) const
{
    return m_nodeTags[iNode] == m_currentTag;
}

inline uint IcosahedronComponent::modIndex(TIndex i)
{
    return  i % 3;
}

inline void IcosahedronComponent::tag(TIndex iNode)
{
    m_nodeTags[iNode] = m_currentTag;
}

#endif // __ICOSAHEDRON_COMPONENT_H__

#ifndef __GRAVITY_COMPONENT_H__
#define __GRAVITY_COMPONENT_H__

#include "proxima.h"

// OE_Core
#include <eigen_helpers.h>
#include <flagset.h>

// OE_Engine
#include <component.h>


DEFINE_COMPONENT_UPDATE_PARAMS(GravityComponent)

class GravityComponent : public OE_Engine::Component
{
    RTTI_DECLARATION

public:
    EIGEN_ALIGNED

    GravityComponent();
    virtual ~GravityComponent();

    GravityComponent(const GravityComponent& rhs) = delete;
    GravityComponent& operator=(const GravityComponent& rhs) = delete;

    void update(const GravityComponentUpdateParams& params);

    bool isInitialized() const;

private:
    struct Source
    {
        enum class SetFlags
        {
            Id,
            Mass,
            Position,
            NumberOf
        };

        Source()
        {
            m_id = 0;
            m_position = Vec3d(0.0, 0.0, 0.0);
            m_mass = 0;
        }
        uintptr_t m_id;
        Vec3d m_position;
        Real m_mass;
        OE_Core::FlagSet<SetFlags> m_setFlags;
    }m_source;

    struct Receiver
    {
        enum class SetFlags
        {
            Id,
            Position,
            NumberOf
        };

        Receiver()
        {
            m_id = 0;
            m_position = Vec3d(0.0, 0.0, 0.0);
            m_accelCompReceiver = nullptr;
        }
        uintptr_t m_id;
        Vec3d m_position;
        Component* m_accelCompReceiver;
        OE_Core::FlagSet<SetFlags> m_setFlags;
    };
    Vector<Receiver> m_receivers;

    enum class InitFlags
    {
        SourceSet,
        ReceiverSet,
        SourcePositionSet,
        ReceiverPositionSet,
        NumberOf,
    };
};

#endif // __GRAVITY_COMPONENT_H__

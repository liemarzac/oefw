#include "orbital_msg.h"

RTTI_IMPL_PARENT(OrbitalMsg, Message)

OrbitalMsg::OrbitalMsg(void* sender) :
    Message(sender)
{
    m_ownerKinematicCompId = 0;
    m_centralKinematicCompId = 0;
}



#include "orbital_component.h"

#include "proxima_private.h"

// OE_Core
#include <oe_math.h>

// OE_Engine
#include <kinematic_msg.h>
#include <composer.h>

// Ta
#include "celestial_body_properties_msg.h"
#include "orbital_msg.h"


using namespace AstroPhysics;

RTTI_IMPL_PARENT(OrbitalComponent, Component)

OrbitalComponent::OrbitalComponent()
{
    m_kinematicCompId = 0;
    m_position = Vec3d(0.0, 0.0, 0.0);
    m_orbitalPeriod = 0.0;
    m_timeSincePeriapsis = 0.0f;
}

OrbitalComponent::~OrbitalComponent()
{
}

void OrbitalComponent::update(const OrbitalComponentUpdateParams& params)
{
    while(Message* message = m_messageQueue.beginMessage())
    {
        uint32 messageTypeHash = message->getTypeHash();
        if(KinematicMsg* castedMsg = cast<KinematicMsg>(message))
        {
            if(castedMsg->m_set.isSet(KinematicMsg::Flags::Velocity))
            {
                if(castedMsg->m_sender == m_kinematicCompId)
                {
                    m_velocity = castedMsg->m_velocity;
                }
            }

            if(castedMsg->m_set.isSet(KinematicMsg::Flags::Position))
            {
                if(castedMsg->m_sender == m_kinematicCompId)
                {
                    m_position = castedMsg->m_position;
                }
                else if(castedMsg->m_sender == m_centralBody.m_kinematicCompId)
                {
                    m_centralBody.m_position = castedMsg->m_position;
                }
            }
        }
        else if(CelestialBodyPropertiesMsg* castedMsg = cast<CelestialBodyPropertiesMsg>(message))
        {
            m_centralBody.m_mass = castedMsg->m_mass;
        }
        else if(OrbitalMsg* castedMsg = cast<OrbitalMsg>(message))
        {
            if(castedMsg->m_set.isSet(OrbitalMsg::OwnerKinematicCompId))
            {
                m_kinematicCompId = castedMsg->m_ownerKinematicCompId;
            }
            else if(castedMsg->m_set.isSet(OrbitalMsg::CentralBodyKinematicCompId))
            {
                m_centralBody.m_kinematicCompId = castedMsg->m_centralKinematicCompId;
            }
        }

        m_messageQueue.endMessage();
    }

    if(m_velocity.norm() < EPSILON_D)
        return;

    Vec3d deltaPos = m_position - m_centralBody.m_position;

    OrbitalElements prevOrbitalElements = m_orbitalElements;

    m_timeSincePeriapsis += params.m_deltaTime;

    stateVectorsToOrbitalElements(
        deltaPos,
        m_velocity,
        m_centralBody.m_mass,
        m_orbitalElements);


    m_orbitalPeriod = orbitalPeriod(m_orbitalElements.m_semiMajorAxis, m_centralBody.m_mass);

    if(prevOrbitalElements.m_meanAnomaly > m_orbitalElements.m_meanAnomaly)
    {
        // We just passed periapsis;
        m_timeSincePeriapsis = 0.0f;
    }

//#if _DEBUG
//    if(debug())
//    {
        Real r = radiusFromTrueAnomaly(m_orbitalElements.m_trueAnomaly, m_orbitalElements.m_semiMajorAxis, m_orbitalElements.m_eccentricity);
        Real r2 = radiusFromEccentricAnomaly(m_orbitalElements.m_eccentricAnomaly, m_orbitalElements.m_semiMajorAxis, m_orbitalElements.m_eccentricity);

        Vec3d posCalc;
        Vec3d velCalc;
        orbitalElementsToStateVectors(m_orbitalElements, m_centralBody.m_mass, posCalc, velCalc);

        //Composer* renderer = Engine::getInstance().getRenderer();

        //Vec2 positionOnScreen;
        //renderer->projectOnScreen(vec3dToVect3(m_position), positionOnScreen);

        //Rect2D rect;
        //uint32 w = 4;
        //uint32 h = 4;
        //rect.m_x = (uint32)positionOnScreen[0] - (w / 2);
        //rect.m_y = (uint32)positionOnScreen[1] - (h / 2);
        //rect.m_width = w;
        //rect.m_height = h;
        //renderer->renderQuadColored2d(rect, ColorList::WhiteSmoke);

        //int32 dx = -5;
        //int32 dy = -30;
        //uint32 thickness = 1;
        //Vec2i p1((int32)positionOnScreen[0], (int32)positionOnScreen[1]);
        //Vec2i p2((int32)positionOnScreen[0] + dx, (int32)positionOnScreen[1] + dy);
        //renderer->renderLinedColored2d(p1, p2, ColorList::WhiteSmoke, thickness);

        //Vec2i p3 = p2 + Vec2i(-150, 0);
        //renderer->renderLinedColored2d(p2, p3, ColorList::WhiteSmoke, thickness);

        //char buffer[512];
        //Vec2i pText = p3 + Vec2i(0, 5);

        //if(m_orbitalElements.m_trueLongitude != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "true longitude: %.2f", radToDeg(m_orbitalElements.m_trueLongitude));
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //if(m_orbitalElements.m_argumentOfLatitude != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "argument of latitude: %.2f", radToDeg(m_orbitalElements.m_argumentOfLatitude));
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //if(m_orbitalElements.m_longitudeOfPeriapsis != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "longitude of periapsis: %.2f", radToDeg(m_orbitalElements.m_longitudeOfPeriapsis));
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //OE_SPRINTF(buffer, 512, "M: %.2f; dM %.2f/sec", radToDeg(m_orbitalElements.m_meanAnomaly), radToDeg(m_orbitalElements.m_meanAnomaly - prevOrbitalElements.m_meanAnomaly) / params.m_deltaTime);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "E: %.2f; dE %.2f/sec", radToDeg(m_orbitalElements.m_eccentricAnomaly), radToDeg(m_orbitalElements.m_eccentricAnomaly - prevOrbitalElements.m_eccentricAnomaly) / params.m_deltaTime);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //if(m_orbitalElements.m_trueAnomaly != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "T: %.2f; dT %.2f/sec", radToDeg(m_orbitalElements.m_trueAnomaly), radToDeg(m_orbitalElements.m_trueAnomaly - prevOrbitalElements.m_trueAnomaly) / params.m_deltaTime);
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //if(m_orbitalElements.m_lan != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "LAN: %.2f", radToDeg(m_orbitalElements.m_lan));
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //if(m_orbitalElements.m_argumentOfPeriapsis != INFINITY_D)
        //{
        //    OE_SPRINTF(buffer, 512, "AoP: %.2f", radToDeg(m_orbitalElements.m_argumentOfPeriapsis));
        //    pText += Vec2i(0, -15);
        //    renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
        //}

        //OE_SPRINTF(buffer, 512, "i: %.2f", radToDeg(m_orbitalElements.m_inclination));
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "e: %.2f", m_orbitalElements.m_eccentricity);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "a: %.2f", m_orbitalElements.m_semiMajorAxis);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "r: %.2f ; %.2f", r, r2);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "t: %.2f / %.2f", m_timeSincePeriapsis, m_orbitalPeriod);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "velC: %.2f;%.2f;%.2f", velCalc[0], velCalc[1], velCalc[2]);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "vel: %.2f;%.2f;%.2f", m_velocity[0], m_velocity[1], m_velocity[2]);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "posC: %.2f;%.2f;%.2f", posCalc[0], posCalc[1], posCalc[2]);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);

        //OE_SPRINTF(buffer, 512, "pos: %.2f;%.2f;%.2f", m_position[0], m_position[1], m_position[2]);
        //pText += Vec2i(0, -15);
        //renderer->renderText(buffer, "vera", 12, pText[0], pText[1], HorizontalAnchor::Left, VerticalAnchor::Bottom);
//    }
//#endif
}



#ifndef __GRAVITY_MSG_H__
#define __GRAVITY_MSG_H__

#include <flagset.h>

#include "proxima_private.h"

#include "message.h"

namespace OE_Engine
{
    class Component;
}

class GravityMsg : public Message
{
    RTTI_DECLARATION
    GET_SIZE_IMPL

public:
    GravityMsg(void* sender);

    void setSource(Real mass);
    void setReceiver(Component* accelCompReceiver);

    Real m_mass;
    Component* m_accelCompReceiver;

    enum class Flags
    {
        Source,
        Receiver,
    };

    FlagSet<Flags> m_set;
};

#endif // __GRAVITY_MSG_H__

﻿#include "astro_physics.h"

#include "proxima_private.h"

// OE_Core
#include <oe_math.h>


namespace AstroPhysics
{

// Gravitational constant
double G = 6.67408e-11;

void stateVectorsToOrbitalElements(
    const Vec3d& positionVector,
    const Vec3d& velocityVector,
    Real mass,
    OrbitalElements& orbitalElements)
{
    // Change frame of reference to have z pointing up.
    Vec3d _r = Vec3d(positionVector[0], positionVector[2], positionVector[1]);
    Vec3d _v = Vec3d(velocityVector[0], velocityVector[2], velocityVector[1]);

     // Calculate orbital momentum vector.
    Vec3d _h = _r.cross(_v);

    Vec3d _k(0.0f, 0.0f, 1.0f);

    Vec3d _n = _k.cross(_h);
    Vec3d __n = _n.normalized();
    Real n = _n.norm();

    // Calculate gravitation parameter.
    Real mu = G * mass;

    // Calculate eccentricity.
    Real v = _v.norm();
    Real r = _r.norm();
    Real v2 = v * v;
    Vec3d _e = (((v2 - (mu / r)) * _r) - ((_r.dot(_v)) * _v)) / (mu);
    Real e = _e.norm();

    Real energy = (v2 / 2.0) - (mu / r);

    Real h = _h.norm();

    // Calculate semi-major axis 'a'.
    Real a = 0.0;
    Real p = 0.0;
    if(fabs(e - 1.0) > EPSILON_D)
    {
        a = -mu / (2.0 * energy);
        p = a * (1.0 - (e * e));
    }
    else
    {
        a = INFINITY_D;
        p = (h * h) / mu;
    }

    // Calculate inclination 'i'.
    Real i = acos(_h[2] / h);

    // Calculate Longitude of the Ascending Node 'Omega'
    Real Omega = INFINITY_D;
    if(n > EPSILON_D)
    {
        Omega = acos(_n[0] / n);

        if(_n[1] < 0.0)
            Omega = (2 * PI) - Omega;
    }

    // Calculate argument of periapsis 'omega'
    Real omega = INFINITY_D;
    if((n * e) > EPSILON_D)
    {
        omega = acos(_n.dot(_e) / (n * e));

        if(_e[2] < 0.0)
            omega = (2 * PI) - omega;
    }

    // Calculate true anomaly 'nu'.
    Real nu = INFINITY_D;
    if((e * r) > EPSILON_D)
    {
        nu = acos(_e.dot(_r) / (e * r));

        if(_r.dot(_v) < 0.0)
            nu = (2.0 * PI) - nu;
    }

    // Calculate eccentric anomaly 'E'.
    Real E = atan2((sqrt(1 - (e * e)) * sin(nu)), (e + cos(nu)));

    if(E < 0.0)
        E = (2 * PI) + E;

    // Calculate mean anomaly 'M'.
    Real M = E - e * sin(E);

    Vec3d _principal(1.0, 0.0f, 0.0f);

    Real argumentOfLatitude = INFINITY_D;
    Real longitudeOfPeriapsis = INFINITY_D;
    Real trueLongitude = INFINITY_D;

    if(e < EPSILON_D && (i < EPSILON_D || abs(i - PI) < EPSILON_D))
    {
        // Calculate True Longitude which is used when e is 0 and i is 0 or 180.
        trueLongitude = acos(_r.dot(_principal) / r);
        if(_r[1] < 0.0)
            trueLongitude = (2.0 * PI) - trueLongitude;
    }
    else
    {
        if(e < EPSILON_D)
        {
            // Calculate Argument of Latitude used when e is 0.
            argumentOfLatitude = acos(__n.dot(_r) / r);

            Vec3d _nxh = _n.cross(_h);
            if(_r.dot(_nxh) > 0)
                argumentOfLatitude = (2.0 * PI) - argumentOfLatitude;
        }
        else if(i < EPSILON_D || abs(i - PI) < EPSILON_D)
        {
            // Calculate Longitude of Periapsis used when i is 0 or 180.
            longitudeOfPeriapsis = acos(_e.dot(_r) / (r * e));

            Vec3d __exh = _e.cross(_h);
            if(_r.dot(__exh) > 0)
                longitudeOfPeriapsis = (2.0 * PI) - longitudeOfPeriapsis;
        }
    }

    orbitalElements.m_argumentOfPeriapsis = omega;
    orbitalElements.m_eccentricity = e;
    orbitalElements.m_inclination = i;
    orbitalElements.m_lan = Omega;
    orbitalElements.m_semiMajorAxis = a;
    orbitalElements.m_trueAnomaly = nu;
    orbitalElements.m_eccentricAnomaly = E;
    orbitalElements.m_meanAnomaly = M;
    orbitalElements.m_trueLongitude = trueLongitude;
    orbitalElements.m_argumentOfLatitude = argumentOfLatitude;
    orbitalElements.m_longitudeOfPeriapsis = longitudeOfPeriapsis;
}

void orbitalElementsToStateVectors(
    const OrbitalElements& orbitalElements,
    Real mass,
    Vec3d& positionVector,
    Vec3d& velocityVector)
{
    Real E = orbitalElements.m_eccentricAnomaly;
    Real T = orbitalElements.m_trueAnomaly;
    Real a = orbitalElements.m_semiMajorAxis;
    Real e = orbitalElements.m_eccentricity;
    Real i = orbitalElements.m_inclination;
    Real LAN = orbitalElements.m_lan;
    Real AoP = orbitalElements.m_argumentOfPeriapsis;

    Real r = radiusFromEccentricAnomaly(E, a, e);

    // Calculate gravitation parameter.
    Real mu = G * mass;

    // Calculate position on orbital frame reference where z is perpendicular to the orbital plane,
    // and x pointing to the periapsis of the orbit.
    Vec3d _o = r * Vec3d(cos(T), sin(T), 0.0);

    Vec3d _odt = (sqrt(mu * a) / r) * Vec3d(-sin(E), sqrt(1 - (e * e)) * cos(E), 0.0);

    Eigen::AngleAxisd qa(-LAN, Eigen::Vector3d::UnitZ());
    Eigen::AngleAxisd qb(-i, Eigen::Vector3d::UnitX());
    Eigen::AngleAxisd qc(-AoP, Eigen::Vector3d::UnitZ());

    Eigen::Quaternion<double> q = qa * qb * qc;

    Vec3d _r = q * _o;
    Vec3d _v = q * _odt;

    positionVector = Vec3d(_r[0], _r[2], _r[1]);
    velocityVector = Vec3d(_v[0], _v[2], _v[1]);
}

Real eccentricAnomaly(Real M, Real e, Real delta)
{
    Real E = e < 0.8 ? M : PI;

    Real dE = E - e * sin(M) - M;
    while(abs(dE) > delta)
    {
        E = E - dE / (1.0 - e * cos(E));
        dE = E - e * sin(E) - M;
    }

    return E;
}

Real trueAnomaly(Real E, Real e)
{
    Real sinE = sin(E);
    Real cosE = cos(E);

    Real tmp = sqrt(1.0 - (e * e));
    return atan2(tmp * sinE, cosE - e);
}

Real orbitalPeriod(Real a, Real M)
{
    return 2 * PI * sqrt((a * a * a) / (G * M));
}

Real radiusFromTrueAnomaly(Real T, Real a, Real e)
{
    return (a * (1 - (e * e))) / (1 + (e * cos(T)));
}

Real radiusFromEccentricAnomaly(Real E, Real a, Real e)
{
    return a * (1 - e * cos(E));
}

}

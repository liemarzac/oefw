#ifndef __ORBITAL_COMPONENT_H__
#define __ORBITAL_COMPONENT_H__

#include "proxima_private.h"

#include "astro_physics.h"
#include "component.h"

DEFINE_COMPONENT_UPDATE_PARAMS(OrbitalComponent)

class OrbitalComponent : public Component
{
    RTTI_DECLARATION

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    OrbitalComponent();
    virtual ~OrbitalComponent();

    void update(const OrbitalComponentUpdateParams& params);

private:
    OrbitalComponent(const OrbitalComponent& rhs);
    OrbitalComponent& operator=(const OrbitalComponent& rhs);

    AstroPhysics::OrbitalElements m_orbitalElements;

    struct CentralBody
    {
        CentralBody()
        {
            m_mass = 0.0;
            m_position = Vec3d(0.0, 0.0, 0.0);
            m_kinematicCompId = 0;
        }
        uintptr_t m_kinematicCompId;
        Real m_mass;
        Vec3d m_position;
    }m_centralBody;

    uintptr_t m_kinematicCompId;
    Vec3d m_position;
    Vec3d m_velocity;
    Real m_orbitalPeriod;
    Real m_timeSincePeriapsis;
};

#endif // __ORBITAL_COMPONENT_H__

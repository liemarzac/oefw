#include "celestial_body.h"

#include "kinematic_component.h"
#include "kinematic_msg.h"
#include "orbital_component_manager.h"

CelestialBody::CelestialBody(const String& name):
    Prop(name)
    ,m_mass(1.0f)
    ,m_kinematicComponent(nullptr)
{
    m_orbitalComponent = OrbitalComponentManager::getInstance().createComponent();
    m_orbitalComponent->setName(name);
    m_orbitalComponent->acceptMessages(true);
    attachComponent(m_orbitalComponent);
}

CelestialBody::~CelestialBody()
{
    detachComponent(m_orbitalComponent);
    OrbitalComponentManager::getInstance().destroyComponent(m_orbitalComponent);
}

void CelestialBody::setVelocity(const Vec3d& velocity)
{
    if(m_kinematicComponent)
    {
        KinematicMsg* kinematicMsg = m_kinematicComponent->allocMessage<KinematicMsg>(this);
        kinematicMsg->setVelocity(velocity);
        m_kinematicComponent->commitMessage();
    }
}

#include "planet.h"

#include "celestial_body_properties_msg.h"
#include "icosahedron_component_manager.h"
#include "gravity_component.h"
#include "gravity_component_manager.h"
#include "gravity_msg.h"
#include "kinematic_component.h"
#include "kinematic_component_manager.h"
#include "kinematic_msg.h"
#include "orbital_component.h"
#include "orbital_msg.h"
#include "prop_component_manager.h"
#include "spacial_state_msg.h"

#define OE_CHANNEL_PLANET "Planet"

Planet::Planet(const String& name):
    CelestialBody(name)
    ,m_radius(1.0f)
    ,m_gravityComponent(nullptr)
{
    m_color = ColorList::Yellow;
}

Planet::~Planet()
{
    if(m_icosahedronComponent)
        IcosahedronComponentManager::getInstance().destroyComponent(m_icosahedronComponent);

    if(m_kinematicComponent)
        KinematicComponentManager::getInstance().destroyComponent(m_kinematicComponent);

    if(m_gravityComponent)
        GravityComponentManager::getInstance().destroyComponent(m_gravityComponent);
}

void Planet::setRadius(float radius)
{
    m_radius = radius;
}

void Planet::generate(bool bDynamic)
{

    PropResPtr propResource;

    // Create icosahedron.
    OE_CHECK(!m_icosahedronComponent, OE_CHANNEL_PLANET);
    m_icosahedronComponent = IcosahedronComponentManager::getInstance().createComponent();
    IcosahedronComponent::GenerationParams generationParams;
    generationParams.m_color = m_color;
    generationParams.m_radius = m_radius;
    propResource = m_icosahedronComponent->generate(generationParams);

    // Create prop component.
    PropComponent* propComponent = createPropComponent(propResource);

    // Create kinematic component.
    m_kinematicComponent = KinematicComponentManager::getInstance().createComponent();
    m_kinematicComponent->acceptMessages(true);
    m_kinematicComponent->setName(getName());
    attachComponent(m_kinematicComponent);

    // Give initial values to kinematic component.
    KinematicMsg* kinematicMsg = m_kinematicComponent->allocMessage<KinematicMsg>(this, getName());
    kinematicMsg->setPosition(vec3ToVect3d(m_position));
    m_kinematicComponent->commitMessage();

    // Create gravity component.
    m_gravityComponent = GravityComponentManager::getInstance().createComponent();
    m_gravityComponent->acceptMessages(true);
    m_gravityComponent->setName(getName());
    attachComponent(m_gravityComponent);

    // Set the source of gravity to be the planet's kinematic component,
    // because it is that component which is going to report the position of the planet.
    GravityMsg* gravitySourceMsg = m_gravityComponent->allocMessage<GravityMsg>(m_kinematicComponent);
    gravitySourceMsg->setSource(m_mass);
    m_gravityComponent->commitMessage();

    // Gravity component receives kinematic component message.
    m_kinematicComponent->connectTo(m_gravityComponent, KinematicMsg::getTypeHash());

    // Tell the orbital component what is our kinematic component.
    OrbitalMsg* orbitMsg = m_orbitalComponent->allocMessage<OrbitalMsg>(this, getName());
    orbitMsg->setOwnerKinematicComp(m_kinematicComponent);
    m_orbitalComponent->commitMessage();

    // Kinematic component update orbital component to re-calculate orbital elements.
    m_kinematicComponent->connectTo(m_orbitalComponent, KinematicMsg::getTypeHash());
}

void Planet::onLoadedCallbackImpl()
{
    m_kinematicComponent->connectTo(m_propComponent, SpacialStateMsg::getTypeHash());
}

void Planet::pull(Planet* receiver)
{
    OrbitalComponent* receiverOrbitalComp = receiver->getComponent<OrbitalComponent>();
    OE_CHECK(receiverOrbitalComp, OE_CHANNEL_PLANET);

    // Communicate to the orbiting planet our kinematic component id.
    OrbitalMsg* orbitMsg = receiverOrbitalComp->allocMessage<OrbitalMsg>(this, getName());
    orbitMsg->setCentralBodyKinematicComp(m_kinematicComponent);
    receiverOrbitalComp->commitMessage();

    // Communicate to the orbiting planet our properties.
    CelestialBodyPropertiesMsg* properties = receiverOrbitalComp->allocMessage<CelestialBodyPropertiesMsg>(this, getName());
    properties->m_mass = m_mass;
    receiverOrbitalComp->commitMessage();

    // Communicate continuously to the orbiting planet our position.
    OE_CHECK(m_kinematicComponent, OE_CHANNEL_PLANET);
    m_kinematicComponent->connectTo(receiverOrbitalComp, KinematicMsg::getTypeHash());
}

void Planet::pulledBy(Planet* source)
{
    // Communicate to the source planet pulling us, our position
    // to receive the correct gravity acceleration.
    GravityComponent* sourceGravityComp = source->getComponent<GravityComponent>();
    OE_CHECK(sourceGravityComp, OE_CHANNEL_PLANET);

    // Tell to the gravity component of the source planet that is it pulling us.
    // So when we send our position, it sends back its acceleration on us.
    GravityMsg* gravitySourceMsg = sourceGravityComp->allocMessage<GravityMsg>(m_kinematicComponent);
    gravitySourceMsg->setReceiver(m_kinematicComponent);
    sourceGravityComp->commitMessage();

    m_kinematicComponent->connectTo(sourceGravityComp, KinematicMsg::getTypeHash());

#if _DEBUG
    //m_orbitalComponent->debug(true);
#endif
}

bool Planet::isLoaded() const
{
    return m_propComponent->isLoaded();
}




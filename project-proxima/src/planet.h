#ifndef __PLANET_H__
#define __PLANET_H__

#include "proxima_private.h"
#include "celestial_body.h"
#include "color.h"

class GravityComponent;
class IcosahedronComponent;

class Planet : public CelestialBody
{
    NO_COPY(Planet);

public:
    Planet(const String& name);
    virtual ~Planet();

    virtual void onLoadedCallbackImpl() override;

    void setRadius(float radius);
    void setColor(Color color);
    void generate(bool bDynamic);
    void pull(Planet* planet);
    void pulledBy(Planet* planet);
    bool isLoaded() const;

private:
    Planet();

    GravityComponent* m_gravityComponent = nullptr;
    IcosahedronComponent* m_icosahedronComponent = nullptr;

    float m_radius;
    Color m_color;
};

inline void Planet::setColor(Color color)
{
    m_color = color;
}

#endif // __PLANET_H__

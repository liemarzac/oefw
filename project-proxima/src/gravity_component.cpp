#include "gravity_component.h"

#include "astro_physics.h"

// In messages
#include "gravity_msg.h"

// In/Out messages
#include "kinematic_msg.h"


RTTI_IMPL_PARENT(GravityComponent, Component)


GravityComponent::GravityComponent()
{
}

GravityComponent::~GravityComponent()
{
}

void GravityComponent::update(const GravityComponentUpdateParams& params)
{
    while(Message* message = m_messageQueue.beginMessage())
    {
        uint32 messageTypeHash = message->getTypeHash();
        if(GravityMsg* gravityMsg = cast<GravityMsg>(message))
        {
            if(gravityMsg->m_set.isSet(GravityMsg::Flags::Source))
            {
                m_source.m_id = gravityMsg->m_sender;
                m_source.m_mass = gravityMsg->m_mass;
                m_source.m_setFlags.set(Source::SetFlags::Id);
                m_source.m_setFlags.set(Source::SetFlags::Mass);
            }
            else if(gravityMsg->m_set.isSet(GravityMsg::Flags::Receiver))
            {
                m_receivers.push_back(Receiver());
                Receiver& receiver = m_receivers.back();
                receiver.m_id = gravityMsg->m_sender;
                receiver.m_accelCompReceiver = gravityMsg->m_accelCompReceiver;
                receiver.m_setFlags.set(Receiver::SetFlags::Id);
            }
        }
        else if(KinematicMsg* kinematicMsg = cast<KinematicMsg>(message))
        {
            if(kinematicMsg->m_set.isSet(KinematicMsg::Flags::Position))
            {
                if(kinematicMsg->m_sender == m_source.m_id)
                {
                    m_source.m_position = kinematicMsg->m_position;
                    m_source.m_setFlags.set(Source::SetFlags::Position);
                }
                else
                {
                    for(Receiver& receiver : m_receivers)
                    {
                        if(receiver.m_id == kinematicMsg->m_sender)
                        {
                            receiver.m_position = kinematicMsg->m_position;
                            receiver.m_setFlags.set(Receiver::SetFlags::Position);
                        }
                    }

                }
            }
        }

        m_messageQueue.endMessage();
    }

    if(isInitialized())
    {
        for(Receiver& receiver : m_receivers)
        {
            if(!receiver.m_setFlags.areAllSet((uint)Receiver::SetFlags::NumberOf))
                continue;

            Vec3d receiverToSource = m_source.m_position - receiver.m_position;
            Real squaredDistance = receiverToSource.squaredNorm();

            if(squaredDistance > 0.001)
            {
                Real accelVal = AstroPhysics::G * m_source.m_mass / squaredDistance;
                Vec3d accelVec = receiverToSource.normalized() * accelVal;

                KinematicMsg kinematicMsg(this);
                kinematicMsg.setAcceleration(accelVec);

                if(receiver.m_accelCompReceiver)
                {
                    KinematicMsg* msg = receiver.m_accelCompReceiver->allocMessage<KinematicMsg>(this);
                    *msg = kinematicMsg;
                    receiver.m_accelCompReceiver->commitMessage();
                }
            }
        }
    }
}

bool GravityComponent::isInitialized() const
{
    return m_source.m_setFlags.areAllSet((uint)Source::SetFlags::NumberOf);
}

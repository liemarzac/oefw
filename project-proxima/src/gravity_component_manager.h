#ifndef __GRAVITY_COMPONENT_MANAGER_H__
#define __GRAVITY_COMPONENT_MANAGER_H__

#include "proxima_private.h"

#include "component_manager.h"
#include "gravity_component.h"

DEFINE_COMPONENT_MANAGER(GravityComponent)

#endif // __GRAVITY_COMPONENT_MANAGER_H__

#ifndef __CELESTIAL_BODY_PROPERTIES_MSG_H__
#define __CELESTIAL_BODY_PROPERTIES_MSG_H__

#include "proxima.h"

// OE_Core
#include <eigen_helpers.h>

// OE_Engine
#include "message.h"

class CelestialBodyPropertiesMsg : public OE_Engine::Message
{
    RTTI_DECLARATION
    GET_SIZE_IMPL

public:
    CelestialBodyPropertiesMsg(void* sender);

    Real m_mass;
    Vec3d m_position;
};

#endif // __CELESTIAL_BODY_PROPERTIES_MSG_H__

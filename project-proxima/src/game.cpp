#include "game.h"

#include "proxima_private.h"

// OE_Core
#include <file.h>
#include <oe_math.h>
#include <rand.h>
#include <resource_manager.h>

// OE_Engine
#include <camera.h>
#include <engine.h>
#include <entity_manager.h>
#include <light.h>
#include <composer.h>

// OE_Graphic
#include <prop_resource.h>

// Ta
#include "astro_physics.h"
#include "gravity_component_manager.h"
#include "icosahedron_component_manager.h"
#include "orbital_component_manager.h"
#include "planet.h"
#include "solar_system.h"

#define OE_CHANNEL_GAME "Game"

Game::Game():
    m_earth(nullptr)
    ,m_solarSystem(nullptr)
    ,m_light(nullptr)
{
    OrbitalComponentManager::createInstance();
    GravityComponentManager::createInstance();
    IcosahedronComponentManager::createInstance();

    OE_CHECK_RESULT(appLoadJsonFile("configs" PATH_SEPARATOR "game.json", m_config), true, OE_CHANNEL_GAME);
}

Game::~Game()
{
    if(m_earth)
        delete m_earth;

    if(m_sponza)
        delete m_sponza;

    for(Planet* moon : m_moons)
    {
        delete moon;
    }

    if(m_solarSystem)
        delete m_solarSystem;

    if(m_light)
        delete m_light;

    Engine::getInstance().unloadPackage("sponza");

    IcosahedronComponentManager::destroyInstance();
    GravityComponentManager::destroyInstance();
    OrbitalComponentManager::destroyInstance();
}

SolarSystem* Game::createSolarSystem()
{
    Composer& composer = Composer::getInstance();

    SolarSystem* solarSystem = new SolarSystem();
    //entityManager.addEntity(m_solarSystem);

    Real earthMass = 10.0e11f;
    m_earth = new Planet("earth");
    m_earth->setRadius(2.0f);
    m_earth->setPosition(Vec3(0.0, 0.0f, 0.0f));
    m_earth->setMass(earthMass);
    m_earth->setColor(ColorList::Purple);
    const bool bDynamic = true;
    m_earth->generate(bDynamic);
    solarSystem->addPlanet(m_earth);
    //m_earth->setVelocity(Vec3(1.0f, 0.f, 0.0f));
    //entityManager.addEntity(m_earth);
    //scene.addProp(m_earth);

    //const uint32 nMoons = m_config["num_satellites"].asUInt();
    const uint32 nMoons = 0;

    float x = 1.5f;
    for(uint32 i = 0; i < nMoons; ++i)
    {
        char moonName[512];
        OE_SPRINTF(moonName, 512, "moon_%02d", i);
        Planet* moon = new Planet(moonName);
        moon->setRadius(randr32(0.1f, 0.6f));

        Color color;
        color.a = 0xff;
        color.r = randu32(0, 256);
        color.g = randu32(0, 256);
        color.b = randu32(0, 256);

        float z = randr32(-7.0f, 7.0f);
        moon->setRadius(randr32(0.1f, 0.6f));
        moon->setPosition(Vec3(x, 0, z));
        moon->setMass(10.0e11f);
        moon->setColor(color);
        const bool bDynamic = false;
        moon->generate(bDynamic);
        addSatellite(moon, randr64(0.99, 1.20));
        x += m_config["satellites_spacing"].asFloat();

        m_moons.push_back(moon);
        solarSystem->addPlanet(moon);
    }

    //m_light = new Light();
    //m_light->setDirection(Vec3(0.0f, 1.0f, 0.0f));
    //composer.addLight(m_light);

    Camera& camera = Engine::getInstance().getComposer().getCamera();
    camera.setPosition(Vec3(-91.0f, 94.0f, -2.0f));
    camera.setLookAt(Vec3(0.0f, -1.0f, 1.0f));

    return solarSystem;
}

void Game::addSatellite(Planet* planet, Real circularVelocityRatio)
{
    // Add to entity manager.
    //EntityManager& entityManager = EntityManager::getInstance();
    //entityManager.addEntity(planet);

    // Add to world scene.
    Composer& composer = Composer::getInstance();
    composer.addProp(planet);

    m_solarSystem->connect(m_earth, planet);
    //entityManager.addEntity(m_solarSystem);

    Vec3 position;
    planet->getPosition(position);

    Vec3 earthPosition;
    m_earth->getPosition(earthPosition);

    Vec3 deltaPosition = position - earthPosition;
    Real distance = deltaPosition.norm();
    Real circularOrbitVelocity = AstroPhysics::circularOrbitVelocity(m_earth->getMass(), distance);

    Vec3d r;
    r[0] = (Real)deltaPosition[0];
    r[1] = (Real)deltaPosition[1];
    r[2] = (Real)deltaPosition[2];

    Real inclination = atan(deltaPosition[1] / deltaPosition[0]);
    Vec3d orbitNormal;
    orbitNormal[0] = -sin(inclination);
    orbitNormal[1] = cos(inclination);
    orbitNormal[2] = 0.0;

    Vec3d v = r.cross(orbitNormal);
    v.normalize();
    v *= circularOrbitVelocity * circularVelocityRatio;

    planet->setVelocity(v);
}

void Game::update()
{
    auto& engine = Engine::getInstance();

    switch(m_state)
    {
        case State::LoadingEngine:
        {
            if(engine.isLoaded())
            {
                m_state = State::LoadingGamePackage;
            }
            else
            {
                break;
            }
        }

        case State::LoadingGamePackage:
        {
            m_state = State::CreateGameResources;
            break;
        }

        case State::CreateGameResources:
        {
            m_solarSystem = createSolarSystem();

            m_state = State::LoadingGameResources;
        }

        case State::LoadingGameResources:
        {
            if (m_solarSystem->isLoaded())
            {
                m_solarSystem->addToScene();
                m_state = State::Running;
            }
        }
    }
}

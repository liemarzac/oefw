#include "solar_system.h"

// Engine
#include <composer.h>


#include "planet.h"

SolarSystem::SolarSystem()
{
}

SolarSystem::~SolarSystem()
{
}

void SolarSystem::addPlanet(Planet* planet)
{
    m_planets.push_back(planet);
}

void SolarSystem::connect(Planet* planetA, Planet* planetB)
{
    planetA->pull(planetB);
    planetB->pulledBy(planetA);

    //planetA->pulledBy(planetB);
    //planetB->pull(planetA);
}

bool SolarSystem::isLoaded() const
{
    bool bIsLoaded = true;
    for (auto* planet : m_planets)
    {
        if (!planet->isLoaded())
        {
            bIsLoaded = false;
            break;
        }
    }

    return bIsLoaded;
}

void SolarSystem::addToScene()
{
    Composer& composer = Composer::getInstance();
    for (auto* planet : m_planets)
    {
        composer.addProp(planet);
    }
}

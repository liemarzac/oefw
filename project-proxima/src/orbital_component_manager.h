#ifndef __ORBITAL_COMPONENT_MANAGER_H__
#define __ORBITAL_COMPONENT_MANAGER_H__

#include "proxima_private.h"

#include "component_manager.h"
#include "orbital_component.h"

DEFINE_COMPONENT_MANAGER(OrbitalComponent)

#endif // __ORBITAL_COMPONENT_MANAGER_H__

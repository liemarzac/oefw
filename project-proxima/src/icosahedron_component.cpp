#include "icosahedron_component.h"

#include "proxima_private.h"

// OE_Core
#include <file.h>
#include <oe_math.h>
#include <resource_manager.h>
#include <vector.h>

// OE_Engine
#include <input_manager.h>
#include <engine.h>
#include <composer.h>

// OE_Graphic
#include <material_resource.h>
#include <mesh_resource.h>
#include <prop_resource.h>
#include <vertex_stream_resource.h>


RTTI_IMPL_PARENT(IcosahedronComponent, Component)

#define OE_CHANNEL_ICOSAHEDRON "Icosahedron"

#ifdef _DEBUG
#define SANITY_CHECK sanityCheck();
#else
#define SANITY_CHECK
#endif

IcosahedronComponent::TIndex IcosahedronComponent::m_splitThreshold = 12000;

IcosahedronComponent::IcosahedronComponent():
    m_radius(1.0f)
    ,m_nPoints(0)
    ,m_nTriangles(0)
    ,m_iCurrentTesselationLOD(0)
    ,m_color(ColorList::Yellow)
    ,m_bPop(false)
{
    m_currentPos = Vec3(0.0f, 0.0f, 0.0f);
    m_currentLookAt = Vec3(0.0f, 0.0f, 1.0f);
    m_currentTag = 0;

#if !OE_RELEASE
    debug(true);
#endif

    m_tesselationThread = std::thread(&IcosahedronComponent::tesselate, this);
}

IcosahedronComponent::~IcosahedronComponent()
{
    m_tesselationSync.abort();
    m_tesselationThread.join();
}

void IcosahedronComponent::update(const IcosahedronComponentUpdateParams& params)
{
    if(InputManager::getInstance().hasJustBeenPressed(SDLK_KP_MINUS))
    {
        m_bPop = !m_bPop;
    }

#if !OE_RELEASE
    if(InputManager::getInstance().hasJustBeenPressed(SDLK_f))
    {
        m_bFreeze = !m_bFreeze;
    }
#endif

#if !OE_RELEASE
    if(debug())
    {
        if(InputManager::getInstance().hasJustBeenPressed(SDLK_SPACE))
        {
            m_debugRenderMode = DebugRenderMode(((int(m_debugRenderMode) + 1) % 4));
        }
    }
#endif

#if !OE_RELEASE
    static uint32 nPoints = 0;
#endif

#if !OE_RELEASE
    if(!m_bFreeze)
    {
#endif

    if(m_tesselationSync.isWorkerFinished())
    {
#if !OE_RELEASE
        m_workTime = m_tesselationSync.getWorkTime();
#endif

        if(m_tesselationSync.hasChanged())
        {
            generateMesh(params.m_camLookAt);
        }

        m_currentPos = params.m_camPosition;
        m_currentLookAt = params.m_camLookAt;

#if !OE_RELEASE
        nPoints = m_nPoints;
#endif

        m_tesselationSync.start();
    }

#if !OE_RELEASE
    }
#endif

#if !OE_RELEASE
    if(m_debugRenderMode == DebugRenderMode::General)
    {
        char text[512];
        OE_SPRINTF(text, 512, "Worker Time: %.3f ms", m_workTime * 1000.0);
        auto& composer = Engine::getInstance().getComposer();
        composer.renderText(text, "vera", 12, 10, 10, HorizontalAnchor::Left, VerticalAnchor::Top);

        OE_SPRINTF(text, 512, "Points: %d", nPoints);
        composer.renderText(text, "vera", 12, 10, 25, HorizontalAnchor::Left, VerticalAnchor::Top);
    }
#endif
}

void IcosahedronComponent::tesselate()
{
    while(m_tesselationSync.waitForOwner())
    {
        m_iCurrentTesselationLOD = 0;

        bool bChanged = false;

        // Clean.
        static bool bClean = true;
        if(bClean)
        {
            Vector<TIndex> outdatedNodes;
            outdatedNodes.reserve(m_nodes.size() / 3);
            for(TIndex iNode = 0; iNode < m_nodes.size(); ++iNode)
            {
                if(isNodeUsed(iNode) && !isTagCurrent(iNode))
                {
                    outdatedNodes.push_back(iNode);
                }
            }

            for(TIndex iNode = 0; iNode < outdatedNodes.size(); ++iNode)
            {
                const TIndex iOutdatedNode = outdatedNodes[iNode];
                if(isNodeUsed(iOutdatedNode))
                {
                    bool bIsNeighborCurrent = false;
                    for(uint iNeighbor = 0; iNeighbor < 3; ++iNeighbor)
                    {
                        const TIndex iNeighborNode = m_nodes[iOutdatedNode].m_iNeighbors[iNeighbor];
                        if(isValidIndex(iNeighborNode) && isTagCurrent(iNeighborNode))
                        {
                            bIsNeighborCurrent = true;
                            break;
                        }
                    }

                    if(!bIsNeighborCurrent)
                    {
                        bChanged |= popNode(iOutdatedNode);
                    }
                }
            }
        }

        ++m_currentTag;

        bool bFinished = false;
        while(!bFinished)
        {
            TIndex iNode = getNodeFromPosition(m_currentPos, m_iCurrentTesselationLOD);
            m_pendingProcessNodes.push_back(iNode);

            uint32 nIterations = 0;

            while(m_pendingProcessNodes.size() > 0)
            {
                bChanged |= processPendingNodes(m_currentLookAt);
                ++nIterations;
            }

            if(nIterations > 1)
            {
                // More than one iteration means we had to subdivide new nodes
                // meaning the tessellation has changed.
                bChanged = true;
                ++m_iCurrentTesselationLOD;
            }
            else
            {
                bFinished = true;
            }
        }

        if(bChanged)
        {
            m_tesselationSync.setChanged();
        }

        m_tesselationSync.workFinished();
    }
}

bool IcosahedronComponent::processPendingNodes(const Vec3& lookAt)
{
    const TIndex iNode = m_pendingProcessNodes.back();

    m_pendingProcessNodes.pop_back();

    const uint32 visibleSurface = calcNormalDotLookAt(iNode, lookAt, false) < 0 ? uint32(calcVisibleSurface(iNode)) : 0;

    uint32 lod = getLODFromVisibleSurface(visibleSurface);

    bool bChanged = false;
    if(lod > 0)
    {
        SubdivisionParams params;
        params.bInitial = true;
        bChanged = subdivideNode(iNode, params);

        for(uint32 iNeighbor = 0; iNeighbor < 3; ++iNeighbor)
        {
            const TIndex iNeighborNode = m_nodes[iNode].m_iNeighbors[iNeighbor];
            if(isValidIndex(iNeighborNode) && !isTagCurrent(iNeighborNode))
            {
                m_pendingProcessNodes.push_back(iNeighborNode);
            }
        }
    }

    tag(iNode);
    return bChanged;
}

uint32 IcosahedronComponent::getLODFromVisibleSurface(uint32 visibleSurface)
{
    uint32 maxLOD = 0;

    auto& config = IcosahedronComponentConfig::getConstInstance();

    for(uint iTesseletionEntry = 0; iTesseletionEntry < config.m_tesselation.size(); ++iTesseletionEntry)
    {
        if(visibleSurface < config.m_tesselation[iTesseletionEntry].m_surface)
        {
            return config.m_tesselation[iTesseletionEntry].m_lod;
        }
    }

    return config.m_tesselation.back().m_lod;
}

IcosahedronComponent::TIndex IcosahedronComponent::getNodeFromPosition(const Vec3& pos, int32 iMaxLOD) const
{
    for(TIndex iNode = 0; iNode < 20; ++iNode)
    {
        if(isInNode(pos, iNode))
        {
            return getNodeFromPosition(pos, iNode, iMaxLOD);
        }
    }

    return -1;
}

IcosahedronComponent::TIndex IcosahedronComponent::getNodeFromPosition(const Vec3& pos, TIndex iNode, int32 iMaxLOD) const
{
    if(m_nodes[iNode].m_level < iMaxLOD)
    {
        if(hasChildren(iNode))
        {
            for(TIndex iChild = 0; iChild < 4; ++iChild)
            {
                TIndex iChildNode = m_nodes[iNode].m_iChildren[iChild];

                if(isValidIndex(iChildNode))
                {
                    if(isInNode(pos, iChildNode))
                    {
                        return getNodeFromPosition(pos, iChildNode, iMaxLOD);
                    }
                }
            }
        }
    }

    return iNode;
}

bool IcosahedronComponent::isInNode(const Vec3& pos, TIndex iNode) const
{
    const Node& node = m_nodes[iNode];
    const Triangle& triangle = m_triangles[iNode];

    Vec3 a, b, c;
    float3ToVec3(m_points[triangle.m_indices[0]].m_pos, a);
    float3ToVec3(m_points[triangle.m_indices[1]].m_pos, b);
    float3ToVec3(m_points[triangle.m_indices[2]].m_pos, c);

    Vec3 nab, nbc, nca;
    nab = a.cross(b);
    nbc = b.cross(c);
    nca = c.cross(a);

    if(pos.dot(nab) < 0)
        return false;

    if(pos.dot(nbc) < 0)
        return false;

    if(pos.dot(nca) < 0)
        return false;

    return true;
}

PropResPtr IcosahedronComponent::generate(const GenerationParams& params)
{
    m_radius = params.m_radius;
    m_color = params.m_color;

    const double Theta = 26.56505117707799 * PI / 180.0;
    const double CTheta = cos(Theta);
    const double STheta = sin(Theta);

    {
        TIndex iTopPoint;
        acquirePoints(1, &iTopPoint);
        m_points[iTopPoint].m_pos[0] = 0.0f;
        m_points[iTopPoint].m_pos[1] = m_radius, 0.0f;
        m_points[iTopPoint].m_pos[2] = 0.0f;
    }

    {
        // Make upper pentagon.
        TIndex iPoints[5];
        acquirePoints(5, iPoints);

        for(int i = 0; i < 5; ++i)
        {
            double phi = -i * (2 * PI / 5);
            m_points[iPoints[i]].m_pos[0] = (float)(CTheta * sin(phi)) * m_radius;
            m_points[iPoints[i]].m_pos[1] = (float)STheta * m_radius;
            m_points[iPoints[i]].m_pos[2] = (float)(-CTheta * cos(phi)) * m_radius;
        }
    }

    {
        TIndex iPoints[5];
        acquirePoints(5, iPoints);

        // Make lower pentagon.
        for(int i = 0; i < 5; ++i)
        {
            double phi = (-i * (2 * PI / 5)) + (PI / 5);
            m_points[iPoints[i]].m_pos[0] = (float)(CTheta * sin(phi)) * m_radius;
            m_points[iPoints[i]].m_pos[1] = (float)-STheta * m_radius;
            m_points[iPoints[i]].m_pos[2] = (float)(-CTheta * cos(phi)) * m_radius;
        }
    }

    {
        TIndex iBottomPoint;
        acquirePoints(1, &iBottomPoint);
        m_points[iBottomPoint].m_pos[0] = 0.0f;
        m_points[iBottomPoint].m_pos[1] = -m_radius;
        m_points[iBottomPoint].m_pos[2] = 0.0f;
    }

    static const TIndex InitialIndices[20][3] =
    {
        {0, 1, 2}, {0, 2, 3}, {0, 3, 4},
        {0, 4, 5}, {0, 5, 1}, {1, 6, 7},
        {1, 7, 2}, {2, 7, 8}, {2, 8, 3},
        {3, 8, 9}, {3, 9, 4}, {4, 9, 10},
        {4, 10, 5}, {5, 10, 6}, {5, 6, 1},
        {11, 10, 9}, {11, 9, 8}, {11, 8, 7},
        {11, 7, 6}, {11, 6, 10}
    };

    for(uint iIndicesSource = 0; iIndicesSource < 20; ++iIndicesSource)
    {
        m_triangles.push_back(Triangle());
        Triangle& t = m_triangles.back();
        memcpy(&t.m_indices, InitialIndices[iIndicesSource], 3 * sizeof(TIndex));

        m_nodes.push_back(Node());
        Node& node = m_nodes.back();
        // Root nodes will never have a parent.
        node.m_iParent = -1;
        node.m_level = 0;
        node.m_flags.set(Node::Flags::Used);
        memset(&node.m_iChildren, 0, sizeof(node.m_iChildren));

        m_nodeTags.push_back(-1);

        // Find the 3 neighbors of the triangle.
        for(uint iEdgeSource = 0; iEdgeSource < 3; ++iEdgeSource)
        {
            // To find the neighbor, iterate, the list of indices and find the matching edge.
            TIndex edgeP1Source;
            TIndex edgeP2Source;

            if(iEdgeSource < 2)
            {
                edgeP1Source = InitialIndices[iIndicesSource][iEdgeSource];
                edgeP2Source = InitialIndices[iIndicesSource][iEdgeSource + 1];
            }
            else
            {
                edgeP1Source = InitialIndices[iIndicesSource][2];
                edgeP2Source = InitialIndices[iIndicesSource][0];
            }

            bool bNeighborFound = false;
            TIndex iNeighbor = 0;

            for(uint iIndicesDest = 0; iIndicesDest < 20; ++iIndicesDest)
            {
                // The neighbor cannot be the same triangle currently being processed.
                if(iIndicesDest == iIndicesSource)
                    continue;

                bool bEdgeFound = false;

                for(uint iEdgeDest = 0; iEdgeDest < 3; ++iEdgeDest)
                {
                    TIndex edgeP1Dest;
                    TIndex edgeP2Dest;

                    if(iEdgeDest < 2)
                    {
                        edgeP1Dest = InitialIndices[iIndicesDest][iEdgeDest];
                        edgeP2Dest = InitialIndices[iIndicesDest][iEdgeDest + 1];
                    }
                    else
                    {
                        edgeP1Dest = InitialIndices[iIndicesDest][2];
                        edgeP2Dest = InitialIndices[iIndicesDest][0];
                    }

                    // Edge [point1;point2] seen by the source edge,
                    // will be [point2;point1] seen by the dest edge.
                    if(edgeP1Source == edgeP2Dest && edgeP2Source == edgeP1Dest)
                    {
                        bEdgeFound = true;
                        break;
                    }
                }

                if(bEdgeFound)
                {
                    bNeighborFound = true;
                    iNeighbor = iIndicesDest;
                    break;
                }
            }

            OE_CHECK(bNeighborFound, OE_CHANNEL_ICOSAHEDRON);
            node.m_iNeighbors[iEdgeSource] = iNeighbor;
        }
    }

    m_nTriangles = 20;

    Vec3 camLookAt(1.0f, 0.0f, 0.0f);
    return generateMesh(camLookAt);
}

void IcosahedronComponent::subdivideNode(TIndex iNode)
{
    // Make sure the node is a leaf.
    OE_CHECK(!m_nodes[iNode].m_flags.isSet(Node::Flags::HasChildren), OE_CHANNEL_ICOSAHEDRON);

    TIndex iOriginIndices[3];
    memcpy(iOriginIndices, m_triangles[iNode].m_indices, sizeof(iOriginIndices));

    // Add a new point in the middle of each edge
    // and subdivide the neighbor.
    TIndex iSubIndices[3];
    {
        const Triangle& triangle = m_triangles[iNode];

        acquirePoints(3, iSubIndices);

        for(uint iEdge = 0; iEdge < 3; ++iEdge)
        {
            TIndex i1 = iEdge;
            TIndex i2 = iEdge < 2 ? iEdge + 1 : 0;

            setMidPointPosition(triangle.m_indices[i1], triangle.m_indices[i2], iSubIndices[iEdge]);
        }
    }

    TIndex iSubNodes[4];
    memset(iSubNodes, -1, sizeof(iSubNodes));
    makeSubNodes(iNode, iOriginIndices, iSubIndices, iSubNodes);
    setChildren(iNode, iSubNodes);

    propagateAndConnect(iNode, iOriginIndices, iSubIndices, iSubNodes);

    // 4 triangles are created but the parent will not be part of the mesh anymore.
    m_nTriangles += 3;
}

bool IcosahedronComponent::subdivideNode(TIndex iNode, SubdivisionParams& params)
{
    bool bChanged = false;
    if(!isTransitioning(iNode))
    {
        // The node is not in transition.
        if(!hasChildren(iNode))
        {
            // The node can start transition because it does not have children yet.
            if(params.bInitial)
            {
                subdivideNode(iNode);
            }
            else
            {
                subdivideNodeToTransition(iNode, params);
            }
            bChanged = true;
        }
        else
        {
            if(!params.bInitial)
            {
                // The node is already fully subdivided.
                // The only thing to do is to return the neighbors.
                connectChildrenNeighbors(iNode, params);
            }
        }
    }
    else
    {
        // The node is already partially subdivided.
        subdivideNodeInTransition(iNode, params);
        bChanged = true;
    }

    return bChanged;
}

void IcosahedronComponent::subdivideNodeToTransition(TIndex iNode, SubdivisionParams& params)
{
    bool bFound = false;

    uint iFoundEdge = 0;
    bool bRes = findEdgeForNeighbor(iNode, params.iSrcNode, iFoundEdge);
    OE_CHECK(bRes, OE_CHANNEL_ICOSAHEDRON);

    const TIndex iOppositePoint = m_triangles[iNode].m_indices[modIndex(iFoundEdge + 2)];

    TIndex iNewNodes[2];
    acquireNodes(2, iNewNodes);

    {
        Node& node1 = m_nodes[iNewNodes[0]];
        Triangle& triangle1 = m_triangles[iNewNodes[0]];

        OE_CHECK(
            m_triangles[iNode].m_indices[iFoundEdge] == params.iEdgePoints[2],
            OE_CHANNEL_ICOSAHEDRON);

        triangle1.m_indices[0] = params.iEdgePoints[2];
        triangle1.m_indices[1] = params.iEdgePoints[1];
        triangle1.m_indices[2] = iOppositePoint;
        params.iDstNeighborNodes[1] = -1; // We don't connect transition nodes.
        node1.m_iParent = iNode;
        node1.m_level = m_nodes[iNode].m_level + 1;
    }

    {
        Node& node2 = m_nodes[iNewNodes[1]];
        Triangle& triangle2 = m_triangles[iNewNodes[1]];

        OE_CHECK(
            m_triangles[iNode].m_indices[iFoundEdge < 2 ? iFoundEdge + 1 : 0] == params.iEdgePoints[0],
            OE_CHANNEL_ICOSAHEDRON);

        triangle2.m_indices[0] = params.iEdgePoints[1];
        triangle2.m_indices[1] = params.iEdgePoints[0];
        triangle2.m_indices[2] = iOppositePoint;
        params.iDstNeighborNodes[0] = -1;  // We don't connect transition nodes.
        node2.m_iParent = iNode;
        node2.m_level = m_nodes[iNode].m_level + 1;
    }

    {
        Node& parentNode = m_nodes[iNode];
        parentNode.m_iChildren[0] = iNewNodes[0];
        parentNode.m_iChildren[1] = iNewNodes[1];
        parentNode.m_flags.set(Node::Flags::HasChildren);
        parentNode.m_flags.set(Node::Flags::Transitioning);
    }

    // 4 triangles are created but the parent will not be part of the mesh anymore.
    m_nTriangles += 1;
}

void IcosahedronComponent::subdivideNodeInTransition(TIndex iNode, SubdivisionParams& params)
{
    TIndex iSubIndices[3];
    memset(iSubIndices, -1, sizeof(iSubIndices));

    // Check if we already share sub-points with neighbor.
    // If it is the case, we don't need to recreate them.
    for(uint32 iEdge = 0; iEdge < 3; ++iEdge)
    {
        TIndex iNeighborNode = m_nodes[iNode].m_iNeighbors[iEdge];

        if(isValidIndex(iNeighborNode))
        {
            if(isFullySubdivided(iNeighborNode))
            {
                // The following code is from the point of view of the neighbor node.
                uint32 iIncomingEdge;
                bool bRes = findEdgeForNeighbor(iNeighborNode, iNode, iIncomingEdge);
                OE_CHECK(bRes, OE_CHANNEL_ICOSAHEDRON);

                iSubIndices[iEdge] = getSubpointForEdge(iNeighborNode, iIncomingEdge);
            }
        }
    }

    // Create missing points.
    for(uint iEdge = 0; iEdge < 3; ++iEdge)
    {
        if(iSubIndices[iEdge] == TIndex(-1))
        {
            acquirePoints(1, &iSubIndices[iEdge]);

            setMidPointPosition(
                m_triangles[iNode].m_indices[iEdge],
                m_triangles[iNode].m_indices[iEdge < 2 ? iEdge + 1 : 0],
                iSubIndices[iEdge]);
        }
    }

    // Subdivide, but re-use existing 2 children.
    TIndex iSubNodes[4];
    iSubNodes[0] = m_nodes[iNode].m_iChildren[0];
    iSubNodes[1] = m_nodes[iNode].m_iChildren[1];
    iSubNodes[2] = -1;
    iSubNodes[3] = -1;

    TIndex iOriginIndices[3];
    memcpy(iOriginIndices, m_triangles[iNode].m_indices, sizeof(iOriginIndices));
    makeSubNodes(iNode, iOriginIndices, iSubIndices, iSubNodes);
    setChildren(iNode, iSubNodes);

    if(!params.bInitial)
    {
        connectChildrenNeighbors(iNode, params);
    }

    m_nodes[iNode].m_flags.remove(Node::Flags::Transitioning);

    // 2 triangles were released and 4 were created.
    m_nTriangles += 2;

    propagateAndConnect(
        iNode,
        iOriginIndices,
        iSubIndices,
        iSubNodes);
}

void IcosahedronComponent::makeSubNodes(TIndex iParentNode, const TIndex iOriginIndices[3], const TIndex iNewIndices[3], TIndex iNewNodes[4])
{
    // Acquire nodes where entry is -1
    for(uint32 iSubNode = 0; iSubNode < 4; ++iSubNode)
    {
        if(iNewNodes[iSubNode] == TIndex(-1))
        {
            acquireNodes(1, &iNewNodes[iSubNode]);
        }
    }

    const TLevel subLevel = m_nodes[iParentNode].m_level + 1;

    Node& node1 = m_nodes[iNewNodes[0]];
    node1.m_iParent = iParentNode;
    node1.m_level = subLevel;
    Triangle& triangle1 = m_triangles[iNewNodes[0]];
    triangle1.m_indices[0] = iNewIndices[0];
    triangle1.m_indices[1] = iNewIndices[2];
    triangle1.m_indices[2] = iOriginIndices[0];

    Node& node2 = m_nodes[iNewNodes[1]];
    node2.m_iParent = iParentNode;
    node2.m_level = subLevel;
    Triangle& triangle2 = m_triangles[iNewNodes[1]];
    triangle2.m_indices[0] = iNewIndices[1];
    triangle2.m_indices[1] = iNewIndices[0];
    triangle2.m_indices[2] = iOriginIndices[1];

    Node& node3 = m_nodes[iNewNodes[2]];
    node3.m_iParent = iParentNode;
    node3.m_level = subLevel;
    Triangle& triangle3 = m_triangles[iNewNodes[2]];
    triangle3.m_indices[0] = iNewIndices[2];
    triangle3.m_indices[1] = iNewIndices[1];
    triangle3.m_indices[2] = iOriginIndices[2];

    Node& node4 = m_nodes[iNewNodes[3]];
    node4.m_iParent = iParentNode;
    node4.m_level = subLevel;
    Triangle& triangle4 = m_triangles[iNewNodes[3]];
    triangle4.m_indices[0] = iNewIndices[0];
    triangle4.m_indices[1] = iNewIndices[1];
    triangle4.m_indices[2] = iNewIndices[2];

    // Connect child nodes between them.
    const TIndex iMiddleNode = iNewNodes[3];
    m_nodes[iMiddleNode].m_iNeighbors[0] = iNewNodes[1];
    m_nodes[iMiddleNode].m_iNeighbors[1] = iNewNodes[2];
    m_nodes[iMiddleNode].m_iNeighbors[2] = iNewNodes[0];

    for(int iChild = 0; iChild < 3; ++iChild)
    {
        m_nodes[iNewNodes[iChild]].m_iNeighbors[0] = iNewNodes[3];
    }
}

void IcosahedronComponent::connectChildrenNeighbors(TIndex iNode, SubdivisionParams& params)
{
    uint32 iEdge;
    bool bRes = findEdgeForNeighbor(iNode, params.iSrcNode, iEdge);
    OE_CHECK(bRes, OE_CHANNEL_ICOSAHEDRON);

    params.iDstNeighborNodes[0] = m_nodes[iNode].m_iChildren[modIndex(iEdge + 1)];
    params.iDstNeighborNodes[1] = m_nodes[iNode].m_iChildren[iEdge];

    const TIndex iChild1 = m_nodes[iNode].m_iChildren[modIndex(iEdge + 1)];
    m_nodes[iChild1].m_iNeighbors[1] = params.iSrcTriangleNeighbors[0];

    const TIndex iChild2 = m_nodes[iNode].m_iChildren[iEdge];
    m_nodes[iChild2].m_iNeighbors[2] = params.iSrcTriangleNeighbors[1];
}

void IcosahedronComponent::setMidPointPosition(TIndex iP1, TIndex iP2, TIndex iPMid)
{
    // v1 = original point1, v2 = original point2, vmid = middle point.
    Vec3 v1, v2, vmid;
    float3ToVec3(m_points[iP1].m_pos, v1);
    float3ToVec3(m_points[iP2].m_pos, v2);
    vmid = (v1 + v2) / 2.0f;
    vmid.normalize();
    vmid *= m_radius;
    vec3ToFloat3(vmid, m_points[iPMid].m_pos);
}

void IcosahedronComponent::setChildren(TIndex iNode, const TIndex iSubNodes[4])
{
    Node& parentNode = m_nodes[iNode];
    parentNode.m_iChildren[0] = iSubNodes[0];
    parentNode.m_iChildren[1] = iSubNodes[1];
    parentNode.m_iChildren[2] = iSubNodes[2];
    parentNode.m_iChildren[3] = iSubNodes[3];
    parentNode.m_flags.set(Node::Flags::HasChildren);
}

void IcosahedronComponent::propagateAndConnect(
    TIndex iNode,
    const TIndex iOriginIndices[3],
    const TIndex iSubIndices[3],
    const TIndex iSubNodes[4])
{
    const TIndex MapEdgeToNodes[3][2] =
    {
        {0, 1}, {1, 2}, {2, 0}
    };

    SubdivisionParams subdivisionParams;
    {
        TIndex iSharedParentNeighbors[3];
        findParentNeighborsSharingChildEdge(iNode, iSharedParentNeighbors);

        for(uint iEdge = 0; iEdge < 3; ++iEdge)
        {
            const TIndex iSharedParentNeightbor = iSharedParentNeighbors[iEdge];
            if(isValidIndex(iSharedParentNeightbor) && isTransitioning(iSharedParentNeightbor))
            {
                SubdivisionParams parentSubdivideParams;
                parentSubdivideParams.bInitial = true;
                subdivideNode(iSharedParentNeightbor, parentSubdivideParams);
            }

            subdivisionParams.iSrcNode = iNode;
            subdivisionParams.iEdgePoints[0] = iOriginIndices[iEdge];
            subdivisionParams.iEdgePoints[1] = iSubIndices[iEdge];
            subdivisionParams.iEdgePoints[2] = iOriginIndices[iEdge < 2 ? iEdge + 1 : 0];
            subdivisionParams.iSrcTriangleNeighbors[0] = iSubNodes[MapEdgeToNodes[iEdge][0]];
            subdivisionParams.iSrcTriangleNeighbors[1] = iSubNodes[MapEdgeToNodes[iEdge][1]];
            subdivideNode(m_nodes[iNode].m_iNeighbors[iEdge], subdivisionParams);

            // Connect with newly created neighbors.
            const TIndex iChild1 = m_nodes[iNode].m_iChildren[iEdge];
            m_nodes[iChild1].m_iNeighbors[2] = subdivisionParams.iDstNeighborNodes[0];

            const TIndex iChild2 = m_nodes[iNode].m_iChildren[modIndex(iEdge + 1)];
            m_nodes[iChild2].m_iNeighbors[1] = subdivisionParams.iDstNeighborNodes[1];
        }
    }
}

bool IcosahedronComponent::popNode(TIndex iNode)
{
    Vector<TIndex> iPointsToRelease;
    iPointsToRelease.reserve(20);

    bool bChanged = popNode(iNode, iPointsToRelease);

    if(iPointsToRelease.size() > 0)
    {
        releasePoints(&iPointsToRelease[0], uint32(iPointsToRelease.size()));
    }

    return bChanged;
}

bool IcosahedronComponent::popNode(TIndex iNode, Vector<TIndex>& iPointsToRelease)
{
    bool bChanged = false;

    if(hasChildren(iNode))
    {
        if(hasGrandChildren(iNode))
        {
            for(uint iChildren = 0; iChildren < 4; ++iChildren)
            {
                bChanged |= popNode(m_nodes[iNode].m_iChildren[iChildren]);
            }
        }

        OE_CHECK(!hasGrandChildren(iNode), OE_CHANNEL_ICOSAHEDRON);

        m_nodes[iNode].m_flags.remove(Node::Flags::HasChildren);

        if(isTransitioning(iNode))
        {
            // Pop opposite node.
            const TIndex iOppositeNeighborNode = findOppositveNodeOfTransitioningNode(iNode);

            m_nodes[iNode].m_flags.remove(Node::Flags::Transitioning);

            uint32 iOppositeNeighborEdge;
            bool bRes = findEdgeForNeighbor(iNode, iOppositeNeighborNode, iOppositeNeighborEdge);
            OE_CHECK(bRes, OE_CHANNEL_ICOSAHEDRON);

            popNodeToTransition(
                iOppositeNeighborNode,
                iNode,
                iPointsToRelease);

            // Release the 2 children.
            releaseNodes(m_nodes[iNode].m_iChildren, 2);
            m_nTriangles -= 1;

            TIndex iPointToRelease = m_triangles[m_nodes[iNode].m_iChildren[0]].m_indices[1];
            addUnique(iPointsToRelease, iPointToRelease);
        }
        else
        {
            // Pop all surrounding nodes.
            for(uint iEdge = 0; iEdge < 3; ++iEdge)
            {
                popNodeToTransition(
                    m_nodes[iNode].m_iNeighbors[iEdge],
                    iNode,
                    iPointsToRelease);
            }

            // Release the 4 children.
            releaseNodes(m_nodes[iNode].m_iChildren, 4);
            m_nTriangles -= 3;

            // Release the 3 sub-points.
            for(uint iChild = 0; iChild < 3; ++iChild)
            {
                addUnique(iPointsToRelease, m_triangles[m_nodes[iNode].m_iChildren[iChild]].m_indices[0]);
            }
        }

        bChanged = true;
    }

    return bChanged;
}

IcosahedronComponent::TIndex IcosahedronComponent::findCounterClockWiseNeighborNode(TIndex iNode, TIndex iPoint) const
{
    uint32 iFoundEdge = UINT32_MAX;

    for(uint iEdge = 0; iEdge < 3; ++iEdge)
    {
        if(m_triangles[iNode].m_indices[iEdge] == iPoint)
        {
            iFoundEdge = iEdge;
            break;
        }
    }

    OE_CHECK(iFoundEdge != UINT32_MAX, OE_CHANNEL_ICOSAHEDRON);

    return m_nodes[iNode].m_iNeighbors[iFoundEdge];
}

void IcosahedronComponent::findNodeAroundFanPoint(TIndex iInitialNode, TIndex iFanPoint, Vector<TIndex>& iNodes) const
{
    //findNodesAroundFanPoint(iInitialNode, iInitialNode, iFanPoint, iNodes, FanDirection::Clockwise);
    findNodesAroundFanPoint(iInitialNode, iInitialNode, iFanPoint, iNodes, FanDirection::AntiClockwise);
}

void IcosahedronComponent::findNodesAroundFanPoint(TIndex iInitialNode, TIndex iCurrentNode, TIndex iFanPoint, Vector<TIndex>& iNodes, FanDirection direction) const
{
    uint32 iEdgeOfFanPoint = UINT32_MAX;

    for(uint iEdge = 0; iEdge < 3; ++iEdge)
    {
        if(m_triangles[iCurrentNode].m_indices[iEdge] == iFanPoint)
        {
            iEdgeOfFanPoint = direction == FanDirection::Clockwise ? (iEdge + 2) % 3 : iEdge;
            break;
        }
    }

    OE_CHECK(iEdgeOfFanPoint != UINT32_MAX, OE_CHANNEL_ICOSAHEDRON);

    const TIndex iNeighborNode = m_nodes[iCurrentNode].m_iNeighbors[iEdgeOfFanPoint];

    if(!isValidIndex(iNeighborNode))
        return;

    if(iNeighborNode == iInitialNode)
        return;

    bool bStop = false;
    bool bAdd = true;

    if(!m_nodes[iNeighborNode].m_flags.isSet(Node::Flags::HasChildren))
    {
        bStop = true;
        bAdd = false;
    }
    else if(m_nodes[iNeighborNode].m_flags.isSet(Node::Flags::Transitioning))
    {
        bStop = true;

        if(findOppositveNodeOfTransitioningNode(iNeighborNode) != iCurrentNode)
        {
            bAdd = false;
        }
    }

    if(bAdd)
    {
        if(!addUnique(iNodes, iNeighborNode))
        {
            bStop = true;
        }
    }

    if(!bStop)
    {
        //findNodesAroundFanPoint(iInitialNode, iNeighborNode, iFanPoint, iNodes, direction);
    }
}

IcosahedronComponent::TIndex IcosahedronComponent::findOppositveNodeOfTransitioningNode(TIndex iTransitioningNode) const
{
    OE_CHECK(m_nodes[iTransitioningNode].m_flags.isSet(Node::Flags::Transitioning), OE_CHANNEL_ICOSAHEDRON);

    const TIndex iFirstChildNode = m_nodes[iTransitioningNode].m_iChildren[0];
    const TIndex iFirstPointFirstChild = m_triangles[iFirstChildNode].m_indices[0];

    uint32 iFoundEdge = UINT32_MAX;

    // Find edge.
    for(uint iEdge = 0; iEdge < 3; ++iEdge)
    {
        if(m_triangles[iTransitioningNode].m_indices[iEdge] == iFirstPointFirstChild)
        {
            iFoundEdge = iEdge;
            break;
        }
    }

    OE_CHECK(iFoundEdge != UINT32_MAX, OE_CHANNEL_ICOSAHEDRON);

    return m_nodes[iTransitioningNode].m_iNeighbors[iFoundEdge];
}

IcosahedronComponent::TIndex IcosahedronComponent::getSubpointForEdge(TIndex iNode, uint32 iEdge) const
{
    OE_CHECK(isFullySubdivided(iNode), OE_CHANNEL_ICOSAHEDRON);
    TIndex iChild = m_nodes[iNode].m_iChildren[iEdge];
    return m_triangles[iChild].m_indices[0];
}


void IcosahedronComponent::popNodeToTransition(
    TIndex iNode,
    TIndex iIncomingNode,
    Vector<TIndex>& iPointsToTelease)
{
    if(hasGrandChildren(iNode))
    {
        for(uint iChild = 0; iChild < 4; ++iChild)
        {
            popNode(m_nodes[iNode].m_iChildren[iChild]);
        }
    }

    if(hasChildren(iNode))
    {
        if(isTransitioning(iNode))
        {
            // Remove the transition if it was transitioning
            const TIndex iOppositeNode = findOppositveNodeOfTransitioningNode(iNode);

            if(iOppositeNode == iIncomingNode)
            {
                releaseNodes(m_nodes[iNode].m_iChildren, 2);
                m_nTriangles -= 1; // The parent being now used instead of the 2 children.
                m_nodes[iNode].m_flags.remove(Node::Flags::Transitioning);
                m_nodes[iNode].m_flags.remove(Node::Flags::HasChildren);

                TIndex iPointToRelease = m_triangles[m_nodes[iNode].m_iChildren[0]].m_indices[1];
                addUnique(iPointsToTelease, iPointToRelease);
            }
        }
        else
        {
            // Calculate incoming edge and edge offsets.
            uint iIncomingEdge = 0;
            bool bResFindEdge = findEdgeForNeighbor(iNode, iIncomingNode, iIncomingEdge);
            OE_CHECK(bResFindEdge, OE_CHANNEL_ICOSAHEDRON);
            const uint i1Mod3 = modIndex(iIncomingEdge + 1);
            const uint i2Mod3 = modIndex(iIncomingEdge + 2);

            if(isTransitioning(m_nodes[iNode].m_iNeighbors[i2Mod3]))
            {
                // Don't become a transition node for another transition node.
                // Pop completely instead.
                popNode(iNode, iPointsToTelease);
            }
            else
            {
                //
                // Become a transitioning node.

                // Remove neighbor connection of the 2 children being released.
                for(uint i = 0; i < 2; ++i)
                {
                    const uint iChild = i == 0 ? iIncomingEdge : i2Mod3;

                    TIndex iChildNode = m_nodes[iNode].m_iChildren[iChild];
                    TIndex iChildNeighbor = m_nodes[iChildNode].m_iNeighbors[i + 1];

if(isValidIndex(iChildNeighbor))
{
    uint iReciprocalNeighborEdge = 0;
    bool bResReciprocalFindEdge = findEdgeForNeighbor(iChildNeighbor, iChildNode, iReciprocalNeighborEdge);
    OE_CHECK(m_nodes[iChildNeighbor].m_iNeighbors[iReciprocalNeighborEdge] == iChildNode, OE_CHANNEL_ICOSAHEDRON);
    m_nodes[iChildNeighbor].m_iNeighbors[iReciprocalNeighborEdge] = -1;
}
                }

                addUnique(iPointsToTelease, m_triangles[m_nodes[iNode].m_iChildren[iIncomingEdge]].m_indices[0]);
                addUnique(iPointsToTelease, m_triangles[m_nodes[iNode].m_iChildren[i1Mod3]].m_indices[0]);

                // Update triangle for the 2 first children
                TIndex iChildPoints[2][3];
                iChildPoints[0][0] = m_triangles[iNode].m_indices[i2Mod3];
                iChildPoints[0][1] = m_triangles[m_nodes[iNode].m_iChildren[i2Mod3]].m_indices[0];
                iChildPoints[0][2] = m_triangles[iNode].m_indices[i1Mod3];

                iChildPoints[1][0] = m_triangles[m_nodes[iNode].m_iChildren[i2Mod3]].m_indices[0];
                iChildPoints[1][1] = m_triangles[iNode].m_indices[iIncomingEdge];
                iChildPoints[1][2] = m_triangles[iNode].m_indices[i1Mod3];

                const TIndex iChildNode1 = m_nodes[iNode].m_iChildren[0];
                memcpy(m_triangles[iChildNode1].m_indices, iChildPoints[0], 3 * sizeof(TIndex));

                const TIndex iChildNode2 = m_nodes[iNode].m_iChildren[1];
                memcpy(m_triangles[iChildNode2].m_indices, iChildPoints[1], 3 * sizeof(TIndex));

                // Release the 2 last children.
                TIndex iChildrenToRelease[2];
                iChildrenToRelease[0] = m_nodes[iNode].m_iChildren[2];
                iChildrenToRelease[1] = m_nodes[iNode].m_iChildren[3];
                m_nodes[iNode].m_iChildren[2] = -1;
                m_nodes[iNode].m_iChildren[3] = -1;
                releaseNodes(iChildrenToRelease, 2);
                m_nTriangles -= 2;

                m_nodes[iNode].m_flags.set(Node::Flags::Transitioning);

                // Propagate
                TIndex iNeighborNodeToPop = m_nodes[iNode].m_iNeighbors[i1Mod3];
                popNodeToTransition(iNeighborNodeToPop, iNode, iPointsToTelease);
            }
        }
    }
}

bool IcosahedronComponent::hasChildren(TIndex iNode) const
{
    return m_nodes[iNode].m_flags.isSet(Node::Flags::HasChildren);
}

bool IcosahedronComponent::isTransitioning(TIndex iNode) const
{
    return m_nodes[iNode].m_flags.isSet(Node::Flags::Transitioning);
}

bool IcosahedronComponent::hasGrandChildren(TIndex iNode) const
{
    if(!hasChildren(iNode))
        return false;

    // Transitioning node cannot have grand-children.
    if(isTransitioning(iNode))
        return false;

    for(uint iChild = 0; iChild < 4; ++iChild)
    {
        const TIndex iChildNode = m_nodes[iNode].m_iChildren[iChild];
        if(isValidIndex(iChildNode))
        {
            if(hasChildren(iChildNode))
            {
                return true;
            }
        }
    }

    return false;
}

PropResPtr IcosahedronComponent::generateMesh(const Vec3& camLookAt)
{
    using VertexFormat = Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent;

    UniqueBufferPtr verticesBuffer = MakeUnique<Buffer>(
        m_points.size() * sizeof(VertexFormat));

    for(uint16 i = 0; i < m_points.size(); ++i)
    {
        VertexFormat vertex;

        // Make position.
        vertex.m_position[0] = m_points[i].m_pos[0];
        vertex.m_position[1] = m_points[i].m_pos[1];
        vertex.m_position[2] = m_points[i].m_pos[2];

        // Make UV in relation to latitude and longitude.
        double longitude = asin(m_points[i].m_pos[2] / m_radius) + PI_OVER_TWO;
        if(m_points[i].m_pos[0] < 0)
        {
            longitude = -longitude;
        }
        double latitude = asin(m_points[i].m_pos[0] / m_radius);
        vertex.m_uv[0] = static_cast<float>((longitude + PI) / TWO_PI);
        vertex.m_uv[1] = static_cast<float>((latitude + PI_OVER_TWO) / PI);
        OE_CHECK(vertex.m_uv[0] >= 0.0f && vertex.m_uv[0] <= 1.0f, OE_CHANNEL_ICOSAHEDRON);
        OE_CHECK(vertex.m_uv[1] >= 0.0f && vertex.m_uv[1] <= 1.0f, OE_CHANNEL_ICOSAHEDRON);

        // Make color.
        vertex.m_color[0] = 255.0f / m_color.r;
        vertex.m_color[1] = 255.0f / m_color.g;
        vertex.m_color[2] = 255.0f / m_color.b;

        // Make normal. position = direction from center = normal
        Vec3 normal;
        normal[0] = vertex.m_position[0];
        normal[1] = vertex.m_position[1];
        normal[2] = vertex.m_position[2];
        normal.normalize();
        vertex.m_normal[0] = normal[0];
        vertex.m_normal[1] = normal[1];
        vertex.m_normal[2] = normal[2];

        verticesBuffer->write(vertex);
    }
    OE_CHECK(verticesBuffer->isEndOfBuffer(), OE_CHANNEL_RESOURCE);
    verticesBuffer->rewind();

    // Calculate tangents and bitangents.
    for(size_t i = 0; i < m_triangles.size(); ++i)
    {
        TIndex i0 = m_triangles[i].m_indices[0];
        TIndex i1 = m_triangles[i].m_indices[1];
        TIndex i2 = m_triangles[i].m_indices[2];

        VertexFormat* vertex = reinterpret_cast<VertexFormat*>(verticesBuffer->m_rawData);

        const Vec3 v0 = {
            vertex[i0].m_position[0],
            vertex[i0].m_position[1],
            vertex[i0].m_position[2] };

        const Vec3 v1 = {
            vertex[i1].m_position[0],
            vertex[i1].m_position[1],
            vertex[i1].m_position[2] };

        const Vec3 v2 = {
            vertex[i2].m_position[0],
            vertex[i2].m_position[1],
            vertex[i2].m_position[2] };


        const Vec2& uv0 = {
            vertex[i0].m_uv[0],
            vertex[i0].m_uv[1] };

        const Vec2& uv1 = {
            vertex[i1].m_uv[0],
            vertex[i1].m_uv[1] };

        const Vec2& uv2 = {
            vertex[i2].m_uv[0],
            vertex[i2].m_uv[1] };

        // Edges of the triangle : position delta
        const Vec3 deltaPos1 = v1 - v0;
        const Vec3 deltaPos2 = v2 - v0;

        // UV delta
        const Vec2 deltaUV1 = uv1 - uv0;
        const Vec2 deltaUV2 = uv2 - uv0;

        float r = 1.0f / ((deltaUV1[0] * deltaUV2[1]) - (deltaUV1[1] * deltaUV2[0]));
        Vec3 tangent = (deltaPos1 * deltaUV2[1] - deltaPos2 * deltaUV1[1]) * r;
        Vec3 bitangent = (deltaPos2 * deltaUV1[0] - deltaPos1 * deltaUV2[0]) * r;
    }


    UniquePtr<Buffer> indiciesBuffer(new Buffer(m_nTriangles * 3 * sizeof(TIndex)));

    // Euler formula: V - E + F = 2
    // E = V + F - 2 = 12 + 20 - 2 = 30
    OE_CHECK(m_nTriangles % 2 == 0, OE_CHANNEL_ICOSAHEDRON);

    UniquePtr<Buffer> linesBuffer(new Buffer((m_nPoints + m_nTriangles - 2) * 2 * sizeof(Vertex_XYZ_Color)));

    uint nNodes = 0;

#if _DEBUG
    for(uint iNode = 0; iNode < m_nodes.size(); ++iNode)
    {
        if(m_nodes[iNode].m_flags.isSet(Node::Flags::Used) && !m_nodes[iNode].m_flags.isSet(Node::Flags::HasChildren))
        {
            ++nNodes;
        }
    }

    OE_CHECK(nNodes == m_nTriangles, OE_CHANNEL_ICOSAHEDRON);
    nNodes = 0;
#endif

    for(uint iNode = 0; iNode < m_nodes.size(); ++iNode)
    {
        const Node& node = m_nodes[iNode];

        if(node.m_flags.isSet(Node::Flags::Used) && !node.m_flags.isSet(Node::Flags::HasChildren))
        {
            ++nNodes;

            // Append indices
            const Triangle& triangle = m_triangles[iNode];
            indiciesBuffer->write(&triangle.m_indices, sizeof(triangle.m_indices));

#if !OE_RELEASE
            if(m_debugRenderMode == DebugRenderMode::Triangles || m_debugRenderMode == DebugRenderMode::Surface)
            {
                auto& composer = Engine::getInstance().getComposer();

                Vec3 p1, p2, p3;
                float3ToVec3(m_points[triangle.m_indices[0]].m_pos, p1);
                float3ToVec3(m_points[triangle.m_indices[1]].m_pos, p2);
                float3ToVec3(m_points[triangle.m_indices[2]].m_pos, p3);
                Vec3 triangleCenterPos = (p1 + p2 + p3) / 3.0f;

                Vec3 v1 = p2 - p1;
                Vec3 v2 = p3 - p2;
                Vec3 n = v1.cross(v2);

                float det = n.dot(camLookAt);
                const uint32 iScene = DefaultRenderSceneType::World;

                if(det < 0)
                {
                    Vec2 positionOnScreen;

                    char text[512];

                    if(m_debugRenderMode == DebugRenderMode::Triangles)
                    {
                        OE_SPRINTF(text, 512, "%d", iNode);
                        composer.projectOnScreen(iScene, triangleCenterPos, positionOnScreen);
                        composer.renderText(text, "vera", 12, uint32(positionOnScreen[0]), uint32(positionOnScreen[1]), HorizontalAnchor::Center, VerticalAnchor::Center);
                    }
                    else if(m_debugRenderMode == DebugRenderMode::Surface)
                    {
                        float surface = n.norm() / 2.0f;
                        OE_SPRINTF(text, 512, "%.3f", surface);
                        composer.projectOnScreen(iScene, triangleCenterPos, positionOnScreen);
                        //renderer->renderText(text, "vera", 12, uint32(positionOnScreen[0]), uint32(positionOnScreen[1]), HorizontalAnchor::Center, VerticalAnchor::Center);

                        Vec2 p1s, p2s, p3s;
                        composer.projectOnScreen(iScene, p1, p1s);
                        composer.projectOnScreen(iScene, p2, p2s);
                        composer.projectOnScreen(iScene, p3, p3s);

                        Vec2 v1s = p2s - p1s;
                        Vec2 v2s = p3s - p2s;
                        Vec3 v1s3d(v1s[0], v1s[1], 0.0f);
                        Vec3 v2s3d(v2s[0], v2s[1], 0.0f);
                        
                        surface = v1s3d.cross(v2s3d).norm() / 2.0f;

                        OE_SPRINTF(text, 512, "%.1f", surface);
                        composer.renderText(text, "vera", 12, uint32(positionOnScreen[0]), uint32(positionOnScreen[1]), HorizontalAnchor::Center, VerticalAnchor::Center);
                    }
                }
            }
#endif

            // Append line points.
            Color lineColor(0xff, 0xff, 0xff);
            for(uint i = 0; i < 3; ++i)
            {
                TIndex i1 = i < 2 ? i : 2;
                TIndex i2 = i < 2 ? i + 1 : 0;

                // Append the edge only if the first index is below the second one
                // to avoid adding the same edge twice. This is using the fact that an edge shared by two triangles
                // have indices swapped.
                if(triangle.m_indices[i1] < triangle.m_indices[i2])
                {
                    const Point& p1 = m_points[triangle.m_indices[i1]];
                    const Point& p2 = m_points[triangle.m_indices[i2]];

                    Vec3 point[2];
                    float3ToVec3(p1.m_pos, point[0]);
                    float3ToVec3(p2.m_pos, point[1]);

                    for(uint iPoint = 0; iPoint < 2; ++iPoint)
                    {
                        // Make them stick slightly above faces of the mesh.
                        point[iPoint].normalize();
                        point[iPoint] *= m_radius + (m_radius * 0.05f);

                        Vertex_XYZ_Color vertex;

                        vec3ToFloat3(point[iPoint], vertex.m_position);

                        vertex.m_color = lineColor.color;

                        linesBuffer->write(vertex);
                    }
                }
            }
        }
    }

#if !OE_RELEASE
    if(m_debugRenderMode == DebugRenderMode::Points)
    {
        auto& composer = Engine::getInstance().getComposer();

        for(uint iPoint = 0; iPoint < m_points.size(); ++iPoint)
        {
            if(!m_points[iPoint].m_bUsed)
                continue;

            Vec3 p;
            float3ToVec3(m_points[iPoint].m_pos, p);

            Vec2 positionOnScreen;

            char text[512];
            OE_SPRINTF(text, 512, "%d", iPoint);

            composer.projectOnScreen(DefaultRenderSceneType::World, p, positionOnScreen);
            composer.renderText(text, "vera", 12, uint32(positionOnScreen[0]), uint32(positionOnScreen[1]), HorizontalAnchor::Center, VerticalAnchor::Center);
        }
    }
#endif

    OE_CHECK(indiciesBuffer->isEndOfBuffer(), OE_CHANNEL_ICOSAHEDRON);
    indiciesBuffer->rewind();

    OE_CHECK(linesBuffer->isEndOfBuffer(), OE_CHANNEL_ICOSAHEDRON);
    linesBuffer->rewind();

    if(m_propResource)
    {
        // Update vertex stream.
        const Vector<MeshResPtr>& meshes = m_propResource->getMeshResources();
        meshes[0]->updateVertexStreamResource(
            std::move(verticesBuffer),
            std::move(indiciesBuffer)
        );

        meshes[1]->updateVertexStreamResource(
            std::move(linesBuffer),
            nullptr
        );
    }
    else
    {
        // Create resources.
        auto vertexStreamResource = MakeShared<VertexStreamResource>(
            std::move(verticesBuffer),
            std::move(indiciesBuffer),
            IndexType::U16,
            VertexDeclaration::XYZ_UV_Color_Normal_Tangent_Bitangent,
            PrimitiveType::TriangleList,
            BufferUsage::Dynamic,
            "icosahedron");

        auto linesStreamResource = MakeShared<VertexStreamResource>(
            std::move(linesBuffer),
            VertexDeclaration::XYZ_Color,
            PrimitiveType::LineList,
            BufferUsage::Dynamic,
            "icosahedron_shell");

        static MaterialResPtr materialResource;
        if(!materialResource)
        {
            auto materialDescPtr = MakeShared<MaterialResource::Descriptor>();
            materialDescPtr->m_vsPath = "mrt";
            materialDescPtr->m_psPath = "mrt";
            materialDescPtr->m_renderPassPath = "render/deferred_render_pass";
            materialDescPtr->m_viewResourcePath = "render/views/deferred";
            materialDescPtr->m_shaderBindingLayoutPath = "deferred_binding";
            strVertexDeclarationToInputs(
                "Vertex_XYZ_UV_Color_Normal_Tangent_Bitangent",
                materialDescPtr->m_inputs);
            materialDescPtr->m_renderStates.m_alphaBlendEnable = false;
            materialDescPtr->m_renderStates.m_cullMode = CullMode::CounterClockWise;
            materialDescPtr->m_renderStates.m_zEnable = true;
            materialDescPtr->m_renderStates.m_zWriteEnable = true;
            materialDescPtr->m_topology = PrimitiveType::TriangleList;

            materialResource = MakeShared<MaterialResource>("icosahedron", materialDescPtr);
        }

        auto meshResourceDesc = MakeShared<MeshResource::Descriptor>();
        meshResourceDesc->m_vertexStreamResource = vertexStreamResource;
        meshResourceDesc->m_materialResource = materialResource;
        auto meshResource = MakeShared<MeshResource>(
            "icosahedron",
            meshResourceDesc);

        auto& resourceManager = ResourceManager::getInstance();
        auto linesMaterialResource = resourceManager.getResource<MaterialResource>("materials/color_line");

        auto lineMeshResourceDesc = MakeShared<MeshResource::Descriptor>();
        lineMeshResourceDesc->m_vertexStreamResource = linesStreamResource;
        lineMeshResourceDesc->m_materialResource = linesMaterialResource;
        auto lineMeshResource = MakeShared<MeshResource>(
            "icosahedron_shell",
            lineMeshResourceDesc);

        Vector<MeshResPtr> meshResources;
        meshResources.reserve(2);
        meshResources.push_back(meshResource);
        meshResources.push_back(lineMeshResource);

        auto propResourceDesc = MakeShared<PropResource::Descriptor>();
        propResourceDesc->m_meshResources = std::move(meshResources);

        m_propResource = MakeShared<PropResource>(
            "icosahedron",
            propResourceDesc);
        resourceManager.scheduleResourceLoad(m_propResource);
    }

    return m_propResource;
}

void IcosahedronComponent::setColor(Color color)
{
    m_color = color;
}

void IcosahedronComponent::acquirePoints(uint32 n, TIndex indices[])
{
    const uint32 nRecycled = min(n, uint32(m_freePoints.size()));
    for(uint32 iRecycle = 0; iRecycle < nRecycled; ++iRecycle)
    {
        indices[iRecycle] = m_freePoints.back();
        m_freePoints.pop_back();
    }

    for(uint32 iNew = nRecycled; iNew < n; ++iNew)
    {
        indices[iNew] = uint32(m_points.size());
        m_points.push_back(Point());
    }

#if _DEBUG
    for(uint32 i = 0; i < n; ++i)
    {
        m_points[indices[i]].m_bUsed = true;
    }
    
#endif

    m_nPoints += n;
}

void IcosahedronComponent::acquireNodes(uint32 n, TIndex indices[])
{
    const uint32 nRecycled = min(n, uint32(m_freeNodes.size()));
    for(uint32 iRecycle = 0; iRecycle < nRecycled; ++iRecycle)
    {
        indices[iRecycle] = m_freeNodes.back();
        new(&m_nodes[indices[iRecycle]])Node();
        m_nodeTags[indices[iRecycle]] = m_currentTag;
        m_freeNodes.pop_back();
    }

    for(uint32 iNew = nRecycled; iNew < n; ++iNew)
    {
        OE_CHECK(m_nodes.size() + 1 < TIndex(-1), OE_CHANNEL_ICOSAHEDRON);
        indices[iNew] = uint32(m_nodes.size());
        m_nodes.push_back(Node());
        m_triangles.push_back(Triangle());
        m_nodeTags.push_back(m_currentTag);
    }

    for(uint32 i = 0; i < n; ++i)
    {
        m_nodes[indices[i]].m_flags.set(Node::Flags::Used);
    }
}

void IcosahedronComponent::releasePoints(const TIndex indices[], uint32 n)
{
    for(uint32 iRecycle = 0; iRecycle < n; ++iRecycle)
    {
        m_freePoints.push_back(indices[iRecycle]);

#if _DEBUG
        m_points[indices[iRecycle]].m_bUsed = false;
#endif
    }

    m_nPoints -= n;
}

void IcosahedronComponent::releaseNodes(const TIndex indices[], uint32 n)
{
    for(uint32 iRecycle = 0; iRecycle < n; ++iRecycle)
    {
        if(indices[iRecycle] == 0)
        {
            OE_CHECK(false, OE_CHANNEL_ICOSAHEDRON);
        }
        m_nodes[indices[iRecycle]].m_flags.remove(Node::Flags::Used);
        m_freeNodes.push_back(indices[iRecycle]);
    }
}

uint32 IcosahedronComponent::getNumChildren(TIndex iNode) const
{
    if(hasChildren(iNode))
    {
        return isTransitioning(iNode) ? 2 : 4;
    }

    return 0;
}

bool IcosahedronComponent::findNeighborsChildSharingEdge(
    TIndex iNeightborNode,
    const TIndex points[2], 
    FoundNeighborChildSharedResult& result) const
{
    const uint nChildren = getNumChildren(iNeightborNode);
    OE_CHECK(nChildren > 0, OE_CHANNEL_ICOSAHEDRON);

    for(uint iChild = 0; iChild < nChildren; ++iChild)
    {
        const TIndex iChildNode = m_nodes[iNeightborNode].m_iChildren[iChild];
        for(uint iEdge = 0; iEdge < 3; ++iEdge)
        {
            if(m_triangles[iChildNode].m_indices[iEdge] == points[0] &&
                m_triangles[iChildNode].m_indices[(iEdge + 1) % 3] == points[1])
            {
                result.m_iNode = iChildNode;
                result.m_iEdge = iEdge;
                return true;
            }
        }
    }

    return false;
}

void IcosahedronComponent::findParentNeighborsSharingChildEdge(TIndex iChildNode, TIndex iChildEdgeParentNeightbor[3]) const
{
    const TIndex iParentNode = m_nodes[iChildNode].m_iParent;

    bool bFound = false;

    if(isValidIndex(iParentNode))
    {
        for(uint iChild = 0; iChild < 3; ++iChild)
        {
            if(m_nodes[iParentNode].m_iChildren[iChild] == iChildNode)
            {
                // The first edge of a child is never shared with the parent.
                iChildEdgeParentNeightbor[0] = -1;
                iChildEdgeParentNeightbor[1] = m_nodes[iParentNode].m_iNeighbors[(iChild + 2) % 3];
                iChildEdgeParentNeightbor[2] = m_nodes[iParentNode].m_iNeighbors[iChild];
                bFound = true;
                break;
            }
        }
    }

    if(!bFound)
    {
        // The central child shares no edge with its parents.
        iChildEdgeParentNeightbor[0] = -1;
        iChildEdgeParentNeightbor[1] = -1;
        iChildEdgeParentNeightbor[2] = -1;
    }
}

bool IcosahedronComponent::findEdgeForNeighbor(TIndex iNode, TIndex iNeighborNode, uint& iEdge) const
{
    for(int i = 0; i < 3; ++i)
    {
        if(m_nodes[iNode].m_iNeighbors[i] == iNeighborNode)
        {
            iEdge = i;
            return true;
        }
    }

    return false;
}

uint32 IcosahedronComponent::findTransitionEdge(TIndex iNode) const
{
    OE_CHECK(isTransitioning(iNode), OE_CHANNEL_ICOSAHEDRON);

    const TIndex iOppositePoint = m_triangles[m_nodes[iNode].m_iChildren[0]].m_indices[2];

    for(uint32 iEdge = 0; iEdge < 3; ++iEdge)
    {
        if(m_triangles[iNode].m_indices[iEdge] == iOppositePoint)
        {
            return modIndex(iEdge + 1);
        }
    }

    return -1;
}

float IcosahedronComponent::calcVisibleSurface(TIndex iNode) const
{
    Vec3 p1, p2, p3;

    const Triangle& triangle = m_triangles[iNode];

    float3ToVec3(m_points[triangle.m_indices[0]].m_pos, p1);
    float3ToVec3(m_points[triangle.m_indices[1]].m_pos, p2);
    float3ToVec3(m_points[triangle.m_indices[2]].m_pos, p3);

    Composer& renderer = Composer::getInstance();

    Vec2 p1s, p2s, p3s;
    const uint32 iScene = DefaultRenderSceneType::World;
    renderer.projectOnScreen(iScene, p1, p1s);
    renderer.projectOnScreen(iScene, p2, p2s);
    renderer.projectOnScreen(iScene, p3, p3s);

    Vec2 v1s = p2s - p1s;
    Vec2 v2s = p3s - p2s;
    Vec3 v1s3d(v1s[0], v1s[1], 0.0f);
    Vec3 v2s3d(v2s[0], v2s[1], 0.0f);

    float visibleSurface = v1s3d.cross(v2s3d).norm() / 2.0f;
    return visibleSurface;
}

float IcosahedronComponent::calcNormalDotLookAt(TIndex iNode, const Vec3& lookAt, bool bNormalize) const
{
    Vec3 p1, p2, p3;

    const Triangle& triangle = m_triangles[iNode];

    float3ToVec3(m_points[triangle.m_indices[0]].m_pos, p1);
    float3ToVec3(m_points[triangle.m_indices[1]].m_pos, p2);
    float3ToVec3(m_points[triangle.m_indices[2]].m_pos, p3);
    Vec3 triangleCenterPos = (p1 + p2 + p3) / 3.0f;

    Vec3 v1 = p2 - p1;
    Vec3 v2 = p3 - p2;
    Vec3 n = v1.cross(v2);

    if(bNormalize)
    {
        n.normalize();
    }

    return n.dot(lookAt);
}


#ifdef _DEBUG
void IcosahedronComponent::sanityCheck()
{
    for(int iNode = 0; iNode < int(m_nodes.size()); ++iNode)
    {
        if(!isNodeUsed(iNode))
            continue;

        const Node& node = m_nodes[iNode];

        if(isValidIndex(node.m_iParent) && isTransitioning(node.m_iParent))
        {
            // No other node should have a reference to us because we are a child of a transitioning node.
            for(int iNodeOther = 0; iNodeOther < int(m_nodes.size()); ++iNodeOther)
            {
                if(iNodeOther == iNode)
                    continue;

                if(!isNodeUsed(iNodeOther))
                    continue;

                const Node& otherNode = m_nodes[iNodeOther];

                for(int iOtherNeighbor = 0; iOtherNeighbor < 3; ++iOtherNeighbor)
                {
                    if(isValidIndex(otherNode.m_iParent) && isTransitioning(otherNode.m_iParent))
                    {
                        continue;
                    }

                    for(int iOtherNeighbor = 0; iOtherNeighbor < 3; ++iOtherNeighbor)
                    {
                        if(otherNode.m_iNeighbors[iOtherNeighbor] == iNode)
                        {
                            OE_CHECK(false, OE_CHANNEL_ICOSAHEDRON);
                        }
                    }
                }
            }
        }
        else if(!isValidIndex(node.m_iParent) || !isTransitioning(node.m_iParent))
        {
            // We have 3 neighbors.
            for(int iNeighbor = 0; iNeighbor < 3; ++iNeighbor)
            {
                TIndex iNeighborNode = node.m_iNeighbors[iNeighbor];

                if(!isValidIndex(iNeighborNode))
                    continue;

                OE_CHECK(iNeighborNode != iNode, OE_CHANNEL_ICOSAHEDRON);
                OE_CHECK(isNodeUsed(iNeighborNode), OE_CHANNEL_ICOSAHEDRON);

                const Node& neighborNode = m_nodes[iNeighborNode];

                TIndex i10 = m_triangles[iNode].m_indices[iNeighbor];
                TIndex i11 = m_triangles[iNode].m_indices[modIndex(iNeighbor + 1)];

                OE_CHECK(node.m_level == neighborNode.m_level, OE_CHANNEL_ICOSAHEDRON);

                // Make sure we can find ourselves in the list of neighbors.
                bool bFound = false;
                for(int iNeighborNeighbor = 0; iNeighborNeighbor < 3; ++iNeighborNeighbor)
                {
                    if(neighborNode.m_iNeighbors[iNeighborNeighbor] == iNode)
                    {
                        TIndex i20 = m_triangles[iNeighborNode].m_indices[iNeighborNeighbor];
                        TIndex i21 = m_triangles[iNeighborNode].m_indices[modIndex(iNeighborNeighbor + 1)];

                        // Indices should be switched.
                        OE_CHECK(i10 == i21, OE_CHANNEL_ICOSAHEDRON);
                        OE_CHECK(i21 == i10, OE_CHANNEL_ICOSAHEDRON);

                        bFound = true;
                        break;
                    }
                }
                OE_CHECK(bFound, OE_CHANNEL_ICOSAHEDRON);
            }
        }
    }
}
#endif

IcosahedronComponentConfig::IcosahedronComponentConfig()
{
    Json::Value jsonConfig;
    OE_CHECK_RESULT(
        appLoadJsonFile("configs" PATH_SEPARATOR "icosahedron_component.json", jsonConfig),
        true,
        OE_CHANNEL_ICOSAHEDRON);

    const Json::Value& jsonTesselation = jsonConfig["tesselation"];
    OE_CHECK(jsonTesselation.isArray(), OE_CHANNEL_ICOSAHEDRON);

    m_tesselation.reserve(jsonTesselation.size());

    for(uint i = 0; i < jsonTesselation.size(); ++i)
    {
        const Json::Value& jsonTeselationEntry = jsonTesselation[i];
        OE_CHECK(jsonTeselationEntry.isArray(), OE_CHANNEL_ICOSAHEDRON);
        OE_CHECK(jsonTeselationEntry.size() == 2, OE_CHANNEL_ICOSAHEDRON);

        TesselationEntry tesselationEntry;
        tesselationEntry.m_surface = jsonTeselationEntry[0].asUInt();
        tesselationEntry.m_lod = jsonTeselationEntry[1].asUInt();
        m_tesselation.push_back(tesselationEntry);
    }
}

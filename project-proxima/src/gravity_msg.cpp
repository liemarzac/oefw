#include "gravity_msg.h"

RTTI_IMPL_PARENT(GravityMsg, Message)

GravityMsg::GravityMsg(void* sender) :
    Message(sender)
    ,m_mass(0.0)
    ,m_accelCompReceiver(nullptr)
{
}

void GravityMsg::setSource(Real mass)
{
    m_set.set(Flags::Source);
    m_mass = mass;
}

void GravityMsg::setReceiver(Component* accelCompReceiver)
{
    m_accelCompReceiver = accelCompReceiver;
    OE_CHECK(m_accelCompReceiver != nullptr, OE_CHANNEL_MESSAGE);
    m_set.set(Flags::Receiver);
}



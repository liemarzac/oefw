#include "sequence.h"
#include "neural_network.h"
#include "neuron.h"

namespace OE_Neural
{

Sequence::Sequence():
    m_nInputs(0)
    ,m_nOutputs(0)
{
}

Sequence::~Sequence()
{
}

SharedPtr<NeuralNetwork> Sequence::makeNetwork()
{
    m_network = SharedPtr<NeuralNetwork>(new NeuralNetwork());

    // The first step is to create all required neurons.
    for(uint32 i = 0; i < m_blocks.size(); ++i)
    {
        const Block& block = m_blocks[i];
        m_network->addNeuron(block);
    }

    // Then connect neurons to each others.
    for(uint32 i = 0; i < m_blocks.size(); ++i)
    {
        const Block& block = m_blocks[i];
        m_network->connectNeuron(block);
    }

    return m_network;
}

void Sequence::generateFrom(SharedPtr<NeuralNetwork> network)
{
    m_network = network;

    auto inputNeurons = network->getNeurons(NeuronType::Input);
    auto intermediateNeurons = network->getNeurons(NeuronType::Intermediate);
    auto outputNeurons = network->getNeurons(NeuronType::Output);

    m_blocks.clear();
    const size_t nNeurons =
        inputNeurons.size() + intermediateNeurons.size() + outputNeurons.size();

    m_blocks.reserve(nNeurons);

    m_nInputs = uint32(inputNeurons.size());
    m_nOutputs = uint32(outputNeurons.size());

    Block block;
    for(Neuron* neuron : inputNeurons)
    {
        neuron->makeBlock(block);
        block.m_type = (uint8)(NeuronType::Input);
        m_blocks.push_back(block);
    }

    for(Neuron* neuron : intermediateNeurons)
    {
        neuron->makeBlock(block);
        block.m_type = (uint8)(NeuronType::Intermediate);
        m_blocks.push_back(block);
    }

    for(Neuron* neuron : outputNeurons)
    {
        neuron->makeBlock(block);
        block.m_type = (uint8)(NeuronType::Output);
        m_blocks.push_back(block);
    }
}


} // OE_Neural


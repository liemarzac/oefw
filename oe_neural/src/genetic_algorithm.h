#ifndef __OE_NEURAL_GENETIC_ALGORITHM_H__
#define __OE_NEURAL_GENETIC_ALGORITHM_H__

#include "neural_private.h"

namespace OE_Neural
{


class OE_NEURAL_EXPORT GeneticAlgorithm
{
public:
    GeneticAlgorithm(size_t numSamples);
    ~GeneticAlgorithm();

private:
    GeneticAlgorithm();
    void evaluate();
    void sort();
    void mate();
    class DNA* m_populationA;
    class DNA* m_populationB;
    class DNA* m_parentPopulation;
    class DNA* m_offspringPopulation;
    size_t m_numSamples;
    uint32 m_numGenerations;
    float m_minFitness;
    float m_maxFitness;
    float m_minError;
    float m_maxError;
    float m_averageError;
    float m_fitnessTotalizer;
    float m_errorTotalizer;
    float m_averageFitness;
    uint32 m_ibestDNA;
};


}

#endif // __OE_NEURAL_GENETIC_ALGORITHM_H__
#ifndef __OE_NEURAL_DNA_H__
#define __OE_NEURAL_DNA_H__

#include "neural_private.h"

namespace OE_Neural
{


class DNA
{
public:
    static const unsigned int MaxAttendants = 2;

    enum class Hospital : unsigned int
    {
        GeneralNeurology,
        MultipleSclerosis,
        Sleep,
        Rascp,
        MovementDisorderNational,
        MovementDisorderRoyalFree,
        EpilepsyDrRunnGun,
        EpilepsyDrEricsson,
        NeuroGenetics,
        MoyaMoya,
        ICH,
        Headache,
        CardioStrokeHeartFailure,
        DayCare,
        Bronchiectasis,
        NumberOf
    };

    static const char* HospitalNames[];

    enum class Day : unsigned char
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        NumberOf
    };

    static const char* DayNames[];

    enum class DayTime : unsigned char
    {
        AM,
        PM,
        NumberOf,
    };

    static const char* DayTimeNames[];

    enum class Attendant : unsigned int
    {
        Robbin,
        Bola,
        Miriam,
        Elyssa,
        Grace,
        NumberOf
    };

    static const char* AttendantNames[];

    enum class Frequency : unsigned int
    {
        EveryWeek,
        EveryOtherWeek,
        NumberOf
    };

    static const char* FrequencyNames[];

    static unsigned int BloodTakers[];

    typedef uint8 Week;

    struct Slot
    {
        Day m_day;
        DayTime m_dayTime;
        Hospital m_hospital;
        Attendant m_attendants[MaxAttendants];
        Frequency m_frequency;
        uint8 m_nAttendants;
        Week m_iWeek;
        bool m_bBloodRequired;
        bool m_bAllDay;
    };

    static const unsigned int NumWeeks = 4;
    static const unsigned int NumSlots = NumWeeks * (9 + 1);

	DNA();
	~DNA();
	void fitness();
	float getFitness() const;
    float getErrors() const;
	void copyFrom(const DNA* original);
	void mixWith(const DNA* original);
	const Slot* getSlots() const;
	void generate();

private:
    Slot m_slots[NumSlots];

    float m_fitness;
    float m_errors;
    float m_warnings;
    float m_bonus;

    void randAttendants(Slot* slot);
};


}

#endif // __OE_NEURAL_NEURAL_NETWORK_H__
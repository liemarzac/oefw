#include "genetic_algorithm.h"

#include "neural_private.h"

// External
#include <cfloat>
#include <cstring>
#include <fstream>
#include <iostream>

// OE_Core
#include <rand.h>

// OE_Neural
#include "dna.h"


namespace OE_Neural
{

GeneticAlgorithm::GeneticAlgorithm(size_t numSamples)
{
    OE_CHECK(numSamples >= 2, OE_CHANNEL_NEURAL);
    m_numSamples = numSamples;
    m_ibestDNA = 0;

    m_populationA = new DNA[numSamples];
    for(size_t i = 0; i < numSamples; ++i)
    {
        m_populationA[i].generate();
        m_populationA[i].fitness();
    }
    m_parentPopulation = m_populationA;
    m_numGenerations = 1;
    evaluate();
    sort();

    m_populationB = new DNA[numSamples];
    for(size_t i = 0; i < numSamples; ++i)
    {
        m_populationB[i].generate();
    }
    m_offspringPopulation = m_populationB;

    uint32 nGenerationsWithoutChange = 0;
    float prevMaxFitness = 0.0f;

    do
    {
        float avg = m_averageFitness;
        mate();
        sort();
        log(OE_CHANNEL_NEURAL, "======== Generation #%d ========", m_numGenerations);
        log(OE_CHANNEL_NEURAL, "    max fitness %.4f", m_maxFitness);
        log(OE_CHANNEL_NEURAL, "    avg fitness %.4f", m_averageFitness);
        log(OE_CHANNEL_NEURAL, "    min error %.4f", m_minError);
        log(OE_CHANNEL_NEURAL, "    avg error %.4f", m_averageError);

        if(prevMaxFitness != m_maxFitness)
        {
            nGenerationsWithoutChange = 0;
            prevMaxFitness = m_maxFitness;
        }
        else
        {
            ++nGenerationsWithoutChange;
        }
    } while(nGenerationsWithoutChange < 20);

    // Output best result.
    std::ofstream resultFile;
    resultFile.open("rota.txt");

    DNA& bestDNA = m_parentPopulation[0];

    const size_t bufferSize = 1024;
    char buffer[bufferSize];

    for(uint32 i = 0; i < bestDNA.NumSlots; ++i)
    {
        const DNA::Slot& slot = bestDNA.getSlots()[i];

        resultFile << "***********************************\n";

        if(slot.m_bAllDay)
        {
            OE_SPRINTF(buffer, bufferSize, "Week %d, %s (all day)\n", slot.m_iWeek + 1, DNA::DayNames[(uint32)slot.m_day]);
        }
        else
        {
            OE_SPRINTF(buffer, bufferSize, "Week %d, %s %s\n", slot.m_iWeek + 1, DNA::DayNames[(uint32)slot.m_day], DNA::DayTimeNames[(uint32)slot.m_dayTime]);
        }

        resultFile << buffer;

        if(slot.m_bBloodRequired)
        {
            OE_SPRINTF(buffer, bufferSize, "Hospital: %s (blood required)\n", DNA::HospitalNames[(uint32)slot.m_hospital]);
        }
        else
        {
            OE_SPRINTF(buffer, bufferSize, "Hospital: %s\n", DNA::HospitalNames[(uint32)slot.m_hospital]);
        }

        resultFile << buffer;

        const size_t attendantBufferSize = 512;
        char attendantsbuffer[attendantBufferSize];
        char* bufferPos = attendantsbuffer;
        for(size_t i = 0; i < slot.m_nAttendants; ++i)
        {
            const char* attendantName = DNA::AttendantNames[(uint32)slot.m_attendants[i]];
            if(i == slot.m_nAttendants - 1)
            {
                OE_SPRINTF(bufferPos, attendantBufferSize - (bufferPos - attendantsbuffer), "%s", attendantName);
            }
            else
            {
                OE_SPRINTF(bufferPos, attendantBufferSize - (bufferPos - attendantsbuffer), "%s, ", attendantName);
                bufferPos += strlen(attendantName) + 2;
            }
        }

        OE_SPRINTF(buffer, bufferSize, "Attendants: %s\n\n", attendantsbuffer);
        resultFile << buffer;
    }

    resultFile << "***********************************";

    resultFile.close();
}

GeneticAlgorithm::~GeneticAlgorithm()
{
    if(m_populationB)
        delete[] m_populationB;

    if(m_populationA)
        delete[] m_populationA;
}

void GeneticAlgorithm::evaluate()
{
    m_fitnessTotalizer = 0.0f;
    m_minFitness = FLT_MAX;
    m_maxFitness = 0.0f;
    m_minError = FLT_MAX;
    m_maxError = 0.0f;
    m_errorTotalizer = 0.0f;
    for(uint i = 0; i < uint(m_numSamples); ++i)
    {
        float fitness = m_parentPopulation[i].getFitness();
        m_fitnessTotalizer += fitness;
        if(fitness > m_maxFitness)
        {
            m_maxFitness = fitness;
            m_ibestDNA = i;
        }

        if(fitness < m_minFitness)
            m_minFitness = fitness;

        float errors = m_parentPopulation[i].getErrors();
        m_errorTotalizer += errors;
        if(errors > m_maxError)
            m_maxError = errors;

        if(errors < m_minError)
            m_minError = errors;
    }

    m_averageFitness = (float)m_fitnessTotalizer / m_numSamples;
    m_averageError = (float)m_errorTotalizer / m_numSamples;
}

void GeneticAlgorithm::sort()
{
    // Quick sort.
    std::qsort(m_parentPopulation, m_numSamples, sizeof(DNA), [](const void* a, const void* b)
    {
        const DNA* arg1 = reinterpret_cast<const DNA*>(a);
        const DNA* arg2 = reinterpret_cast<const DNA*>(b);

        const float fitness1 = arg1->getFitness();
        const float fitness2 = arg2->getFitness();

        if(fitness1 > fitness2)
            return -1;

        if(fitness1 < fitness2)
            return 1;

        return 0;
    });
}

void GeneticAlgorithm::mate()
{
    size_t iCurrentChild = 0;
    while(iCurrentChild < m_numSamples)
    {
        DNA* parentA = NULL;
        DNA* parentB = NULL;
        size_t iParentA = 0;
        size_t iParentB = 0;
        if(m_fitnessTotalizer > 0)
        {
            float randParentA = randr32(0.0f, m_fitnessTotalizer);
            float randParentB = randr32(0.0f, m_fitnessTotalizer);
            float currentFitnessAccumulator = 0;

            for(size_t i = 0; i < m_numSamples; ++i)
            {
                currentFitnessAccumulator += m_parentPopulation[i].getFitness();
                if(parentA == NULL && currentFitnessAccumulator > randParentA || i == m_numSamples - 1)
                {
                    iParentA = i;
                    if(parentB != NULL)
                    {
                        while(iParentA == iParentB)
                        {
                            iParentA = (size_t)randi32(0, (int)m_numSamples);
                        };
                    }

                    parentA = &m_parentPopulation[iParentA];
                    if(parentB != NULL)
                        break;
                }

                if(parentB == NULL && currentFitnessAccumulator > randParentB || i == m_numSamples - 1)
                {
                    iParentB = i;
                    if(parentA != NULL)
                    {
                        while(iParentB == iParentA)
                        {
                            iParentB = (size_t)randi32(0, (int)m_numSamples);
                        };
                    }

                    parentB = &m_parentPopulation[iParentB];
                    if(parentA != NULL)
                        break;
                }
            }

            OE_CHECK(parentA, OE_CHANNEL_NEURAL);
            OE_CHECK(parentB, OE_CHANNEL_NEURAL);
        }
        else
        {
            iParentA = randi32(0, (int)m_numSamples);
            do
            {
                iParentB = randi32(0, (int)m_numSamples);
            } while(iParentB == iParentA);
            parentA = &m_parentPopulation[iParentA];
            parentB = &m_parentPopulation[iParentB];
        }

        // Make a baby.
        m_offspringPopulation[iCurrentChild].copyFrom(parentA);
        m_offspringPopulation[iCurrentChild].mixWith(parentB);
        m_offspringPopulation[iCurrentChild].fitness();
        ++iCurrentChild;
    }

    ++m_numGenerations;

    // swap parents and offspring.
    DNA* temp = m_parentPopulation;
    m_parentPopulation = m_offspringPopulation;
    m_offspringPopulation = temp;
    evaluate();
}

} // OE_Neural

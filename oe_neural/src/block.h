#ifndef __OE_NEURAL_BLOCK_H__
#define __OE_NEURAL_BLOCK_H__

#include "neural_private.h"

namespace OE_Neural
{

struct Block
{
    struct Connection
    {
        uint32 m_id;
        uint32 m_index;

        Connection& operator=(const Connection& rhs)
        {
            m_id = rhs.m_id;
            m_index = rhs.m_index;
            return *this;
        }
    };

    uint32 m_id;
    uint32 m_nDentrites;
    uint32 m_nConnections;
    uint8 m_type;
    float m_weights[MaxDentrites];
    Connection m_connections[MaxDentrites];

    Block& operator=(const Block& rhs)
    {
        m_id = rhs.m_id;
        m_nDentrites = rhs.m_nDentrites;
        m_nConnections = rhs.m_nConnections;
        m_type = rhs.m_type;
        for(uint32 i = 0; i < m_nDentrites; ++i)
        {
            m_weights[i] = rhs.m_weights[i];
        }

        for(uint32 i = 0; i < m_nDentrites; ++i)
        {
            m_connections[i] = rhs.m_connections[i];
        }

        return *this;
    }
};

} // OE_Neural

#endif // __OE_NEURAL_BLOCK_H__

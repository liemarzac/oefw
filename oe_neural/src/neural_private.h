#ifndef __OE_NEURAL_PRIVATE_H__
#define __OE_NEURAL_PRIVATE_H__

#include "oe_neural.h"

// OE_Core
#include <rand.h>
#include <vector.h>

using namespace OE_Core;

#define MaxDentrites 10
#define OE_CHANNEL_NEURAL "Neural"

#define SANITY_CHECKS

#ifdef SANITY_CHECKS
#define CHECK_SANITY {checkSanity();}
#else
#define CHECK_SANITY
#endif

namespace OE_Neural
{

enum class NeuronType
{
    Input,
    Intermediate,
    Output,
    NumberOf
};

template<typename T>
class Roulette
{
public:
    Roulette() :
        m_total(0)
    {
    }

    void setEntries(const Vector<uint32>& entries)
    {
        m_entries = entries;
        for(uint i = 0; i < m_entries.size(); ++i)
        {
            m_total += entries[i];
        }
    }

    T roll() const
    {
        OE_CHECK(m_total > 0, OE_CHANNEL_NEURAL);
        uint32 val = randu32(0, m_total - 1);
        uint32 accumulator = 0;
        for(uint i = 0; i < m_entries.size(); ++i)
        {
            accumulator += m_entries[i];
            if(val < accumulator)
            {
                return (T)i;
            }
        }

        OE_CHECK(false, OE_CHANNEL_NEURAL);
        return (T)0;
    }

private:
    Vector<uint32> m_entries;
    uint32 m_total;
};

}

#if _WIN32
#pragma warning(disable: 4251)
#endif

#endif // __OE_NEURAL_PRIVATE_H__

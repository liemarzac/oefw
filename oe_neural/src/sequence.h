#ifndef __OE_NEURAL_SEQUENCE_H__
#define __OE_NEURAL_SEQUENCE_H__

#include "neural_private.h"
#include "block.h"

#include "std_smartptr_helpers.h"

namespace OE_Neural
{

class NeuralNetwork;
class Neuron;

class OE_NEURAL_EXPORT Sequence
{
public:
    Sequence();
    virtual ~Sequence();

    SharedPtr<NeuralNetwork> makeNetwork();
    void generateFrom(SharedPtr<NeuralNetwork> network);

private:
    Sequence(const Sequence& rhs);
    Sequence& operator=(const Sequence& rhs);

    Vector<Block> m_blocks;
    uint32 m_nInputs;
    uint32 m_nOutputs;
    SharedPtr<NeuralNetwork> m_network;
};

} // OE_Neural

#endif // __OE_NEURAL_SEQUENCE_H__

#ifndef __OE_NEURAL_NEURON_H__
#define __OE_NEURAL_NEURON_H__

#include "neural_private.h"

#include <functional>

namespace OE_Neural
{

struct Block;
OE_NEURAL_EXPORT float sigmoidTriggerFunction(float input);
OE_NEURAL_EXPORT float linearTriggerFunction(float input);

extern OE_NEURAL_EXPORT std::function<float(float)> g_triggerFunction;

class OE_NEURAL_EXPORT Neuron
{
public:
    struct Dentrite
    {
        Dentrite()
        {
            m_weight = 0.0f;
            m_input = 0.0f;
            m_isConnected = false;
        }

        float m_weight;
        float m_input;
        bool m_isConnected;

        Dentrite& operator=(const Dentrite& rhs)
        {
            m_weight = rhs.m_weight;
            m_input = rhs.m_input;
            m_isConnected = rhs.m_isConnected;
            return *this;
        }
    };

    struct Axon
    {
        struct Connection
        {
            Neuron* m_neuron;
            uint32 m_index;
        };

        Axon()
        {
            m_output = 0.0f;
        }

        void feedForward();

        Vector<Connection> m_connections;
        float m_output;
    };

    Neuron(uint32 id);
    void makeBlock(Block& block) const;
    uint32 getId() const;
    bool setInput(uint32 iNeuron, float input);
    float getOutput() const;
    uint32 addDentrite(float weight);
    float getDentriteWeight(uint32 iDentrite) const;
    void setDentriteWeight(uint32 iDentrite, float weight);
    uint32 getNumTotalDentrites() const;
    uint32 getNumConnectedDentrites() const;
    bool isDentriteConnected(uint32 iDentrite) const;
    void connect(Neuron* toNeuron, float weight);
    void connect(Neuron* toNeuron, uint32 iDentrite);
    void disconnect(uint32 iConnection);
    void disconnectAll();
    float update();
    void getConnectedNeurons(Vector<Neuron*>& connectedNeurons);
    uint32 getNumConnections() const;
    const Axon::Connection& getConnectionIndex(uint32 iConnection) const;
    const Axon::Connection* getConnectionToNeuronId(uint32 neuronId) const;

private:
    Neuron();
    Neuron(const Neuron& rhs);
    Neuron& operator=(const Neuron& rhs);

    Vector<Dentrite> m_dentrites;
    Axon m_axon;
    uint32 m_id;
};

}

#endif // __OE_NEURAL_NEURON_H__

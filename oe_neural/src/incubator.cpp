#include "incubator.h"

#include "oe_neural.h"

// External
#include <algorithm>
#include <cfloat>
#include <json/json.h>

// OE_Core
#include <file.h>
#include <rand.h>

// OE_Neural
#include "neural_network.h"
#include "neuron.h"


namespace OE_Neural
{

std::function<float(NeuralNetwork*)> g_testFunction;

Incubator::Incubator(BestScoreType type):
    m_bestScoreType(type)
    ,m_avgScore(0.0f)
    ,m_avgEliteScore(0.0f)
    ,m_nGenerationWithoutImprovement(0)
    ,m_lastSteppingDecrease(5)
{
    m_elitismRate = 0.0f;
    m_addNeuronRate = 0.0f;
    m_removeNeuronRate = 0.0f;
    m_addConnectionRate = 0.0f;
    m_removeConnectionRate = 0.0f;
    m_bestNetwork = nullptr;

    Json::Value config;
    bool result = appLoadJsonFile("configs" PATH_SEPARATOR "incubator.json", config);
    OE_CHECK(result, OE_CHANNEL_NEURAL);

    m_elitismRate = config["elitism_rate"].asFloat();
    OE_CHECK_MSG(
        m_elitismRate >= 0.0f && m_elitismRate <= 1.0f,
        OE_CHANNEL_NEURAL,
        "elitism_rate must be between 0.0 and 1.0");

    m_addNeuronRate = config["add_neuron_rate"].asFloat();
    OE_CHECK_MSG(
        m_addNeuronRate >= 0.0f && m_addNeuronRate <= 1.0f,
        OE_CHANNEL_NEURAL,
        "add_neuron_rate must be between 0.0 and 1.0");

    m_removeNeuronRate = config["remove_neuron_rate"].asFloat();
    OE_CHECK_MSG(
        m_removeNeuronRate >= 0.0f && m_removeNeuronRate <= 1.0f,
        OE_CHANNEL_NEURAL,
        "remove_neuron_rate must be between 0.0 and 1.0");

    m_addConnectionRate = config["add_connection_rate"].asFloat();
    OE_CHECK_MSG(
        m_addConnectionRate >= 0.0f && m_addConnectionRate <= 1.0f,
        OE_CHANNEL_NEURAL,
        "add_connection_rate must be between 0.0 and 1.0");

    m_removeConnectionRate = config["remove_connection_rate"].asFloat();
    OE_CHECK_MSG(
        m_removeConnectionRate >= 0.0f && m_removeConnectionRate <= 1.0f,
        OE_CHANNEL_NEURAL,
        "remove_connection_rate must be between 0.0 and 1.0");

    Vector<uint32> mutationRouletteEntries;
    mutationRouletteEntries.resize((size_t)NetworkMutationType::NumberOf);
    mutationRouletteEntries[(uint)NetworkMutationType::CreateNeuron] =
        config["mutation_roulette"]["create_neuron"].asUInt();
    mutationRouletteEntries[(uint)NetworkMutationType::CreateConnection] =
        config["mutation_roulette"]["create_connection"].asUInt();
    mutationRouletteEntries[(uint)NetworkMutationType::RemoveNeuron] =
        config["mutation_roulette"]["remove_neuron"].asUInt();
    mutationRouletteEntries[(uint)NetworkMutationType::RemoveConnection] =
        config["mutation_roulette"]["remove_connection"].asUInt();
    mutationRouletteEntries[(uint)NetworkMutationType::None] =
        config["mutation_roulette"]["none"].asUInt();
    m_mutationRoulette.setEntries(mutationRouletteEntries);
}

Incubator::~Incubator()
{
    for(auto network : m_networks)
    {
        delete network;
    }
}

void Incubator::makeNeuralNetworks(uint32 n)
{
    m_networks.reserve(m_networks.size() + n);

    for(uint i = 0; i < n; ++i)
    {
        NeuralNetwork* network = new NeuralNetwork();

        // Create input
        Neuron* i1 = network->addInput();

        // Create output
        Neuron* o1 = network->addOutput();

        i1->connect(o1, NeuralNetwork::getRandomWeight());

        m_networks.push_back(network);
        m_untestedNetworks.push_back(network);
    }
}

void Incubator::runTest()
{
    NeuralNetwork* exBestNetwork = m_bestNetwork;

    // Test untest networks.
    for(NeuralNetwork* network : m_untestedNetworks)
    {
        const float score = g_testFunction(network);
        const float exScore = 0.0f;

        network->setScore(score);

        NeuralNetwork* parent = network->getParent();
        if(parent)
        {
            const float parentScore = parent->getScore();
            if((m_bestScoreType == BestScoreType::Maximum && parentScore >= score) ||
                (m_bestScoreType == BestScoreType::Minimum && parentScore <= score))
            {
                parent->increaseStagnatedGenerations();
            }
            else
            {
                parent->resetStagnatedGenerations();
            }
        }
    }

    // Update statistics.
    double scoreAccum = 0.0;
    float bestScore = m_bestScoreType == BestScoreType::Minimum ? FLT_MAX : 0.0f;
    for(NeuralNetwork* network : m_networks)
    {
        const float score = network->getScore();
        scoreAccum += score;
        if(m_bestScoreType == BestScoreType::Maximum && score > bestScore
            || m_bestScoreType == BestScoreType::Minimum && score < bestScore)
        {
            bestScore = score;
            m_bestNetwork = network;
        }
    }

    m_avgScore = (float)(scoreAccum / m_networks.size());

    // Sort for elitism. Best networks are at the beginning of the vector.
    if(m_bestScoreType == BestScoreType::Maximum)
    {
        std::sort(m_networks.begin(), m_networks.end(), sortMaximum);
    }
    else
    {
        std::sort(m_networks.begin(), m_networks.end(), sortMinimum);
    }

    const uint32 nElite = (uint32)(m_networks.size() * m_elitismRate);

    double eliteScoreAccum = 0.0;
    for(uint i = 0; i < nElite; ++i)
    {
        eliteScoreAccum += m_networks[i]->getScore();
    }

    if(nElite > 0)
    {
        m_avgEliteScore = (float)(eliteScoreAccum / nElite);
    }
    else
    {
        m_avgEliteScore = m_bestScoreType == BestScoreType::Minimum ? FLT_MAX : 0.0f;
    }

    m_untestedNetworks.clear();
}

void Incubator::mutate()
{
    // Keep the elite.
    uint32 nElite = (uint32)(m_networks.size() * m_elitismRate);

    Vector<NeuralNetwork*> offspringNetworks;
    offspringNetworks.reserve(m_networks.size());

    for(uint i = 0; i < nElite; ++i)
    {
        offspringNetworks.push_back(m_networks[i]);
    }

    Vector<NeuralNetwork*> parents = m_networks;

    while(offspringNetworks.size() < m_networks.size())
    {
        float totalScore = 0.0f;
        float minimizedTotalScore = 0.0f;
        for(auto parent : parents)
        {
            totalScore += parent->getScore();
        }

        float maxRand = 0.0f;
        if(m_bestScoreType == BestScoreType::Minimum)
        {
            for(auto parent : parents)
            {
                minimizedTotalScore += totalScore - parent->getScore();
            }
            maxRand = minimizedTotalScore;
        }
        else
        {
            maxRand = totalScore;
        }

        const float rand = randr32(0.0f, maxRand);
        NeuralNetwork* choosenNetwork = nullptr;

        float scoreAccum = 0.0f;
        for(auto parentItr = parents.begin(); parentItr != parents.end(); ++parentItr)
        {
            if(m_bestScoreType == BestScoreType::Minimum)
            {
                scoreAccum += totalScore - (*parentItr)->getScore();
            }
            else
            {
                scoreAccum += (*parentItr)->getScore();
            }

            if(scoreAccum >= rand)
            {
                choosenNetwork = *parentItr;

                // Cannot re-use the parent again.
                parents.erase(parentItr);
                break;
            }
        }

        NeuralNetwork* offspring = choosenNetwork->copy();
        choosenNetwork->addChild(offspring);
        offspring->tryMutateAnyDentrite();

        NetworkMutationType mutationRoulette = m_mutationRoulette.roll();
        switch(mutationRoulette)
        {
            case NetworkMutationType::CreateNeuron:
            {
                if(offspring->tryCreateAnyNeuron())
                {
                    offspring->resetStepping();
                }
                break;
            }

            case NetworkMutationType::CreateConnection:
            {
                if(offspring->tryCreateAnyConnection())
                {
                    offspring->resetStepping();
                }
                break;
            }

            case NetworkMutationType::RemoveNeuron:
            {
                if(offspring->tryRemoveAnyNeuron())
                {
                    offspring->resetStepping();
                }
                break;
            }

            case NetworkMutationType::RemoveConnection:
            {
                if(offspring->tryRemoveAnyConnection())
                {
                    offspring->resetStepping();
                }
                break;
            }
        }

        offspringNetworks.push_back(offspring);
        m_untestedNetworks.push_back(offspring);
    }

    // Delete parents networks not kept for elitism.
    for(uint i = nElite; i < m_networks.size(); ++i)
    {
        // Make sure the best network is not part of it.
        // It most of the time isn't but if there are no elites, it can happen.
        if(m_bestNetwork == m_networks[i])
            m_bestNetwork = nullptr;

        delete m_networks[i];
    }

    m_networks = offspringNetworks;
}

bool Incubator::sortMaximum(NeuralNetwork* a, NeuralNetwork* b)
{
    return a->getScore() > b->getScore();
}

bool Incubator::sortMinimum(NeuralNetwork* a, NeuralNetwork* b)
{
    return a->getScore() < b->getScore();
}

float Incubator::getBestScore() const
{
    if(!m_bestNetwork)
        return m_bestScoreType == BestScoreType::Minimum ? FLT_MAX : 0.0f;

    return m_bestNetwork->getScore();
}

float Incubator::getAvgScore() const
{
    return m_avgScore;
}

float Incubator::getAvgEliteScore() const
{
    return m_avgEliteScore;
}

} // OE_Neural

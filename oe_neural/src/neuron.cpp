#include "neuron.h"

// External
#include <cmath>

// OE_Neural
#include "block.h"

namespace OE_Neural
{

float sigmoidTriggerFunction(float input)
{
    return 2.0f * ((1.0f / (1.0f + (float)pow(100.0, -input))) - 0.5f);
}

float linearTriggerFunction(float input)
{
    return input;
}

std::function<float(float)> g_triggerFunction = linearTriggerFunction;

Neuron::Neuron(uint32 id) :
m_id(id)
{
}

uint32 Neuron::getId() const
{
    return m_id;
}

bool Neuron::setInput(uint32 index, float input)
{
    if(input < -1.0f || input > 1.0f)
        return false;

    if(index >= m_dentrites.size())
        return false;

    m_dentrites[index].m_input = input;
    return true;
}

uint32 Neuron::addDentrite(float weight)
{
    OE_CHECK_MSG(m_dentrites.size() < MaxDentrites,
        OE_CHANNEL_NEURAL,
        "Cannot add more dentrites");

    Dentrite dentrite;
    dentrite.m_weight = weight;
    dentrite.m_isConnected = false;
    m_dentrites.push_back(dentrite);
    return (uint32)m_dentrites.size() - 1;
}

float Neuron::getDentriteWeight(uint32 index) const
{
    OE_CHECK_MSG(index < m_dentrites.size(),
        OE_CHANNEL_NEURAL,
        "Dentrite index out of bound");

    return m_dentrites[index].m_weight;
}

void Neuron::setDentriteWeight(uint32 index, float weight)
{
    OE_CHECK_MSG(index < m_dentrites.size(),
        OE_CHANNEL_NEURAL,
        "Dentrite index out of bound");

    m_dentrites[index].m_weight = weight;
}

uint32 Neuron::getNumTotalDentrites() const
{
    return uint32(m_dentrites.size());
}

uint32 Neuron::getNumConnectedDentrites() const
{
    uint32 nConnectedDentrites = 0;
    for(auto dentrite : m_dentrites)
    {
        if(dentrite.m_isConnected)
        {
            ++nConnectedDentrites;
        }
    }

    return nConnectedDentrites;
}

bool Neuron::isDentriteConnected(uint32 iDentrite) const
{
    OE_CHECK_MSG(iDentrite < m_dentrites.size(),
        OE_CHANNEL_NEURAL,
        "Dentrite index out of bound");

    return m_dentrites[iDentrite].m_isConnected;
}

float Neuron::getOutput() const
{
    return m_axon.m_output;
}

void Neuron::connect(Neuron* toNeuron, float weight)
{
    // Check if any dentrite is free.
    bool hasFoundDisconnectedDentrite = false;
    uint iDisconnectedDentrite = 0;
    for(uint i = 0; i < toNeuron->m_dentrites.size(); ++i)
    {
        if(!toNeuron->m_dentrites[i].m_isConnected)
        {
            hasFoundDisconnectedDentrite = true;
            iDisconnectedDentrite = i;
            break;
        }
    }

    Axon::Connection axonConnection;
    if(hasFoundDisconnectedDentrite)
    {
        axonConnection.m_index = iDisconnectedDentrite;
        toNeuron->m_dentrites[iDisconnectedDentrite].m_weight = weight;
        toNeuron->m_dentrites[iDisconnectedDentrite].m_isConnected = true;
    }
    else
    {
        axonConnection.m_index = toNeuron->addDentrite(weight);
        toNeuron->m_dentrites[axonConnection.m_index].m_isConnected = true;
    }
    
    axonConnection.m_neuron = toNeuron;
    m_axon.m_connections.push_back(axonConnection);
}

void Neuron::connect(Neuron* toNeuron, uint32 iDentrite)
{
    Axon::Connection axonConnection;
    axonConnection.m_neuron = toNeuron;
    axonConnection.m_index = iDentrite;
    m_axon.m_connections.push_back(axonConnection);

    if(iDentrite >= toNeuron->m_dentrites.size())
    {
        // Create unconnected dentrites until the iDentrite is reached.
        toNeuron->m_dentrites.reserve(iDentrite + 1);
        uint32 startIndex = uint32(toNeuron->m_dentrites.size());

        Dentrite dentrite;
        dentrite.m_input = 0.0f;
        dentrite.m_weight = 0.0f;

        for(uint32 i = startIndex; i <= iDentrite; ++i)
        {
            // Set last dentrite occupied.
            if(i == iDentrite)
            {
                dentrite.m_isConnected = true;
            }

            toNeuron->m_dentrites.push_back(dentrite);
        }
    }
    else
    {
        OE_CHECK(!toNeuron->m_dentrites[iDentrite].m_isConnected, OE_CHANNEL_NEURAL);
        toNeuron->m_dentrites[iDentrite].m_isConnected = true;
    }
}

void Neuron::disconnect(uint32 iConnection)
{
    OE_CHECK_MSG(iConnection < getNumConnections(),
        OE_CHANNEL_NEURAL,
        "Connection index out of bound");

    Axon::Connection& connection = m_axon.m_connections[iConnection];
    Neuron* connectedNeuron = connection.m_neuron;
    OE_CHECK(connectedNeuron->m_dentrites[connection.m_index].m_isConnected, OE_CHANNEL_NEURAL);
    connectedNeuron->m_dentrites[connection.m_index].m_isConnected = false;
    m_axon.m_connections.erase(m_axon.m_connections.begin() + iConnection);
}

void Neuron::disconnectAll()
{
    for(int iConnection = getNumConnections() - 1; iConnection >= 0; --iConnection)
    {
        Axon::Connection& connection = m_axon.m_connections[iConnection];
        disconnect(iConnection);
    }
}

void Neuron::getConnectedNeurons(Vector<Neuron*>& connectedNeurons)
{
    connectedNeurons.clear();
    connectedNeurons.reserve(m_axon.m_connections.size());
    for(auto connection : m_axon.m_connections)
    {
        connectedNeurons.push_back(connection.m_neuron);
    }
}

uint32 Neuron::getNumConnections() const
{
    return uint32(m_axon.m_connections.size());
}

const Neuron::Axon::Connection& Neuron::getConnectionIndex(uint32 iConnection) const
{
    OE_CHECK_MSG(iConnection < getNumConnections(),
        OE_CHANNEL_NEURAL,
        "Connection index out of bound");

    return m_axon.m_connections[iConnection];
}

const Neuron::Axon::Connection* Neuron::getConnectionToNeuronId(uint32 neuronId) const
{
    for(uint32 iConnection = 0; iConnection < m_axon.m_connections.size(); ++iConnection)
    {
        if(m_axon.m_connections[iConnection].m_neuron->getId() == neuronId)
        {
            return &m_axon.m_connections[iConnection];
        }
    }

    return nullptr;
}

float Neuron::update()
{
    float inputs = 0.0f;
    for (auto& dentrite : m_dentrites)
    {
        inputs += dentrite.m_input * dentrite.m_weight;
    }

    m_axon.m_output = g_triggerFunction(inputs);
    m_axon.feedForward();

    return m_axon.m_output;
}

void Neuron::Axon::feedForward()
{
    for(auto& connection : m_connections)
    {
        connection.m_neuron->setInput(connection.m_index, m_output);
    }
}

void Neuron::makeBlock(Block& block) const
{
    block.m_id = m_id;

    block.m_nDentrites = uint32(m_dentrites.size());
    for(uint32 iDentrite = 0; iDentrite < m_dentrites.size(); ++iDentrite)
    {
        block.m_weights[iDentrite] = m_dentrites[iDentrite].m_weight;
    }

    block.m_nConnections = uint32(m_axon.m_connections.size());
    for(uint32 iConnection = 0; iConnection < m_axon.m_connections.size(); ++iConnection)
    {
        block.m_connections[iConnection].m_id = m_axon.m_connections[iConnection].m_neuron->m_id;
        block.m_connections[iConnection].m_index = m_axon.m_connections[iConnection].m_index;
    }
}

} // namespace OE_Neural

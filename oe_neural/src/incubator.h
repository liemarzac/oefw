#ifndef __OE_NEURAL_INCUBATOR_H__
#define __OE_NEURAL_INCUBATOR_H__

#include "neural_private.h"

#include <functional>

namespace OE_Neural
{

class NeuralNetwork;

extern OE_NEURAL_EXPORT std::function<float(NeuralNetwork*)> g_testFunction;

class OE_NEURAL_EXPORT Incubator
{
public:
    enum class BestScoreType
    {
        Maximum,
        Minimum
    };

    enum class NetworkMutationType
    {
        CreateNeuron,
        CreateConnection,
        RemoveNeuron,
        RemoveConnection,
        None,
        NumberOf
    };

    Incubator(BestScoreType type);
    virtual ~Incubator();

    void makeNeuralNetworks(uint32 n);
    void runTest();
    void offspring();
    void mutate();
    float getBestScore() const;
    float getAvgScore() const;
    float getAvgEliteScore() const;

private:
    Incubator();
    Incubator(const Incubator& rhs);
    Incubator& operator=(const Incubator& rhs);
    static bool sortMinimum(NeuralNetwork* a, NeuralNetwork* b);
    static bool sortMaximum(NeuralNetwork* a, NeuralNetwork* b);
    Vector<NeuralNetwork*> m_networks;
    Vector<NeuralNetwork*> m_untestedNetworks;
    float m_elitismRate;
    float m_addNeuronRate;
    float m_removeNeuronRate;
    float m_addConnectionRate;
    float m_removeConnectionRate;
    float m_avgScore;
    float m_avgEliteScore;
    uint32 m_nGenerationWithoutImprovement;
    uint32 m_lastSteppingDecrease;
    BestScoreType m_bestScoreType;
    NeuralNetwork* m_bestNetwork;
    Roulette<NetworkMutationType> m_mutationRoulette;
};

} // OE_Neural

#endif // __OE_NEURAL_INCUBATOR_H__

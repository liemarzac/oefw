#include "neural_network.h"

#include "neural_private.h"

// External
#include <algorithm>
#include <json/json.h>

// OE_Core
#include <file.h>
#include <oe_math.h>
#include <rand.h>

// OE_Neural
#include "block.h"
#include "neuron.h"


namespace OE_Neural
{

float NeuralNetwork::m_config_step = 0.0f;

NeuralNetwork::NeuralNetwork() :
m_nextNeuronId(0)
,m_prevOutputId(-1)
,m_score(0.0f)
,m_nGenerations(0)
,m_nStagnatedGenerations(0)
,m_parent(nullptr)
{
    Json::Value config;
    bool result = appLoadJsonFile("configs" PATH_SEPARATOR "neural_network.json", config);

    Vector<uint32> dentriteRouletteEntries;
    dentriteRouletteEntries.resize((size_t)DentriteCrossTypes::NumberOf);
    dentriteRouletteEntries[(uint)DentriteCrossTypes::UseReference] =
        config["dentrite_roulette"]["use_reference"].asUInt();
    dentriteRouletteEntries[(uint)DentriteCrossTypes::UseOther] =
        config["dentrite_roulette"]["use_other"].asUInt();
    dentriteRouletteEntries[(uint)DentriteCrossTypes::Average] =
        config["dentrite_roulette"]["average"].asUInt();
    dentriteRouletteEntries[(uint)DentriteCrossTypes::Mutate] =
        config["dentrite_roulette"]["mutate"].asUInt();
    dentriteRouletteEntries[(uint)DentriteCrossTypes::Step] =
        config["dentrite_roulette"]["step"].asUInt();
    m_dentritesRoulette.setEntries(dentriteRouletteEntries);


    Vector<uint32> mutationRouletteEntries;
    mutationRouletteEntries.resize((size_t)DentriteMutationType::NumberOf);
    mutationRouletteEntries[(uint)DentriteMutationType::Step] =
        config["dentrite_mutation_roulette"]["step"].asUInt();
    mutationRouletteEntries[(uint)DentriteMutationType::Random] =
        config["dentrite_mutation_roulette"]["random"].asUInt();
    m_dentritesMutationRoulette.setEntries(mutationRouletteEntries);

    m_config_step = config["step"].asFloat();
    m_step = m_config_step;

    OE_CHECK(result, OE_CHANNEL_NEURAL);
}

NeuralNetwork::~NeuralNetwork()
{
    for(Vector<Neuron*>& group : m_neuronGroups)
    {
        for(Neuron* neuron : group)
        {
            delete neuron;
        }
    }

    if(m_parent)
        m_parent->removeChild(this);

    for(NeuralNetwork* child : m_children)
    {
        child->m_parent = nullptr;
    }
}

Neuron* NeuralNetwork::addInput()
{
    OE_CHECK_MSG(
        m_neuronGroups[(uint)NeuronType::Intermediate].size() == 0,
        OE_CHANNEL_NEURAL,
        "Cannot add input neurons when intermediates already exists");

    Neuron* neuron = new Neuron(m_nextNeuronId++);
    neuron->addDentrite(1.0f);
    m_neuronGroups[(uint)NeuronType::Input].push_back(neuron);
    return m_neuronGroups[(uint)NeuronType::Input].back();
}

Neuron* NeuralNetwork::addOutput()
{
    OE_CHECK_MSG(
        m_neuronGroups[(uint)NeuronType::Intermediate].size() == 0,
        OE_CHANNEL_NEURAL,
        "Cannot add output neurons when intermediates already exists");

    Neuron* neuron = new Neuron(m_prevOutputId--);
    m_neuronGroups[(uint)NeuronType::Output].push_back(neuron);
    return m_neuronGroups[(uint)NeuronType::Output].back();
}

Neuron* NeuralNetwork::addIntermediate()
{
    OE_CHECK_MSG(
        m_nextNeuronId <= m_prevOutputId,
        OE_CHANNEL_NEURAL,
        "Reached the maximum number of intermediate neurons");

    Neuron* neuron = new Neuron(m_nextNeuronId++);
    m_neuronGroups[(uint)NeuronType::Intermediate].push_back(neuron);
    return m_neuronGroups[(uint)NeuronType::Intermediate].back();
}

bool NeuralNetwork::setInput(uint32 index, float input)
{
    if(index >= m_neuronGroups[(uint)NeuronType::Input].size())
        return false;

    m_neuronGroups[(uint)NeuronType::Input][index]->setInput(0, input);
    return true;
}

float NeuralNetwork::getOutput(uint32 index) const
{
    if(index >= m_neuronGroups[(uint)NeuronType::Output].size())
        return 0.0f;

    return m_neuronGroups[(uint)NeuronType::Output][index]->getOutput();
}

void NeuralNetwork::update()
{
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        for(Neuron* neuron : m_neuronGroups[iGroup])
        {
            neuron->update();
        }
    }
}

const Vector<Neuron*>& NeuralNetwork::getNeurons(NeuronType type) const
{
    OE_CHECK((uint32)type < (uint32)NeuronType::NumberOf, OE_CHANNEL_NEURAL);
    return m_neuronGroups[(uint)type];
}

Neuron* NeuralNetwork::addNeuron(const Block& block)
{
    Neuron* neuron = new Neuron(block.m_id);
    m_neuronGroups[(uint32)block.m_type].push_back(neuron);
    sortNeurons();

    for(uint32 iDentrite = 0; iDentrite < block.m_nDentrites; ++iDentrite)
    {
        neuron->addDentrite(block.m_weights[iDentrite]);
    }

    return neuron;
}

void NeuralNetwork::connectNeuron(const Block& block)
{
    if(block.m_nConnections == 0)
        return;

    Neuron* neuron = getNeuron(block.m_id);
    OE_CHECK(neuron != nullptr, OE_CHANNEL_NEURAL);

    for(uint32 iConnection = 0; iConnection < block.m_nConnections; ++iConnection)
    {
        uint32 id = block.m_connections[iConnection].m_id;
        Neuron* connectedNeuron = getNeuron(block.m_connections[iConnection].m_id);
        OE_CHECK(connectedNeuron != nullptr, OE_CHANNEL_NEURAL);

        neuron->connect(connectedNeuron, block.m_connections[iConnection].m_index);
    }
}

Neuron* NeuralNetwork::getNeuron(uint32 id) const
{
    for(const Vector<Neuron*>& group : m_neuronGroups)
    {
        for(Neuron* neuron : group)
        {
            if(neuron->getId() == id)
                return neuron;
        }
    }

    return nullptr;
}

bool NeuralNetwork::tryMutateAnyDentrite()
{
    const uint32 nInputs = uint32(m_neuronGroups[(uint)NeuronType::Input].size());
    const uint32 nInterm = uint32(m_neuronGroups[(uint)NeuronType::Intermediate].size());
    const uint32 nOutputs = uint32(m_neuronGroups[(uint)NeuronType::Output].size());
    const uint32 nTotal = nInputs + nInterm + nOutputs;
    const uint32 iOutputs = nInputs + nInterm;

    const uint32 iNeuron = randu32(0, nTotal - 1);

    uint32 iNeuronInGroup;
    uint32 iNeuronType;
    if(iNeuron < nInputs)
    {
        iNeuronInGroup = iNeuron;
        iNeuronType = (uint32)NeuronType::Input;
    }
    else if(iNeuron < nInputs + nInterm)
    {
        iNeuronInGroup = iNeuron - nInputs;
        iNeuronType = (uint32)NeuronType::Intermediate;
    }
    else
    {
        iNeuronInGroup = iNeuron - (nInputs + nInterm);
        iNeuronType = (uint32)NeuronType::Output;
    }

    Neuron* neuron = m_neuronGroups[iNeuronType][iNeuronInGroup];

    uint32 iDentrite = 0;

    if(iNeuronType != (uint32)NeuronType::Input)
    {
        uint32 nConnectedDentrites = neuron->getNumConnectedDentrites();

        if(nConnectedDentrites == 0)
        {
            OE_CHECK(iNeuronType == (uint32)NeuronType::Output, OE_CHANNEL_NEURAL);
            return false;
        }

        uint32 iRandConnectedDentrite = randu32(0, nConnectedDentrites - 1);
        uint32 iConnectedDentrite = 0;

        for(; iDentrite < neuron->getNumTotalDentrites(); ++iDentrite)
        {
            if(neuron->isDentriteConnected(iDentrite))
            {
                if(iConnectedDentrite == iRandConnectedDentrite)
                {
                    break;
                }
                ++iConnectedDentrite;
            }
        }
    }

    DentriteMutationType roulette = m_dentritesMutationRoulette.roll();
    switch(roulette)
    {
        case DentriteMutationType::Step:
        {
            float weight = neuron->getDentriteWeight(iDentrite);
            bool increase = randu32(0, 1) == 1;
            if(increase)
            {
                weight = OE_Core::min(10.0f, weight + m_step);
            }
            else
            {
                weight = OE_Core::max(-10.0f, weight - m_step);
            }
            neuron->setDentriteWeight(iDentrite, weight);
            break;
        }

        case DentriteMutationType::Random:
        {
            float weight = randr32(-10.0f, 10.0f);
            neuron->setDentriteWeight(iDentrite, weight);
            break;
        }
    }

    return true;
}

void NeuralNetwork::cross(const NeuralNetwork* other)
{
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        for(uint iNeuron = 0; iNeuron < m_neuronGroups[iGroup].size(); ++iNeuron)
        {
            const Neuron* myNeuron = m_neuronGroups[iGroup][iNeuron];
            const Neuron* otherNeuron = other->getNeuron(myNeuron->getId());

            const uint32 nConnections = myNeuron->getNumConnections();
            for(uint iConnection = 0; iConnection < nConnections; ++iConnection)
            {
                const Neuron::Axon::Connection& myConnection =
                    myNeuron->getConnectionIndex(iConnection);

                Neuron* myConnectedNeuron =
                    myConnection.m_neuron;

                DentriteCrossTypes dentriteCrossType = m_dentritesRoulette.roll();

                bool isConnectionHandled = false;
                // Handle cases where the other neuron connection is unecessary.
                switch(dentriteCrossType)
                {
                    case DentriteCrossTypes::UseReference:
                    {
                        // Does not change anything.
                        isConnectionHandled = true;
                        break;
                    }

                    case DentriteCrossTypes::Mutate:
                    {
                        myConnectedNeuron->setDentriteWeight(
                            myConnection.m_index,
                            randr32(-10.0f, 10.0f));
                        isConnectionHandled = true;
                        break;
                    }

                    case DentriteCrossTypes::Step:
                    {
                        float myWeight =
                            myConnectedNeuron->getDentriteWeight(myConnection.m_index);

                        bool increase = randu32(0, 1) == 1;
                        if(increase)
                        {
                            myWeight = OE_Core::min(10.0f, myWeight + m_step);
                        }
                        else
                        {
                            myWeight = OE_Core::max(-10.0f, myWeight - m_step);
                        }

                        myConnectedNeuron->setDentriteWeight(
                            myConnection.m_index,
                            myWeight);

                        isConnectionHandled = true;
                        break;
                    }
                }

                if(isConnectionHandled)
                    break;

                // Find same connection on other.
                const Neuron::Axon::Connection* otherConnection = nullptr;

                if(otherNeuron)
                {
                    otherConnection = otherNeuron->getConnectionToNeuronId(
                        myConnectedNeuron->getId());
                }

                if(!otherConnection)
                    break;

                const Neuron* otherConnectedNeuron = otherConnection->m_neuron;

                switch(dentriteCrossType)
                {
                    case DentriteCrossTypes::UseOther:
                    {
                        const float otherWeight =
                            otherConnectedNeuron->getDentriteWeight(otherConnection->m_index);

                        myConnectedNeuron->setDentriteWeight(
                            myConnection.m_index,
                            otherWeight);
                        break;
                    }

                    case DentriteCrossTypes::Average:
                    {
                        const float myWeight =
                            myConnectedNeuron->getDentriteWeight(myConnection.m_index);

                        const float otherWeight =
                            otherConnectedNeuron->getDentriteWeight(otherConnection->m_index);

                        const float avgWeight = (myWeight + otherWeight) / 2.0f;
                        myConnectedNeuron->setDentriteWeight(
                            myConnection.m_index,
                            avgWeight);
                        break;
                    }
                }
            }
        }
    }

    CHECK_SANITY
}

NeuralNetwork* NeuralNetwork::copy() const
{
    NeuralNetwork* copied = new NeuralNetwork();

    copied->m_nextNeuronId = m_nextNeuronId;
    copied->m_prevOutputId = m_prevOutputId;
    copied->m_step = m_step;
    copied->m_nGenerations = m_nGenerations;

    // Create neurons.
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        copied->m_neuronGroups[iGroup].reserve(m_neuronGroups[iGroup].size());
        for(uint iNeuron = 0; iNeuron < m_neuronGroups[iGroup].size(); ++iNeuron)
        {
            const Neuron* originalNeuron = m_neuronGroups[iGroup][iNeuron];
            Neuron* copiedNeuron = new Neuron(originalNeuron->getId());

            const uint32 nDentrites = originalNeuron->getNumTotalDentrites();
            for(uint iDentrite = 0; iDentrite < nDentrites; ++iDentrite)
            {
                copiedNeuron->addDentrite(originalNeuron->getDentriteWeight(iDentrite));
            }

            copied->m_neuronGroups[iGroup].push_back(copiedNeuron);
        }
    }

    // Create connections.
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        for(uint iNeuron = 0; iNeuron < m_neuronGroups[iGroup].size(); ++iNeuron)
        {
            const Neuron* originalNeuron = m_neuronGroups[iGroup][iNeuron];
            Neuron* copiedNeuron = copied->getNeuron(originalNeuron->getId());

            const uint32 nConnections = originalNeuron->getNumConnections();
            for(uint iConnection = 0; iConnection < nConnections; ++iConnection)
            {
                const Neuron::Axon::Connection& connection =
                    originalNeuron->getConnectionIndex(iConnection);

                Neuron* connectedNeuron = copied->getNeuron(connection.m_neuron->getId());
                copiedNeuron->connect(connectedNeuron, connection.m_index);
            }
        }
    }

    CHECK_SANITY

    return copied;
}

uint32 NeuralNetwork::getNumGenerations() const
{
    return m_nGenerations;
}

void NeuralNetwork::increaseGeneration()
{
    ++m_nGenerations;
}

uint32 NeuralNetwork::getNumStagnatedGenerations() const
{
    return m_nStagnatedGenerations;
}

void NeuralNetwork::resetStagnatedGenerations()
{
    m_nStagnatedGenerations = 0;
}

void NeuralNetwork::increaseStagnatedGenerations()
{
    ++m_nStagnatedGenerations;
    if((m_nStagnatedGenerations % 100) == 0)
    {
        decreaseStepping();
    }
}

bool NeuralNetwork::tryCreateAnyConnection()
{
    const uint32 nInputs = uint32(m_neuronGroups[(uint)NeuronType::Input].size());
    const uint32 nInterm = uint32(m_neuronGroups[(uint)NeuronType::Intermediate].size());
    const uint32 nOutputs = uint32(m_neuronGroups[(uint)NeuronType::Output].size());
    const uint32 nTotal = nInputs + nInterm + nOutputs;
    const uint32 iOutputs = nInputs + nInterm;

    // The "from" neuron can be any except outputs.
    const uint32 iNeuronFrom = randu32(0, iOutputs - 1);

    // Get the "from" neuron from the correct group.
    Neuron* from = nullptr;
    if(iNeuronFrom < nInputs)
    {
        from = m_neuronGroups[(uint)NeuronType::Input][iNeuronFrom];
    }
    else
    {
        from = m_neuronGroups[(uint)NeuronType::Intermediate][iNeuronFrom - nInputs];
    }

    // The "to" neuron index has to be at least greater than "from" index.
    const uint32 iNeuronsToStart = iNeuronFrom + 1;
    const uint32 iNeuronTo = randu32(iNeuronsToStart, nTotal - 1);

    // Get the "to" neuron from the correct group.
    Neuron* to = nullptr;
    if(iNeuronTo < iOutputs)
    {
        to = m_neuronGroups[(uint)NeuronType::Intermediate][iNeuronTo - nInputs];
    }
    else
    {
        to = m_neuronGroups[(uint)NeuronType::Output][iNeuronTo - (nInputs + nInterm)];
    }

    Vector<Neuron*> connectedNeurons;
    from->getConnectedNeurons(connectedNeurons);
    auto itr = std::find(connectedNeurons.begin(), connectedNeurons.end(), to);
    if(itr != connectedNeurons.end())
    {
        // Already connected to that neuron.
        return false;
    }

    if(to->getNumConnectedDentrites() == MaxDentrites)
    {
        // All dentrites are in use.
        return false;
    }

    // Finally create the connection with any weight.
    from->connect(to, 0.0f);

    CHECK_SANITY

    return true;
}

bool NeuralNetwork::tryCreateAnyNeuron()
{
    const uint32 nInputs = uint(m_neuronGroups[(uint)NeuronType::Input].size());
    const uint32 nInterm = uint32(m_neuronGroups[(uint)NeuronType::Intermediate].size());

    // Find a free index.
    Vector<uint32> usedIndices;
    usedIndices.reserve(nInterm);

    for(const Neuron* neuron : m_neuronGroups[(uint)NeuronType::Intermediate])
    {
        usedIndices.push_back(neuron->getId());
    }

    // The first potential free index is the one after the last one used by input neurons.
    uint32 iPotential = nInputs;

    while(true)
    {
        bool found = true;

        // Check max index reached.
        if(iPotential == nInputs + nInterm)
            break;

        // Reached the max number of intermediates.
        if(iPotential == m_prevOutputId + 1)
            return false;

        for(uint32 i : usedIndices)
        {
            if(i == iPotential)
            {
                // Index in use.
                found = false;
                break;
            }
        }

        if(found)
            break;

        ++iPotential;
    }

    // Create neuron.
    Neuron* newNeuron = new Neuron(iPotential);

    // Connect the neuron to the network.
    Vector<Neuron*> potentialPreNeurons;
    Vector<Neuron*> potentialPostNeurons;
    potentialPreNeurons.reserve(getNumNeurons());
    potentialPostNeurons.reserve(getNumNeurons());

    for(Neuron* neuron : m_neuronGroups[(uint)NeuronType::Input])
    {
        potentialPreNeurons.push_back(neuron);
    }

    for(Neuron* neuron : m_neuronGroups[(uint)NeuronType::Intermediate])
    {
        if(neuron->getId() < newNeuron->getId())
        {
            potentialPreNeurons.push_back(neuron);
        }
        else
        {
            potentialPostNeurons.push_back(neuron);
        }
    }

    for(Neuron* neuron : m_neuronGroups[(uint)NeuronType::Output])
    {
        potentialPostNeurons.push_back(neuron);
    }

    const uint32 iPreNeuron = randu32(0, uint32(potentialPreNeurons.size()) - 1);
    const uint32 iPostNeuron = randu32(0, uint32(potentialPostNeurons.size()) - 1);

    if(potentialPostNeurons[iPostNeuron]->getNumConnectedDentrites() == MaxDentrites)
    {
        delete newNeuron;
        return false;
    }

    potentialPreNeurons[iPreNeuron]->connect(newNeuron, 0.0f);

    m_neuronGroups[(uint)NeuronType::Intermediate].push_back(newNeuron);
    sortNeurons();

    newNeuron->connect(potentialPostNeurons[iPostNeuron], 0.0f);

    CHECK_SANITY

    return true;
}

bool NeuralNetwork::tryRemoveAnyConnection()
{
    // Find any input or intermediate neuron.
    const uint32 nNeurons =
        uint32(m_neuronGroups[(uint)NeuronType::Input].size() +
        m_neuronGroups[(uint)NeuronType::Intermediate].size());

    uint32 iNeuronFrom = randu32(0, nNeurons - 1);

    NeuronType neuronFromType = iNeuronFrom < m_neuronGroups[(uint)NeuronType::Input].size() ?
        NeuronType::Input : NeuronType::Intermediate;

    if(neuronFromType == NeuronType::Intermediate)
    {
        iNeuronFrom -= uint32(m_neuronGroups[(uint)NeuronType::Input].size());
    }

    Neuron* neuron = m_neuronGroups[(uint)neuronFromType][iNeuronFrom];

    if(neuronFromType == NeuronType::Input && neuron->getNumConnections() < 2)
    {
        // Cannot completely disconnect input neurons.
        return false;
    }

    // Find any connection.
    uint32 iConnection = randu32(0, neuron->getNumConnections() - 1);

    Neuron* nextNeuron = neuron->getConnectionIndex(iConnection).m_neuron;
    const uint32 connectedNeuronId = nextNeuron->getId();

    // Remove neuron if its axon does not connect any other neurons.
    if(neuron->getNumConnections() == 1 &&
        getNeuronType(neuron) == NeuronType::Intermediate)
    {
        // It was the unique connection of that neuron so it has to be removed.
        removeNeuron(neuron);

        // Next neuron may have been deleted by remove the neuron.
        nextNeuron = nullptr;
    }
    else
    {
        // Simply disconnect and the neuron can remain.
        neuron->disconnect(iConnection);
    }

    if(!nextNeuron)
    {
        nextNeuron = getNeuron(connectedNeuronId);
    }

    // Remove right neuron neuron if all its dentrites are disconnected as well.
    if(nextNeuron &&
        nextNeuron->getNumConnectedDentrites() == 0 &&
        getNeuronType(nextNeuron) == NeuronType::Intermediate)
    {
        removeNeuron(nextNeuron);
    }

    CHECK_SANITY

    return true;
}

bool NeuralNetwork::tryRemoveAnyNeuron()
{
    // Only intermediates neurons can be removed.
    if(m_neuronGroups[(uint32)NeuronType::Intermediate].size() == 0)
        return false;

    uint32 iNeuron = randu32(
        0,
        uint32(m_neuronGroups[(uint32)NeuronType::Intermediate].size()) - 1);

    removeNeuron(m_neuronGroups[(uint32)NeuronType::Intermediate][iNeuron]);

    CHECK_SANITY

    return true;
}

void NeuralNetwork::removeNeuron(Neuron* neuron)
{
    OE_CHECK_MSG(
        getNeuronType(neuron) == NeuronType::Intermediate,
        OE_CHANNEL_NEURAL,
        "Only intermediate neurons can be removed."
    );

    removeLeft(neuron);
    removeRight(neuron);

    deleteIsolatedNeurons();

    CHECK_SANITY
}

void NeuralNetwork::removeLeft(Neuron* neuron)
{
    struct LeftConnection
    {
        Neuron* m_neuron;
        uint32 m_iConnection;
    };

    // Find all left connecting to it.
    Vector<LeftConnection> leftConnections;

    for(uint iGroup = 0; iGroup <= (uint)NeuronType::Intermediate; ++iGroup)
    {
        for(uint iNeuron = 0; iNeuron < m_neuronGroups[iGroup].size(); ++iNeuron)
        {
            Neuron* fromNeuron = m_neuronGroups[iGroup][iNeuron];

            if(fromNeuron->getId() >= neuron->getId())
            {
                continue;
            }

            const uint32 nConnections = fromNeuron->getNumConnections();
            for(uint iConnection = 0; iConnection < nConnections; ++iConnection)
            {
                const Neuron::Axon::Connection& connection =
                    fromNeuron->getConnectionIndex(iConnection);

                if(connection.m_neuron == neuron)
                {
                    LeftConnection leftConnection;
                    leftConnection.m_neuron = fromNeuron;
                    leftConnection.m_iConnection = iConnection;
                    leftConnections.push_back(leftConnection);
                }
            }
        }
    }

    for(LeftConnection& leftConnection : leftConnections)
    {
        leftConnection.m_neuron->disconnect(leftConnection.m_iConnection);

        if(leftConnection.m_neuron->getNumConnections() == 0 &&
            getNeuronType(leftConnection.m_neuron) == NeuronType::Intermediate)
        {
            removeLeft(leftConnection.m_neuron);
        }
    }
}

void NeuralNetwork::removeRight(Neuron* neuron)
{
    const uint32 nConnections = neuron->getNumConnections();
    for(int iConnection = nConnections - 1; iConnection >= 0; --iConnection)
    {
        const Neuron::Axon::Connection& connection =
            neuron->getConnectionIndex(iConnection);

        Neuron* rightNeuron = connection.m_neuron;

        neuron->disconnect(iConnection);

        if(rightNeuron->getNumConnectedDentrites() == 0 &&
            getNeuronType(rightNeuron) == NeuronType::Intermediate)
        {
            removeRight(rightNeuron);
        }
    }
}

void NeuralNetwork::deleteIsolatedNeurons()
{
    const uint iGroup = (uint)NeuronType::Intermediate;
    for(int iNeuron = int(m_neuronGroups[iGroup].size()) - 1; iNeuron >= 0; --iNeuron)
    {
        Neuron* neuron = m_neuronGroups[iGroup][iNeuron];

        if(neuron->getNumConnections() == 0 && neuron->getNumConnectedDentrites() == 0)
        {
            m_neuronGroups[iGroup].erase(m_neuronGroups[iGroup].begin() + iNeuron);
            delete neuron;
        }
    }

    CHECK_SANITY
}


uint32 NeuralNetwork::getNumNeurons() const
{
    uint32 nNeurons = 0;
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        nNeurons += uint32(m_neuronGroups[iGroup].size());
    }

    return nNeurons;
}

NeuronType NeuralNetwork::getNeuronType(Neuron* neuron) const
{
    if(neuron->getId() < m_neuronGroups[(uint)NeuronType::Input].size())
    {
        return NeuronType::Input;
    }
    else if(neuron->getId() <= m_prevOutputId)
    {
        return NeuronType::Intermediate;
    }
    else
    {
        return NeuronType::Output;
    }
}

float NeuralNetwork::getScore() const
{
    return m_score;
}

void NeuralNetwork::setScore(float score)
{
    m_score = score;
}

float NeuralNetwork::getRandomWeight()
{
    return randr32(-10.0f, 10.0f);
}

void NeuralNetwork::sortNeurons()
{
    std::sort(m_neuronGroups[(uint32)NeuronType::Intermediate].begin(),
        m_neuronGroups[(uint32)NeuronType::Intermediate].end(), sort);
}

bool NeuralNetwork::sort(Neuron* a, Neuron* b)
{
    return a->getId() < b->getId();
}

void NeuralNetwork::decreaseStepping()
{
    m_step = max(0.000001f, m_step * 0.5f);
}

void NeuralNetwork::resetStepping()
{
    m_step = m_config_step;
}

void NeuralNetwork::addChild(NeuralNetwork* child)
{
    m_children.push_back(child);
    child->m_parent = this;
}

void NeuralNetwork::removeChild(NeuralNetwork* child)
{
    removeUnordered(m_children, child);
}

NeuralNetwork* NeuralNetwork::getParent() const
{
    return m_parent;
}

const Vector<NeuralNetwork*>& NeuralNetwork::getChildren() const
{
    return m_children;
}

#ifdef SANITY_CHECKS
bool NeuralNetwork::checkSanity() const
{
    for(uint iGroup = 0; iGroup < (uint)NeuronType::NumberOf; ++iGroup)
    {
        for(Neuron* neuron : m_neuronGroups[iGroup])
        {
            Vector<Neuron*> connected;
            neuron->getConnectedNeurons(connected);

            if(iGroup == (uint32)NeuronType::Intermediate && connected.size() == 0)
            {
                OE_CHECK(false, OE_CHANNEL_NEURAL);
                return false;
            }

            for(Neuron* connectedNeuron : connected)
            {
                if(connectedNeuron->getId() <= neuron->getId())
                {
                    OE_CHECK(false, OE_CHANNEL_NEURAL);
                    return false;
                }
            }
        }
    }

    return true;
}
#endif // SANITY_CHECKS

} // namespace OE_Neural


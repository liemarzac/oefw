#ifndef __OE_NEURAL_NEURAL_NETWORK_H__
#define __OE_NEURAL_NEURAL_NETWORK_H__

#include "neural_private.h"

namespace OE_Neural
{

class Neuron;
struct Block;

enum class DentriteCrossTypes
{
    UseReference,
    UseOther,
    Average,
    Mutate,
    Step,
    NumberOf
};

enum class DentriteMutationType
{
    Step,
    Random,
    NumberOf
};

class OE_NEURAL_EXPORT NeuralNetwork
{
public:
    NeuralNetwork();
    ~NeuralNetwork();
    Neuron* addInput();
    Neuron* addOutput();
    Neuron* addIntermediate();
    bool setInput(uint32 index, float input);
    float getOutput(uint32 index) const;
    void update();
    Neuron* addNeuron(const Block& block);
    void connectNeuron(const Block& block);
    Neuron* getNeuron(uint32 id) const;
    void cross(const NeuralNetwork* other);
    NeuralNetwork* copy() const;
    const Vector<Neuron*>& getNeurons(NeuronType type) const;
    NeuronType getNeuronType(Neuron* neuron) const;
    bool tryMutateAnyDentrite();
    bool tryRemoveAnyConnection();
    bool tryRemoveAnyNeuron();
    bool tryCreateAnyConnection();
    bool tryCreateAnyNeuron();
    void removeNeuron(Neuron* neuron);
    uint32 getNumNeurons() const;
    float getScore() const;
    void setScore(float score);
    void resetStepping();
    void decreaseStepping();
    void increaseGeneration();
    uint32 getNumGenerations() const;
    uint32 getNumStagnatedGenerations() const;
    void resetStagnatedGenerations();
    void increaseStagnatedGenerations();
    void addChild(NeuralNetwork* network);
    void removeChild(NeuralNetwork* network);
    NeuralNetwork* getParent() const;
    const Vector<NeuralNetwork*>& getChildren() const;
    static float getRandomWeight();

private:
    NeuralNetwork(const NeuralNetwork& rhs);
    NeuralNetwork& operator=(const NeuralNetwork& rhs);

    void removeLeft(Neuron* from);
    void removeRight(Neuron* from);
    void deleteIsolatedNeurons();
    void sortNeurons();

#ifdef SANITY_CHECKS
    bool checkSanity() const;
#endif

    static bool sort(Neuron* a, Neuron* b);

    Vector<Neuron*> m_neuronGroups[(uint32)NeuronType::NumberOf];
    uint32 m_nextNeuronId;
    uint32 m_prevOutputId;
    float m_score;
    static float m_config_step;
    float m_step;
    uint32 m_nGenerations;
    uint32 m_nStagnatedGenerations;
    NeuralNetwork* m_parent;
    Vector<NeuralNetwork*> m_children;

    Roulette<DentriteCrossTypes> m_dentritesRoulette;
    Roulette<DentriteMutationType> m_dentritesMutationRoulette;
};

}

#endif // __OE_NEURAL_NEURAL_NETWORK_H__

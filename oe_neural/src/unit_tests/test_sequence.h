#include "../neural_network.h"
#include "../neuron.h"
#include "../sequence.h"

using namespace OE_Neural;

SUITE(SequenceTestSuite)
{
    TEST(SimpleSequence)
    {
        SharedPtr<NeuralNetwork> network(new NeuralNetwork());

        // Make a simple network with only 1 input and 1 output.
        Neuron* input = network->addInput();
        Neuron* output = network->addOutput();
        input->connect(output, 0.5f);
        const float InputSignal = 0.2f;
        network->setInput(0, InputSignal);
        network->update();
        float outputVal = network->getOutput(0);

        // Generate a sequence from the network.
        Sequence sequence;
        sequence.generateFrom(network);

        // Make a network from the sequence.
        SharedPtr<NeuralNetwork> cloneNetwork = sequence.makeNetwork();
        cloneNetwork->setInput(0, InputSignal);
        cloneNetwork->update();
        float cloneOutputVal = cloneNetwork->getOutput(0);

        CHECK_EQUAL(outputVal, cloneOutputVal);
    }

    TEST(Intermediate1Sequence)
    {
        SharedPtr<NeuralNetwork> network(new NeuralNetwork());

        // Make a network with only 1 input, 1 intermediate and 1 output.
        Neuron* input = network->addInput();
        Neuron* output = network->addOutput();
        Neuron* intermediate = network->addIntermediate();        
        input->connect(intermediate, 0.6f);
        intermediate->connect(output, 0.7f);
        const float InputSignal = 0.3f;
        network->setInput(0, InputSignal);
        network->update();
        float outputVal = network->getOutput(0);

        // Generate a sequence from the network.
        Sequence sequence;
        sequence.generateFrom(network);

        // Make a network from the sequence.
        SharedPtr<NeuralNetwork> cloneNetwork = sequence.makeNetwork();
        cloneNetwork->setInput(0, InputSignal);
        cloneNetwork->update();
        float cloneOutputVal = cloneNetwork->getOutput(0);

        CHECK_EQUAL(outputVal, cloneOutputVal);
    }

    TEST(ChainedIntermediateSequence)
    {
        SharedPtr<NeuralNetwork> network(new NeuralNetwork());

        // Make a simple network with 1 input, 2 intermediates and 1 output.
        Neuron* input = network->addInput();
        Neuron* output = network->addOutput();
        Neuron* intermediate1 = network->addIntermediate();
        Neuron* intermediate2 = network->addIntermediate();
        input->connect(intermediate1, 0.6f);
        intermediate1->connect(intermediate2, 0.7f);
        intermediate2->connect(output, -9.3f);
        const float InputSignal = -0.7f;
        network->setInput(0, InputSignal);
        network->update();
        float outputVal = network->getOutput(0);

        // Generate a sequence from the network.
        Sequence sequence;
        sequence.generateFrom(network);

        // Make a network from the sequence.
        SharedPtr<NeuralNetwork> cloneNetwork = sequence.makeNetwork();
        cloneNetwork->setInput(0, InputSignal);
        cloneNetwork->update();
        float cloneOutputVal = cloneNetwork->getOutput(0);

        CHECK_EQUAL(outputVal, cloneOutputVal);
    }

    TEST(MultipleInputsSequence)
    {
        SharedPtr<NeuralNetwork> network(new NeuralNetwork());

        // Make the original network.
        Neuron* input1 = network->addInput();
        Neuron* input2 = network->addInput();
        Neuron* output = network->addOutput();
        Neuron* intermediate1 = network->addIntermediate();
        Neuron* intermediate2 = network->addIntermediate();        
        input1->connect(intermediate1, -1.2f);
        input1->connect(intermediate2, 0.5f);
        input2->connect(intermediate1, 8.3f);
        input2->connect(intermediate2, 2.4f);
        intermediate1->connect(output, 3.1f);
        intermediate2->connect(output, -1.7f);
        const float InputSignal1 = 0.9f;
        const float InputSignal2 = -0.3f;
        network->setInput(0, InputSignal1);
        network->setInput(1, InputSignal2);
        network->update();
        float outputVal = network->getOutput(0);

        // Generate a sequence from the network.
        Sequence sequence;
        sequence.generateFrom(network);

        // Make a cloned network from the sequence.
        SharedPtr<NeuralNetwork> cloneNetwork = sequence.makeNetwork();
        cloneNetwork->setInput(0, InputSignal1);
        cloneNetwork->setInput(1, InputSignal2);
        cloneNetwork->update();
        float cloneOutputVal = cloneNetwork->getOutput(0);

        CHECK_EQUAL(outputVal, cloneOutputVal);
    }

    TEST(MultipleOutputsSequence)
    {
        SharedPtr<NeuralNetwork> network(new NeuralNetwork());

        Neuron* input1 = network->addInput();
        Neuron* input2 = network->addInput();
        Neuron* output1 = network->addOutput();
        Neuron* output2 = network->addOutput();
        Neuron* intermediate1 = network->addIntermediate();
        Neuron* intermediate2 = network->addIntermediate();
        input1->connect(intermediate1, -1.2f);
        input1->connect(intermediate2, 3.5f);
        input2->connect(intermediate1, 0.3f);
        input2->connect(intermediate2, 0.1f);
        intermediate1->connect(output1, 1.1f);
        intermediate1->connect(output2, 0.1f);
        intermediate2->connect(output1, -0.5f);
        intermediate2->connect(output2, -2.7f);
        const float InputSignal1 = -0.7f;
        const float InputSignal2 = -1.3f;
        network->setInput(0, InputSignal1);
        network->setInput(1, InputSignal2);
        network->update();
        float outputSignal1 = network->getOutput(0);
        float outputSignal2 = network->getOutput(1);

        // Generate a sequence from the network.
        Sequence sequence;
        sequence.generateFrom(network);

        // Make a network from the sequence.
        SharedPtr<NeuralNetwork> cloneNetwork = sequence.makeNetwork();
        cloneNetwork->setInput(0, InputSignal1);
        cloneNetwork->setInput(1, InputSignal2);
        cloneNetwork->update();
        float cloneOutputSignal1 = cloneNetwork->getOutput(0);
        float cloneOutputSignal2 = cloneNetwork->getOutput(1);

        CHECK_EQUAL(outputSignal1, cloneOutputSignal1);
        CHECK_EQUAL(outputSignal2, cloneOutputSignal2);
    }
}


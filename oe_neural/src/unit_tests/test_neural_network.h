#include "../neural_network.h"

using namespace OE_Neural;

SUITE(NeuralNetworkTestSuite)
{
    static float Tolerance = 0.00001f;

    const float RandWeightMin = -1.0f;
    const float RandWeightMax = 1.0f;
    const float RandInputMin = -1.0f;
    const float RandInputMax = 1.0f;

    float randWeight()
    {
        return randr32(RandWeightMin, RandWeightMax);
    }

    float randInput()
    {
        return randr32(RandInputMin, RandInputMax);
    }

    TEST(OneInputOneOutput)
    {
        NeuralNetwork network;

        // Create neurons.
        Neuron* inputNeuron = network.addInput();
        Neuron* outputNeuron = network.addOutput();

        // Connect input to output with a weight.
        float weight = 1.5f;
        inputNeuron->connect(outputNeuron, weight);

        // Set input on input neuron dentrite.
        float input = 0.3f;
        network.setInput(0, input);

        // Calculate expected value on input and output neurons axons.
        float inputNeuronAxonVal = OE_Neural::g_triggerFunction(input);
        float outputNeuronAxonVal = OE_Neural::g_triggerFunction(inputNeuronAxonVal * weight);

        // Update the network.
        network.update();

        CHECK_CLOSE(outputNeuronAxonVal, network.getOutput(0), Tolerance);
    }


    TEST(TwoInputsTwoOutputs)
    {
        NeuralNetwork network;

        // Create input/output neurons.
        Neuron* iN1 = network.addInput(); // InputNeuron 1
        Neuron* iN2 = network.addInput();
        Neuron* oN1 = network.addOutput(); // OutputNeuron 1
        Neuron* oN2 = network.addOutput();

        // Connect each input neurons to both output neurons.
        float wd1o1 = -5.0f; // Weight Dentrite 1 OutputNeuron 1
        float wd2o1 = 0.3f;
        float wd1o2 = 3.0f;
        float wd2o2 = -0.8f;

        iN1->connect(oN1, wd1o1);
        iN1->connect(oN2, wd1o2);
        iN2->connect(oN1, wd2o1);
        iN2->connect(oN2, wd2o2);

        float d1 = 0.1f; // Dentrite InputNeuron 1
        float d2 = -0.6f;
        CHECK(network.setInput(0, d1));
        CHECK(network.setInput(1, d2));

        // Calculate expected values on input neurons axons.
        float i1a = OE_Neural::g_triggerFunction(d1); // InputNeuron 1 Axon
        float i2a = OE_Neural::g_triggerFunction(d2);

        // Calculate expected values on output neurons axons.
        float o1a = OE_Neural::g_triggerFunction((i1a * wd1o1) + (i2a * wd2o1));
        float o2a = OE_Neural::g_triggerFunction((i1a * wd1o2) + (i2a * wd2o2));

        // Update the network.
        network.update();

        CHECK_CLOSE(o1a, network.getOutput(0), Tolerance);
        CHECK_CLOSE(o2a, network.getOutput(1), Tolerance);
    }

    TEST(TwoInputsTwoIntermediatesTwoOutputs)
    {
        NeuralNetwork network;

        // Create input/output neurons.
        Neuron* iN1 = network.addInput(); // InputNeuron 1
        Neuron* iN2 = network.addInput();
        Neuron* mN1 = network.addInput(); // MiddleNeuron 1
        Neuron* mN2 = network.addInput();
        Neuron* oN1 = network.addOutput(); // OutputNeuron 1
        Neuron* oN2 = network.addOutput();

        srand(1);

        float w1 = randWeight();
        float w2 = randWeight();
        float w3 = randWeight();
        float w4 = randWeight();
        float w5 = randWeight();
        float w6 = randWeight();
        float w7 = randWeight();
        float w8 = randWeight();

        iN1->connect(mN1, w1);
        iN1->connect(mN2, w2);
        iN2->connect(mN1, w3);
        iN2->connect(mN2, w4);
        mN1->connect(oN1, w5);
        mN1->connect(oN2, w6);
        mN2->connect(oN1, w7);
        mN2->connect(oN2, w8);

        float si1 = randInput(); // Signal Input 1
        float si2 = randInput();
        CHECK(network.setInput(0, si1));
        CHECK(network.setInput(1, si2));

        // Calculate expected values on input neurons axons.
        float sai1 = OE_Neural::g_triggerFunction(si1); // Signal Input Axon 1
        float sai2 = OE_Neural::g_triggerFunction(si2);

        // Calculate expected values on middle neurons axons.
        float sam1 = OE_Neural::g_triggerFunction((sai1 * w1) + (sai2 * w3)); // Signal Middle Axon 1
        float sam2 = OE_Neural::g_triggerFunction((sai1 * w2) + (sai2 * w4));

        // Calculate expected values on output neurons axons.
        float sao1 = OE_Neural::g_triggerFunction((sam1 * w5) + (sam2 * w7)); // Signal Output Axon 1
        float sao2 = OE_Neural::g_triggerFunction((sam1 * w6) + (sam2 * w8));

        // Update the network.
        network.update();

        CHECK_CLOSE(sao1, network.getOutput(0), Tolerance);
        CHECK_CLOSE(sao2, network.getOutput(1), Tolerance);
    }

    TEST(Copy)
    {
        NeuralNetwork network;

        // Create input/output neurons.
        Neuron* iN1 = network.addInput(); // InputNeuron 1
        Neuron* iN2 = network.addInput();
        Neuron* mN1 = network.addInput(); // MiddleNeuron 1
        Neuron* mN2 = network.addInput();
        Neuron* oN1 = network.addOutput(); // OutputNeuron 1
        Neuron* oN2 = network.addOutput();

        srand(1);

        float w1 = randWeight();
        float w2 = randWeight();
        float w3 = randWeight();
        float w4 = randWeight();
        float w5 = randWeight();
        float w6 = randWeight();
        float w7 = randWeight();
        float w8 = randWeight();

        iN1->connect(mN1, w1);
        iN1->connect(mN2, w2);
        iN2->connect(mN1, w3);
        iN2->connect(mN2, w4);
        mN1->connect(oN1, w5);
        mN1->connect(oN2, w6);
        mN2->connect(oN1, w7);
        mN2->connect(oN2, w8);

        NeuralNetwork* copiedNetwork;
        copiedNetwork = network.copy();

        float si1 = randInput(); // Signal Input 1
        float si2 = randInput();
        CHECK(network.setInput(0, si1));
        CHECK(network.setInput(1, si2));
        CHECK(copiedNetwork->setInput(0, si1));
        CHECK(copiedNetwork->setInput(1, si2));

        // Update networks.
        network.update();
        copiedNetwork->update();

        CHECK_EQUAL(network.getOutput(0), copiedNetwork->getOutput(0));
        CHECK_EQUAL(network.getOutput(1), copiedNetwork->getOutput(1));

        delete copiedNetwork;
    }

    TEST(Removal)
    {
        NeuralNetwork network;

        // Create input
        Neuron* i1 = network.addInput();

        // Create output
        Neuron* o1 = network.addOutput();
 
        // Create branch "A"
        Neuron* a1 = network.addIntermediate();
        i1->connect(a1, randWeight());
        Neuron* a2 = network.addIntermediate();
        a1->connect(a2, randWeight());
        Neuron* aa1 = network.addIntermediate();
        a2->connect(aa1, randWeight());
        Neuron* ab1 = network.addIntermediate();
        a2->connect(ab1, randWeight());
        Neuron* a3 = network.addIntermediate();
        aa1->connect(a3, randWeight());
        ab1->connect(a3, randWeight());

        // Create branch "B"
        Neuron* b1 = network.addIntermediate();
        i1->connect(b1, randWeight());

        // Connect to outputs
        a3->connect(o1, randWeight());
        b1->connect(o1, randWeight());

        CHECK_EQUAL(8, network.getNumNeurons());

        network.removeNeuron(a2);

        // After deleting "a2", the input could not reach the output using branch "A"
        // Only branch "B" remains.
        CHECK_EQUAL(3, network.getNumNeurons());
    }

    TEST(AddRemoveNeuron)
    {
        NeuralNetwork network;

        // Create input
        Neuron* i1 = network.addInput();

        // Create output
        Neuron* o1 = network.addOutput();

        i1->connect(o1, randWeight());

        // Because there is only one output, every neuron with try to connect to it after creation.
        // This is also because all new neurons are higher rank than previous ones..
        // However the output can only support up to MaxDentrites connections.
        // "i < MaxDentrites - 1" because the input neuron is already connected to output and uses one dentrite.
        for(uint i = 0; i < MaxDentrites - 1; ++i) 
        {
            CHECK(network.tryCreateAnyNeuron());
        }

        // After that all newly created neuron will fail.
        CHECK(!network.tryCreateAnyNeuron());

        // Remove any intermediate neuron.
        CHECK(network.tryRemoveAnyNeuron());

        // Adding a new neuron will succeed again.
        CHECK(network.tryCreateAnyNeuron());
    }

    TEST(Evolve)
    {
        Vector<NeuralNetwork*> networks;

        // Create networks.
        static const uint nNetworks = 10;
        for(uint i = 0; i < nNetworks; ++i)
        {
            NeuralNetwork* network = new NeuralNetwork();

            // Create input
            Neuron* i1 = network->addInput();

            // Create output
            Neuron* o1 = network->addOutput();

            i1->connect(o1, randWeight());

            networks.push_back(network);
        }

        // Cleanup
        for(auto network : networks)
        {
            delete network;
        }
    }
}


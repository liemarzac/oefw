#include "../neuron.h"

using namespace OE_Neural;

SUITE(NeuronTestSuite)
{
    static float Tolerance = 0.01f;

    TEST(InvalidInputWhenDentriteIsNotSet)
    {
        Neuron neuron(0);
        CHECK(!neuron.setInput(0, 0.0f));
    }

    TEST(ValidInputWhenDentriteIsSet)
    {
        Neuron neuron(0);
        neuron.addDentrite(0.0f);
        CHECK(neuron.setInput(0, 0.0f));
    }

    TEST(ZeroAsOutputWithNoInput)
    {
        Neuron neuron(0);
        neuron.addDentrite(0.0f);
        float output = neuron.update();
        CHECK(output == 0.0f);
    }

    TEST(ZeroAsOutputWithZeroInput)
    {
        Neuron neuron(0);
        neuron.addDentrite(1.0f);
        neuron.setInput(0, 0.0f);
        float output = neuron.update();
        CHECK_CLOSE(0.0f, output, Tolerance);
    }

    TEST(ZeroAsOutputWithZeroWeight)
    {
        Neuron neuron(0);
        neuron.addDentrite(0.0f);
        neuron.setInput(0, 1.0f);
        float output = neuron.update();
        CHECK_CLOSE(0.0f, output, Tolerance);
    }

    TEST(OneAsOutput)
    {
        g_triggerFunction = sigmoidTriggerFunction;
        Neuron neuron(0);
        neuron.addDentrite(10.0f);
        neuron.setInput(0, 1.0f);
        float output = neuron.update();
        CHECK_CLOSE(1.0f, output, Tolerance);
    }

    TEST(MinusOneAsOutput)
    {
        Neuron neuron(0);
        neuron.addDentrite(-10.0f);
        neuron.setInput(0, 1.0f);
        float output = neuron.update();
        CHECK_CLOSE(-1.0f, output, Tolerance);
    }

    TEST(ZeroWithTwoOppositeInputs)
    {
        Neuron neuron(0);
        neuron.addDentrite(1.0f);
        neuron.addDentrite(1.0f);
        CHECK(neuron.setInput(0, 1.0f));
        CHECK(neuron.setInput(1, -1.0f));
        float output = neuron.update();
        CHECK_CLOSE(0.0f, output, Tolerance);
    }

    TEST(ZeroWithTwoOppositeWeights)
    {
        Neuron neuron(0);
        neuron.addDentrite(2.0f);
        neuron.addDentrite(-2.0f);
        CHECK(neuron.setInput(0, 1.0f));
        CHECK(neuron.setInput(1, 1.0f));
        float output = neuron.update();
        CHECK_CLOSE(0.0f, output, Tolerance);
    }

    TEST(ChainTwoNeurons)
    {
        Neuron neuron1(0);
        Neuron neuron2(1);

        float weight1 = 10.0f;
        neuron1.addDentrite(weight1);

        float weight2 = 1.0f;
        neuron1.connect(&neuron2, weight2);

        Vector<Neuron*> connectedNeurons;
        neuron1.getConnectedNeurons(connectedNeurons);
        CHECK_EQUAL(1, connectedNeurons.size());
        CHECK_EQUAL(&neuron2, connectedNeurons[0]);

        CHECK_EQUAL(0.0f, neuron1.getOutput());
        CHECK_EQUAL(0.0f, neuron2.getOutput());

        neuron1.update();
        CHECK_EQUAL(0.0f, neuron1.getOutput());
        CHECK_EQUAL(0.0f, neuron2.getOutput());

        float input1 = 0.06f;
        CHECK(neuron1.setInput(0, input1));

        neuron1.update();

        CHECK_CLOSE(
            OE_Neural::g_triggerFunction(input1 * weight1),
            neuron1.getOutput(),
            Tolerance);

        CHECK_EQUAL(0.0f, neuron2.getOutput());

        neuron2.update();

        CHECK_CLOSE(
            OE_Neural::g_triggerFunction(input1 * weight1),
            neuron1.getOutput(),
            Tolerance);

        CHECK_CLOSE(
            OE_Neural::g_triggerFunction(neuron1.getOutput() * weight2),
            neuron2.getOutput(),
            Tolerance);
    }
}


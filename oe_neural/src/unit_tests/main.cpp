#include <UnitTest++.h>
#include <iostream>
#include <TestReporterStdout.h>

// Tests
#include "test_incubator.h"
#include "test_neuron.h"
#include "test_neural_network.h"
#include "test_sequence.h"

#include "application.h"


// Undefine main defined by the SDL.
#ifdef main
#undef main
#endif

int main(int, char const * argv[])
{
    appInit();
    appSetExePath(argv[0]);
    const int testResult = UnitTest::RunAllTests();
    std::cin.get();
    return testResult;
}

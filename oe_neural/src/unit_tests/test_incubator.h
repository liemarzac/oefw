// External
#include <cmath>

// OE_Core
#include <oe_math.h>
#include <rand.h>
#include <stop_watch.h>

// OE_Neural
#include "../incubator.h"
#include "../neural_network.h"

using namespace OE_Neural;

SUITE(IncubatorTestSuite)
{
    float testSinus(NeuralNetwork* network)
    {
        float errorAccum = 0.0f;
        for(float input = 0.0f; input <= (float)(PI/2.0f); input += (float)(PI/20.0f))
        {
            network->setInput(0, input);
            network->update();
            float actual = network->getOutput(0);
            float expected = (float)sin(input);
            errorAccum += abs(actual - expected);
        }

        return errorAccum;
    }

    TEST(Incubate)
    {
        Incubator incubator(Incubator::BestScoreType::Minimum);
        g_testFunction = testSinus;
        incubator.makeNeuralNetworks(10000);
        seedRand(1354);
        StopWatch timer;
        timer.start();
        do
        {
            incubator.runTest();
            OE_Core::log(OE_CHANNEL_NEURAL, "best: %f, avg: %f, elite_avg: %f", incubator.getBestScore(), incubator.getAvgScore(), incubator.getAvgEliteScore());
            incubator.mutate();
        }
        while(incubator.getBestScore() > 0.01f);
        timer.stop();
        OE_Core::log(OE_CHANNEL_NEURAL, "Time: %f", timer.getElapsedTime());
    }
}


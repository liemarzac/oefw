#include "dna.h"

#include "neural_private.h"

// External
#include <cstdlib>
#include <cstring>
#include <assert.h>
#include <math.h>

// OE_Core
#include <rand.h>


namespace OE_Neural
{

#define MutationRate 0.001f

const char* DNA::HospitalNames[(unsigned int)Hospital::NumberOf] =
{
    "General Neurology",
    "Multiple Sclerosis",
    "Sleep",
    "Rascp",
    "Movement Disorder National",
    "Movement Disorder RoyalFree",
    "Epilepsy Dr.RunnGun",
    "Epilepsy Dr.Ericsson",
    "Neuro Genetics",
    "Moya Moya",
    "ICH",
    "Headache",
    "Cardio Stroke Heart Failure",
    "Day Care",
    "Bronchiectasis"
};

const char* DNA::DayNames[(unsigned int)Day::NumberOf] =
{
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
};

const char* DNA::DayTimeNames[(unsigned int)DayTime::NumberOf] =
{
    "AM",
    "PM",
};

const char* DNA::AttendantNames[(unsigned int)Attendant::NumberOf] =
{
    "Robbin",
    "Bola",
    "Miriam",
    "Elyssa",
    "Grace",
};

const char* DNA::FrequencyNames[(unsigned int)Frequency::NumberOf] =
{
    "EveryWeek",
    "EveryOtherWeek",
};

unsigned int DNA::BloodTakers[] = { 0, 1 };

DNA::DNA()
{
    m_fitness = 0.0f;
    m_errors = 0.0f;
    m_bonus = 0.0f;
}

DNA::~DNA()
{
}

void DNA::generate()
{
    Slot* slot = nullptr;

    unsigned int iSlot = 0;

    for(Week iWeek = 0; iWeek < NumWeeks; ++iWeek)
    {
        // Monday AM - GeneralNeurology
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Monday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::GeneralNeurology;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Tuesday AM - MultipleSclerosis
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Tuesday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::MultipleSclerosis;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Tuesday AM - Rascp
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Tuesday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::Rascp;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = true;
        ++iSlot;

        if(iWeek % 2 == 0)
        {
            // Tuesday PM - Sleep
            slot = m_slots + iSlot;
            slot->m_nAttendants = 2;
            randAttendants(slot);
            slot->m_day = Day::Tuesday;
            slot->m_dayTime = DayTime::PM;
            slot->m_hospital = Hospital::Sleep;
            slot->m_frequency = Frequency::EveryOtherWeek;
            slot->m_iWeek = iWeek;
            slot->m_bBloodRequired = true;
            slot->m_bAllDay = false;
            ++iSlot;
        }

        // Wednesday AM - DayCare
        slot = m_slots + iSlot;
        slot->m_nAttendants = 1;
        randAttendants(slot);
        slot->m_day = Day::Wednesday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::DayCare;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = false;
        slot->m_bAllDay = false;
        ++iSlot;

        // Wednesday AM - EpilepsyDrEricsson
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Wednesday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::EpilepsyDrEricsson;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Wednesday PM - Cardio
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Wednesday;
        slot->m_dayTime = DayTime::PM;
        slot->m_hospital = Hospital::CardioStrokeHeartFailure;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Thursday AM - Headache
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Thursday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::Headache;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Thursday AM - EpilepsyDrRunnGun
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Thursday;
        slot->m_dayTime = DayTime::AM;
        slot->m_hospital = Hospital::EpilepsyDrRunnGun;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        // Friday PM - NeuroGenetics
        slot = m_slots + iSlot;
        slot->m_nAttendants = 2;
        randAttendants(slot);
        slot->m_day = Day::Friday;
        slot->m_dayTime = DayTime::PM;
        slot->m_hospital = Hospital::NeuroGenetics;
        slot->m_frequency = Frequency::EveryWeek;
        slot->m_iWeek = iWeek;
        slot->m_bBloodRequired = true;
        slot->m_bAllDay = false;
        ++iSlot;

        if(iWeek % 2 == 1)
        {
            // Friday PM - Bronchiectasis
            slot = m_slots + iSlot;
            slot->m_nAttendants = 2;
            randAttendants(slot);
            slot->m_day = Day::Friday;
            slot->m_dayTime = DayTime::PM;
            slot->m_hospital = Hospital::Bronchiectasis;
            slot->m_frequency = Frequency::EveryOtherWeek;
            slot->m_iWeek = iWeek;
            slot->m_bBloodRequired = true;
            slot->m_bAllDay = false;
            ++iSlot;
        }
    }

    OE_CHECK(iSlot == NumSlots, OE_CHANNEL_NEURAL);
}

void DNA::fitness()
{
    m_fitness = 0.0f;

    m_errors = 0.0f;
    m_warnings = 0.0f;
    m_bonus = 0.0f;

    const float PerError = 10.0f;
    const float PerWarning = 5.0f;
    const float PerBonus = 5.0f;

    static const unsigned int ErrorValue = 100;

    // Check that an attendant with blood test skill is present.
    for (unsigned int i = 0; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];

        bool bBloodSkillTestPass = true;;
        if (!a.m_bBloodRequired)
            continue;

        bBloodSkillTestPass = false;
        for (unsigned int iAttendant = 0; iAttendant < a.m_nAttendants; ++iAttendant)
        {
            unsigned int nBloodTestSkilled = sizeof(BloodTakers) / sizeof(BloodTakers[0]);
            for (unsigned iBloodTestSkilled = 0; iBloodTestSkilled < nBloodTestSkilled; ++iBloodTestSkilled)
            {
                if ((unsigned int)a.m_attendants[iAttendant] == BloodTakers[iBloodTestSkilled])
                {
                    bBloodSkillTestPass = true;
                    break;
                }
            }

            if (bBloodSkillTestPass)
                break;
        }

        if (!bBloodSkillTestPass)
        {
            m_errors += PerError;
        }
    }

    // Make sure the same attendant does not cover multiple times the same slot.
    for(unsigned int i = 0; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];

        for (unsigned iAttendantA = 0; iAttendantA < a.m_nAttendants; ++iAttendantA)
        {
            for (unsigned iAttendantB = 0; iAttendantB < a.m_nAttendants; ++iAttendantB)
            {
                if(iAttendantA == iAttendantB)
                    continue;

                if(a.m_attendants[iAttendantA] == a.m_attendants[iAttendantB])
                {
                    m_errors += PerError;
                }
            }
        }
    }

    // Check that the same attendant is not in another slot the same day
    // and the same time.
    for (unsigned int i = 0; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];

        for (unsigned int j = 0; j < NumSlots; ++j)
        {
            if (i == j)
                continue;

            const Slot& b = m_slots[j];

            if (b.m_day != a.m_day)
                continue;

            if (b.m_dayTime != a.m_dayTime)
                continue;

            if(b.m_iWeek != a.m_iWeek)
                continue;

            for (unsigned iAttendantA = 0; iAttendantA < a.m_nAttendants; ++iAttendantA)
            {
                for (unsigned iAttendantB = 0; iAttendantB < b.m_nAttendants; ++iAttendantB)
                {
                    if (a.m_attendants[iAttendantA] == b.m_attendants[iAttendantB])
                    {
                        m_errors += PerError;
                    }
                }
            }
        }
    }

    // Check that the same attendant is not in another slot the same day somewhere else
    // if the slot required all day attendency.
    for (unsigned int i = 0; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];

        if (!a.m_bAllDay)
            continue;

        for (unsigned int j = 0; j < NumSlots; ++j)
        {
            if (i == j)
                continue;

            const Slot& b = m_slots[j];

            if (b.m_day != a.m_day)
                continue;

            if(b.m_iWeek != a.m_iWeek)
                continue;

            for (unsigned iAttendantA = 0; iAttendantA < a.m_nAttendants; ++iAttendantA)
            {
                for (unsigned iAttendantB = 0; iAttendantB < b.m_nAttendants; ++iAttendantB)
                {
                    if (a.m_attendants[iAttendantA] == b.m_attendants[iAttendantB])
                    {
                        m_errors += PerError;
                    }
                }
            }
        }
    }

    // Add warning if the same attendants are working 2 days in a raw.
    for(unsigned int i = 1; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];
        const Slot& b = m_slots[i - 1];

        if(a.m_nAttendants != b.m_nAttendants)
            continue;

        uint32 nMatchingAttendants = 0;

        for(unsigned iAttendantA = 0; iAttendantA < a.m_nAttendants; ++iAttendantA)
        {
            for(unsigned iAttendantB = 0; iAttendantB < b.m_nAttendants; ++iAttendantB)
            {
                if(a.m_attendants[iAttendantA] == b.m_attendants[iAttendantB])
                {
                    ++nMatchingAttendants;
                }
            }
        }

        if(nMatchingAttendants == a.m_nAttendants)
        {
            m_warnings += PerWarning;
        }
    }

    unsigned int attendancy[(uint)Attendant::NumberOf][(uint)Hospital::NumberOf];
    memset(attendancy, 0, sizeof(attendancy));

    for (unsigned int i = 0; i < NumSlots; ++i)
    {
        const Slot& a = m_slots[i];

        for (unsigned iAttendant = 0; iAttendant < a.m_nAttendants; ++iAttendant)
        {
            Attendant attendant = a.m_attendants[iAttendant];
            Hospital hospital = a.m_hospital;
            OE_CHECK(attendant < Attendant::NumberOf, OE_CHANNEL_NEURAL);
            OE_CHECK(hospital < Hospital::NumberOf, OE_CHANNEL_NEURAL);
            ++attendancy[(uint32)attendant][(uint32)hospital];
        }
    }

    //
    // Calculate standard deviation.

    // Calculate average.
    uint32 total = 0;
    for (uint32 iAttendant = 0; iAttendant < (uint32)Attendant::NumberOf; ++iAttendant)
    {
        for (uint32 iHospital = 0; iHospital < (uint32)Hospital::NumberOf; ++iHospital)
        {
            total += attendancy[iAttendant][iHospital];
        }
    }

    float average = (float)total / (float)((uint32)Attendant::NumberOf * (uint32)Hospital::NumberOf);

    // Calculate variance.
    float variance = 0.0f;
    for(uint32 iAttendant = 0; iAttendant < (uint32)Attendant::NumberOf; ++iAttendant)
    {
        for(uint32 iHospital = 0; iHospital < (uint32)Hospital::NumberOf; ++iHospital)
        {
            float diff = ((float)attendancy[iAttendant][iHospital] - average);
            variance += diff * diff;
        }
    }
    variance /= (float)Attendant::NumberOf;

    float standartDeviation = (float)sqrt(variance);

    m_fitness = 1.0f / (1.0f + m_errors + m_warnings + variance);
}

float DNA::getFitness() const
{
    return m_fitness;
}

float DNA::getErrors() const
{
    return m_errors;
}

void DNA::copyFrom(const DNA* original)
{
    memcpy(m_slots, original->m_slots, sizeof(m_slots));
    m_fitness = original->m_fitness;
    m_errors = original->m_errors;
}

void DNA::mixWith(const DNA* original)
{
	float mutationRate = MutationRate;

    // Apply crossover.
    int midpoint = randi32(0, (int)NumSlots);
    size_t copySize = (NumSlots - midpoint) * sizeof(Slot);
    if (copySize > 0)
    {
        memcpy(m_slots + midpoint, original->m_slots + midpoint, copySize);
    }

	// Apply mutations.
	for(size_t i = 0; i < NumSlots; i++)
	{
		const float rand = randr32(0.0f, 1.0f);
		if(rand < mutationRate)
		{
			randAttendants(m_slots + i);
		}
	}
}

void DNA::randAttendants(Slot* slot)
{
    for (unsigned int i = 0; i < slot->m_nAttendants; ++i)
    {
        slot->m_attendants[i] = (Attendant)randi32(0, (unsigned int)Attendant::NumberOf);
    }
}

const DNA::Slot* DNA::getSlots() const
{
	return m_slots;
}

}
